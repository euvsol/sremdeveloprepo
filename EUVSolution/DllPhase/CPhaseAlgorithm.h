#pragma once
using namespace std;
#include <vector>
#include <tuple>
#define _USE_MATH_DEFINES
#include <cmath>
#include <mil.h>
#include <algorithm>

#ifdef _DEBUG
#define _DEBUG_WAS_DEFINED 1
#undef _DEBUG
#endif

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy\arrayobject.h>
#include <numpy\npy_3kcompat.h>

#ifdef _DEBUG_WAS_DEFINED
#define _DEBUG
#endif

#define X_DIRECTION 0
#define Y_DIRECTION 1


class AFX_EXT_CLASS CPhaseAlgorithm : public CECommon
{
public:
	CPhaseAlgorithm();
	~CPhaseAlgorithm();

private:
	int PythonInitialize();
	PyObject* m_pyName, *m_pyModule, *m_pyFuncFineAlignCurveFit, *m_pyFuncHomograpy, *m_pyFuncEphaseFFT, *m_pyFuncFineAlignCurveFit2;

public:
	int LoopBackTest(double* pos, double* intensity, int nSize, int nDir, int slope, double refAbs, double refMl);
	tuple<double, double, double, double, double*, int> PhaseFineAlignCurveFit(double* pos, double* intensity, int N, int nDir, int slope, double refAbs, double refMl);
	tuple<double, double, double, double, double*, int> PhaseFineAlignCurveFit2(double* pos, double* intensity, int N, int nDir, int slope, double refAbs, double refMl);
	void Homograpy(double* h, int n, double* xin, double* yin, double* xout, double* yout);
	void HomographyTransform(double x, double y, double *h, double & xout, double & yout);
	double AngleDiffrence(double ref, double target);
	void AngleDiffrence(double* ref, double* target, double* result, int n);
	void EPhaseFFT(int n_dataSize, double* intensity, int n_fftSize, double t_sampling, double* magnitude, double* phase, double* frequncy = NULL);
	void ImageProjection(MIL_ID milImage, double*arrProjection, int nSize);

	MIL_ID m_MilSystem = M_NULL;
	void MilInitialize(MIL_ID milSystem);


	// Reference Data
	double m_CenterWaveLengthFromFft = 0;
	int m_CenterFreqIndex = 0;
	int m_ShiftPos = 0;
	double m_ReferencePhase = 0;


	double m_CenterWaveLengthFromCw = 0;


	void FindCenterFrequency();
	void AllocMeasure(int dataSize);
	void FreeMeasure();


};
