/**
 * FST EUV Source Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

#pragma once
class AFX_EXT_CLASS CFSTEuvSourceCtrl : public CEthernetCom
{
public:
	CFSTEuvSourceCtrl();
	~CFSTEuvSourceCtrl();

	virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int				ReceiveData(char *lParam); ///< Recive

	/**
	*@var CString m_sEuvStatus
	EUV 발진 확인 변수
	*/
	CString m_sEuvStatus;

	/**
	*@var CString m_sLaserShutterStatus
	EUV Gate 상태 확인 변수
	*/
	CString m_sLaserShutterStatus;

	/**
	*@var CString m_sMainVacuum
	Source Main Chamber 진공 값
	*/
	CString m_sMainVacuum;

	/**
	*@var CString m_sSubVacuum
	Source Sub Chamber 진공 값
	*/
	CString m_sSubVacuum;

	/**
	*@var CString m_sBufVacuum
	Source Buffer Chamber 진공 값
	*/
	CString m_sBufVacuum;

	/**
	*@var CString m_sMFCNeonGauge
	Source MFC Neon 게이지 값
	*/
	CString m_sMFCNeonGauge;

	/**
	*@var CString m_sNeonGasGauge
	Source Neon Gas 게이지 값
	*/
	CString m_sNeonGasGauge;

	/**
	*@var CString m_sForelineGauge
	Source Foreline 게이지 값
	*/
	CString m_sForelineGauge;

	/**
	*@var CString m_sCurrentPos
	Source 모니터링 CCD를 위한 스테이지 위치
	*/
	CString m_sCurrentPos;

	/**
	*@var CString m_sMechShutterStatus
	Source 메카니컬 셔터 오픈 상태
	*/
	CString m_sMechShutterStatus;


	/**
	*@fn void ParsingData(CString strRecvMsg)
	*@brief 수신 받은 데이터 파싱
	*@date 2019/07/31
	*@param strRecvMsg 수신받은 메시지 내용
	*/
	void					ParsingData(CString strRecvMsg);

	/**
	*@fn int SRC_StartVacuum()
	*@brief Chamber Pumping 시작
	*@date 2019/07/31
	*/
	int						SRC_StartVacuum();

	/**
	*@fn int SRC_StopVacuum()
	*@brief Chamber Venting 시작
	*@date 2019/07/31
	*/
	int						SRC_StopVacuum();

	/**
	*@fn int SRC_StartEuv()
	*@brief EUV On 동작
	*@date 2019/07/31
	*/
	int						SRC_StartEuv();

	/**
	*@fn int SRC_StopEuv()
	*@brief EUV Off 동작
	*@date 2019/07/31
	*/
	int						SRC_StopEuv();

	/**
	*@fn int SRC_OpenShutter()
	*@brief 메카니컬 셔터 Open 동작
	*@date 2019/07/31
	*/
	int						SRC_OpenShutter();

	/**
	*@fn int SRC_CloseShutter()
	*@brief 메카니컬 셔터 Close 동작
	*@date 2019/07/31
	*/
	int						SRC_CloseShutter();

	/**
	*@fn int SRC_MoveBeamPassPos()
	*@brief Beam Pass 위치로 이동
	*@date 2019/07/31
	*/
	int						SRC_MoveBeamPassPos();

	/**
	*@fn int SRC_MoveBeamSplitPos()
	*@brief Beam Split 위치로 이동
	*@date 2019/07/31
	*/
	int						SRC_MoveBeamSplitPos();

	/**
	*@fn int SRC_MoveBeamAlignPos()
	*@brief Beam Align 위치로 이동
	*@date 2019/07/31
	*/
	int						SRC_MoveBeamAlignPos();

	/**
	*@fn int SRC_Status()
	*@brief Source 상태 요청
	*@date 2019/07/31
	*/
	int						SRC_Status();

	/**
	*@fn int SRC_SetMirrorPos(double dPos)
	*@brief 미러 스테이지 위치 이동
	*@date 2019/07/31
	*@param 스테이지 이동 위치값 (-13 ~ 13)
	*/
	int						SRC_SetMirrorPos(double dPos);
};

