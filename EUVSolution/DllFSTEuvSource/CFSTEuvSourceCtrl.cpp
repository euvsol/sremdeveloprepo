#include "stdafx.h"
#include "CFSTEuvSourceCtrl.h"

CFSTEuvSourceCtrl::CFSTEuvSourceCtrl()
{
	m_sEuvStatus			= _T("");
	m_sLaserShutterStatus	= _T("");
	m_sMainVacuum			= _T("");
	m_sSubVacuum			= _T("");
	m_sBufVacuum			= _T("");
	m_sMFCNeonGauge			= _T("");
	m_sNeonGasGauge			= _T("");
	m_sForelineGauge		= _T("");
	m_sCurrentPos			= _T("");
	m_sMechShutterStatus	= _T("");

	m_strReceiveEndOfStreamSymbol = _T("\r");
}

CFSTEuvSourceCtrl::~CFSTEuvSourceCtrl()
{
}

int	CFSTEuvSourceCtrl::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	
	m_strSendMsg = lParam;
	SaveLogFile("SrcPc_Com", _T((LPSTR)(LPCTSTR)("PC -> SrcPc : " + m_strSendMsg)));	//통신 상태 기록.
	nRet = Send(lParam, nTimeOut);
	if (nRet != 0)
	{
		CString Temp;
		Temp.Format(_T("PC -> SrcPc : SEND FAIL (%d)"), nRet);
		SaveLogFile("SrcPc_Com", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
	}
	
	return nRet;
}

int CFSTEuvSourceCtrl::ReceiveData(char *lParam)
{
	m_strReceivedMsg = lParam;
	SaveLogFile("SrcPc_Com", _T((LPSTR)(LPCTSTR)("SrcPc -> PC : " + m_strReceivedMsg))); //통신 상태 기록.
	ParsingData(m_strReceivedMsg);

	return 0;
}

void CFSTEuvSourceCtrl::ParsingData(CString strRecvMsg)
{
	if (strRecvMsg == _T("")) return;

	AfxExtractSubString(m_sEuvStatus, strRecvMsg, 0, _T(','));
	AfxExtractSubString(m_sLaserShutterStatus, strRecvMsg, 1, _T(','));
	AfxExtractSubString(m_sMainVacuum, strRecvMsg, 2, _T(','));
	AfxExtractSubString(m_sSubVacuum, strRecvMsg, 3, _T(','));
	AfxExtractSubString(m_sBufVacuum, strRecvMsg, 4, _T(','));
	AfxExtractSubString(m_sMFCNeonGauge, strRecvMsg, 5, _T(','));
	AfxExtractSubString(m_sNeonGasGauge, strRecvMsg, 6, _T(','));
	AfxExtractSubString(m_sForelineGauge, strRecvMsg, 7, _T(','));
	AfxExtractSubString(m_sCurrentPos, strRecvMsg, 8, _T(','));
	AfxExtractSubString(m_sMechShutterStatus, strRecvMsg, 9, _T(','));
}

int CFSTEuvSourceCtrl::SRC_StartVacuum()
{
	int ret = 0;

	CString strCommand = _T("@VACUUM_START") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_StopVacuum()
{
	int ret = 0;

	CString strCommand = _T("@VACUUM_STOP") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_StartEuv()
{
	int ret = 0;

	CString strCommand = _T("@EUV_START") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_StopEuv()
{
	int ret = 0;

	CString strCommand = _T("@EUV_STOP") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_OpenShutter()
{
	int ret = 0;

	CString strCommand = _T("@SHUTTER_OPEN") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_CloseShutter()
{
	int ret = 0;

	CString strCommand = _T("@SHUTTER_CLOSE") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_MoveBeamPassPos()
{
	int ret = 0;

	CString strCommand = _T("@PI_MOVE_BEAMPASS") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_MoveBeamSplitPos()
{
	int ret = 0;

	CString strCommand = _T("@PI_MOVE_BEAMSPLIT") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_MoveBeamAlignPos()
{
	int ret = 0;

	CString strCommand = _T("@PI_MOVE_ALIGN") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_Status()
{
	int ret = 0;

	CString strCommand = _T("@STATUS") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_SetMirrorPos(double dPos)
{
	int ret = 0;

	CString sTmp;
	sTmp.Format("%f", dPos);
	CString strCommand = _T("@PI_MOVE_POS,") + sTmp + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}
