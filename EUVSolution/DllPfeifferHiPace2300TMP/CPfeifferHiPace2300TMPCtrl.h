/**
 * Pfeiffer HiPace 2300 TMP Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

#pragma once
class AFX_EXT_CLASS CPfeifferHiPace2300TMPCtrl : public CSerialCom
{
public:
	CPfeifferHiPace2300TMPCtrl();
	~CPfeifferHiPace2300TMPCtrl();


	//virtual int			SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 1);
	//virtual int			SendData(int nSize, BYTE *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	//virtual int			SendData(int nSize, char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	//void					ParsingData(CString strRecvMsg);
	//CString				str_mc_tmp_RecevieData;						            //Receive 된 Data 저장 변수
	//CString				str_mc_tmp_ReceiveDataHz;								// HZ
	//CString				str_mc_tmp_ReceiveDataState;							// STATE
	//CString				str_mc_tmp_ReceiveDataErrorCode;						//	ERROR CODE
	//CString				str_mc_tmp_ReceiveDataErrorState;						//	ERROR STATE 
	//CString				str_mc_tmp_ReceiveDataOnOffState;						// ON/OFF STATE
	//CString				str_mc_tmp_ReceiveDataRpm;								// 현재 rpm
	//CString				str_mc_tmp_ReceiveDataTemperature_1;					//전자 장치 온도
	//CString				str_mc_tmp_ReceiveDataTemperature_2;					//펌프 하단부 온도
	//CString				str_mc_tmp_ReceiveDataTemperature_3;					//모터온도	
	//CString				str_mc_tmp_ReceiveDataTemperature_4;					//베어링 온도	
	//CString				str_mc_tmp_Error_War_State;								// Error , Warning 구분
	//CString				str_mc_tmp_ReveiveDataSetHz;							// 현재 설정 값 hz
	//CString				str_mc_tmp_ReveiveDataSetRpm;							// 현재 설정 값 rpm
	//bool					m_bool_mc_tmp_speed_set;					//목표 속도 도달 
	

	virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual int				OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);
	virtual int				ReceiveData(char *lParam, DWORD dwRead);


	
	void initialize();

	HANDLE m_hPumpOnEvent;
	HANDLE m_hPumpOffEvent;
	HANDLE m_hResetPumpEvent;

	int WaitExecGetPumpOnEvent(int nTimeout);
	int WaitExecGetPumpOffEvent(int nTimeout);
	int WaitExecGetResetPumpEvent(int nTimeout);

	int			SetPumpOn();
	int			SetPUmpOff();
	int			ResetPump();
	int			GetTmpHz();
	int			GetErrorView();
	int 		GetTmpSetHz();
	int			GetTmpSetRpm();
	int 		GetTmpRpm();
	int			GetTmpSetSpeedOn();
	int			GetTmpElectronicDeviceTemp();
	int			GetTmpLowerPartTemp();
	int			GetTmpMotorTemp();
	int			GetTmpBearingTemp();
	
	//void		GetUpdate();
	//CString	GetCheckSumValue(char *command);
	
	CString		GetCheckSumValue(CString command);
	CString		str_CR;
};

