#include "stdafx.h"
#include "CPfeifferHiPace2300TMPCtrl.h"


CPfeifferHiPace2300TMPCtrl::CPfeifferHiPace2300TMPCtrl()
{
	//str_mc_tmp_ReceiveDataErrorCode = "No Error";

	m_hPumpOnEvent	  = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hPumpOffEvent	  = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hResetPumpEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	str_CR = "\r";
}

CPfeifferHiPace2300TMPCtrl::~CPfeifferHiPace2300TMPCtrl()
{
	//str_mc_tmp_RecevieData.Empty();
	//str_mc_tmp_ReceiveDataHz.Empty();
	//str_mc_tmp_ReceiveDataState.Empty();
	//str_mc_tmp_ReceiveDataErrorCode.Empty();
	//str_mc_tmp_ReceiveDataErrorState.Empty();
	//str_mc_tmp_ReceiveDataTemperature_1.Empty();
	//str_mc_tmp_ReceiveDataTemperature_2.Empty();
	//str_mc_tmp_ReceiveDataTemperature_3.Empty();
	//str_mc_tmp_Error_War_State.Empty();

	CloseHandle(m_hPumpOnEvent);
	CloseHandle(m_hPumpOffEvent);
	CloseHandle(m_hResetPumpEvent);
}

void CPfeifferHiPace2300TMPCtrl::initialize()
{

}

int CPfeifferHiPace2300TMPCtrl::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	m_strSendMsg = lParam;
	nRet = Send(lParam, nTimeOut, nRetryCnt);
	
	if (nRet != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : " + m_strSendMsg + "  Send Fail")));	//통신 상태 기록.
		nRet = -81002;
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : " + m_strSendMsg)));	//통신 상태 기록.
	}

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
{

	int nRet = -1;

	nRet = OpenSerialPort(sPortName, dwBaud, wByte, wStop, wParity);

	if (nRet != 0) 
	{
		m_strSendMsg = _T("TMP Pfeiffer SERIAL PORT OPEN FAIL");
	}
	else 
	{
		m_strSendMsg = _T("TMP Pfeiffer SERIAL PORT OPEN");
	}
	SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP : " + m_strSendMsg))); //통신 상태 기록.


	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::ReceiveData(char *lParam, DWORD dwRead)
{
	int nRet = 0;

	//str_mc_tmp_RecevieData = (LPSTR)lParam;
	//ParsingData(str_mc_tmp_RecevieData);

	return nRet;
}

/*

void CPfeifferHiPace2300TMPCtrl::ParsingData(CString strRecvMsg)
{

	int	cnt = 0;

	CString mc_tmp_ReData = strRecvMsg;
	
	CString Command;			// 명령값 추출
	CString SubDataCommand;		// 리시브 값 추출
	CString Command_Error_War;	// Error 코드 중 Error , Warning 구분
	
	CString pre_data;


	Command = mc_tmp_ReData.Mid(5, 3);      //명령값
	SubDataCommand = mc_tmp_ReData.Mid(10, 6);  //데이터값

	//ReData = lParam;
	//Received 데이터가 정상 데이터이면 Signal 해준다.
	SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]:  Command : " + Command + "    ReceiveData : " + SubDataCommand))); //통신 상태 기록.

	// 펌핑스테이션
	if (Command == "010")
	{
		if (SubDataCommand == "000000")
		{
			str_mc_tmp_ReceiveDataOnOffState = "TMP Turn Off";
			SetEvent(m_hPumpOffEvent);
		}
		else if (SubDataCommand == "111111")
		{
			str_mc_tmp_ReceiveDataOnOffState = "TMP Turn On";
			SetEvent(m_hPumpOnEvent);

		}
		else
		{
			mc_tmp_ReData = _T("ERROR");
			SetEvent(m_hPumpOnEvent);
			SetEvent(m_hPumpOffEvent);
		}

	}
	// 실제 회전속도 (hz)
	else if (Command == "309")
	{
		mc_tmp_ReData = mc_tmp_ReData.Mid(10, 6);

		if (SubDataCommand == "000000") 
		{
			str_mc_tmp_ReceiveDataState = _T("Stop");
			str_mc_tmp_ReceiveDataOnOffState = _T("TMP Turn Off");
		}
		if (SubDataCommand == "000525")
		{
			str_mc_tmp_ReceiveDataState = _T("Running");
			str_mc_tmp_ReceiveDataOnOffState = _T("TMP Turn On");
		}
		else if (SubDataCommand < pre_data)
		{
			str_mc_tmp_ReceiveDataState = _T("Deceleration");
			SetEvent(m_hPumpOffEvent);
		}
		else if (SubDataCommand > pre_data)
		{
			str_mc_tmp_ReceiveDataState = _T("Acceleration");
			SetEvent(m_hPumpOnEvent);
		}
		else if (SubDataCommand == pre_data)
		{
			cnt++;
			if (cnt == 2000) {
				str_mc_tmp_ReceiveDataState = _T("Running");
				cnt = 0;
			}
		}

		//mc_tmp_ReState = "TMP RUNNING";
		pre_data = mc_tmp_ReData;
		str_mc_tmp_ReceiveDataHz = mc_tmp_ReData + " Hz";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: "+ str_mc_tmp_ReceiveDataState +" ::  "+ str_mc_tmp_ReceiveDataHz))); //통신 상태 기록.
	}
	////목표 속도 도달 
	else if (Command == "306")
	{
		if (SubDataCommand == "000000") m_bool_mc_tmp_speed_set = false;
		else if (SubDataCommand == "111111") m_bool_mc_tmp_speed_set = true;
		else m_bool_mc_tmp_speed_set = false;
	}
	//설정 Hz
	else if (Command == "308")
	{
		str_mc_tmp_ReveiveDataSetHz = SubDataCommand + " Hz";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 설정 Hz  :: " + str_mc_tmp_ReveiveDataSetHz))); //통신 상태 기록.
	}
	//설정 rpm
	else if (Command == "397")
	{
		str_mc_tmp_ReveiveDataSetRpm = SubDataCommand + " rpm";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 설정 Rpm :: " + str_mc_tmp_ReveiveDataSetRpm))); //통신 상태 기록.
	}
	//현재 rpm
	else if (Command == "398")
	{
		str_mc_tmp_ReceiveDataRpm = SubDataCommand + " rpm";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 현재 Rpm :: " + str_mc_tmp_ReceiveDataRpm))); //통신 상태 기록.
	}
	//전자 장치 온도
	else if (Command == "326")
	{
		str_mc_tmp_ReceiveDataTemperature_1 = SubDataCommand + "℃";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 전자 장치 온도 :: " + str_mc_tmp_ReceiveDataTemperature_1))); //통신 상태 기록.
		// 000042
	}
	//펌프 하단부 온도
	else if (Command == "330")
	{
		str_mc_tmp_ReceiveDataTemperature_2 = SubDataCommand + "℃";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 펌프 하단부 온도 :: " + str_mc_tmp_ReceiveDataTemperature_2))); //통신 상태 기록.
		// 000022
	}
	//모터온도	
	else if (Command == "346")
	{
		str_mc_tmp_ReceiveDataTemperature_3 = SubDataCommand + "℃";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 모터 온도 :: " + str_mc_tmp_ReceiveDataTemperature_3))); //통신 상태 기록.
		// 000042
	}
	//베어링 온도	
	else if (Command == "342")
	{
		str_mc_tmp_ReceiveDataTemperature_4 = SubDataCommand + "℃";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 베어링 온도 :: " + str_mc_tmp_ReceiveDataTemperature_4))); //통신 상태 기록.
	}
	//오류코드
	else if (Command == "303")
	{
		Command_Error_War = SubDataCommand.Mid(1, 3);
		if (Command_Error_War == "Err")
		{
			str_mc_tmp_Error_War_State = "ERORR";
			str_mc_tmp_ReceiveDataState = "Error";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  :: " + str_mc_tmp_ReceiveDataState))); //통신 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE : " + str_mc_tmp_ReceiveDataState ))); //ERROR 상태 기록
		}

		if (SubDataCommand == "Err001")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err001 ";  // 과잉 회전 속도 문제
			str_mc_tmp_ReceiveDataErrorState = "과잉 회전 속도";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err002")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err002 "; // 과잉 전압 문제
			str_mc_tmp_ReceiveDataErrorState = "과잉 전압";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err006")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err006 "; //: 런업 오류
			str_mc_tmp_ReceiveDataErrorState = "런업 오류(확인필요)";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err007")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err007 "; // 작동유 낮음
			str_mc_tmp_ReceiveDataErrorState = "작동유 낮음";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err008")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err008 "; // 전자드라이브 유닛 <-> 터보 펌프 연결 결함
			str_mc_tmp_ReceiveDataErrorState = "전자드라이브 유닛 연결 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err010")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err010 "; // 내부 장치 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 장치 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err021")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err021 "; // 전자 드라이브 유닛이 터보 펌프를 감지 못함
			str_mc_tmp_ReceiveDataErrorState = "전자드라이브 유닛 터보 펌프 감지 못함 발생";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err041")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err041 "; // 전자 드라이브 유닛이 터보 펌프를 감지 못함
			str_mc_tmp_ReceiveDataErrorState = "드라이브 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err043")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err043 "; // 내부 구성 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 구성 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err044")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err044 "; // 과잉 온도, 전자 장치 ( 불충분한 냉각 )
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도, 전자 장치";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err045")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err045 "; // 과잉 온도, 모터 ( 불충분한 냉각 )
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도, 모터";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err046")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err046 "; // 내부 초기화 오류 ( 장치 결함 )
			str_mc_tmp_ReceiveDataErrorState = "내부 초기화 오류 (장치 결함)";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err073")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err073 "; // 축 방향의 자기 베어링 과부하 (압력 상승률이 너무 높음)
			str_mc_tmp_ReceiveDataErrorState = "축 방향의 자기 베어링 과부하 (압력 상승률이 너무 높음)";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err074")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err074 "; // 방시상의 자기 베어링 과부하 (압력 상승률이 너무 높음)
			str_mc_tmp_ReceiveDataErrorState = "방시상의 자기 베어링 과부하 (압력 상승률이 너무 높음)";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err089")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err089 "; // 로터 불안정
			str_mc_tmp_ReceiveDataErrorState = "로터 불안정";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err091")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err091 "; //내부 장치 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 장치 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err092")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err092 "; //알수 없는 연결 패널
			str_mc_tmp_ReceiveDataErrorState = "알수 없는 연결 패널";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err093")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err093 "; // 모터 온도 평가 결함
			str_mc_tmp_ReceiveDataErrorState = "모터 온도 평가 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err094")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err094 "; // 전자 장치 온도 평가 결함
			str_mc_tmp_ReceiveDataErrorState = "전자 장치 온도 평가 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err098")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err098 "; // 내부 통신 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 통신 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err107")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err107 "; // 최종 단계 그룹 오류
			str_mc_tmp_ReceiveDataErrorState = "최종 단계 그룹 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err108")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err108 "; // 회전 속도 측정 결함
			str_mc_tmp_ReceiveDataErrorState = "회전 속도 측정 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err109")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err109 "; // 소프트웨어 릴리스 되지 않음
			str_mc_tmp_ReceiveDataErrorState = "소프트웨어 릴리스 되지 않음";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err110")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err110 "; // 작동유 센서 결함
			str_mc_tmp_ReceiveDataErrorState = "작동유 센서 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err111")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err111 "; // 작동유 펌프 통신 오류
			str_mc_tmp_ReceiveDataErrorState = "작동유 펌프 통신 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err112")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err112 "; // 작동유 펌프 그룹 오류
			str_mc_tmp_ReceiveDataErrorState = "작동유 펌프 그룹 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err113")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err113 "; //: 과잉온도,펌프하단부
			str_mc_tmp_ReceiveDataErrorState = "로터온도부정확";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err114")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err114 "; //최종 단계 온도 평가 결함
			str_mc_tmp_ReceiveDataErrorState = "최종 단계 온도 평가 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err117")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err117 "; //: 과잉온도,펌프하단부
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도 펌프 하단부";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err118")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err118 "; // 과잉 온도 ,최동 단계
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도 최종 단계";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err119")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err119 "; //: 과잉온도,베어링
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도 베어링";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err143")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err143 "; //: 과잉온도,작동유 펌프
			str_mc_tmp_ReceiveDataErrorState = "작동유 펌프 과잉 온도";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err777")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err777 "; // 공칭 회전 속도가 확인 되지 않음
			str_mc_tmp_ReceiveDataErrorState = "공칭 회전 속도가 확인 되지 않음";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err800")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err800 "; //자기 베어링 과류
			str_mc_tmp_ReceiveDataErrorState = "자기 베어링 과류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err802")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err802 "; //자기 베어링 센서 기술 결함
			str_mc_tmp_ReceiveDataErrorState = "자기 베어링 센서 기술 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err810")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err810 "; //내부 구성 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 구성 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err815")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err815 "; //자기 베어링 과류
			str_mc_tmp_ReceiveDataErrorState = "자기 베어링 과류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err890")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err890 "; //안전 베어링 마모
			str_mc_tmp_ReceiveDataErrorState = "안전 베어링 마모";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "Err891")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err891 "; //로터 불균형이 너무 높음
			str_mc_tmp_ReceiveDataErrorState = "로터 불균형이 너무 높음";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (SubDataCommand == "000000")
		{
			str_mc_tmp_ReceiveDataErrorCode = "No Error";
			str_mc_tmp_ReceiveDataErrorState = "No Error";
			str_mc_tmp_Error_War_State = "No Error";
		}

	}
	// UnKnown.
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: UnKnown Command :: " + Command + ",  UnKnown SubData :: " + SubDataCommand))); //통신 상태 기록.
	}
	
}

*/

//CString CPfeifferHiPace2300TMPCtrl::GetCheckSumValue(char *command)
//{
//
//	CString strCommand;
//	strCommand.Format("%s", command);
//
//
//	byte	btCheckSum = 0x00;
//	CString strCS;
//
//	for (int nIdx = 0; nIdx < strCommand.GetLength(); nIdx++)
//	{
//		btCheckSum += (byte)strCommand[nIdx];
//	}
//
//	sprintf_s(LPSTR(LPCTSTR(strCS)), sizeof(strCS), "%03d", btCheckSum);
//	strCS.Format("%s", strCS);
//	strCommand.Append(strCS);
//	//sprintf_s(LPSTR(LPCTSTR(strCS)), sizeof(strCS), "%02X", btCheckSum);
//
//	return strCommand;
//}

CString CPfeifferHiPace2300TMPCtrl::GetCheckSumValue(CString command)
{

	CString strCommand = command;
	byte	btCheckSum = 0x00;
	CString strCS;

	for (int nIdx = 0; nIdx < strCommand.GetLength(); nIdx++)
	{
		btCheckSum += (byte)strCommand[nIdx];
	}

	sprintf_s(LPSTR(LPCTSTR(strCS)), sizeof(strCS), "%03d", btCheckSum);
	strCS.Format("%s", strCS);
	strCommand.Append(strCS);
	//sprintf_s(LPSTR(LPCTSTR(strCS)), sizeof(strCS), "%02X", btCheckSum);

	return strCommand;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpHz()
{
	int nRet = 0;
	//char* mc_command;
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020030902=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetErrorView()
{
	int nRet = 0;
	//char* mc_command;

	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020030302=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpSetHz()
{
	int nRet = 0;
	//char* mc_command;
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	//mc_command = "0020039702 = ?";
	str_mc_command = _T("0020030802=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpSetRpm()
{
	int nRet = 0;
	//char* mc_command;

	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	//mc_command = "0020039702 = ?";
	str_mc_command = _T("0020039702=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpRpm()
{
	int nRet = 0;
	//char* mc_command;
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();


	str_mc_command = _T("0020039802=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpSetSpeedOn()
{
	int nRet = 0;
	//char* mc_command;
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();


	str_mc_command = _T("0020030602=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpElectronicDeviceTemp()
{
	int nRet = 0;
	//char* mc_command;
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();


	str_mc_command = _T("0020032602=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpLowerPartTemp()
{
	int nRet = 0;
	//char* mc_command;
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020033002=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpMotorTemp()
{
	int nRet = 0;
	//char* mc_command;
	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020034602=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::GetTmpBearingTemp()
{
	int nRet = 0;
	//char* mc_command;

	CString str_mc_command;
	CString str_command;

	str_mc_command.Empty();
	str_command.Empty();

	str_mc_command = _T("0020034202=?");
	str_command = GetCheckSumValue(str_mc_command);
	str_command.Append(str_CR);


	nRet = SendData((LPSTR)(LPCTSTR)str_command);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::SetPumpOn()
{
	int nRet = 0;

	CString sCommand = "0021001006111111016\r";
	ResetEvent(m_hPumpOnEvent);
	nRet = SendData((LPSTR)(LPCTSTR)sCommand);
	if (nRet != 0)
		SetEvent(m_hPumpOnEvent);
	else
		nRet = WaitExecGetPumpOnEvent(500);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::SetPUmpOff()
{
	int nRet = 0;

	CString sCommand = "0021001006000000010\r";

	ResetEvent(m_hPumpOffEvent);
	nRet = SendData((LPSTR)(LPCTSTR)sCommand);
	if (nRet != 0)
		SetEvent(m_hPumpOffEvent);
	else
		nRet = WaitExecGetPumpOffEvent(500);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::ResetPump()
{
	int nRet = 0;

	CString sCommand = "0021000906111111024\r";
	
	ResetEvent(m_hResetPumpEvent);
	nRet = SendData((LPSTR)(LPCTSTR)sCommand);
	if (nRet != 0)
		SetEvent(m_hResetPumpEvent);
	else
		nRet = WaitExecGetResetPumpEvent(500);

	return nRet;
}

int CPfeifferHiPace2300TMPCtrl::WaitExecGetPumpOnEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hPumpOnEvent, nTimeout) != WAIT_OBJECT_0)
		return -1;  //타임아웃 시 리턴

	//만약 에러가 발생하면 에러코드 리턴

	return 0;
}

int CPfeifferHiPace2300TMPCtrl::WaitExecGetPumpOffEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hPumpOffEvent, nTimeout) != WAIT_OBJECT_0)
		return -1;	//타임아웃 시 리턴

	//만약 에러가 발생하면 에러코드 리턴

	return 0;
}

int CPfeifferHiPace2300TMPCtrl::WaitExecGetResetPumpEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hResetPumpEvent, nTimeout) != WAIT_OBJECT_0)
		return -1;	//타임아웃 시 리턴

	//만약 에러가 발생하면 에러코드 리턴

	return 0;
}

