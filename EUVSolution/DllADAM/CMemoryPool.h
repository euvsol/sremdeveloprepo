
#pragma once

#include <atomic>
#include <vector>
#include <condition_variable>
#include <mutex>
#include <queue>

using namespace std;

//using std::unique_lock;
//using std::conditional; 
//using std::mutex;

//template <typename T>
//class MemoryPool;

template <typename T>

class MemoryPool : public CECommon
{

public:

	MemoryPool()
	{
		MemoryTotalCount = 0;
	
	};

	~MemoryPool()
	{
		cv_pool.notify_all();
	};

public:

	deque<T> MemoryPoolQue;

	int MemoryTotalCount;
	mutex mutexMemoryPool;

	condition_variable cv_pool;

	CString objectName;
	CString strLog;
	CString strTemp;

	T Acquire()
	{
		unique_lock<mutex> lock(mutexMemoryPool);

		T object;

		while (true)
		{
			if (MemoryPoolQue.empty())
			{
				// Memory Full Log 삽입
				cv_pool.wait(lock);
			}
			else
			{
				object = MemoryPoolQue.front();
				MemoryPoolQue.pop_front();

				strLog.Format("Acquire: %d", object);
				strLog = strLog + ":";

				for (deque<T>::iterator iter = MemoryPoolQue.begin(); iter != MemoryPoolQue.end(); iter++)
				{
					strTemp.Format(" %d", *iter);
					strLog = strLog + strTemp;
				}
				SaveLogFile(objectName, strLog);

				return object;
			}
		}

		//cv_pool.wait(lock, [&] {return !MemoryPoolQue.empty(); }); //predicate  lamda		
		//object = MemoryPoolQue.front();
		//MemoryPoolQue.pop();

	}

	void Release(T object)
	{
		lock_guard<mutex> lock(mutexMemoryPool);		

		BOOL isExit = FALSE;

		for (deque<T>::iterator iter = MemoryPoolQue.begin(); iter != MemoryPoolQue.end(); iter++)
		{
			if (object == *iter)
			{
				isExit = TRUE;
				break;
			}
		}

		if (isExit == FALSE)
		{
			MemoryPoolQue.push_back(object);

			strLog.Format("Release: %d", object);
			strLog = strLog + ":";
						 
			for (deque<T>::iterator iter = MemoryPoolQue.begin(); iter != MemoryPoolQue.end(); iter++)
			{
				strTemp.Format(" %d", *iter);
				strLog = strLog + strTemp;
			}
			SaveLogFile(objectName, strLog);

			// notify a random thread 확인 필요
			cv_pool.notify_one();
		}
		
	}

	void CreateMemoryPool(int size)
	{
		lock_guard<mutex> lock(mutexMemoryPool);

		for (int i = 0; i < size; i++)
		{
			T object;
			MemoryPoolQue.push_back(object);
			MemoryTotalCount++;		

		}
	}

	void AddToMemoryPool(T object)
	{
		lock_guard<mutex> lock(mutexMemoryPool);

		MemoryPoolQue.push_back(object);
		MemoryTotalCount++;

		strLog.Format("Add: %d", object);
		strLog = strLog + ":";

		for (deque<T>::iterator iter = MemoryPoolQue.begin(); iter != MemoryPoolQue.end(); iter++)
		{
			strTemp.Format(" %d", *iter);
			strLog = strLog + strTemp;
		}

		SaveLogFile(objectName, strLog);

	}

private:

};
#pragma once


// 올바른 사용법

//https://www.modernescpp.com/index.php/c-core-guidelines-be-aware-of-the-traps-of-condition-variables
//// conditionVariables.cpp
//
//#include <condition_variable> 
//#include <iostream> 
//#include <thread>
//
//std::뮤텍스 뮤텍스 _;
//std::condition_variable condVar;
//
//bool dataReady{ false };
//
//void  waitingForWork() {
//	std::cout << "대기 중" << std::endl;
//	std::unique_lock < std::mutex > lck(mutex_);
//	condVar.wait(lck, [] { return dataReady; });   // (4) 
//	std::cout << "실행 중" << std::endl;
//}
//
//무효  setDataReady() {
//	{
//		std::lock_guard < std::mutex > lck(mutex_);
//		dataReady = true;
//	}
//	std::cout << "데이터 준비 됨" << std::endl;
//	condVar.notify_one();                        // (삼)
//}
//
//int  main() {
//
//	std::cout << std::endl;
//
//	std::스레드 t1(waitingForWork);               // (1) 
//	std::스레드 t2(setDataReady);                 // (2)
//
//	t1.join();
//	t2.join();
//
//	std::cout << std::endl;
//
//}