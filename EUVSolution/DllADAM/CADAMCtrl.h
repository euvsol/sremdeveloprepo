/**
 * ADAM(Advanced Data Aquisition Module) Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 * ADAM header file define by YDKIM form 2019.07.11
 *
 **/
#pragma once
#define NUMBEROFPACKET						1000
#define HEADERSIZE							4	

#define PACKETSIZE			124	
#define ADAM_RECEIVE_BUFFER_SIZE NUMBEROFPACKET * PACKETSIZE + HEADERSIZE


#define MAX_POINT_MEASURE_MEMORY_NUMBER	5 			// 스캔 횟수별로 저장하기 위한 메모리 공간 확모  
#define MAX_SCAN_MEMORY_NUMBER	8 			// 스캔 횟수별로 저장하기 위한 메모리 공간 확모  
#define MAX_SCAN_NUMBER			100 		// 스캔 횟수별 최대 쓰레드 ==> 쓰레드 pool 개념으로 변경 필요.. 
#define ACQUISITION_TIME_OUT_MS 6000000     //테스트로 크게 잡았음(ihlee) ==> 500000 (8.3분에서..100분으로 증가시킴)



#define KINDS_OF_ADAM_DATA 16
#define NUM_I0_DATA 5
#define KINDS_OF_DATA KINDS_OF_ADAM_DATA+NUM_I0_DATA	//I0_normalize 한 데이터, I0 필터링한것 추가

#define PACKET_NUMBER           0			// ADAM에서 날아오는 1 번째 DATA가 Packet number 값
#define LASERINTERFEROMETER_X	1			// ADAM에서 날아오는 2 번째 DATA가 Laser interferomter X 값
#define LASERINTERFEROMETER_Y	2			// ADAM에서 날아오는 4 번째 DATA가 Laser interferomter Y 값
#define DETECTOR_INTENSITY_1	3			// ADAM에서 날아오는 4 번째 DATA가 Detector Intensity 1 값
#define DETECTOR_INTENSITY_2	4			// ADAM에서 날아오는 5 번째 DATA가 Detector Intensity 1 값
#define DETECTOR_INTENSITY_3	5			// ADAM에서 날아오는 4 번째 DATA가 Detector Intensity 1 값
#define DETECTOR_INTENSITY_4	6			// ADAM에서 날아오는 5 번째 DATA가 Detector Intensity 1 값
#define CAP_ADAM_SENSOR_1		7			// adam에서 날아오는 6 번째 data가 Cap sensor 1 값
#define CAP_ADAM_SENSOR_2		8			// adam에서 날아오는 7 번째 data가 Cap sensor 2 값
#define CAP_ADAM_SENSOR_3		9			// adam에서 날아오는 8 번째 data가 Cap sensor 3 값
#define CAP_ADAM_SENSOR_4		10			// adam에서 날아오는 9 번째 data가 Cap sensor 4 값 
#define OPTIC_SENSOR_1			11			// adam에서 날아오는 11 번째 data가 Optic sensor 1 값
#define OPTIC_SENSOR_2			12			// adam에서 날아오는 12 번째 data가 Optic sensor 2 값
#define OPTIC_SENSOR_3			13			// adam에서 날아오는 13 번째 data가 Optic sensor 3 값
#define ZR_TYPE_I0_SENSOR		14			// adam에서 날아오는 14 번째 data가 Zr type I0 sensor 값
#define BS_TYPE_I0_SENSOR		15			// adam에서 날아오는 15 번째 data가 B/S type I0 sensor 값
 ///////// I0 Data
#define D_NOR_I0_FILTER		16			// I0 normalization 적용된값
#define I0 	    17			// I0 ORIGINAL, I0_DETECTOR_INTENSITY이거랑 같아야함
#define I0_FILTERED 		18			// I0 전후 데이터로 평균낸 값
#define I0_INDEX 	        19			// 전체 받은 packet에서 I0 포지션
#define D_NOR_I0 	        20			// 전체 받은 packet에서 I0 포지션




 // 새로운 정의 ihlee
#define KIND_OF_REGRIDDATA		6 
#define X_REGRID 0
#define Y_REGRID 1
#define D1_REGRID 2
#define D2_REGRID 3
#define D_NOR_I0_FILTER_REGRID 4
#define D_NOR_I0_REGRID 5

//#define I0_FILTER_WINDOW_SIZE 200   // 한쪽으로 I0_FILTER_WINDOW_SIZE, 총 윈도우 사이즈 2*I0_FILTER_WINDOW_SIZE+1
#define I0_DETECTOR_INTENSITY DETECTOR_INTENSITY_2        //DETECTOR_INTENSITY_2 //현재 I0 D2에 물려있음.

// ADAM 이미지 데이터 크기 정의
//#define	ADAM_MAX_WIDTH			1000 // 2 um, 4 nm Grid 고려
//#define	ADAM_MAX_HEIGHT			1000
//
//#define	ADAM_MAX_WIDTH_REGRID			3000   // 2 um, 1 nm Grid 
//#define	ADAM_MAX_HEIGHT_REGRID			3000
//
//#define ADAM_MAX_DATA		   ADAM_MAX_WIDTH*ADAM_MAX_HEIGHT
//#define ADAM_MAX_DATA_REGRID   ADAM_MAX_WIDTH_REGRID*ADAM_MAX_HEIGHT_REGRID
//#define ORIGINAL_DATA_MAX		   ADAM_MAX_DATA*MAX_SCAN_MEMORY_NUMBER * 8  // Ring 버퍼 개념으로 사용중

#define UNI_DIRECTION						1
#define BI_DIRECTION						2
#define ELITHO_DIRECTION					3

#define DISP_RAW_IMAGE						1
#define DISP_REGRID_IMAGE					2

#define IMAGEFILE_PATH							_T("D:\\EUVSolution\\ADAM")										// Image 파일 경로  --> D 드라이브로 수정_2020/04/10_KYD

#ifdef _DEBUG
#define _DEBUG_WAS_DEFINED 1
#undef _DEBUG
#endif

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy\arrayobject.h>
#include <numpy\npy_3kcompat.h>

#ifdef _DEBUG_WAS_DEFINED
#define _DEBUG
#endif


#include <thread>
#include <vector>
#include "CMemoryPool.h"
using namespace std;
using std::thread;




#define CAP_SENSOR1 1
#define CAP_SENSOR2 2
#define CAP_SENSOR3 3
#define CAP_SENSOR4 4


#define D3_MAX 40000000


class AFX_EXT_CLASS CADAMCtrl : public CEthernetCom, public CADAMAlgorithm
{
public:
	CADAMCtrl();
	~CADAMCtrl();

	virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);

	int SendData(int nSize, char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	int ReceivedData_UniD(char *lParam);	//NPacket에서 Packet을 잘라내서 이 함수로 보낸다. by smchoi
	int ReceivedData_BiD(char *lParam);		// Bi-direction 스캔 기능 추가
	int ReceivedData_Elitho(char *lParam);	

	int m_nFovDataCntold;
	int ReceivedData_Average(char *lParam);
	int ReceivedData_After(char *lParam, int num_packet);

	int ReceivedData_3umOnly(char *lParam);		// Bi-direction 스캔 기능 추가

	char** m_chReceivedbuf = NULL;		//NPacket에서 Packet을 잘라서 저장하는 버퍼 by smchoi	
	char* m_chReceivedPrebuf = NULL;
	/*
	* EUV 이미지 정보를 정의하는 변수 FOV and Scan grid
	*/
	//int m_nEuvImage_Old_Fov;	//nm
	int m_nEuvImage_Fov;		//nm
	int m_nEuvImage_ScanGrid;	//nm
	int m_nEuvImage_ScanNumber;

	int m_nEUVImage_InterpolationGrid;
	int m_nEUVImage_Detectorselection;

	/**
	* Description: Scan하는 FOV의 Pixel Width, Pixel Height
	*/
	int m_nRawImage_PixelWidth;
	int m_nRawImage_PixelHeight;
	/**
	* Description: Regrid 하는 FOV의 Pixel Height, Pixel Height
	*/
	int m_nReGridImage_PixelWidth;
	int m_nReGridImage_PixelHeight;

	/**
	* Description: ADAM에서 날아오는 Data 중 유효한 FOV 영역의 data만 저장하는 변수
	*/
	double** m_dRawImageForDisplay; // ICD2에서 Display용 ihlee	
	double* m_dRawImageDataFor1DRegrid;

	/**
	*  이미지만 Buffer 하기 위한 메모리 공간
	*/
	double** m_dReGridImageForDisplay; // ICD2에서 Display용 ihlee	

	double*** m_dAveragedImage_Data3D;
	double*** m_dFilteredImage_Data3D;


	double*** m_dRawImage_Data3D;
	double*** m_dRegridImage_Data3D;

	double* m_dI0_Original = NULL;
	double* m_dI0_Filtered = NULL;

	unsigned int m_nOriignaDataI0_Index = 0;


	/**
	* Description: Scan Stage정지상태에서 ADAM에서 날아오는 Raw Data를 Real Time으로 이용하기 위한 변수
	*/	
	double *m_dParsedData;

	/**
	* Laser Interferometer 값이 Uni-Direction data 획득 구간이면 TRUE, 그외 구간이면 FALSE
	*/
	BOOL m_bCanGetData_AtUniDirection;				//
	BOOL m_bCanGetData_AtYaxisDirectoin;


	// Bidirection 용으로 만들어 놓은 Falg			KYD_20200630

	BOOL m_bCanGetData_AtXaxisDirectoin;

	BOOL m_bIsScanDirectionForward;
	int m_nLineNumCnt;								/*라인마다 갯수를 정해놓고 해당 라인에서 Data가 count 되었는지 확인함*/

	int m_nFovDataCnt_Bi;

	BOOL m_bIsLineScanCompleted;
	BOOL m_bIsAcquisitionStart;							//
	int m_nReverseDataCnt;

	double pos_x_old;
	double pos_x_oldold;
	double pos_x_current;

	double pos_y_old;
	double pos_y_oldold;
	double pos_y_current;

	int m_nScanType;				// 1: Uni-Direction, 2: Bi-Direction

	

	/*
	* ADAM 데이터를 구조체로 정의함
	*/
	//추가로 필요한 항목을 삽입 해야함
	typedef struct ADAM_Data
	{
		unsigned long m_nPacketNum;				/** packet Number */
		double m_dX_position;
		double m_dY_position;
		double m_dDetector1;
		double m_dDetector2;
		double m_dDetector3;
		double m_dDetector4;
		double m_dCapsensor1;
		double m_dCapsensor2;
		double m_dCapsensor3;
		double m_dCapsensor4;
		double m_dOpticsensor1;
		double m_dOpticsensor2;
		double m_dOpticsensor3;
		double m_dI0_BS;
		double m_dI0_ZR;
		int m_nLIFStatus1;
		int m_nLIFStatus2;
		int m_nLIFStatus3;
		int m_nTemp1;
		int m_nTemp2;
		int m_nTemp3;
		int m_nTemp4;
		unsigned long m_nPacketNum_old;//통신 테스트용 추가 ihlee
		unsigned int I0_Index;
	} ADAM_Data;

	//#pragma pack(push, 1)
	//	typedef struct strADAM_Data {	
	//		unsigned long m_nPacketNum;				/** packet Number */
	//		double m_dX_position;
	//		double m_dY_position;
	//		double m_nDetector1;
	//		double m_nDetector2;
	//		double m_nDetector3;
	//		double m_nDetector4;
	//		double m_dCapsensor1;
	//		double m_dCapsensor2;
	//		double m_dCapsensor3;
	//		double m_dCapsensor4;		
	//		double m_dOpticsensor1;
	//		double m_dOpticsensor2;
	//		double m_dOpticsensor3;
	//		double m_dI0_ZR;
	//		double m_dI0_BS;
	//			
	//	} ADAM_DataNew;
	//#pragma pack(pop)
	//	ADAM_DataNew  AdamDataNew; //memcpy(&AdamDataNew, original, sizeof(ADAM_DataNew));

		/* 구조체를 상속받아 AdamData라는 명령으로 활용*/
	ADAM_Data AdamData;



	/* 심플 Run/Stop을 위한 명령어 선언*/
	int ADAMRunStart();
	int ADAMAbort();	


	//Overriding
	void virtual AdamUIControl(BOOL isEnable) = 0;
	
	void virtual CapSensorDataUiUpdate() = 0;
	void virtual CurrentScanNumUiUpdate() = 0;
	void virtual InitAdamDlgForScan() = 0;
	virtual int	RawImageFileSave() = 0;
	virtual int	RawImageFileSave3D(int scanNum) = 0;

	int Command_LIFReset();		//LIF Reset을 위해서 임의로작성한 함수, 향후에 크게 필요 없을듯
	int Command_ADAMSimpleRun();
	int Command_OpticSensorReset();

	int Command_ADAMStart();
	int Command_ADAMStop();


	/* Interpolation 조건 및  실행을 호출하기 위한 함수*/
	int Regrid(int scanBufIndex, int scanNum, int pointNum=0);
	int Averaging(int scanBufIndex, int scanNum, int totalScanNum, int pointMeasureDataIndex=0);

	int Filtering(int pointMeasureDataIndex = 0);

	//int GetImageCapSensorPos();
	//int GetCurrentLIFPosValue();

	/* ADAM data를 parsing하기 위한 함수 선언*/
	void AdamDataMemReset(int pointMeasureDataIndex=0, vector<int> vec_scanBufIndex = { });
	
	void Parsing_ADAM_memcpy(char *original);

	/* ADAM control을 위한 global variable 선언*/
	BOOL m_bIsScaningOn;
	BOOL m_bIsScaningOnPrevious;
	BOOL m_bIsFirstScan;


	BOOL m_bSaveEveryScanRawImage = FALSE;  //

	double m_dLaserX_1st;
	double m_dLaserY_1st;

	double m_dAcelDistance_X;		// 
	double m_dAcelDistance_Y;		// N 스캔 기능 구현을 위해 Y 방향의 
	double m_dScanLine_Y_sum;
	double m_dScanLine_Y_ave;

	// 결과 파일에 Cap sensor의 값을 넣기 위해서 삽입함
	//double m_dCurrentImagePosition_Cap1;
	//double m_dCurrentImagePosition_Cap2;
	//double m_dCurrentImagePosition_Cap3;
	//double m_dCurrentImagePosition_Cap4;

	double m_dCurrentXposition; //사용안하는 변수임 ihlee 2020.12.24
	double m_dCurrentYposition; //사용안하는 변수임 ihlee 2020.12.24

	int	m_nFovDataCnt;
	int m_nImageScanCnt;
	int m_nRemainPixelCnt;
	int m_nScanLIneDataCnt;

	int m_nInterpolationCnt;

	double m_I0Reference;
	int m_I0FilterWindowSize;
	double m_LowPassFilterSigma;

	// EUV PTR 개발코드 부분--------------------------------------------------
	double** m_dRawMeasure_Data_PTR;

	int m_nPRTAcqTime;

	int Command_ADAMStart_PTR();
	int Command_ADAMStop_PTR();


	//int Command_GetValueAfter();
	int Command_AverageRun();
	int Command_AverageRunTimeout();
	int Command_AverageRunAfterTimeout();
	int Command_SetAverageCount(unsigned int count);

	int m_nPTRDataNumADAM;


	HANDLE m_hAcquisitionEnd; // ihlee 21.01.16 SetEvent()어딘가에서 제거됨. ㅠㅠ
	//--------------------------------------------------------------------------

	//BigEndian
	//double CharToDouble(char* buf);
	unsigned long CharToUlong(char* buf);
	//unsigned short CharToUshort(char* buf);

	// Loop time 계산용 Global variable..... KYD
	time_t start, end;
	double ParsingTime;
	CString LoopTimeMsg;
	void tic();
	void toc();		// tic(); 을 호출한 다음 Toc();을 호출하면 시간까지 출력함


	/*5Khz ICD2 적용 위한 변경*/

	BOOL bSaveNormalizedImage = TRUE;

	virtual	int ReceiveData(char *lParam, int nLength);
	virtual int WaitReceiveEventThread();																						//

	//virtual int DisplayFilteredImage(double* image) = 0;
	virtual int DisplayFilteredImage(double* image);
	virtual int DisplayRawImage() = 0;

	int OnRegrid(int ScanNum);
	int OnAverage();
	int OnFilter();
	int OnWaitScanEnd();

	CWinThread* m_pRegridThreads[MAX_SCAN_NUMBER] = { NULL, };
	CWinThread* m_pAverageThread = NULL;
	CWinThread* m_pFilterThread = NULL;
	CWinThread*	m_pWaitScanEndThread = NULL;

	BOOL m_bAbort = FALSE;

	HANDLE			m_hAcquisitionEnds[MAX_SCAN_NUMBER] = { NULL, };
	HANDLE			m_hRegridEnds[MAX_SCAN_NUMBER] = { NULL, };
	HANDLE			m_hAverageEnd = NULL;
	HANDLE			m_hFilterEnd = NULL;

	HANDLE				m_hSendMutex;


	void InterpolationCubic1D(double *x, double *y, int length, double *x_out, double *y_out, int lenth_out);
	void InterpolationPchip1D(double *x, double *y, int length, double *x_out, double *y_out, int lenth_out);
	int previousLintCountInterpolation = 0;

	struct RegridThreadParam
	{
		CADAMCtrl* pAdam;
		int scanNum;
		void Set(CADAMCtrl* adam, int num)
		{
			pAdam = adam;
			scanNum = num;
		}
	};
	RegridThreadParam m_ThreadParam[MAX_SCAN_NUMBER];


	static	UINT __cdecl	RegridThread(LPVOID ThreadParam) { ((RegridThreadParam *)ThreadParam)->pAdam->OnRegrid(((RegridThreadParam *)ThreadParam)->scanNum); return 0; }
	static	UINT __cdecl	AverageThread(LPVOID pClass) { ((CADAMCtrl *)pClass)->OnAverage(); return 0; }
	static	UINT __cdecl	FilterThread(LPVOID pClass) { ((CADAMCtrl *)pClass)->OnFilter(); return 0; }
	static	UINT __cdecl	WaitScanEndThread(LPVOID pClass) { ((CADAMCtrl *)pClass)->OnWaitScanEnd(); return 0; }


	int ADAM_MAX_WIDTH = 1000;
	int ADAM_MAX_HEIGHT = 1000;
	int ADAM_MAX_WIDTH_REGRID = 3000;
	int ADAM_MAX_HEIGHT_REGRID = 3000;

	int ADAM_MAX_DATA = ADAM_MAX_WIDTH * ADAM_MAX_HEIGHT;
	int ADAM_MAX_DATA_REGRID = ADAM_MAX_WIDTH_REGRID * ADAM_MAX_HEIGHT_REGRID;
	int ORIGINAL_DATA_MAX = ADAM_MAX_DATA * MAX_SCAN_MEMORY_NUMBER * 8;

	void MemoryAllocation();

	BOOL m_bIsUseInterpolation = TRUE;
	BOOL m_bIsUseHighFovInterpolation = FALSE;
	BOOL m_bIsUseHighFovInterpolationOld = FALSE;
	int m_HighFovCriteria = 4000; //4000um 이상



protected:
	PyObject* m_pyName, *m_pyModule, *m_pyFuncReconstructImage, *m_pyFuncReconstructImageCubic, *m_pyFuncReconstructImageRemoveNan, *m_pyFuncFilterImage;

	BOOL m_bReceiveBytesReset = TRUE;					// 수신 버퍼 Packet 초기화 (Adam에 packet 분할 문제 대응용, ihlee)
	BOOL m_bReceivedBytesSizeTotalUpdated = FALSE;

	int	m_nReceivedBytesSizeCurrent = 0;	        // 현재까지 수신된  Packetsize
	int m_nReceivedBytesSizeTotal = 0;	            // 수신 될 PacketSize	


	HANDLE			m_hDisplayRawImage = NULL;
	BOOL m_bAverageRunAfter = FALSE;
	HANDLE m_hAverageRun;

public:
	MemoryPool<int> MemoryPoolScan;
	MemoryPool<int> MemoryPoolPointMeasure;

	void MemoryPoolInitialization();


	//void MemoryAcquire(int scanNum, int** PointMeasureData, vector<int*>* ScanData);
	void MemoryAcquireAndInitForPointMeasure(int totalScanNum, int &pointMeasureData, vector<int> &scanData);

	
	void RegridThreadFun(int scanBufIndex, int pointNum = 0, int scanNum = 0, int througFocusNum = 0);
	void PointMeasureThreadFun(int pointMeasureDataIndex, vector<int> scanBufIndex, int pointNum = 0, int totalPointNum = 1, int totalScanNum = 1, int througFocusNum=0, int totalThroughFocusNum=1, BOOL isSaveFile =FALSE);

	
	void RegridThreadFunForTest(int scanBufIndex, int pointNum = 0, int scanNum = 0, int througFocusNum = 0);
	void PointMeasureThreadFunForTest(int pointMeasureDataIndex, vector<int> scanBufIndex, int pointNum = 0, int totalPointNum = 1, int totalScanNum = 1, int througFocusNum = 0, int totalThroughFocusNum = 1);

	void ScanDataReceiveThreadFunForTest(int totalScanNum, vector <int> scanBufIndex, int pointNum = 0, int througFocusNum = 0);//Test 용	

	// Test용 최대 스캔 6회, 최대 Measure 3회 중첩	
	int MaxScanMemory = MAX_SCAN_MEMORY_NUMBER;
	int MaxPointMeasureMemory = MAX_POINT_MEASURE_MEMORY_NUMBER;

	deque <condition_variable> cvScanDataReady;
	deque <mutex> mutexScanDataReady;
	deque <BOOL> boolScanDataReady;

	BOOL m_IsUse3umFOV = TRUE;
	
	BOOL m_IsMeasureComplete;


	vector <int> m_ScanBufIndexForReceiveData;

	void SetScanBufIndexForReceiveData(vector <int> _ScanBufIndexForReceiveData);

	void FilteredImageFileSave_Resize(int removeEdge, int nDefectNo, double **ImageBuffer, double capPos, int capNum =0,int throughFocusNum = 0, double focusPos =0);

	int m_currentPointNum,  m_currentThroughFocusNum; //currentScanNum
	
	int m_cuurentPointMeasureBufIndex;
	vector <int> m_vecScanBufIndex;

	void SetPointMeasureBufIndex(int PointMeasureBufIndex);
	void SetScanBufIndexVector(vector <int> scanBufIndexVector);


	// Adam Start
	condition_variable cvAdamStart;
	mutex mutexAdamStart;
	BOOL IsAdamStart;

	//ScanEnd
	condition_variable cvAdamEnd;
	mutex mutexAdamEnd;
	BOOL IsAdamEnd;

	BOOL threadEnd = FALSE;

	//	ELitho	
	int ADAMRunStartForElitho();
	int ADAMAbortForElitho();

	double  *D3Data;
	int D3Length;
	double D3Average = 0;

	BOOL m_IsResetStartPos = TRUE;

};


