#pragma once

#include "stdafx.h"
#include "CADAMCtrl.h"
#include "math.h"
#include <time.h>
#include <windows.h>
#include "CADAMInterpolationAlgorithm.h"
//#include "CADAMAlgorithm.h"

using namespace std;

#pragma comment(lib, "winmm.lib") 

/*
 ** ADAM 통산 관련 주요 내용 수정... 2019.07.15. by YDK
*/

// ADAM control 생성자 선언
// 왜서 최대 이미지 갯수만큼 추가 메모리 버퍼를 생성했지?

CADAMCtrl::CADAMCtrl()
{
	m_nRawImage_PixelWidth = 300;
	m_nRawImage_PixelHeight = 300;

	m_nReGridImage_PixelWidth = 1000;
	m_nReGridImage_PixelHeight = 1000;

	m_strReceiveEndOfStreamSymbol = _T("\r");  // Packet에 \r 이 있어야 한 패킷으로 인식함

	// ADAM Data Initialize
	//AdamData.m_chPacketStart = "KYD";
	AdamData.m_nPacketNum = 0;
	AdamData.m_dX_position = 0.0;
	AdamData.m_dY_position = 0.0;
	AdamData.m_dDetector1 = 0.0;
	AdamData.m_dDetector2 = 0.0;
	AdamData.m_dCapsensor1 = 0.0;
	AdamData.m_dCapsensor2 = 0.0;
	AdamData.m_dCapsensor3 = 0.0;
	AdamData.m_dCapsensor4 = 0.0;
	AdamData.m_dOpticsensor1 = 0.0;
	AdamData.m_dOpticsensor2 = 0.0;
	AdamData.m_dOpticsensor3 = 0.0;
	AdamData.m_dI0_BS = 0.0;
	AdamData.m_dI0_ZR = 0.0;
	AdamData.m_nLIFStatus1 = 0;
	AdamData.m_nLIFStatus2 = 0;
	AdamData.m_nLIFStatus3 = 0;
	AdamData.m_nTemp1 = 0;
	AdamData.m_nTemp2 = 0;
	AdamData.m_nTemp3 = 0;
	AdamData.m_nTemp4 = 0;
	//AdamData.m_chPacketEnd = "DYK";

	// Data Initialize
	m_dLaserX_1st = 0.0f;
	m_dLaserY_1st = 0.0f;
	m_dAcelDistance_X = 0.0f;
	m_dAcelDistance_Y = 0.0f;
	m_dScanLine_Y_sum = 0.0f;
	m_dScanLine_Y_ave = 0.0f;
	m_nFovDataCnt = 0;
	m_nImageScanCnt = 0;
	m_nRemainPixelCnt = 0;
	m_nScanLIneDataCnt = 0;

	m_nInterpolationCnt = 0;

	//m_dCurrentImagePosition_Cap1 = 0.0;
	//m_dCurrentImagePosition_Cap2 = 0.0;
	//m_dCurrentImagePosition_Cap3 = 0.0;
	//m_dCurrentImagePosition_Cap4 = 0.0;

	m_dCurrentXposition = 0.0;
	m_dCurrentYposition = 0.0;

	// Bi-direction
	m_nScanType = UNI_DIRECTION;			// Uni-direction으로 초기화

	m_nReverseDataCnt = 0;
	m_nFovDataCnt_Bi = 0;		// Bi-direction 용 Fov data count

	pos_x_current = 0;			// Bi-diriction 방향 확인 용
	pos_x_old = 0;
	pos_x_oldold = 0;
	pos_y_current = 0;
	pos_y_old = 0;
	pos_y_oldold = 0;

	// PTR
	m_nPTRDataNumADAM = 1000;

	// 기본동작에 필요한 변수 생성
	m_bCanGetData_AtUniDirection = FALSE;									// X Scan
	m_bCanGetData_AtYaxisDirectoin = FALSE;

	// Bi direction 용으로 변수 생성										// Reverse derection 포함
	m_bIsScanDirectionForward = FALSE;
	m_nLineNumCnt = 0;														// 생성자 초기화
	m_bIsLineScanCompleted = FALSE;

	m_bCanGetData_AtXaxisDirectoin = FALSE;				// bidirection용 코드_KYD

	m_bIsAcquisitionStart = FALSE;											// Bi-direction scan 시 Data를 획득할 것인지 판단하는 Flag

	m_bIsScaningOn = FALSE;
	m_bIsScaningOnPrevious = TRUE;  //프로 그램 시작시 Cap read 지연을 위해 TRUE로 설정, 동작안함

	m_bIsFirstScan = FALSE;

	// Python 초기화
	PythonInitailize();//이거 한번만 실행해도 되는지 테스트 필요...
	m_pyName = PyUnicode_FromString("interpolation_try");
	m_pyModule = PyImport_Import(m_pyName);
	m_pyFuncReconstructImage = PyObject_GetAttrString(m_pyModule, "reconstruct_image");
	m_pyFuncReconstructImageCubic = PyObject_GetAttrString(m_pyModule, "reconstruct_image_cubic");
	m_pyFuncReconstructImageRemoveNan = PyObject_GetAttrString(m_pyModule, "reconstruct_image_remove_nan");
	m_pyFuncFilterImage = PyObject_GetAttrString(m_pyModule, "filter_image");
	
	// 5Khz ICD2 Thread 관련
	//m_pAcqusitionThreads = NULL;
	//m_pAcqusitionThreads.reserve(MAX_SCAN_NUMBER);

	m_hAcquisitionEnd = CreateEvent(NULL, TRUE, FALSE, NULL);

	for (int i = 0; i < MAX_SCAN_NUMBER; i++)
	{
		m_hAcquisitionEnds[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
		m_hRegridEnds[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
		m_ThreadParam[i].Set(this, i);
	}

	m_hAverageEnd = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hFilterEnd = CreateEvent(NULL, TRUE, FALSE, NULL);

	m_hDisplayRawImage = CreateEvent(NULL, FALSE, FALSE, NULL);//자동리셋
	//ResetEvent(m_hDisplayRawImage);

	m_hSendMutex = CreateMutex(NULL, FALSE, "");

	m_hAverageRun = CreateEvent(NULL, TRUE, FALSE, NULL);

}

CADAMCtrl::~CADAMCtrl()
{
	int i = 0;
	if (m_dRawImageForDisplay != NULL)
	{
		for (i = 0; i < KINDS_OF_DATA; i++)
			delete[] m_dRawImageForDisplay[i];

		delete[] m_dRawImageForDisplay;
	}

	if (m_dReGridImageForDisplay != NULL)
	{
		for (i = 0; i < KIND_OF_REGRIDDATA; i++)
			delete[] m_dReGridImageForDisplay[i];

		delete[] m_dReGridImageForDisplay;
	}

	if (m_dRawImageDataFor1DRegrid != NULL)
	{
		delete[] m_dRawImageDataFor1DRegrid;
	}

	//----------------------------------------------- PTR
	if (m_dRawMeasure_Data_PTR != NULL)
	{
		for (i = 0; i < KINDS_OF_DATA; i++)
			delete[] m_dRawMeasure_Data_PTR[i];

		delete[] m_dRawMeasure_Data_PTR;
	}
	//------------------------------------------------


	// Reconstructed data 소멸자..... 해당 변수를 어디다기 넣을 지는 추가로 고민이 필요함 KYD_20200119
	if (m_dAveragedImage_Data3D != NULL)
	{
		for (int i = 0; i < MAX_POINT_MEASURE_MEMORY_NUMBER; i++)
		{
			for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
			{
				delete[] m_dAveragedImage_Data3D[i][j];
			}

			delete[] m_dAveragedImage_Data3D[i];
		}
		delete[] m_dAveragedImage_Data3D;
	}

	if (m_dFilteredImage_Data3D != NULL)
	{
		for (int i = 0; i < MAX_POINT_MEASURE_MEMORY_NUMBER; i++)
		{
			for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
			{
				delete[] m_dFilteredImage_Data3D[i][j];
			}

			delete[] m_dFilteredImage_Data3D[i];
		}
		delete[] m_dFilteredImage_Data3D;
	}


	if (m_dRawImage_Data3D != NULL)
	{
		for (int i = 0; i < MAX_SCAN_MEMORY_NUMBER; i++)
		{
			for (int j = 0; j < KINDS_OF_DATA; j++)
			{
				delete[] m_dRawImage_Data3D[i][j];
			}

			delete[] m_dRawImage_Data3D[i];
		}

		delete[] m_dRawImage_Data3D;
	}

	if (m_dI0_Original != NULL)
	{
		delete[] m_dI0_Original;
	}

	if (m_dI0_Filtered != NULL)
	{
		delete[] m_dI0_Filtered;
	}

	//if (m_dRawImage_Data_forRegrid3D != NULL)
	//{
	//	for (int i = 0; i < MAX_SCAN_MEMORY_NUMBER; i++)
	//	{
	//		for (int j = 0; j < KINDS_OF_DATA; j++)
	//		{
	//			delete[] m_dRawImage_Data_forRegrid3D[i][j];
	//		}

	//		delete[] m_dRawImage_Data_forRegrid3D[i];
	//	}

	//	delete[] m_dRawImage_Data_forRegrid3D;
	//}



	if (m_dRegridImage_Data3D != NULL)
	{
		for (int i = 0; i < MAX_SCAN_MEMORY_NUMBER; i++)
		{
			for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
			{
				delete[] m_dRegridImage_Data3D[i][j];
			}

			delete[] m_dRegridImage_Data3D[i];
		}
		delete[] m_dRegridImage_Data3D;
	}

	//if (m_dRawADAMData != NULL)
	//{
	//	delete[] m_dRawADAMData;
	//}

	//if (m_chReceivebuf != NULL)
	//{
	//	for (i = 0; i < KINDS_OF_ADAMDATA; i++)
	//		delete[] m_chReceivebuf[i];

	//	delete[] m_chReceivebuf;
	//}

	delete[] m_dParsedData;

	if (m_chReceivedbuf != NULL)
	{
		for (i = 0; i < NUMBEROFPACKET; i++)
			delete[] m_chReceivedbuf[i];

		delete[] m_chReceivedbuf;
	}

	if (m_chReceivedPrebuf != NULL)
	{
		delete[] m_chReceivedPrebuf;
	}



	// For Elitho
	delete[] D3Data;

	//if (m_dtempImage_Data_EdgeFind != NULL)
	//{
	//	delete[] m_dtempImage_Data_EdgeFind;
	//}

	for (int i = 0; i < MAX_SCAN_NUMBER; i++)
	{
		CloseHandle(m_hAcquisitionEnds[i]);
		CloseHandle(m_hRegridEnds[i]);
	}
	CloseHandle(m_hAverageEnd);
	CloseHandle(m_hFilterEnd);
	CloseHandle(m_hSendMutex);

	CloseHandle(m_hDisplayRawImage);

	CloseHandle(m_hAverageRun);


	Py_XDECREF(m_pyFuncReconstructImage);
	Py_XDECREF(m_pyFuncReconstructImageCubic);
	Py_XDECREF(m_pyFuncReconstructImageRemoveNan);
	Py_XDECREF(m_pyFuncFilterImage);		
	Py_DECREF(m_pyModule);
	Py_DECREF(m_pyName);
}



void CADAMCtrl::MemoryAllocation()
{

	ADAM_MAX_DATA = ADAM_MAX_WIDTH * ADAM_MAX_HEIGHT;
	ADAM_MAX_DATA_REGRID = ADAM_MAX_WIDTH_REGRID * ADAM_MAX_HEIGHT_REGRID;
	ORIGINAL_DATA_MAX = ADAM_MAX_DATA * MAX_SCAN_MEMORY_NUMBER * 8;

	// 이중포인터로 2차원 배열을 생성하기 위한 생성자 구현		// 데이터 종류 X 최대 픽셀갯수 의 2차원 버퍼
	m_dRawImageForDisplay = new double *[KINDS_OF_DATA];								// m_dRawImage_Data 의 메모리 버퍼를 만들어서 데이터를 획득함(ADAM 이미지 데이터 갯수만큼 버퍼 생성
	for (int i = 0; i < KINDS_OF_DATA; i++)
	{
		m_dRawImageForDisplay[i] = new double[ADAM_MAX_DATA];							// m_dRawImage_Data[i]에 ADAM 에서 전송되는 image data 한 개의 패킷에 저장된 데이터들이 순서대로 기록되도록 메모리 버퍼를 생성함. 생성하는 버퍼는 22개
	}

	m_dRawImageDataFor1DRegrid = new double[ADAM_MAX_DATA];

	m_dReGridImageForDisplay = new double *[KIND_OF_REGRIDDATA];								// m_dRawImage_Data 의 메모리 버퍼를 만들어서 데이터를 획득함(ADAM 이미지 데이터 갯수만큼 버퍼 생성
	for (int i = 0; i < KIND_OF_REGRIDDATA; i++)
	{
		m_dReGridImageForDisplay[i] = new double[ADAM_MAX_DATA_REGRID];							// m_dRawImage_Data[i]에 ADAM 에서 전송되는 image data 한 개의 패킷에 저장된 데이터들이 순서대로 기록되도록 메모리 버퍼를 생성함. 생성하는 버퍼는 22개
	}


	// PTR용 메모리 저장 버퍼
	//-----------------------------------------
	m_dRawMeasure_Data_PTR = new double *[KINDS_OF_DATA];								// m_dRawImage_Data 의 메모리 버퍼를 만들어서 데이터를 획득함(ADAM 이미지 데이터 갯수만큼 버퍼 생성
	for (int i = 0; i < KINDS_OF_DATA; i++)
	{
		m_dRawMeasure_Data_PTR[i] = new double[ADAM_MAX_DATA];							// m_dRawImage_Data[i]에 ADAM 에서 전송되는 image data 한 개의 패킷에 저장된 데이터들이 순서대로 기록되도록 메모리 버퍼를 생성함. 생성하는 버퍼는 22개
	}
	//-------------------------------------------

	m_dI0_Original = new double[ORIGINAL_DATA_MAX];
	m_dI0_Filtered = new double[ORIGINAL_DATA_MAX];

	m_dRawImage_Data3D = new double**[MAX_SCAN_MEMORY_NUMBER];
	for (int i = 0; i < MAX_SCAN_MEMORY_NUMBER; i++)
	{
		m_dRawImage_Data3D[i] = new double*[KINDS_OF_DATA];

		for (int j = 0; j < KINDS_OF_DATA; j++)
		{
			m_dRawImage_Data3D[i][j] = new double[ADAM_MAX_DATA];
		}
	}

	m_dRegridImage_Data3D = new double**[MAX_SCAN_MEMORY_NUMBER]; //메모리 소요 많음 18GBYTE 정도
	for (int i = 0; i < MAX_SCAN_MEMORY_NUMBER; i++)
	{
		m_dRegridImage_Data3D[i] = new double*[KIND_OF_REGRIDDATA];

		for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
		{
			m_dRegridImage_Data3D[i][j] = new double[ADAM_MAX_DATA_REGRID];
			memset(m_dRegridImage_Data3D[i][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
		}
	}


	m_dAveragedImage_Data3D = new double**[MAX_POINT_MEASURE_MEMORY_NUMBER]; //메모리 소요 많음 18GBYTE 정도
	for (int i = 0; i < MAX_POINT_MEASURE_MEMORY_NUMBER; i++)
	{
		m_dAveragedImage_Data3D[i] = new double*[KIND_OF_REGRIDDATA];

		for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
		{
			m_dAveragedImage_Data3D[i][j] = new double[ADAM_MAX_DATA_REGRID];
			memset(m_dAveragedImage_Data3D[i][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
		}
	}

	m_dFilteredImage_Data3D = new double**[MAX_POINT_MEASURE_MEMORY_NUMBER]; //메모리 소요 많음 18GBYTE 정도
	for (int i = 0; i < MAX_POINT_MEASURE_MEMORY_NUMBER; i++)
	{
		m_dFilteredImage_Data3D[i] = new double*[KIND_OF_REGRIDDATA];

		for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
		{
			m_dFilteredImage_Data3D[i][j] = new double[ADAM_MAX_DATA_REGRID];
			memset(m_dFilteredImage_Data3D[i][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
		}
	}

	//--------------------------------------------------

	m_dParsedData = new double[KINDS_OF_ADAM_DATA];                              //-NUM_AUGMENTED_DATA?

	m_chReceivedbuf = new char *[NUMBEROFPACKET];								// N Packet을 붙여서 보내는 경우 이버퍼로 잘라낸다. by smchoi
	for (int i = 0; i < NUMBEROFPACKET; i++)
	{
		m_chReceivedbuf[i] = new char[PACKETSIZE];
	}

	m_chReceivedPrebuf = new char[ADAM_RECEIVE_BUFFER_SIZE];

	//----------------------------------------------------
	//int i = 0;
	for (int i = 0; i < KINDS_OF_DATA; i++)
		memset(m_dRawImageForDisplay[i], 0, sizeof(double) * ADAM_MAX_DATA);  // std::fill() and std::fill_n()

	memset(m_dRawImageDataFor1DRegrid, 0, sizeof(double) * ADAM_MAX_DATA);  // std::fill() and std::fill_n()	

	MemoryPoolInitialization();


	// For Elitho

	D3Data = new double[D3_MAX];

}
// 로그 작성용 변경 내용은 추가로 확인이 필요함 KYD 20191203 ICD 1에서만 사용
int	CADAMCtrl::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	char logsend[60];
	char *t1 = "ADAM command", *t3 = "transfer", *t2;
	CString test;

	ResetEvent(m_hStateOkEvent);	//Sets the specified event object to the nonsignaled state.

	m_strSendMsg = lParam;
	t2 = lParam;

	nRet = Send(lParam, nTimeOut);

	nRet = WaitEvent(m_hStateOkEvent, nTimeOut);

	sprintf(logsend, "%s%s%s", t1, t2, t3); // 빈 배열 test 에 여러 문자열 집어넣기
	test = logsend;

	if (nRet != 0)
		m_strSendMsg = _T("ADAM Run command transfer");

	SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("PC -> ADAM : " + m_strSendMsg + "Transfered")));	//통신 상태 기록.
	SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("PC -> ADAM : " + test + "test2")));	//통신 상태 기록.

	return nRet;
}



int	CADAMCtrl::SendData(int nSize, char *lParam, int nTimeOut, int nRetryCnt) // ICD2 로그저장  수정해야함 ihlee
{
	int nRet = 0;
	char logsend[60];
	char *t1 = "ADAM command", *t3 = "transfer", *t2;
	CString test;

	MutexLock(m_hSendMutex);


	// Receive 초기화
	m_bReceiveBytesReset = TRUE; // Receive Packet 초기화 ihlee

	if (nTimeOut > 0)
	{
		ResetEvent(m_hStateOkEvent);	//Sets the specified event object to the nonsignaled state.
	}

	m_strSendMsg = lParam;
	t2 = lParam;

	nRet = Send(nSize, lParam, nTimeOut, nRetryCnt);

	if (nTimeOut > 0)
	{
		nRet = WaitEvent(m_hStateOkEvent, nTimeOut);
	}

	sprintf(logsend, "%s%s%s", t1, t2, t3); // 빈 배열 test 에 여러 문자열 집어넣기
	test = logsend;

	if (nRet != 0)
		m_strSendMsg = _T("ADAM Run command transfer");

	SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("PC -> ADAM : " + m_strSendMsg + "Transfered")));	//통신 상태 기록.
	SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("PC -> ADAM : " + test + "test2")));	//통신 상태 기록.

	MutexUnlock(m_hSendMutex);

	return nRet;
}

int CADAMCtrl::Command_ADAMSimpleRun()
{
	int nRet = 0;


	Command_ADAMStart();
	Command_ADAMStop();

	WaitSec(0.1); //Data update되는 시간 기다림 ihlee

	//CapSensorDataUiUpdate();

	//Command_GetValueBefore();

	//char chBuf[10];
	//// Simple Run 버튼하나로 Start/Stop 구현하기 위해 Flag를 삽입함
	//if (m_blsSimpleRun == FALSE)
	//{
	//	m_blsSimpleRun = TRUE;
	//	command.m_bADAMRun = TRUE;
	//	command.m_bADAMSimpleRun = TRUE;
	//	command.m_bInterferometerReset = FALSE;
	//	command.m_bOpticSensorReset = FALSE;
	//}
	//else
	//{
	//	m_blsSimpleRun = FALSE;
	//	command.m_bADAMRun = FALSE;
	//	command.m_bADAMSimpleRun = FALSE;
	//	command.m_bInterferometerReset = FALSE;
	//	command.m_bOpticSensorReset = FALSE;
	//}

	//sprintf(chBuf, "%d%d%d%d", command.m_bADAMRun, command.m_bADAMSimpleRun, command.m_bInterferometerReset, command.m_bOpticSensorReset);
	////AfxMessageBox(chBuf); simple run 잘 작동하는지 확인하기 위해 작성한 코드
	//if (m_bConnected)
	//	nRet = SendData(chBuf);

	return nRet;
}

// Laser interferometer reset을 위해서 테스트로 만든 코드
int CADAMCtrl::Command_LIFReset()
{
	int nRet = 0;
	char chBuf[10];

	chBuf[0] = 0x01;
	chBuf[1] = 0x00;
	chBuf[2] = 0x01;
	chBuf[3] = 0x21;
	chBuf[4] = 0x01;

	nRet = SendData(5, chBuf);

	return nRet;
}

// Optical sensor resot
int CADAMCtrl::Command_OpticSensorReset()
{
	int nRet = 0;
	char chBuf[10];

	// Optic sensor reset 기능 구현을 위해서 제작함
	chBuf[0] = 0x01;
	chBuf[1] = 0x00;
	chBuf[2] = 0x01;
	chBuf[3] = 0x22;
	chBuf[4] = 0x01;

	nRet = SendData(5, chBuf);

	return nRet;
}

int CADAMCtrl::Regrid(int scanBufIndex, int scanNum, int pointNum)
{
	int nRet = 0;
	int image_size_x, image_size_y, num_scans;

	double FOV = m_nEuvImage_Fov;// *1000;											//field of view
	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step

	int output_size = int(FOV / grid_step);										//int output_size = 1000;			// Interpolation 후에 만들어질 픽셀의 갯수

	//num_scans = m_nEuvImage_ScanNumber;										// Interpolation algorithm으로 들어가는 scan number는 항상 1로 하고, Thread로 계속 ㅇ라고리즘을 불러 들임
	num_scans = 1;											//num_scans = 1;
	image_size_x = image_size_y = int(FOV / m_nEuvImage_ScanGrid);				//image_size_x = image_size_y = 100;	//계산속도 확인을 위해 임의로 기록, 2 um 20 nm 라서 100


	///////// Regrid 전에 I0 normalization 수행 //////////////////////
	int maxIndex, minIndex;
	int w = m_I0FilterWindowSize / 2;


	//int ScanNum = ScanNum % MAX_SCAN_MEMORY_NUMBER;


	//Bi direction은 카운트 교차함
	int temp1 = (int)m_dRawImage_Data3D[scanBufIndex][I0_INDEX][0];
	int temp2 = (int)m_dRawImage_Data3D[scanBufIndex][I0_INDEX][m_nRawImage_PixelWidth - 1];
	minIndex = (temp1 < temp2) ? temp1 : temp2;


	temp1 = (int)m_dRawImage_Data3D[scanBufIndex][I0_INDEX][m_nRawImage_PixelWidth * m_nRawImage_PixelHeight - m_nRawImage_PixelWidth];
	temp2 = (int)m_dRawImage_Data3D[scanBufIndex][I0_INDEX][m_nRawImage_PixelWidth * m_nRawImage_PixelHeight - 1];
	maxIndex = (temp1 > temp2) ? temp1 : temp2;


	////// 초기값 계산, 	
	double sum = 0;
	for (int i = minIndex - w; i <= minIndex + w; i++)
	{
		int j = (i < 0) ? 0 : i;
		sum = sum + m_dI0_Original[j];
	}
	m_dI0_Filtered[minIndex] = sum / (2 * w + 1);

	////// 점화식으로 이후 값 계산	
	for (int k = minIndex + 1; k <= maxIndex; k++)
	{
		int m = (k - w - 1 < 0) ? 0 : k - w - 1;
		int n = (k + w > maxIndex) ? maxIndex : k + w;

		m_dI0_Filtered[k] = m_dI0_Filtered[k - 1] + (m_dI0_Original[n] - m_dI0_Original[m]) / (2 * w + 1);
	}

	// Buffer update
	int index;
	double I0_reference = m_I0Reference;

	for (int i = 0; i < m_nRawImage_PixelWidth * m_nRawImage_PixelHeight; i++)
	{
		index = (int)m_dRawImage_Data3D[scanBufIndex][I0_INDEX][i];
		m_dRawImage_Data3D[scanBufIndex][I0_FILTERED][i] = m_dI0_Filtered[index];
		m_dRawImage_Data3D[scanBufIndex][I0][i] = m_dI0_Original[index];

		m_dRawImage_Data3D[scanBufIndex][D_NOR_I0][i] = m_dRawImage_Data3D[scanBufIndex][DETECTOR_INTENSITY_1][i] * I0_reference / m_dRawImage_Data3D[scanBufIndex][I0][i];
		m_dRawImage_Data3D[scanBufIndex][D_NOR_I0_FILTER][i] = m_dRawImage_Data3D[scanBufIndex][DETECTOR_INTENSITY_1][i] * I0_reference / m_dRawImage_Data3D[scanBufIndex][I0_FILTERED][i];

		//Display 버퍼
		m_dRawImageForDisplay[D_NOR_I0][i] = m_dRawImage_Data3D[scanBufIndex][D_NOR_I0][i];
		m_dRawImageForDisplay[I0][i] = m_dRawImage_Data3D[scanBufIndex][I0][i];
		m_dRawImageForDisplay[D_NOR_I0_FILTER][i] = m_dRawImage_Data3D[scanBufIndex][D_NOR_I0_FILTER][i];
		m_dRawImageForDisplay[I0_FILTERED][i] = m_dRawImage_Data3D[scanBufIndex][I0_FILTERED][i];

	}


	//std::chrono::duration
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	try
	{
		//chrono::time_point<chrono::high_resolution_clock> start = chrono::high_resolution_clock::now();
		//chrono::time_point<chrono::high_resolution_clock> end = chrono::high_resolution_clock::now();
		//chrono::milliseconds duration2 = chrono::duration_cast<chrono::milliseconds>(end - start);

		if (m_nEuvImage_Fov == 1000)
		{
			GriddataNew(m_pyFuncReconstructImage, this->m_dRawImage_Data3D[scanBufIndex][DETECTOR_INTENSITY_1], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
				, this->m_dRegridImage_Data3D[scanBufIndex][D1_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 180.0, 160.0, FOV);


			//GriddataRemoveNan(m_pyFuncReconstructImageRemoveNan, this->m_dRawImage_Data3D[scanBufIndex][DETECTOR_INTENSITY_1], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
			//	, this->m_dRegridImage_Data3D[scanBufIndex][D1_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 180.0, 160.0, FOV);


			//GriddataNew(m_pyFuncReconstructImage, this->m_dRawImage_Data3D[scanBufIndex][D_NOR_I0], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
			//	, this->m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 180.0, 160.0, FOV);

			//GriddataNew(m_pyFuncReconstructImage, this->m_dRawImage_Data3D[scanBufIndex][D_NOR_I0_FILTER], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
			//	, this->m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_FILTER_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 180.0, 160.0, FOV);
		}
		else if (m_nEuvImage_Fov == 3000 && m_nEuvImage_ScanGrid == 10 && m_IsUse3umFOV)
		{

			//tempExtraPixel = 200 은 추가 데이터..
			GriddataNew(m_pyFuncReconstructImage, this->m_dRawImage_Data3D[scanBufIndex][DETECTOR_INTENSITY_1], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
				, this->m_dRegridImage_Data3D[scanBufIndex][D1_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 0.0, 0.0, FOV);
			
			//GriddataRemoveNan(m_pyFuncReconstructImageRemoveNan, this->m_dRawImage_Data3D[scanBufIndex][DETECTOR_INTENSITY_1], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
				//, this->m_dRegridImage_Data3D[scanBufIndex][D1_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 0.0, 0.0, FOV);

			
			//GriddataNew(m_pyFuncReconstructImage, this->m_dRawImage_Data3D[scanBufIndex][D_NOR_I0], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
			//	, this->m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 0.0, 0.0, FOV);

			//GriddataNew(m_pyFuncReconstructImage, this->m_dRawImage_Data3D[scanBufIndex][D_NOR_I0_FILTER], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
			//	, this->m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_FILTER_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 0.0, 0.0, FOV);
		}
		else
		{

			GriddataNew(m_pyFuncReconstructImage, this->m_dRawImage_Data3D[scanBufIndex][DETECTOR_INTENSITY_1], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
				, this->m_dRegridImage_Data3D[scanBufIndex][D1_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 220.0, 220.0, FOV);

			//GriddataRemoveNan(m_pyFuncReconstructImageRemoveNan, this->m_dRawImage_Data3D[scanBufIndex][DETECTOR_INTENSITY_1], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
				//, this->m_dRegridImage_Data3D[scanBufIndex][D1_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 220.0, 220.0, FOV);



			//GriddataNew(m_pyFuncReconstructImage, this->m_dRawImage_Data3D[scanBufIndex][D_NOR_I0], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
			//	, this->m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 220.0, 220.0, FOV);

			//GriddataNew(m_pyFuncReconstructImage, this->m_dRawImage_Data3D[scanBufIndex][D_NOR_I0_FILTER], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_X], this->m_dRawImage_Data3D[scanBufIndex][LASERINTERFEROMETER_Y]
			//	, this->m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_FILTER_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][X_REGRID], this->m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], image_size_x, image_size_y, output_size, output_size, 1, 220.0, 220.0, FOV);
		}
	}
	catch (...)
	{
		AfxMessageBox("Griddata(python) Exception 발생");
		//_set_se_translator(new_SE_translator) 정확한 exception 파악하기 위해서는 사용필요 참고
		nRet = -1;
	}

	return nRet;
}

int CADAMCtrl::Averaging(int scanBufIndex, int scanNum, int totalScanNum, int pointMeasureDataIndex)		// 이 함수를 Thread로 돌릴지... 아래 On_Interpolation을 돌릴지 고민중_ KYD_20200210
{
	int nRet = 0;
	double FOV = m_nEuvImage_Fov;// *1000;											//field of view
	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step

	int size = int(FOV / grid_step);
	int Gridsize = size * size;


	//ihlee 
	if (scanNum == 0)
	{
		memcpy(m_dAveragedImage_Data3D[pointMeasureDataIndex][X_REGRID], m_dRegridImage_Data3D[scanBufIndex][X_REGRID], sizeof(double) * Gridsize);
		memcpy(m_dAveragedImage_Data3D[pointMeasureDataIndex][Y_REGRID], m_dRegridImage_Data3D[scanBufIndex][Y_REGRID], sizeof(double) * Gridsize);
	}

	for (int i = 0; i < Gridsize; i++)
	{

		if (scanNum == 0)
		{
			m_dAveragedImage_Data3D[pointMeasureDataIndex][D1_REGRID][i] = m_dRegridImage_Data3D[scanBufIndex][D1_REGRID][i];
			//m_dAveragedImage_Data3D[pointMeasureDataIndex][D_NOR_I0_REGRID][i] = m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_REGRID][i];
			//m_dAveragedImage_Data3D[pointMeasureDataIndex][D_NOR_I0_FILTER_REGRID][i] = m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_FILTER_REGRID][i];
		}
		else
		{
			m_dAveragedImage_Data3D[pointMeasureDataIndex][D1_REGRID][i] += m_dRegridImage_Data3D[scanBufIndex][D1_REGRID][i];
			//m_dAveragedImage_Data3D[pointMeasureDataIndex][D_NOR_I0_REGRID][i] += m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_REGRID][i];
			//m_dAveragedImage_Data3D[pointMeasureDataIndex][D_NOR_I0_FILTER_REGRID][i] += m_dRegridImage_Data3D[scanBufIndex][D_NOR_I0_FILTER_REGRID][i];
		}
	}

	if (scanNum == m_nEuvImage_ScanNumber - 1)
	{
		for (int k = 0; k < Gridsize; k++)
		{
			m_dAveragedImage_Data3D[pointMeasureDataIndex][D1_REGRID][k] = m_dAveragedImage_Data3D[pointMeasureDataIndex][D1_REGRID][k] / m_nEuvImage_ScanNumber;
			//m_dAveragedImage_Data3D[pointMeasureDataIndex][D_NOR_I0_REGRID][k] = m_dAveragedImage_Data3D[0][D_NOR_I0_REGRID][k] / m_nEuvImage_ScanNumber;
			//m_dAveragedImage_Data3D[pointMeasureDataIndex][D_NOR_I0_FILTER_REGRID][k] = m_dAveragedImage_Data3D[0][D_NOR_I0_FILTER_REGRID][k] / m_nEuvImage_ScanNumber;
		}
	}

	return nRet;
}


int CADAMCtrl::Filtering(int pointMeasureDataIndex)
{
	int nRet = 0;
	//m_dFilteredImage_Data[] = m_dAveragedImage_Data; // error 발생 m_dFilteredImage_Data[] 삭제 또는 복사 필요 ihlee

	for (int i = 0; i < KIND_OF_REGRIDDATA; i++)
	{
		//memcpy(m_dFilteredImage_Data[][i], m_dAveragedImage_Data[i], sizeof(double) * ADAM_MAX_DATA_REGRID);
		memcpy(m_dFilteredImage_Data3D[pointMeasureDataIndex][i], m_dAveragedImage_Data3D[pointMeasureDataIndex][i], sizeof(double) * m_nReGridImage_PixelWidth * m_nReGridImage_PixelHeight);
	}

	// Roman High NA Filter에서 사용하는 변수_나중에 위치 및 순서 수정 필요_KYD
	double threshold = 0.3;
	double averaging_size = 50;

	double FOV = m_nEuvImage_Fov;// *1000;			// in nm scale
	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step
	int output_size = int(FOV / grid_step);

	//GetImageCapSensorPos();

	try
	{
		//LowPassFilter(m_pyFuncFilterImage, this->m_dFilteredImage_Data[], output_size, output_size, conventional, 0.35 / 13.5, 0.8, 0.0, 0.0, grid_step);		//python filter  // Roman, 이동근 합의하에 이 0.33에서 0..35로 변경... 
		//LowPassFilterNew(m_pyFuncFilterImage, this->m_dFilteredImage_Data[][D1_REGRID], output_size, output_size, conventional, 0.35 / 13.5, 0.8, 0.0, 0.0, grid_step);			
		//LowPassFilterNew(m_pyFuncFilterImage, this->m_dFilteredImage_Data[][D_NOR_REGRID], output_size, output_size, conventional, 0.35 / 13.5, 0.8, 0.0, 0.0, grid_step);

		LowPassFilterNew(m_pyFuncFilterImage, this->m_dFilteredImage_Data3D[pointMeasureDataIndex][D1_REGRID], output_size, output_size, grid_step, m_LowPassFilterSigma);
		//normalized
		//LowPassFilterNew(m_pyFuncFilterImage, this->m_dFilteredImage_Data3D[pointMeasureDataIndex][D_NOR_I0_REGRID], output_size, output_size, grid_step, m_LowPassFilterSigma);
		//LowPassFilterNew(m_pyFuncFilterImage, this->m_dFilteredImage_Data3D[pointMeasureDataIndex][D_NOR_I0_FILTER_REGRID], output_size, output_size, grid_step, m_LowPassFilterSigma);
	}
	catch (...)
	{
		AfxMessageBox("LowPassFilter(python) Exception 발생");
		//_set_se_translator(new_SE_translator) 정확한 exception 파악하기 위해서는 사용필요 참고
		nRet = -1;
	}

	return nRet;
}

// 패킷 ICD 바뀌면 수정 필요함
// 메모리에 직접 복사도 하고, Class로 만들어서 변수 직접 접근도 하기 위해서 두개로 만들었음.... 향후 최적화 필요..KYD_20191230

void CADAMCtrl::Parsing_ADAM_memcpy(char *original)
{
	unsigned long num_packet = CharToUlong(original);

	//Little endian		
	m_dParsedData[0] = (double)num_packet;
	memcpy(m_dParsedData + 1, original + 4, sizeof(double) * (KINDS_OF_ADAM_DATA - 1)); // PACKET_NUMBER제외 15가지 데이터, 

	//memcpy(&AdamDataNew, original, sizeof(ADAM_DataNew));	
	AdamData.m_nPacketNum = num_packet;

	AdamData.m_dX_position = m_dParsedData[1];
	AdamData.m_dY_position = m_dParsedData[2];

	AdamData.m_dDetector1 = m_dParsedData[3];
	AdamData.m_dDetector2 = m_dParsedData[4];
	AdamData.m_dDetector3 = m_dParsedData[5];
	AdamData.m_dDetector4 = m_dParsedData[6];

	AdamData.m_dCapsensor1 = m_dParsedData[7];
	AdamData.m_dCapsensor2 = m_dParsedData[8];
	AdamData.m_dCapsensor3 = m_dParsedData[9];
	AdamData.m_dCapsensor4 = m_dParsedData[10];

	AdamData.m_dOpticsensor1 = m_dParsedData[11];
	AdamData.m_dOpticsensor2 = m_dParsedData[12];
	AdamData.m_dOpticsensor3 = m_dParsedData[13];

	AdamData.m_dI0_BS = m_dParsedData[14];
	AdamData.m_dI0_ZR = m_dParsedData[15];

	//if (AdamData.m_nPacketNum % NUMBEROFPACKET == 0)
	//{
	//	CapSensorDataUiUpdate(); // UI관련 함수로 UI 쓰레드와 충돌남??
	//}

	////I0 Data Update
	//AdamData.I0_Index = (double)m_nOriignaDataI0_Index;
	//m_dI0_Original[m_nOriignaDataI0_Index++] = m_dParsedData[I0_DETECTOR_INTENSITY];

	//if (m_nOriignaDataI0_Index >= ORIGINAL_DATA_MAX)
	//{
	//	CString strLog;
	//	strLog.Format("[Warning] m_dI0_Original Overflow =%u", m_nOriignaDataI0_Index);
	//	SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
	//	m_nOriignaDataI0_Index = 0;
	//}

	if (AdamData.m_nPacketNum != 0)
	{
		if (AdamData.m_nPacketNum - AdamData.m_nPacketNum_old != 1)
		{
			CString strLog;
			strLog.Format("[Error]Packet error previous =%lu current=%lu", AdamData.m_nPacketNum_old, AdamData.m_nPacketNum);
			SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		}
	}
	AdamData.m_nPacketNum_old = AdamData.m_nPacketNum;

}

void CADAMCtrl::AdamDataMemReset(int pointMeasureDataIndex, vector<int> vec_scanBufIndex) // for ICD2
{
	//필요한 사이즈 만큼 리셋 하는것으로 변경 필요 ihlee

	int size = vec_scanBufIndex.size();

	if (size > 0)
	{
		for (int i = 0; i < size; i++)
		{
			int index = vec_scanBufIndex.at(i);

			for (int j = 0; j < KINDS_OF_DATA; j++)
			{
				memset(m_dRawImage_Data3D[index][j], 0, sizeof(double) * m_nRawImage_PixelWidth * m_nRawImage_PixelHeight); //std::fill() and std::fill_n()
			}
		}
	}
	else
	{
		for (int i = 0; i < MAX_SCAN_MEMORY_NUMBER; i++)
		{
			for (int j = 0; j < KINDS_OF_DATA; j++)
			{
				memset(m_dRawImage_Data3D[i][j], 0, sizeof(double) * m_nRawImage_PixelWidth * m_nRawImage_PixelHeight); //std::fill() and std::fill_n()
			}
		}
	}


	if (m_bIsUseInterpolation)
	{
		for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
		{
			memset(m_dRegridImage_Data3D[pointMeasureDataIndex][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
			memset(m_dAveragedImage_Data3D[pointMeasureDataIndex][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
			memset(m_dFilteredImage_Data3D[pointMeasureDataIndex][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
		}
	}
	// TEST용 삭제해야함?, Display 리셋용
	for (int i = 0; i < KINDS_OF_DATA; i++)
	{
		memset(m_dRawImageForDisplay[i], 0, sizeof(double) * m_nRawImage_PixelWidth * m_nRawImage_PixelHeight); //std::fill() and std::fill_n()
	}

	memset(m_dRawImageDataFor1DRegrid, 0, sizeof(double) * m_nRawImage_PixelWidth * m_nRawImage_PixelHeight);

}


void CADAMCtrl::tic()
{
	start = clock();
}

void CADAMCtrl::toc()
{
	end = clock();
	ParsingTime = (double)(end - start);
	LoopTimeMsg.Format("Processing time은 : %.2f 입니다.", ParsingTime);
	AfxMessageBox(LoopTimeMsg);
}

// EUV PTR 개발코드 부분--------------------------------------------------
int CADAMCtrl::Command_ADAMStart_PTR()
{
	int nRet = 0;
	char chBuf[10];

	return -100; //ihlee 21.01.16 Adma 변경된부분 반영 필요

	AdamDataMemReset();

	Command_LIFReset();

	nRet = SendData(chBuf);

	ResetEvent(m_hAcquisitionEnd);

	return nRet;
}

int CADAMCtrl::Command_ADAMStop_PTR()
{
	int nRet = 0;
	char chBuf[10];

	nRet = SendData(chBuf);

	return -100; //ihlee 21.01.16 Adma 변경된부분 반영 필요

	//그냥 넣어 줬음
	m_bIsScaningOn = FALSE;
	m_nImageScanCnt = 0;
	m_nInterpolationCnt = 0;


	m_nFovDataCnt = 0;		//강제 종료후 처음부터 시작하기 위해서 초기화

	WaitSec(1);

	return nRet;
}

unsigned long CADAMCtrl::CharToUlong(char* buf)
{
	unsigned long out;

	unsigned char* pChar;

	pChar = (unsigned char*)&out;

	for (int i = 0; i < 4; i++)
	{
		pChar[i] = buf[i];
	}
	return out;
}

int CADAMCtrl::Command_AverageRun()
{
	//이벤트로 값 업데이트 확인하는 부분 추가 필요 ihlee
	int nRet = 0;
	char chBuf[5];

	chBuf[0] = 0x01;
	chBuf[1] = 0x00;
	chBuf[2] = 0x01;
	chBuf[3] = 0x11;
	chBuf[4] = 0x00;


	nRet = SendData(5, chBuf);

	return nRet;
}

int CADAMCtrl::Command_AverageRunTimeout()
{
	int timeout = 1000;
	//이벤트로 값 업데이트 확인하는 부분 추가 필요 ihlee
	int nRet = 0;
	char chBuf[5];

	chBuf[0] = 0x01;
	chBuf[1] = 0x00;
	chBuf[2] = 0x01;
	chBuf[3] = 0x11;
	chBuf[4] = 0x00;

	nRet = SendData(5, chBuf, timeout, 1);

	return nRet;
}

int CADAMCtrl::Command_AverageRunAfterTimeout()
{
	int timeout = 1000;
	//이벤트로 값 업데이트 확인하는 부분 추가 필요 ihlee
	int nRet = 0;

	m_bAverageRunAfter = TRUE;
	ResetEvent(m_hAverageRun);

	Command_ADAMStart();

	//Event Reset
	nRet = WaitEvent(m_hAverageRun, 10000);

	if (nRet != 0)
	{
		Command_ADAMStop();
	}
	m_bAverageRunAfter = FALSE;

	return nRet;
}


int CADAMCtrl::Command_SetAverageCount(unsigned int count)
{
	int nRet = 0;

	char chBuf[8];

	chBuf[0] = 0x01;
	chBuf[1] = 0x00;
	chBuf[2] = 0x04;
	chBuf[3] = 0x12;

	unsigned char* p_char = (unsigned char*)&count;

	chBuf[4] = p_char[3];
	chBuf[5] = p_char[2];
	chBuf[6] = p_char[1];
	chBuf[7] = p_char[0];

	nRet = SendData(8, chBuf);

	return nRet;
}


int CADAMCtrl::Command_ADAMStart()
{
	int nRet = 0;
	char chBuf[10];

	chBuf[0] = 0x01;
	chBuf[1] = 0x00;
	chBuf[2] = 0x01;
	chBuf[3] = 0x01;
	chBuf[4] = 0x01;

	nRet = SendData(5, chBuf);

	return nRet;
}

//
//int CADAMCtrl::Command_ADAMStart()
//{
//	int nRet = 0;
//	char chBuf[10];
//
//	try
//	{
//		if (m_bConnected)
//		{
//
//			chBuf[0] = 0x01;
//			chBuf[1] = 0x00;
//			chBuf[2] = 0x01;
//			chBuf[3] = 0x01;
//			chBuf[4] = 0x01;
//
//			nRet = SendData(5, chBuf);
//		}
//		else
//		{
//			nRet = -1;
//			throw nRet;
//		}
//	}
//	catch (int ex)
//	{
//		// Exception 발생
//		ex;
//	}
//
//	// Exception 발행 해도 이 부분 실행됨(메모리 정리등)
//	return nRet;
//}
int CADAMCtrl::Command_ADAMStop()
{
	int nRet = 0;
	char chBuf[10];
	try
	{
		if (m_bConnected)
		{

			chBuf[0] = 0x01;
			chBuf[1] = 0x00;
			chBuf[2] = 0x01;
			chBuf[3] = 0x01;
			chBuf[4] = 0x00;

			nRet = SendData(5, chBuf);
		}
		else
		{
			nRet = -1;
			throw nRet;
		}
	}
	catch (int ex)
	{
		// Exception 발생
		ex;
	}

	// Exception 발행 해도 이 부분 실행됨(메모리 정리등)
	return nRet;

}


//void CADAMCtrl::AdamUIControl(BOOL isEnable)
//{
//
//}

//void CADAMCtrl::CapSensorDataUiUpdate()
//{
//}

//void CADAMCtrl::CurrentScanNumUiUpdate()
//{
//}

int CADAMCtrl::ReceiveData(char *lParam, int nLength)
{
	int nRet = 0;
	CString strLog;

#if TRUE
	/// 1. Packet 분할되서 들어올 경우 처리 초기화
	if (m_bReceiveBytesReset == TRUE)
	{
		m_bReceivedBytesSizeTotalUpdated = FALSE;
		m_nReceivedBytesSizeTotal = 0;
		m_nReceivedBytesSizeCurrent = 0;
		m_bReceiveBytesReset = FALSE;
	}

	int m_nReceivedBytesSizePrevious = m_nReceivedBytesSizeCurrent;

	int test = sizeof(int);

	m_nReceivedBytesSizeCurrent = m_nReceivedBytesSizeCurrent + nLength;

	if (m_nReceivedBytesSizeCurrent > ADAM_RECEIVE_BUFFER_SIZE || m_nReceivedBytesSizeCurrent < 0)
	{
		strLog.Format("[Error] m_nReceivedBytesSizeCurrent > ADAM_RECEIVE_BUFFER_SIZE: m_nReceivedBytesSizeCurrent = %d, nLength = %d", m_nReceivedBytesSizeCurrent, nLength);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

		//ERROR 처리 2개 PACEKT이 완성되는경우 어떻게 처리?? 일단 리셋함
		m_bReceiveBytesReset = TRUE;

		nRet = -1;
		return nRet;
	}

	//데이터가 이전 데이터가 분할된 경우
	if (m_nReceivedBytesSizePrevious != 0)
	{
		strLog.Format("[Info] Packet Is Devided: m_nReceivedBytesSizePrevious =%d, m_nReceivedBytesSizeCurrent =%d", m_nReceivedBytesSizePrevious, m_nReceivedBytesSizeCurrent);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
	}

	memcpy(&m_chReceivedPrebuf[m_nReceivedBytesSizePrevious], lParam, sizeof(byte) * nLength);


	if (m_bReceivedBytesSizeTotalUpdated == FALSE)
	{
		if (m_nReceivedBytesSizeCurrent >= HEADERSIZE)
		{
			// Parsing
			m_nReceivedBytesSizeTotal = (unsigned char)m_chReceivedPrebuf[1] + (unsigned char)m_chReceivedPrebuf[2] * 256;  //E460 = 58464, 1E460 = 124000, 007c 124

			if (m_nReceivedBytesSizeTotal == 58464) 	   //2byte로 data  한계가 있음. 임시 ihlee //packet 1000일경우
			{
				m_nReceivedBytesSizeTotal = NUMBEROFPACKET * PACKETSIZE;
				m_bReceivedBytesSizeTotalUpdated = TRUE;
			}
			else if (m_nReceivedBytesSizeTotal == 124)
			{
				m_bReceivedBytesSizeTotalUpdated = TRUE;
			}
			else if (m_nReceivedBytesSizeTotal == NUMBEROFPACKET * PACKETSIZE) 	   //packet 500 일경우
			{
				m_bReceivedBytesSizeTotalUpdated = TRUE;
			}
			else
			{
				//m_nReceivedBytesSizeTotal = NUMBEROFPACKET * PACKETSIZE;
				//m_bReceivedBytesSizeTotalUpdated = TRUE;

				strLog.Format("[Error] Wrong Packet Size: m_nReceivedBytesSizeTotal= %d", m_nReceivedBytesSizeTotal);
				SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

				m_bReceiveBytesReset = TRUE;

				nRet = -1;
				return nRet;
			}
		}
		else
		{
			strLog.Format("[Info] Received size is lower than Header: m_nReceivedBytesSizeCurrent = %d", m_nReceivedBytesSizeCurrent);
			SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		}
	}

	if (m_bReceivedBytesSizeTotalUpdated)
	{
		if (m_nReceivedBytesSizeCurrent == m_nReceivedBytesSizeTotal + HEADERSIZE)
		{
			m_bReceiveBytesReset = TRUE;

			if (m_nReceivedBytesSizeCurrent == PACKETSIZE + HEADERSIZE) //Average Run
			{
				//Average Run Parsing
				memcpy(m_chReceivedbuf[0], m_chReceivedPrebuf + HEADERSIZE, sizeof(byte) * PACKETSIZE);
				ReceivedData_Average(m_chReceivedbuf[0]);
			}
			//else if (m_nReceivedPacketSizeCurrent == ADAM_RECEIVE_BUFFER_SIZE)
			else if (m_nReceivedBytesSizeCurrent == NUMBEROFPACKET * PACKETSIZE + HEADERSIZE)
			{
				int numOfPacket = m_nReceivedBytesSizeTotal / PACKETSIZE;
				if (m_bAverageRunAfter)
				{
					Command_ADAMStop();
					ReceivedData_After(m_chReceivedPrebuf, numOfPacket);
				}
				else
				{
					for (int i = 0; i < numOfPacket; i++)
					{
						memcpy(m_chReceivedbuf[i], m_chReceivedPrebuf + HEADERSIZE + PACKETSIZE * i, sizeof(byte) * PACKETSIZE);

						if (m_nScanType == UNI_DIRECTION)
						{
							nRet = ReceivedData_UniD(m_chReceivedbuf[i]);
						}
						if (m_nScanType == BI_DIRECTION)
						{
							if (m_nEuvImage_Fov == 3000 && m_nEuvImage_ScanGrid == 10 && m_IsUse3umFOV)
							{
								nRet = ReceivedData_3umOnly(m_chReceivedbuf[i]);
								//nRet = ReceivedData_BiD(m_chReceivedbuf[i]);
							}
							else
							{
								nRet = ReceivedData_BiD(m_chReceivedbuf[i]);
							}
						}
						if (m_nScanType == ELITHO_DIRECTION)
						{
							nRet = ReceivedData_Elitho(m_chReceivedbuf[i]);
						}
					}
				}

			}
		}
		else if (m_nReceivedBytesSizeCurrent > m_nReceivedBytesSizeTotal + HEADERSIZE)
		{
			strLog.Format("[Error] Received size is higher than estimated: m_nReceivedBytesSizeCurrent = %d", m_nReceivedBytesSizeCurrent);
			SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		}
	}


#endif

#if FALSE
	if (nLength == ADAM_RECEIVE_BUFFER_SIZE)
	{
		//버퍼에 복사
		for (int i = 0; i < NUMBEROFPACKET; i++)
		{
			memcpy(m_chReceivedbuf[i], lParam + HEADERSIZE + PACKETSIZE * i, sizeof(byte) * PACKETSIZE);

			if (m_nScanType == UNI_DIRECTION)
			{
				nRet = ReceivedData_UniD(m_chReceivedbuf[i]);
			}
			if (m_nScanType == BI_DIRECTION)
			{
				nRet = ReceivedData_BiD(m_chReceivedbuf[i]);
			}
		}

	}
	else if (nLength == PACKETSIZE + HEADERSIZE) //Average Run
	{
		//Average Run Parsing
		memcpy(m_chReceivedbuf[0], lParam + HEADERSIZE, sizeof(byte) * PACKETSIZE);
		ReceivedData_Average(m_chReceivedbuf[0]);
	}
	else
	{
		int test = 0;
		strLog.Format("[Error] nLength error =%d", nLength);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

	}
#endif

	return nRet;
}



int CADAMCtrl::WaitReceiveEventThread()
{

	int		nRet = 0;
	int		i = 0;
	char*	pInBuf = new char[m_nReceiveMaxBufferSize];
	char*	pOutBuf = new char[m_nReceiveMaxBufferSize];
	//int		total = 0;


	int		nCondition = 0;
	unsigned long	tmp = 0;
	struct timeval timeout;
	fd_set fds;

	SOCKET	tmpSocket = INVALID_SOCKET;
	if (m_bIsServerSocket)
	{
		tmpSocket = m_AcceptSocket;
	}
	else
	{
		tmpSocket = m_Socket;
	}

	//TRACE("TCPIP Receive Thread Start..\n");

	memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
	while (!m_bKillReceiveThread)
	{
		m_bConnected = TRUE;
		timeout.tv_sec = 0;
		timeout.tv_usec = 100;
		FD_ZERO(&fds);
		FD_SET(m_Socket, &fds);
		m_nSocketStatus = select(sizeof(fds) * 8, &fds, NULL, NULL, &timeout);

		if (m_nSocketStatus == SOCKET_ERROR) // m_nSocketStatus=0: Timeout 종종 발생함  ihlee
		{
			CString strLog;
			strLog.Format("[Error]Adam Disconnected: m_nSocketStatus = SOCKET_ERROR");
			SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

			m_bKillReceiveThread = TRUE;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}

		if (tmpSocket != INVALID_SOCKET)
		{
			int bytesReceived = recv(tmpSocket, pInBuf, m_nReceiveMaxBufferSize, NULL);

			if (m_bKillReceiveThread)
			{
				break;
			}

			SetEvent(m_hSocketEvent);
			BOOL isParsing = FALSE;
			if (bytesReceived > 0)
			{
				memcpy(pOutBuf, pInBuf, sizeof(byte) * bytesReceived);
				nRet = ReceiveData(pOutBuf, bytesReceived);
			}
			else
			{
				// 아랫 부분 있으면 통신 THREAD 종료 지연됨, 메모리 릭 발생 ihlee
				//bytesReceived==0 : gracefully closed 접속 해제 상태ihlee 
				CString strLog;
				strLog.Format("[Error]Adam bytesReceived: bytesReceived = %d", bytesReceived);
				SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

				m_bKillReceiveThread = TRUE;
				m_bConnected = FALSE;
				nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
				m_nRet = nRet;
				//SetEvent(m_hSocketEvent);			
			}
		}
		else
		{
			if (m_bIsServerSocket)
			{
				m_AcceptSocket = INVALID_SOCKET;
			}
			else
			{
				m_Socket = INVALID_SOCKET;
			}

			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}

		//Sleep(1); //김영덕 변경
		//usleep(50); // 이거 왜? ihlee 삭제함
	}

	delete[] pInBuf;
	pInBuf = NULL;
	delete[] pOutBuf;
	pOutBuf = NULL;
	TRACE("TCPIP Receive Thread End..\n");

	return nRet;

}

int CADAMCtrl::ADAMRunStart()
{
	int nRet = 0;
	CString exceptionMsg;

	// 추가한 초기화 ihlee 2021.03.21
	int TotalScanNum = m_nEuvImage_ScanNumber;//m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;						
	int PointMeasureBufIndex;
	vector <int> vecScanBufIndex;
	//vecScanBufIndex.reserve(TotalScanNum);
	//MemoryAcquireAndInitForPointMeasure(TotalScanNum, PointMeasureBufIndex, vecScanBufIndex);

	PointMeasureBufIndex = 0;
	for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
	{		
		vecScanBufIndex.push_back(i);
	}

	SetPointMeasureBufIndex(PointMeasureBufIndex);
	SetScanBufIndexVector(vecScanBufIndex); //? 이건 뭐고
	SetScanBufIndexForReceiveData(vecScanBufIndex); // 이건 뭐임 같은거 같음

	try
	{
		if (!m_bConnected)
		{
			exceptionMsg.Format("Adam is not Connected");
			nRet = -1;
			throw exceptionMsg;
		}

		// UI disable
		AdamUIControl(FALSE);

		//변수 초기화 
		m_bIsScaningOn = TRUE; //ADAMRunStart
		m_bAbort = FALSE;
		m_bIsFirstScan = FALSE;
		m_nImageScanCnt = 0;
		CurrentScanNumUiUpdate();

		m_nInterpolationCnt = 0;
		m_nFovDataCnt = 0;

		m_bCanGetData_AtUniDirection = FALSE;									// X Scan
		m_bCanGetData_AtYaxisDirectoin = FALSE;

		// Bi direciton 변수 초기화
		m_bIsScanDirectionForward = FALSE;
		m_nLineNumCnt = 0;
		m_bIsLineScanCompleted = FALSE;
		m_bCanGetData_AtXaxisDirectoin = FALSE;
		m_bIsAcquisitionStart = FALSE;

		m_nReverseDataCnt = 0;
		m_nFovDataCnt_Bi = 0;

		pos_x_current = 0;
		pos_x_old = 0;
		pos_x_oldold = 0;
		pos_y_current = 0;
		pos_y_old = 0;
		pos_y_oldold = 0;

		//Diaplay interpolation초기화
		previousLintCountInterpolation = 0;

		//MutexLock(m_hMutex); //동작 안함  한 함수 내에 있어야 함? 

		if (m_nEuvImage_Fov == 2000)
		{
			//m_dAcelDistance_X = 300.000;			// 숫자 1 이 1 nm 임... ADAM에서 넘어 오는 것도 나노 단위 기준으로 세팅 필요, 정수배로 맞아 떨어져서 pixel 위치가 바뀌는 것을 방지하기위해 넣었음 (m_nEuvImage_ScanGrid/2)
			//m_dAcelDistance_Y = 300.000;			// +(m_nEuvImage_ScanGrid / 2);			// 향후 FOV 크기 맞춰서 조정 필요

			m_dAcelDistance_X = 200.000;			// 숫자 1 이 1 nm 임... ADAM에서 넘어 오는 것도 나노 단위 기준으로 세팅 필요, 정수배로 맞아 떨어져서 pixel 위치가 바뀌는 것을 방지하기위해 넣었음 (m_nEuvImage_ScanGrid/2)
			m_dAcelDistance_Y = 200.000;			// +(m_nEuvImage_ScanGrid / 2);			// 향후 FOV 크기 맞춰서 조정 필요
		}
		else if (m_nEuvImage_Fov == 10000)
		{
			m_dAcelDistance_X = 300.000;			// 
			m_dAcelDistance_Y = 200.000;			// 
		}
		else if (m_nEuvImage_Fov == 3000 && m_nEuvImage_ScanGrid == 10 && m_IsUse3umFOV) // 임시 ihlee 2021.03.17
		{
			m_dAcelDistance_X = 0.0;			// 
			m_dAcelDistance_Y = 0.0;			// 
		}
		else
		{
			m_dAcelDistance_X = 200.000;			// 
			m_dAcelDistance_Y = 200.000;			// 
		}

		// Pixel size를 정의한 부분임
		m_nRawImage_PixelWidth = m_nEuvImage_Fov/* * 1000 */ / m_nEuvImage_ScanGrid;
		m_nRawImage_PixelHeight = m_nEuvImage_Fov/* * 1000 */ / m_nEuvImage_ScanGrid;

		m_nReGridImage_PixelWidth = m_nEuvImage_Fov/* * 1000 */ / m_nEUVImage_InterpolationGrid;
		m_nReGridImage_PixelHeight = m_nEuvImage_Fov/* * 1000 */ / m_nEUVImage_InterpolationGrid;


		if (m_nRawImage_PixelWidth > ADAM_MAX_WIDTH || m_nRawImage_PixelHeight > ADAM_MAX_HEIGHT)
		{
			exceptionMsg.Format("Exceed memory, Check FOV and ScanGrid");
			nRet = -2;
			throw exceptionMsg;
		}

		if (m_nEuvImage_Fov >= m_HighFovCriteria && m_bIsUseHighFovInterpolation == FALSE)
		{
			m_bIsUseInterpolation = FALSE;
		}
		else
		{
			m_bIsUseInterpolation = TRUE;
		}

		if (m_bIsUseInterpolation)
		{
			if (m_nReGridImage_PixelWidth > ADAM_MAX_WIDTH_REGRID || m_nReGridImage_PixelHeight > ADAM_MAX_HEIGHT_REGRID)
			{
				exceptionMsg.Format("Exceed memory, Check FOV and Interpolation Grid");
				nRet = -2;
				throw exceptionMsg;
			}
		}

		//버퍼 초기화 
		m_nOriignaDataI0_Index = 0;
		AdamDataMemReset();



		InitAdamDlgForScan();

		Command_LIFReset();
		Sleep(200);		// Reset 하고 조금 있다가 Stage를 Start 하기 위해서 sleep을 많이 주었음.

		/* Thread 초기화*/
		for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
		{
			ResetEvent(m_hAcquisitionEnds[i]);
			ResetEvent(m_hRegridEnds[i]);

		}
		ResetEvent(m_hAverageEnd);
		ResetEvent(m_hFilterEnd);

		/* Thread 시작 (거꾸로 실행)*/
		m_pWaitScanEndThread = AfxBeginThread(WaitScanEndThread, this, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
		m_pFilterThread = AfxBeginThread(FilterThread, this, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL, THREAD_PRIORITY_HIGHEST				
		m_pAverageThread = AfxBeginThread(AverageThread, this, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL, THREAD_PRIORITY_HIGHEST				

		for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
		{
			m_pRegridThreads[i] = AfxBeginThread(RegridThread, &m_ThreadParam[i], THREAD_PRIORITY_NORMAL, 0, 0);
		}

		Sleep(200);		//Thread 실행 시간 확보
		//Adam Start
		Command_ADAMStart();

	}
	catch (CString exMsg)
	{
		// Exception 발생
		m_bIsScaningOn = FALSE;
		AfxMessageBox("Exception:ADAMRunStart(): " + exMsg);
	}

	return nRet;
}

int CADAMCtrl::ADAMAbort()
{
	int nRet = 0;

	//Thread 종료 명령
	Command_ADAMStop();
	m_bAbort = TRUE;
	m_IsMeasureComplete = TRUE;

	SaveLogFile("ProcessLogPointMeasure", "Abort");

	for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
	{
		SetEvent(m_hAcquisitionEnds[i]);
	}

	//New Thread 종료 확인필요.모든거.
	for (int i = 0; i < MaxScanMemory; i++)
	{
		//unique_lock<mutex> lock(mutexScanDataReady[i]);
		{
			unique_lock<mutex> lock(mutexScanDataReady[i]);
			boolScanDataReady[i] = TRUE;
		}		
		cvScanDataReady.at(i).notify_all();
	}

	m_bIsScaningOn = FALSE;

	return nRet;
}

int CADAMCtrl::ReceivedData_UniD(char *lParam)		//지금사용
{
	int nRet = 0, i = 0, num = 0, num2 = 0, test = 0;
	double pos_x = 0, pos_y = 0;

	// Data parssing 함수
	Parsing_ADAM_memcpy(lParam);

	//I0 Data Update
	AdamData.I0_Index = (double)m_nOriignaDataI0_Index;
	m_dI0_Original[m_nOriignaDataI0_Index++] = m_dParsedData[I0_DETECTOR_INTENSITY];

	if (m_nOriignaDataI0_Index >= ORIGINAL_DATA_MAX)
	{
		CString strLog;
		strLog.Format("[Warning] m_dI0_Original Overflow =%u", m_nOriignaDataI0_Index);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		m_nOriignaDataI0_Index = 0;
	}


	if (AdamData.m_nPacketNum == 0)			// ADAM 에서 넘어오는 시작 순서 변경으로 0 확정 _ KYD_20200214
	{
		m_bIsFirstScan = TRUE;
	}
	// 실제 패킷에서 날아온 정보를 메모리에 넣어주는 부분
	if (m_bIsScaningOn == TRUE)	    //Scan 동작 중에만 TRUE
	{
		if (m_bIsFirstScan == TRUE)	//Scan 시작하고 첫 Data를 기억 후 다음 들어오는 값을 Normalization하자
		{
			if (m_IsResetStartPos)
			{
				if (m_IsResetStartPos)
				{
					m_dLaserX_1st = pos_x = AdamData.m_dX_position;
					m_dLaserY_1st = pos_y = AdamData.m_dY_position;
				}

			}
			m_bIsFirstScan = FALSE;
		}
		else
		{
			pos_x = AdamData.m_dX_position - m_dLaserX_1st;	//laser interferometer x axis position data
			pos_y = AdamData.m_dY_position - m_dLaserY_1st;	//laser interferometer y axis position data
		}

		//------------------Y 축 스캐닝 포함하기 위해서 코드 수정함----------------------------------- Start of Section A, 
		// 코드 Hystory에 축만 있는 것도 존재함 필요실 확인 바람_KYD_20200239

		if (pos_x < m_dAcelDistance_X)
		{
			m_bCanGetData_AtUniDirection = TRUE;
		}

		if (pos_y < m_dAcelDistance_Y)
			m_bCanGetData_AtYaxisDirectoin = TRUE;		// Y 축 데이터 획득 구간 정의함2

		if (m_bCanGetData_AtYaxisDirectoin == TRUE)
		{
			if (pos_y >= m_dAcelDistance_Y)
			{
				if (m_bCanGetData_AtUniDirection == TRUE)
				{
					if (pos_x >= m_dAcelDistance_X)
					{
						num = m_nFovDataCnt;

						if (m_nFovDataCnt > (m_nRawImage_PixelWidth - 1))
						{
							num2 = num - m_nRawImage_PixelWidth;

							for (i = 0; i < KINDS_OF_ADAM_DATA; i++) // 수정필요
							{
								if (i == LASERINTERFEROMETER_X)
								{
									m_dRawImageForDisplay[i][num2] = pos_x;  // TEST용 삭제해야함, Display? 
									m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][num2] = pos_x;
								}
								else if (i == LASERINTERFEROMETER_Y)
								{
									m_dRawImageForDisplay[i][num2] = pos_y; // TEST용 삭제해야함, Display? 
									m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][num2] = pos_y;
								}
								else
								{
									m_dRawImageForDisplay[i][num2] = m_dParsedData[i]; // TEST용 삭제해야함, Display? 
									m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][num2] = m_dParsedData[i];
								}
							}
							m_dRawImageForDisplay[I0_INDEX][num2] = AdamData.I0_Index;
							m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][I0_INDEX][num2] = AdamData.I0_Index;

						}

						m_nFovDataCnt++;

						m_nRemainPixelCnt = m_nFovDataCnt % m_nRawImage_PixelWidth;

						if (m_nRemainPixelCnt == 0)
						{
							m_bCanGetData_AtUniDirection = FALSE;
							SetEvent(m_hDisplayRawImage);
							//TRACE("Image Line Cnt: %d\n", m_nFovDataCnt / m_nRawImage_PixelWidth);
						}

						if (m_nFovDataCnt == m_nRawImage_PixelWidth * m_nRawImage_PixelHeight + m_nRawImage_PixelHeight)
						{
							m_bCanGetData_AtYaxisDirectoin = FALSE;

							// Image 받아졌으면 이벤트 Signaled로 만들어 Interpolation 수행					
							SetEvent(m_hAcquisitionEnds[m_nImageScanCnt]);
							m_nImageScanCnt++;

							// Adam 종료
							if (m_nImageScanCnt == m_nEuvImage_ScanNumber)
							{
								//DisplayRawImage();
								Command_ADAMStop();
							}
							else
							{
								CurrentScanNumUiUpdate();
							}

							m_nFovDataCnt = 0;		// 이게 들어가 줘야 다음번 스캐닝에 다시 0부터 시작하는 것 아닌가?							
						}
					}
				}
			}
		}
	}

	return nRet;
}

int CADAMCtrl::ReceivedData_BiD(char *lParam)
{
	int nRet = 0, i = 0, num = 0, dataNum = 0, dataNum2 = 0, num2 = 0, num3 = 0, num4 = 0;
	double pos_x = 0, pos_y = 0;
	CString strLog;

	Parsing_ADAM_memcpy(lParam);

	//I0 Data Update
	AdamData.I0_Index = (double)m_nOriignaDataI0_Index;
	m_dI0_Original[m_nOriignaDataI0_Index++] = m_dParsedData[I0_DETECTOR_INTENSITY];

	if (m_nOriignaDataI0_Index >= ORIGINAL_DATA_MAX)
	{
		CString strLog;
		strLog.Format("[Warning] m_dI0_Original Overflow =%u", m_nOriignaDataI0_Index);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		m_nOriignaDataI0_Index = 0;
	}

	if (AdamData.m_nPacketNum == 0)			// ADAM 에서 넘어오는 시작 순서 변경으로 0 확정 _ KYD_20200214
	{
		m_bIsFirstScan = TRUE;
		m_bIsScanDirectionForward = TRUE;
	}

	if (m_bIsScaningOn == TRUE)	    //Scan 동작 중에만 TRUE
	{
		m_nFovDataCntold = m_nFovDataCnt;

		//처음 스캔 시작시 위치를 정하기 위한 부분 // 일단은 Uni-Direction 과 동일하게
		if (m_bIsFirstScan == TRUE)
		{
			if (m_IsResetStartPos)
			{
				m_dLaserX_1st = pos_x = AdamData.m_dX_position;
				m_dLaserY_1st = pos_y = AdamData.m_dY_position;
			}
			m_bIsFirstScan = FALSE;
		}
		else
		{
			pos_x_current = pos_x = AdamData.m_dX_position - m_dLaserX_1st;	//laser interferometer x axis position data
			pos_y_current = pos_y = AdamData.m_dY_position - m_dLaserY_1st;	//laser interferometer y axis position data
		}

		if ((pos_x_current - pos_x_oldold) > 0)
			m_bIsScanDirectionForward = TRUE;

		if ((pos_x_current - pos_x_oldold) < 0)
			m_bIsScanDirectionForward = FALSE;

		//다시 고민해 볼것....
		if ((m_bIsScanDirectionForward == FALSE) && (pos_x > m_dAcelDistance_X + m_nRawImage_PixelWidth * m_nEuvImage_ScanGrid + 80))
			m_bCanGetData_AtXaxisDirectoin = TRUE;

		if ((m_bIsScanDirectionForward == TRUE) && (pos_x < m_dAcelDistance_X - 80))
			m_bCanGetData_AtXaxisDirectoin = TRUE;

		if (pos_y < m_dAcelDistance_Y)
			m_bCanGetData_AtYaxisDirectoin = TRUE;		// Y 축 데이터 획득 구간 정의함2

		if (m_bCanGetData_AtYaxisDirectoin == TRUE)
		{
			if (m_bCanGetData_AtXaxisDirectoin == TRUE)
			{
				if (pos_y >= m_dAcelDistance_Y)
				{
					if (m_bIsScanDirectionForward == TRUE)
					{
						// Right scan 데이터 획득 코드

						if (pos_x >= m_dAcelDistance_X)
						{
							num = m_nFovDataCnt_Bi;

							if (m_nFovDataCnt_Bi > (m_nRawImage_PixelWidth - 1))
							{
								dataNum = num - m_nRawImage_PixelWidth;

								for (i = 0; i < KINDS_OF_ADAM_DATA; i++)
								{
									// LIF NO Reset 전 Bi-ddirection Code
									//m_dRawImage_Data[i][dataNum] = m_dParsedData[i];// TEST용 삭제해야함, Display? 
									//m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = m_dParsedData[i];

									if (i == LASERINTERFEROMETER_X)
									{
										m_dRawImageForDisplay[i][dataNum] = pos_x;  // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = pos_x;
									}
									else if (i == LASERINTERFEROMETER_Y)
									{
										m_dRawImageForDisplay[i][dataNum] = pos_y; // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = pos_y;
									}
									else
									{
										m_dRawImageForDisplay[i][dataNum] = m_dParsedData[i]; // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = m_dParsedData[i];
									}

								}

								m_dRawImageForDisplay[I0_INDEX][dataNum] = AdamData.I0_Index;
								m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][I0_INDEX][dataNum] = AdamData.I0_Index;
							}

							m_nFovDataCnt_Bi++;

							m_nRemainPixelCnt = m_nFovDataCnt_Bi % m_nRawImage_PixelWidth;

							if (m_nRemainPixelCnt == 0)
							{
								m_bCanGetData_AtXaxisDirectoin = FALSE;
								SetEvent(m_hDisplayRawImage);
							}
						}
					}

					if (m_bIsScanDirectionForward == FALSE)
					{
						// Left scan 데이터 획득 코드

						if (pos_x <= (m_dAcelDistance_X + (m_nRawImage_PixelWidth * m_nEuvImage_ScanGrid)))		//bi direction 용으로 잠깐 수정 // 1.5에서 잠깐 수정 20은 DDL 적용하고 스테이지 안정화 되면 지워야함 KYD 2021/01/22
						{
							num3 = m_nFovDataCnt_Bi;

							if (m_nFovDataCnt_Bi > (m_nRawImage_PixelWidth - 1))
							{
								num4 = num3 - m_nRawImage_PixelWidth;

								dataNum = num4 + ((m_nRawImage_PixelWidth - 1) - 2 * m_nReverseDataCnt);

								for (i = 0; i < KINDS_OF_ADAM_DATA; i++)
								{
									//m_dRawImage_Data[i][dataNum] = m_dParsedData[i];// TEST용 삭제해야함, Display? 
									//m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = m_dParsedData[i];

									if (i == LASERINTERFEROMETER_X)
									{
										m_dRawImageForDisplay[i][dataNum] = pos_x;  // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = pos_x;
									}
									else if (i == LASERINTERFEROMETER_Y)
									{
										m_dRawImageForDisplay[i][dataNum] = pos_y; // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = pos_y;
									}
									else
									{
										m_dRawImageForDisplay[i][dataNum] = m_dParsedData[i]; // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = m_dParsedData[i];
									}
								}

								m_dRawImageForDisplay[I0_INDEX][dataNum] = AdamData.I0_Index;
								m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][I0_INDEX][dataNum] = AdamData.I0_Index;

								m_nReverseDataCnt++;

							}

							m_nFovDataCnt_Bi++;


							m_nRemainPixelCnt = m_nFovDataCnt_Bi % m_nRawImage_PixelWidth;

							if (m_nRemainPixelCnt == 0)
							{
								m_bCanGetData_AtXaxisDirectoin = FALSE;
								m_nReverseDataCnt = 0;
								SetEvent(m_hDisplayRawImage);
							}
						}
					}

					if (m_nFovDataCnt_Bi == m_nRawImage_PixelWidth * m_nRawImage_PixelHeight + m_nRawImage_PixelHeight)
					{
						m_bCanGetData_AtYaxisDirectoin = FALSE;

						// Image 받아졌으면 이벤트 Signaled로 만들어 Interpolation 수행										
						SetEvent(m_hAcquisitionEnds[m_nImageScanCnt]);
						m_nImageScanCnt++;

						// Adam 종료
						if (m_nImageScanCnt == m_nEuvImage_ScanNumber)
						{
							//DisplayRawImage();
							Command_ADAMStop();
						}
						else
						{
							CurrentScanNumUiUpdate();
						}

						m_nFovDataCnt_Bi = 0;

					}

				}

			}
		}

		pos_x_oldold = pos_x_old;
		pos_y_oldold = pos_y_old;

		pos_x_old = pos_x;
		pos_y_old = pos_y;
	}

	return nRet;
}


int CADAMCtrl::ADAMRunStartForElitho()
{

	//Command_LIFReset();
	//Sleep(200);		// Reset 하고 조금 있다가 Stage를 Start 하기 위해서 sleep을 많이 주었음.


	D3Length = 0;
	m_nScanType = ELITHO_DIRECTION;

	Command_ADAMStart();

	return 0;
}

int CADAMCtrl::ReceivedData_Elitho(char *lParam)
{
	int nRet = 0;
	
	Parsing_ADAM_memcpy(lParam);
	D3Data[D3Length++] = AdamData.m_dDetector2;
	
	if (D3Length >= D3_MAX)
	{
		D3Length = 0;
	}

	

	return nRet;
}


double AverageD3(double *data, int length)
{
	double sum = 0;

	for (int i = 0; i < length; i++)
	{
		sum = sum + data[i];
	}

	double average = sum / length;

	return average;
}

int CADAMCtrl::ADAMAbortForElitho()
{

	Command_ADAMStop();


	D3Average = AverageD3(D3Data, D3Length);
	// 1회 스캔 21초
	// 최대 15회 스캔, 5Khz
	// 버퍼 사이즈  21*15 * 5000 = 1,575,000  ==>2,000,000개
	// 2,000,000

	return 0;
}

// 삼성 데모를 위해서 임의로 3 um 함수를 만들었음 3um, 10nm외 사용 금지
int CADAMCtrl::ReceivedData_3umOnly(char *lParam)
{
	int nRet = 0, num = 0;
	double pos_x = 0, pos_y = 0;

	CString strLog;

	Parsing_ADAM_memcpy(lParam);

	//I0 Data Update
	AdamData.I0_Index = (double)m_nOriignaDataI0_Index;
	m_dI0_Original[m_nOriignaDataI0_Index++] = m_dParsedData[I0_DETECTOR_INTENSITY];

	if (m_nOriignaDataI0_Index >= ORIGINAL_DATA_MAX)
	{
		CString strLog;
		strLog.Format("[Warning] m_dI0_Original Overflow =%u", m_nOriignaDataI0_Index);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		m_nOriignaDataI0_Index = 0;
	}

	if (AdamData.m_nPacketNum == 0)			// ADAM 에서 넘어오는 시작 순서 변경으로 0 확정 _ KYD_20200214
	{
		m_bIsFirstScan = TRUE;
		m_bIsScanDirectionForward = TRUE;
	}

	if (m_bIsScaningOn == TRUE)	    //Scan 동작 중에만 TRUE
	{
		//처음 스캔 시작시 위치를 정하기 위한 부분 // 일단은 Uni-Direction 과 동일하게
		if (m_bIsFirstScan == TRUE)
		{
			if (m_IsResetStartPos)
			{
				m_dLaserX_1st = pos_x = AdamData.m_dX_position;
				m_dLaserY_1st = pos_y = AdamData.m_dY_position;
			}
			m_bIsFirstScan = FALSE;
		}
		else
		{
			pos_x_current = pos_x = AdamData.m_dX_position - m_dLaserX_1st;	//laser interferometer x axis position data
			pos_y_current = pos_y = AdamData.m_dY_position - m_dLaserY_1st;	//laser interferometer y axis position data
		}

		if ((pos_x_current - pos_x_oldold) > 12 && pos_y < 50)
		{
			m_bCanGetData_AtYaxisDirectoin = TRUE;
		}

		if (m_nFovDataCntold == 0 && m_nFovDataCnt == 1)
		{
			strLog.Format("Begin(ScanDataReceive): Point:%d Through_Focus:%d Scan:%d", m_currentPointNum, m_currentThroughFocusNum, m_nImageScanCnt);
			SaveLogFile("ProcessLogPointMeasure", strLog);
		}
		m_nFovDataCntold = m_nFovDataCnt;


		// 수정중인 부분 3 um only
		if (m_bCanGetData_AtYaxisDirectoin == TRUE)
		{
			num = m_nFovDataCnt;

			for (int i = 0; i < KINDS_OF_ADAM_DATA; i++) // 수정필요
			{
				if (i == LASERINTERFEROMETER_X)
				{
					m_dRawImageForDisplay[i][num] = pos_x;  // TEST용 삭제해야함, Display? 
					m_dRawImage_Data3D[m_vecScanBufIndex.at(m_nImageScanCnt)][i][num] = pos_x;
				}
				else if (i == LASERINTERFEROMETER_Y)
				{
					m_dRawImageForDisplay[i][num] = pos_y; // TEST용 삭제해야함, Display? 
					m_dRawImage_Data3D[m_vecScanBufIndex.at(m_nImageScanCnt)][i][num] = pos_y;
				}
				else
				{
					m_dRawImageForDisplay[i][num] = m_dParsedData[i]; // TEST용 삭제해야함, Display? 
					m_dRawImage_Data3D[m_vecScanBufIndex.at(m_nImageScanCnt)][i][num] = m_dParsedData[i];
				}
			}

			// 필요한 건지 확인 필요
			m_dRawImageForDisplay[I0_INDEX][num] = AdamData.I0_Index;
			m_dRawImage_Data3D[m_vecScanBufIndex.at(m_nImageScanCnt)][I0_INDEX][num] = AdamData.I0_Index;


			m_nFovDataCnt++;

			if (m_nFovDataCnt % 300 == 0)
			{
				m_nFovDataCnt_Bi = m_nFovDataCnt + 300;
				SetEvent(m_hDisplayRawImage);
			}

			if (m_nFovDataCnt == 300 * 300 + 200)	// X Scan 갯수 * Y line 갯수 + a
			{
				//Scan 데이터 다 받아지면 Regrid Thread Wakeup

				//unique_lock<mutex> lock(mutexScanDataReady[m_ScanBufIndexForReceiveData.at(m_nImageScanCnt)]);
				{
					unique_lock<mutex> lock(mutexScanDataReady[m_ScanBufIndexForReceiveData.at(m_nImageScanCnt)]);
					boolScanDataReady[m_ScanBufIndexForReceiveData.at(m_nImageScanCnt)] = TRUE;
				}
				cvScanDataReady.at(m_ScanBufIndexForReceiveData.at(m_nImageScanCnt)).notify_one();
							
					
				strLog.Format("Notifiy(%d): Point:%d Through_Focus:%d Scan:%d", m_vecScanBufIndex.at(m_nImageScanCnt), m_currentPointNum, m_currentThroughFocusNum, m_nImageScanCnt);
				SaveLogFile("ProcessLogPointMeasure", strLog);

				strLog.Format("End(ScanDataReceive): Point:%d Through_Focus:%d Scan:%d", m_currentPointNum, m_currentThroughFocusNum, m_nImageScanCnt);
				SaveLogFile("ProcessLogPointMeasure", strLog);

				m_bCanGetData_AtYaxisDirectoin = FALSE;
				
				//Display 이거 씹힐때 있음, regrid에서 한번 더 호출
				SetEvent(m_hDisplayRawImage);

				SetEvent(m_hAcquisitionEnds[m_nImageScanCnt]);
				m_nImageScanCnt++;

				// Adam 종료
				if (m_nImageScanCnt == m_nEuvImage_ScanNumber)
				{
					//m_bIsScaningOn = FALSE;
					//DisplayRawImage();
					Command_ADAMStop();
					
					// Scan End 알림
					{
						unique_lock<mutex> lock(mutexAdamEnd);
						IsAdamEnd = TRUE;
					}
					cvAdamEnd.notify_one();

				}
				else
				{
					//CurrentScanNumUiUpdate(); // UI관련 함수로 UI 쓰레드와 충돌남??
				}

				m_nFovDataCnt = 0;		// 이게 들어가 줘야 다음번 스캐닝에 다시 0부터 시작하는 것 아닌가?		
			}

		}

		pos_x_oldold = pos_x_old;
		pos_y_oldold = pos_y_old;

		pos_x_old = pos_x;
		pos_y_old = pos_y;
	}

	return nRet;
}

int CADAMCtrl::ReceivedData_Average(char *lParam)   //Bi-direction용 코드 추가 KYD_2020/06/20
{
	int nRet = 0;

	Parsing_ADAM_memcpy(lParam);

	SetEvent(m_hStateOkEvent);

	CapSensorDataUiUpdate();

	return nRet;
}

int CADAMCtrl::ReceivedData_After(char *lParam, int num_packet)   //Bi-direction용 코드 추가 KYD_2020/06/20
{
	int nRet = 0;

	double cap1 = 0;
	double cap2 = 0;
	double cap3 = 0;
	double cap4 = 0;


	double LifX = 0;
	double LifY = 0;

	for (int i = 0; i < num_packet; i++)
	{
		memcpy(m_chReceivedbuf[i], lParam + HEADERSIZE + PACKETSIZE * i, sizeof(byte) * PACKETSIZE);
		Parsing_ADAM_memcpy(m_chReceivedbuf[i]);

		cap1 = cap1 + AdamData.m_dCapsensor1;
		cap2 = cap2 + AdamData.m_dCapsensor2;
		cap3 = cap3 + AdamData.m_dCapsensor3;
		cap4 = cap4 + AdamData.m_dCapsensor4;

		LifX = LifX + AdamData.m_dX_position;
		LifY = LifY + AdamData.m_dY_position;
	}


	AdamData.m_dCapsensor1 = cap1 / num_packet;
	AdamData.m_dCapsensor2 = cap2 / num_packet;
	AdamData.m_dCapsensor3 = cap3 / num_packet;
	AdamData.m_dCapsensor4 = cap4 / num_packet;
	AdamData.m_dX_position = LifX / num_packet;
	AdamData.m_dY_position = LifY / num_packet;

	m_bAverageRunAfter = FALSE;
	CapSensorDataUiUpdate();
	SetEvent(m_hAverageRun);

	return nRet;
}


int CADAMCtrl::DisplayFilteredImage(double* image)
{
	return 0;
}

//
//int CADAMCtrl::DisplayRawImage()
//{
//	return 0;
//}


//int CADAMCtrl::RawImageFileSave()
//{
//	return 0;
//}

//int CADAMCtrl::RawImageFileSave3D(int totalScanNum)
//{
//	return 0;
//}

int CADAMCtrl::OnRegrid(int ScanNum)
{
	DWORD dwResult;

	dwResult = WaitForSingleObject(m_hAcquisitionEnds[ScanNum], ACQUISITION_TIME_OUT_MS * (ScanNum + 1));

	if (dwResult == WAIT_OBJECT_0)
	{
		if (!m_bAbort)
		{			
			DisplayRawImage(); //이벤트 씹힐때 있어서  추가. 이렇게 하면 연산 딜레이 있음..

			if (m_bIsUseInterpolation && !m_bSaveEveryScanRawImage)
			{
				CString strLog;

				strLog.Format("Begin(Regrid): scanBufIndex:%d", ScanNum + 1);
				SaveLogFile("ProcessLogAdam ", strLog);

				Regrid(ScanNum, ScanNum);

				strLog.Format("End(Regrid): scanBufIndex:%d", ScanNum + 1);
				SaveLogFile("ProcessLogAdam ", strLog);
			}
			// Raw Image 저장...
			if (m_bSaveEveryScanRawImage)
			{
				RawImageFileSave3D(ScanNum);
			}
		}
		else
		{
			//강제종료
			int test = 0;
		}
	}
	else
	{
		//Error Timeout or 다른 에러

		Command_ADAMStop();
		m_bAbort = TRUE;
		for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
		{
			SetEvent(m_hAcquisitionEnds[i]);
		}

		int test = 0;
	}

	SetEvent(m_hRegridEnds[ScanNum]);

	return 0;
}

int CADAMCtrl::OnAverage()
{
	DWORD dwResult;

	for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
	{
		dwResult = WaitForSingleObject(m_hRegridEnds[i], INFINITE); //ACQUISITION_TIME_OUT_MS

		if (dwResult == WAIT_OBJECT_0)
		{
			if (!m_bAbort)
			{
				//정상진행
				//if (m_nEuvImage_Fov < 4000 && !m_bSaveEveryScanRawImage)
				if (m_bIsUseInterpolation && !m_bSaveEveryScanRawImage)
				{
					CString strLog;

					strLog.Format("Begin(Average): scanBufIndex:%d", i + 1);
					SaveLogFile("ProcessLogAdam ", strLog);

					Averaging(i, i, m_nEuvImage_ScanNumber, 0);

					strLog.Format("End(Average): scanBufIndex:%d", i + 1);
					SaveLogFile("ProcessLogAdam ", strLog);
				}

			}
			else
			{
				//강제종료				
				int test = 0;
			}
		}
		else
		{
			//Error Timeout or 다른 에러
			int test = 0;
		}
	}

	SetEvent(m_hAverageEnd);
	return 0;
}

int CADAMCtrl::OnFilter()
{
	DWORD dwResult;

	dwResult = WaitForSingleObject(m_hAverageEnd, INFINITE); //ACQUISITION_TIME_OUT_MS

	if (dwResult == WAIT_OBJECT_0)
	{
		if (!m_bAbort)
		{
			//정상진행						
			//if (m_nEuvImage_Fov < 4000 && !m_bSaveEveryScanRawImage)
			if (m_bIsUseInterpolation && !m_bSaveEveryScanRawImage)
			{
				CString strLog;

				strLog.Format("Begin(Filter)");
				SaveLogFile("ProcessLogAdam ", strLog);

				Filtering(0);

				strLog.Format("End(Filter)");
				SaveLogFile("ProcessLogAdam ", strLog);
			}
		}
		else
		{
			//강제종료			
			int test = 0;
		}
	}
	else
	{
		//Error Timeout or 다른 에러
		int test = 0;
	}

	SetEvent(m_hFilterEnd);

	return 0;
}

int CADAMCtrl::OnWaitScanEnd()
{
	DWORD dwResult;
	dwResult = WaitForSingleObject(m_hFilterEnd, INFINITE);// ACQUISITION_TIME_OUT_MS * m_nEuvImage_ScanNumber

	m_bIsScaningOn = FALSE;
	AdamUIControl(TRUE);
	if (dwResult == WAIT_OBJECT_0)
	{
		if (!m_bAbort)
		{
			//정상진행
			//if (m_nEuvImage_Fov < 4000 && !m_bSaveEveryScanRawImage)
			if (m_bIsUseInterpolation && !m_bSaveEveryScanRawImage)
			{
				DisplayFilteredImage(m_dFilteredImage_Data3D[0][D1_REGRID]);
			}
			else //10um영상 마지막 update
			{
				DisplayRawImage();
			}
			if (m_bSaveEveryScanRawImage)
			{
				DisplayRawImage();
			}
		}
		else
		{
			//강제종료
			int test = 0;
		}
	}
	else
	{
		//Error Timeout or 다른 에러
		int test = 0;
	}


	return 0;
}


void CADAMCtrl::InterpolationCubic1D(double *x, double *y, int length, double *x_out, double *y_out, int lenth_out)
{
	//Cubic Interpolation

	double *a = new double[length];
	double *b = new double[length];
	double *c = new double[length];
	double *d = new double[length];
	double *alpha = new double[length];

	for (int i = 0; i < length; i++) { a[i] = y[i]; }// n EA

	double *h = new double[length]; //step 1

	for (int i = 0; i < length; i++) // n-1 EA
	{
		h[i] = x[i + 1] - x[i];
	}

	for (int i = 1; i < length - 1; i++)  //step 2 //n-2 EA
	{
		alpha[i] = 3 * (a[i + 1] - a[i]) / h[i] - 3 * (a[i] - a[i - 1]) / h[i - 1];
	}
	double *l = new double[length];
	double *mu = new double[length];
	double *z = new double[length];

	l[0] = 1;
	mu[0] = 0;
	z[0] = 0;
	for (int i = 1; i < length - 1; i++) //n-2 EA
	{
		l[i] = 2 * (x[i + 1] - x[i - 1]) - h[i - 1] * mu[i - 1];
		mu[i] = h[i] / l[i];
		z[i] = (alpha[i] - h[i - 1] * z[i - 1]) / l[i];
	}

	l[length - 1] = 1;
	z[length - 1] = 0;
	c[length - 1] = 0;

	for (int i = length - 2; i >= 0; i--) //n-1 EA
	{
		c[i] = z[i] - mu[i] * c[i + 1];
		b[i] = (a[i + 1] - a[i]) / h[i] - h[i] * (c[i + 1] + 2 * c[i]) / 3;
		d[i] = (c[i + 1] - c[i]) / (3 * h[i]);
	}

	for (int i = 0; i < lenth_out; i++) // n_out EA
	{
		if (x_out[i] < x[0])
		{
			y_out[i] = y[0];
		}
		else if (x_out[i] > x[length - 1])
		{
			y_out[i] = y[length - 1]; // length -> length - 1 
		}
		else
		{
			int dd = 0;
			int cc;
			for (cc = 1; cc < length; cc++)
			{
				if (x_out[i] < x[cc]) { dd = cc - 1; break; }
			} //위치 판별

			y_out[i] = a[dd] + b[dd] * (x_out[i] - x[dd]) + c[dd] * (x_out[i] - x[dd])*(x_out[i] - x[dd]) + d[dd] * (x_out[i] - x[dd])*(x_out[i] - x[dd])*(x_out[i] - x[dd]); // n_out EA		

			if (y_out[i] > 1000)
			{
				int test = 0;
			}
		}
	}

	delete[] a;
	delete[] b;
	delete[] c;
	delete[] d;
	delete[] alpha;

	delete[] l;
	delete[] z;
	delete[] mu;
	delete[] h;

	return;
}


void CADAMCtrl::InterpolationPchip1D(double *x, double *y, int length, double *x_out, double *y_out, int lenth_out)
{
	int j = 0;
	double tempx = 0, tempy = 0;

	//sorting
	for (int i = 1; i < length; i++)
	{
		j = i - 1;
			tempx = x[i];
		tempy = y[i];
		while (x[j] > tempx && j >= 0)
		{
			x[j + 1] = x[j];
			y[j + 1] = y[j];
			j = j - 1;
			x[j + 1] = tempx;
			y[j + 1] = tempy;
		}
	}

	//sort Test
	//for (int i = 0; i < length - 1; i++)
	//{
	//	if (x[i] > x[i + 1])
	//	{
	//		int test = 0;
	//	}
	//}

	double* dk = new double[length];
	double* mk = new double[length];

	for (int i = 0; i < length - 1; i++)
	{
		dk[i] = (y[i + 1] - y[i]) / (x[i + 1] - x[i]);
	}
	mk[0] = dk[0];
	mk[length - 1] = dk[length - 2];

	for (int i = 1; i < length - 1; i++)
	{
		if (dk[i - 1] * dk[i] < 0)
		{
			mk[i] = 0;
		}
		else
		{
			mk[i] = (dk[i - 1] + dk[i]) / 2;
		}
	}
	for (int k = 0; k < length - 2; k++) {

		if (fabs(dk[k]) <= 0) { mk[k] = mk[k + 1] = 0; }
		else
		{
			double ak = mk[k] / dk[k];
			double bk = mk[k + 1] / dk[k];
			if (ak*ak + bk * bk > 9)
			{
				mk[k] = 3 * ak*dk[k] / (sqrt(ak*ak + bk * bk));
				mk[k + 1] = 3 * bk*dk[k] / (sqrt(ak*ak + bk * bk));
			}
		}
	}
	for (int i = 0; i < lenth_out; i++) {

		if (x_out[i] < x[0])
		{
			y_out[i] = y[0];
		}
		else if (x_out[i] > x[length - 1])
		{
			y_out[i] = y[length - 1]; // length -> length - 1 
		}
		else
		{
			int dd = 0;
			for (int cc = 1; cc < length; cc++)
			{
				if (x_out[i] < x[cc]) { dd = cc - 1; break; }
			}// 위치 판별
			double t = (x_out[i] - x[dd]) / (x[dd + 1] - x[dd]);

			y_out[i] = y[dd] * (2 * t*t*t - 3 * t*t + 1) + (x[dd + 1] - x[dd]) * mk[dd] * (t*(1 - t)*(1 - t)) + y[dd + 1] * (t*t*(3 - 2 * t)) + (x[dd + 1] - x[dd]) * mk[dd + 1] * (t*t*(t - 1));
		}
	}

	delete[] dk;
	delete[] mk;

	return;
}

void CADAMCtrl::ScanDataReceiveThreadFunForTest(int totalScanNum, vector <int> scanBufIndex, int pointNum, int througFocusNum)
{
	CString strLog;

	for (int scanNum = 0; scanNum < totalScanNum; scanNum++)
	{
		if (!m_bAbort)
		{
			strLog.Format("Begin(ScanDataReceive): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);

			this_thread::sleep_for(chrono::milliseconds(1000));

			//Scan 데이터 다 받아지면 Regrid Thread Wakeup
			{
				unique_lock<mutex> lock(mutexScanDataReady[scanBufIndex.at(scanNum)]);
				boolScanDataReady[scanBufIndex.at(scanNum)] = TRUE;
			}
			cvScanDataReady.at(scanBufIndex.at(scanNum)).notify_one();

			strLog.Format("Notifiy(%d): Point:%d Through_Focus:%d Scan:%d", scanBufIndex.at(scanNum), pointNum, througFocusNum, scanNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);

			strLog.Format("End(ScanDataReceive): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);
		}
	}

	//chrono::time_point<chrono::high_resolution_clock> start = chrono::high_resolution_clock::now();
	//chrono::time_point<chrono::high_resolution_clock> end = chrono::high_resolution_clock::now();
	//chrono::milliseconds duration2 = chrono::duration_cast<chrono::milliseconds>(end - start);

	//chrono::duration<double, milli> test = end - start;
	//auto diff = end - start;
	//chrono::milliseconds duration1 = chrono::duration_cast<chrono::milliseconds>(diff);


	//시간 체크
	//chrono::time_point<chrono::high_resolution_clock> start = chrono::high_resolution_clock::now();
	//chrono::time_point<chrono::high_resolution_clock> end = chrono::high_resolution_clock::now();
	//chrono::milliseconds duration2 = chrono::duration_cast<chrono::milliseconds>(end - start);
}

void CADAMCtrl::RegridThreadFunForTest(int scanBufIndex, int scanNum, int pointNum, int througFocusNum)
{
	CString strLog;

	//Data 받아질때 까지 기다림...현재는 event로 하는데.. 어떻게 구현??			
	strLog.Format("Begin(WakeUp %d): Point:%d Through_Focus:%d Scan:%d", scanBufIndex, pointNum, througFocusNum, scanNum);
	SaveLogFile("ProcessLogPointMeasure", strLog);

	unique_lock<mutex> lock(mutexScanDataReady.at(scanBufIndex));
	//cvScanDataReady.at(scanBufIndex).wait(lock);	
	cvScanDataReady.at(scanBufIndex).wait(lock, [this, scanBufIndex] { return boolScanDataReady[scanBufIndex]; });   // (4)
	boolScanDataReady[scanBufIndex] = FALSE; // 있어야 하나? 
	//boolScanDataReady[scanBufIndex]

	strLog.Format("End(WakeUp %d): Point:%d Through_Focus:%d Scan:%d", scanBufIndex, pointNum, througFocusNum, scanNum);
	SaveLogFile("ProcessLogPointMeasure", strLog);

	if (!m_bAbort)
	{
		strLog.Format("Begin(Regrid): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
		SaveLogFile("ProcessLogPointMeasure", strLog);

		this_thread::sleep_for(chrono::milliseconds(2000));

		strLog.Format("End(Regrid): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
		SaveLogFile("ProcessLogPointMeasure", strLog);
	}
	else
	{
		int test = 0;
		//강제종료
	}
}



void CADAMCtrl::MemoryAcquireAndInitForPointMeasure(int totalScanNum, int &pointMeasureData, vector<int> &scanData)
{
	CString strLog;
	//1. Point Measure 메모리 요청	
	pointMeasureData = MemoryPoolPointMeasure.Acquire();

	//2. Scan 메모리 요청		
	for (int i = 0; i < totalScanNum; i++)
	{
		int scanMemoryIndex = MemoryPoolScan.Acquire();
		boolScanDataReady[scanMemoryIndex] = FALSE; //
		scanData.push_back(scanMemoryIndex);
	}

	{
		unique_lock<mutex> lock(mutexAdamStart);
		IsAdamStart = FALSE; 
	}

	{
		unique_lock<mutex> lock(mutexAdamEnd);
		IsAdamEnd = FALSE;
	}
	
	//m_bIsScaningOn = FALSE; //이게 여기가 맞음? 
	
}


void CADAMCtrl::PointMeasureThreadFunForTest(int pointMeasureDataIndex, vector<int> scanBufIndex, int pointNum, int totalPointNum, int totalScanNum, int througFocusNum, int totalThroughFocusNum)
{
	CString strLog;

	CString exceptionMsg;
	try
	{
		strLog.Format("Begin(PointMeasureThreadFun): Point:%d Through_Focus:%d", pointNum, througFocusNum);
		SaveLogFile("ProcessLogPointMeasure", strLog);

		// 1. 초기화
		// UI disable
		//AdamUIControl(FALSE); //이거 있으면 동작 안함 순수가상함수라? 쓰레드 발동이 늦음..

		//deque<std::thread> RegridThreadQue;
		m_bIsScaningOn = TRUE; //PointMeasureThreadFunForTest

		vector<thread> RegridThread;
		RegridThread.reserve(totalScanNum);

		if (!m_bAbort)
		{
			strLog.Format("Begin(RegridThread): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);

			//2. RegridThread 시작
			for (int i = 0; i < totalScanNum; i++)
			{
				RegridThread.push_back(thread([=] {RegridThreadFunForTest(scanBufIndex.at(i), i, pointNum, totalThroughFocusNum); }));
				//RegridThreadQue.emplace_back([=] {RegridThreadFunForTest(scanBufIndex.at(i), i, pointNum, totalThroughFocusNum); });
			}

			strLog.Format("End(RegridThread): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);
		}


		if (!m_bAbort)
		{

			SetScanBufIndexForReceiveData(scanBufIndex);

			strLog.Format("Begin(AdamRun): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);

			//3. Adam Start
			this_thread::sleep_for(chrono::milliseconds(10));// Adam Run() 

			strLog.Format("End(AdamRun): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);
		}


		//4. 각 스캔별 Regrid 종료 기다린후 Averaging 수행, 마지막 regrid  완료되면 low pass filterfing 수행 ( PostProcessing)
		for (int scanNum = 0; scanNum < totalScanNum; scanNum++)
		{
			//thread& pointMeasureThread = RegridThreadQue.front();
			//RegridThreadQue.pop_front();
			//pointMeasureThread.join();

			//if (!m_bAbort)
			if (RegridThread.size() > scanNum)
			{
				RegridThread.at(scanNum).join(); //정상종료 되야함
			}

			if (!m_bAbort)
			{
				strLog.Format("Begin(Averageing): Point:%d  Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
				SaveLogFile("ProcessLogPointMeasure", strLog);

				//5. Averageing
				this_thread::sleep_for(chrono::milliseconds(500));

				strLog.Format("End(Averageing): Point:%d  Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
				SaveLogFile("ProcessLogPointMeasure", strLog);
			}

			//6. Scan 메모리 해제
			MemoryPoolScan.Release(scanBufIndex.at(scanNum));
		}

		//7. ScanEnd
		m_bIsScaningOn = FALSE;

		if (!m_bAbort)
		{
			strLog.Format("Begin(Filtering): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);

			//8. Low passfiltering
			this_thread::sleep_for(chrono::milliseconds(500));

			strLog.Format("End(Filtering): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);
		}



		//9. Display Image()


		if (!m_bAbort)
		{
			strLog.Format("Begin(SaveImage): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);

			//10. 이미지 저장	
			this_thread::sleep_for(chrono::milliseconds(2000));

			strLog.Format("End(SaveImage): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);
		}

		//11. Point Measure 메모리 해제
		MemoryPoolPointMeasure.Release(pointMeasureDataIndex);

		//12. 모든 측정 Point 완료, 이거 보장됨? 
		//if (!m_bAbort) 이떄 순차적으로 종료 안될수 있음...
		if ((pointNum == totalPointNum - 1 && througFocusNum == totalThroughFocusNum - 1) || m_bAbort)
		{
			m_bIsScaningOn = FALSE;
			m_IsMeasureComplete = TRUE;
		}

		strLog.Format("End(PointMeasureThreadFun): Point:%d Through_Focus:%d", pointNum, througFocusNum);
		SaveLogFile("ProcessLogPointMeasure", strLog);
	}
	catch (CString exMsg)
	{
		//ResetMemoryPool		
		for (int scanNum = 0; scanNum < totalScanNum; scanNum++)
		{
			MemoryPoolScan.Release(scanBufIndex.at(scanNum));
		}

		MemoryPoolPointMeasure.Release(pointMeasureDataIndex);

		//ADAMRunEnd();
		AfxMessageBox("Exception:PointMeasureThreadFun(): " + exMsg);
	}

}


void CADAMCtrl::MemoryPoolInitialization()
{

	MemoryPoolScan.objectName = "MemoryPoolScan";
	MemoryPoolPointMeasure.objectName = "MemoryPoolPointMeasure";

	for (int i = 0; i < MaxScanMemory; i++)
	{
		MemoryPoolScan.AddToMemoryPool(i);
		cvScanDataReady.emplace_back();
		mutexScanDataReady.emplace_back();
		boolScanDataReady.push_back(FALSE);

	}
	for (int i = 0; i < MaxPointMeasureMemory; i++)
	{
		MemoryPoolPointMeasure.AddToMemoryPool(i);
	}
}


void CADAMCtrl::PointMeasureThreadFun(int pointMeasureDataIndex, vector<int> scanBufIndex, int pointNum, int totalPointNum, int totalScanNum, int througFocusNum, int totalThroughFocusNum, BOOL isSaveFile)
{
	CString strLog;
	CString exceptionMsg;
	try
	{
		strLog.Format("Begin(PointMeasureThreadFun): Point:%d Through_Focus:%d", pointNum, througFocusNum);
		SaveLogFile("ProcessLogPointMeasure", strLog);

		m_currentPointNum = pointNum;
		m_currentThroughFocusNum = througFocusNum;

		// 1. 초기화
		if (!m_bConnected)
		{
			//exceptionMsg.Format("Adam is not Connected");
			throw exceptionMsg;
		}
		// UI disable
		//AdamUIControl(FALSE); //이거 있으면 동작안함 순수가상함수 일때, UI thread 에 sleep 있으면 지연 됨


		SetPointMeasureBufIndex(pointMeasureDataIndex);
		SetScanBufIndexVector(scanBufIndex);

		//변수 초기화 
		m_bIsScaningOn = TRUE; //PointMeasureThreadFun
		//m_bAbort = FALSE;  // 중요 제거해야함 ihlee
		m_bIsFirstScan = FALSE;
		m_nImageScanCnt = 0;		

		m_nInterpolationCnt = 0;
		m_nFovDataCnt = 0;

		m_bCanGetData_AtUniDirection = FALSE;
		m_bCanGetData_AtYaxisDirectoin = FALSE;

		// Bi direciton 변수 초기화
		m_bIsScanDirectionForward = FALSE;
		m_nLineNumCnt = 0;
		m_bIsLineScanCompleted = FALSE;
		m_bCanGetData_AtXaxisDirectoin = FALSE;
		m_bIsAcquisitionStart = FALSE;

		m_nReverseDataCnt = 0;
		m_nFovDataCnt_Bi = 0;

		pos_x_current = 0;
		pos_x_old = 0;
		pos_x_oldold = 0;
		pos_y_current = 0;
		pos_y_old = 0;
		pos_y_oldold = 0;

		//Diaplay interpolation초기화
		previousLintCountInterpolation = 0;

		//MutexLock(m_hMutex); //동작 안함  한 함수 내에 있어야 함? 

		if (m_nEuvImage_Fov == 2000)
		{
			//m_dAcelDistance_X = 300.000;			// 숫자 1 이 1 nm 임... ADAM에서 넘어 오는 것도 나노 단위 기준으로 세팅 필요, 정수배로 맞아 떨어져서 pixel 위치가 바뀌는 것을 방지하기위해 넣었음 (m_nEuvImage_ScanGrid/2)
			//m_dAcelDistance_Y = 300.000;			// +(m_nEuvImage_ScanGrid / 2);			// 향후 FOV 크기 맞춰서 조정 필요

			m_dAcelDistance_X = 200.000;			// 숫자 1 이 1 nm 임... ADAM에서 넘어 오는 것도 나노 단위 기준으로 세팅 필요, 정수배로 맞아 떨어져서 pixel 위치가 바뀌는 것을 방지하기위해 넣었음 (m_nEuvImage_ScanGrid/2)
			m_dAcelDistance_Y = 200.000;			// +(m_nEuvImage_ScanGrid / 2);			// 향후 FOV 크기 맞춰서 조정 필요
		}
		else if (m_nEuvImage_Fov == 10000)
		{
			m_dAcelDistance_X = 300.000;			// 
			m_dAcelDistance_Y = 200.000;			// 
		}
		else if (m_nEuvImage_Fov == 3000 && m_nEuvImage_ScanGrid == 10 && m_IsUse3umFOV) // 임시 ihlee 2021.03.17
		{
			m_dAcelDistance_X = 0.0;			// 
			m_dAcelDistance_Y = 0.0;			// 
		}
		else
		{
			m_dAcelDistance_X = 200.000;			// 
			m_dAcelDistance_Y = 200.000;			// 
		}

		// Pixel size를 정의한 부분임
		m_nRawImage_PixelWidth = m_nEuvImage_Fov/* * 1000 */ / m_nEuvImage_ScanGrid;
		m_nRawImage_PixelHeight = m_nEuvImage_Fov/* * 1000 */ / m_nEuvImage_ScanGrid;

		m_nReGridImage_PixelWidth = m_nEuvImage_Fov/* * 1000 */ / m_nEUVImage_InterpolationGrid;
		m_nReGridImage_PixelHeight = m_nEuvImage_Fov/* * 1000 */ / m_nEUVImage_InterpolationGrid;


		if (m_nRawImage_PixelWidth > ADAM_MAX_WIDTH || m_nRawImage_PixelHeight > ADAM_MAX_HEIGHT)
		{
			exceptionMsg.Format("Exceed memory, Check FOV and ScanGrid");
			throw exceptionMsg;
		}

		if (m_nEuvImage_Fov >= m_HighFovCriteria && m_bIsUseHighFovInterpolation == FALSE)
		{
			m_bIsUseInterpolation = FALSE;
		}
		else
		{
			m_bIsUseInterpolation = TRUE;
		}

		if (m_bIsUseInterpolation)
		{
			if (m_nReGridImage_PixelWidth > ADAM_MAX_WIDTH_REGRID || m_nReGridImage_PixelHeight > ADAM_MAX_HEIGHT_REGRID)
			{

			}
		}

		//버퍼 초기화 
		m_nOriignaDataI0_Index = 0;
		AdamDataMemReset(pointMeasureDataIndex, scanBufIndex);


		vector<thread> RegridThread;
		if (!m_bAbort)
		{
			strLog.Format("Begin(RegridThread): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);

			//2. RegridThread 시작
			RegridThread.reserve(totalScanNum);
			for (int i = 0; i < totalScanNum; i++)
			{
				RegridThread.push_back(thread([=] {RegridThreadFun(scanBufIndex.at(i), i, pointNum, througFocusNum); }));
			}

			strLog.Format("End(RegridThread): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);
		}

		if (!m_bAbort)
		{
			strLog.Format("Begin(AdamRun): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);

			//3. Adam Start
			SetScanBufIndexForReceiveData(scanBufIndex);
			Command_ADAMStart();
			
			// Adam Start 알림 to scanStage
			{
				unique_lock<mutex> lock(mutexAdamStart);
				IsAdamStart = TRUE;
			}
			cvAdamStart.notify_one();
	
			//CurrentScanNumUiUpdate(); // UI관련 함수로 UI 쓰레드와 충돌남 이동
			//InitAdamDlgForScan(); //UI 관련 함수 이동

			strLog.Format("End(AdamRun): Point:%d Through_Focus:%d", pointNum, througFocusNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);
		}

		//4. 각 스캔별 Regrid 종료 기다린후 Averaging 수행, 마지막 regrid  완료되면 low pass filterfing 수행 ( PostProcessing)
		for (int scanNum = 0; scanNum < totalScanNum; scanNum++)
		{
			/*if (m_bAbort)
			{
				int test = 0;
				exceptionMsg.Format("Aborted");
				throw exceptionMsg;
			}*/

			//일단 실행된것은 종료 기다림


			if (RegridThread.size() > scanNum)
			{
				BOOL test = RegridThread.at(scanNum).joinable();
				RegridThread.at(scanNum).join(); //정상종료 되야함
			}

			if (!m_bAbort)
			{
				if (m_bIsUseInterpolation && !m_bSaveEveryScanRawImage)
				{
					strLog.Format("Begin(Averageing): Point:%d  Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
					SaveLogFile("ProcessLogPointMeasure", strLog);

					//5. Averageing
					Averaging(scanBufIndex.at(scanNum), scanNum, totalScanNum, pointMeasureDataIndex);

					strLog.Format("End(Averageing): Point:%d  Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
					SaveLogFile("ProcessLogPointMeasure", strLog);
				}
			}
			//6. Scan 메모리 해제
			MemoryPoolScan.Release(scanBufIndex.at(scanNum));
		}
		//7. ScanEnd
		m_bIsScaningOn = FALSE;


		if (!m_bAbort)
		{
			if (m_bIsUseInterpolation && !m_bSaveEveryScanRawImage)
			{
				strLog.Format("Begin(Filtering): Point:%d Through_Focus:%d", pointNum, througFocusNum);
				SaveLogFile("ProcessLogPointMeasure", strLog);

				//8. Low passfiltering
				Filtering(pointMeasureDataIndex);

				strLog.Format("End(Filtering): Point:%d Through_Focus:%d", pointNum, througFocusNum);
				SaveLogFile("ProcessLogPointMeasure", strLog);
			}
		}

		if (!m_bAbort)
		{

			if (m_bIsUseInterpolation && !m_bSaveEveryScanRawImage)
			{
				//9. Display Image()
				DisplayFilteredImage(m_dFilteredImage_Data3D[pointMeasureDataIndex][D1_REGRID]);
			}
			else //10um영상 마지막 update ?? 삭제 가능? 
			{
				DisplayRawImage();
			}

			if (m_bSaveEveryScanRawImage)
			{
				DisplayRawImage();
			}
		}

		/*	if (m_bAbort)
		{
			int test = 0;
			exceptionMsg.Format("Aborted");
			throw exceptionMsg;
		}*/
		if (!m_bAbort)
		{
			if (isSaveFile)
			{
				strLog.Format("Begin(SaveImage): Point:%d Through_Focus:%d", pointNum, througFocusNum);
				SaveLogFile("ProcessLogPointMeasure", strLog);

				//10. 이미지 저장	
				double capPos = 0;
				int removeEdge = 50;
				FilteredImageFileSave_Resize(removeEdge, pointNum, m_dFilteredImage_Data3D[pointMeasureDataIndex], capPos, througFocusNum);

				strLog.Format("End(SaveImage): Point:%d Through_Focus:%d", pointNum, througFocusNum);
				SaveLogFile("ProcessLogPointMeasure", strLog);
			}
		}

		//11. Point Measure 메모리 해제
		MemoryPoolPointMeasure.Release(pointMeasureDataIndex);

		//12. 모든 측정 Point 완료, 이거 보장됨?? 순차로 실행된다는 가정. Que 같은거 하나 만들어서...
		if (pointNum == totalPointNum - 1 && througFocusNum == totalThroughFocusNum - 1)
		{
			m_IsMeasureComplete = TRUE;
		}

		strLog.Format("End(PointMeasureThreadFun): Point:%d Through_Focus:%d", pointNum, througFocusNum);
		SaveLogFile("ProcessLogPointMeasure", strLog);

	}
	catch (CString exMsg)
	{
		//ResetMemoryPool
		for (int scanNum = 0; scanNum < totalScanNum; scanNum++)
		{
			MemoryPoolScan.Release(scanBufIndex.at(scanNum));
		}

		m_bIsScaningOn = FALSE;
		AfxMessageBox("Exception:PointMeasureThreadFun(): " + exMsg);
	}

}

void CADAMCtrl::RegridThreadFun(int scanBufIndex, int scanNum, int pointNum, int througFocusNum)
{

	CString strLog;

	//Data 받아질때 까지 기다림.
	strLog.Format("Begin(WakeUp %d): Point:%d Through_Focus:%d Scan:%d", scanBufIndex, pointNum, througFocusNum, scanNum);
	SaveLogFile("ProcessLogPointMeasure", strLog);

	{
		unique_lock<mutex> lock(mutexScanDataReady.at(scanBufIndex));
		//cvScanDataReady.at(scanBufIndex).wait(lock);
		cvScanDataReady.at(scanBufIndex).wait(lock, [scanBufIndex, this] { return boolScanDataReady[scanBufIndex]; });		
		boolScanDataReady[scanBufIndex] = FALSE; // 있어야 하나? 
	}

	strLog.Format("End(WakeUp %d): Point:%d Through_Focus:%d Scan:%d", scanBufIndex, pointNum, througFocusNum, scanNum);
	SaveLogFile("ProcessLogPointMeasure", strLog);

	if (!m_bAbort)
	{
		strLog.Format("Begin(DisplayRawImage): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
		SaveLogFile("ProcessLogPointMeasure", strLog);

		DisplayRawImage();	//이벤스 씹힐때 있어서  추가. 이렇게 하면 연산 딜레이 있음..

		strLog.Format("End(DisplayRawImage): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
		SaveLogFile("ProcessLogPointMeasure", strLog);


		if (m_bIsUseInterpolation && !m_bSaveEveryScanRawImage)
		{
			strLog.Format("Begin(Regrid): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);

			Regrid(scanBufIndex, scanNum, pointNum);

			strLog.Format("End(Regrid): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
			SaveLogFile("ProcessLogPointMeasure", strLog);
		}
		// Raw Image 저장...
		if (m_bSaveEveryScanRawImage)
		{
			//strLog.Format("Begin(RawImageFileSave3D): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
			//SaveLogFile("ProcessLogPointMeasure", strLog);

			RawImageFileSave3D(scanBufIndex);

			//strLog.Format("Begin(RawImageFileSave3D): Point:%d Through_Focus:%d Scan:%d", pointNum, througFocusNum, scanNum);
			//SaveLogFile("ProcessLogPointMeasure", strLog);
		}
	}
	else
	{
		int test = 0;
		//강제종료
	}

	int test = 0;

}



//이거 괜찮을까?
void CADAMCtrl::SetScanBufIndexForReceiveData(vector <int> _ScanBufIndexForReceiveData)
{
	m_ScanBufIndexForReceiveData = _ScanBufIndexForReceiveData;
}


void CADAMCtrl::FilteredImageFileSave_Resize(int removeEdge, int nDefectNo, double **ImageBuffer, double capPos, int capNum, int throughFocusNum, double focusPos)
{
	CString m_strFilteredImageFileName;

	// Interpolated image pixel number 구하는 부분
	double FOV = m_nEuvImage_Fov;// *1000;										//field of view
	double Re_EUV_FOV = double(m_nEuvImage_Fov / 1000.) - 0.2;					// 0.2 -> 좌우로 100 nm 짤라내는거 하드코딩 수정 필요 YD


	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step
	int pixelsize = int(FOV / grid_step);

	int i = 0;
	int k = 0;
	int pixelsize_R = pixelsize - removeEdge*2;

	//1.저장할 폴더 날짜별로 생성
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	CString dir;
	dir = IMAGEFILE_PATH;
	if (!IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}
	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	dir += "\\";
	dir += strDate;
	if (!IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}

	CString strCap;

	switch (capNum)
	{
	case CAP_SENSOR1:
		strCap ="Cap1";
		break;
	case CAP_SENSOR2:
		strCap = "Cap2";
		break;
	case CAP_SENSOR3:
		strCap = "Cap3";
		break;
	case CAP_SENSOR4:
		strCap = "Cap4";
		break;
	default:	
		strCap = "CapN";
		break;
	}

	double refCapos = capPos - focusPos;
	m_strFilteredImageFileName.Format(_T("%s\\_FilteredImageData_No_%03d_%0.1f um_Grid %d nm_Focus  %0.2f um_%s %0.2f um_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, focusPos, strCap, refCapos, year, month, day, hour, minute, second);
	
	//old 버전	
	FILE *fp;
	errno_t err;
	err = fopen_s(&fp, m_strFilteredImageFileName, "w");

	fprintf(fp, "[Header]\n");
	fprintf(fp, "FILE TYPE: Filtered: (2)\n");
	fprintf(fp, "%d\n", pixelsize_R);
	fprintf(fp, "%d\n", pixelsize_R);
	fprintf(fp, "[Image]\n");

	for (i = removeEdge; i < pixelsize_R + removeEdge; i++)		// 수정필요... KYD
	{
		for (k = removeEdge; k < pixelsize_R + removeEdge; k++)
		{
			//fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D1_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k]);
			fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D1_REGRID][i*pixelsize + k], 0);
		}
	}

	fclose(fp);



	// old name
	//strCap
	//m_strFilteredImageFileName.Format(_T("%s\\FilteredImageData_No_%03d_%0.1f um_Grid %d nm_Cap1Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, capPos, year, month, day, hour, minute, second);
		m_strFilteredImageFileName.Format(_T("%s\\FilteredImageData_No_%03d_%0.1f um_Grid %d nm_%s %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, strCap, capPos, year, month, day, hour, minute, second);
	//FILE *fp;	
	err = fopen_s(&fp, m_strFilteredImageFileName, "w");

	fprintf(fp, "[Header]\n");
	fprintf(fp, "FILE TYPE: Filtered: (2)\n");
	fprintf(fp, "%d\n", pixelsize_R);
	fprintf(fp, "%d\n", pixelsize_R);
	fprintf(fp, "[Image]\n");

	for (i = removeEdge; i < pixelsize_R + removeEdge; i++)		// 수정필요... KYD
	{
		for (k = removeEdge; k < pixelsize_R + removeEdge; k++)
		{
			//fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D1_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k]);
			fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D1_REGRID][i*pixelsize + k], 0);
		}
	}

	fclose(fp);


	


}

#if FALSE
void CADAMCtrl::FilteredImageFileSave_Resize(int removeEdge, int nDefectNo, double **ImageBuffer, double capPos, int throughFocusNum)
{
	CString m_strFilteredImageFileName;
	CString m_strFilteredImageFileName_Nor_I0;
	CString m_strFilteredImageFileName_Nor_I0_Filter;
	//CString m_strFilteredImageFileName_IO_Normalized;

	// Interpolated image pixel number 구하는 부분
	double FOV = m_nEuvImage_Fov;// *1000;										//field of view
	double Re_EUV_FOV = double(m_nEuvImage_Fov / 1000.) - 0.1;


	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step
	int pixelsize = int(FOV / grid_step);

	int i = 0;
	int k = 0;
	int pixelsize_R = pixelsize - removeEdge;

	//1.저장할 폴더 날짜별로 생성
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	CString dir;
	dir = IMAGEFILE_PATH;
	if (!IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}
	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	dir += "\\";
	dir += strDate;
	if (!IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}

	m_strFilteredImageFileName.Format(_T("%s\\FilteredImageData_No_%03d_%0.1f um_Grid %d nm_Cap1Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, capPos, year, month, day, hour, minute, second);
	m_strFilteredImageFileName_Nor_I0.Format(_T("%s\\FilteredImageData_I0_Nor1_No_%03d_%0.1f um_Grid %d nm_Cap1Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, capPos, year, month, day, hour, minute, second);
	m_strFilteredImageFileName_Nor_I0_Filter.Format(_T("%s\\FilteredImageData_I0_Nor2_No_%03d_%0.1f um_Grid %d nm_Cap1Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, capPos, year, month, day, hour, minute, second);

	FILE *fp;
	errno_t err;
	err = fopen_s(&fp, m_strFilteredImageFileName, "w");

	fprintf(fp, "[Header]\n");
	fprintf(fp, "FILE TYPE: Filtered: (2)\n");
	fprintf(fp, "%d\n", pixelsize_R);
	fprintf(fp, "%d\n", pixelsize_R);
	fprintf(fp, "[Image]\n");

	for (i = removeEdge; i < pixelsize; i++)		// 수정필요... KYD
	{
		for (k = 0; k < pixelsize_R; k++)
		{
			//fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D1_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k]);
			fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D1_REGRID][i*pixelsize + k], 0);
		}
	}

	fclose(fp);
	////// I0 Normalized Image Save

	FILE *fp_I0;
	FILE *fp_I0_Average;
	if (bSaveNormalizedImage)
	{

		err = fopen_s(&fp_I0, m_strFilteredImageFileName_Nor_I0, "w");

		fprintf(fp_I0, "[Header]\n");
		fprintf(fp_I0, "FILE TYPE: Filtered: (2)\n");
		fprintf(fp_I0, "%d\n", pixelsize_R);
		fprintf(fp_I0, "%d\n", pixelsize_R);
		fprintf(fp_I0, "[Image]\n");


		err = fopen_s(&fp_I0_Average, m_strFilteredImageFileName_Nor_I0_Filter, "w");

		fprintf(fp_I0_Average, "[Header]\n");
		fprintf(fp_I0_Average, "FILE TYPE: Filtered: (2)\n");
		fprintf(fp_I0_Average, "%d\n", pixelsize_R);
		fprintf(fp_I0_Average, "%d\n", pixelsize_R);
		fprintf(fp_I0_Average, "[Image]\n");

		for (i = removeEdge; i < pixelsize; i++)		// 수정필요... KYD
		{
			for (k = 0; k < pixelsize_R; k++)
			{
				//fprintf(fp_I0, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k]);
				//fprintf(fp_I0_Average, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k]);
				fprintf(fp_I0, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_REGRID][i*pixelsize + k], 0);
				fprintf(fp_I0_Average, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k], 0);
			}
		}

		fclose(fp_I0);
		fclose(fp_I0_Average);
}

}
#endif

void CADAMCtrl::SetPointMeasureBufIndex(int PointMeasureBufIndex)
{
	m_cuurentPointMeasureBufIndex = PointMeasureBufIndex;
}

void CADAMCtrl::SetScanBufIndexVector(vector <int> scanBufIndexVector)
{
	m_vecScanBufIndex = scanBufIndexVector;
}

