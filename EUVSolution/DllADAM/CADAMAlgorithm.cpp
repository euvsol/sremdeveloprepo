/**
 * ADAM Module Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */

#pragma once

#include "stdafx.h"
#include "CADAMAlgorithm.h"
 //#include "CADAMCtrl.h"
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

CADAMAlgorithm::CADAMAlgorithm()
{
}


CADAMAlgorithm::~CADAMAlgorithm()
{
}

// 알고리즘을 구현하기 위한 함수
double CADAMAlgorithm::DataRegrid()
{
	double m = 0;
	double** RegridedData;

	//for (int num = 0; num < 1000; num++)
	//{
	//	for (int i = 0; i < KINDS_OF_ADAMDATA; i++)
	//	{



	//		//m_dRawImage_Data[i][num] = 0;
	//		// Inter polation algorithm 작성 예정___ KYD


	//	}
	//}


	return m;

}

double CADAMAlgorithm::DataInterpolation()
{
	double m = 0;


	// Inter polation algorithm 작성 예정___ KYD


	return m;

}

void CADAMAlgorithm::DataLinearRegrid(double** m_dRawImage_Data, double** m_dReconstructedImage_Data, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, double FOV, int num_scans)
{
	// 작성 할 것...KYD


	//----------시간 계산 코드------------

	time_t start, end;
	double ParsingTime1;
	CString msg;
	start = clock();

	// 실행할 함수

	end = clock();
	ParsingTime1 = (double)(end - start);
	msg.Format("Interpolatoin time: %.2f", ParsingTime1);
	AfxMessageBox(msg);


}

int CADAMAlgorithm::FindEdge_X(double** m_dImagetoEdgeFind, int pixel_x, int pixel_y)
{
	int x_edge_point = 0;


	//int Pixel_X = pixel_x;
	//int Pixel_Y = pixel_y;

	//double threshold = 0.5;

	//double** temp_image;
	////-------메모리 생성--------------
	//temp_image = new double *[Pixel_Y];						// 이미지 저장을 위한 추가 버퍼 생성
	//for (int i = 0; i < Pixel_Y; i++)
	//{
	//	temp_image[i] = new double[Pixel_X];
	//}

	//temp_image = m_dImagetoEdgeFind;


	//-------------------------------------------------------------------------
	// 1. (fx)*(fx), (fx)*(fy), (fy)*(fy) 계산
	//-------------------------------------------------------------------------



	//float** dx2 = temp_image.GetPixels2D();
	//float** dy2 = imgDy2.GetPixels2D();
	//float** dxy = imgDxy.GetPixels2D();

	//float tx, ty;
	//for (j = 1; j < h - 1; j++)
	//	for (i = 1; i < w - 1; i++)
	//	{
	//		tx = (ptr[j - 1][i + 1] + ptr[j][i + 1] + ptr[j + 1][i + 1]
	//			- ptr[j - 1][i - 1] - ptr[j][i - 1] - ptr[j + 1][i - 1]) / 6.f;
	//		ty = (ptr[j + 1][i - 1] + ptr[j + 1][i] + ptr[j + 1][i + 1]
	//			- ptr[j - 1][i - 1] - ptr[j - 1][i] - ptr[j - 1][i + 1]) / 6.f;

	//		dx2[j][i] = tx * tx;
	//		dy2[j][i] = ty * ty;
	//		dxy[j][i] = tx * ty;
	//	}




	//for (int i = 0; i <  Pixel_Y*Pixel_X; i++)
	//{
	//	if (temp_image[DETECTOR_INTENSITY_1][i] < threshold)
	//		temp_image[DETECTOR_INTENSITY_1][i] = 0;

	//	if (temp_image[DETECTOR_INTENSITY_1][i] >= threshold)
	//		temp_image[DETECTOR_INTENSITY_1][i] = 100;
	//}

	//// 이미지에서 Edge 의 위치를 찾는 알고리즘

	//for (int i = 0; i < Pixel_Y; i++)
	//{
	//	for (int j = 0; j < Pixel_X; j++)
	//	{



	//
	//	}
	//}



	////-------메모리 지우기--------------
	//if (temp_image != NULL)
	//{
	//	for (int i = 0; i < Pixel_Y; i++)							// 10은 나중에 수정 할 것... KYD
	//		delete temp_image[i];

	//	delete temp_image;
	//}
	//-------메모리 지우기--------------
	return x_edge_point;
}


int CADAMAlgorithm::FindEdge_Y(double** m_dImagetoEdgeFind, int pixel_x, int pixel_y)
{
	int y_edge_point = 0;
	//--------------------------------------------





	//------------------------------------------
	return y_edge_point;
}



int CADAMAlgorithm::kkktest(double test)
{
	int fff = 0;
	//--------------------------------------------


	AfxMessageBox("test ok");


	//------------------------------------------
	return fff;
}





//   파일에서 값 불러오는 함수
/*

	// 불러오기-------------------------------------------------------------------------------------------------

	WIN32_FIND_DATA w32fd;
	HANDLE hFind;
	string folder_name = ".\\Scan test result_20200207\\";
	LPCSTR folder_name_with_filter = ".\\Scan test result_20200207\\*.txt";

	hFind = FindFirstFile(folder_name_with_filter, &w32fd);
	int image_size_x, image_size_y, num_scans;
	PrepareInterpolationDataFromFile(this->m_dRawImage_Data_forRegrid, image_size_x, image_size_y,
		num_scans, folder_name + string(w32fd.cFileName), *this, 1);
	ofstream input_file;
	input_file.open("input image.txt");
	int current_index = 0;
	for (int n = 0; n < image_size_x; n++)
	{
		for (int m = 0; m < image_size_y; m++)
		{
			input_file << this->m_dRawImage_Data_forRegrid[DETECTOR_INTENSITY_1][current_index] << "\t";
			current_index++;
		}
		input_file << "\n";
	}
	input_file.close();


	// 출럭-----------------------------------------------------------------------------------------------------

	ofstream output_file;
	output_file.open("output image.txt");
	current_index = 0;
	for (int n = 0; n < 2000; n++)
	{
		for (int m = 0; m < 2000; m++)
		{
			output_file << this->m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][current_index] << "\t";
			current_index++;
		}
		output_file << "\n";
	}
	output_file.close();
	vector<vector<double>> filtered_image;
	int output_size = 2000;
	FilterImage(m_dReconstructedImage_Data, filtered_image, output_size, output_size, 0, conventional, 0.33 / 13.5, 0.8, 0, 0, 1, m_hWnd);
	ofstream output_file2;
	output_file2.open("filtered aerial image.txt");
	current_index = 0;
	for (int n = 0; n < output_size; n++)
	{
		for (int m = 0; m < output_size; m++)
		{
			output_file2 << m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][current_index] << "\t";
			current_index++;
		}
		output_file2 << "\n";
	}
	output_file2.close();


*/
int CADAMAlgorithm::Mean(const double data[], int n, double* mean)
{
	double sum = 0;
	for (int i = 0; i < n; i++)
	{
		sum = sum + data[i];
	}
	*mean = sum / (double)n;

	return 0;
}

int CADAMAlgorithm::StandardDeviation(const double data[], int n, double* std, double mean)
{
	double var_sum = 0;
	for (int i = 0; i < n; i++)
	{
		var_sum = var_sum + (data[i] - mean) *(data[i] - mean);
	}

	double var = var_sum / (double)n;

	*std = sqrt(var);

	return 0;

}

int CADAMAlgorithm::Statistics(const double data[], int n, double *mean, double* std)
{

	double sum = 0;
	for (int i = 0; i < n; i++)
	{
		sum = sum + data[i];
	}
	*mean = sum / (double)n;

	double var_sum = 0;
	for (int i = 0; i < n; i++)
	{
		var_sum = var_sum + (data[i] - *mean) *(data[i] - *mean);
	}

	double var = var_sum / (double)n;

	*std = sqrt(var);

	return 0;

}

double CADAMAlgorithm::Min(const double data[], int n)
{
	double min = data[0];

	for (int i = 0; i < n; i++)
	{
		if (data[i] < min)
		{
			min = data[i];

		}
	}

	return min;
}

double CADAMAlgorithm::Max(const double data[], int n)
{
	double max = data[0];

	for (int i = 0; i < n; i++)
	{
		if (data[i] > max)
		{
			max = data[i];
		}
	}
	return max;
}

int CADAMAlgorithm::test()
{
	return 0;
}

void CADAMAlgorithm::CaculatePTR()
{
	// Transmittance
	double D1_wp = mD1WithPellicle - mD1BackGround;
	double D2_wp = mD2WithPellicle - mD2BackGround;
	double D3_wp = mD3WithPellicle - mD3BackGround;

	double D1_wop = mD1WithoutPellicle - mD1BackGround;
	double D2_wop = mD2WithoutPellicle - mD2BackGround;
	double D3_wop = mD3WithoutPellicle - mD3BackGround;

	double D1_ref = mD1RefReflectance - mD1BackGround;
	double D2_ref = mD2RefReflectance - mD2BackGround;
	double D3_ref = mD3RefReflectance - mD3BackGround;

	double T, R;

	T = (D1_wp / D3_wp) / (D1_wop / D3_wop);
	mT = T * 100;

	mTwithoutNormalization = D1_wp / D1_wop * 100;

	//Reflectance From D1/D2 Gain ratio
	double d1_normalize = D1_wop / D3_wop;
	double d2_normalize = D2_wp / D3_wp;
	mRbyGainCalculation = d2_normalize / d1_normalize / mD2ToD1_GainRatioCalculation * 100;

	//Reflectance From Reference Reflectance Measure	
	double d2_normalize_100_percent = D2_ref / D3_ref / mRefReflectance;
	mD2ToD1_GainRatioMeasure = d2_normalize_100_percent / d1_normalize;
	R = ((D2_wp / D3_wp) - T * T *D2_wop / D3_wop) / d2_normalize_100_percent;

	mR = R * 100;
}