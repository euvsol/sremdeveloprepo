#include "stdafx.h"
#include "CADAMInterpolationAlgorithm.h"
#include "..\DllADAM\CADAMCtrl.h"
#include "..\DllADAM\CADAMAlgorithm.h"

#include <iostream>
#include <vector>
#include <list>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <exception>


using namespace std;

//Produces vector of num_in equally spaced numbers between start_in and end_in
//Similar to Matlab function linspace


template <typename T>
vector<float> linspace(T start_in, T end_in, int num_in)
{

	vector<float> linspaced;

	float start = static_cast<float>(start_in);
	float end = static_cast<float>(end_in);
	float num = static_cast<float>(num_in);

	if (num == 0) { return linspaced; }
	if (num == 1)
	{
		linspaced.push_back(start);
		return linspaced;
	}

	float delta = (end - start) / (num - 1);

	for (int i = 0; i < num - 1; ++i)
	{
		linspaced.push_back(start + delta * i);
	}
	linspaced.push_back(end); // I want to ensure that start and end
							  // are exactly the same as the input
	return linspaced;
}

//Produces two-dimensional arrays X and Y from one-dimensional x and y
//Similar as Matlab function meshgrid
//Example: if x={0,1,2} and y={3,4}, then X={{0,0},{1,1},{2,2}}, Y={{3,4},{3,4}}
template <typename T>
void meshgrid(vector <vector<T>>& X, vector <vector<T>>& Y, vector<T> x, vector<T> y)
{
	X.resize(x.size(), y);
	Y.resize(x.size(), y);
	typename vector<T>::iterator itx = x.begin();
	typename vector<vector<T>>::iterator it = X.begin();
	for (it; it != X.end(); ++it, ++itx)
		fill(it->begin(), it->end(), *itx);
}

//Produces vector of equally spaced numbers with step_value, starting from begin_value, 
//not more (or less, if step is negative) than end_value
//Similar to Matlab colon notation
template <typename T, typename T1, typename T2, typename T3>
vector <T> range(T1 end_value, T2 begin_value = 0, T3 step_value = 1)
{
	vector <T> output_value(0);
	T converted_begin_value = static_cast<T>(begin_value);
	T converted_end_value = static_cast<T>(end_value);
	T converted_step_value = static_cast<T>(step_value);
	if (converted_step_value > 0)
	{
		if (converted_begin_value <= converted_end_value)
		{
			T current_value = converted_begin_value;
			while (current_value <= converted_end_value)
			{
				output_value.push_back(current_value);
				current_value += converted_step_value;
			}
		}
	}
	if (converted_step_value < 0)
	{
		if (converted_begin_value >= converted_end_value)
		{
			T current_value = converted_begin_value;
			while (current_value >= converted_end_value)
			{
				output_value.push_back(current_value);
				current_value += converted_step_value;
			}
		}
	}
	return output_value;
}


//Same as above, but with step_value of the same type as output
template <typename T, typename T1, typename T2>
vector <T> range(T1 end_value, T2 begin_value = 0, T step_value = 1)
{
	vector <T> output_value(0);
	T converted_begin_value = static_cast<T>(begin_value);
	T converted_end_value = static_cast<T>(end_value);
	T converted_step_value = static_cast<T>(step_value);
	if (converted_step_value > 0)
	{
		if (converted_begin_value <= converted_end_value)
		{
			T current_value = converted_begin_value;
			while (current_value <= converted_end_value)
			{
				output_value.push_back(current_value);
				current_value += converted_step_value;
			}
		}
	}
	if (converted_step_value < 0)
	{
		if (converted_begin_value >= converted_end_value)
		{
			T current_value = converted_begin_value;
			while (current_value >= converted_end_value)
			{
				output_value.push_back(current_value);
				current_value += converted_step_value;
			}
		}
	}
	return output_value;
}

//Produces repeated copy of the same vector 
//Similar to Matlab repmat function
template <typename T>
vector <T> repmat(vector <T> original_vector, int num_repetitions)
{
	vector <T> output_vector(original_vector.size() * num_repetitions);
	for (int n = 0; n < num_repetitions; n++)
		for (int m = 0; m < original_vector.size(); m++)
			output_vector[m + n * original_vector.size()] = original_vector[m];
	return output_vector;
}


//Two below functions convert 2D and 1D C++ vectors into Python numpy array

/** Convert a c++ 2D vector into a numpy array
 *
 * @param const vector< vector<T> >& vec : 2D vector data
 * @return PyArrayObject* array : converted numpy array
 *
 * Transforms an arbitrary 2D C++ vector into a numpy array. Throws in case of
 * unregular shape. The array may contain empty columns or something else, as
 * long as it's shape is square.
 *
 * Warning this routine makes a copy of the memory!
 */

template<typename T>
static PyArrayObject* vector_to_nparray(const vector< vector<T> >& vec, int type_num = PyArray_DOUBLE) {

	// rows not empty
	if (!vec.empty()) {

		// column not empty
		if (!vec[0].empty()) {

			size_t nRows = vec.size();
			size_t nCols = vec[0].size();
			npy_intp dims[2] = { nRows, nCols };
			PyArrayObject* vec_array = (PyArrayObject*)PyArray_SimpleNew(2, dims, type_num);

			T* vec_array_pointer = (T*)PyArray_DATA(vec_array);

			// copy vector line by line ... maybe could be done at one
			for (size_t iRow = 0; iRow < vec.size(); ++iRow) {

				if (vec[iRow].size() != nCols) {
					Py_DECREF(vec_array); // delete
					throw(string("Can not convert vector<vector<T>> to np.array, since c++ matrix shape is not uniform."));
				}

				copy(vec[iRow].begin(), vec[iRow].end(), vec_array_pointer + iRow * nCols);
			}

			return vec_array;

			// Empty columns
		}
		else {
			npy_intp dims[2] = { vec.size(), 0 };
			return (PyArrayObject*)PyArray_ZEROS(2, dims, PyArray_DOUBLE, 0);
		}


		// no data at all
	}
	else {
		npy_intp dims[2] = { 0, 0 };
		return (PyArrayObject*)PyArray_ZEROS(2, dims, PyArray_DOUBLE, 0);
	}

}

template<typename T>
static PyArrayObject* vector_to_nparray(const vector<T>& vec, int type_num = PyArray_FLOAT) {

	// rows not empty
	if (!vec.empty()) {

		size_t nRows = vec.size();
		npy_intp dims[1] = { nRows };

		PyArrayObject* vec_array = (PyArrayObject*)PyArray_SimpleNew(1, dims, type_num);
		T* vec_array_pointer = (T*)PyArray_DATA(vec_array);

		copy(vec.begin(), vec.end(), vec_array_pointer);
		return vec_array;

		// no data at all
	}
	else {
		npy_intp dims[1] = { 0 };
		return (PyArrayObject*)PyArray_ZEROS(1, dims, PyArray_FLOAT, 0);
	}

}

vector<double> gauss(vector< vector<double> > A) {
	int n = int(A.size());

	for (int i = 0; i < n; i++) {
		// Search for maximum in this column
		double maxEl = abs(A[i][i]);
		int maxRow = i;
		for (int k = i + 1; k < n; k++) {
			if (abs(A[k][i]) > maxEl) {
				maxEl = abs(A[k][i]);
				maxRow = k;
			}
		}

		// Swap maximum row with current row (column by column)
		for (int k = i; k < n + 1; k++) {
			double tmp = A[maxRow][k];
			A[maxRow][k] = A[i][k];
			A[i][k] = tmp;
		}

		// Make all rows below this one 0 in current column
		for (int k = i + 1; k < n; k++) {
			double c = -A[k][i] / A[i][i];
			for (int j = i; j < n + 1; j++) {
				if (i == j) {
					A[k][j] = 0;
				}
				else {
					A[k][j] += c * A[i][j];
				}
			}
		}
	}

	// Solve equation Ax=b for an upper triangular matrix A
	vector<double> x(n);
	for (int i = n - 1; i >= 0; i--) {
		x[i] = A[i][n] / A[i][i];
		for (int k = i - 1; k >= 0; k--) {
			A[k][n] -= A[k][i] * x[i];
		}
	}
	return x;
}


//int Griddata(PyObject* pFunc, double** _dRawImage_Data, double** m_dReconstructedImage_Data, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV)
//{
//	PyObject* pArgs = NULL, *pValue = NULL, *pArgsNormailize = NULL, *pValueNormalize = NULL;
//
//	double x_regrid_step = FOV / reconstructed_size_x;
//	double y_regrid_step = FOV / reconstructed_size_y;
//
//	for (int n = 0; n < reconstructed_size_y; n++)
//		for (int m = 0; m < reconstructed_size_x; m++)
//		{
//			m_dReconstructedImage_Data[LASERINTERFEROMETER_X][m + n * reconstructed_size_x] = m * x_regrid_step + x_start;
//			m_dReconstructedImage_Data[LASERINTERFEROMETER_Y][m + n * reconstructed_size_x] = n * y_regrid_step + y_start;
//		}
//
//	if (pFunc && PyCallable_Check(pFunc))
//	{
//		npy_intp input_dims[1]{ raw_size_x*raw_size_y*num_scans };
//		npy_intp scalar_dims[1]{ 1 };
//		double* num_scans_array;
//		double* python_output;
//		double* python_outputNormailize;
//
//		num_scans_array = new double[1];
//		num_scans_array[0] = double(num_scans);
//		npy_intp output_dims[1]{ reconstructed_size_x*reconstructed_size_y };
//		const int ND{ 1 };
//		try
//		{
//			PyArrayObject* py_X_experimental = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(_dRawImage_Data[LASERINTERFEROMETER_X])));
//			PyArrayObject* py_Y_experimental = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(_dRawImage_Data[LASERINTERFEROMETER_Y])));
//			PyArrayObject* py_measured_experimental_image = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(_dRawImage_Data[DETECTOR_INTENSITY_1])));		
//			PyArrayObject* py_X_test = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(m_dReconstructedImage_Data[LASERINTERFEROMETER_X])));
//			PyArrayObject* py_Y_test = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(m_dReconstructedImage_Data[LASERINTERFEROMETER_Y])));
//			PyArrayObject* py_num_scans = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, scalar_dims, NPY_DOUBLE, reinterpret_cast<void*>(num_scans_array)));
//			pArgs = PyTuple_New(6);
//			PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(py_X_experimental));
//			PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(py_Y_experimental));
//			PyTuple_SetItem(pArgs, 2, reinterpret_cast<PyObject*>(py_measured_experimental_image));
//			PyTuple_SetItem(pArgs, 3, reinterpret_cast<PyObject*>(py_X_test));
//			PyTuple_SetItem(pArgs, 4, reinterpret_cast<PyObject*>(py_Y_test));
//			PyTuple_SetItem(pArgs, 5, reinterpret_cast<PyObject*>(py_num_scans));
//			pValue = PyObject_CallObject(pFunc, pArgs);
//			PyArrayObject* interpolated_image = reinterpret_cast<PyArrayObject*>(pValue);
//			if (PyErr_Occurred())
//				PyErr_Print();
//			python_output = reinterpret_cast<double*>(PyArray_DATA(interpolated_image));								
//			memcpy(m_dReconstructedImage_Data[DETECTOR_INTENSITY_1], python_output, sizeof(double) * reconstructed_size_x*reconstructed_size_y);
//			// memcopy로 하면 효율 향상 될까? ihlee
//			//for (int n = 0; n < reconstructed_size_x*reconstructed_size_y; n++)
//				//m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][n] = python_output[n];
//	
//	
//			// I0 normalize 
//			PyArrayObject* py_X_experimentalNormailize = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(_dRawImage_Data[LASERINTERFEROMETER_X])));
//			PyArrayObject* py_Y_experimentalNormailize = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(_dRawImage_Data[LASERINTERFEROMETER_Y])));
//			PyArrayObject* py_measured_experimental_imageNormalize = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(_dRawImage_Data[D1_NORMALIZED]))); //ihlee
//			PyArrayObject* py_X_testNormailize = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(m_dReconstructedImage_Data[LASERINTERFEROMETER_X])));
//			PyArrayObject* py_Y_testNormailize = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(m_dReconstructedImage_Data[LASERINTERFEROMETER_Y])));
//			PyArrayObject* py_num_scansNormailize = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, scalar_dims, NPY_DOUBLE, reinterpret_cast<void*>(num_scans_array)));
//
//			pArgsNormailize = PyTuple_New(6);
//			PyTuple_SetItem(pArgsNormailize, 0, reinterpret_cast<PyObject*>(py_X_experimentalNormailize));
//			PyTuple_SetItem(pArgsNormailize, 1, reinterpret_cast<PyObject*>(py_Y_experimentalNormailize));
//			PyTuple_SetItem(pArgsNormailize, 2, reinterpret_cast<PyObject*>(py_measured_experimental_imageNormalize));
//			PyTuple_SetItem(pArgsNormailize, 3, reinterpret_cast<PyObject*>(py_X_testNormailize));
//			PyTuple_SetItem(pArgsNormailize, 4, reinterpret_cast<PyObject*>(py_Y_testNormailize));
//			PyTuple_SetItem(pArgsNormailize, 5, reinterpret_cast<PyObject*>(py_num_scansNormailize));
//			pValueNormalize = PyObject_CallObject(pFunc, pArgsNormailize);
//			PyArrayObject* interpolated_imageNormalize = reinterpret_cast<PyArrayObject*>(pValueNormalize);
//			python_outputNormailize = reinterpret_cast<double*>(PyArray_DATA(interpolated_imageNormalize));
//			memcpy(m_dReconstructedImage_Data[D1_NORMALIZED_REGRID], python_outputNormailize, sizeof(double) * reconstructed_size_x*reconstructed_size_y);
//					
//			if (pArgs != NULL) Py_DECREF(pArgs);
//			if (pValue != NULL) Py_DECREF(pValue);
//			if (pArgsNormailize != NULL) Py_DECREF(pArgsNormailize);
//			if (pValueNormalize != NULL) Py_DECREF(pValueNormalize);
//
//			delete[] num_scans_array;
//		}
//		catch (...)
//		{
//			if (pArgs != NULL) Py_DECREF(pArgs);
//			if (pValue != NULL) Py_DECREF(pValue);
//			if (pArgsNormailize != NULL) Py_DECREF(pArgsNormailize);
//			if (pValueNormalize != NULL) Py_DECREF(pValueNormalize);
//			delete[] num_scans_array;
//
//			throw;
//		}
//	}
//
//	return 0;
//}

int Griddata(PyObject* pFunc, double** _dRawImage_Data, double** m_dReconstructedImage_Data, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV)
{
	PyObject* pArgs = NULL, *pValue = NULL;

	double x_regrid_step = FOV / reconstructed_size_x;
	double y_regrid_step = FOV / reconstructed_size_y;

	for (int n = 0; n < reconstructed_size_y; n++)
		for (int m = 0; m < reconstructed_size_x; m++)
		{
			m_dReconstructedImage_Data[LASERINTERFEROMETER_X][m + n * reconstructed_size_x] = m * x_regrid_step + x_start;
			m_dReconstructedImage_Data[LASERINTERFEROMETER_Y][m + n * reconstructed_size_x] = n * y_regrid_step + y_start;
		}

	if (pFunc && PyCallable_Check(pFunc))
	{
		npy_intp input_dims[1]{ raw_size_x*raw_size_y*num_scans };
		npy_intp scalar_dims[1]{ 1 };
		double* num_scans_array;
		double* python_output;
		num_scans_array = new double[1];
		num_scans_array[0] = double(num_scans);
		npy_intp output_dims[1]{ reconstructed_size_x*reconstructed_size_y };
		const int ND{ 1 };
		try
		{
			PyArrayObject* py_X_experimental = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(_dRawImage_Data[LASERINTERFEROMETER_X])));
			PyArrayObject* py_Y_experimental = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(_dRawImage_Data[LASERINTERFEROMETER_Y])));
			PyArrayObject* py_measured_experimental_image = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(_dRawImage_Data[DETECTOR_INTENSITY_1])));
			PyArrayObject* py_X_test = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(m_dReconstructedImage_Data[LASERINTERFEROMETER_X])));
			PyArrayObject* py_Y_test = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(m_dReconstructedImage_Data[LASERINTERFEROMETER_Y])));
			PyArrayObject* py_num_scans = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, scalar_dims, NPY_DOUBLE, reinterpret_cast<void*>(num_scans_array)));
			pArgs = PyTuple_New(6);
			PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(py_X_experimental));
			PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(py_Y_experimental));
			PyTuple_SetItem(pArgs, 2, reinterpret_cast<PyObject*>(py_measured_experimental_image));
			PyTuple_SetItem(pArgs, 3, reinterpret_cast<PyObject*>(py_X_test));
			PyTuple_SetItem(pArgs, 4, reinterpret_cast<PyObject*>(py_Y_test));
			PyTuple_SetItem(pArgs, 5, reinterpret_cast<PyObject*>(py_num_scans));
			pValue = PyObject_CallObject(pFunc, pArgs);
			PyArrayObject* interpolated_image = reinterpret_cast<PyArrayObject*>(pValue);
			if (PyErr_Occurred())
				PyErr_Print();
			python_output = reinterpret_cast<double*>(PyArray_DATA(interpolated_image));
			for (int n = 0; n < reconstructed_size_x*reconstructed_size_y; n++)
				m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][n] = python_output[n];

			if (pArgs != NULL) Py_DECREF(pArgs);
			if (pValue != NULL) Py_DECREF(pValue);

			delete[] num_scans_array;
		}
		catch (...)
		{
			if (pArgs != NULL) Py_DECREF(pArgs);
			if (pValue != NULL) Py_DECREF(pValue);
			delete[] num_scans_array;

			throw;
		}
	}

	return 0;
}

// New interpolatoin algorithm_20200129_Roman. //  White가 좁게 나오는 현상 때문에 minor revision 하였음

int GriddataNew(PyObject* pFunc, double* Image_raw, double* x_raw, double* y_raw, double* Image_regrid, double* x_regrid, double* y_regrid,
	int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV, int tempExtraPixel)
{
	PyObject* pArgs = NULL, *pValue = NULL;

	double x_regrid_step = FOV / reconstructed_size_x;
	double y_regrid_step = FOV / reconstructed_size_y;

	for (int n = 0; n < reconstructed_size_y; n++)
		for (int m = 0; m < reconstructed_size_x; m++)
		{
			x_regrid[m + n * reconstructed_size_x] = m * x_regrid_step + x_start;
			y_regrid[m + n * reconstructed_size_x] = n * y_regrid_step + y_start;
		}

	if (pFunc && PyCallable_Check(pFunc))
	{
		npy_intp input_dims[1]{ raw_size_x*raw_size_y*num_scans + tempExtraPixel};
		npy_intp scalar_dims[1]{ 1 };
		double* num_scans_array;
		double* python_output;

		num_scans_array = new double[1];
		num_scans_array[0] = double(num_scans);
		npy_intp output_dims[1]{ reconstructed_size_x*reconstructed_size_y };
		const int ND{ 1 };
		try
		{
			PyArrayObject* py_X_experimental = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(x_raw)));
			PyArrayObject* py_Y_experimental = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(y_raw)));
			PyArrayObject* py_measured_experimental_image = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(Image_raw)));
			PyArrayObject* py_X_test = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(x_regrid)));
			PyArrayObject* py_Y_test = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(y_regrid)));
			PyArrayObject* py_num_scans = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, scalar_dims, NPY_DOUBLE, reinterpret_cast<void*>(num_scans_array)));
			pArgs = PyTuple_New(6);
			PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(py_X_experimental));
			PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(py_Y_experimental));
			PyTuple_SetItem(pArgs, 2, reinterpret_cast<PyObject*>(py_measured_experimental_image));
			PyTuple_SetItem(pArgs, 3, reinterpret_cast<PyObject*>(py_X_test));
			PyTuple_SetItem(pArgs, 4, reinterpret_cast<PyObject*>(py_Y_test));
			PyTuple_SetItem(pArgs, 5, reinterpret_cast<PyObject*>(py_num_scans));
			pValue = PyObject_CallObject(pFunc, pArgs);
			PyArrayObject* interpolated_image = reinterpret_cast<PyArrayObject*>(pValue);
			if (PyErr_Occurred())
				PyErr_Print();
			python_output = reinterpret_cast<double*>(PyArray_DATA(interpolated_image));
			memcpy(Image_regrid, python_output, sizeof(double) * reconstructed_size_x*reconstructed_size_y);


			if (pArgs != NULL) Py_DECREF(pArgs);
			if (pValue != NULL) Py_DECREF(pValue);
			delete[] num_scans_array;
		}
		catch (...)
		{
			if (pArgs != NULL) Py_DECREF(pArgs);
			if (pValue != NULL) Py_DECREF(pValue);
			delete[] num_scans_array;

			throw;
		}
	}

	return 0;
}

int GriddataRemoveNan(PyObject* pFunc, double* Image_raw, double* x_raw, double* y_raw, double* Image_regrid, double* x_regrid, double* y_regrid,
	int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV, int tempExtraPixel)
{
	
	PyObject* pArgs = NULL, *pValue = NULL;

	double x_regrid_step = FOV / reconstructed_size_x;
	double y_regrid_step = FOV / reconstructed_size_y;

	for (int n = 0; n < reconstructed_size_y; n++)
		for (int m = 0; m < reconstructed_size_x; m++)
		{
			x_regrid[m + n * reconstructed_size_x] = m * x_regrid_step + x_start;
			y_regrid[m + n * reconstructed_size_x] = n * y_regrid_step + y_start;
		}

	if (pFunc && PyCallable_Check(pFunc))
	{
		npy_intp input_dims[1]{ raw_size_x*raw_size_y*num_scans + tempExtraPixel };
		npy_intp scalar_dims[1]{ 1 };		
		double* python_output;
		
		npy_intp output_dims[1]{ reconstructed_size_x*reconstructed_size_y };
		const int ND{ 1 };
		try
		{
			PyArrayObject* py_X_experimental = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(x_raw)));
			PyArrayObject* py_Y_experimental = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(y_raw)));
			PyArrayObject* py_measured_experimental_image = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(Image_raw)));
			PyArrayObject* py_X_test = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(x_regrid)));
			PyArrayObject* py_Y_test = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, output_dims, NPY_DOUBLE, reinterpret_cast<void*>(y_regrid)));			
			pArgs = PyTuple_New(5);
			PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(py_X_experimental));
			PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(py_Y_experimental));
			PyTuple_SetItem(pArgs, 2, reinterpret_cast<PyObject*>(py_measured_experimental_image));
			PyTuple_SetItem(pArgs, 3, reinterpret_cast<PyObject*>(py_X_test));
			PyTuple_SetItem(pArgs, 4, reinterpret_cast<PyObject*>(py_Y_test));			
			pValue = PyObject_CallObject(pFunc, pArgs);
			PyArrayObject* interpolated_image = reinterpret_cast<PyArrayObject*>(pValue);
			if (PyErr_Occurred())
				PyErr_Print();
			python_output = reinterpret_cast<double*>(PyArray_DATA(interpolated_image));
			memcpy(Image_regrid, python_output, sizeof(double) * reconstructed_size_x*reconstructed_size_y);


			if (pArgs != NULL) Py_DECREF(pArgs);
			if (pValue != NULL) Py_DECREF(pValue);
			
		}
		catch (...)
		{
			if (pArgs != NULL) Py_DECREF(pArgs);
			if (pValue != NULL) Py_DECREF(pValue);

			throw;
		}
	}

	return 0;
}


// *
void LinearRegrid(double** m_dRawImage_DataA, double** m_dReconstructedImage_Data, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, double FOV, int num_scans, double x_start, double y_start)
{
	//ATTENTION, it is assumed that ratio of grid steps in raw image and image after regridding is integer! for example, 10 nm grid->1 nm grid
	//This is essential for algorithm

	double x_step = FOV / raw_size_x;
	double y_step = FOV / raw_size_y;
	double x_regrid_step = FOV / reconstructed_size_x;
	double y_regrid_step = FOV / reconstructed_size_y;
	for (int n = 0; n < reconstructed_size_y; n++)
		for (int m = 0; m < reconstructed_size_x; m++)
		{
			m_dReconstructedImage_Data[LASERINTERFEROMETER_X][m + n * reconstructed_size_x] = m * x_regrid_step + x_start;
			m_dReconstructedImage_Data[LASERINTERFEROMETER_Y][m + n * reconstructed_size_x] = n * y_regrid_step + y_start;
		}
	int x_start_index = 0;
	int y_start_index = 0;
	int x_step_index = int(x_step / x_regrid_step);
	int y_step_index = int(y_step / y_regrid_step);
	vector <float> x_array = linspace(0.0, x_step - x_regrid_step, x_step_index);
	vector <float> y_array = linspace(0.0, y_step - y_regrid_step, y_step_index);
	vector<vector<double> > A(4, vector<double>(5, 0.0));
	vector <double> C(4);
	double xx, yy;

	//AfxMessageBox("LinearRegird 알고리즘 안으로 들어갔음. KYD");


	for (int k = 0; k < num_scans; k++)
	{
		for (int n = 0; n < raw_size_y - 1; n++)
		{
			for (int m = 0; m < raw_size_x - 1; m++)
			{
				xx = m_dRawImage_DataA[LASERINTERFEROMETER_X][m + n * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
				yy = m_dRawImage_DataA[LASERINTERFEROMETER_Y][m + n * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
				A[0][0] = 1.0;
				A[0][1] = xx;
				A[0][2] = yy;
				A[0][3] = xx * yy;
				A[0][4] = m_dRawImage_DataA[DETECTOR_INTENSITY_1][m + n * raw_size_x + k * raw_size_x * raw_size_y];
				xx = m_dRawImage_DataA[LASERINTERFEROMETER_X][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
				yy = m_dRawImage_DataA[LASERINTERFEROMETER_Y][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
				A[1][0] = 1.0;
				A[1][1] = xx;
				A[1][2] = yy;
				A[1][3] = xx * yy;
				A[1][4] = m_dRawImage_DataA[DETECTOR_INTENSITY_1][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y];
				xx = m_dRawImage_DataA[LASERINTERFEROMETER_X][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
				yy = m_dRawImage_DataA[LASERINTERFEROMETER_Y][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
				A[2][0] = 1.0;
				A[2][1] = xx;
				A[2][2] = yy;
				A[2][3] = xx * yy;
				A[2][4] = m_dRawImage_DataA[DETECTOR_INTENSITY_1][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
				xx = m_dRawImage_DataA[LASERINTERFEROMETER_X][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
				yy = m_dRawImage_DataA[LASERINTERFEROMETER_Y][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
				A[3][0] = 1.0;
				A[3][1] = xx;
				A[3][2] = yy;
				A[3][3] = xx * yy;
				A[3][4] = m_dRawImage_DataA[DETECTOR_INTENSITY_1][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
				C = gauss(A);
				vector <double> square_values(4);
				square_values[0] = m_dRawImage_DataA[DETECTOR_INTENSITY_1][m + n * raw_size_x + k * raw_size_x * raw_size_y];
				square_values[1] = m_dRawImage_DataA[DETECTOR_INTENSITY_1][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y];
				square_values[2] = m_dRawImage_DataA[DETECTOR_INTENSITY_1][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
				square_values[3] = m_dRawImage_DataA[DETECTOR_INTENSITY_1][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
				double local_max = *max_element(square_values.begin(), square_values.end());
				double local_min = *min_element(square_values.begin(), square_values.end());
				for (int k1 = 0; k1 < y_step_index; k1++)
					for (int k2 = 0; k2 < x_step_index; k2++)
					{
						double current_value = C[0] + C[1] * x_array[k2] + C[2] * y_array[k1] + C[3] * x_array[k2] * y_array[k1];
						if (current_value > local_max)
							current_value = local_max;
						if (current_value < local_min)
							current_value = local_min;
						if (k == 0)
							m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m*x_step_index + k2 + n * reconstructed_size_x*y_step_index + k1 * reconstructed_size_x + x_start_index + y_start_index * reconstructed_size_x] =
							current_value / num_scans;
						else
							m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m*x_step_index + k2 + n * reconstructed_size_x*y_step_index + k1 * reconstructed_size_x + x_start_index + y_start_index * reconstructed_size_x] +=
							current_value / num_scans;
					}
			}
		}
	}
}

void CubicRegrid(double** m_dRawImage_Data, double** m_dReconstructedImage_Data, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, double FOV, int num_scans, double x_start, double y_start)
{
	//ATTENTION, it is assumed that ratio of grid steps in raw image and image after regridding is integer! for example, 10 nm grid->1 nm grid
	//This is essential for algorithm
	double x_step = FOV / raw_size_x;
	double y_step = FOV / raw_size_y;
	double x_regrid_step = FOV / reconstructed_size_x;
	double y_regrid_step = FOV / reconstructed_size_y;
	for (int n = 0; n < reconstructed_size_y; n++)
		for (int m = 0; m < reconstructed_size_x; m++)
		{
			m_dReconstructedImage_Data[LASERINTERFEROMETER_X][m + n * reconstructed_size_x] = m * x_regrid_step + x_start;
			m_dReconstructedImage_Data[LASERINTERFEROMETER_Y][m + n * reconstructed_size_x] = n * y_regrid_step + y_start;
			m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m + n * reconstructed_size_x] = 0.0;
		}
	int x_start_index = 0;
	int y_start_index = 0;
	int x_step_index = int(x_step / x_regrid_step);
	int y_step_index = int(y_step / y_regrid_step);
	vector <float> x_array = linspace(0.0, x_step - x_regrid_step, x_step_index);
	vector <float> y_array = linspace(0.0, y_step - y_regrid_step, y_step_index);
	vector<vector<double> > A(16, vector<double>(17, 0.0));
	vector <double> C(16);
	double x00, x01, x10, x11, y00, y01, y10, y11;
	for (int k = 0; k < num_scans; k++)
	{
		for (int n = 1; n < raw_size_y - 2; n++)
		{
			for (int m = 1; m < raw_size_x - 2; m++)
			{
				x00 = m_dRawImage_Data[LASERINTERFEROMETER_X][m + n * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
				x01 = m_dRawImage_Data[LASERINTERFEROMETER_X][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
				x10 = m_dRawImage_Data[LASERINTERFEROMETER_X][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
				x11 = m_dRawImage_Data[LASERINTERFEROMETER_X][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
				y00 = m_dRawImage_Data[LASERINTERFEROMETER_Y][m + n * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
				y01 = m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
				y10 = m_dRawImage_Data[LASERINTERFEROMETER_Y][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
				y11 = m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
				A[0][0] = 1.0;
				A[0][1] = x00;
				A[0][2] = y00;
				A[0][3] = x00 * x00;
				A[0][4] = y00 * y00;
				A[0][5] = x00 * y00;
				A[0][6] = x00 * x00 * x00;
				A[0][7] = y00 * y00 * y00;
				A[0][8] = x00 * x00 * y00;
				A[0][9] = x00 * y00 * y00;
				A[0][10] = x00 * x00 * x00 * y00;
				A[0][11] = x00 * y00 * y00 * y00;
				A[0][12] = x00 * x00 * x00 * y00 * y00;
				A[0][13] = x00 * x00 * y00 * y00 * y00;
				A[0][14] = x00 * x00 * x00 * y00 * y00 * y00;
				A[0][15] = x00 * x00 * y00 * y00;

				A[1][0] = 1.0;
				A[1][1] = x01;
				A[1][2] = y01;
				A[1][3] = x01 * x01;
				A[1][4] = y01 * y01;
				A[1][5] = x01 * y01;
				A[1][6] = x01 * x01 * x01;
				A[1][7] = y01 * y01 * y01;
				A[1][8] = x01 * x01 * y01;
				A[1][9] = x01 * y01 * y01;
				A[1][10] = x01 * x01 * x01 * y01;
				A[1][11] = x01 * y01 * y01 * y01;
				A[1][12] = x01 * x01 * x01 * y01 * y01;
				A[1][13] = x01 * x01 * y01 * y01 * y01;
				A[1][14] = x01 * x01 * x01 * y01 * y01 * y01;
				A[1][15] = x01 * x01 * y01 * y01;

				A[2][0] = 1.0;
				A[2][1] = x10;
				A[2][2] = y10;
				A[2][3] = x10 * x10;
				A[2][4] = y10 * y10;
				A[2][5] = x10 * y10;
				A[2][6] = x10 * x10 * x10;
				A[2][7] = y10 * y10 * y10;
				A[2][8] = x10 * x10 * y10;
				A[2][9] = x10 * y10 * y10;
				A[2][10] = x10 * x10 * x10 * y10;
				A[2][11] = x10 * y10 * y10 * y10;
				A[2][12] = x10 * x10 * x10 * y10 * y10;
				A[2][13] = x10 * x10 * y10 * y10 * y10;
				A[2][14] = x10 * x10 * x10 * y10 * y10 * y10;
				A[2][15] = x10 * x10 * y10 * y10;

				A[3][0] = 1.0;
				A[3][1] = x11;
				A[3][2] = y11;
				A[3][3] = x11 * x11;
				A[3][4] = y11 * y11;
				A[3][5] = x11 * y11;
				A[3][6] = x11 * x11 * x11;
				A[3][7] = y11 * y11 * y11;
				A[3][8] = x11 * x11 * y11;
				A[3][9] = x11 * y11 * y11;
				A[3][10] = x11 * x11 * x11 * y11;
				A[3][11] = x11 * y11 * y11 * y11;
				A[3][12] = x11 * x11 * x11 * y11 * y11;
				A[3][13] = x11 * x11 * y11 * y11 * y11;
				A[3][14] = x11 * x11 * x11 * y11 * y11 * y11;
				A[3][15] = x11 * x11 * y11 * y11;

				A[4][0] = 0.0;
				A[4][1] = 1.0;
				A[4][2] = 0.0;
				A[4][3] = 2.0 * x00;
				A[4][4] = 0.0;
				A[4][5] = y00;
				A[4][6] = 3 * x00 * x00;
				A[4][7] = 0.0;
				A[4][8] = 2.0 * x00 * y00;
				A[4][9] = y00 * y00;
				A[4][10] = 3.0 * x00 * x00 * y00;
				A[4][11] = y00 * y00 * y00;
				A[4][12] = 3.0 * x00 * x00 * y00 * y00;
				A[4][13] = 2.0 * x00 * y00 * y00 * y00;
				A[4][14] = 3.0 * x00 * x00 * y00 * y00 * y00;
				A[4][15] = 2.0 * x00 * y00 * y00;

				A[5][0] = 0.0;
				A[5][1] = 1.0;
				A[5][2] = 0.0;
				A[5][3] = 2.0 * x01;
				A[5][4] = 0.0;
				A[5][5] = y01;
				A[5][6] = 3.0 * x01 * x01;
				A[5][7] = 0.0;
				A[5][8] = 2.0 * x01 * y01;
				A[5][9] = y01 * y01;
				A[5][10] = 3.0 * x01 * x01 * y01;
				A[5][11] = y01 * y01 * y01;
				A[5][12] = 3.0 * x01 * x01 * y01 * y01;
				A[5][13] = 2.0 * x01 * y01 * y01 * y01;
				A[5][14] = 3.0 * x01 * x01 * y01 * y01 * y01;
				A[5][15] = 2.0 * x01 * y01 * y01;

				A[6][0] = 0.0;
				A[6][1] = 1.0;
				A[6][2] = 0.0;
				A[6][3] = 2.0 * x10;
				A[6][4] = 0.0;
				A[6][5] = y10;
				A[6][6] = 3.0 * x10 * x10;
				A[6][7] = 0.0;
				A[6][8] = 2.0 * x10 * y10;
				A[6][9] = y10 * y10;
				A[6][10] = 3.0 * x10 * x10 * y10;
				A[6][11] = y10 * y10 * y10;
				A[6][12] = 3.0 * x10 * x10 * y10 * y10;
				A[6][13] = 2.0 * x10 * y10 * y10 * y10;
				A[6][14] = 3.0 * x10 * x10 * y10 * y10 * y10;
				A[6][15] = 2.0 * x10 * y10 * y10;

				A[7][0] = 0.0;
				A[7][1] = 1.0;
				A[7][2] = 0.0;
				A[7][3] = 2.0 * x11;
				A[7][4] = 0.0;
				A[7][5] = y11;
				A[7][6] = 3.0 * x11 * x11;
				A[7][7] = 0.0;
				A[7][8] = 2.0 * x11 * y11;
				A[7][9] = y11 * y11;
				A[7][10] = 3.0 * x11 * x11 * y11;
				A[7][11] = y11 * y11 * y11;
				A[7][12] = 3.0 * x11 * x11 * y11 * y11;
				A[7][13] = 2.0 * x11 * y11 * y11 * y11;
				A[7][14] = 3.0 * x11 * x11 * y11 * y11 * y11;
				A[7][15] = 2.0 * x11 * y11 * y11;

				A[8][0] = 0.0;
				A[8][1] = 0.0;
				A[8][2] = 1.0;
				A[8][3] = 0.0;
				A[8][4] = 2.0 * y00;
				A[8][5] = x00;
				A[8][6] = 0.0;
				A[8][7] = 3.0 * y00 * y00;
				A[8][8] = x00 * x00;
				A[8][9] = 2.0 * x00 * y00;
				A[8][10] = x00 * x00 * x00;
				A[8][11] = 3.0 * x00 * y00 * y00;
				A[8][12] = 2.0 * x00 * x00 * x00 * y00;
				A[8][13] = 3.0 * x00 * x00 * y00 * y00;
				A[8][14] = 3.0 * x00 * x00 * x00 * y00 * y00;
				A[8][15] = 2.0 * x00 * x00 * y00;

				A[9][0] = 0.0;
				A[9][1] = 0.0;
				A[9][2] = 1.0;
				A[9][3] = 0.0;
				A[9][4] = 2.0 * y01;
				A[9][5] = x01;
				A[9][6] = 0.0;
				A[9][7] = 3.0 * y01 * y01;
				A[9][8] = x01 * x01;
				A[9][9] = 2.0 * x01 * y01;
				A[9][10] = x01 * x01 * x01;
				A[9][11] = 3.0 * x01 * y01 * y01;
				A[9][12] = 2.0 * x01 * x01 * x01 * y01;
				A[9][13] = 3.0 * x01 * x01 * y01 * y01;
				A[9][14] = 3.0 * x01 * x01 * x01 * y01 * y01;
				A[9][15] = 2.0 * x01 * x01 * y01;

				A[10][0] = 0.0;
				A[10][1] = 0.0;
				A[10][2] = 1.0;
				A[10][3] = 0.0;
				A[10][4] = 2.0 * y10;
				A[10][5] = x10;
				A[10][6] = 0.0;
				A[10][7] = 3.0 * y10 * y10;
				A[10][8] = x10 * x10;
				A[10][9] = 2.0 * x10 * y10;
				A[10][10] = x10 * x10 * x10;
				A[10][11] = 3.0 * x10 * y10 * y10;
				A[10][12] = 2.0 * x10 * x10 * x10 * y10;
				A[10][13] = 3.0 * x10 * x10 * y10 * y10;
				A[10][14] = 3.0 * x10 * x10 * x10 * y10 * y10;
				A[10][15] = 2.0 * x10 * x10 * y10;

				A[11][0] = 0.0;
				A[11][1] = 0.0;
				A[11][2] = 1.0;
				A[11][3] = 0.0;
				A[11][4] = 2.0 * y11;
				A[11][5] = x11;
				A[11][6] = 0.0;
				A[11][7] = 3.0 * y11 * y11;
				A[11][8] = x11 * x11;
				A[11][9] = 2.0 * x11 * y11;
				A[11][10] = x11 * x11 * x11;
				A[11][11] = 3.0 * x11 * y11 * y11;
				A[11][12] = 2.0 * x11 * x11 * x11 * y11;
				A[11][13] = 3.0 * x11 * x11 * y11 * y11;
				A[11][14] = 3.0 * x11 * x11 * x11 * y11 * y11;
				A[11][15] = 2.0 * x11 * x11 * y11;

				A[12][0] = 0.0;
				A[12][1] = 0.0;
				A[12][2] = 0.0;
				A[12][3] = 0.0;
				A[12][4] = 0.0;
				A[12][5] = 1.0;
				A[12][6] = 0.0;
				A[12][7] = 0.0;
				A[12][8] = 2.0 * x00;
				A[12][9] = 2.0 * y00;
				A[12][10] = 3.0 * x00 * x00;
				A[12][11] = 3.0 * y00 * y00;
				A[12][12] = 6.0 * x00 * x00 * y00;
				A[12][13] = 6.0 * x00 * y00 * y00;
				A[12][14] = 9.0 * x00 * x00 * y00 * y00;
				A[12][15] = 4.0 * x00 * y00;

				A[13][0] = 0.0;
				A[13][1] = 0.0;
				A[13][2] = 0.0;
				A[13][3] = 0.0;
				A[13][4] = 0.0;
				A[13][5] = 1.0;
				A[13][6] = 0.0;
				A[13][7] = 0.0;
				A[13][8] = 2.0 * x01;
				A[13][9] = 2.0 * y01;
				A[13][10] = 3.0 * x01 * x01;
				A[13][11] = 3.0 * y01 * y01;
				A[13][12] = 6.0 * x01 * x01 * y01;
				A[13][13] = 6.0 * x01 * y01 * y01;
				A[13][14] = 9.0 * x01 * x01 * y01 * y01;
				A[13][15] = 4.0 * x01 * y01;

				A[14][0] = 0.0;
				A[14][1] = 0.0;
				A[14][2] = 0.0;
				A[14][3] = 0.0;
				A[14][4] = 0.0;
				A[14][5] = 1.0;
				A[14][6] = 0.0;
				A[14][7] = 0.0;
				A[14][8] = 2.0 * x10;
				A[14][9] = 2.0 * y10;
				A[14][10] = 3.0 * x10 * x10;
				A[14][11] = 3.0 * y10 * y10;
				A[14][12] = 6.0 * x10 * x10 * y10;
				A[14][13] = 6.0 * x10 * y10 * y10;
				A[14][14] = 9.0 * x10 * x10 * y10 * y10;
				A[14][15] = 4.0 * x10 * y10;

				A[15][0] = 0.0;
				A[15][1] = 0.0;
				A[15][2] = 0.0;
				A[15][3] = 0.0;
				A[15][4] = 0.0;
				A[15][5] = 1.0;
				A[15][6] = 0.0;
				A[15][7] = 0.0;
				A[15][8] = 2.0 * x11;
				A[15][9] = 2.0 * y11;
				A[15][10] = 3.0 * x11 * x11;
				A[15][11] = 3.0 * y11 * y11;
				A[15][12] = 6.0 * x11 * x11 * y11;
				A[15][13] = 6.0 * x11 * y11 * y11;
				A[15][14] = 9.0 * x11 * x11 * y11 * y11;
				A[15][15] = 4.0 * x11 * y11;

				A[0][16] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + n * raw_size_x + k * raw_size_x * raw_size_y];
				A[1][16] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y];
				A[2][16] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
				A[3][16] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
				A[4][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m - 1 + n * raw_size_x + k * raw_size_x * raw_size_y]) /
					(m_dRawImage_Data[LASERINTERFEROMETER_X][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_X][m - 1 + n * raw_size_x + k * raw_size_x * raw_size_y]);
				A[5][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 2 + n * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + n * raw_size_x + k * raw_size_x * raw_size_y]) /
					(m_dRawImage_Data[LASERINTERFEROMETER_X][m + 2 + n * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_X][m + n * raw_size_x + k * raw_size_x * raw_size_y]);
				A[6][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m - 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y]) /
					(m_dRawImage_Data[LASERINTERFEROMETER_X][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_X][m - 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y]);
				A[7][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 2 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y]) /
					(m_dRawImage_Data[LASERINTERFEROMETER_X][m + 2 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_X][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y]);
				A[8][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y]) /
					(m_dRawImage_Data[LASERINTERFEROMETER_Y][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_Y][m + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y]);
				A[9][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y]) /
					(m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y]);
				A[10][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + n * raw_size_x + k * raw_size_x * raw_size_y]) /
					(m_dRawImage_Data[LASERINTERFEROMETER_Y][m + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_Y][m + n * raw_size_x + k * raw_size_x * raw_size_y]);
				A[11][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y]) /
					(m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y]);
				A[12][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m - 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y] +
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m - 1 + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y]) /
					((m_dRawImage_Data[LASERINTERFEROMETER_X][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_X][m - 1 + n * raw_size_x + k * raw_size_x * raw_size_y]) *
						(m_dRawImage_Data[LASERINTERFEROMETER_Y][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
							m_dRawImage_Data[LASERINTERFEROMETER_Y][m + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y]));
				A[13][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 2 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 2 + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y] +
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y]) /
					((m_dRawImage_Data[LASERINTERFEROMETER_X][m + 2 + n * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_X][m + n * raw_size_x + k * raw_size_x * raw_size_y]) *
						(m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
							m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + (n - 1) * raw_size_x + k * raw_size_x * raw_size_y]));
				A[14][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m - 1 + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] +
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m - 1 + n * raw_size_x + k * raw_size_x * raw_size_y]) /
					((m_dRawImage_Data[LASERINTERFEROMETER_X][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_X][m - 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y]) *
						(m_dRawImage_Data[LASERINTERFEROMETER_Y][m + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
							m_dRawImage_Data[LASERINTERFEROMETER_Y][m + n * raw_size_x + k * raw_size_x * raw_size_y]));
				A[15][16] = (m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 2 + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 2 + n * raw_size_x + k * raw_size_x * raw_size_y] +
					m_dRawImage_Data[DETECTOR_INTENSITY_1][m + n * raw_size_x + k * raw_size_x * raw_size_y]) /
					((m_dRawImage_Data[LASERINTERFEROMETER_X][m + 2 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] -
						m_dRawImage_Data[LASERINTERFEROMETER_X][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y]) *
						(m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + (n + 2) * raw_size_x + k * raw_size_x * raw_size_y] -
							m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y]));
				C = gauss(A);
				bool is_break = false;
				vector <double> square_values(4);
				square_values[0] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + n * raw_size_x + k * raw_size_x * raw_size_y];
				square_values[1] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y];
				square_values[2] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
				square_values[3] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
				double local_max = *max_element(square_values.begin(), square_values.end());
				double local_min = *min_element(square_values.begin(), square_values.end());
				for (int k1 = 0; k1 < y_step_index; k1++)
				{
					for (int k2 = 0; k2 < x_step_index; k2++)
					{
						double current_value = (C[0] + C[1] * x_array[k2] + C[2] * y_array[k1] + C[3] * x_array[k2] * x_array[k2] +
							C[4] * y_array[k1] * y_array[k1] + C[5] * x_array[k2] * y_array[k1] +
							C[6] * x_array[k2] * x_array[k2] * x_array[k2] +
							C[7] * y_array[k1] * y_array[k1] * y_array[k1] +
							C[8] * x_array[k2] * x_array[k2] * y_array[k1] +
							C[9] * x_array[k2] * y_array[k1] * y_array[k1] +
							C[10] * x_array[k2] * x_array[k2] * x_array[k2] * y_array[k1] +
							C[11] * x_array[k2] * y_array[k1] * y_array[k1] * y_array[k1] +
							C[12] * x_array[k2] * x_array[k2] * x_array[k2] * y_array[k1] * y_array[k1] +
							C[13] * x_array[k2] * x_array[k2] * y_array[k1] * y_array[k1] * y_array[k1] +
							C[14] * x_array[k2] * x_array[k2] * x_array[k2] * y_array[k1] * y_array[k1] * y_array[k1] +
							C[15] * x_array[k2] * x_array[k2] * y_array[k1] * y_array[k1]);
						if (current_value > local_max || current_value < local_min)
						{
							is_break = true;
							break;
						}
					}
					if (is_break)
						break;
				}
				if (is_break)
				{
					double xx, yy;
					vector<vector<double> > A1(4, vector<double>(5, 0.0));
					vector <double> C1(4);
					xx = m_dRawImage_Data[LASERINTERFEROMETER_X][m + n * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
					yy = m_dRawImage_Data[LASERINTERFEROMETER_Y][m + n * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
					A1[0][0] = 1.0;
					A1[0][1] = xx;
					A1[0][2] = yy;
					A1[0][3] = xx * yy;

					A1[0][4] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + n * raw_size_x + k * raw_size_x * raw_size_y];
					xx = m_dRawImage_Data[LASERINTERFEROMETER_X][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
					yy = m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
					A1[1][0] = 1.0;
					A1[1][1] = xx;
					A1[1][2] = yy;
					A1[1][3] = xx * yy;
					A1[1][4] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + n * raw_size_x + k * raw_size_x * raw_size_y];
					xx = m_dRawImage_Data[LASERINTERFEROMETER_X][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
					yy = m_dRawImage_Data[LASERINTERFEROMETER_Y][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
					A1[2][0] = 1.0;
					A1[2][1] = xx;
					A1[2][2] = yy;
					A1[2][3] = xx * yy;
					A1[2][4] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
					xx = m_dRawImage_Data[LASERINTERFEROMETER_X][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (x_start + x_step * m);
					yy = m_dRawImage_Data[LASERINTERFEROMETER_Y][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y] - (y_start + y_step * n);
					A1[3][0] = 1.0;
					A1[3][1] = xx;
					A1[3][2] = yy;
					A1[3][3] = xx * yy;
					A1[3][4] = m_dRawImage_Data[DETECTOR_INTENSITY_1][m + 1 + (n + 1) * raw_size_x + k * raw_size_x * raw_size_y];
					C1 = gauss(A1);
					for (int k1 = 0; k1 < y_step_index; k1++)
						for (int k2 = 0; k2 < x_step_index; k2++)
						{
							double current_value = (C1[0] + C1[1] * x_array[k2] + C1[2] * y_array[k1] + C1[3] * x_array[k2] * y_array[k1]);
							if (current_value > local_max)
								current_value = local_max;
							if (current_value < local_min)
								current_value = local_min;
							if (k == 0)
								m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m*x_step_index + k2 + n * reconstructed_size_x*y_step_index + k1 * reconstructed_size_x + x_start_index + y_start_index * reconstructed_size_x] =
								current_value / num_scans;
							else
								m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m*x_step_index + k2 + n * reconstructed_size_x*y_step_index + k1 * reconstructed_size_x + x_start_index + y_start_index * reconstructed_size_x] +=
								current_value / num_scans;
						}
				}

				else
				{
					for (int k1 = 0; k1 < y_step_index; k1++)
					{
						for (int k2 = 0; k2 < x_step_index; k2++)
						{
							double current_value = (C[0] + C[1] * x_array[k2] + C[2] * y_array[k1] + C[3] * x_array[k2] * x_array[k2] +
								C[4] * y_array[k1] * y_array[k1] + C[5] * x_array[k2] * y_array[k1] +
								C[6] * x_array[k2] * x_array[k2] * x_array[k2] +
								C[7] * y_array[k1] * y_array[k1] * y_array[k1] +
								C[8] * x_array[k2] * x_array[k2] * y_array[k1] +
								C[9] * x_array[k2] * y_array[k1] * y_array[k1] +
								C[10] * x_array[k2] * x_array[k2] * x_array[k2] * y_array[k1] +
								C[11] * x_array[k2] * y_array[k1] * y_array[k1] * y_array[k1] +
								C[12] * x_array[k2] * x_array[k2] * x_array[k2] * y_array[k1] * y_array[k1] +
								C[13] * x_array[k2] * x_array[k2] * y_array[k1] * y_array[k1] * y_array[k1] +
								C[14] * x_array[k2] * x_array[k2] * x_array[k2] * y_array[k1] * y_array[k1] * y_array[k1] +
								C[15] * x_array[k2] * x_array[k2] * y_array[k1] * y_array[k1]);
							if (k == 0)
								m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m*x_step_index + k2 + n * reconstructed_size_x*y_step_index +
								k1 * reconstructed_size_x + x_start_index + y_start_index * reconstructed_size_x] =
								current_value / num_scans;
							else
								m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m*x_step_index + k2 + n * reconstructed_size_x*y_step_index +
								k1 * reconstructed_size_x + x_start_index + y_start_index * reconstructed_size_x] +=
								current_value / num_scans;
						}
					}
				}
			}
		}
	}
}

void PrepareInterpolationDataFromFile(double** m_dRawImage_Data, int &image_size_x, int &image_size_y, int &num_scans, string input_file_name, HWND main_window, int file_type)
{
	double buf;
	int int_buf;
	string s;
	ifstream input_file(input_file_name);
	int start_pos;
	if (input_file.is_open())
	{
		if (!getline(input_file, s))
			MessageBox(main_window, "Unable to read input file with measurement data", "Error", 0);
		switch (file_type)
		{
		case 0:
			start_pos = int(s.find(": "));
			istringstream(s.substr(start_pos + 2, s.size() - start_pos - 2)) >> int_buf;
			if (int_buf != 1)
				MessageBox(main_window, "Wrong type of measurement data file, must be equal to 1", "Error", 0);
			if (!getline(input_file, s))
				MessageBox(main_window, "Unable to read input file with measurement data", "Error", 0);
			start_pos = int(s.find(": "));
			istringstream(s.substr(start_pos + 2, s.size() - start_pos - 2)) >> int_buf;
			if (int_buf > 10 || int_buf < 1)
				MessageBox(main_window, "Wrong scan number, must be from 1 to 10", "Error", 0);
			num_scans = int_buf;
			if (!getline(input_file, s))
				MessageBox(main_window, "Unable to read input file with measurement data", "Error", 0);
			start_pos = int(s.find(": "));
			istringstream(s.substr(start_pos + 2, s.size() - start_pos - 2)) >> int_buf;
			if (int_buf > 1000 || int_buf < 1)
				MessageBox(main_window, "Wrong image size x, must be from 1 to 1000", "Error", 0);
			image_size_x = int_buf;
			if (!getline(input_file, s))
				MessageBox(main_window, "Unable to read input file with measurement data", "Error", 0);
			start_pos = int(s.find(": "));
			istringstream(s.substr(start_pos + 2, s.size() - start_pos - 2)) >> int_buf;
			if (int_buf > 1000 || int_buf < 1)
				MessageBox(main_window, "Wrong image size y, must be from 1 to 1000", "Error", 0);
			image_size_y = int_buf;
			break;
		case 1:
			if (s.find("[Header]") == string::npos)
				MessageBox(main_window, "For this type of input file, first line must contain string Header", "Error", 0);
			if (!getline(input_file, s))
				MessageBox(main_window, "Unable to read input file with measurement data", "Error", 0);
			if (s.find("FILE TYPE") == string::npos || s.find("RAW") == string::npos)
				MessageBox(main_window, "For this type of input file, second line must contain FILE TYPE equal to RAW", "Error", 0);
			num_scans = 1;
			if (!getline(input_file, s))
				MessageBox(main_window, "Unable to read input file with measurement data", "Error", 0);
			start_pos = int(s.find(": "));
			istringstream(s) >> int_buf;
			if (int_buf > 1000 || int_buf < 1)
				MessageBox(main_window, "Wrong image size x, must be from 1 to 1000", "Error", 0);
			image_size_x = int_buf;
			if (!getline(input_file, s))
				MessageBox(main_window, "Unable to read input file with measurement data", "Error", 0);
			istringstream(s) >> int_buf;
			if (int_buf > 1000 || int_buf < 1)
				MessageBox(main_window, "Wrong image size y, must be from 1 to 1000", "Error", 0);
			image_size_y = int_buf;
			if (!getline(input_file, s))
				MessageBox(main_window, "Unable to read input file with measurement data", "Error", 0);
			if (s.find("[Image]") == string::npos)
				MessageBox(main_window, "Wrong format of input file, expected [Image] record", "Error", 0);
			break;
			if (!getline(input_file, s))
				MessageBox(main_window, "Unable to read input file with measurement data", "Error", 0);
		}
		int line_count = 0;
		while (getline(input_file, s))
		{
			istringstream ss(s);
			for (int n = 0; n < num_scans; n++)
			{
				ss >> buf;
				m_dRawImage_Data[LASERINTERFEROMETER_X][line_count + n * image_size_x*image_size_y] = buf;
				ss >> buf;
				m_dRawImage_Data[LASERINTERFEROMETER_Y][line_count + n * image_size_x*image_size_y] = buf;
				ss >> buf;
				m_dRawImage_Data[DETECTOR_INTENSITY_1][line_count + n * image_size_x*image_size_y] = buf;
			}
			line_count++;
		}
	}
	input_file.close();
}

double EvaluateCD(vector <double>::iterator aerial_image_begin, vector <double>::iterator aerial_image_end, double threshold,
	HWND m_hWnd, bool& center_sign)
{
	if (threshold < *(min_element(aerial_image_begin, aerial_image_end))
		|| threshold > *(max_element(aerial_image_begin, aerial_image_end)))
	{
		MessageBox(m_hWnd, "In function EvaluateCD, threshold is out of range of aerial image", "EvaluateCD function error", 0);
		return 0.0;
	}
	vector <double>::iterator index1, index2;
	if (*aerial_image_begin > threshold)
	{
		index1 = find_if(aerial_image_begin, aerial_image_end, [threshold](auto e) {return e < threshold; });
		index2 = find_if(index1, aerial_image_end, [threshold](auto e) {return e > threshold; });
	}
	else
	{
		index1 = find_if(aerial_image_begin, aerial_image_end, [threshold](auto e) {return e > threshold; });
		index2 = find_if(index1, aerial_image_end, [threshold](auto e) {return e < threshold; });
	}
	double left_border = (threshold - *(index1 - 1)) / (*index1 - *(index1 - 1)) + double(int(index1 - 1 - aerial_image_begin));
	double right_border = (threshold - *(index2 - 1)) / (*index2 - *(index2 - 1)) + double(int(index2 - 1 - aerial_image_begin));
	if (*index1 > *(index1 - 1))
		center_sign = true;
	else
		center_sign = false;
	return right_border - left_border;
}

void GenerateCDVector(vector <double> cross_section, double threshold, vector <double>& CD_vector, bool& CD_type, HWND main_window)
{
	vector <int> left_points;
	vector <int> right_points;
	bool right_flag = false;
	bool center_sign;
	if (cross_section[0] > threshold)
		CD_type = false;
	else
		CD_type = true;
	for (int n = 0; n < (int)cross_section.size() - 1; n++)
	{
		if ((cross_section[n] - threshold)*(cross_section[n + 1] - threshold) < 0)
		{
			if (right_flag)
			{
				right_points.push_back(n + 1);
				right_flag = false;
			}
			else
			{
				left_points.push_back(n);
				right_flag = true;
			}
		}
	}
	if (left_points.size() > right_points.size())
		left_points.pop_back();
	CD_vector.resize(left_points.size());
	for (int n = 0; n < (int)left_points.size(); n++)
		CD_vector[n] = EvaluateCD(cross_section.begin() + left_points[n],
			cross_section.begin() + right_points[n], threshold, main_window, center_sign);
}

void EvaluateCD_forLS(vector <vector<double>> input_image, double threshold, double x_step,
	double averaging_size, vector<vector<double>>& CD_array, double& average_CD, double & LWR, bool& CD_type, HWND main_window)
{
	//AllocConsole();
	//freopen("CONOUT$", "w", stdout);
	//freopen("CONOUT$", "w", stderr);
	if (!(int)input_image.size())
	{
		MessageBox(main_window, "Input image is empty in function EvaluateCD_forLS", "Error", 0);
		CD_array.resize(0);
		LWR = numeric_limits<double>::quiet_NaN();
		CD_type = false;
		average_CD = numeric_limits<double>::quiet_NaN();
		return;
	}
	if (!(int)input_image[0].size())
	{
		MessageBox(main_window, "Input image is empty in function EvaluateCD_forLS", "Error", 0);
		CD_array.resize(0);
		LWR = numeric_limits<double>::quiet_NaN();
		CD_type = false;
		average_CD = numeric_limits<double>::quiet_NaN();
		return;
	}
	double max_value = input_image[0][0];
	double min_value = input_image[0][0];
	bool local_CD_type;
	CD_array.resize(input_image.size());
	for (vector<vector<double>>::iterator it = input_image.begin(); it != input_image.end(); ++it)
	{
		double local_max = *max_element(it->begin(), it->end());
		double local_min = *min_element(it->begin(), it->end());
		if (local_max > max_value)
			max_value = local_max;
		if (local_min < min_value)
			min_value = local_min;
	}
	if (threshold > max_value || threshold < min_value)
	{
		MessageBox(main_window, "Threshold is out of range", "Error", 0);
		CD_array.resize(0);
		LWR = numeric_limits<double>::quiet_NaN();
		CD_type = false;
		average_CD = numeric_limits<double>::quiet_NaN();
		return;
	}
	if (input_image[0][0] > threshold)
		CD_type = false;
	else
		CD_type = true;
	vector<vector<double>>::iterator it1;
	for (vector<vector<double>>::iterator it = input_image.begin(), it1 = CD_array.begin(); it != input_image.end(); ++it, ++it1)
	{
		vector <double> cross_section = *it;
		if (cross_section[0] > threshold && CD_type)
		{
			vector<double>::iterator starting_point = find_if(cross_section.begin(), cross_section.end(), [threshold](auto e) {return e < threshold; });
			cross_section.assign(starting_point, cross_section.end());
		}
		if (cross_section[0] < threshold && !CD_type)
		{
			vector<double>::iterator starting_point = find_if(cross_section.begin(), cross_section.end(), [threshold](auto e) {return e > threshold; });
			cross_section.assign(starting_point, cross_section.end());
		}
		max_value = *max_element(cross_section.begin(), cross_section.end());
		min_value = *min_element(cross_section.begin(), cross_section.end());
		if (threshold > max_value || threshold < min_value)
		{
			MessageBox(main_window, "Threshold is out of range", "Error", 0);
			CD_array.resize(0);
			LWR = numeric_limits<double>::quiet_NaN();
			CD_type = false;
			average_CD = numeric_limits<double>::quiet_NaN();
			return;
		}
		vector <double> new_CD_array;
		GenerateCDVector(cross_section, threshold, new_CD_array, local_CD_type, main_window);
		transform(new_CD_array.begin(), new_CD_array.end(), new_CD_array.begin(), [&x_step](auto& c) {return c * x_step; });
		*it1 = new_CD_array;
	}
	int basic_cd_length = (int)CD_array[0].size();
	int averaging_length = (int)round(averaging_size / x_step);
	int num_values = 0;
	double val_sum = 0;
	double square_val_sum = 0;
	for (int n = 0; n < basic_cd_length; n++)
	{
		for (int m = 0; m < (int)CD_array.size() - averaging_length; m++)
		{
			double local_CD_sum = 0;
			int local_count = 0;
			for (int k = 0; k < averaging_length; k++)
			{
				if ((int)CD_array[m + k].size() > n)
				{
					local_CD_sum += CD_array[m + k][n];
					local_count++;
				}
				else
					MessageBox(main_window, "Different number of CDs detected at various cross sections, CD and LWR results may be inaccurate",
						"Warning", 0);
			}
			double local_CD = local_CD_sum / (double)local_count;
			val_sum += local_CD;
			square_val_sum += local_CD * local_CD;
			num_values++;
		}
	}
	LWR = 3.0*sqrt((square_val_sum - val_sum * val_sum / ((double)num_values)) / ((double)(num_values - 1)));
	average_CD = val_sum / ((double)num_values);
}

void FindEdges(vector <vector<double>> input_image, vector <vector<vector<double>>>& image_edges, double threshold, HWND main_window)
{
	if (!(int)input_image.size())
	{
		MessageBox(main_window, "Input image is empty in function FindEdges", "Error", 0);
		image_edges.resize(0);
		return;
	}
	if (!(int)input_image[0].size())
	{
		MessageBox(main_window, "Input image is empty in function FindEdges", "Error", 0);
		image_edges.resize(0);
		return;
	}
	double max_value = input_image[0][0];
	double min_value = input_image[0][0];
	for (vector<vector<double>>::iterator it = input_image.begin(); it != input_image.end(); ++it)
	{
		double local_max = *max_element(it->begin(), it->end());
		double local_min = *min_element(it->begin(), it->end());
		if (local_max > max_value)
			max_value = local_max;
		if (local_min < min_value)
			min_value = local_min;
	}
	if (threshold > max_value || threshold < min_value)
	{
		MessageBox(main_window, "Threshold is out of range in function FindEdges", "Error", 0);
		image_edges.resize(0);
		return;
	}
	image_edges.resize(0);
	list <vector<double>> edges;
	for (vector<vector<double>>::iterator it = input_image.begin(); it != input_image.end(); ++it)
	{
		double local_max = *max_element(it->begin(), it->end());
		double local_min = *min_element(it->begin(), it->end());
		if (threshold<local_min || threshold >local_max)
			continue;
		for (vector<double>::iterator it1 = it->begin(); it1 != it->end() - 1; ++it1)
		{
			if ((*it1 - threshold)*(*(it1 + 1) - threshold) < 0)
			{
				vector <double> current_point(2);
				current_point[0] = double(int(it - input_image.begin()));
				current_point[1] = double(int(it1 - it->begin())) + (threshold - *it1) / (*(it1 + 1) - *it1);
				edges.push_back(current_point);
			}
		}
	}
	double max_square_distance = 400;
	while ((int)edges.size() > 0)
	{
		vector<vector<double>> current_edge(1);
		current_edge[0] = *(edges.begin());
		cout << (int)edges.size() << endl;
		if ((int)edges.size() > 1)
		{
			bool is_found = true;
			while (is_found && (int)edges.size() > 1)
			{
				is_found = false;
				vector <double> second_element = *(next(edges.begin()));
				vector <double> first_element = *(edges.begin());
				double min_distance = (second_element[0] - first_element[0])*(second_element[0] - first_element[0]) +
					(second_element[1] - first_element[1])*(second_element[1] - first_element[1]);
				list<vector <double>>::iterator min_element = next(edges.begin());
				if ((int)edges.size() > 2)
				{
					for (list<vector<double>>::iterator it = next(next(edges.begin())); it != edges.end(); ++it)
					{
						vector <double> current_element = *it;
						double current_distance = (current_element[0] - first_element[0])*(current_element[0] - first_element[0]) +
							(current_element[1] - first_element[1])*(current_element[1] - first_element[1]);
						if (current_distance < min_distance)
						{
							min_distance = current_distance;
							min_element = it;
						}
					}
				}
				if (min_distance < max_square_distance)
				{
					*edges.begin() = *min_element;
					current_edge.push_back(*min_element);
					edges.erase(min_element);
					is_found = true;
				}
			}
		}
		edges.erase(edges.begin());
		image_edges.push_back(current_edge);
	}
}

void FindCNTCentersAndLCDU(vector <vector<vector<double>>> image_edges, int cut_size, int image_size_x, int image_size_y, vector<vector<double>>& centers_and_radius, double& CD_X, double& CD_Y, double& LCDU_X, double& LCDU_Y, HWND main_window)
{
	centers_and_radius.resize(image_edges.size());
	vector<vector<double>>::iterator c_it;
	c_it = centers_and_radius.begin();
	for (vector<vector<vector<double>>>::iterator it = image_edges.begin(); it != image_edges.end(); ++it, ++c_it)
	{
		double x_sum = 0;
		double y_sum = 0;
		bool is_out = false;
		c_it->resize(4);
		for (vector<vector<double>>::iterator it1 = it->begin(); it1 != it->end(); ++it1)
		{
			if ((*it1)[0] < (double)cut_size || (*it1)[0] >= (double)(image_size_x - cut_size)
				|| (*it1)[1] < (double)cut_size || (*it1)[1] >= (double)(image_size_x - cut_size))
			{
				is_out = true;
				break;
			}
			x_sum += (*it1)[0];
			y_sum += (*it1)[1];
		}
		if (is_out)
		{
			(*c_it)[0] = numeric_limits<double>::quiet_NaN();
			(*c_it)[1] = numeric_limits<double>::quiet_NaN();
			(*c_it)[2] = numeric_limits<double>::quiet_NaN();
			(*c_it)[3] = numeric_limits<double>::quiet_NaN();
			continue;
		}
		x_sum = x_sum / double(it->size());
		y_sum = y_sum / double(it->size());
		double Sxy2 = 0;
		double Sx4 = 0;
		double Sy4 = 0;
		double Sx2 = 0;
		double Sy2 = 0;
		for (vector<vector<double>>::iterator it1 = it->begin(); it1 != it->end(); ++it1)
		{
			double xi = (*it1)[0];
			double yi = (*it1)[1];
			Sx4 += (xi - x_sum)*(xi - x_sum)*(xi - x_sum)*(xi - x_sum);
			Sy4 += (yi - y_sum)*(yi - y_sum)*(yi - y_sum)*(yi - y_sum);
			Sx2 += (xi - x_sum)*(xi - x_sum);
			Sy2 += (yi - y_sum)*(yi - y_sum);
			Sxy2 += (xi - x_sum)*(xi - x_sum)*(yi - y_sum)*(yi - y_sum);
		}
		(*c_it)[0] = x_sum;
		(*c_it)[1] = y_sum;
		double a2 = (Sxy2*Sxy2 - Sx4 * Sy4) / (Sxy2*Sy2 - Sy4 * Sx2);
		(*c_it)[2] = sqrt(a2);
		double b2 = Sxy2 / (Sx2 - Sx4 / a2);
		(*c_it)[3] = sqrt(b2);
	}
	double val_x = 0;
	double val_y = 0;
	double square_val_x = 0;
	double square_val_y = 0;
	int num_values = 0;
	for (vector<vector<double>>::iterator it = centers_and_radius.begin(); it != centers_and_radius.end(); ++it)
	{
		double cd_x = (*it)[2];
		double cd_y = (*it)[3];
		if (!isnan(cd_x) && !isnan(cd_y))
		{
			val_x += cd_x;
			val_y += cd_y;
			square_val_x += cd_x * cd_x;
			square_val_y += cd_y * cd_y;
			num_values++;
		}
	}
	if (num_values < 1)
		MessageBox(main_window, "Too few contours detected to measure LCDU", "Error", 0);
	CD_X = val_x / ((double)num_values);
	CD_Y = val_y / ((double)num_values);
	LCDU_X = 3.0*sqrt((square_val_x - val_x * val_x / ((double)num_values)) / ((double)(num_values - 1)));
	LCDU_Y = 3.0*sqrt((square_val_y - val_y * val_y / ((double)num_values)) / ((double)(num_values - 1)));
}

void FilterImage(vector<vector<double>> input_image, vector<vector<double>>& output_image, int cut_size, source_types source_type, double f_unit, double sigma_out, double sigma_center, double sigma_radius, double x_step, HWND main_window)
{
	int image_size_1 = (int)input_image.size();
	int image_size_2 = (int)input_image[0].size();
	if (!image_size_1 || !image_size_2)
	{
		MessageBox(main_window, "Input image is empty for function FilterImage", "Error", 0);
		return;
	}
	double sigma;
	switch (source_type)
	{
	case conventional:
		sigma = 4.0 / (3.0*f_unit*(1.0 + sigma_out));
		break;
	case annular:
		sigma = 4.0 / (3.0*f_unit*(1.0 + sigma_out));
		break;
	case quadrupole:
		sigma = 4.0 / (3.0*f_unit*(sqrt((1 + sigma_radius) * (1 + sigma_radius) - 0.5*sigma_center*sigma_center) + sigma_center / sqrt(2.0)));
		break;
	case dipole:
		sigma = 4.0 / (3.0*f_unit*(sqrt((1 + sigma_radius) * (1 + sigma_radius) - sigma_center * sigma_center)));
	}
	sigma = round(sigma / x_step);
	int filter_size = (int)sigma * 4 + 1;
	int int_sigma = (int)sigma;
	vector<double> filter(filter_size);
	double sum_values = 0;
	for (int n = 0; n < filter_size; n++)
	{
		int local_index_n = n - 3 * int_sigma;
		double current_value = exp(-local_index_n * local_index_n / (2 * sigma*sigma));
		sum_values += current_value;
		filter[n] = current_value;
	}
	sum_values = 2 * sum_values;
	for (vector<double>::iterator it = filter.begin(); it != filter.end(); ++it)
		*it = (*it) / sum_values;
	output_image = input_image;
	for (int n = cut_size + filter_size; n < image_size_1 - cut_size - filter_size; n++)
	{
		for (int m = cut_size + filter_size; m < image_size_2 - cut_size - filter_size; m++)
		{
			output_image[n][m] = 0;
			for (int k = 0; k < filter_size; k++)
			{
				int local_index_k = k - 2 * int_sigma;
				output_image[n][m] += input_image[n][m + local_index_k] * filter[k];
			}
		}
	}
	for (int n = cut_size + filter_size; n < image_size_1 - cut_size - filter_size; n++)
	{
		for (int m = cut_size + filter_size; m < image_size_2 - cut_size - filter_size; m++)
		{
			for (int k = 0; k < filter_size; k++)
			{
				int local_index_k = k - 2 * int_sigma;
				output_image[n][m] += input_image[n + local_index_k][m] * filter[k];
			}
		}
	}

}

void FilterImage(double** m_dReconstructedImage_Data, vector<vector<double>>& output_image, int reconstructed_size_x,
	int reconstructed_size_y, int cut_size, source_types source_type, double f_unit, double sigma_out, double sigma_center, double sigma_radius, double x_step, HWND main_window)
{
	output_image = vector<vector<double>>(reconstructed_size_x, vector<double>(reconstructed_size_y, 0.0));
	double sigma;
	switch (source_type)
	{
	case conventional:
		sigma = 4.0 / (3.0*f_unit*(1.0 + sigma_out));
		break;
	case annular:
		sigma = 4.0 / (3.0*f_unit*(1.0 + sigma_out));
		break;
	case quadrupole:
		sigma = 4.0 / (3.0*f_unit*(sqrt((1 + sigma_radius) * (1 + sigma_radius) - 0.5*sigma_center*sigma_center) + sigma_center / sqrt(2.0)));
		break;
	case dipole:
		sigma = 4.0 / (3.0*f_unit*(sqrt((1 + sigma_radius) * (1 + sigma_radius) - sigma_center * sigma_center)));
	}
	sigma = round(sigma / x_step);
	int filter_size = (int)sigma * 4 + 1;
	int int_sigma = (int)sigma;
	vector<double> filter(filter_size);
	double sum_values = 0;
	for (int n = 0; n < filter_size; n++)
	{
		int local_index_n = n - 2 * int_sigma;
		double current_value = exp(-local_index_n * local_index_n / (2 * sigma*sigma));
		sum_values += current_value;
		filter[n] = current_value;
	}
	sum_values = 2 * sum_values;		// 3에서 2로 수정 하였음_2020/02/21
	for (vector<double>::iterator it = filter.begin(); it != filter.end(); ++it)
		*it = (*it) / sum_values;
	for (int n = cut_size + filter_size; n < reconstructed_size_x - cut_size - filter_size; n++)
	{
		for (int m = cut_size + filter_size; m < reconstructed_size_y - cut_size - filter_size; m++)
		{
			output_image[n][m] = 0;
			for (int k = 0; k < filter_size; k++)
			{
				int local_index_k = k - 2 * int_sigma;
				output_image[n][m] += m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m + local_index_k + n * reconstructed_size_x] * filter[k];
			}
		}
	}
	for (int n = cut_size + filter_size; n < reconstructed_size_x - cut_size - filter_size; n++)
	{
		for (int m = cut_size + filter_size; m < reconstructed_size_y - cut_size - filter_size; m++)
		{
			for (int k = 0; k < filter_size; k++)
			{
				int local_index_k = k - 2 * int_sigma;
				output_image[n][m] += m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m + (n + local_index_k) * reconstructed_size_x] * filter[k];
			}
		}
	}
	for (int n = cut_size + filter_size; n < reconstructed_size_x - cut_size - filter_size; n++)
		for (int m = cut_size + filter_size; m < reconstructed_size_y - cut_size - filter_size; m++)
			m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][m + n * reconstructed_size_x] = output_image[n][m];
}

int LowPassFilter(PyObject* pFunc, double** m_dReconstructedImage_Data, int reconstructed_size_x, int reconstructed_size_y, source_types source_type, double f_unit, double sigma_out, double sigma_center, double sigma_radius, double x_step)
{
	double sigma = 10.0;
	int int_sigma = round(sigma / x_step);

	PyObject* pArgs = NULL, *pValue = NULL;

	if (PyErr_Occurred())
		PyErr_Print();
	
		if (pFunc && PyCallable_Check(pFunc))
		{
			npy_intp input_dims[2]{ reconstructed_size_x, reconstructed_size_y };
			npy_intp output_dims[2]{ reconstructed_size_x, reconstructed_size_y };
			npy_intp sigma_dims[1]{ 2 };
			double* sigma_array;
			double*	python_output;
			sigma_array = new double[2];
			sigma_array[0] = sigma / x_step;
			sigma_array[1] = sigma / x_step;
			const int ND{ 2 };
			const int ND1{ 1 };
			try
			{
				PyArrayObject* py_input_image = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(m_dReconstructedImage_Data[DETECTOR_INTENSITY_1])));
				PyArrayObject* py_sigma = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND1, sigma_dims, NPY_DOUBLE, reinterpret_cast<void*>(sigma_array)));
				pArgs = PyTuple_New(2);
				PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(py_input_image));
				PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(py_sigma));
				pValue = PyObject_CallObject(pFunc, pArgs);
				PyArrayObject* filtered_image = reinterpret_cast<PyArrayObject*>(pValue);
				if (PyErr_Occurred())
					PyErr_Print();
				python_output = reinterpret_cast<double*>(PyArray_DATA(filtered_image));
				for (int n = 0; n < reconstructed_size_x*reconstructed_size_y; n++)
					m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][n] = python_output[n];
				Py_DECREF(pArgs);
				Py_DECREF(pValue);
				delete[] sigma_array;
			}
			catch (...)
			{
				if (pArgs != NULL) Py_DECREF(pArgs);
				if (pValue != NULL) Py_DECREF(pValue);
				delete[] sigma_array;

				throw;
			}
		}	
	return 0;
}

int LowPassFilterNew(PyObject* pFunc, double* ImageData, int reconstructed_size_x, int reconstructed_size_y, double x_step, double sigma)
{
	//double sigma = 29.69;		// Roman cut off freq 조정 2021.01.25
	int int_sigma = round(sigma / x_step);

	PyObject* pArgs = NULL, *pValue = NULL;

	if (PyErr_Occurred())
		PyErr_Print();

	if (pFunc && PyCallable_Check(pFunc))
	{
		npy_intp input_dims[2]{ reconstructed_size_x, reconstructed_size_y };
		npy_intp output_dims[2]{ reconstructed_size_x, reconstructed_size_y };
		npy_intp sigma_dims[1]{ 2 };
		double* sigma_array;
		double*	python_output;
		sigma_array = new double[2];
		sigma_array[0] = sigma / x_step;
		sigma_array[1] = sigma / x_step;
		const int ND{ 2 };
		const int ND1{ 1 };
		try
		{
			PyArrayObject* py_input_image = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(ImageData)));
			PyArrayObject* py_sigma = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND1, sigma_dims, NPY_DOUBLE, reinterpret_cast<void*>(sigma_array)));
			pArgs = PyTuple_New(2);
			PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(py_input_image));
			PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(py_sigma));
			pValue = PyObject_CallObject(pFunc, pArgs);
			PyArrayObject* filtered_image = reinterpret_cast<PyArrayObject*>(pValue);
			if (PyErr_Occurred())
				PyErr_Print();
			python_output = reinterpret_cast<double*>(PyArray_DATA(filtered_image));
			memcpy(ImageData, python_output, sizeof(double)*reconstructed_size_x*reconstructed_size_y);
			//for (int n = 0; n < reconstructed_size_x*reconstructed_size_y; n++)
				//ImageData[n] = python_output[n];
			Py_DECREF(pArgs);
			Py_DECREF(pValue);
			delete[] sigma_array;
		}
		catch (...)
		{
			if (pArgs != NULL) Py_DECREF(pArgs);
			if (pValue != NULL) Py_DECREF(pValue);
			delete[] sigma_array;

			throw;
		}
	}
	return 0;
}

// 20201218 이전 ihlee
//int LowPassFilter(PyObject* pFunc, double** m_dReconstructedImage_Data, int reconstructed_size_x, int reconstructed_size_y, source_types source_type, double f_unit, double sigma_out, double sigma_center, double sigma_radius, double x_step)
//{
//	double sigma = 10.0;
//	/*switch (source_type)
//	{
//	case conventional:
//		sigma = 4.0 / (3.0*f_unit*(1.0 + sigma_out));
//		break;
//	case annular:
//		sigma = 4.0 / (3.0*f_unit*(1.0 + sigma_out));
//		break;
//	case quadrupole:
//		sigma = 4.0 / (3.0*f_unit*(sqrt((1 + sigma_radius) * (1 + sigma_radius) - 0.5*sigma_center*sigma_center) + sigma_center / sqrt(2.0)));
//		break;
//	case dipole:
//		sigma = 4.0 / (3.0*f_unit*(sqrt((1 + sigma_radius) * (1 + sigma_radius) - sigma_center * sigma_center)));
//	}*/
//	int int_sigma = round(sigma / x_step);
//	//PyObject* pName, *pModule, *pFunc;
//	PyObject* pArgs, *pValue;
//	//Py_Initialize();
//	//import_array();
//	//pName = PyUnicode_FromString("interpolation_try");
//	//pModule = PyImport_Import(pName);
//	//Py_DECREF(pName);
//	if (PyErr_Occurred())
//		PyErr_Print();
//	//if (pModule != NULL) 
//	{
//		//pFunc = PyObject_GetAttrString(pModule, "filter_image");
//		if (pFunc && PyCallable_Check(pFunc))
//		{
//			npy_intp input_dims[2]{ reconstructed_size_x, reconstructed_size_y };
//			npy_intp output_dims[2]{ reconstructed_size_x, reconstructed_size_y };
//			npy_intp sigma_dims[1]{ 2 };
//			double* sigma_array;
//			double*	python_output;
//			sigma_array = new double[2];
//			sigma_array[0] = sigma / x_step;
//			sigma_array[1] = sigma / x_step;
//			const int ND{ 2 };
//			const int ND1{ 1 };
//			PyArrayObject* py_input_image = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND, input_dims, NPY_DOUBLE, reinterpret_cast<void*>(m_dReconstructedImage_Data[DETECTOR_INTENSITY_1])));
//			PyArrayObject* py_sigma = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(ND1, sigma_dims, NPY_DOUBLE, reinterpret_cast<void*>(sigma_array)));
//			pArgs = PyTuple_New(2);
//			PyTuple_SetItem(pArgs, 0, reinterpret_cast<PyObject*>(py_input_image));
//			PyTuple_SetItem(pArgs, 1, reinterpret_cast<PyObject*>(py_sigma));
//			pValue = PyObject_CallObject(pFunc, pArgs);
//			PyArrayObject* filtered_image = reinterpret_cast<PyArrayObject*>(pValue);
//			if (PyErr_Occurred())
//				PyErr_Print();
//			python_output = reinterpret_cast<double*>(PyArray_DATA(filtered_image));
//			for (int n = 0; n < reconstructed_size_x*reconstructed_size_y; n++)
//				m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][n] = python_output[n];
//			Py_DECREF(pArgs);
//			Py_DECREF(pValue);
//			delete[] sigma_array;
//		}
//		//Py_XDECREF(pFunc);
//		//Py_DECREF(pModule);
//	}
//	//Py_Finalize();
//	return 0;
//}


BOOL FindCorner(double** m_dRawImage_Data, int raw_size_x, int raw_size_y, double th, int* result_x, int* result_y)		// 새로 고민 필요함
{
	BOOL bRet = FALSE;

	int size_y = raw_size_y;
	int size_x = raw_size_x;

	int left_line1 = 0;
	int left_line2 = 0;
	int Right_line1 = 0;
	int Right_line2 = 0;


	if (size_y == 250)
	{
		left_line1 = 20;
		left_line2 = 30;
		Right_line1 = 220;
		Right_line2 = 230;
	}
	else
	{
		left_line1 = 10;
		left_line2 = 20;
		Right_line1 = 105;
		Right_line2 = 115;
	}


	float x_mask[1][5] = { 0, 0, 0, 0, 0 };
	float y_mask[5][1] = { {0},{0},{0},{0},{0} };
	float total_mask[5][5] = { {0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0} };

	double threshold = th;


	// Data information acquisition

	// Auto threshold 및 다양한 기능 추가하기 위한 부분.....
	vector<vector<double> > Preparation(size_y, vector<double>(size_x, 0.0));

	for (int y = 0; y < size_y; y++)
	{
		for (int x = 0; x < size_x; x++)
		{
			Preparation[y][x] = m_dRawImage_Data[DETECTOR_INTENSITY_1][x + size_x * y];
		}
	}


	// Data preparation to 2d array
	vector<vector<double> > ptr(size_y, vector<double>(size_x, 0.0));

	double xx, yy;

	for (int y = 0; y < size_y; y++)
	{
		for (int x = 0; x < size_x; x++)
		{
			if (m_dRawImage_Data[DETECTOR_INTENSITY_1][x + size_x * y] > threshold)
			{
				ptr[y][x] = 255;
			}
			else
			{
				ptr[y][x] = 0;
			}
		}
	}

	// Mean filter
	vector<vector<double> > mean(size_y, vector<double>(size_x, 0.0));

	for (int y = 1; y < size_y - 2; y++)
	{
		for (int x = 1; x < size_x - 2; x++)
		{
			mean[y][x] = (ptr[y - 1][x - 1] + ptr[y - 1][x] + ptr[y - 1][x + 1] + ptr[y][x - 1] + ptr[y][x + 1] + ptr[y + 1][x - 1] + ptr[y + 1][x] + ptr[y + 1][x + 1]) / 8;
		}
	}

	// Double threshold
	vector<vector<double> > dfilter(size_y, vector<double>(size_x, 0.0));
	for (int y = 0; y < size_y; y++)
	{
		for (int x = 0; x < size_x; x++)

		{
			if (mean[y][x] > 120)
			{
				dfilter[y][x] = 255;
			}
			else
			{
				dfilter[y][x] = 0;
			}

		}
	}

	// Edge detection
	vector<vector<double> > edgeImage(size_y, vector<double>(size_x, 0.0));
	double eval_X = 0;
	double eval_Y = 0;

	for (int y = 2; y < size_y - 2; y++)
	{
		for (int x = 2; x < size_x - 2; x++)
		{
			eval_X = dfilter[y][x + 1] - dfilter[y][x - 1];
			eval_Y = dfilter[y + 1][x] - dfilter[y - 1][x];

			if (eval_X == 0 && eval_Y == 0)
			{
				edgeImage[y][x] = 0;
			}
			else
			{
				edgeImage[y][x] = 255;
			}
		}
	}

	// X-Y position calculation
	int x_val1 = 0, x_cnt1 = 0, x_val2 = 0, x_cnt2 = 0;
	int y_val1 = 0, y_cnt1 = 0, y_val2 = 0, y_cnt2 = 0;

	for (int y = left_line1; y < left_line2; y++)
	{
		for (int x = 3; x < size_x - 4; x++)
		{
			if (edgeImage[y][x] > 100)
			{
				x_val1 = x + x_val1;
				x_cnt1 = x_cnt1 + 1;
			}
		}
	}

	for (int y = Right_line1; y < Right_line2; y++)
	{
		for (int x = 3; x < size_x - 4; x++)
		{
			if (edgeImage[y][x] > 100)
			{
				x_val2 = x + x_val2;
				x_cnt2 = x_cnt2 + 1;
			}
		}
	}

	for (int y = 3; y < size_y - 4; y++)
	{
		for (int x = left_line1; x < left_line2; x++)
		{
			if (edgeImage[y][x] > 100)
			{
				y_val1 = y + y_val1;
				y_cnt1 = y_cnt1 + 1;
			}
		}
	}

	for (int y = 3; y < size_y - 4; y++)
	{
		for (int x = Right_line1; x < Right_line2; x++)
		{
			if (edgeImage[y][x] > 100)
			{
				y_val2 = y + y_val2;
				y_cnt2 = y_cnt2 + 1;
			}
		}
	}

	double x1 = 0, x2 = 0, y1 = 0, y2 = 0;

	if (x_cnt1 != 0)
		x1 = int(x_val1 / x_cnt1);
	if (x_cnt2 != 0)
		x2 = int(x_val2 / x_cnt2);
	if (y_cnt1 != 0)
		y1 = int(y_val1 / y_cnt1);
	if (y_cnt2 != 0)
		y2 = int(y_val2 / y_cnt2);

	double x_pos = 0, y_pos = 0;

	if (x1 > x2)
	{
		x_pos = x1;
	}
	else
	{
		x_pos = x2;
	}

	if (y1 > y2)
	{
		y_pos = y1;
	}
	else
	{
		y_pos = y2;
	}

	//CString msg;
	//msg.Format("X의 위치값은 %0.0f이고 Y의 위치값은 %0.0f 입니다", x_pos, y_pos);
	//AfxMessageBox(msg);

	//AfxMessageBox("이미지 코너를 찾는 알고리즘이 끝났습니다.");

	*result_x = (int)x_pos;
	*result_y = (int)y_pos;

	return TRUE;
}




BOOL FindCorner_all(double** m_dRawImage_Data, int raw_size_x, int raw_size_y, double th, int* result_x, int* result_y)		// 새로 고민 필요함
{
	BOOL bRet = FALSE;

	int x_pos = 0;
	int y_pos = 0;

	int pixelNum = raw_size_x;

	double LTEdge = 0;
	double RTEdge = 0;
	double LBEdge = 0;
	double RBEdge = 0;

	double mlSignalLevel = 0;
	double absSignalLevel = 0;

	double mlSumValue = 0;
	int mlCnt = 0;

	double absSumValue = 0;
	int absCnt = 0;

	double cutOffTh = 0;


	// Auto threshold 및 다양한 기능 추가하기 위한 부분.....
	vector<vector<double> > Preparation(pixelNum, vector<double>(pixelNum, 0.0));

	for (int y = 0; y < pixelNum; y++)
	{
		for (int x = 0; x < pixelNum; x++)
		{
			Preparation[y][x] = m_dRawImage_Data[DETECTOR_INTENSITY_1][x + pixelNum * y];
		}
	}

	LTEdge = (Preparation[0][0] + Preparation[0][1] + Preparation[1][0] + Preparation[1][1]) / 4;
	RTEdge = (Preparation[0][pixelNum] + Preparation[0][pixelNum - 1] + Preparation[1][pixelNum] + Preparation[1][pixelNum - 1]) / 4;
	LBEdge = (Preparation[pixelNum][0] + Preparation[pixelNum][1] + Preparation[pixelNum - 1][0] + Preparation[pixelNum - 1][1]) / 4;
	RBEdge = (Preparation[pixelNum][pixelNum] + Preparation[pixelNum][pixelNum - 1] + Preparation[pixelNum - 1][pixelNum] + Preparation[pixelNum - 1][pixelNum - 1]) / 4;


	if (LTEdge > th)
	{
		mlSumValue = mlSumValue + LTEdge;
		mlCnt++;
	}
	else if (LTEdge < th)
	{
		absSumValue = absSumValue + LTEdge;
		absCnt++;
	}

	if (RTEdge > th)
	{
		mlSumValue = mlSumValue + RTEdge;
		mlCnt++;
	}
	else if (RTEdge < th)
	{
		absSumValue = absSumValue + RTEdge;
		absCnt++;
	}

	if (LBEdge > th)
	{
		mlSumValue = mlSumValue + LBEdge;
		mlCnt++;
	}
	else if (LBEdge < th)
	{
		absSumValue = absSumValue + LBEdge;
		absCnt++;
	}

	if (RBEdge > th)
	{
		mlSumValue = mlSumValue + RBEdge;
		mlCnt++;
	}
	else if (RBEdge < th)
	{
		absSumValue = absSumValue + RBEdge;
		absCnt++;
	}

	mlSignalLevel = mlSumValue / mlCnt;
	absSignalLevel = absSumValue / absCnt;
	cutOffTh = (mlSignalLevel - absSignalLevel) * 0.2 + absSignalLevel;


	// 오늘 작업 시작 량....
	// Cutoff Threshold setting
	vector<vector<double> > ptr(pixelNum, vector<double>(pixelNum, 0.0));

	for (int y = 0; y < pixelNum; y++)
	{
		for (int x = 0; x < pixelNum; x++)
		{
			if (m_dRawImage_Data[DETECTOR_INTENSITY_1][x + pixelNum * y] > cutOffTh)
			{
				ptr[y][x] = 255;
			}
			else
			{
				ptr[y][x] = 0;
			}
		}
	}

	// Conventional Sovel

	int MaskSobelX[3][3] = { {-1,0,1},
							 {-2,0,2},
							 {-1,0,1} };

	int MaskSobelY[3][3] = { {1,2,1},
							 {0,0,0},
							 {-1,-2,-1} };


	int *pImgSobelX, *pImgSobelY;




	// Uniform 이미지 중에서 Edge를 검출하는 알고리즘
	// Edge detection을 좀더 정교하게 수정 하였음... KYD
	vector<vector<double> > edgeImage(pixelNum, vector<double>(pixelNum, 0.0));
	double eval_X = 0;
	double eval_Y = 0;

	for (int y = 0; y < pixelNum - 1; y++)
	{
		for (int x = 0; x < pixelNum - 1; x++)
		{
			eval_X = ptr[y][x + 1] - ptr[y][x - 1];
			eval_Y = ptr[y + 1][x] - ptr[y - 1][x];

			if (eval_X == 0 && eval_Y == 0)
			{
				edgeImage[y][x] = 0;
			}
			else
			{
				edgeImage[y][x] = 255;
			}
		}
	}




	// Mean 및 double threshold 필터링은 일단 넣지 않는 것으로 함. -> 예전에 센서 노이즈가 너무 심할때 사용했던 것이기 때문에...
	//-------------------------------------




	//-----------------------------------



	for (int x = 0; x < pixelNum; x++)
	{
		if (Preparation[0][x] > th)
		{
			mlSumValue = mlSumValue + Preparation[0][x];
			mlCnt++;

		}

		int initial = Preparation[0][x];


		if (Preparation[0][x]);
	}


	// Pre-process



	// Calculation
	*result_x = (int)x_pos;
	*result_y = (int)y_pos;

	return TRUE;
}

int PythonInitailize()
{
	import_array();//이거 한번만 실행해도 되는지 테스트 필요...

	//return 0;
}

