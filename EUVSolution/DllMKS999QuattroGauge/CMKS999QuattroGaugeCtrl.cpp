#include "stdafx.h"
#include "CMKS999QuattroGaugeCtrl.h"


CMKS999QuattroGaugeCtrl::CMKS999QuattroGaugeCtrl()
{
	m_strPressure = "OFFLINE";
	m_dPressure = -1.0;
	m_nStatusCode = 0;

	m_strReceiveEndOfStreamSymbol = _T("");
	m_hStateOkEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
}


CMKS999QuattroGaugeCtrl::~CMKS999QuattroGaugeCtrl()
{
	if (m_hStateOkEvent != NULL)
		CloseHandle(m_hStateOkEvent);
}

void CMKS999QuattroGaugeCtrl::initialize()
{
	
}

int CMKS999QuattroGaugeCtrl::OpenPort(CString strPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
{
	return CSerialCom::OpenSerialPort(strPortName, dwBaud, wByte, wStop, wParity);
}


int CMKS999QuattroGaugeCtrl::ClosePort(void)
{
	int ret = 0;

	return ret;
}

int	CMKS999QuattroGaugeCtrl::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int ret = 0;

	ResetEvent(m_hStateOkEvent);	//Sets the specified event object to the nonsignaled state.

	m_strSendMsg = lParam;

	ret = Send(lParam, nTimeOut);

	ret = WaitEvent(m_hStateOkEvent, nTimeOut);

	if (ret != 0)
		m_strSendMsg = _T("OFFLINE");

	SaveLogFile("VGauge_Com", _T((LPSTR)(LPCTSTR)("PC -> VGuage : " + m_strSendMsg)));	//통신 상태 기록.

	return ret;
}

int CMKS999QuattroGaugeCtrl::ReceiveData(char *lParam, DWORD nSize)
{
	CString str;

	/////////// Confirm the termination string 'FF' //////////////////////
	static BYTE buffer[512];
	static int total = 0;
	int i = 0;
	for (i = 0; i < nSize; i++)
	{
		buffer[total] = lParam[i];
		total++;
	}	
	if (buffer[total - 1] != 'F' || buffer[total - 2] != 'F')
		return 0;
	//	m_strReceiveMsg = (CString)buffer;
	memset(buffer, '\0', 512);
	total =  0;
	/////////////////////////////////////////////////////////////////////

	m_strReceivedMsg = lParam;
	SaveLogFile("VGauge_Com", _T((LPSTR)(LPCTSTR)("VGuage -> PC : " + m_strReceivedMsg))); //통신 상태 기록.

	int len = m_strReceivedMsg.GetLength();
	if (m_strSendMsg.Find("001PR3") != -1 && len == 17)
	{
		m_strReceivedMsg = m_strReceivedMsg.Mid(m_strReceivedMsg.Find("K") + 1, m_strReceivedMsg.Find(";") - (m_strReceivedMsg.Find("K") + 1));
		m_strPressure = m_strReceivedMsg + " Torr";
		m_dPressure = atof(m_strReceivedMsg);
	}
	//else if (m_strSendMsg.Find("001T") != -1 && len ==  )	// 상태 Code를 읽어오는 부분...아래 정리 필요
	//{
	//	m_nStatusCode =
	//}

	//Received 데이터가 정상 데이터이면 Signal 해준다.
	SetEvent(m_hStateOkEvent);	//Sets the specified event object to the signaled state.

	return 1;
}

int CMKS999QuattroGaugeCtrl::Command_ReadingGauge()
{
	int ret = 0;
	char chBuf[125];
	memset(chBuf, '\0', 125);

	sprintf(chBuf, "@001PR3?;FF");		//measure pressure combination of all
	if (m_bSerialConnected)
		ret = SendData(chBuf);

	return ret;
}

int CMKS999QuattroGaugeCtrl::Command_GetStatusGauge()
{
	int ret = 0;
	char chBuf[125];
	memset(chBuf, '\0', 125);

	sprintf(chBuf, "@001T?;FF");	//Transducer Status 
	if(m_bSerialConnected)
		ret = SendData(chBuf);

	return ret;
}

double CMKS999QuattroGaugeCtrl::GetValue()
{
	return m_dPressure;
}
