/**
 * Serial Communication Class
 *
 * Description: A common function set using at all project.
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

#define LOG_PATH								_T("D:\\EUVSolution\\Log")
#define COMMUNICATION_TIMEOUT		-99

class AFX_EXT_CLASS CECommon
{
public:
	CECommon();
	~CECommon();
	
	HANDLE				m_hMutex;
	void				MutexLock(HANDLE handle) { WaitForSingleObject(handle, -1); };
	void				MutexUnlock(HANDLE handle) { ReleaseMutex(handle); };

	HANDLE					m_hStateOkEvent;
	int						WaitEvent(HANDLE hHandle, int nTimeOut = 60000);										//

	/**
	* description:		모든 Window Message가 처리되며, Loop 대기하고 싶은경우 사용(주로 for, while 문 안에서)
	* return:			void
	*/
	void				ProcessMessages(void);

	/**
	* description:		모든 Window Message가 처리되며, 일정 시간(초단위) 대기하고 싶은경우 사용.
	* parameter[in]:	double sec : 기다릴 초단위 시간
	* return:			void
	*/
	void				WaitSec(double sec);

	/**
	* description:		현 시스템의 년,월,일,시,분,초 정보 알고 싶을 때 사용
	* parameter[out]:	strDate: 년,월,일 정보 가져옴 
	* parameter[out]:	strTime: 시간, 분, 초 정보 가져옴(msec까지)
	* return			CTime: PC 년,월,일,시,분,초 정보 리턴
	*/
	CTime				GetSystemDateTime(CString* strDate, CString* strTime);

	/**
	* description:		로그 경로 초기화
	* parameter[in]:	strLogPath : 로그파일 경로(시스템마다 다를 수 있다.)
	* return			int : 에러여부 및 내용
	*/
	virtual int			SetLogPath(CString strLogPath);
	CString				m_strLogPath;

	/**
	* description:		Operation,Error,특정 File 명으로 로그를 만드는 함수
	* parameter[in]:	logFilename : Log File Name
	* parameter[in]:	logMessage : 기록할 로그
	* @return			void
	*/
	void				SaveLogFile(char* logFilename, char* logMessage);

	/**
	* description:		Operation,Error,특정 File 명으로 로그를 만드는 함수
	* parameter[in]:	logFilename : Log File Name
	* parameter[in]:	logMessage : 기록할 로그
	* parameter[in]:	logTime : 로그 타임
	* @return			void
	*/
	void				SaveLogFile(CString logFilename, CString logMessage);

	/**
	 * Discription:		Error정보를 File로 저장하는 함수. 별도로 필요?
	 * parameter[in]:	pchErrCode : Error Code number
	 * parameter[in]:	pchErrName : Error Name
	 * parameter[in]:	pchErrData : Error Data
	 * return : void
	 */
	//void				SaveErrorLog(char* pchErrCode, char *pchErrName, char *pchErrData);

	/**
	* description:		폴더 유무 확인 함수
	* parameter[in]:	strFolderName : 폴더 풀 Path 네임
	* return			BOOL
	*/
	BOOL				IsFolderExist(CString strFolderName);

	/**
	* description		파일 유무 확인 함수
	* parameter[in]:	strFileName : 파일 풀 Path 네임
	* return			BOOL
	*/
	BOOL				IsFileExist(CString strFileName);
	   	 
	/**
	* description		Directory내 파일들을 모두 복사하는 함수, 하위 디렉토리는 복사되지 않음
	* parameter[in]:	SrcDir : 파일 풀 Path 네임
	* return			BOOL
	*/
	BOOL				CopyFiles(CString SrcFolder, CString DstFolder);// 

	/**
	* description		디렉토리 삭제 
	* parameter[in]     strPath 삭제할 폴더 풀 Path
	* return			BOOL
	*/
	int					DeleteFolder(CString strPath);
};

