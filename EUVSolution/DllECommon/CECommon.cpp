#include "stdafx.h"
#include "CECommon.h"


CECommon::CECommon()
{
	m_hMutex = CreateMutex(NULL, FALSE, "");
	m_strLogPath = LOG_PATH;
	m_hStateOkEvent = CreateEvent(NULL, TRUE, FALSE, NULL);		//If bInitialState is FALSE, the initial state of the event object is nonsignaled.
}

CECommon::~CECommon()
{
	CloseHandle(m_hMutex);
	CloseHandle(m_hStateOkEvent);
	m_hMutex = NULL;
	m_hStateOkEvent = NULL;
}

int	CECommon::WaitEvent(HANDLE hHandle, int nTimeOut)
{
	int nRet = 0;

	DWORD flagWait = -1;
	LARGE_INTEGER ferquency = { 0, };
	LARGE_INTEGER start = { 0, };
	LARGE_INTEGER end = { 0, };
	double duration = 0;

	QueryPerformanceFrequency(&ferquency);
	QueryPerformanceCounter(&start);
	duration = 0;

	MSG msg;
	while (duration < nTimeOut && flagWait != WAIT_OBJECT_0)
	{
		Sleep(1);
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		//ProcessMessages();
		//WaitSec(1);
		QueryPerformanceCounter(&end);
		duration = ((end.QuadPart - start.QuadPart) / (double)ferquency.QuadPart) * 1000; //ms second
		flagWait = ::WaitForSingleObject(hHandle, 0);	//Waits until the specified object is in the signaled state or the time-out interval elapses.
														//If dwMilliseconds is zero, the function does not enter a wait state if the object is not signaled
	}

	if (flagWait != WAIT_OBJECT_0)
	{
		nRet = COMMUNICATION_TIMEOUT;
	}

	return nRet;
}

int CECommon::SetLogPath(CString strLogPath)
{
	int nRet = 0;

	if (strLogPath == _T(""))
		return -1;

	m_strLogPath = strLogPath;

	return nRet;
}

void CECommon::SaveLogFile(char* logFilename, char* logMessage)
{
	if (m_strLogPath == _T(""))
		return;

	MutexLock(m_hMutex);

	if (!IsFolderExist(m_strLogPath))
	{
		CreateDirectory(m_strLogPath, NULL);
	}

	CString strLogDatePath, strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	strLogDatePath.Format(_T("%s\\%s"), m_strLogPath, strDate);
	if (!IsFolderExist(strLogDatePath))
	{
		CreateDirectory(strLogDatePath, NULL);
	}

	//switch (logType) {
	//case ERRORLOG:
	//	strFullPathname = strFolderName + "\\" + "error.log";
	//	break;
	//case OPERATIONLOG:
	//	strFullPathname = strFolderName + "\\" + "operation.log";
	//	break;
	//case USERLOG:
	//	strFullPathname = strFolderName + "\\" + logFilename + ".log";
	//	break;
	//default:
	//	break;
	//}

	CString strLogFullPath;
	strLogFullPath.Format(_T("%s\\%s.log"), strLogDatePath, logFilename);

	FILE *fp = NULL;
	fp = fopen((char *)(LPCSTR)strLogFullPath, "a");
	if (fp == NULL)
	{
		MutexUnlock(m_hMutex);
		return;
	}

	CString str;
	//str.Format("%s %s\r\n", strTime, logMessage);
	str.Format("%s %s\n", strTime, logMessage);
	fputs(str, fp);
	fclose(fp);

	MutexUnlock(m_hMutex);
}

void CECommon::SaveLogFile(CString logFilename, CString logMessage)
{
	if (m_strLogPath == _T(""))
		return;

	MutexLock(m_hMutex);

	if (!IsFolderExist(m_strLogPath))
	{
		CreateDirectory(m_strLogPath, NULL);
	}

	CString strLogDatePath, strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	strLogDatePath.Format(_T("%s\\%s"), m_strLogPath, strDate);
	if (!IsFolderExist(strLogDatePath))
	{
		CreateDirectory(strLogDatePath, NULL);
	}

	CString strLogFullPath;
	strLogFullPath.Format(_T("%s\\%s.log"), strLogDatePath, logFilename);

	FILE *fp = NULL;
	fp = fopen((char *)(LPCSTR)strLogFullPath, "a");
	if (fp == NULL)
	{
		MutexUnlock(m_hMutex);
		return;
	}

	CString str;
	//str.Format("%s %s\r\n", strTime, logMessage);
	str.Format("%s %s\n", strTime, logMessage);
	fputs(str, fp);
	fclose(fp);

	MutexUnlock(m_hMutex);
}


//
//void CECommon::SaveErrorLog(char* pchErrCode, char *pchErrName, char *pchErrData)
//{
//	if (m_strLogPath == _T(""))
//		return;
//
//	FILE *fp = NULL;
//	CString strFolderName;
//
//	CString dir;
//
//	dir = m_strLogPath;
//
//	if (!IsDirExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//	char strdirname[100];
//	GetSystemDate(strdirname);
//	dir += "\\";
//	dir += strdirname;
//	if (!IsDirExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//	CString filename;
//	filename = dir + "\\alarm.log";
//
//
//	fp = fopen((char *)(LPCSTR)filename, "a");
//	if (fp == NULL)
//		return;
//
//	char strtime[100];
//	GetSystemTime(strtime);
//	CString strErrorLog = "";
//	CString strErrorLogWithTime = "";
//
//	if (pchErrData == NULL || 0 == strcmp(pchErrData, ""))
//	{
//		strErrorLog.Format("[%s] [%s]", pchErrCode, pchErrName);
//	}
//	else
//	{
//		strErrorLog.Format("[%s] [%s] [%s]", pchErrCode, pchErrName, pchErrData);
//	}
//
//	strErrorLogWithTime.Format("%s %s \r\n", strtime, strErrorLog);
//
//	fputs(strErrorLogWithTime, fp);
//	fclose(fp);
//}

CTime CECommon::GetSystemDateTime(CString* strDate, CString* strTime)
{
	SYSTEMTIME	systemtime;
	::GetSystemTime(&systemtime);
	int msec = systemtime.wMilliseconds;
	CTime ctime = CTime::GetCurrentTime();
	ctime.GetAsSystemTime(systemtime);

	if (strDate)
		strDate->Format(_T("%04d%02d%02d"), ctime.GetYear(), ctime.GetMonth(), ctime.GetDay());
	if (strTime)
		strTime->Format(_T("[ %02d:%02d:%02d.%03d ]"), ctime.GetHour(), ctime.GetMinute(), ctime.GetSecond(), msec);

	return ctime;
}

void CECommon::ProcessMessages(void)
{
	MSG msg;
	while (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void CECommon::WaitSec(double sec)
{
	MSG msg;
	clock_t clockt = clock();
	while (sec - (((clock() - clockt) / CLOCKS_PER_SEC)) > 0)
	{
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}

BOOL CECommon::IsFolderExist(CString strFolderName)
{
	CFileFind filefind;
	BOOL bFolderExist = FALSE;

	if (filefind.FindFile(strFolderName) == TRUE)
	{
		filefind.FindNextFile();
		if (filefind.IsDirectory() == TRUE)
			bFolderExist = TRUE;
	}

	return bFolderExist;
}

BOOL CECommon::IsFileExist(CString strFileName)
{
	if (strFileName.IsEmpty())
		return FALSE;

	if (GetFileAttributes(strFileName) == -1)
		return FALSE;
	else
		return TRUE;

	return 0;
}

int CECommon::DeleteFolder(CString strPath)
{
	int nRet = 0;
	CFileFind finder;
	BOOL bProcessing = TRUE;
	CString strDirFile = strPath + CString(_T("\\*.*"));
	bProcessing = finder.FindFile(strDirFile);

	while (bProcessing)
	{
		bProcessing = finder.FindNextFileA();
		if (finder.IsDots())
		{
			continue;
		}

		if (finder.IsDirectory())
		{
			DeleteFolder(finder.GetFilePath());
		}
		else
		{
			::DeleteFile(finder.GetFilePath());
		}
	}

	finder.Close();
	::RemoveDirectory(strPath);

	return nRet;
}

BOOL CECommon::CopyFiles(CString SrcFolder, CString DstFolder)
{
	if (!IsFolderExist(DstFolder))
	{
		return 0;
	}

	CFileFind finder;
	BOOL bProcessing = finder.FindFile(SrcFolder + "\\*.*");

	while (bProcessing)
	{
		bProcessing = finder.FindNextFile();

		if (finder.IsDots() || finder.IsDirectory())
		{
			continue;
		}
		else
		{
			CString FileName, SrcFile, DstFile;

			FileName = finder.GetFileName();

			SrcFile = SrcFolder + "\\" + FileName;
			DstFile = DstFolder;
			DstFile += "\\";
			DstFile += FileName;

			CopyFile(SrcFile, DstFile, TRUE);
		}
	}

	return TRUE;
}