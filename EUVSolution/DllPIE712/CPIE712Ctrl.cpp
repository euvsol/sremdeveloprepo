#include "stdafx.h"
#include "CPIE712Ctrl.h"
#include <fstream>
#include "math.h"

using namespace std;

CPIE712Ctrl::CPIE712Ctrl()
{
	m_nPIStage_ID = 0;
	m_bConnect = FALSE;
	m_bScanStop = FALSE;
	m_nScanNum = 0;
	strcpy(m_chPIStage_Axis, "1 2 3 4 5");

	int i = 0;
	for (i = 0; i < AXIS_NUMBER; i++)
	{
		m_piDdlTableIdsArray[i] = i + 1;
		m_piWaveGeneratorIdsArray[i] = i + 1;
	}

	m_dPIStage_MovePos[X_AXIS] = X_INITIAL_POS_UM;
	m_dPIStage_MovePos[Y_AXIS] = Y_INITIAL_POS_UM;
	m_dPIStage_MovePos[Z_AXIS] = Z_INITIAL_POS_UM;
	m_dPIStage_MovePos[TIP_AXIS] = m_dInitialTx_urad;
	m_dPIStage_MovePos[TILT_AXIS] = m_dInitialTy_urad;

	m_dPIStage_MinusLimitPos[X_AXIS] = X_MINUSLIMIT;
	m_dPIStage_PlusLimitPos[X_AXIS] = X_PLUSLIMIT;
	m_dPIStage_MinusLimitPos[Y_AXIS] = Y_MINUSLIMIT;
	m_dPIStage_PlusLimitPos[Y_AXIS] = Y_PLUSLIMIT;
	m_dPIStage_MinusLimitPos[Z_AXIS] = Z_MINUSLIMIT;
	m_dPIStage_PlusLimitPos[Z_AXIS] = Z_PLUSLIMIT;
	m_dPIStage_MinusLimitPos[TIP_AXIS] = TIP_MINUSLIMIT;
	m_dPIStage_PlusLimitPos[TIP_AXIS] = TIP_PLUSLIMIT;
	m_dPIStage_MinusLimitPos[TILT_AXIS] = TILT_MINUSLIMIT;
	m_dPIStage_PlusLimitPos[TILT_AXIS] = TILT_PLUSLIMIT;

	m_dZUpperLimit = Z_PLUSLIMIT;

	m_pdValueArray[0] = X_INITIAL_POS_UM;
	m_pdValueArray[1] = Y_INITIAL_POS_UM;
	m_pdValueArray[2] = 0;
	m_pdValueArray[3] = 0;
	m_pdValueArray[4] = 0;

	//ddl 학습에 사용하는 명령
	m_scan_rep_mode[0] = 0x41;		// scanning axis
	m_scan_rep_mode[1] = 0x141;		// step axis DDL 사용
	//m_scan_rep_mode[1] = 0x101;	// step axis DDL 사용 안할때

	//ddl 학습후에 scan에 사용하는 명령
	m_scan_mode[0] = 0x81;			// scanning axis
	m_scan_mode[1] = 0x181;			// step axis DDL 사용
	//m_scan_mode[1] = 0x101;		// step axis DDL 사용 안할때
	   
	m_nScanAxis = X_AXIS;
	m_nStepAxis = Y_AXIS;
	m_nStrokeNo = 5;		//5: 2um(fov)_20nm(step)_200nm(accel/dccel) -> Default FOV
	m_nOffsetOfFirstPointInDdlTable = 1;
	m_nNumberOfValues = 0;

	m_iWaveTableIds[0] = 2;		//Scan Axis WaveTable
	m_iWaveTableIds[1] = 1;		//Step Axis WaveTable
	m_iWaveGenerator[0] = 2;	//Scan Axis WaveGenerator
	m_iWaveGenerator[1] = 1;	//Step Axis WaveGenerator

	m_pdValueDdlArrayX = new double[DDL_MEM_NUM];
	m_pdValueDdlArrayY = new double[DDL_MEM_NUM];
	m_dpdDdlDataX = new double*[DDL_LEARNING_NUMBER];
	for (int i = 0; i < DDL_LEARNING_NUMBER; i++)
	{
		m_dpdDdlDataX[i] = new double[DDL_MEM_NUM];
		memset(m_dpdDdlDataX[i], 0.0, sizeof(double) * DDL_MEM_NUM);
	}
	m_dpdDdlDataY = new double*[DDL_LEARNING_NUMBER];
	for (int i = 0; i < DDL_LEARNING_NUMBER; i++)
	{
		m_dpdDdlDataY[i] = new double[DDL_MEM_NUM];
		memset(m_dpdDdlDataY[i], 0.0, sizeof(double) * DDL_MEM_NUM);
	}

	m_pdServoCalculationSpeed = new double[DDL_LEARNING_NUMBER];
	m_piPointOfSpeedUpDown = new int[DDL_LEARNING_NUMBER];
	m_piDdlLearningRepeatNumber = new int[DDL_LEARNING_NUMBER];
	m_piRecordDataNumber = new int[DDL_LEARNING_NUMBER];
	m_piDdlPointNumber = new int[DDL_LEARNING_NUMBER];
	m_piCenterpointOfWave = new int[DDL_LEARNING_NUMBER];
	m_pdAmplitudeOfRampWave = new double[DDL_LEARNING_NUMBER];
	m_pdAmplitudeOfLineWave = new double[DDL_LEARNING_NUMBER];
	m_pstrDdlfileX = new CString[DDL_LEARNING_NUMBER];
	m_pstrDdlfileY = new CString[DDL_LEARNING_NUMBER];
	m_pstrScamData = new CString[DDL_LEARNING_NUMBER];
	m_piDdlScanNum = new int[DDL_LEARNING_NUMBER];

	m_dInitialTx_urad = 0.0;
	m_dInitialTy_urad = 0.0;

	//Servo Update Rate
	for (int i = 0; i < DDL_LEARNING_NUMBER; i++)
	{
		m_pdServoCalculationSpeed[i] = ONEMSEC_SERVO_CALCULATION_POINTNO;
	}
	//5Khz Bi-direction으로 Symetric하게 Ramp 형태로 움직일 떼 Average Tracking Error 맞추기 위한 Servo Update Rate
	m_pdServoCalculationSpeed[39] = 17.3;		//Bi 1.5um 10nm
	m_pdServoCalculationSpeed[40] = 17.3;		//Bi 1.5um 20nm
	m_pdServoCalculationSpeed[20] = 16.9;		//Bi 2um 10nm
	m_pdServoCalculationSpeed[21] = 17.55;		//Bi 2um 20nm
	m_pdServoCalculationSpeed[26] = 17.3;		//Bi 10um 40nm
	m_pdServoCalculationSpeed[27] = 17.3;		//Bi 10um 80nm

	//SpeedUpDown point 수
	for (int i = 0; i < DDL_LEARNING_NUMBER; i++)
	{
		m_piPointOfSpeedUpDown[i] = SPEED_UPDOWN_POINTNO;
	}
	//5Khz Bi-direction으로 Symetric하게 Ramp 형태로 움직일 떼 SpeedUpDown point 수
	m_piPointOfSpeedUpDown[39] = SPEED_UPDOWN_POINTNO;		//Bi 1.5um 10nm
	m_piPointOfSpeedUpDown[40] = SPEED_UPDOWN_POINTNO;		//Bi 1.5um 20nm
	m_piPointOfSpeedUpDown[20] = SPEED_UPDOWN_POINTNO;		//Bi 2um 10nm
	m_piPointOfSpeedUpDown[21] = SPEED_UPDOWN_POINTNO;		//Bi 2um 20nm
	m_piPointOfSpeedUpDown[26] = SPEED_UPDOWN_POINTNO;		//Bi 10um 40nm
	m_piPointOfSpeedUpDown[27] = SPEED_UPDOWN_POINTNO;		//Bi 10um 80nm


	//1Khz Uni-direction에서 Record 기록할 data 개수(기존에는 20000개로 고정했지만,Scan 거리별로 저장할 data 개수 다름)
	m_piRecordDataNumber[0] = 20000;
	m_piRecordDataNumber[1] = 20000;
	m_piRecordDataNumber[2] = 20000;
	m_piRecordDataNumber[3] = 20000;
	m_piRecordDataNumber[4] = 20000;
	m_piRecordDataNumber[5] = 20000;
	m_piRecordDataNumber[6] = 20000;
	m_piRecordDataNumber[7] = 20000;
	m_piRecordDataNumber[8] = 20000;
	m_piRecordDataNumber[9] = 20000;
	m_piRecordDataNumber[10] =20000;
	m_piRecordDataNumber[11] =20000;
	m_piRecordDataNumber[12] =20000;
	m_piRecordDataNumber[13] =20000;
	//1Khz Bi-direction에서 Record 기록할 data 개수
	m_piRecordDataNumber[14] = 20000;
	m_piRecordDataNumber[15] = 20000;
	m_piRecordDataNumber[16] = 20000;
	m_piRecordDataNumber[17] = 20000;
	m_piRecordDataNumber[18] = 20000;
	m_piRecordDataNumber[19] = 20000;
	//5Khz Bi-direction에서 Record 기록할 data 개수
	m_piRecordDataNumber[36] = 1000; //PROFILE_5KHZ_BI_1UM_10NM	
	m_piRecordDataNumber[37] = 500; //PROFILE_5KHZ_BI_1UM_20NM	
	m_piRecordDataNumber[38] = 250; //PROFILE_5KHZ_BI_1UM_40NM	
	m_piRecordDataNumber[39] = 1500; //PROFILE_5KHZ_BI_1_5UM_10NM 
	m_piRecordDataNumber[40] = 750; //PROFILE_5KHZ_BI_1_5UM_20NM 
	m_piRecordDataNumber[41] = 375; //PROFILE_5KHZ_BI_1_5UM_40NM
	m_piRecordDataNumber[20] = 2000; //PROFILE_5KHZ_BI_2UM_10NM
	m_piRecordDataNumber[21] = 1000; //PROFILE_5KHZ_BI_2UM_20NM
	m_piRecordDataNumber[35] = 500; //PROFILE_5KHZ_BI_2UM_40NM
	m_piRecordDataNumber[42] = 3000; //PROFILE_5KHZ_BI_3UM_10NM
	m_piRecordDataNumber[43] = 1500; //PROFILE_5KHZ_BI_3UM_20NM
	m_piRecordDataNumber[26] = 2500; //PROFILE_5KHZ_BI_10UM_40NM
	m_piRecordDataNumber[27] = 1250; //PROFILE_5KHZ_BI_10UM_80NM
	m_piRecordDataNumber[44] = 625; //PROFILE_5KHZ_BI_10UM_100NM
	m_piRecordDataNumber[45] = 312; //PROFILE_5KHZ_BI_10UM_200NM
	//5Khz Uni-direction에서 Record 기록할 data 개수
	m_piRecordDataNumber[29] = 20000;
	m_piRecordDataNumber[30] = 20000;
	m_piRecordDataNumber[31] = 20000;
	m_piRecordDataNumber[32] = 20000;
	m_piRecordDataNumber[33] = 20000;
	m_piRecordDataNumber[34] = 20000;
	m_piRecordDataNumber[22] = 20000;
	m_piRecordDataNumber[23] = 20000;
	m_piRecordDataNumber[28] = 20000;
	m_piRecordDataNumber[24] = 20000;
	m_piRecordDataNumber[25] = 20000;

	//1Khz Uni-direction에서 ddl learning 회수
	m_piDdlLearningRepeatNumber[0] = 60;
	m_piDdlLearningRepeatNumber[1] = 60;
	m_piDdlLearningRepeatNumber[2] = 60;
	m_piDdlLearningRepeatNumber[3] = 60;
	m_piDdlLearningRepeatNumber[4] = 60;
	m_piDdlLearningRepeatNumber[5] = 60;
	m_piDdlLearningRepeatNumber[6] = 60;
	m_piDdlLearningRepeatNumber[7] = 60;
	m_piDdlLearningRepeatNumber[8] = 60;
	m_piDdlLearningRepeatNumber[9] = 60;
	m_piDdlLearningRepeatNumber[10] = 60;
	m_piDdlLearningRepeatNumber[11] = 60;
	m_piDdlLearningRepeatNumber[12] = 60;
	m_piDdlLearningRepeatNumber[13] = 60;
	//1Khz Bi-direction에서 ddl learning 회수
	m_piDdlLearningRepeatNumber[14] = 60;
	m_piDdlLearningRepeatNumber[15] = 60;
	m_piDdlLearningRepeatNumber[16] = 60;
	m_piDdlLearningRepeatNumber[17] = 60;
	m_piDdlLearningRepeatNumber[18] = 60;
	m_piDdlLearningRepeatNumber[19] = 60;
	//5Khz Bi-direction에서 ddl learning 회수
	m_piDdlLearningRepeatNumber[36] = 60; //PROFILE_5KHZ_BI_1UM_10NM	
	m_piDdlLearningRepeatNumber[37] = 60; //PROFILE_5KHZ_BI_1UM_20NM	
	m_piDdlLearningRepeatNumber[38] = 60; //PROFILE_5KHZ_BI_1UM_40NM	
	m_piDdlLearningRepeatNumber[39] = 60; //PROFILE_5KHZ_BI_1_5UM_10NM 
	m_piDdlLearningRepeatNumber[40] = 60; //PROFILE_5KHZ_BI_1_5UM_20NM 
	m_piDdlLearningRepeatNumber[41] = 60; //PROFILE_5KHZ_BI_1_5UM_40NM
	m_piDdlLearningRepeatNumber[20] = 60; //PROFILE_5KHZ_BI_2UM_10NM
	m_piDdlLearningRepeatNumber[21] = 60; //PROFILE_5KHZ_BI_2UM_20NM
	m_piDdlLearningRepeatNumber[35] = 60; //PROFILE_5KHZ_BI_2UM_40NM
	m_piDdlLearningRepeatNumber[42] = 60; //PROFILE_5KHZ_BI_3UM_10NM
	m_piDdlLearningRepeatNumber[43] = 60; //PROFILE_5KHZ_BI_3UM_20NM
	m_piDdlLearningRepeatNumber[26] = 60; //PROFILE_5KHZ_BI_10UM_40NM
	m_piDdlLearningRepeatNumber[27] = 60; //PROFILE_5KHZ_BI_10UM_80NM
	m_piDdlLearningRepeatNumber[44] = 60; //PROFILE_5KHZ_BI_10UM_100NM
	m_piDdlLearningRepeatNumber[45] = 30; //PROFILE_5KHZ_BI_10UM_200NM
	//5Khz Uni-direction에서 ddl learning 회수
	m_piDdlLearningRepeatNumber[29] = 60;
	m_piDdlLearningRepeatNumber[30] = 60;
	m_piDdlLearningRepeatNumber[31] = 60;
	m_piDdlLearningRepeatNumber[32] = 60;
	m_piDdlLearningRepeatNumber[33] = 60;
	m_piDdlLearningRepeatNumber[34] = 60;
	m_piDdlLearningRepeatNumber[22] = 60;
	m_piDdlLearningRepeatNumber[23] = 60;
	m_piDdlLearningRepeatNumber[28] = 60;
	m_piDdlLearningRepeatNumber[24] = 60;
	m_piDdlLearningRepeatNumber[25] = 60;


	//1Khz Uni-direction으로 Asymetric하게 Ramp 형태로 움직인 전체 point 수
	m_piDdlPointNumber[0] = (int)(PROFILE_UNI_1UM_20NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[1] = (int)(PROFILE_UNI_1UM_40NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[2] = (int)(PROFILE_UNI_1UM_80NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[3] = (int)(PROFILE_UNI_1UM_160NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[4] = (int)(PROFILE_UNI_2UM_10NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[5] = (int)(PROFILE_UNI_2UM_20NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[6] = (int)(PROFILE_UNI_3UM_10NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[7] = (int)(PROFILE_UNI_3UM_20NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[8] = (int)(PROFILE_UNI_7UM_40NM_POINTNO	 + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[9] = (int)(PROFILE_UNI_10UM_10NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[10] = (int)(PROFILE_UNI_10UM_20NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[11] = (int)(PROFILE_UNI_10UM_40NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[12] = (int)(PROFILE_UNI_10UM_80NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[13] = (int)(PROFILE_UNI_10UM_160NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	//1Khz Bi-direction으로 Symetric하게 Ramp 형태로 움직인 전체 point 수
	m_piDdlPointNumber[14] = (int)PROFILE_BI_1UM_10NM_POINTNO;	
	m_piDdlPointNumber[15] = (int)PROFILE_BI_1UM_20NM_POINTNO;	
	m_piDdlPointNumber[16] = (int)PROFILE_BI_2UM_10NM_POINTNO;	
	m_piDdlPointNumber[17] = (int)PROFILE_BI_2UM_20NM_POINTNO;	
	m_piDdlPointNumber[18] = (int)PROFILE_BI_3UM_10NM_POINTNO;	
	m_piDdlPointNumber[19] = (int)PROFILE_BI_3UM_20NM_POINTNO;	
	//5Khz Bi-direction으로 Symetric하게 Ramp 형태로 움직인 전체 point 수
	m_piDdlPointNumber[36] = (int)PROFILE_5KHZ_BI_1UM_10NM_POINTNO;		//933 
	m_piDdlPointNumber[37] = (int)PROFILE_5KHZ_BI_1UM_20NM_POINTNO;		//466 
	m_piDdlPointNumber[38] = (int)PROFILE_5KHZ_BI_1UM_40NM_POINTNO;		//233 
	m_piDdlPointNumber[39] = 1290;	//int(PROFILE_5KHZ_BI_1_5UM_10NM_MSECTIME * m_pdServoCalculationSpeed[39]);	//76ms*16.667points=1266 points	//1314(9.815) 1300(9.92) 1290(9.999)
	m_piDdlPointNumber[40] = 672;	//int(PROFILE_5KHZ_BI_1_5UM_20NM_MSECTIME * m_pdServoCalculationSpeed[40]);	//38ms*16.667points=633 points	//657(20.59)  670(20.09) 672(20.05) 674(19.94)
	m_piDdlPointNumber[41] = (int)PROFILE_5KHZ_BI_1_5UM_40NM_POINTNO;	//316 
	m_piDdlPointNumber[20] = 1610;	//int(PROFILE_5KHZ_BI_2UM_10NM_MSECTIME * m_pdServoCalculationSpeed[20]);	//96ms*16.667points=1600 points //1630(9.89) 1622(9.92) 1610(10.07) 
	m_piDdlPointNumber[21] = 838;	//int(PROFILE_5KHZ_BI_2UM_20NM_MSECTIME * m_pdServoCalculationSpeed[21]);	//48ms*16.667points=800 points	//842(19.85) 838(19.98,20.07) 836(20.14)
	m_piDdlPointNumber[35] = (int)PROFILE_5KHZ_BI_2UM_40NM_POINTNO;		//400
	m_piDdlPointNumber[42] = 2000;// (int)PROFILE_5KHZ_BI_3UM_10NM_POINTNO;		//2266->2000
	m_piDdlPointNumber[43] = 1000;// (int)PROFILE_5KHZ_BI_3UM_20NM_POINTNO;		//1133->1000
	m_piDdlPointNumber[26] = 1802;	//int(PROFILE_5KHZ_BI_10UM_40NM_MSECTIME * m_pdServoCalculationSpeed[26]);	//106ms*16.667points=1766 points //1833(39.12) 1800(40.065) 1802(40.015) 1806(39.90)
	m_piDdlPointNumber[27] = 915;	//int(PROFILE_5KHZ_BI_10UM_80NM_MSECTIME * m_pdServoCalculationSpeed[27]);	//53ms*16.667points=883 points	 //916(79.92,79.95,79.7) 915(79.999) 914(80.12,80.13)
	m_piDdlPointNumber[44] = (int)PROFILE_5KHZ_BI_10UM_100NM_POINTNO;		//706
	m_piDdlPointNumber[45] = (int)PROFILE_5KHZ_BI_10UM_200NM_POINTNO;		//353
	//5Khz Uni-direction으로 Asymetric하게 Ramp 형태로 움직인 전체 point 수
	m_piDdlPointNumber[29] = (int)(PROFILE_5KHZ_UNI_1UM_10NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[30] = (int)(PROFILE_5KHZ_UNI_1UM_20NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[31] = (int)(PROFILE_5KHZ_UNI_1UM_40NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[32] = (int)(PROFILE_5KHZ_UNI_1_5UM_10NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[33] = (int)(PROFILE_5KHZ_UNI_1_5UM_20NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[34] = (int)(PROFILE_5KHZ_UNI_1_5UM_40NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[22] = (int)(PROFILE_5KHZ_UNI_2UM_10NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[23] = (int)(PROFILE_5KHZ_UNI_2UM_20NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[28] = (int)(PROFILE_5KHZ_UNI_10UM_20NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[24] = (int)(PROFILE_5KHZ_UNI_10UM_40NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);
	m_piDdlPointNumber[25] = (int)(PROFILE_5KHZ_UNI_10UM_80NM_POINTNO + UNIDIRECTION_RETURN_POINTNO);


	//1Khz Uni-direction에서 Asymetric Ramp 중 Scan 구간 point 수
	m_piCenterpointOfWave[0] = (int)PROFILE_UNI_1UM_20NM_POINTNO;
	m_piCenterpointOfWave[1] = (int)PROFILE_UNI_1UM_40NM_POINTNO;
	m_piCenterpointOfWave[2] = (int)PROFILE_UNI_1UM_80NM_POINTNO;
	m_piCenterpointOfWave[3] = (int)PROFILE_UNI_1UM_160NM_POINTNO;
	m_piCenterpointOfWave[4] = (int)PROFILE_UNI_2UM_10NM_POINTNO;
	m_piCenterpointOfWave[5] = (int)PROFILE_UNI_2UM_20NM_POINTNO;
	m_piCenterpointOfWave[6] = (int)PROFILE_UNI_3UM_10NM_POINTNO;
	m_piCenterpointOfWave[7] = (int)PROFILE_UNI_3UM_20NM_POINTNO;
	m_piCenterpointOfWave[8] = (int)PROFILE_UNI_7UM_40NM_POINTNO;
	m_piCenterpointOfWave[9] = (int)PROFILE_UNI_10UM_10NM_POINTNO;
	m_piCenterpointOfWave[10] = (int)PROFILE_UNI_10UM_20NM_POINTNO;
	m_piCenterpointOfWave[11] = (int)PROFILE_UNI_10UM_40NM_POINTNO;
	m_piCenterpointOfWave[12] = (int)PROFILE_UNI_10UM_80NM_POINTNO;
	m_piCenterpointOfWave[13] = (int)PROFILE_UNI_10UM_160NM_POINTNO;
	//1Khz Bi-direction에서 Symetric Ramp 중 Center까지 point 수
	m_piCenterpointOfWave[14] = (int)(PROFILE_BI_1UM_10NM_POINTNO / 2.);	//2666
	m_piCenterpointOfWave[15] = (int)(PROFILE_BI_1UM_20NM_POINTNO / 2.);	//1333
	m_piCenterpointOfWave[16] = (int)(PROFILE_BI_2UM_10NM_POINTNO / 2.);	//4333
	m_piCenterpointOfWave[17] = (int)(PROFILE_BI_2UM_20NM_POINTNO / 2.);	//2166
	m_piCenterpointOfWave[18] = (int)(PROFILE_BI_3UM_10NM_POINTNO / 2.);	//6000
	m_piCenterpointOfWave[19] = (int)(PROFILE_BI_3UM_20NM_POINTNO / 2.);	//3000
	//5Khz Bi-direction에서 Symetric Ramp 중 Center까지 point 수
	m_piCenterpointOfWave[36] = (int)(PROFILE_5KHZ_BI_1UM_10NM_POINTNO / 2.);	//466
	m_piCenterpointOfWave[37] = (int)(PROFILE_5KHZ_BI_1UM_20NM_POINTNO / 2.);	//233
	m_piCenterpointOfWave[38] = (int)(PROFILE_5KHZ_BI_1UM_40NM_POINTNO / 2.);	//116
	m_piCenterpointOfWave[39] = (int)(m_piDdlPointNumber[39] / 2.);	//633
	m_piCenterpointOfWave[40] = (int)(m_piDdlPointNumber[40] / 2.);	//316
	m_piCenterpointOfWave[41] = (int)(PROFILE_5KHZ_BI_1_5UM_40NM_POINTNO / 2.);	//158
	m_piCenterpointOfWave[20] = (int)(m_piDdlPointNumber[20] / 2.);	//800
	m_piCenterpointOfWave[21] = (int)(m_piDdlPointNumber[21] / 2.);	//400
	m_piCenterpointOfWave[35] = (int)(PROFILE_5KHZ_BI_2UM_40NM_POINTNO / 2.);	//200
	m_piCenterpointOfWave[42] = (int)(m_piDdlPointNumber[42] / 2.);	//(int)(PROFILE_5KHZ_BI_3UM_10NM_POINTNO / 2.);	//1133 -> 1000
	m_piCenterpointOfWave[43] = (int)(m_piDdlPointNumber[43] / 2.);	//(int)(PROFILE_5KHZ_BI_3UM_20NM_POINTNO / 2.);	//566 -> 500
	m_piCenterpointOfWave[26] = (int)(m_piDdlPointNumber[26] / 2.);	//883
	m_piCenterpointOfWave[27] = (int)(m_piDdlPointNumber[27] / 2.);	//441
	m_piCenterpointOfWave[44] = (int)(m_piDdlPointNumber[44] / 2.);	//353
	m_piCenterpointOfWave[45] = (int)(m_piDdlPointNumber[45] / 2.);	//176
	//5Khz Uni-direction에서 Asymetric Ramp 중 Scan 구간 point 수
	m_piCenterpointOfWave[29] = (int)PROFILE_5KHZ_UNI_1UM_10NM_POINTNO;
	m_piCenterpointOfWave[30] = (int)PROFILE_5KHZ_UNI_1UM_20NM_POINTNO;
	m_piCenterpointOfWave[31] = (int)PROFILE_5KHZ_UNI_1UM_40NM_POINTNO;
	m_piCenterpointOfWave[32] = (int)PROFILE_5KHZ_UNI_1_5UM_10NM_POINTNO;
	m_piCenterpointOfWave[33] = (int)PROFILE_5KHZ_UNI_1_5UM_20NM_POINTNO;
	m_piCenterpointOfWave[34] = (int)PROFILE_5KHZ_UNI_1_5UM_40NM_POINTNO;
	m_piCenterpointOfWave[22] = (int)PROFILE_5KHZ_UNI_2UM_10NM_POINTNO;
	m_piCenterpointOfWave[23] = (int)PROFILE_5KHZ_UNI_2UM_20NM_POINTNO;
	m_piCenterpointOfWave[28] = (int)PROFILE_5KHZ_UNI_10UM_20NM_POINTNO;
	m_piCenterpointOfWave[24] = (int)PROFILE_5KHZ_UNI_10UM_40NM_POINTNO;
	m_piCenterpointOfWave[25] = (int)PROFILE_5KHZ_UNI_10UM_80NM_POINTNO;


	//1Khz Uni-direction에서 Scan 거리
	m_pdAmplitudeOfRampWave[0] = ((double)PROFILE_UNI_1UM_20NM_MSECTIME * STEP_SIZE_20NM) / 1000.;		//(80ms * 20nm)/1000=1.6, scan 거리는 1.6um
	m_pdAmplitudeOfRampWave[1] = ((double)PROFILE_UNI_1UM_40NM_MSECTIME * STEP_SIZE_40NM) / 1000.;		//(40ms * 40nm)/1000=1.6, scan 거리는 1.6um
	m_pdAmplitudeOfRampWave[2] = ((double)PROFILE_UNI_1UM_80NM_MSECTIME * STEP_SIZE_80NM) / 1000.;		//(20ms * 80nm)/1000=1.6, scan 거리는 1.6um
	m_pdAmplitudeOfRampWave[3] = ((double)PROFILE_UNI_1UM_160NM_MSECTIME * STEP_SIZE_160NM) / 1000.;	//(10ms * 160nm)/1000=1.6, scan 거리는 1.6um
	m_pdAmplitudeOfRampWave[4] = ((double)PROFILE_UNI_2UM_10NM_MSECTIME * STEP_SIZE_10NM) / 1000.;		//(260ms * 10nm)/1000=2.6, scan 거리는 2.6um
	m_pdAmplitudeOfRampWave[5] = ((double)PROFILE_UNI_2UM_20NM_MSECTIME * STEP_SIZE_20NM) / 1000.;		//(130ms * 20nm)/1000=2.6, scan 거리는 2.6um
	m_pdAmplitudeOfRampWave[6] = ((double)PROFILE_UNI_3UM_10NM_MSECTIME * STEP_SIZE_10NM) / 1000.;		//(360ms * 10nm)/1000=3.6, scan 거리는 3.6um
	m_pdAmplitudeOfRampWave[7] = ((double)PROFILE_UNI_3UM_20NM_MSECTIME * STEP_SIZE_20NM) / 1000.;		//(180ms * 20nm)/1000=3.6, scan 거리는 3.6um
	m_pdAmplitudeOfRampWave[8] = ((double)PROFILE_UNI_7UM_40NM_MSECTIME * STEP_SIZE_40NM) / 1000.;		//(190ms * 40nm)/1000=7.6, scan 거리는 7.6um
	m_pdAmplitudeOfRampWave[9] = ((double)PROFILE_UNI_10UM_10NM_MSECTIME * STEP_SIZE_10NM) / 1000.;		//(1060ms * 10nm)/1000=10.6, scan 거리는 10.6um
	m_pdAmplitudeOfRampWave[10] = ((double)PROFILE_UNI_10UM_20NM_MSECTIME * STEP_SIZE_20NM) / 1000.;		//(530ms * 20nm)/1000=10.6, scan 거리는 10.6um
	m_pdAmplitudeOfRampWave[11] = ((double)PROFILE_UNI_10UM_40NM_MSECTIME * STEP_SIZE_40NM) / 1000.;		//(265ms * 40nm)/1000=10.6, scan 거리는 10.6um
	m_pdAmplitudeOfRampWave[12] = ((double)PROFILE_UNI_10UM_80NM_MSECTIME * STEP_SIZE_80NM) / 1000.;	//(132.5ms * 80nm)/1000=10.6, scan 거리는 10.6um
	m_pdAmplitudeOfRampWave[13] = ((double)PROFILE_UNI_10UM_160NM_MSECTIME * STEP_SIZE_160NM) / 1000.;	//(66.25ms * 1600nm)/1000=10.6, scan 거리는 10.6um
	//1Khz Bi-direction에서 Scan 거리
	m_pdAmplitudeOfRampWave[14] = ((double)PROFILE_BI_1UM_10NM_MSECTIME * STEP_SIZE_10NM) / 2000.;		//(320ms * 10nm)/2000=1.6, scan 거리는 1.6um
	m_pdAmplitudeOfRampWave[15] = ((double)PROFILE_BI_1UM_20NM_MSECTIME * STEP_SIZE_20NM) / 2000.;		//(160ms * 20nm)/2000=1.6, scan 거리는 1.6um
	m_pdAmplitudeOfRampWave[16] = ((double)PROFILE_BI_2UM_10NM_MSECTIME * STEP_SIZE_10NM) / 2000.;		//(520ms * 10nm)/2000=2.6, scan 거리는 2.6um
	m_pdAmplitudeOfRampWave[17] = ((double)PROFILE_BI_2UM_20NM_MSECTIME * STEP_SIZE_20NM) / 2000.;		//(260ms * 20nm)/2000=2.6, scan 거리는 2.6um
	m_pdAmplitudeOfRampWave[18] = ((double)PROFILE_BI_3UM_10NM_MSECTIME * STEP_SIZE_10NM) / 2000.;		//(720ms * 10nm)/2000=3.6, scan 거리는 3.6um
	m_pdAmplitudeOfRampWave[19] = ((double)PROFILE_BI_3UM_20NM_MSECTIME * STEP_SIZE_20NM) / 2000.;		//(360ms * 20nm)/2000=3.6, scan 거리는 3.6um
	//5Khz Bi-direction에서 Scan 거리
	m_pdAmplitudeOfRampWave[36] = ((double)PROFILE_5KHZ_BI_1UM_10NM_MSECTIME * STEP_SIZE_10NM * 5) / 2000.;		//(56ms * 10nm * 5)/2000=1.4, scan 거리는 1.4um
	m_pdAmplitudeOfRampWave[37] = ((double)PROFILE_5KHZ_BI_1UM_20NM_MSECTIME * STEP_SIZE_20NM * 5) / 2000.;		//(28ms * 20nm * 5)/2000=1.4, scan 거리는 1.4um
	m_pdAmplitudeOfRampWave[38] = ((double)PROFILE_5KHZ_BI_1UM_40NM_MSECTIME * STEP_SIZE_40NM * 5) / 2000.;		//(14ms * 40nm * 5)/2000=1.4, scan 거리는 1.4um
	m_pdAmplitudeOfRampWave[39] = ((double)PROFILE_5KHZ_BI_1_5UM_10NM_MSECTIME * STEP_SIZE_10NM * 5) / 2000.;		//(76ms * 10nm * 5)/2000=1.9, scan 거리는 1.9um
	m_pdAmplitudeOfRampWave[40] = ((double)PROFILE_5KHZ_BI_1_5UM_20NM_MSECTIME * STEP_SIZE_20NM * 5) / 2000.;		//(38ms * 20nm * 5)/2000=1.9, scan 거리는 1.9um
	m_pdAmplitudeOfRampWave[41] = ((double)PROFILE_5KHZ_BI_1_5UM_40NM_MSECTIME * STEP_SIZE_40NM * 5) / 2000.;		//(19ms * 40nm * 5)/2000=1.9, scan 거리는 1.9um
	m_pdAmplitudeOfRampWave[20] = ((double)PROFILE_5KHZ_BI_2UM_10NM_MSECTIME * STEP_SIZE_10NM * 5) / 2000.;		//(96ms * 10nm * 5)/2000=2.4, scan 거리는 2.4um
	m_pdAmplitudeOfRampWave[21] = ((double)PROFILE_5KHZ_BI_2UM_20NM_MSECTIME * STEP_SIZE_20NM * 5) / 2000.;		//(48ms * 20nm * 5)/2000=2.4, scan 거리는 2.4um
	m_pdAmplitudeOfRampWave[35] = ((double)PROFILE_5KHZ_BI_2UM_40NM_MSECTIME * STEP_SIZE_40NM * 5) / 2000.;		//(24ms * 40nm * 5)/2000=2.4, scan 거리는 2.4um
	m_pdAmplitudeOfRampWave[42] = ((double)PROFILE_5KHZ_BI_3UM_10NM_MSECTIME * STEP_SIZE_10NM * 5) / 2000.;		//(136ms * 10nm * 5)/2000=3.4, scan 거리는 3.4um -> (120ms * 10nm * 5)/2000=3, scan 거리는 3um
	m_pdAmplitudeOfRampWave[43] = ((double)PROFILE_5KHZ_BI_3UM_20NM_MSECTIME * STEP_SIZE_20NM * 5) / 2000.;		//(68ms * 20nm * 5)/2000=3.4, scan 거리는 3.4um -> (60ms * 20nm * 5)/2000=3, scan 거리는 3um
	m_pdAmplitudeOfRampWave[26] = ((double)PROFILE_5KHZ_BI_10UM_40NM_MSECTIME * STEP_SIZE_40NM * 5) / 2000.;		//(106ms * 40nm * 5)/2000=10.6, scan 거리는 10.6um
	m_pdAmplitudeOfRampWave[27] = ((double)PROFILE_5KHZ_BI_10UM_80NM_MSECTIME * STEP_SIZE_80NM * 5) / 2000.;		//(53ms * 80nm * 5)/2000=10.6, scan 거리는 10.6um
	m_pdAmplitudeOfRampWave[44] = ((double)PROFILE_5KHZ_BI_10UM_100NM_MSECTIME * STEP_SIZE_100NM * 5) / 2000.;		//(42.4ms * 100nm * 5)/2000=10.6, scan 거리는 10.6um
	m_pdAmplitudeOfRampWave[45] = ((double)PROFILE_5KHZ_BI_10UM_200NM_MSECTIME * STEP_SIZE_200NM * 5) / 2000.;		//(21.2ms * 200nm * 5)/2000=10.6, scan 거리는 10.6um
	//5Khz Uni-direction에서 Scan 거리
	m_pdAmplitudeOfRampWave[29] = ((double)PROFILE_5KHZ_UNI_1UM_10NM_MSECTIME * STEP_SIZE_10NM * 5) / 1000.;		//(28ms * 10nm * 5)/1000=1.4, scan 거리는 1.4um
	m_pdAmplitudeOfRampWave[30] = ((double)PROFILE_5KHZ_UNI_1UM_20NM_MSECTIME * STEP_SIZE_20NM * 5) / 1000.;		//(14ms * 20nm * 5)/1000=1.4, scan 거리는 1.4um
	m_pdAmplitudeOfRampWave[31] = ((double)PROFILE_5KHZ_UNI_1UM_40NM_MSECTIME * STEP_SIZE_40NM * 5) / 1000.;		//(7ms * 40nm * 5)/1000=1.4, scan 거리는 1.4um
	m_pdAmplitudeOfRampWave[32] = ((double)PROFILE_5KHZ_UNI_1_5UM_10NM_MSECTIME * STEP_SIZE_10NM * 5) / 1000.;		//(38ms * 10nm * 5)/1000=1.9, scan 거리는 1.9um
	m_pdAmplitudeOfRampWave[33] = ((double)PROFILE_5KHZ_UNI_1_5UM_20NM_MSECTIME * STEP_SIZE_20NM * 5) / 1000.;		//(19ms * 20nm * 5)/1000=1.9, scan 거리는 1.9um
	m_pdAmplitudeOfRampWave[34] = ((double)PROFILE_5KHZ_UNI_1_5UM_40NM_MSECTIME * STEP_SIZE_40NM * 5) / 1000.;		//(9.5ms * 40nm * 5)/1000=1.9, scan 거리는 1.9um
	m_pdAmplitudeOfRampWave[22] = ((double)PROFILE_5KHZ_UNI_2UM_10NM_MSECTIME * STEP_SIZE_10NM * 5) / 1000.;		//(52ms * 10nm * 5)/1000=2.6, scan 거리는 2.6um
	m_pdAmplitudeOfRampWave[23] = ((double)PROFILE_5KHZ_UNI_2UM_20NM_MSECTIME * STEP_SIZE_20NM * 5) / 1000.;		//(26ms * 20nm * 5)/1000=2.6, scan 거리는 2.6um
	m_pdAmplitudeOfRampWave[28] = ((double)PROFILE_5KHZ_UNI_10UM_20NM_MSECTIME * STEP_SIZE_20NM * 5) / 1000.;		//(106ms * 20nm * 5)/1000=10.6, scan 거리는 10.6um
	m_pdAmplitudeOfRampWave[24] = ((double)PROFILE_5KHZ_UNI_10UM_40NM_MSECTIME * STEP_SIZE_40NM * 5) / 1000.;		//(53ms * 40nm * 5)/1000=10.6, scan 거리는 10.6um
	m_pdAmplitudeOfRampWave[25] = ((double)PROFILE_5KHZ_UNI_10UM_80NM_MSECTIME * STEP_SIZE_80NM * 5) / 1000.;		//(27.5ms * 80nm * 5)/1000=11, scan 거리는 11um


	//1Khz Uni-direction Step Size
	m_pdAmplitudeOfLineWave[0] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[1] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[2] = STEP_SIZE_80NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[3] = STEP_SIZE_160NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[4] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[5] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[6] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[7] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[8] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[9] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[10] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[11] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[12] = STEP_SIZE_80NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[13] = STEP_SIZE_160NM / 1000.;	//um 단위로 변환
	//1Khz Bi-direction Step Size
	m_pdAmplitudeOfLineWave[14] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환	//0.01
	m_pdAmplitudeOfLineWave[15] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환	//0.02
	m_pdAmplitudeOfLineWave[16] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환	//0.01
	m_pdAmplitudeOfLineWave[17] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환	//0.02
	m_pdAmplitudeOfLineWave[18] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환	//0.01
	m_pdAmplitudeOfLineWave[19] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환	//0.02
	//5Khz Bi-direction Step Size
	m_pdAmplitudeOfLineWave[36] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환	//0.01
	m_pdAmplitudeOfLineWave[37] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환	//0.02
	m_pdAmplitudeOfLineWave[38] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환	//0.04
	m_pdAmplitudeOfLineWave[39] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환	//0.01
	m_pdAmplitudeOfLineWave[40] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환	//0.02
	m_pdAmplitudeOfLineWave[41] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환	//0.04
	m_pdAmplitudeOfLineWave[20] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환	//0.01
	m_pdAmplitudeOfLineWave[21] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환	//0.02
	m_pdAmplitudeOfLineWave[35] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환	//0.04
	m_pdAmplitudeOfLineWave[42] = (STEP_SIZE_10NM+1.5) / 1000.;	//um 단위로 변환	//0.0115
	m_pdAmplitudeOfLineWave[43] = (STEP_SIZE_20NM+3) / 1000.;	//um 단위로 변환	//0.023
	m_pdAmplitudeOfLineWave[26] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환	//0.04
	m_pdAmplitudeOfLineWave[27] = STEP_SIZE_80NM / 1000.;	//um 단위로 변환	//0.08
	m_pdAmplitudeOfLineWave[44] = STEP_SIZE_100NM / 1000.;	//um 단위로 변환	//0.1
	m_pdAmplitudeOfLineWave[45] = STEP_SIZE_200NM / 1000.;	//um 단위로 변환	//0.2
	//5Khz Uni-direction Step Size
	m_pdAmplitudeOfLineWave[29] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[30] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[31] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[32] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[33] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[34] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[22] = STEP_SIZE_10NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[23] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[28] = STEP_SIZE_20NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[24] = STEP_SIZE_40NM / 1000.;	//um 단위로 변환
	m_pdAmplitudeOfLineWave[25] = STEP_SIZE_80NM / 1000.;	//um 단위로 변환


	//1Khz Uni-direction ddl file name
	m_pstrDdlfileX[0].Format(_T("ddldata_uni_1000_20_600_x.txt"));
	m_pstrDdlfileY[0].Format(_T("ddldata_uni_1000_20_600_y.txt"));
	m_pstrDdlfileX[1].Format(_T("ddldata_uni_1000_40_600_x.txt"));
	m_pstrDdlfileY[1].Format(_T("ddldata_uni_1000_40_600_y.txt"));
	m_pstrDdlfileX[2].Format(_T("ddldata_uni_1000_80_600_x.txt"));
	m_pstrDdlfileY[2].Format(_T("ddldata_uni_1000_80_600_y.txt"));
	m_pstrDdlfileX[3].Format(_T("ddldata_uni_1000_160_600_x.txt"));
	m_pstrDdlfileY[3].Format(_T("ddldata_uni_1000_160_600_y.txt"));
	m_pstrDdlfileX[4].Format(_T("ddldata_uni_2000_10_600_x.txt"));
	m_pstrDdlfileY[4].Format(_T("ddldata_uni_2000_10_600_y.txt"));
	m_pstrDdlfileX[5].Format(_T("ddldata_uni_2000_20_600_x.txt"));
	m_pstrDdlfileY[5].Format(_T("ddldata_uni_2000_20_600_y.txt"));
	m_pstrDdlfileX[6].Format(_T("ddldata_uni_3000_10_600_x.txt"));
	m_pstrDdlfileY[6].Format(_T("ddldata_uni_3000_10_600_y.txt"));
	m_pstrDdlfileX[7].Format(_T("ddldata_uni_3000_20_600_x.txt"));
	m_pstrDdlfileY[7].Format(_T("ddldata_uni_3000_20_600_y.txt"));
	m_pstrDdlfileX[8].Format(_T("ddldata_uni_7000_40_600_x.txt"));
	m_pstrDdlfileY[8].Format(_T("ddldata_uni_7000_40_600_y.txt"));
	m_pstrDdlfileX[9].Format(_T("ddldata_uni_10000_10_600_x.txt"));
	m_pstrDdlfileY[9].Format(_T("ddldata_uni_10000_10_600_y.txt"));
	m_pstrDdlfileX[10].Format(_T("ddldata_uni_10000_20_600_x.txt"));
	m_pstrDdlfileY[10].Format(_T("ddldata_uni_10000_20_600_y.txt"));
	m_pstrDdlfileX[11].Format(_T("ddldata_uni_10000_40_600_x.txt"));
	m_pstrDdlfileY[11].Format(_T("ddldata_uni_10000_40_600_y.txt"));
	m_pstrDdlfileX[12].Format(_T("ddldata_uni_10000_80_600_x.txt"));
	m_pstrDdlfileY[12].Format(_T("ddldata_uni_10000_80_600_y.txt"));
	m_pstrDdlfileX[13].Format(_T("ddldata_uni_10000_160_600_x.txt"));
	m_pstrDdlfileY[13].Format(_T("ddldata_uni_10000_160_600_y.txt"));
	//1Khz Bi-direction ddl file name
	m_pstrDdlfileX[14].Format(_T("ddldata_bi_1000_10_600_x.txt"));
	m_pstrDdlfileY[14].Format(_T("ddldata_bi_1000_10_600_y.txt"));
	m_pstrDdlfileX[15].Format(_T("ddldata_bi_1000_20_600_x.txt"));
	m_pstrDdlfileY[15].Format(_T("ddldata_bi_1000_20_600_y.txt"));
	m_pstrDdlfileX[16].Format(_T("ddldata_bi_2000_10_600_x.txt"));
	m_pstrDdlfileY[16].Format(_T("ddldata_bi_2000_10_600_y.txt"));
	m_pstrDdlfileX[17].Format(_T("ddldata_bi_2000_20_600_x.txt"));
	m_pstrDdlfileY[17].Format(_T("ddldata_bi_2000_20_600_y.txt"));
	m_pstrDdlfileX[18].Format(_T("ddldata_bi_3000_10_600_x.txt"));
	m_pstrDdlfileY[18].Format(_T("ddldata_bi_3000_10_600_y.txt"));
	m_pstrDdlfileX[19].Format(_T("ddldata_bi_3000_20_600_x.txt"));
	m_pstrDdlfileY[19].Format(_T("ddldata_bi_3000_20_600_y.txt"));
	//5Khz Bi-direction ddl file name
	m_pstrDdlfileX[36].Format(_T("ddldata_5khz_bi_1000_10_400_x.txt"));
	m_pstrDdlfileY[36].Format(_T("ddldata_5khz_bi_1000_10_400_y.txt"));
	m_pstrDdlfileX[37].Format(_T("ddldata_5khz_bi_1000_20_400_x.txt"));
	m_pstrDdlfileY[37].Format(_T("ddldata_5khz_bi_1000_20_400_y.txt"));
	m_pstrDdlfileX[38].Format(_T("ddldata_5khz_bi_1000_40_400_x.txt"));
	m_pstrDdlfileY[38].Format(_T("ddldata_5khz_bi_1000_40_400_y.txt"));
	m_pstrDdlfileX[39].Format(_T("ddldata_5khz_bi_1500_10_400_x.txt"));
	m_pstrDdlfileY[39].Format(_T("ddldata_5khz_bi_1500_10_400_y.txt"));
	m_pstrDdlfileX[40].Format(_T("ddldata_5khz_bi_1500_20_400_x.txt"));
	m_pstrDdlfileY[40].Format(_T("ddldata_5khz_bi_1500_20_400_y.txt"));
	m_pstrDdlfileX[41].Format(_T("ddldata_5khz_bi_1500_40_400_x.txt"));
	m_pstrDdlfileY[41].Format(_T("ddldata_5khz_bi_1500_40_400_y.txt"));
	m_pstrDdlfileX[20].Format(_T("ddldata_5khz_bi_2000_10_400_x.txt"));
	m_pstrDdlfileY[20].Format(_T("ddldata_5khz_bi_2000_10_400_y.txt"));
	m_pstrDdlfileX[21].Format(_T("ddldata_5khz_bi_2000_20_400_x.txt"));
	m_pstrDdlfileY[21].Format(_T("ddldata_5khz_bi_2000_20_400_y.txt"));
	m_pstrDdlfileX[35].Format(_T("ddldata_5khz_bi_2000_40_400_x.txt"));
	m_pstrDdlfileY[35].Format(_T("ddldata_5khz_bi_2000_40_400_y.txt"));
	//m_pstrDdlfileX[42].Format(_T("ddldata_5khz_bi_3000_10_400_x.txt"));
	//m_pstrDdlfileY[42].Format(_T("ddldata_5khz_bi_3000_10_400_y.txt"));
	m_pstrDdlfileX[42].Format(_T("ddldata_5khz_bi_3000_10_x.txt"));
	m_pstrDdlfileY[42].Format(_T("ddldata_5khz_bi_3000_10_y.txt"));
	//m_pstrDdlfileX[43].Format(_T("ddldata_5khz_bi_3000_20_400_x.txt"));
	//m_pstrDdlfileY[43].Format(_T("ddldata_5khz_bi_3000_20_400_y.txt"));
	m_pstrDdlfileX[43].Format(_T("ddldata_5khz_bi_3000_20_x.txt"));
	m_pstrDdlfileY[43].Format(_T("ddldata_5khz_bi_3000_20_y.txt"));
	m_pstrDdlfileX[26].Format(_T("ddldata_5khz_bi_10000_40_600_x.txt"));
	m_pstrDdlfileY[26].Format(_T("ddldata_5khz_bi_10000_40_600_y.txt"));
	m_pstrDdlfileX[27].Format(_T("ddldata_5khz_bi_10000_80_600_x.txt"));
	m_pstrDdlfileY[27].Format(_T("ddldata_5khz_bi_10000_80_600_y.txt"));
	m_pstrDdlfileX[44].Format(_T("ddldata_5khz_bi_10000_100_600_x.txt"));
	m_pstrDdlfileY[44].Format(_T("ddldata_5khz_bi_10000_100_600_y.txt"));
	m_pstrDdlfileX[45].Format(_T("ddldata_5khz_bi_10000_200_600_x.txt"));
	m_pstrDdlfileY[45].Format(_T("ddldata_5khz_bi_10000_200_600_y.txt"));
	//5Khz Uni-direction ddl file name
	m_pstrDdlfileX[29].Format(_T("ddldata_5khz_uni_1000_10_400_x.txt"));
	m_pstrDdlfileY[29].Format(_T("ddldata_5khz_uni_1000_10_400_y.txt"));
	m_pstrDdlfileX[30].Format(_T("ddldata_5khz_uni_1000_20_400_x.txt"));
	m_pstrDdlfileY[30].Format(_T("ddldata_5khz_uni_1000_20_400_y.txt"));
	m_pstrDdlfileX[31].Format(_T("ddldata_5khz_uni_1000_40_400_x.txt"));
	m_pstrDdlfileY[31].Format(_T("ddldata_5khz_uni_1000_40_400_y.txt"));
	m_pstrDdlfileX[32].Format(_T("ddldata_5khz_uni_1500_10_400_x.txt"));
	m_pstrDdlfileY[32].Format(_T("ddldata_5khz_uni_1500_10_400_y.txt"));
	m_pstrDdlfileX[33].Format(_T("ddldata_5khz_uni_1500_20_400_x.txt"));
	m_pstrDdlfileY[33].Format(_T("ddldata_5khz_uni_1500_20_400_y.txt"));
	m_pstrDdlfileX[34].Format(_T("ddldata_5khz_uni_1500_40_400_x.txt"));
	m_pstrDdlfileY[34].Format(_T("ddldata_5khz_uni_1500_40_400_y.txt"));
	//m_pstrDdlfileX[22].Format(_T("ddldata_5khz_uni_1900_10_400_x.txt"));
	//m_pstrDdlfileY[22].Format(_T("ddldata_5khz_uni_1900_10_400_y.txt"));
	//m_pstrDdlfileX[23].Format(_T("ddldata_5khz_uni_1900_20_400_x.txt"));
	//m_pstrDdlfileY[23].Format(_T("ddldata_5khz_uni_1900_20_400_y.txt"));
	m_pstrDdlfileX[22].Format(_T("ddldata_5khz_uni_2000_10_600_x.txt"));
	m_pstrDdlfileY[22].Format(_T("ddldata_5khz_uni_2000_10_600_y.txt"));
	m_pstrDdlfileX[23].Format(_T("ddldata_5khz_uni_2000_20_600_x.txt"));
	m_pstrDdlfileY[23].Format(_T("ddldata_5khz_uni_2000_20_600_y.txt"));
	m_pstrDdlfileX[28].Format(_T("ddldata_5khz_uni_10000_20_600_x.txt"));
	m_pstrDdlfileY[28].Format(_T("ddldata_5khz_uni_10000_20_600_y.txt"));
	m_pstrDdlfileX[24].Format(_T("ddldata_5khz_uni_10000_40_600_x.txt"));
	m_pstrDdlfileY[24].Format(_T("ddldata_5khz_uni_10000_40_600_y.txt"));
	m_pstrDdlfileX[25].Format(_T("ddldata_5khz_uni_10000_80_1000_x.txt"));
	m_pstrDdlfileY[25].Format(_T("ddldata_5khz_uni_10000_80_1000_y.txt"));
	//m_pstrDdlfileX[25].Format(_T("ddldata_5khz_uni_10000_80_600_x.txt"));
	//m_pstrDdlfileY[25].Format(_T("ddldata_5khz_uni_10000_80_600_y.txt"));


	//1Khz Uni-direction scan result file name
	m_pstrScamData[0].Format(_T("scandata_1000_20_600"));
	m_pstrScamData[1].Format(_T("scandata_1000_40_600"));
	m_pstrScamData[2].Format(_T("scandata_1000_80_600"));
	m_pstrScamData[3].Format(_T("scandata_1000_160_600"));
	m_pstrScamData[4].Format(_T("scandata_2000_10_600"));
	m_pstrScamData[5].Format(_T("scandata_2000_20_600"));
	m_pstrScamData[6].Format(_T("scandata_3000_10_600"));
	m_pstrScamData[7].Format(_T("scandata_3000_20_600"));
	m_pstrScamData[8].Format(_T("scandata_7000_40_600"));
	m_pstrScamData[9].Format(_T("scandata_10000_10_600"));
	m_pstrScamData[10].Format(_T("scandata_10000_20_600"));
	m_pstrScamData[11].Format(_T("scandata_10000_40_600"));
	m_pstrScamData[12].Format(_T("scandata_10000_80_600"));
	m_pstrScamData[13].Format(_T("scandata_10000_160_600"));
	//1Khz Bi-direction scan result file name
	m_pstrScamData[14].Format(_T("scandata_bi_1000_10_600"));
	m_pstrScamData[15].Format(_T("scandata_bi_1000_20_600"));
	m_pstrScamData[16].Format(_T("scandata_bi_2000_10_600"));
	m_pstrScamData[17].Format(_T("scandata_bi_2000_20_600"));
	m_pstrScamData[18].Format(_T("scandata_bi_3000_10_600"));
	m_pstrScamData[19].Format(_T("scandata_bi_3000_20_600"));
	//5Khz Bi-direction scan result file name
	m_pstrScamData[36].Format(_T("scandata_5khz_bi_1000_10_400"));
	m_pstrScamData[37].Format(_T("scandata_5khz_bi_1000_20_400"));
	m_pstrScamData[38].Format(_T("scandata_5khz_bi_1000_40_400"));
	m_pstrScamData[39].Format(_T("scandata_5khz_bi_1500_10_400"));
	m_pstrScamData[40].Format(_T("scandata_5khz_bi_1500_20_400"));
	m_pstrScamData[41].Format(_T("scandata_5khz_bi_1500_40_400"));
	m_pstrScamData[20].Format(_T("scandata_5khz_bi_2000_10_400"));
	m_pstrScamData[21].Format(_T("scandata_5khz_bi_2000_20_400"));
	m_pstrScamData[35].Format(_T("scandata_5khz_bi_2000_40_400"));
	//m_pstrScamData[42].Format(_T("scandata_5khz_bi_3000_10_400"));
	m_pstrScamData[42].Format(_T("scandata_5khz_bi_3000_10"));
	//m_pstrScamData[43].Format(_T("scandata_5khz_bi_3000_20_400"));
	m_pstrScamData[43].Format(_T("scandata_5khz_bi_3000_20"));
	m_pstrScamData[26].Format(_T("scandata_5khz_bi_10000_40_600"));
	m_pstrScamData[27].Format(_T("scandata_5khz_bi_10000_80_600"));
	m_pstrScamData[44].Format(_T("scandata_5khz_bi_10000_100_600"));
	m_pstrScamData[45].Format(_T("scandata_5khz_bi_10000_200_600"));
	//5Khz Uni-direction scan result file name
	m_pstrScamData[29].Format(_T("scandata_5khz_uni_1000_10_400"));
	m_pstrScamData[30].Format(_T("scandata_5khz_uni_1000_20_400"));
	m_pstrScamData[31].Format(_T("scandata_5khz_uni_1000_40_400"));
	m_pstrScamData[32].Format(_T("scandata_5khz_uni_1500_10_400"));
	m_pstrScamData[33].Format(_T("scandata_5khz_uni_1500_20_400"));
	m_pstrScamData[34].Format(_T("scandata_5khz_uni_1500_40_400"));
	//m_pstrScamData[22].Format(_T("scandata_5khz_uni_1900_10_400"));
	//m_pstrScamData[23].Format(_T("scandata_5khz_uni_1900_20_400"));
	m_pstrScamData[22].Format(_T("scandata_5khz_uni_2000_10_600"));
	m_pstrScamData[23].Format(_T("scandata_5khz_uni_2000_20_600"));
	m_pstrScamData[28].Format(_T("scandata_5khz_uni_10000_20_600"));
	m_pstrScamData[24].Format(_T("scandata_5khz_uni_10000_40_600"));
	m_pstrScamData[25].Format(_T("scandata_5khz_uni_10000_80_1000"));
	//m_pstrScamData[25].Format(_T("scandata_5khz_uni_10000_80_600"));


	//1Khz Uni-direction scan 회수(Y step 거리)
	m_piDdlScanNum[0] =	(int)(SCAN_SIZE_01UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM);	//80회 step: stroke 1000nm에 대한 20nm 간격은 50회 step. Drift감안 70로 설정
	m_piDdlScanNum[1] = (int)(SCAN_SIZE_01UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_600 / STEP_SIZE_40NM);	//40회(60회) step: stroke 1000nm에 대한 40nm 간격은 25회 step. Drift감안 45로 설정
	m_piDdlScanNum[2] =	(int)(SCAN_SIZE_01UM / STEP_SIZE_80NM + SCAN_ADD_SIZE_600 / STEP_SIZE_40NM);	//20회 step: stroke 1000nm에 대한 80nm 간격은 13회 step. Drift감안 30로 설정
	m_piDdlScanNum[3] =	(int)(SCAN_SIZE_01UM / STEP_SIZE_160NM + SCAN_ADD_SIZE_600 / STEP_SIZE_40NM);	//10회 step: stroke 1000nm에 대한 160nm 간격은 7회 step. Drift감안 15로 설정
	m_piDdlScanNum[4] =	(int)(SCAN_SIZE_02UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_600 / STEP_SIZE_10NM);	//260회 step: stroke 2000nm에 대한 10nm 간격은 200회 step. Drift감안 260로 설정
	m_piDdlScanNum[5] =	(int)(SCAN_SIZE_02UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM);	//130회 step: stroke 2000nm에 대한 20nm 간격은 100회 step. Drift감안 130로 설정
	m_piDdlScanNum[6] =	(int)(SCAN_SIZE_03UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_600 / STEP_SIZE_10NM);	//360회 step: stroke 3000nm에 대한 10nm 간격은 300회 step. Drift감안 320로 설정
	m_piDdlScanNum[7] =	(int)(SCAN_SIZE_03UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM);	//180회 step: stroke 3000nm에 대한 20nm 간격은 150회 step. Drift감안 170로 설정
	m_piDdlScanNum[8] =	(int)(SCAN_SIZE_07UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_600 / STEP_SIZE_40NM);	//190회 step: stroke 7000nm에 대한 40nm 간격은 175회 step. Drift감안 200로 설정
	m_piDdlScanNum[9] = (int)(SCAN_SIZE_10UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_600 / STEP_SIZE_10NM);	//1060회 step: stroke 10000nm에 대한 40nm 간격은 250회 step. Drift감안 280로 설정
	m_piDdlScanNum[10] = (int)(SCAN_SIZE_10UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM);	//530회(287회) step: stroke 10000nm에 대한 40nm 간격은 250회 step. Drift감안 280로 설정
	m_piDdlScanNum[11] = (int)(SCAN_SIZE_10UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_600 / STEP_SIZE_40NM);	//265회(287회) step: stroke 10000nm에 대한 40nm 간격은 250회 step. Drift감안 280로 설정
	m_piDdlScanNum[12] = 145;// (int)(SCAN_SIZE_10UM / STEP_SIZE_80NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM);	//140회(143회) step: stroke 10000nm에 대한 80nm 간격은 125회 step. Drift감안 140로 설정
	m_piDdlScanNum[13] = (int)(SCAN_SIZE_10UM / STEP_SIZE_160NM + SCAN_ADD_SIZE_600 / STEP_SIZE_40NM);	//77회(71회) step: stroke 10000nm에 대한 160nm 간격은 62회 step. Drift감안 70로 설정
	//1Khz Bi-direction scan 회수(Y step 거리)
	m_piDdlScanNum[14] = (int)(SCAN_SIZE_01UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_600 / STEP_SIZE_10NM) / 2.;	//80회 step: stroke 1000nm에 대한 20nm 간격은 50회 step. Drift감안 80로 설정
	m_piDdlScanNum[15] = (int)(SCAN_SIZE_01UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM) / 2.;	//40회 step: stroke 1000nm에 대한 40nm 간격은 25회 step. Drift감안 40로 설정
	m_piDdlScanNum[16] = (int)(SCAN_SIZE_02UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_600 / STEP_SIZE_10NM) / 2.;	//130회 step: stroke 2000nm에 대한 20nm 간격은 100회 step. Drift감안 130로 설정
	m_piDdlScanNum[17] = (int)(SCAN_SIZE_02UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM) / 2.;	//65회 step: stroke 2000nm에 대한 40nm 간격은 50회 step. Drift감안 65로 설정
	m_piDdlScanNum[18] = (int)(SCAN_SIZE_03UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_600 / STEP_SIZE_10NM) / 2.;	//180회 step: stroke 3000nm에 대한 20nm 간격은 150회 step. Drift감안 180로 설정
	m_piDdlScanNum[19] = (int)(SCAN_SIZE_03UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM) / 2.;	//90회 step: stroke 3000nm에 대한 40nm 간격은 75회 step. Drift감안 90로 설정
	//5Khz Bi-direction scan 회수(Y step 거리)
	m_piDdlScanNum[36] = (int)(SCAN_SIZE_01UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_400 / STEP_SIZE_10NM) / 2.;	//70회 step: stroke 1000nm에 대한 2*10nm 간격은 50회 step. Drift감안 70로 설정
	m_piDdlScanNum[37] = (int)(SCAN_SIZE_01UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_400 / STEP_SIZE_20NM) / 2.;	//35회 step: stroke 1000nm에 대한 2*20nm 간격은 25회 step. Drift감안 35로 설정
	m_piDdlScanNum[38] = (int)(SCAN_SIZE_01UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_400 / STEP_SIZE_40NM) / 2.;	//17회 step: stroke 1000nm에 대한 2*40nm 간격은 12.5회 step. Drift감안 17로 설정
	m_piDdlScanNum[39] = (int)(SCAN_SIZE_01_5UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_400 / STEP_SIZE_10NM) / 2.;	//95회 step: stroke 1500nm에 대한 2*10nm 간격은 75회 step. Drift감안 95로 설정
	m_piDdlScanNum[40] = (int)(SCAN_SIZE_01_5UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_400 / STEP_SIZE_20NM) / 2.;	//47회 step: stroke 1500nm에 대한 2*20nm 간격은 37.5회 step. Drift감안 47로 설정
	m_piDdlScanNum[41] = (int)(SCAN_SIZE_01_5UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_400 / STEP_SIZE_40NM) / 2.;	//23회 step: stroke 1500nm에 대한 2*40nm 간격은 18.75회 step. Drift감안 23로 설정
	m_piDdlScanNum[20] = (int)(SCAN_SIZE_02UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_400 / STEP_SIZE_10NM) / 2.;	//120회 step: stroke 2000nm에 대한 2*10nm 간격은 100회 step. Drift감안 120로 설정
	m_piDdlScanNum[21] = (int)(SCAN_SIZE_02UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_400 / STEP_SIZE_20NM) / 2.;	//60회 step: stroke 2000nm에 대한 2*20nm 간격은 50회 step. Drift감안 60로 설정
	m_piDdlScanNum[35] = (int)(SCAN_SIZE_02UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_400 / STEP_SIZE_40NM) / 2.;	//30회 step: stroke 2000nm에 대한 2*40nm 간격은 25회 step. Drift감안 30로 설정
	m_piDdlScanNum[42] = 150;// (int)(SCAN_SIZE_03UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_400 / STEP_SIZE_10NM) / 2.;	//120회 step: stroke 3000nm에 대한 2*10nm 간격은 150회 step. Drift감안 170로 설정
	m_piDdlScanNum[43] = 75;// (int)(SCAN_SIZE_03UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_400 / STEP_SIZE_20NM) / 2.;	//60회 step: stroke 3000nm에 대한 2*20nm 간격은 75회 step. Drift감안 85로 설정
	m_piDdlScanNum[26] = 140;// (int)(SCAN_SIZE_10UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_600 / STEP_SIZE_40NM) / 2.;	//140회(132.5) step: stroke 10000nm에 대한 40nm 간격은 250회 step. Drift감안 265/2=132.5 로 설정
	m_piDdlScanNum[27] = 70;// (int)(SCAN_SIZE_10UM / STEP_SIZE_80NM + SCAN_ADD_SIZE_600 / STEP_SIZE_40NM) / 2.;	//70회(66.25)회 step: stroke 10000nm에 대한 80nm 간격은 132.5회 step. Drift감안 132.5/2=66.25로 설정
	m_piDdlScanNum[44] = (int)(SCAN_SIZE_10UM / STEP_SIZE_100NM + SCAN_ADD_SIZE_600 / STEP_SIZE_100NM) / 2.;	//140회 step: stroke 10000nm에 대한 100nm 간격은 100회 step. Drift감안 106/2=53회로 설정
	m_piDdlScanNum[45] = (int)(SCAN_SIZE_10UM / STEP_SIZE_200NM + SCAN_ADD_SIZE_600 / STEP_SIZE_200NM) / 2.;	//70회 step: stroke 10000nm에 대한 200nm 간격은 50회 step. Drift감안 53/2=26.5회로 설정
	//5Khz Uni-direction scan 회수(Y step 거리)
	m_piDdlScanNum[29] = (int)(SCAN_SIZE_01UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_400 / STEP_SIZE_10NM);	//140회 step: stroke 1000nm에 대한 10nm 간격은 100회 step. Drift감안 120로 설정
	m_piDdlScanNum[30] = (int)(SCAN_SIZE_01UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_400 / STEP_SIZE_20NM);	//70회 step: stroke 1000nm에 대한 20nm 간격은 50회 step. Drift감안 70로 설정
	m_piDdlScanNum[31] = (int)(SCAN_SIZE_01UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_400 / STEP_SIZE_20NM);	//45회(35회) step: stroke 1000nm에 대한 40nm 간격은 25회 step. Drift감안 45로 설정
	m_piDdlScanNum[32] = (int)(SCAN_SIZE_01_5UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_400 / STEP_SIZE_10NM);	//190회 step: stroke 1500nm에 대한 10nm 간격은 150회 step. Drift감안 170로 설정
	m_piDdlScanNum[33] = (int)(SCAN_SIZE_01_5UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_400 / STEP_SIZE_20NM);	//95회 step: stroke 1500nm에 대한 20nm 간격은 75회 step. Drift감안 95로 설정
	m_piDdlScanNum[34] = (int)(SCAN_SIZE_01_5UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_400 / STEP_SIZE_20NM);	//57회(47회) step: stroke 1500nm에 대한 40nm 간격은 37회 step. Drift감안 57로 설정
	//m_piDdlScanNum[22] = (int)(SCAN_SIZE_019UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_400 / STEP_SIZE_10NM);	//260회 step: stroke 1900nm에 대한 10nm 간격은 190회 step. Drift감안 230로 설정
	//m_piDdlScanNum[23] = (int)(SCAN_SIZE_019UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_400 / STEP_SIZE_20NM);	//130회 step: stroke 1900nm에 대한 20nm 간격은 95회 step. Drift감안 115로 설정
	m_piDdlScanNum[22] = (int)(SCAN_SIZE_02UM / STEP_SIZE_10NM + SCAN_ADD_SIZE_600 / STEP_SIZE_10NM);	//260회 step: stroke 2000nm에 대한 10nm 간격은 200회 step. Drift감안 260로 설정
	m_piDdlScanNum[23] = (int)(SCAN_SIZE_02UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM);	//130회 step: stroke 2000nm에 대한 20nm 간격은 100회 step. Drift감안 130로 설정
	m_piDdlScanNum[28] = (int)(SCAN_SIZE_10UM / STEP_SIZE_20NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM);	//530회 step: stroke 10000nm에 대한 20nm 간격은 500회 step. Drift감안 530로 설정
	m_piDdlScanNum[24] = (int)(SCAN_SIZE_10UM / STEP_SIZE_40NM + SCAN_ADD_SIZE_600 / STEP_SIZE_40NM);	//265회 step: stroke 10000nm에 대한 40nm 간격은 250회 step. Drift감안 265로 설정
	m_piDdlScanNum[25] = (int)(SCAN_SIZE_10UM / STEP_SIZE_80NM + SCAN_ADD_SIZE_1000 / STEP_SIZE_40NM);	//155회(137회) step: stroke 10000nm에 대한 80nm 간격은 125회 step. Drift감안 137->150로 설정
	//m_piDdlScanNum[25] = (int)(SCAN_SIZE_10UM / STEP_SIZE_80NM + SCAN_ADD_SIZE_600 / STEP_SIZE_20NM);	//155회(133회) step: stroke 10000nm에 대한 80nm 간격은 125회 step. Drift감안 133->155로 설정

}

CPIE712Ctrl::~CPIE712Ctrl()
{
	if (m_dpdDdlDataX != NULL)
	{
		for (int i = 0; i < DDL_LEARNING_NUMBER; i++)
			delete m_dpdDdlDataX[i];
		delete m_dpdDdlDataX;
	}

	if (m_dpdDdlDataY != NULL)
	{
		for (int i = 0; i < DDL_LEARNING_NUMBER; i++)
			delete m_dpdDdlDataY[i];
		delete m_dpdDdlDataY;
	}

	if (m_piPointOfSpeedUpDown != NULL)
		delete m_piPointOfSpeedUpDown;

	if (m_pdServoCalculationSpeed != NULL)
		delete m_pdServoCalculationSpeed;

	if (m_piDdlLearningRepeatNumber != NULL)
		delete m_piDdlLearningRepeatNumber;

	if (m_piRecordDataNumber != NULL)
		delete m_piRecordDataNumber;

	if (m_piDdlPointNumber != NULL)
		delete m_piDdlPointNumber;

	if (m_piCenterpointOfWave != NULL)
		delete m_piCenterpointOfWave;

	if (m_pdAmplitudeOfRampWave != NULL)
		delete m_pdAmplitudeOfRampWave;

	if (m_pdAmplitudeOfLineWave != NULL)
		delete m_pdAmplitudeOfLineWave;

	if (m_pdValueDdlArrayX != NULL)
		delete m_pdValueDdlArrayX;

	if (m_pdValueDdlArrayY != NULL)	
		delete m_pdValueDdlArrayY;

	if (m_pstrDdlfileX != NULL)
		delete[] m_pstrDdlfileX;

	if (m_pstrDdlfileY != NULL)	
		delete[] m_pstrDdlfileY;

	if (m_piDdlScanNum != NULL)
		delete m_piDdlScanNum;

	if (m_pstrScamData != NULL)
		delete[] m_pstrScamData;
}

BOOL CPIE712Ctrl::ConnectComm(int CommunicationType, char *Ip, int nPort)
{
	m_bConnect = FALSE;
	switch (CommunicationType)
	{
	case ETHERNET:
		m_nPIStage_ID = PI_ConnectTCPIP(Ip, nPort);
		if (m_nPIStage_ID < 0)
		{
			m_nErrorCode = PI_GetError(m_nPIStage_ID);
			PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
			TRACE(_T("ConnectTCP/IP: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
			m_bConnect = FALSE;
		}
		else
		{
			m_bConnect = TRUE;
		}
		break;
	case SERIAL:
		break;
	case SIMULATOR:
		break;
	}
	return m_bConnect;
}

int CPIE712Ctrl::DisconnectComm()
{
	PI_CloseConnection(m_nPIStage_ID);
	m_bConnect = FALSE;
	return 0;
}

void CPIE712Ctrl::Initialize_PIStage()
{
	if (m_bConnect == FALSE)
		return;

	Get_Stage_Axes();
	ServoOn();
	ClearDDLTable();
	SetWaveGeneratorOffset();
	GetPosAxesData();
	Move_XY_Origin_Position();	//smchoi: 2020.11 PI Stage 수리 후 AutoZero 하면 X,Y가 - 위치로 이동되어 있음. 이런경우 out of limit error가 발생하므로 x,y를 >0 위치로 이동 시킨다.
	MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	//Move_AllAxis_Initial_Position();
}

void CPIE712Ctrl::ServoOn()
{
	if (m_bConnect == FALSE)
		return;

	BOOL bFlags[5];

	bFlags[0] = TRUE; // servo on for the axis in the string 'axes'.
	bFlags[1] = TRUE; // servo on for the axis in the string 'axes'.
	bFlags[2] = TRUE; // servo on for the axis in the string 'axes'.
	bFlags[3] = TRUE; // servo on for the axis in the string 'axes'.
	bFlags[4] = TRUE; // servo on for the axis in the string 'axes'.

	if (!PI_SVO(m_nPIStage_ID, m_chPIStage_Axis, bFlags))
		TRACE(_T("PI_SVO Error"));
}

void CPIE712Ctrl::DDL_DataLoadingFromFile()
{
	int i = 0, j = 0;
	CString strDdlDataX, strDdlDataY;
	ifstream ddlfileX, ddlfileY;
	CString str;
	//X axis
	str = PISTAGE_DDL_FILE_PATH + CString(_T("\\")) + m_pstrDdlfileX[m_nStrokeNo];

	for (i = 0; i < DDL_LEARNING_NUMBER; i++)
	{
		strDdlDataX = PISTAGE_DDL_FILE_PATH + CString(_T("\\")) + m_pstrDdlfileX[i];
		strDdlDataY = PISTAGE_DDL_FILE_PATH + CString(_T("\\")) + m_pstrDdlfileY[i];
		ddlfileX.open(strDdlDataX.GetBuffer(0));
		ddlfileY.open(strDdlDataY.GetBuffer(0));
		for (j = 0; j < m_piDdlPointNumber[i]; j++)
		{
			ddlfileX >> m_dpdDdlDataX[i][j];
			ddlfileY >> m_dpdDdlDataY[i][j];
		}
		ddlfileX.close();
		ddlfileY.close();
	}
}

void CPIE712Ctrl::Move_AllAxis_Zero_Position()
{
	if (m_bConnect == FALSE)
		return;
	m_dPIStage_MovePos[X_AXIS] = 0;	// 1um
	m_dPIStage_MovePos[Y_AXIS] = 0;	// 1um
	m_dPIStage_MovePos[Z_AXIS] = 0;	// 0um
	m_dPIStage_MovePos[TIP_AXIS] = 0;	// 0um
	m_dPIStage_MovePos[TILT_AXIS] = 0;	// 0um
	PI_Move_Absolute();
}

void CPIE712Ctrl::Move_AllAxis_Initial_Position()
{
	if (m_bConnect == FALSE)
		return;
	m_dPIStage_MovePos[X_AXIS] = X_INITIAL_POS_UM;
	m_dPIStage_MovePos[Y_AXIS] = Y_INITIAL_POS_UM;
	m_dPIStage_MovePos[Z_AXIS] = Z_INITIAL_POS_UM;
	m_dPIStage_MovePos[TIP_AXIS] = m_dInitialTx_urad;		// 170 urad
	m_dPIStage_MovePos[TILT_AXIS] = m_dInitialTy_urad;	// -180 urad
	PI_Move_Absolute();
}

void CPIE712Ctrl::Move_XY_Origin_Position()
{
	if (m_bConnect == FALSE)
		return;
	m_dPIStage_MovePos[X_AXIS] = X_INITIAL_POS_UM;
	m_dPIStage_MovePos[Y_AXIS] = Y_INITIAL_POS_UM;
	m_dPIStage_MovePos[Z_AXIS] = m_dPIStage_GetPos[Z_AXIS];
	m_dPIStage_MovePos[TIP_AXIS] = m_dInitialTx_urad;
	m_dPIStage_MovePos[TILT_AXIS] = m_dInitialTy_urad;
	PI_Move_Absolute();
}

//아래 Code는 Uni-Direction Only일때
//void CPIE712Ctrl::WaveGeneration()
//{
//	if (m_bConnect == FALSE)
//		return;
//
//	//PROFILE_TIME:2um(fov),20nm(step),600nm(accel/dccel), Servo Control이 20Khz일때
//	//m_piDdlPointNumber[0] = 3000 points	
//	//m_piCenterpointOfWave[0] = 2600 points
//	//m_pdAmplitudeOfRampWave[0] = 2.6 um	//accel/dccel 각 0.3um
//	//m_pdAmplitudeOfLineWave[0] = 0.02 um
//	//PI_WAV_RAMP(0, 1, 0, 3000, 0, 2600, 10, 2.6, 0.0, 3000)
//
//	//PROFILE_TIME:2um(fov),20nm(step),600nm(accel/dccel), Servo Control이 16.6Khz일때
//	//m_piDdlPointNumber[0] = 2558 points	
//	//m_piCenterpointOfWave[0] = 2158 points
//	//m_pdAmplitudeOfRampWave[0] = 2.6 um	//accel/dccel 각 0.3um
//	//m_pdAmplitudeOfLineWave[0] = 0.02 um
//	//PI_WAV_RAMP(0, 1, 0, 2558, 0, 2158, 10, 2.6, 0.0, 2558)
//
//	//PROFILE_TIME:2um(fov),10nm(step),200nm(accel/dccel)
//	//m_piDdlPointNumber[1] =4800 points
//	//m_piCenterpointOfWave[1] = 4400 points
//	//m_pdAmplitudeOfRampWave[1] = 2.2 um	//accel/dccel 각 0.3um
//	//m_pdAmplitudeOfLineWave[1] = 0.01 um
//	//PI_WAV_RAMP(0, 1, 0, 4800, 0, 4400, 10, 2.2, 0.0, 4800)
//
//	//PROFILE_TIME:3um(fov),20nm(step),600nm(accel/dccel)
//	//m_piDdlPointNumber[2] = 4000 points
//	//m_piCenterpointOfWave[2] = 3600 points	//accel/dccel 각 0.3um
//	//m_pdAmplitudeOfRampWave[2] = 3.6 um
//	//m_pdAmplitudeOfLineWave[2] = 0.02 um
//	//PI_WAV_RAMP(0, 1, 0,    4000,         0,           3600,             10,            3.6,     0.0,      4000)		->        Uni-direction으로 3.6um(20nm step) stroke로 Asymetric하게 만든 Ramp wave
//	//                    <SegLength> <StartPoint> <CurveCenterPoint> <SpeedUpDown>      <Amp>   <Offset> <WaveLength> 
//	//PI_WAV_RAMP(0, 1, 0,    7200,         0,           3600,             10,            3.6,     0.0,      7200)		->        Bi-direction으로 3.6um(20nm step) stroke로 Symetric하게 만든 Ramp wave
//	//                    <SegLength> <StartPoint> <CurveCenterPoint> <SpeedUpDown>      <Amp>   <Offset> <WaveLength> 
//	//4um_vel20: 1 WAV 1 X RAMP 6666       4       3         6666          0           10              3333				-> PI에서 Bi-direction으로 4  um(20nm step) stroke로 Symetric하게 만든 Ramp wave
//	//                       <SegLength> <Amp> <Offset> <WaveLength> <StartPoint> <SpeedUpDown> <CurveCenterPoint>
//
//	//PROFILE_TIME:3um(fov),10nm(step),600nm(accel/dccel)
//	//m_piDdlPointNumber[3] = 4000 points
//	//m_piCenterpointOfWave[3] = 3600 points
//	//m_pdAmplitudeOfRampWave[3] = 3.6 um	//accel/dccel 각 0.3um
//	//m_pdAmplitudeOfLineWave[3] = 0.02 um
//
//	//PROFILE_TIME:7um(fov),40nm(step),600nm(accel/dccel)
//	//m_piDdlPointNumber[4] = 4000 points
//	//m_piCenterpointOfWave[4] = 3600 points
//	//m_pdAmplitudeOfRampWave[4] = 3.6 um	//accel/dccel 각 0.3um
//	//m_pdAmplitudeOfLineWave[4] = 0.04 um
//	//PI_WAV_RAMP(0, 1, 0,    15200,         0,           7600,             10,            3.6,     0.0,      15200)	->        Uni-direction으로 7.6um(40nm step) stroke로 Symetric하게 만든 Ramp wave
//	//                    <SegLength> <StartPoint> <CurveCenterPoint> <SpeedUpDown>      <Amp>   <Offset> <WaveLength> 
//	//PI_WAV_RAMP(0, 1, 0,    15200,         0,           7600,             10,            3.6,     0.0,      15200)	->        Bi-direction으로 7.6um(20nm step) stroke로 Symetric하게 만든 Ramp wave
//	//                    <SegLength> <StartPoint> <CurveCenterPoint> <SpeedUpDown>      <Amp>   <Offset> <WaveLength> 
//	//8um_vel20: 1 WAV 1 X RAMP 13333      8       1       13333          0            10             6666				-> PI에서 Bi-direction으로 8  um(20nm step) stroke로 Symetric하게 만든 Ramp wave -> Controller가 20KHz가 아니라는 의미
//	//                       <SegLength> <Amp> <Offset> <WaveLength> <StartPoint> <SpeedUpDown> <CurveCenterPoint>
//
//	//PROFILE_TIME:10um(fov),40nm(step),600nm(accel/dccel)
//	//m_piDdlPointNumber[5] = 4000 points
//	//m_piCenterpointOfWave[5] = 3600 points
//	//m_pdAmplitudeOfRampWave[5] = 3.6 um	//accel/dccel 각 0.3um
//	//m_pdAmplitudeOfLineWave[5] = 0.04 um
//
//	//Scan Axis Wave Generation
//	if (!PI_WAV_RAMP(m_nPIStage_ID, m_iWaveTableIds[0], 0, m_piDdlPointNumber[m_nStrokeNo], 0, m_piCenterpointOfWave[m_nStrokeNo], SPEED_UPDOWN_POINTNO, m_pdAmplitudeOfRampWave[m_nStrokeNo], 0.0, m_piDdlPointNumber[m_nStrokeNo]))
//	{
//		m_nErrorCode = PI_GetError(m_nPIStage_ID);
//		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
//		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
//		DisconnectComm();
//	}
//
//	//Step Axis Wave Generation
//	//if (m_nStrokeNo == 0)//For 2um FOV, 20nm step, 200nm Acel/Dcel Distance, Uni-Direction Scan
//	//{
//	if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, (int)m_piCenterpointOfWave[m_nStrokeNo], 0, SPEED_UPDOWN_POINTNO, 0.0, 0.0, (int)m_piCenterpointOfWave[m_nStrokeNo]))
//	{
//		m_nErrorCode = PI_GetError(m_nPIStage_ID);
//		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
//		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
//		DisconnectComm();
//	}
//
//	if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, UNIDIRECTION_RETURN_POINTNO, 2, SPEED_UPDOWN_POINTNO, m_pdAmplitudeOfLineWave[m_nStrokeNo], 0.0, UNIDIRECTION_RETURN_POINTNO))
//	{
//		m_nErrorCode = PI_GetError(m_nPIStage_ID);
//		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
//		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
//		DisconnectComm();
//	}
//	//}
//	//else if (m_nStrokeNo == 1)//For 2um FOV, 10nm step, 200nm Acel/Dcel Distance, Uni-Direction Scan
//	//{
//	//	if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, (int)m_piCenterpointOfWave[m_nStrokeNo], 0, SPEED_UPDOWN_POINTNO, 0.0, 0.0, (int)m_piCenterpointOfWave[m_nStrokeNo]))
//	//	{
//	//		m_nErrorCode = PI_GetError(m_nPIStage_ID);
//	//		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
//	//		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
//	//		DisconnectComm();
//	//	}
//
//	//	if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, UNIDIRECTION_RETURN_POINTNO, 2, SPEED_UPDOWN_POINTNO, m_pdAmplitudeOfLineWave[m_nStrokeNo], 0.0, UNIDIRECTION_RETURN_POINTNO))
//	//	{
//	//		m_nErrorCode = PI_GetError(m_nPIStage_ID);
//	//		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
//	//		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
//	//		DisconnectComm();
//	//	}
//	//}
//	//else if (m_nStrokeNo == 2)//For 3um FOV, 20nm step, 600nm Acel/Dcel Distance, Uni-Direction Scan
//	//{
//	//	if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, (int)m_piCenterpointOfWave[m_nStrokeNo], 0, SPEED_UPDOWN_POINTNO, 0.0, 0.0, (int)m_piCenterpointOfWave[m_nStrokeNo]))
//	//	{
//	//		m_nErrorCode = PI_GetError(m_nPIStage_ID);
//	//		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
//	//		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
//	//		DisconnectComm();
//	//	}
//
//	//	if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, UNIDIRECTION_RETURN_POINTNO, 2, SPEED_UPDOWN_POINTNO, m_pdAmplitudeOfLineWave[m_nStrokeNo], 0.0, UNIDIRECTION_RETURN_POINTNO))
//	//	{
//	//		m_nErrorCode = PI_GetError(m_nPIStage_ID);
//	//		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
//	//		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
//	//		DisconnectComm();
//	//	}
//	//}
//
//}

//Uni-direction Wave + Bi-direction Wave
void CPIE712Ctrl::WaveGeneration()
{
	if (m_bConnect == FALSE)
		return;

	//PROFILE_TIME:2um(fov),20nm(step),600nm(accel/dccel), Servo Control이 20Khz일때
	//m_piDdlPointNumber[0] = 3000 points	
	//m_piCenterpointOfWave[0] = 2600 points
	//m_pdAmplitudeOfRampWave[0] = 2.6 um	//accel/dccel 각 0.3um
	//m_pdAmplitudeOfLineWave[0] = 0.02 um
	//PI_WAV_RAMP(0, 1, 0, 3000, 0, 2600, 10, 2.6, 0.0, 3000)

	//PROFILE_TIME:2um(fov),20nm(step),600nm(accel/dccel), Servo Control이 16.6Khz일때
	//m_piDdlPointNumber[0] = 2558 points	
	//m_piCenterpointOfWave[0] = 2158 points
	//m_pdAmplitudeOfRampWave[0] = 2.6 um	//accel/dccel 각 0.3um
	//m_pdAmplitudeOfLineWave[0] = 0.02 um
	//PI_WAV_RAMP(0, 1, 0, 2558, 0, 2158, 10, 2.6, 0.0, 2558)

	//PROFILE_TIME:2um(fov),10nm(step),200nm(accel/dccel)
	//m_piDdlPointNumber[1] =4800 points
	//m_piCenterpointOfWave[1] = 4400 points
	//m_pdAmplitudeOfRampWave[1] = 2.2 um	//accel/dccel 각 0.3um
	//m_pdAmplitudeOfLineWave[1] = 0.01 um
	//PI_WAV_RAMP(0, 1, 0, 4800, 0, 4400, 10, 2.2, 0.0, 4800)

	//PROFILE_TIME:3um(fov),20nm(step),600nm(accel/dccel)
	//m_piDdlPointNumber[2] = 4000 points
	//m_piCenterpointOfWave[2] = 3600 points	//accel/dccel 각 0.3um
	//m_pdAmplitudeOfRampWave[2] = 3.6 um
	//m_pdAmplitudeOfLineWave[2] = 0.02 um
	//PI_WAV_RAMP(0, 1, 0,    4000,         0,           3600,             10,            3.6,     0.0,      4000)		->        Uni-direction으로 3.6um(20nm step) stroke로 Asymetric하게 만든 Ramp wave
	//                    <SegLength> <StartPoint> <CurveCenterPoint> <SpeedUpDown>      <Amp>   <Offset> <WaveLength> 
	//PI_WAV_RAMP(0, 1, 0,    7200,         0,           3600,             10,            3.6,     0.0,      7200)		->        Bi-direction으로 3.6um(20nm step) stroke로 Symetric하게 만든 Ramp wave(at 20 khz)
	//                    <SegLength> <StartPoint> <CurveCenterPoint> <SpeedUpDown>      <Amp>   <Offset> <WaveLength> 

	//PI_WAV_RAMP(0, 1, 0,    7200,         0,           3600,             10,            3.6,     0.0,      7200)		->        Bi-direction으로 2.4um(20nm step) stroke로 Symetric하게 만든 Ramp wave(at 16.667 khz)
	//                    <SegLength> <StartPoint> <CurveCenterPoint> <SpeedUpDown>      <Amp>   <Offset> <WaveLength> 

	// PI에서 Bi-direction으로 4um(20nm step) stroke로 Symetric하게 만든 Ramp wave(at 16.667 khz)
	// 4um_vel20: 1 WAV 1 X RAMP 6666       4       3         6666          0           10              3333				
	//                        <SegLength> <Amp> <Offset> <WaveLength> <StartPoint> <SpeedUpDown> <CurveCenterPoint>
	//            1 WAV 2 X LIN 3333 0 0.02 3333 0 10			//Set Waveform Definition LINE Wave
	//	          1 WAV 2 & LIN 3333 0 0.04 3333 0 10			//Set Waveform Definition LINE Wave

	//PROFILE_TIME:3um(fov),10nm(step),600nm(accel/dccel)
	//m_piDdlPointNumber[3] = 4000 points
	//m_piCenterpointOfWave[3] = 3600 points
	//m_pdAmplitudeOfRampWave[3] = 3.6 um	//accel/dccel 각 0.3um
	//m_pdAmplitudeOfLineWave[3] = 0.02 um

	//PROFILE_TIME:7um(fov),40nm(step),600nm(accel/dccel)
	//m_piDdlPointNumber[4] = 4000 points
	//m_piCenterpointOfWave[4] = 3600 points
	//m_pdAmplitudeOfRampWave[4] = 3.6 um	//accel/dccel 각 0.3um
	//m_pdAmplitudeOfLineWave[4] = 0.04 um
	//PI_WAV_RAMP(0, 1, 0,    15200,         0,           7600,             10,            3.6,     0.0,      15200)	->        Uni-direction으로 7.6um(40nm step) stroke로 Symetric하게 만든 Ramp wave
	//                    <SegLength> <StartPoint> <CurveCenterPoint> <SpeedUpDown>      <Amp>   <Offset> <WaveLength> 
	//PI_WAV_RAMP(0, 1, 0,    15200,         0,           7600,             10,            3.6,     0.0,      15200)	->        Bi-direction으로 7.6um(20nm step) stroke로 Symetric하게 만든 Ramp wave
	//                    <SegLength> <StartPoint> <CurveCenterPoint> <SpeedUpDown>      <Amp>   <Offset> <WaveLength> 
	//8um_vel20: 1 WAV 1 X RAMP 13333      8       1       13333          0            10             6666				-> PI에서 Bi-direction으로 8  um(20nm step) stroke로 Symetric하게 만든 Ramp wave -> Controller가 20KHz가 아니라는 의미
	//                       <SegLength> <Amp> <Offset> <WaveLength> <StartPoint> <SpeedUpDown> <CurveCenterPoint>

	//PROFILE_TIME:10um(fov),40nm(step),600nm(accel/dccel)
	//m_piDdlPointNumber[5] = 4000 points
	//m_piCenterpointOfWave[5] = 3600 points
	//m_pdAmplitudeOfRampWave[5] = 3.6 um	//accel/dccel 각 0.3um
	//m_pdAmplitudeOfLineWave[5] = 0.04 um

	//PROFILE: 5Khz_Bidirection,2um(fov)_20nm(step)_400nm(drift distance)
	//PI_WAV_RAMP(0, 2, 0, 800, 0, 400, 40, 2.4, 0.0, 800)
	//PI_WAV_LIN(0, 1, 0, 360, 0, 10, 0.0, 0.0, 360)
	//PI_WAV_LIN(0, 1, 0, 40, 2, 10, 0.02, 0.0, 40)
	//PI_WAV_LIN(0, 1, 0, 360, 2, 10, 0.0, 0.02, 360)
	//PI_WAV_LIN(0, 1, 0, 40, 2, 10, 0.02, 0.02, 40)

	//Scan Axis Uni&Bi-direction Wave Generation
	if (!PI_WAV_RAMP(m_nPIStage_ID, m_iWaveTableIds[0], 0, m_piDdlPointNumber[m_nStrokeNo], 0, m_piCenterpointOfWave[m_nStrokeNo], m_piPointOfSpeedUpDown[m_nStrokeNo], m_pdAmplitudeOfRampWave[m_nStrokeNo], 0.0, m_piDdlPointNumber[m_nStrokeNo]))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}

	//Step Axis Bi-direction Wave Generation
	if (m_nStrokeNo == 14 || m_nStrokeNo == 15 || m_nStrokeNo == 16 || m_nStrokeNo == 17 || m_nStrokeNo == 18 || m_nStrokeNo == 19 || m_nStrokeNo == 20 || m_nStrokeNo == 21 || m_nStrokeNo == 26 || m_nStrokeNo == 27
		|| m_nStrokeNo == 35 || m_nStrokeNo == 36 || m_nStrokeNo == 37 || m_nStrokeNo == 38 || m_nStrokeNo == 39 || m_nStrokeNo == 40 || m_nStrokeNo == 41 || m_nStrokeNo == 42 || m_nStrokeNo == 43 || m_nStrokeNo == 44 || m_nStrokeNo == 45)
	{
		if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, (int)m_piCenterpointOfWave[m_nStrokeNo] - 40, 0, 10, 0.0, 0.0, (int)m_piCenterpointOfWave[m_nStrokeNo] - 40))
		{
			m_nErrorCode = PI_GetError(m_nPIStage_ID);
			PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
			TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
			DisconnectComm();
		}
		if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, 40, 2, 10, m_pdAmplitudeOfLineWave[m_nStrokeNo], 0.0, 40))
		{
			m_nErrorCode = PI_GetError(m_nPIStage_ID);
			PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
			TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
			DisconnectComm();
		}
		if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, (int)m_piCenterpointOfWave[m_nStrokeNo] - 40, 2, 10, 0.0, m_pdAmplitudeOfLineWave[m_nStrokeNo], (int)m_piCenterpointOfWave[m_nStrokeNo] - 40))
		{
			m_nErrorCode = PI_GetError(m_nPIStage_ID);
			PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
			TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
			DisconnectComm();
		}
		if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, 40, 2, 10, m_pdAmplitudeOfLineWave[m_nStrokeNo], m_pdAmplitudeOfLineWave[m_nStrokeNo], 40))
		{
			m_nErrorCode = PI_GetError(m_nPIStage_ID);
			PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
			TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
			DisconnectComm();
		}
	}
	else //Step Axis Uni-direction Wave Generation
	{
		if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, (int)m_piCenterpointOfWave[m_nStrokeNo], 0, 10, 0.0, 0.0, (int)m_piCenterpointOfWave[m_nStrokeNo]))
		{
			m_nErrorCode = PI_GetError(m_nPIStage_ID);
			PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
			TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
			DisconnectComm();
		}
		if (!PI_WAV_LIN(m_nPIStage_ID, m_iWaveTableIds[1], 0, UNIDIRECTION_RETURN_POINTNO, 2, 10, m_pdAmplitudeOfLineWave[m_nStrokeNo], 0.0, UNIDIRECTION_RETURN_POINTNO))
		{
			m_nErrorCode = PI_GetError(m_nPIStage_ID);
			PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
			TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
			DisconnectComm();
		}
	}
}

void CPIE712Ctrl::ScanStart()
{
	if (m_bConnect == FALSE)
		return;
	WaveGeneration();
	LoadDDLdatatoController();
	Scanning_Motion(1);
}

void CPIE712Ctrl::Scanning_Motion(int nRepeat)
{
	if (m_bConnect == FALSE)
		return;

	MSG msg;
	double dZvalue = 0.0;
	ClearDDLTable();
	GetPosAxesData();
	dZvalue = m_dPIStage_GetPos[Z_AXIS];
	Move_XY_Origin_Position();
	SetWaveGeneratorOffset();

	int i = 0, j = 0, k = 0;
	m_nScanNum = 0;
	for (i = 0; i < nRepeat; i++)
	{
		// Make Wave
		WaveGeneration();
		//connects a wave table to a wave generator
		WaveTableSelection();

		LoadDDLdatatoController();

		// Set the number of cycles for the wave generator output
		SetNumberOfCycleForWaveGeneration(m_piDdlScanNum[m_nStrokeNo]);

		//여기서 반복하면 되는지 테스트 필요
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Start Wave Generation
		RunWaveGenerator(m_scan_mode[0], m_scan_mode[1]);

		m_pbValueArray[m_nScanAxis] = TRUE;
		m_pbValueArray[m_nStepAxis] = TRUE;
		m_bScanStop = FALSE;
		while (m_pbValueArray[m_nScanAxis] == TRUE)
		{
			if (!PI_IsGeneratorRunning(m_nPIStage_ID, m_iWaveTableIds, m_pbValueArray, DDL_AXIS_NUMBER))
			{
				m_nErrorCode = PI_GetError(m_nPIStage_ID);
				PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
				TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
				DisconnectComm();
			}

			if (m_bScanStop == TRUE)
			{
				if (!PI_STP(m_nPIStage_ID))
				{
					m_nErrorCode = PI_GetError(m_nPIStage_ID);
					PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
					TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
					DisconnectComm();
				}
				m_pbValueArray[m_nScanAxis] = FALSE;
				m_pbValueArray[m_nStepAxis] = FALSE;
				m_nScanNum = nRepeat - 1;	// 여러번 Scan할때 중지시키기 위해
			}
			if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		GetPosAxesData();
		int steppos = int(m_dPIStage_GetPos[m_nStepAxis]);
		for (j = 0; j < steppos; j++)
		{
			for ( k = 0; k < AXIS_NUMBER; k++)
				m_dPIStage_MovePos[k] = 0;
			m_dPIStage_MovePos[m_nStepAxis] = -1;
			PI_Move_Relative();
			Sleep(10);
		}

		m_dPIStage_MovePos[X_AXIS] = X_INITIAL_POS_UM;
		m_dPIStage_MovePos[Y_AXIS] = Y_INITIAL_POS_UM;
		m_dPIStage_MovePos[Z_AXIS] = dZvalue;
		m_dPIStage_MovePos[TIP_AXIS] = m_dInitialTx_urad;
		m_dPIStage_MovePos[TILT_AXIS] = m_dInitialTy_urad;
		PI_Move_Absolute();
		SetWaveGeneratorOffset();
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		ClearDDLTable();

		m_nScanNum++;
		if (m_nScanNum == nRepeat)
			break;
	}

	GetPosAxesData();
	while (int(m_dPIStage_GetPos[Y_AXIS]*1000) != int(Y_INITIAL_POS_UM*1000))
	{
		GetPosAxesData();
	}
	//TRACE(_T("scan y = %f.3: %s\n"), m_dPIStage_GetPos[Y_AXIS]);
}

void CPIE712Ctrl::DDL_Generation()
{
	if (m_bConnect == FALSE)
		return;

	MSG msg;
	ofstream filesave;
	double dZvalue = 0.0;

	ClearDDLTable();
	GetPosAxesData();
	dZvalue = m_dPIStage_GetPos[Z_AXIS];
	Move_XY_Origin_Position();
	SetWaveGeneratorOffset();

	// Make Wave
	WaveGeneration();

	//connects a wave table to a wave generator
	WaveTableSelection();

	// Set the number of cycles for the wave generator output
	//SetNumberOfCycleForWaveGeneration(DDL_LEARNING_REPEAT_NUM);
	SetNumberOfCycleForWaveGeneration(m_piDdlLearningRepeatNumber[m_nStrokeNo]);

	// Start Wave Generation
	RunWaveGenerator(m_scan_rep_mode[0], m_scan_rep_mode[1]);

	m_pbValueArray[m_nScanAxis] = TRUE;
	m_pbValueArray[m_nStepAxis] = TRUE;
	m_bScanStop = FALSE;
	while (m_pbValueArray[m_nScanAxis] == TRUE)
	{
		if (!PI_IsGeneratorRunning(m_nPIStage_ID, m_iWaveTableIds, m_pbValueArray, DDL_AXIS_NUMBER))
		{
			m_nErrorCode = PI_GetError(m_nPIStage_ID);
			PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
			TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
			DisconnectComm();
		}

		if (m_bScanStop == TRUE)
		{
			if (!PI_STP(m_nPIStage_ID))
			{
				m_nErrorCode = PI_GetError(m_nPIStage_ID);
				PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
				TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
				DisconnectComm();
			}
			m_pbValueArray[m_nScanAxis] = FALSE;
			m_pbValueArray[m_nStepAxis] = FALSE;
		}
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	int j = 0, k = 0;
	GetPosAxesData();
	int steppos = int(m_dPIStage_GetPos[m_nStepAxis]);
	for (j = 0; j < steppos; j++)
	{
		for (k = 0; k < AXIS_NUMBER; k++)
			m_dPIStage_MovePos[k] = 0;
		m_dPIStage_MovePos[m_nStepAxis] = -1;
		PI_Move_Relative();
		Sleep(10);
	}

	m_dPIStage_MovePos[X_AXIS] = X_INITIAL_POS_UM;
	m_dPIStage_MovePos[Y_AXIS] = Y_INITIAL_POS_UM;
	m_dPIStage_MovePos[Z_AXIS] = dZvalue;
	m_dPIStage_MovePos[TIP_AXIS] = m_dInitialTx_urad;
	m_dPIStage_MovePos[TILT_AXIS] = m_dInitialTy_urad;
	PI_Move_Absolute();
	SetWaveGeneratorOffset();

	// DDL Data를 컨트롤러로부터 읽어옴다.
	GetDDLdataFromController();

	CString str;
	//X axis
	str = PISTAGE_DDL_FILE_PATH + CString(_T("\\")) + m_pstrDdlfileX[m_nStrokeNo];
	filesave.open(str.GetBuffer(0));
	for (int i = 0; i < m_nNumberOfValues; i++)
		filesave << m_pdValueDdlArrayX[i] << "\n";
	filesave << endl;
	filesave.close();

	//Y axis
	str = PISTAGE_DDL_FILE_PATH + CString(_T("\\")) + m_pstrDdlfileY[m_nStrokeNo];
	filesave.open(str.GetBuffer(0));
	for (int i = 0; i < m_nNumberOfValues; i++)
		filesave << m_pdValueDdlArrayY[i] << "\n";
	filesave << endl;
	filesave.close();
	//////////////////////////////////////////////////

	ClearDDLTable();

	TransferDDLtoController();
}

void CPIE712Ctrl::RecordScanProfile()
{
	if (m_bConnect == FALSE)
		return;

	MSG msg;
	double dZvalue = 0.0;

	ClearDDLTable();
	GetPosAxesData();
	dZvalue = m_dPIStage_GetPos[Z_AXIS];
	Move_XY_Origin_Position();
	SetWaveGeneratorOffset();

	// Make Wave
	WaveGeneration();

	LoadDDLdatatoController();

	////////////////////////////////////////
	// define the data recorder channels. //
	////////////////////////////////////////
	SetDataRcorderConfig();

	// Set the number of cycles for the wave generator output
	SetNumberOfCycleForWaveGeneration(RECORD_SCAN_REP_NUM);

	// Start Wave Generation
	RunWaveGenerator(m_scan_mode[0], m_scan_mode[1]);

	m_pbValueArray[m_nScanAxis] = TRUE;
	m_pbValueArray[m_nStepAxis] = TRUE;
	m_bScanStop = FALSE;
	while (m_pbValueArray[m_nScanAxis] == TRUE)
	{
		if (!PI_IsGeneratorRunning(m_nPIStage_ID, m_iWaveTableIds, m_pbValueArray, DDL_AXIS_NUMBER))
		{
			m_nErrorCode = PI_GetError(m_nPIStage_ID);
			PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
			TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
			DisconnectComm();
		}

		if (m_bScanStop == TRUE)
		{
			if (!PI_STP(m_nPIStage_ID))
			{
				m_nErrorCode = PI_GetError(m_nPIStage_ID);
				PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
				TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
				DisconnectComm();
			}
			m_pbValueArray[m_nScanAxis] = FALSE;
			m_pbValueArray[m_nStepAxis] = FALSE;
		}
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	int j = 0, k = 0;
	GetPosAxesData();
	int steppos = int(m_dPIStage_GetPos[m_nStepAxis]);
	for (j = 0; j < steppos; j++)
	{
		for (k = 0; k < AXIS_NUMBER; k++)
			m_dPIStage_MovePos[k] = 0;
		m_dPIStage_MovePos[m_nStepAxis] = -1;
		PI_Move_Relative();
		Sleep(10);
	}

	///////////////////////////////////
	// start reading asynchronously. //
	///////////////////////////////////
	SaveRecordData();
	m_dPIStage_MovePos[X_AXIS] = X_INITIAL_POS_UM;
	m_dPIStage_MovePos[Y_AXIS] = Y_INITIAL_POS_UM;
	m_dPIStage_MovePos[Z_AXIS] = dZvalue;
	m_dPIStage_MovePos[TIP_AXIS] = m_dInitialTx_urad;
	m_dPIStage_MovePos[TILT_AXIS] = m_dInitialTy_urad;
	PI_Move_Absolute();
	SetWaveGeneratorOffset();
	ClearDDLTable();
}

void CPIE712Ctrl::SaveRecordData()
{
	if (m_bConnect == FALSE)
		return;

	double* dDataTable;
	char szHeader[301];
	int iNReadChannels = RECORD_NUM;
	//int iNReadValues = SAVE_NUM;
	int iNReadValues = m_piRecordDataNumber[m_nStrokeNo];	
	int iIndex = -1;
	int iOldIndex;
	int i;
	ofstream filesave;

	//////////////////////////////
	// Read data record tables. //
	//////////////////////////////
	m_iDataRecorderChannelIds[0] = 1;
	m_iDataRecorderChannelIds[1] = 2;
	m_iDataRecorderChannelIds[2] = 3;
	m_iDataRecorderChannelIds[3] = 4;
	m_iDataRecorderChannelIds[4] = 5;
	m_iDataRecorderChannelIds[5] = 6;
	if (!PI_qDRR(m_nPIStage_ID, m_iDataRecorderChannelIds, iNReadChannels, 1, iNReadValues, &dDataTable, szHeader, 300))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_qDRR(int ID, const int* piRecTableIdsArray, int iNumberOfRecTables, int iOffsetOfFirstPointInRecordTable, int iNumberOfValues, double** pdValueArray, char* szGcsArrayHeader, int iGcsArrayHeaderMaxSize)
	//	Corresponding command : DRR ?
	//		Read data record tables.This function reads the data asynchronously, it will return as soon as the data header has been read and start a background process which reads in the data itself.See PI_GetAsyncBuffer() 
	//      and PI_GetAsyncBufferIndex().Detailed information about the data read in can be found in the header sent by the controller.See the GCS Array manual for details.
	//		It is possible to read the data while recording is still in progress.
	//		The data is stored on the controller only until a new recording is done or the controller is powered down.
	//		For more information see “Data Recorder” in the controller User Manual.
	//	Arguments :
	//		ID ID of controller
	//		piRecTableIdArray IDs of data record tables
	//		iNumberOfRecTables number of record tables to read
	//		iOffsetOfFirstPointInRecordTable index of first value to be read(starts with index 1)
	//		iNumberOfValues number of values to read
	//		pdValarray pointer to internal array to store the data; data from all tables read will be placed in the same array with the values interspersed; the DLL will allocate enough memory to store all data, 
	//      call PI_GetAsyncBufferIndex() to find out how many data points have already been transferred
	//		szGcsArrayHeader buffer to store the GCS array header
	//		iGcsArrayHeaderMaxSize size of the buffer to store the GCS Array header, must be given to prevent buffer overflow
	//	Returns :
	//		TRUE if successful, FALSE otherwise

	/////////////////////////////////////////////////////////////
	// wait until the read pointer does not increase any more. //
	/////////////////////////////////////////////////////////////
	MSG msg;
	do
	{	// wait until the read pointer does not increase any more
		iOldIndex = iIndex;
		iIndex = PI_GetAsyncBufferIndex(m_nPIStage_ID);
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		//ProcessMessages();
		Sleep(100);
		//WaitSec(1);
	} while (iOldIndex < iIndex);

	/////////////////////////////////////
	// read the values from the array. //
	/////////////////////////////////////

	CTime time;
	time = CTime::GetCurrentTime();
	CString str;
	str.Format(_T("%s\\%s_%d%d%d_%d;%d;%d.txt"), PISTAGE_DDL_FILE_PATH, m_pstrScamData[m_nStrokeNo], time.GetYear(), time.GetMonth(), time.GetDay(), time.GetHour(), time.GetMinute(), time.GetSecond());
	SaveLogFile("ddl_test", (LPSTR)(LPCTSTR)str);

	filesave.open(str.GetBuffer(0));
	for (iIndex = 0; iIndex < (iOldIndex / iNReadChannels); iIndex++)
	{
		for (i = 0; i < iNReadChannels; i++)
		{
			filesave << dDataTable[(iIndex * iNReadChannels) + i] << "\t";
		}
		filesave << "\n";
		//str.Format(_T("%d"), iIndex);
		//SaveLogFile("ddl_test", (LPSTR)(LPCTSTR)str);
	}
	filesave << endl;
	filesave.close();
}

void CPIE712Ctrl::Get_Stage_Axes()
{
	if (m_bConnect == FALSE)
		return;
	/////////////////////////////////////////////////
	// Get the identifiers for all configured axes //
	/////////////////////////////////////////////////
	if (!PI_qSAI(m_nPIStage_ID, m_chPIStage_Axis, 20))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
}

void CPIE712Ctrl::GetPosAxesData()
{
	if (m_bConnect == FALSE)
		return;
	///////////////////////////////////////////
	// Get the current positions of szAxes   //
	///////////////////////////////////////////
	if (!PI_qPOS(m_nPIStage_ID, m_chPIStage_Axis, m_dPIStage_GetPos))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
}

void CPIE712Ctrl::ClearDDLTable()
{
	if (m_bConnect == FALSE)
		return;
	////////////////////////////////
	// clears the given DDL table //
	////////////////////////////////
	if (!PI_DTC(m_nPIStage_ID, m_piDdlTableIdsArray, AXIS_NUMBER))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_DTC(int ID, const int piDdlTableIdsArray, int iArraySize)
	//	Corresponding command : DTC
	//		Dynamic Digital Linearization(DDL) table clear : clears the given DDL table.
	//		PI_DTC() also stops a running DDL initialization process.
	//	Arguments :
	//		ID ID of controller
	//		iDdlTableIdIdsArray array with the IDs of the data tables which are to be cleared.
	//		iArraySize the size of the array iDdlTableIdsArray.
	//	Returns :
	//		TRUE if no error, FALSE otherwise(see p. 7)
}

void CPIE712Ctrl::SetWaveGeneratorOffset()
{
	if (m_bConnect == FALSE)
		return;
	//////////////////////////////////////////////////////
	// Sets an offset to the output of a wave generator //
	//////////////////////////////////////////////////////
	if (!PI_WOS(m_nPIStage_ID, m_piWaveGeneratorIdsArray, m_pdValueArray, AXIS_NUMBER))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_WOS(int ID, const int* piWaveGeneratorIdsArray, consst double* pdValueArray, int iArraySize)
	//	Corresponding command : WOS
	//		Sets an offset to the output of a wave generator.The current wave generator output is then created by adding the offset value to the current wave value :
	//		Generator Output = Offset + Current Wave Value
	//		Do not confuse the output - offset value set with PI_WOS() with the offset settings done during the waveform creation with the PI_WAV() functions.While the PI_WAV() offset belongs to only one waveform, the PI_WOS() offset is added to all waveforms which are output by the given wave generator.
	//		Deleting wave table content with PI_WCL() has no effect on the offset settings for the wave generator output.
	//	Arguments :
	//		ID ID of controller
	//		piWaveGeneratorIdsArray array with wave generators.
	//		pdValueArray			array with the offsets of the wave generators.
	//		iArraySize				the size of the arrays piWaveGeneratorIdsArray and pdValueArray.
	//	Returns :
	//		TRUE if no error, FALSE otherwise(see p. 7)
}

void CPIE712Ctrl::GetDDLdataFromController()
{
	if (m_bConnect == FALSE)
		return;
	//////////////////////////////////////////////////////
	// Get Dynamic Digital Linearizations Table Length  //
	//////////////////////////////////////////////////////
	if (!PI_qDTL(m_nPIStage_ID, m_piDdlTableIdsArray, m_piValueArray, DDL_AXIS_NUMBER))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_ qDTL(int ID, const int* piDdlTableIdsArray, int* piValueArray, int iArraySize)
	//	Corresponding command : DTL ?
	//		Get Dynamic Digital Linearizations Table Length.
	//		The table length should be read before reading the table data by PI_qDDL.
	//	Arguments :
	//		ID ID of controller
	//		piDdlTableIdsArray array of the DDL table IDs
	//		piValueArray array to receive the DDL table size
	//		iArraySize the size of the arrays piDdlTableIdsArray and piValueArray
	//	Returns :
	//	TRUE if no error, FALSE otherwise(see p. 7)

	///////////////////////////////////////////////////////////////
	// Get the DDL data from a DDL data table on the controller  //
	///////////////////////////////////////////////////////////////
	m_nNumberOfValues = m_piValueArray[m_nScanAxis];
	if (!PI_qDDL_SYNC(m_nPIStage_ID, m_nScanAxis+1, m_nOffsetOfFirstPointInDdlTable, m_nNumberOfValues, m_pdValueDdlArrayX)) //Get the x axis DDL data
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	if (!PI_qDDL_SYNC(m_nPIStage_ID, m_nStepAxis+1, m_nOffsetOfFirstPointInDdlTable, m_nNumberOfValues, m_pdValueDdlArrayY))	//Get the y axis DDL data
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_qDDL_SYNC(int ID, int iDdlTableId, int iOffsetOfFirstPointInDdlTable, int iNumberOfValues, double* pdValueArray)
	//	Corresponding command : DDL ?
	//		Get the dynamic digital linearization feature data from a DDL data table from the controller.For large N values, communication timeout must be set long enough, otherwise a communication error may occur.
	//	Arguments :
	//		ID ID of controller
	//		iDdlTableId ID of the DDL data table.
	//		iOffsetOfFirstPointInDdlTable index in the DDL table of first value to be read, the first value in the DDL table has index 1
	//		iNumberOfValues number of values to be read
	//		pdValueArray Array to receive the values.Caller is responsible for providing enough space for iNumberOfValues doubles
	//	Returns :
	//	TRUE if no error, FALSE otherwise(see p. 7)
}

void CPIE712Ctrl::WaveTableSelection()
{
	if (m_bConnect == FALSE)
		return;
	///////////////////////////////////////////////
	// Select the desired wave generator.        //
	// connects a wave table to a wave generator //
	///////////////////////////////////////////////
	if (!PI_WSL(m_nPIStage_ID, m_iWaveGenerator, m_iWaveTableIds, DDL_AXIS_NUMBER))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_WSL(int ID, const int* piWaveGeneratorIdsArray, const int* piWaveTableIdsArray, int iArraySize)
	//	Corresponding command : WSL
	//		Wave table selection : connects a wave table to a wave generator or disconnects the selected generator from any wave table.
	//		Two or more generators can be connected to the same wave table, but a generator cannot be connected to more than one wave table.
	//		Deleting wave table content with PI_WCL has no effect on the PI_WSL settings.
	//		As long as a wave generator is running, it is not possible to change the connected wave table.
	//	Arguments :
	//		ID ID of controller
	//		piWaveGeneratorIdsArray array with wave generators.
	//		piWaveTableIdsArray array with the wave table ID. "0" disconnects the selected generator from any wave table.
	//		iArraySize the size of the arrays piWaveGeneratorIdsArray and piWaveTableIdsArray.
	//	Returns :
	//		TRUE if no error, FALSE otherwise(see p. 7)
}

void CPIE712Ctrl::SetNumberOfCycleForWaveGeneration(int nRepeatNo)
{
	if (m_bConnect == FALSE)
		return;
	////////////////////////////////////////////////////////////
	// Set the number of cycles for the wave generator output //
	////////////////////////////////////////////////////////////
	m_piNumberOfCyclesArray[0] = nRepeatNo;
	m_piNumberOfCyclesArray[1] = nRepeatNo;
	if (!PI_WGC(m_nPIStage_ID, m_iWaveGenerator, m_piNumberOfCyclesArray, DDL_AXIS_NUMBER)) //ddl 수행 횟수 설정
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_WGC(int ID, const int* piWaveGeneratorIdsArray, const int* piNumberOfCyclesArray, int iArraySize)
	//	Corresponding command : WGC
	//		Set the number of cycles for the wave generator output(which is started with PI_WGO()).
	//	Arguments :
	//		ID ID of controller
	//		piWaveGeneratorIdsArray array with wave generators
	//		piNumberOfCyclesArray array with number of cycles for each wave generator in piWaveGeneratorIdsArray
	//		iArraySize the size of the arrays piWaveGeneratorIdsArray and piNumberOfCyclesArray
	//	Returns :
	//		TRUE if successful, FALSE otherwise
}

void CPIE712Ctrl::RunWaveGenerator(int nModeX, int nModeY)
{
	if (m_bConnect == FALSE)
		return;
	/////////////////////////////////////////////////////////////////////
	// Start and stop the specified wave generator with the given mode //
	/////////////////////////////////////////////////////////////////////
	m_iStatMode[0] = nModeX;	// scanning axis
	m_iStatMode[1] = nModeY;	// step axis
	//0: wave generator output is stopped. You can also use PI_STP() to stop the wave generator output, but PI_qWGO() will then still report the last commanded start mode.
	//bit 0 = 0x1 (hex format) or 1 (decimal format): start wave generator output immediately, synchronized by servo cycle
	//bit 1 = 0x2 (hex format) or 2 (decimal format): start wave generator output triggered by external signal, synchronized by servo cycle. To provide the external signal, the digital input lines IN1 or IN2 can be used (see "(Digital)I/O" socket). If IN1 is used: The wave generator output starts with the first rising edge which is detected on this input line.If IN2 is used: The generator output starts with the first rising edge which is detected on this input line, and it will be stopped when a falling edge is detected on this line. With the next rising edge, the generator output will continue at the waveform point where it was stopped. Starting and stopping the wave generator this way can be repeated indefinitely.
	//bit 6 = 0x40 (hex format) or 64 (decimal format): the Dynamic Digital Linearization (DDL) feature is used and reinitialized.
	//bit 7 = 0x80 (hex format) or 128 (decimal format): the DDL feature is used.
	//bit 8 = 0x100 (hex format) or 256 (decimal format): wave generator started at the endpoint of the last cycle; start option. The second and all subsequent output cycles each start at the endpoint of the preceding cycle which makes this start option appropriate to scanning applications. The final position is the sum of the endpoint of the last output cycle and any offset defined with PI_WAV for the waveform.

	if (!PI_WGO(m_nPIStage_ID, m_iWaveGenerator, m_iStatMode, DDL_AXIS_NUMBER))	//ddl 시작
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_WGO(int ID, const int* piWaveGeneratorIdsArray, condt int* piStartModArray, int iArraySize)
	//	Corresponding command : WGO
	//		Start and stop the specified wave generator with the given mode.Depending on the controller, starts also data recording.
	//	Arguments :
	//		ID ID of controller
	//		piWaveGeneratorIdsArray array with wave generators.
	//		piStartModArray array with start modes for each wave generator in piWaveGeneratorIdsArray(hex format, optional decimal format)
	//		iArraySize the size of the arrays piWaveGeneratorIdsArray and piStartModArray
	//	Returns :
	//		TRUE if no error, FALSE otherwise(see p. 7)
}

void CPIE712Ctrl::TransferDDLtoController()
{
	if (m_bConnect == FALSE)
		return;
	/////////////////////////////////////////////////////////
	// Transfer DDL data to a DDL data table on controller //
	/////////////////////////////////////////////////////////
	if (!PI_DDL(m_nPIStage_ID, 2, m_nOffsetOfFirstPointInDdlTable, m_nNumberOfValues, m_pdValueDdlArrayX))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	if (!PI_DDL(m_nPIStage_ID, 1, m_nOffsetOfFirstPointInDdlTable, m_nNumberOfValues, m_pdValueDdlArrayY))	//sm.choi : Uni-direction 추가 위해 Y axis DDL 적용 코드
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_DDL(int ID, int iDdlTableId, int iOffsetOfFirstPointInDdlTable, int iNumberOfValues, double* pdValueArray)
	//		Corresponding command : DDL
	//		Transfer dynamic digital linearization feature data to a DDL data table on the controller.
	//	Arguments :
	//		ID ID of controller
	//		iDdlTableId number of the DDL data table to use.
	//		iOffsetOfFirstPointInDdlTable index of first value to be transferred, (the first value in the DDL table has index 1)
	//		iNumberOfValues number of values to be transferred
	//		pdValueArray Array with the values for the DDL table(can have been filled with PI_qDDL()).
	//	Returns :
	//		TRUE if no error, FALSE otherwise(see p. 7)
}

void CPIE712Ctrl::LoadDDLdatatoController()
{
	if (m_bConnect == FALSE)
		return;

	if (!PI_DDL(m_nPIStage_ID, 2, m_nOffsetOfFirstPointInDdlTable, m_piDdlPointNumber[m_nStrokeNo], m_dpdDdlDataX[m_nStrokeNo]))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}

	if (!PI_DDL(m_nPIStage_ID, 1, m_nOffsetOfFirstPointInDdlTable, m_piDdlPointNumber[m_nStrokeNo], m_dpdDdlDataY[m_nStrokeNo]))		//sm.choi : Uni-direction 추가 위해 Y axis DDL 적용 코드, Y scan을 위해서도 DDL 추가 필요.
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
}

void CPIE712Ctrl::SetDataRcorderConfig()
{
	if (m_bConnect == FALSE)
		return;
	////////////////////////////////////////
	// Set Data Recorder Configuration    //
	////////////////////////////////////////

	// select the desired record channels to change.
	m_iDataRecorderChannelIds[0] = 1;
	m_iDataRecorderChannelIds[1] = 2;
	m_iDataRecorderChannelIds[2] = 3;
	m_iDataRecorderChannelIds[3] = 4;
	m_iDataRecorderChannelIds[4] = 5;
	m_iDataRecorderChannelIds[5] = 6;

	// select the corresponding record source id's.
	char szDataRecorderChannelSources[] = "1 1 1 2 2 2";	//x축 3개 data, y축 3개 data(1 = Target Position of axis, 2 = Current Position of axis, 3 = Position Error of axis

	// select the corresponding record mode.
	int iDataRecorderOptions[] = { 1, 2, 3, 1, 2, 3 };
	//1 = Target Position of axis(i.e.target value in closed - loop operation), corresponds to the MOV ? response
	//2 = Current Position of axis, corresponds to the POS ? response
	//3 = Position Error of axis

	// Call the data recorder configuration command
	if (!PI_DRC(m_nPIStage_ID, m_iDataRecorderChannelIds, szDataRecorderChannelSources, iDataRecorderOptions))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("SAI?: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
	//BOOL PI_DRC(int ID, const int* piRecordTableIdsArray, const char* szRecordSourceIds, const int* piRecordOptionsArray)
	//	Corresponding command : DRC
	//		Set data recorder configuration : determines the data source(szRecordSourceIdsArray) and the kind of data(piRecordOptionsArray) used for the given data recorder table.
	//	Arguments :
	//		ID ID of controller
	//		piRecordTableldsArray ID of the record table
	//		szRecordSourceIds ID of the record source, for example axis number or channel number.The value of this argument depends on the corresponding record option.
	//		piRecordOptionsArray record option, i.e.the kind of data to be recorded
	//	Returns :
	//		TRUE if no error, FALSE otherwise(see p. 7)
}


void CPIE712Ctrl::PI_Move_Absolute()
{
	if (m_bConnect == FALSE)
		return;
	/////////////////////////////////////////////////////
	// Move all axes in szAxes to their home positions //
	/////////////////////////////////////////////////////	
	if (!PI_MOV(m_nPIStage_ID, m_chPIStage_Axis, m_dPIStage_MovePos))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("PI_MOV: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
}

void CPIE712Ctrl::PI_Move_Relative()
{
	if (m_bConnect == FALSE)
		return;
	/////////////////////////////////////////////////////
	// Move szAxes relative to current target position //
	/////////////////////////////////////////////////////	
	if (!PI_MVR(m_nPIStage_ID, m_chPIStage_Axis, m_dPIStage_MovePos))
	{
		m_nErrorCode = PI_GetError(m_nPIStage_ID);
		PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		TRACE(_T("PI_MVR: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}
}

void CPIE712Ctrl::MoveZRelative_SlowInterlock(double pos_um, int direction)
{
	if (m_bConnect == FALSE)
		return;

	double curpos;
	double absolute_pos = 0.0, increase_pos1, increase_pos2;

	GetPosAxesData();
	curpos = m_dPIStage_GetPos[Z_AXIS];

	if (direction == PLUS_DIRECTION)
	{
		increase_pos1 = pos_um;
		absolute_pos = increase_pos1 + curpos;

		if (absolute_pos >= Z_MINUSLIMIT && absolute_pos <= m_dZUpperLimit)
		{
			while (1)
			{
				if (increase_pos1 == 0.0)
					break;
				else if (increase_pos1 > Z_AXIS_STEP)
				{
					increase_pos1 = increase_pos1 - double(Z_AXIS_STEP);
					increase_pos2 = double(Z_AXIS_STEP);
				}
				else
				{
					increase_pos2 = increase_pos1;
					increase_pos1 = 0.0;
				}

				for (int i = 0; i < AXIS_NUMBER; i++)
					m_dPIStage_MovePos[i] = 0;
				m_dPIStage_MovePos[Z_AXIS] = increase_pos2;
				PI_Move_Relative();
				GetPosAxesData();
				Sleep(100);
			}
		}
	}
	else if (direction == MINUS_DIRECTION)
	{
		increase_pos1 = pos_um;
		absolute_pos = -increase_pos1 + curpos;

		if (absolute_pos >= Z_MINUSLIMIT && absolute_pos <= m_dZUpperLimit)
		{
			while (1)
			{
				if (increase_pos1 == 0.0)
					break;
				else if (increase_pos1 > Z_AXIS_STEP)
				{
					increase_pos1 = increase_pos1 - double(Z_AXIS_STEP);
					increase_pos2 = double(Z_AXIS_STEP);
				}
				else
				{
					increase_pos2 = increase_pos1;
					increase_pos1 = 0.0;
				}

				for (int i = 0; i < AXIS_NUMBER; i++)
					m_dPIStage_MovePos[i] = 0;
				m_dPIStage_MovePos[Z_AXIS] = -increase_pos2;
				PI_Move_Relative();
				GetPosAxesData();
				Sleep(100);
			}
		}
	}
	else
	{ }
}

void CPIE712Ctrl::MoveZAbsolute_SlowInterlock(double z_pos)
{
	if (m_bConnect == FALSE)
		return;

	double curpos;
	double absolute_pos = 0.0, increase_pos1, increase_pos2;

	GetPosAxesData();
	curpos = m_dPIStage_GetPos[Z_AXIS];

	if (z_pos > curpos)
	{
		increase_pos1 = z_pos - curpos;
		absolute_pos = z_pos;

		if (absolute_pos >= Z_MINUSLIMIT && absolute_pos <= m_dZUpperLimit)
		{
			while (1)
			{
				if (increase_pos1 == 0.0)
					break;
				else if (increase_pos1 > Z_AXIS_STEP)
				{
					increase_pos1 = increase_pos1 - double(Z_AXIS_STEP);
					increase_pos2 = double(Z_AXIS_STEP);
				}
				else
				{
					increase_pos2 = increase_pos1;
					increase_pos1 = 0.0;
				}

				for (int i = 0; i < AXIS_NUMBER; i++)
					m_dPIStage_MovePos[i] = 0;
				m_dPIStage_MovePos[Z_AXIS] = increase_pos2;
				PI_Move_Relative();
				GetPosAxesData();
				Sleep(100);
			}
		}
	}
	else
	{
		increase_pos1 = curpos - z_pos;
		absolute_pos = z_pos;

		if (absolute_pos >= Z_MINUSLIMIT && absolute_pos <= m_dZUpperLimit)
		{
			while (1)
			{
				if (increase_pos1 == 0.0)
					break;
				else if (increase_pos1 > Z_AXIS_STEP)
				{
					increase_pos1 = increase_pos1 - double(Z_AXIS_STEP);
					increase_pos2 = double(Z_AXIS_STEP);
				}
				else
				{
					increase_pos2 = increase_pos1;
					increase_pos1 = 0.0;
				}

				for (int i = 0; i < AXIS_NUMBER; i++)
					m_dPIStage_MovePos[i] = 0;
				m_dPIStage_MovePos[Z_AXIS] = -increase_pos2;
				PI_Move_Relative();
				GetPosAxesData();
				Sleep(100);
			}
		}
	}
}


int CPIE712Ctrl::PI_Stage_Connected_Check()
{
	if (PI_IsConnected(m_nPIStage_ID))
		return TRUE;
	else
		return FALSE;
}