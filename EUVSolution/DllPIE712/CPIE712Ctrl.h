/**
 * PI E712 Controller(Scan Stage) Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once
 /* Stroke
  x : 0~12 um
  y : 0~12 um
  z : -500~500 um
  rotX: -1000~1000 urad
  rotY: -1000~1000 urad
  */

  //통신 방식 정의
#define ETHERNET	0
#define SERIAL		1
#define SIMULATOR	2

#define ERROR_STRING_SIZE			1024		

#define X_INITIAL_POS_UM			0.5			//항상 0.5um 위치에서 Scan 하자
#define Y_INITIAL_POS_UM			0.5			//항상 0.5um 위치에서 Scan 하자
#define Z_INITIAL_POS_UM			-300			//항상 0 위치에서 Coarse Stage 움직이자
//#define TIP_COMPENSATION_ANGLE		-225		//마스크 평탄도 Tx(Tip) 보상(urad) -> 추후 Config로 빼자
//#define TILT_COMPENSATION_ANGLE		195			//마스크 평탄도 Ty(Tilt) 보상(urad) -> 추후 Config로 빼자

#define AXIS_NUMBER					5			//축 수
#define X_AXIS						1			//x축, Scan Axis
#define Y_AXIS						0			//y축, Step Axis
#define Z_AXIS						2			//z축
#define TIP_AXIS					4			//Tx축
#define TILT_AXIS					3			//Ty축

#define X_MINUSLIMIT				0
#define X_PLUSLIMIT					12
#define Y_MINUSLIMIT				0
#define Y_PLUSLIMIT					12
#define Z_MINUSLIMIT				-500
#define Z_PLUSLIMIT					500
#define TIP_MINUSLIMIT				-1000
#define TIP_PLUSLIMIT				1000
#define TILT_MINUSLIMIT				-1000
#define TILT_PLUSLIMIT				1000

#define PLUS_DIRECTION				1
#define MINUS_DIRECTION				0

#define DDL_AXIS_NUMBER				2		//ddl 적용되는 축 수, x,y 2개
//#define DDL_LEARNING_REPEAT_NUM		60		//ddl 학습시 반복 수행하는 횟수	//30->40으로 늘림.Tracking Error 더 좋아질지도...20200723 //50으로 늘리면 3um_10nm ddl 생성 안됨. overflow문제인듯.. //5Khz는 5배 더 많이 해보자 30 -> 150 까지 올려보자
#define RECORD_SCAN_REP_NUM			15		//data record를 위해 scan하는 회수, FOV 전체를 scan하면 data가 너무많으므로. 5Khz에서는 5배 더 많이 Scan 하자 5->25
//#define SAVE_NUM					10000	//save할 데이터 길이	//5Khz는 5배 더 많이 저장? 20000 -> 100000 -> 10000(scan 개수가 몇개 안됨)
#define RECORD_NUM					6		//record channel 6개(x reference,x real,x tracking error,y reference,y real,y traking error)

#define Z_AXIS_STEP					10		//Z axis을 조금씩 올리기위해

#define SCAN_MODE_IMAGING			0
#define SCAN_MODE_DDL_GERNERATION	1
#define SCAN_MODE_RECORD			2

#define DDL_MEM_NUM			20000

#define SPEED_UPDOWN_POINTNO	10		//number of speed up and down(가감속): 10 points * 50us(20Khz) = 500 us -> z axis 및 tracking error 문제로 40 points로 변경

#define PISTAGE_DDL_FILE_PATH					_T("C:\\EUVSolution\\Config\\PIE712_ddl")				// DDL 파일 경로




#define DDL_LEARNING_NUMBER			46

#define ONEMSEC_SERVO_CALCULATION_POINTNO	16.667

#define STEP_SIZE_200NM			200	
#define STEP_SIZE_160NM			160	
#define STEP_SIZE_100NM			100	
#define STEP_SIZE_80NM			80	
#define STEP_SIZE_40NM			40  
#define STEP_SIZE_20NM			20	
#define STEP_SIZE_10NM			10	
#define STEP_SIZE_05NM			 5	

#define SCAN_SIZE_10UM			10000		
#define SCAN_SIZE_07UM			 7000		
#define SCAN_SIZE_05UM			 5000		
#define SCAN_SIZE_03UM			 3000		
#define SCAN_SIZE_02UM			 2000		
#define SCAN_SIZE_01_9UM		 1900		
#define SCAN_SIZE_01_5UM		 1500		
#define SCAN_SIZE_01UM			 1000		

#define SCAN_ADD_SIZE_1000		  1000		
#define SCAN_ADD_SIZE_600		  600		
#define SCAN_ADD_SIZE_400		  400		
#define SCAN_ADD_SIZE_200		  200		


//1Khz Uni-direction
#define PROFILE_UNI_1UM_05NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_600)/STEP_SIZE_05NM 		//scan:fov 1000(nm) + acel/dcel 600(nm) = 1600(nm), 1ms 당 05nm이면 1600(nm) = 320ms
#define PROFILE_UNI_1UM_10NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_600)/STEP_SIZE_10NM 		//scan:fov 1000(nm) + acel/dcel 600(nm) = 1600(nm), 1ms 당 10nm이면 1600(nm) = 160ms
#define PROFILE_UNI_1UM_20NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_600)/STEP_SIZE_20NM 		//scan:fov 1000(nm) + acel/dcel 600(nm) = 1600(nm), 1ms 당 20nm이면 1600(nm) = 80ms
#define PROFILE_UNI_1UM_40NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_600)/STEP_SIZE_40NM 		//scan:fov 1000(nm) + acel/dcel 600(nm) = 1600(nm), 1ms 당 40nm이면 1600(nm) = 40ms
#define PROFILE_UNI_1UM_80NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_600)/STEP_SIZE_80NM 		//scan:fov 1000(nm) + acel/dcel 600(nm) = 1600(nm), 1ms 당 80nm이면 1600(nm) = 20ms
#define PROFILE_UNI_1UM_160NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_600)/STEP_SIZE_160NM 		//scan:fov 1000(nm) + acel/dcel 600(nm) = 1600(nm), 1ms 당 160nm이면 1600(nm) = 10ms

#define PROFILE_UNI_2UM_05NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)/STEP_SIZE_05NM			//scan:fov 2000(nm) + acel/dcel 600(nm) = 2600(nm), 1ms 당 05nm이면 2600(nm) = 520ms
#define PROFILE_UNI_2UM_10NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)/STEP_SIZE_10NM			//scan:fov 2000(nm) + acel/dcel 600(nm) = 2600(nm), 1ms 당 10nm이면 2600(nm) = 260ms
#define PROFILE_UNI_2UM_20NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)/STEP_SIZE_20NM 		//scan:fov 2000(nm) + acel/dcel 600(nm) = 2600(nm), 1ms 당 20nm이면 2600(nm) = 130ms
#define PROFILE_UNI_2UM_40NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)/STEP_SIZE_40NM 		//scan:fov 2000(nm) + acel/dcel 600(nm) = 2600(nm), 1ms 당 40nm이면 2600(nm) = 65ms
#define PROFILE_UNI_2UM_80NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)/STEP_SIZE_80NM 		//scan:fov 2000(nm) + acel/dcel 600(nm) = 2600(nm), 1ms 당 80nm이면 2600(nm) = 32.5ms
#define PROFILE_UNI_2UM_160NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)/STEP_SIZE_160NM 		//scan:fov 2000(nm) + acel/dcel 600(nm) = 2600(nm), 1ms 당 160nm이면 2600(nm) = 16.25ms

#define PROFILE_UNI_3UM_05NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)/STEP_SIZE_05NM			//scan:fov 3000(nm) + acel/dcel 600(nm) = 3600(nm), 1ms 당 05nm이면 3600(nm) = 720ms
#define PROFILE_UNI_3UM_10NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)/STEP_SIZE_10NM			//scan:fov 3000(nm) + acel/dcel 600(nm) = 3600(nm), 1ms 당 10nm이면 3600(nm) = 360ms
#define PROFILE_UNI_3UM_20NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)/STEP_SIZE_20NM			//scan:fov 3000(nm) + acel/dcel 600(nm) = 3600(nm), 1ms 당 20nm이면 3600(nm) = 180ms
#define PROFILE_UNI_3UM_40NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)/STEP_SIZE_40NM			//scan:fov 3000(nm) + acel/dcel 600(nm) = 3600(nm), 1ms 당 40nm이면 3600(nm) = 90ms
#define PROFILE_UNI_3UM_80NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)/STEP_SIZE_80NM			//scan:fov 3000(nm) + acel/dcel 600(nm) = 3600(nm), 1ms 당 80nm이면 3600(nm) = 45ms
#define PROFILE_UNI_3UM_160NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)/STEP_SIZE_160NM		//scan:fov 3000(nm) + acel/dcel 600(nm) = 3600(nm), 1ms 당 160nm이면 3600(nm) = 22.5ms

#define PROFILE_UNI_5UM_05NM_MSECTIME		(SCAN_SIZE_05UM + SCAN_ADD_SIZE_600)/STEP_SIZE_05NM			//scan:fov 5000(nm) + acel/dcel 600(nm) = 5600(nm), 1ms 당 05nm이면 5600(nm) = 1120ms
#define PROFILE_UNI_5UM_10NM_MSECTIME		(SCAN_SIZE_05UM + SCAN_ADD_SIZE_600)/STEP_SIZE_10NM			//scan:fov 5000(nm) + acel/dcel 600(nm) = 5600(nm), 1ms 당 10nm이면 5600(nm) = 560ms
#define PROFILE_UNI_5UM_20NM_MSECTIME		(SCAN_SIZE_05UM + SCAN_ADD_SIZE_600)/STEP_SIZE_20NM			//scan:fov 5000(nm) + acel/dcel 600(nm) = 5600(nm), 1ms 당 20nm이면 5600(nm) = 280ms
#define PROFILE_UNI_5UM_40NM_MSECTIME		(SCAN_SIZE_05UM + SCAN_ADD_SIZE_600)/STEP_SIZE_40NM			//scan:fov 5000(nm) + acel/dcel 600(nm) = 5600(nm), 1ms 당 40nm이면 5600(nm) = 140ms
#define PROFILE_UNI_5UM_80NM_MSECTIME		(SCAN_SIZE_05UM + SCAN_ADD_SIZE_600)/STEP_SIZE_80NM			//scan:fov 5000(nm) + acel/dcel 600(nm) = 5600(nm), 1ms 당 80nm이면 5600(nm) = 70ms
#define PROFILE_UNI_5UM_160NM_MSECTIME		(SCAN_SIZE_05UM + SCAN_ADD_SIZE_600)/STEP_SIZE_160NM		//scan:fov 5000(nm) + acel/dcel 600(nm) = 5600(nm), 1ms 당 160nm이면 5600(nm) = 35ms

#define PROFILE_UNI_7UM_05NM_MSECTIME		(SCAN_SIZE_07UM + SCAN_ADD_SIZE_600)/STEP_SIZE_05NM			//scan:fov 7000(nm) + acel/dcel 600(nm) = 7600(nm), 1ms 당 05nm이면 7600(nm) = 1520ms
#define PROFILE_UNI_7UM_10NM_MSECTIME		(SCAN_SIZE_07UM + SCAN_ADD_SIZE_600)/STEP_SIZE_10NM			//scan:fov 7000(nm) + acel/dcel 600(nm) = 7600(nm), 1ms 당 10nm이면 7600(nm) = 760ms
#define PROFILE_UNI_7UM_20NM_MSECTIME		(SCAN_SIZE_07UM + SCAN_ADD_SIZE_600)/STEP_SIZE_20NM			//scan:fov 7000(nm) + acel/dcel 600(nm) = 7600(nm), 1ms 당 20nm이면 7600(nm) = 380ms
#define PROFILE_UNI_7UM_40NM_MSECTIME		(SCAN_SIZE_07UM + SCAN_ADD_SIZE_600)/STEP_SIZE_40NM			//scan:fov 7000(nm) + acel/dcel 600(nm) = 7600(nm), 1ms 당 40nm이면 7600(nm) = 190ms
#define PROFILE_UNI_7UM_80NM_MSECTIME		(SCAN_SIZE_07UM + SCAN_ADD_SIZE_600)/STEP_SIZE_80NM			//scan:fov 7000(nm) + acel/dcel 600(nm) = 7600(nm), 1ms 당 80nm이면 7600(nm) = 95ms
#define PROFILE_UNI_7UM_160NM_MSECTIME		(SCAN_SIZE_07UM + SCAN_ADD_SIZE_600)/STEP_SIZE_160NM		//scan:fov 7000(nm) + acel/dcel 600(nm) = 7600(nm), 1ms 당 160nm이면 7600(nm) = 47.5ms

#define PROFILE_UNI_10UM_05NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)/STEP_SIZE_05NM			//scan:fov 10000(nm) + acel/dcel 600(nm) = 10600(nm), 1ms 당 05nm이면 10600(nm) = 2120ms
#define PROFILE_UNI_10UM_10NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)/STEP_SIZE_10NM			//scan:fov 10000(nm) + acel/dcel 600(nm) = 10600(nm), 1ms 당 10nm이면 10600(nm) = 1060ms
#define PROFILE_UNI_10UM_20NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)/STEP_SIZE_20NM			//scan:fov 10000(nm) + acel/dcel 600(nm) = 10600(nm), 1ms 당 20nm이면 10600(nm) = 530ms
#define PROFILE_UNI_10UM_40NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)/STEP_SIZE_40NM			//scan:fov 10000(nm) + acel/dcel 600(nm) = 10600(nm), 1ms 당 40nm이면 10600(nm) = 265ms
#define PROFILE_UNI_10UM_80NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)/STEP_SIZE_80NM			//scan:fov 10000(nm) + acel/dcel 600(nm) = 10600(nm), 1ms 당 80nm이면 10600(nm) = 132.5ms
#define PROFILE_UNI_10UM_160NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)/STEP_SIZE_160NM		//scan:fov 10000(nm) + acel/dcel 600(nm) = 10600(nm), 1ms 당 160nm이면 10600(nm) = 66.25ms

#define PROFILE_UNI_1UM_05NM_POINTNO		PROFILE_UNI_1UM_05NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//320ms*16.667points=5333 points
#define PROFILE_UNI_1UM_10NM_POINTNO		PROFILE_UNI_1UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//160ms*16.667points=2666 points
#define PROFILE_UNI_1UM_20NM_POINTNO		PROFILE_UNI_1UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//80ms*16.667points=1333 points
#define PROFILE_UNI_1UM_40NM_POINTNO		PROFILE_UNI_1UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//40ms*16.667points=666 points
#define PROFILE_UNI_1UM_80NM_POINTNO		PROFILE_UNI_1UM_80NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//20ms*16.667points=333 points
#define PROFILE_UNI_1UM_160NM_POINTNO		PROFILE_UNI_1UM_160NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//10ms*16.667points=166 points

#define PROFILE_UNI_2UM_05NM_POINTNO		PROFILE_UNI_2UM_05NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//520ms*16.667points=8666 points
#define PROFILE_UNI_2UM_10NM_POINTNO		PROFILE_UNI_2UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//260ms*16.667points=4333 points
#define PROFILE_UNI_2UM_20NM_POINTNO		PROFILE_UNI_2UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//130ms*16.667points=2166 points
#define PROFILE_UNI_2UM_40NM_POINTNO		PROFILE_UNI_2UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//65ms*16.667points=1083 points
#define PROFILE_UNI_2UM_80NM_POINTNO		PROFILE_UNI_2UM_80NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//32.5ms*16.667points=541 points
#define PROFILE_UNI_2UM_160NM_POINTNO		PROFILE_UNI_2UM_160NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//16.25ms*16.667points=270 points

#define PROFILE_UNI_3UM_05NM_POINTNO		PROFILE_UNI_3UM_05NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//720ms*16.667points=12000 points
#define PROFILE_UNI_3UM_10NM_POINTNO		PROFILE_UNI_3UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//360ms*16.667points=6000 points
#define PROFILE_UNI_3UM_20NM_POINTNO		PROFILE_UNI_3UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//180ms*16.667points=3000 points
#define PROFILE_UNI_3UM_40NM_POINTNO		PROFILE_UNI_3UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//90ms*16.667points=1500 points
#define PROFILE_UNI_3UM_80NM_POINTNO		PROFILE_UNI_3UM_80NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//45ms*16.667points=750 points
#define PROFILE_UNI_3UM_160NM_POINTNO		PROFILE_UNI_3UM_160NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//22.5ms*16.667points=375 points

#define PROFILE_UNI_5UM_05NM_POINTNO		PROFILE_UNI_5UM_05NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//1120ms*16.667points=18667 points
#define PROFILE_UNI_5UM_10NM_POINTNO		PROFILE_UNI_5UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//560ms*16.667points=9333 points
#define PROFILE_UNI_5UM_20NM_POINTNO		PROFILE_UNI_5UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//280ms*16.667points=4666 points
#define PROFILE_UNI_5UM_40NM_POINTNO		PROFILE_UNI_5UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//140ms*16.667points=2333 points
#define PROFILE_UNI_5UM_80NM_POINTNO		PROFILE_UNI_5UM_80NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//70ms*16.667points=1166 points
#define PROFILE_UNI_5UM_160NM_POINTNO		PROFILE_UNI_5UM_160NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//35ms*16.667points=583 points

#define PROFILE_UNI_7UM_05NM_POINTNO		PROFILE_UNI_7UM_05NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//1520ms*16.667points=25333 points
#define PROFILE_UNI_7UM_10NM_POINTNO		PROFILE_UNI_7UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//760ms*16.667points=12666 points
#define PROFILE_UNI_7UM_20NM_POINTNO		PROFILE_UNI_7UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//380ms*16.667points=6333 points
#define PROFILE_UNI_7UM_40NM_POINTNO		PROFILE_UNI_7UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//190ms*16.667points=3166 points
#define PROFILE_UNI_7UM_80NM_POINTNO		PROFILE_UNI_7UM_80NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//95ms*16.667points=1583 points
#define PROFILE_UNI_7UM_160NM_POINTNO		PROFILE_UNI_7UM_160NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//47.5ms*16.667points=791 points

#define PROFILE_UNI_10UM_05NM_POINTNO		PROFILE_UNI_10UM_05NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//2120ms*16.667points=35334 points
#define PROFILE_UNI_10UM_10NM_POINTNO		PROFILE_UNI_10UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//1060ms*16.667points=17667 points
#define PROFILE_UNI_10UM_20NM_POINTNO		PROFILE_UNI_10UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//530ms*16.667points=8833 points
#define PROFILE_UNI_10UM_40NM_POINTNO		PROFILE_UNI_10UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//265ms*16.667points=4416 points
#define PROFILE_UNI_10UM_80NM_POINTNO		PROFILE_UNI_10UM_80NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//132.5ms*16.667points=2208 points
#define PROFILE_UNI_10UM_160NM_POINTNO		PROFILE_UNI_10UM_160NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//66.25ms*16.667points=1104 points

//5Khz Uni-direction
#define PROFILE_5KHZ_UNI_1UM_10NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_400)/(STEP_SIZE_10NM*5)				//scan:fov 1000(nm) + acel/dcel 400(nm) = 1400(nm), 1ms 당 50nm이면 1400(nm) = 28ms
#define PROFILE_5KHZ_UNI_1UM_20NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_400)/(STEP_SIZE_20NM*5) 			//scan:fov 1000(nm) + acel/dcel 400(nm) = 1400(nm), 1ms 당 100nm이면 1400(nm) = 14ms
#define PROFILE_5KHZ_UNI_1UM_40NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_400)/(STEP_SIZE_40NM*5) 			//scan:fov 1000(nm) + acel/dcel 400(nm) = 1400(nm), 1ms 당 200nm이면 1400(nm) = 7ms

#define PROFILE_5KHZ_UNI_1_5UM_10NM_MSECTIME		(SCAN_SIZE_01_5UM + SCAN_ADD_SIZE_400)/(STEP_SIZE_10NM*5)		//scan:fov 1500(nm) + acel/dcel 400(nm) = 1900(nm), 1ms 당 50nm이면 1900(nm) = 38ms
#define PROFILE_5KHZ_UNI_1_5UM_20NM_MSECTIME		(SCAN_SIZE_01_5UM + SCAN_ADD_SIZE_400)/(STEP_SIZE_20NM*5) 		//scan:fov 1500(nm) + acel/dcel 400(nm) = 1900(nm), 1ms 당 100nm이면 1900(nm) = 19ms
#define PROFILE_5KHZ_UNI_1_5UM_40NM_MSECTIME		(SCAN_SIZE_01_5UM + SCAN_ADD_SIZE_400)/(STEP_SIZE_40NM*5) 		//scan:fov 1500(nm) + acel/dcel 400(nm) = 1900(nm), 1ms 당 200nm이면 1900(nm) = 9.5ms

//#define PROFILE_5KHZ_UNI_2UM_10NM_MSECTIME		(SCAN_SIZE_019UM + SCAN_ADD_SIZE_400)/(STEP_SIZE_10NM*5)		//scan:fov 1900(nm) + acel/dcel 400(nm) = 2300(nm), 1ms 당 50nm이면 2300(nm) = 46ms
//#define PROFILE_5KHZ_UNI_2UM_20NM_MSECTIME		(SCAN_SIZE_019UM + SCAN_ADD_SIZE_400)/(STEP_SIZE_20NM*5) 		//scan:fov 1900(nm) + acel/dcel 400(nm) = 2300(nm), 1ms 당 100nm이면 2300(nm) = 23ms
#define PROFILE_5KHZ_UNI_2UM_10NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)/(STEP_SIZE_10NM*5)			//scan:fov 2000(nm) + acel/dcel 600(nm) = 2600(nm), 1ms 당 50nm이면 2600(nm) = 260ms -> 52ms
#define PROFILE_5KHZ_UNI_2UM_20NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)/(STEP_SIZE_20NM*5) 		//scan:fov 2000(nm) + acel/dcel 600(nm) = 2600(nm), 1ms 당 100nm이면 2600(nm) = 130ms -> 26ms

//#define PROFILE_5KHZ_UNI_3UM_10NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)/(STEP_SIZE_10NM*5)			//scan:fov 3000(nm) + acel/dcel 600(nm) = 3600(nm), 1ms 당 50nm이면 3600(nm) = 360ms -> 72ms
//#define PROFILE_5KHZ_UNI_3UM_20NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)/(STEP_SIZE_20NM*5) 		//scan:fov 3000(nm) + acel/dcel 600(nm) = 3600(nm), 1ms 당 100nm이면 3600(nm) = 180ms -> 36ms

#define PROFILE_5KHZ_UNI_10UM_20NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)/(STEP_SIZE_20NM*5)		//scan:fov 10000(nm) + acel/dcel 600(nm) = 10600(nm), 1ms 당 100nm이면 10600(nm) = 530ms -> 106ms
#define PROFILE_5KHZ_UNI_10UM_40NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)/(STEP_SIZE_40NM*5)		//scan:fov 10000(nm) + acel/dcel 600(nm) = 10600(nm), 1ms 당 200nm이면 10600(nm) = 265ms -> 53ms
#define PROFILE_5KHZ_UNI_10UM_80NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_1000)/(STEP_SIZE_80NM*5)		//scan:fov 10000(nm) + acel/dcel 1000(nm) = 11000(nm), 1ms 당 400nm이면 11000(nm) = 27.5ms
//#define PROFILE_5KHZ_UNI_10UM_80NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)/(STEP_SIZE_80NM*5)		//scan:fov 10000(nm) + acel/dcel 600(nm) = 10600(nm), 1ms 당 400nm이면 10600(nm) = 132.5ms -> 26.5ms

#define PROFILE_5KHZ_UNI_1UM_10NM_POINTNO		PROFILE_5KHZ_UNI_1UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//1um->28ms*16.667points=466 points(1900nm)
#define PROFILE_5KHZ_UNI_1UM_20NM_POINTNO		PROFILE_5KHZ_UNI_1UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//1um->14ms*16.667points=233 points(1900nm)
#define PROFILE_5KHZ_UNI_1UM_40NM_POINTNO		PROFILE_5KHZ_UNI_1UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//1um->7ms*16.667points=116 points(1900nm)

#define PROFILE_5KHZ_UNI_1_5UM_10NM_POINTNO		PROFILE_5KHZ_UNI_1_5UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//1.5um->38ms*16.667points=633 points(1900nm)
#define PROFILE_5KHZ_UNI_1_5UM_20NM_POINTNO		PROFILE_5KHZ_UNI_1_5UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//1.5um->19ms*16.667points=316 points(1900nm)
#define PROFILE_5KHZ_UNI_1_5UM_40NM_POINTNO		PROFILE_5KHZ_UNI_1_5UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//1.5um->9.5ms*16.667points=158 points(1900nm)

#define PROFILE_5KHZ_UNI_2UM_10NM_POINTNO		PROFILE_5KHZ_UNI_2UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//2um->52ms*16.667points=866 points(2600nm), 1.9um->46ms*16.667points=766 points(2300nm)
#define PROFILE_5KHZ_UNI_2UM_20NM_POINTNO		PROFILE_5KHZ_UNI_2UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//2um->26ms*16.667points=433 points(2600nm), 1.9um->23ms*16.667points=383 points(2300nm)

//#define PROFILE_5KHZ_UNI_3UM_10NM_POINTNO		PROFILE_5KHZ_UNI_3UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//72ms*16.667points=1200 points
//#define PROFILE_5KHZ_UNI_3UM_20NM_POINTNO		PROFILE_5KHZ_UNI_3UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//36ms*16.667points=600 points

#define PROFILE_5KHZ_UNI_10UM_20NM_POINTNO		PROFILE_5KHZ_UNI_10UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//106ms*16.667points=1766 points
#define PROFILE_5KHZ_UNI_10UM_40NM_POINTNO		PROFILE_5KHZ_UNI_10UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//53ms*16.667points=883 points
#define PROFILE_5KHZ_UNI_10UM_80NM_POINTNO		PROFILE_5KHZ_UNI_10UM_80NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO	//27.5ms*16.667points=458 points

#define UNIDIRECTION_RETURN_POINTNO			400


//1Khz Bi-direction
#define PROFILE_BI_1UM_10NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_600)*2/STEP_SIZE_10NM		//Bi-Direction: 1000(nm) + 600(nm) = 1600(nm), 왕복 3200(nm), 1ms 당 10nm이면 3200(nm)는 320ms
#define PROFILE_BI_1UM_20NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_600)*2/STEP_SIZE_20NM		//Bi-Direction: 1000(nm) + 600(nm) = 1600(nm), 왕복 3200(nm), 1ms 당 20nm이면 3200(nm)는 160ms
#define PROFILE_BI_2UM_10NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)*2/STEP_SIZE_10NM		//Bi-Direction: 2000(nm) + 600(nm) = 2600(nm), 왕복 5200(nm), 1ms 당 10nm이면 5200(nm)는 520ms
#define PROFILE_BI_2UM_20NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_600)*2/STEP_SIZE_20NM		//Bi-Direction: 2000(nm) + 600(nm) = 2600(nm), 왕복 5200(nm), 1ms 당 20nm이면 5200(nm)는 260ms
#define PROFILE_BI_3UM_10NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)*2/STEP_SIZE_10NM		//Bi-Direction: 3000(nm) + 600(nm) = 3600(nm), 왕복 7200(nm), 1ms 당 10nm이면 7200(nm)는 720ms
#define PROFILE_BI_3UM_20NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_600)*2/STEP_SIZE_20NM		//Bi-Direction: 3000(nm) + 600(nm) = 3600(nm), 왕복 7200(nm), 1ms 당 20nm이면 7200(nm)는 360ms

#define PROFILE_BI_1UM_10NM_POINTNO			PROFILE_BI_1UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//320ms*16.667points=5333 points
#define PROFILE_BI_1UM_20NM_POINTNO			PROFILE_BI_1UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//160ms*16.667points=2666 points
#define PROFILE_BI_2UM_10NM_POINTNO			PROFILE_BI_2UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//520ms*16.667points=8666 points
#define PROFILE_BI_2UM_20NM_POINTNO			PROFILE_BI_2UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//260ms*16.667points=4333 points
#define PROFILE_BI_3UM_10NM_POINTNO			PROFILE_BI_3UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//720ms*16.667points=12000 points
#define PROFILE_BI_3UM_20NM_POINTNO			PROFILE_BI_3UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//360ms*16.667points=6000 points

//5Khz Bi-direction
#define PROFILE_5KHZ_BI_1UM_10NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_10NM*5)		//Bi-Direction: 1000(nm) + 400(nm) = 1400(nm), 왕복 2800(nm), 1ms 당  50nm이면 2800(nm)는 56ms
#define PROFILE_5KHZ_BI_1UM_20NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_20NM*5)		//Bi-Direction: 1000(nm) + 400(nm) = 1400(nm), 왕복 2800(nm), 1ms 당 100nm이면 2800(nm)는 28ms
#define PROFILE_5KHZ_BI_1UM_40NM_MSECTIME		(SCAN_SIZE_01UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_40NM*5)		//Bi-Direction: 1000(nm) + 400(nm) = 1400(nm), 왕복 2800(nm), 1ms 당 200nm이면 2800(nm)는 14ms

#define PROFILE_5KHZ_BI_1_5UM_10NM_MSECTIME		(SCAN_SIZE_01_5UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_10NM*5)		//Bi-Direction: 1500(nm) + 400(nm) = 1900(nm), 왕복 3800(nm), 1ms 당  50nm이면 3800(nm)는 76ms
#define PROFILE_5KHZ_BI_1_5UM_20NM_MSECTIME		(SCAN_SIZE_01_5UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_20NM*5)		//Bi-Direction: 1500(nm) + 400(nm) = 1900(nm), 왕복 3800(nm), 1ms 당 100nm이면 3800(nm)는 38ms
#define PROFILE_5KHZ_BI_1_5UM_40NM_MSECTIME		(SCAN_SIZE_01_5UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_40NM*5)		//Bi-Direction: 1500(nm) + 400(nm) = 1900(nm), 왕복 3800(nm), 1ms 당 200nm이면 3800(nm)는 19ms

#define PROFILE_5KHZ_BI_2UM_10NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_10NM*5)		//Bi-Direction: 2000(nm) + 400(nm) = 2400(nm), 왕복 4800(nm), 1ms 당  50nm이면 4800(nm)는 96ms
#define PROFILE_5KHZ_BI_2UM_20NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_20NM*5)		//Bi-Direction: 2000(nm) + 400(nm) = 2400(nm), 왕복 4800(nm), 1ms 당 100nm이면 4800(nm)는 48ms
#define PROFILE_5KHZ_BI_2UM_40NM_MSECTIME		(SCAN_SIZE_02UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_40NM*5)		//Bi-Direction: 2000(nm) + 400(nm) = 2400(nm), 왕복 4800(nm), 1ms 당 200nm이면 4800(nm)는 24ms

//#define PROFILE_5KHZ_BI_3UM_10NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_10NM*5)		//Bi-Direction: 3000(nm) + 400(nm) = 3400(nm), 왕복 6800(nm), 1ms 당  50nm이면 6800(nm)는 136ms
//#define PROFILE_5KHZ_BI_3UM_20NM_MSECTIME		(SCAN_SIZE_03UM + SCAN_ADD_SIZE_400)*2/(STEP_SIZE_20NM*5)		//Bi-Direction: 3000(nm) + 400(nm) = 3400(nm), 왕복 6800(nm), 1ms 당 100nm이면 6800(nm)는 68ms
#define PROFILE_5KHZ_BI_3UM_10NM_MSECTIME		(SCAN_SIZE_03UM /*+ SCAN_ADD_SIZE_400*/)*2/(STEP_SIZE_10NM*5)	//Bi-Direction: 3000(nm) + 0(nm) = 3000(nm), 왕복 6000(nm), 1ms 당  50nm이면 6000(nm)는 120ms
#define PROFILE_5KHZ_BI_3UM_20NM_MSECTIME		(SCAN_SIZE_03UM /*+ SCAN_ADD_SIZE_400*/)*2/(STEP_SIZE_20NM*5)	//Bi-Direction: 3000(nm) + 0(nm) = 3000(nm), 왕복 6000(nm), 1ms 당 100nm이면 6000(nm)는 60ms


#define PROFILE_5KHZ_BI_10UM_40NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)*2/(STEP_SIZE_40NM*5)		//Bi-Direction: 10000(nm) + 600(nm) = 10600(nm), 왕복 21200(nm), 1ms 당 200nm이면 21200(nm)는 106ms
#define PROFILE_5KHZ_BI_10UM_80NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)*2/(STEP_SIZE_80NM*5)		//Bi-Direction: 10000(nm) + 600(nm) = 10600(nm), 왕복 21200(nm), 1ms 당 400nm이면 21200(nm)는 53ms
#define PROFILE_5KHZ_BI_10UM_100NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)*2/(STEP_SIZE_100NM*5)		//Bi-Direction: 10000(nm) + 600(nm) = 10600(nm), 왕복 21200(nm), 1ms 당 500nm이면 21200(nm)는 42.4ms
#define PROFILE_5KHZ_BI_10UM_200NM_MSECTIME		(SCAN_SIZE_10UM + SCAN_ADD_SIZE_600)*2/(STEP_SIZE_200NM*5)		//Bi-Direction: 10000(nm) + 600(nm) = 10600(nm), 왕복 21200(nm), 1ms 당 1000nm이면 21200(nm)는 21.2ms


#define PROFILE_5KHZ_BI_1UM_10NM_POINTNO		PROFILE_5KHZ_BI_1UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//56ms*16.667points=933 points
#define PROFILE_5KHZ_BI_1UM_20NM_POINTNO		PROFILE_5KHZ_BI_1UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//28ms*16.667points=466 points
#define PROFILE_5KHZ_BI_1UM_40NM_POINTNO		PROFILE_5KHZ_BI_1UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//14ms*16.667points=233 points

//#define PROFILE_5KHZ_BI_1_5UM_10NM_POINTNO		PROFILE_5KHZ_BI_1_5UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//76ms*16.667points=1266 points
//#define PROFILE_5KHZ_BI_1_5UM_20NM_POINTNO		PROFILE_5KHZ_BI_1_5UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//38ms*16.667points=633 points
#define PROFILE_5KHZ_BI_1_5UM_40NM_POINTNO		PROFILE_5KHZ_BI_1_5UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//19ms*16.667points=316 points

//#define PROFILE_5KHZ_BI_2UM_10NM_POINTNO		PROFILE_5KHZ_BI_2UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//96ms*16.667points=1600 points
//#define PROFILE_5KHZ_BI_2UM_20NM_POINTNO		PROFILE_5KHZ_BI_2UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//48ms*16.667points=800 points
#define PROFILE_5KHZ_BI_2UM_40NM_POINTNO		PROFILE_5KHZ_BI_2UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//24ms*16.667points=400 points

#define PROFILE_5KHZ_BI_3UM_10NM_POINTNO		PROFILE_5KHZ_BI_3UM_10NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//136ms*16.667points=2266 points -> 120ms*16.667points=2000 points
#define PROFILE_5KHZ_BI_3UM_20NM_POINTNO		PROFILE_5KHZ_BI_3UM_20NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//68ms*16.667points=1133 points -> 60ms*16.667points=1000 points


#define PROFILE_5KHZ_BI_10UM_40NM_POINTNO		PROFILE_5KHZ_BI_10UM_40NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//106ms*16.667points=1766 points
#define PROFILE_5KHZ_BI_10UM_80NM_POINTNO		PROFILE_5KHZ_BI_10UM_80NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//53ms*16.667points=883 points
#define PROFILE_5KHZ_BI_10UM_100NM_POINTNO		PROFILE_5KHZ_BI_10UM_100NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//42.4ms*16.667points=706 points
#define PROFILE_5KHZ_BI_10UM_200NM_POINTNO		PROFILE_5KHZ_BI_10UM_200NM_MSECTIME*ONEMSEC_SERVO_CALCULATION_POINTNO		//21.2ms*16.667points=353 points


class AFX_EXT_CLASS CPIE712Ctrl : public CECommon
{
public:
	CPIE712Ctrl();
	~CPIE712Ctrl();

	//------ Stage Controller와의 통신 연결 및 초기화 관련 ------//
	BOOL m_bConnect;
	int m_nErrorCode;
	double m_dZUpperLimit;										//um 단위, Mask마다 높이 다르므로 Loading 후 항상 새로 설정해 줘야 한다.
	char m_chPIStage_ErrorMessage[ERROR_STRING_SIZE];
	char m_chPIStage_Strings[ERROR_STRING_SIZE];
	char m_chPIStage_Axis[20];
	double m_dPIStage_MovePos[AXIS_NUMBER];						//um 단위
	double m_dPIStage_GetPos[10];								//um 단위
	double m_dPIStage_MinusLimitPos[AXIS_NUMBER];				//um 단위
	double m_dPIStage_PlusLimitPos[AXIS_NUMBER];				//um 단위

public:
	BOOL ConnectComm(int CommunicationType, char *Ip, int nPort); //Stage Controller와 통신 연결하기.(ETHERNET,SERIAL,SIMULATOR)
	void Initialize_PIStage();
	int DisconnectComm();	/** 통신 port를 닫는다.	 */

	////--------DLL 설정 및 XY Scan 동작 관련 ---------//
	int m_nPIStage_ID;											//ID of Controller
	int m_nOffsetOfFirstPointInDdlTable;						//index of first value to be transferred(the first value in the DDL table has index 1)
	int m_nNumberOfValues;										//number of values to be transferred
	int* m_piPointOfSpeedUpDown;								//SpeedUpDown에 사용할 point 개수
	double* m_pdServoCalculationSpeed;							//PIE712 Controller Update Rate
	int* m_piNumberOfWavePoints;								//The length of the ramp curve as number of points
	int* m_piDdlLearningRepeatNumber;							//ddl 학습 반복 회수
	int* m_piRecordDataNumber;									//Record 기록할 data 개수
	int* m_piDdlPointNumber;									//ddl data 개수, m_piNumberOfWavePoints,m_nNumberOfValues와 정리 필요
	int* m_piCenterpointOfWave;									//The index of the center point of the ramp curve. Determines if the curve is symmetrically or not. Lowest possible value is 0.
	double* m_pdAmplitudeOfRampWave;							//The amplitude of the ramp curve
	double* m_pdAmplitudeOfLineWave;							//The amplitude of the scan line.
	double* m_pdValueDdlArrayX;									//Array with the values for the DDL table(can have been filled with PI_qDDL())
	double* m_pdValueDdlArrayY;									//Uni-direction 추가 위해 Y axis DDL 적용
	double m_pdValueArray[AXIS_NUMBER];							//array with the offsets of the wave generators.(PI_WOS use only)
	int m_nScanAxis;											//1: X Scan, 0: Y Scan
	int m_nStepAxis;											//1: X Scan, 0: Y Scan

	void ClearDDLTable();										//ddl table의 data를 clear한다. scan 후 항상 ddl clear 시켜야 offset이 안생김
	void DDL_Generation();										//지정된 Profile의 ddl 파일을 생성
	void LoadDDLdatatoController();								//ddl 버퍼 데이터를 Controller로 넣음
	void DDL_DataLoadingFromFile();								//모든 ddl file에 저장되어 있는 데이터를 버퍼로 읽어옴
	CString* m_pstrDdlfileX;										//x axis ddl data가 들어있는 file 명
	CString* m_pstrDdlfileY;										//y axis ddl data가 들어있는 file 명
	double** m_dpdDdlDataX;										//m_dpdDdlDataX[파일개수][데이터개수]
	double** m_dpdDdlDataY;										//m_dpdDdlDataY[파일개수][데이터개수]
	void SetWaveGeneratorOffset();								//Sets an offset to the output of a wave generator to 1, 1, 0, 0, 0 , 한번 scan하고나면 offset이 생기기 때문에 1, 1, 0, 0, 0 setting 해줌
	void GetDDLdataFromController();							//Get the DDL data from a DDL data table on the controller
	void WaveTableSelection();									//Wave table selection: connects a wave table to a wave generator or disconnects the selected generator from any wave table.
	void SetNumberOfCycleForWaveGeneration(int nRepeatNo);		//Set the number of cycles for the wave generator output (which is started with PI_WGO()).
	void RunWaveGenerator(int nModeX, int nModeY);				//Start and stop the specified wave generator with the given mode
	void TransferDDLtoController();								//Transfer DDL data to a DDL data table on controller

	void WaveGeneration();										//x,y axis의 wave를 만들어주는 함수, x axis ramp 생성, y axis step 생성
	void Scanning_Motion(int nRepeat);										//scanning만 수행하고 프로파일을 텍스트 파일로 저장하지 않음
	//void Contrast_Motion();									//contrast 구할 때 사용(x 축으로만 왔다갔다, y 축으로 이동 없음)

	void RecordScanProfile();									//scanning 프로파일을 텍스트 파일로 저장
	void SetDataRcorderConfig();								//Set data recorder configuration: determines the data source (szRecordSourceIdsArray) and the kind of data (piRecordOptionsArray) used for the given data recorder table.
	void SaveRecordData();										//ddl table 파일 저장

	BOOL m_bScanStop;											//Scan 동작 진행을 멈추고 싶을 때 flag를 TRUE로
	int m_nStrokeNo;											//FOV 번호 ex) 0: 1um_20nm, 1: 2um_20nm, 2: 2um_10nm, 3: 1um_20nm, 4: 1um_10nm 
	void ScanStart();											//scan start
	int m_nScanNum;

	int m_piWaveGeneratorIdsArray[AXIS_NUMBER];
	int m_piValueArray[AXIS_NUMBER];
	int m_piDdlTableIdsArray[AXIS_NUMBER];
	int m_iWaveGenerator[DDL_AXIS_NUMBER];
	int m_piNumberOfCyclesArray[DDL_AXIS_NUMBER];				//array with number of cycles for each wave generator in piWaveGeneratorIdsArray
	BOOL m_pbValueArray[DDL_AXIS_NUMBER];
	int m_iStatMode[DDL_AXIS_NUMBER];
	int m_iWaveTableIds[DDL_AXIS_NUMBER];
	int m_iDataRecorderChannelIds[RECORD_NUM];
	int m_scan_rep_mode[DDL_AXIS_NUMBER];
	int m_scan_mode[DDL_AXIS_NUMBER];
	int* m_piDdlScanNum;

	//-------- Data Record 관련 ----------//

	CString* m_pstrScamData;

	//-------- 기본 동작 관련 ----------//

	void ServoOn();													//servo on
	void GetPosAxesData();											//stage 좌표를 읽어온다.
	void Get_Stage_Axes();											//stage 축 정보를 읽어온다.

	void PI_Move_Absolute();										//MOVE 동작 수행
	void PI_Move_Relative();										//relative move
	void Move_AllAxis_Zero_Position();										//0,0,0,0,0으로 이동
	void Move_AllAxis_Initial_Position();								//X_INITIAL_POS_UM,Y_INITIAL_POS_UM,Z_INITIAL_POS_UM,TIP_COMPENSATION_ANGLE,TILT_COMPENSATION_ANGLE 으로 이동
	void Move_XY_Origin_Position();									//X_INITIAL_POS_UM,Y_INITIAL_POS_UM,TIP_COMPENSATION_ANGLE,TILT_COMPENSATION_ANGLE 으로 이동, 즉 z axis은 이동 없음
	void MoveZRelative_SlowInterlock(double pos_um, int direction);	//Mask Slip 이 발생하지 않도록 느리게 이동, 인터락도 고려해서 이동
	void MoveZAbsolute_SlowInterlock(double pos_um);				//Mask Slip 이 발생하지 않도록 느리게 이동, 인터락도 고려해서 이동

	double m_dInitialTx_urad;
	double m_dInitialTy_urad;


	BOOL PI_Stage_Connected_Check();
	//BOOL	PI_IsConnected(int ID);

	//void	PI_CloseConnection(int ID);
	//BOOL	PI_IsConnecting(int threadID, BOOL* bCOnnecting);

};

