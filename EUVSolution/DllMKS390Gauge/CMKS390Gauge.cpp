#include "stdafx.h"
#include "CMKS390Gauge.h"


CMKS390Gauge::CMKS390Gauge()
{
	cnt = 0;
}


CMKS390Gauge::~CMKS390Gauge()
{
	m_SendState = FALSE;
	ReData.Empty();
	ch_2.Empty();
	ch_3.Empty();
}

int CMKS390Gauge::ReceiveData(char *lParam, DWORD dwRead)
{
	int nRet = 0;
	CString ch_value;

	ReData = lParam;

	//Received 데이터가 정상 데이터이면 Signal 해준다.
	SetEvent(m_hStateOkEvent);	//Sets the specified event object to the signaled state.

	if (m_SendState)
	{
		ch_value = ReData.Mid(1, 2);
		
		// 2020.09.28
		// 센서 교체 (LLC <-> MC)
		//if (ch_value == "02") ch_2 = ReData.Mid(3, 10); //LLC
		//if (ch_value == "03") ch_3 = ReData.Mid(3, 10); //MC

		if (ch_value == "02") ch_3 = ReData.Mid(3, 10); //MC
		if (ch_value == "03") ch_2 = ReData.Mid(3, 10); //LLC

		if (cnt == 50)
		{
			SaveLogFile("MC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> MC_VacuumGauge : " + ch_3))); //통신 상태 기록.
			SaveLogFile("LLC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> LLC_VaccumGauge : " + ch_2)));	//통신 상태 기록.
			cnt = 0;
		}
		cnt++;
	}
	else
	{
		ReData.Format(_T("Send Error"));
		ch_2 = ReData;
		ch_3 = ReData;
		SaveLogFile("MC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> MC_VacuumGauge : " + ch_3))); //통신 상태 기록.
		SaveLogFile("LLC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> LLC_VaccumGauge : " + ch_2)));	//통신 상태 기록.
	}
	return nRet;
}

int CMKS390Gauge::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;


	ResetEvent(m_hStateOkEvent);	//Sets the specified event object to the nonsignaled state.

	m_strSendMsg = lParam;

	nRet = Send(lParam, nTimeOut, nRetryCnt);

	nRet = WaitEvent(m_hStateOkEvent, nTimeOut);

	if (!nRet) m_SendState = TRUE;
	else	m_SendState = FALSE;

	return nRet;
}



int CMKS390Gauge::OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
{

	int nRet = -1;
	nRet = OpenSerialPort(sPortName, dwBaud, wByte, wStop, wParity);

	if (nRet != 0) {
		m_strSendMsg = _T("SERIAL PORT OPEN FAIL");
	}
	else {
		m_strSendMsg = _T("SERIAL PORT OPEN");

	}
	SaveLogFile("MC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> MC_VacuumGauge : " + sPortName + " " + m_strSendMsg))); //통신 상태 기록.
	SaveLogFile("LLC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> LLC_VaccumGauge : " + sPortName + " " + m_strSendMsg)));	//통신 상태 기록.


	return nRet;
}


