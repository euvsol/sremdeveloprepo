/**
 * MKS 390 Vaccum Gauge Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

#pragma once
class AFX_EXT_CLASS CMKS390Gauge : public CSerialCom
{
public:
	CMKS390Gauge();
	~CMKS390Gauge();

	virtual int			ReceiveData(char *lParam, DWORD dwRead);
	virtual int			SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual int			OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);

	CString						ReData;
	CString						ch_2;
	CString						ch_3;
	CString						State;

	bool						m_SendState;

	int cnt;
};

