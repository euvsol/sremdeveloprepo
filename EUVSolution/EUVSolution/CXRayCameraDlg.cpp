#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <random>

IMPLEMENT_DYNAMIC(CXRayCameraDlg, CDialogEx)

CXRayCameraDlg* CXRayCameraDlg::m_pDlgInst = NULL;

CXRayCameraDlg::CXRayCameraDlg(CWnd* pParent)
	: CDialogEx(IDD_XRAY_CAMERA_DIALOG, pParent)	
	, m_strSavePath(_T("D:\\Xray Images"))
	, m_strSaveFileName(_T("Ephase"))
	, m_bAddDateToFilename(TRUE)
	, m_bAddIncrementToFileName(FALSE)
	, m_nIncrementNumber(1)
	, m_strMousePosition(_T(""))
	, m_ImageRoiX(100)
	, m_ImageRoiY(100)
	, m_ImageRoiWidth(100)
	, m_ImageRoiHeight(100)
	, m_bSaveImage(TRUE)
	, m_bAutoScaleImage(TRUE)
	, m_LutLow(0)
	, m_LutHigh(65535)
	, m_CcdRoiX(0)
	, m_CcdRoiY(0)
	, m_CcdRoiWidth(0)
	, m_CcdRoiHeight(0)
	, m_CcdRoiXBinning(0)
	, m_CcdRoiYBinning(0)
	, m_ExposureTime_ms(0)
	, m_bManualGrab(FALSE)
	, m_bCorrectBackGround(TRUE)
	, m_TempSetpoint(0)
	, m_bCorrectRotation(TRUE)
{
	m_pDlgInst = this;
	m_HistValues = new long long[m_hisNumOfIntensities];
	//m_brush.CreateSolidBrush(LAVENDER);
}

CXRayCameraDlg::~CXRayCameraDlg()
{
	delete[] m_HistValues;
	delete m_ChartHistogram.getChart();
	//m_brush.DeleteObject();
}

BEGIN_MESSAGE_MAP(CXRayCameraDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SET_SENSOR_TEMP_BUTTON, &CXRayCameraDlg::OnBnClickedSetSensorTempButton)	
	ON_BN_CLICKED(IDC_CONNECT_CAMERA_BUTTON, &CXRayCameraDlg::OnBnClickedConnectCameraButton)
	ON_BN_CLICKED(IDC_DISCONNECT_CAMERA_BUTTON, &CXRayCameraDlg::OnBnClickedDisconnectCameraButton)
	ON_EN_CHANGE(IDC_EDIT_SAVE_FILE_NAME, &CXRayCameraDlg::OnChangeEditSaveFileName)
	ON_BN_CLICKED(IDC_CHECK_ADD_DATE_TO_FILENAME, &CXRayCameraDlg::OnClickedCheckAddDateToFilename)
	ON_BN_CLICKED(IDC_CHECK_INCREMENT_FILE_NAME, &CXRayCameraDlg::OnClickedCheckIncrementFileName)
	ON_EN_CHANGE(IDC_EDIT_INCREMENT_START_NUMBER, &CXRayCameraDlg::OnEnChangeEditIncrementStartNumber)
	ON_EN_CHANGE(IDC_EDIT_IMAGE_ROI_X, &CXRayCameraDlg::OnEnChangeEditImageRoiX)
	ON_EN_CHANGE(IDC_EDIT_IMAGE_ROI_Y, &CXRayCameraDlg::OnEnChangeEditImageRoiY)
	ON_EN_CHANGE(IDC_EDIT_IMAGE_ROI_WIDTH, &CXRayCameraDlg::OnEnChangeEditImageRoiWidth)
	ON_EN_CHANGE(IDC_EDIT_IMAGE_ROI_HEIGHT, &CXRayCameraDlg::OnEnChangeEditImageRoiHeight)
	ON_BN_CLICKED(IDC_BUTTON_FIT_DISPLAY, &CXRayCameraDlg::OnBnClickedButtonFitDisplay)
	ON_BN_CLICKED(IDC_CHECK_SAVE_IMAGE, &CXRayCameraDlg::OnBnClickedCheckSaveImage)
	ON_BN_CLICKED(IDC_CHECK_AUTO_SCALE_IMAGE, &CXRayCameraDlg::OnBnClickedCheckAutoScaleImage)
	ON_EN_CHANGE(IDC_EDIT_CAMERA_ROI_X, &CXRayCameraDlg::OnEnChangeEditCameraRoiX)
	ON_EN_CHANGE(IDC_EDIT_CAMERA_ROI_Y, &CXRayCameraDlg::OnEnChangeEditCameraRoiY)
	ON_EN_CHANGE(IDC_EDIT_CAMERA_ROI_WIDTH, &CXRayCameraDlg::OnEnChangeEditCameraRoiWidth)
	ON_EN_CHANGE(IDC_EDIT_CAMERA_ROI_HEIGHT, &CXRayCameraDlg::OnEnChangeEditCameraRoiHeight)
	ON_EN_CHANGE(IDC_ROI_X_BINNING_EDIT, &CXRayCameraDlg::OnEnChangeRoiXBinningEdit)
	ON_EN_CHANGE(IDC_EDIT_AUTOSCALE_LOW, &CXRayCameraDlg::OnEnChangeEditAutoscaleLow)
	ON_EN_CHANGE(IDC_EDIT_AUTOSCALE_HIGH, &CXRayCameraDlg::OnEnChangeEditAutoscaleHigh)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_GRAB_STOP, &CXRayCameraDlg::OnBnClickedButtonGrabStop)
	ON_EN_CHANGE(IDC_EXPOSURE_TIME_EDIT, &CXRayCameraDlg::OnEnChangeExposureTimeEdit)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_SAVE_PATH, &CXRayCameraDlg::OnBnClickedButtonSelectSavePath)
	ON_BN_CLICKED(IDC_BUTTON_SET_ALIGN, &CXRayCameraDlg::OnBnClickedButtonSetAlign)
	ON_BN_CLICKED(IDC_BUTTON_SET_MEASURE, &CXRayCameraDlg::OnBnClickedButtonSetMeasure)
	ON_EN_CHANGE(IDC_ROI_Y_BINNING_EDIT, &CXRayCameraDlg::OnEnChangeRoiYBinningEdit)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CHECK_CORRECT_BACKGROUND, &CXRayCameraDlg::OnBnClickedCheckCorrectBackground)
	ON_BN_CLICKED(IDC_BUTTON_RESET_CCD_ROI, &CXRayCameraDlg::OnBnClickedButtonResetCcdRoi)
	ON_BN_CLICKED(IDC_BUTTON_SET_CCD_ROI, &CXRayCameraDlg::OnBnClickedButtonSetCcdRoi)
	ON_BN_CLICKED(IDC_CHECK_ROTATION_CORRECTION, &CXRayCameraDlg::OnBnClickedCheckRotationCorrection)
	ON_BN_CLICKED(IDC_BUTTON_GRAB, &CXRayCameraDlg::OnBnClickedButtonGrab)
	ON_BN_CLICKED(IDC_BUTTON_SET_IMAGE_ROI, &CXRayCameraDlg::OnBnClickedButtonSetImageRoi)	
	ON_BN_CLICKED(IDC_BUTTON_XRAY_SET_MEASURE_BACKGROUND, &CXRayCameraDlg::OnBnClickedButtonXraySetMeasureBackground)
	ON_BN_CLICKED(IDC_BUTTON_XRAY_SET_ALIGN_BACKGROUND, &CXRayCameraDlg::OnBnClickedButtonXraySetAlignBackground)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_SAVE_MEASURE_BACKGROUND, &CXRayCameraDlg::OnBnClickedButtonPhaseSaveMeasureBackground)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_SAVE_ALIGN_BACKGROUND, &CXRayCameraDlg::OnBnClickedButtonPhaseSaveAlignBackground)
END_MESSAGE_MAP()

void CXRayCameraDlg::DoDataExchange(CDataExchange* pDX)
{
	__super::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_CURRENT_TEMP_STATIC, m_CurrTempCtrl);
	DDX_Control(pDX, IDC_FAN_STATUS_STATIC, m_FanStatusCtrl);
	DDX_Control(pDX, IDC_TEMP_STATUS_STATIC, m_TempStatusCtrl);		
	DDX_Text(pDX, IDC_EDIT_SAVE_PATH, m_strSavePath);
	DDX_Text(pDX, IDC_EDIT_SAVE_FILE_NAME, m_strSaveFileName);
	DDX_Check(pDX, IDC_CHECK_ADD_DATE_TO_FILENAME, m_bAddDateToFilename);
	DDX_Check(pDX, IDC_CHECK_INCREMENT_FILE_NAME, m_bAddIncrementToFileName);
	DDX_Text(pDX, IDC_EDIT_INCREMENT_START_NUMBER, m_nIncrementNumber);
	DDX_Text(pDX, IDC_EDIT_MOUSE_POSITION, m_strMousePosition);
	DDX_Text(pDX, IDC_EDIT_IMAGE_ROI_X, m_ImageRoiX);
	DDX_Text(pDX, IDC_EDIT_IMAGE_ROI_Y, m_ImageRoiY);
	DDX_Text(pDX, IDC_EDIT_IMAGE_ROI_WIDTH, m_ImageRoiWidth);
	DDX_Text(pDX, IDC_EDIT_IMAGE_ROI_HEIGHT, m_ImageRoiHeight);
	DDX_Check(pDX, IDC_CHECK_SAVE_IMAGE, m_bSaveImage);
	DDX_Check(pDX, IDC_CHECK_AUTO_SCALE_IMAGE, m_bAutoScaleImage);
	DDX_Text(pDX, IDC_EDIT_AUTOSCALE_LOW, m_LutLow);
	DDX_Text(pDX, IDC_EDIT_AUTOSCALE_HIGH, m_LutHigh);
	DDX_Text(pDX, IDC_EDIT_CAMERA_ROI_X, m_CcdRoiX);
	DDX_Text(pDX, IDC_EDIT_CAMERA_ROI_Y, m_CcdRoiY);
	DDX_Text(pDX, IDC_EDIT_CAMERA_ROI_WIDTH, m_CcdRoiWidth);
	DDX_Text(pDX, IDC_EDIT_CAMERA_ROI_HEIGHT, m_CcdRoiHeight);
	DDX_Text(pDX, IDC_ROI_X_BINNING_EDIT, m_CcdRoiXBinning);
	DDX_Text(pDX, IDC_ROI_Y_BINNING_EDIT, m_CcdRoiYBinning);
	DDX_Text(pDX, IDC_EDIT_IMAGE_STATISTICS, m_strImageStatistics);
	DDX_Text(pDX, IDC_EXPOSURE_TIME_EDIT, m_ExposureTime_ms);
	DDX_Check(pDX, IDC_CHECK_CORRECT_BACKGROUND, m_bCorrectBackGround);
	DDX_Text(pDX, IDC_TEMP_SETPOINT_EDIT, m_TempSetpoint);
	DDX_Check(pDX, IDC_CHECK_ROTATION_CORRECTION, m_bCorrectRotation);
	DDX_Control(pDX, IDC_PROGRESS_READOUT_TIME, m_ctrlProgressReadoutTime);
	DDX_Control(pDX, IDC_PROGRESS_GRAB_EXPOSURE_TIME, m_ctrlProgressGrabExposureTime);
}

BOOL CXRayCameraDlg::OnInitDialog()
{
	__super::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_XRAY_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_GRAB_STATUS))->SetIcon(m_LedIcon[0]);

	if (m_bAddIncrementToFileName)
	{
		GetDlgItem(IDC_EDIT_INCREMENT_START_NUMBER)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_EDIT_INCREMENT_START_NUMBER)->EnableWindow(FALSE);
	}


	m_pBitmapInfo = NULL;
	m_bOnline = FALSE;
	

	MilInitialize();
	
	// UI Disable
	GetDlgItem(IDC_EDIT_AUTOSCALE_LOW)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT_AUTOSCALE_HIGH)->EnableWindow(FALSE);

	m_ctrlProgressGrabExposureTime.SetBarColor(GREEN);
	m_ctrlProgressReadoutTime.SetBarColor(BLUE);

	UpdateData(false);

	return TRUE;
}

void CXRayCameraDlg::OnDestroy()
{
	__super::OnDestroy();

	if (m_pBitmapInfo != NULL)
	{
		delete[] m_pBitmapInfo;
		m_pBitmapInfo = NULL;
	}

	MilDestroy();
}

int CXRayCameraDlg::OpenDevice()
{
	UpdateData(TRUE);

	char chGetString[128];
	int nValue = 0; double dValue = 0.0;

	if (g_pConfig->m_nEquipmentMode != OFFLINE)
		m_bOnline = TRUE;

	if (OpenXrayCamera(m_bOnline) == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_XRAY_CONNECT))->SetIcon(m_LedIcon[1]);

		ReadConfigFile(XRAY_CAMERA_CONFIG_FILE_FULLPATH);

		//임시 Temperature 는 카메라 세팅 읽어옴		
		piflt fValue;
		if (Picam_GetParameterFloatingPointValue(m_hDevice, PicamParameter_SensorTemperatureSetPoint, &fValue) == PicamError_None)
		{
			XrayCameraControlParameterInConfig.fSensorTemperatureSetPoint = fValue;
		}

		SetXrayCameraControlParameterFromConfig();
		GetXrayCameraControlParameterInDevice();

		UpdataXrayCameraParameter();

		m_TempSetpoint = GetTemperatureSetValue();

		//RegisterFunction(CopyXrayImageToMilBuffer);

		SetTimer(XRAY_CAMERA_UPDATE_TIMER, 500, NULL);

		if (m_MilImageChild != M_NULL)
		{
			MbufFree(m_MilImageChild);
			m_MilImageChild = M_NULL;
		}
		if (m_MilImage != M_NULL)
		{
			MbufFree(m_MilImage);
			m_MilImage = M_NULL;
		}
		if (m_MilImageOriginal != M_NULL)
		{
			MbufFree(m_MilImageOriginal);
			m_MilImageOriginal = M_NULL;
		}

		m_MilImageWidth = XrayCameraControlParameterInConfig.Roi.width / XrayCameraControlParameterInConfig.Roi.x_binning;
		m_MilImageHeight = XrayCameraControlParameterInConfig.Roi.height / XrayCameraControlParameterInConfig.Roi.y_binning;

		MbufAlloc2d(m_MilSystem, m_MilImageWidth, m_MilImageHeight, 16L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImage);
		MbufClear(m_MilImage, 0);

		MbufAlloc2d(m_MilSystem, m_MilImageWidth, m_MilImageHeight, 16L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImageOriginal);
		MbufClear(m_MilImageOriginal, 0);

		m_ImageRoiX = SupplymentaryParameter.ImageRoi.x;
		m_ImageRoiY = SupplymentaryParameter.ImageRoi.y;
		m_ImageRoiWidth = SupplymentaryParameter.ImageRoi.width;
		m_ImageRoiHeight = SupplymentaryParameter.ImageRoi.height;

		MbufChild2d(m_MilImage, m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight, &m_MilImageChild);
		MdispSelectWindow(m_MilDisplay, m_MilImage, m_pWndDisplay->m_hWnd);

		MgraRect(m_MilGraphContext, m_MilGraphList, m_ImageRoiX, m_ImageRoiY, m_ImageRoiX + m_ImageRoiWidth, m_ImageRoiY + m_ImageRoiHeight);
		MgraInquireList(m_MilGraphList, M_LIST, M_DEFAULT, M_LAST_LABEL, &m_RoiLabel);

		UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);

		UpdateData(FALSE);

		return TRUE;
	}

	UpdateData(FALSE);

	return FALSE;
}


void CXRayCameraDlg::UpdataXrayCameraParameter()
{
	m_ExposureTime_ms = XrayCameraControlParameterInDevice.fExposureTime;
	m_SensorTemperatureSetPoint = XrayCameraControlParameterInDevice.fSensorTemperatureSetPoint;

	m_CcdRoiX = XrayCameraControlParameterInDevice.Roi.x;
	m_CcdRoiY = XrayCameraControlParameterInDevice.Roi.y;
	m_CcdRoiWidth = XrayCameraControlParameterInDevice.Roi.width;
	m_CcdRoiHeight = XrayCameraControlParameterInDevice.Roi.height;
	m_CcdRoiXBinning = XrayCameraControlParameterInDevice.Roi.x_binning;
	m_CcdRoiYBinning = XrayCameraControlParameterInDevice.Roi.y_binning;
}

void CXRayCameraDlg::GetPeriodicDeviceStatus()
{
	if (m_bConnected == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_XRAY_CONNECT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_XRAY_CONNECT))->SetIcon(m_LedIcon[0]);

	if (m_hDevice == NULL) return;

	CString temp; int nValue;
	temp.Format("%.1f", GetTemperature());
	m_CurrTempCtrl.SetWindowText(temp);

	nValue = GetTemperatureStatus();
	if (nValue == 1)
		temp = _T("Unlocked");
	else if (nValue == 2)
		temp = _T("Locked");
	else if (nValue == 3)
		temp = _T("Faulted");
	else
		temp = _T("Unknown");
	m_TempStatusCtrl.SetWindowText(temp);

	if (m_bConnected == TRUE)
	{
		nValue = GetCoolingFanStatus();
		if (nValue == 1)
			temp = _T("Off");
		else if (nValue == 2)
			temp = _T("On");
		else if (nValue == 3)
			temp = _T("ForcedOn");
		else
			temp = _T("Unknown");
		m_FanStatusCtrl.SetWindowText(temp);
	}
	else
	{
		temp = _T("Unknown");
		m_FanStatusCtrl.SetWindowText(temp);
	}

	nValue = GetAcquisitionRunning();
	if (nValue == 1)
	{
		temp = _T("GRAB");
		m_bAcquisitionRunning = TRUE;
		((CStatic*)GetDlgItem(IDC_ICON_GRAB_STATUS))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		temp = _T("IDLE");
		m_bAcquisitionRunning = FALSE;
		((CStatic*)GetDlgItem(IDC_ICON_GRAB_STATUS))->SetIcon(m_LedIcon[0]);
	}

}

void CXRayCameraDlg::CreateBitmapInfo(int w, int h, int bpp)
{
	m_pBitmapInfo = (BITMAPINFO *) new BYTE[sizeof(BITMAPINFO)];

	m_pBitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_pBitmapInfo->bmiHeader.biPlanes = 1;
	m_pBitmapInfo->bmiHeader.biBitCount = bpp;
	m_pBitmapInfo->bmiHeader.biCompression = BI_RGB;
	m_pBitmapInfo->bmiHeader.biSizeImage = 0;
	m_pBitmapInfo->bmiHeader.biXPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biYPelsPerMeter = 0;
	m_pBitmapInfo->bmiHeader.biClrUsed = 0;
	m_pBitmapInfo->bmiHeader.biClrImportant = 0;

	m_pBitmapInfo->bmiHeader.biWidth = w;
	m_pBitmapInfo->bmiHeader.biHeight = -h;
}

void CXRayCameraDlg::SaveParameterData(CString sSector, CString sKey, CString sValue)
{
	WritePrivateProfileString(sSector, sKey, sValue, XRAY_CAMERA_CONFIG_FILE_FULLPATH);
}

void CXRayCameraDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case XRAY_CAMERA_UPDATE_TIMER:
		KillTimer(nIDEvent);
		GetPeriodicDeviceStatus();
		SetTimer(nIDEvent, 500, NULL);
		break;
	default:
		break;
	}
	__super::OnTimer(nIDEvent);
}

BOOL CXRayCameraDlg::PreTranslateMessage(MSG* pMsg)
{	
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
		{
			CWnd* ptrWnd = GetFocus();
			int ctrl_ID = ptrWnd->GetDlgCtrlID();
			if (ctrl_ID == IDC_EDIT_CAMERA_ROI_WIDTH)
			{
				int test = 1;
			}
			else if (ctrl_ID == IDC_EDIT_AUTOSCALE_LOW || IDC_EDIT_AUTOSCALE_HIGH)
			{
				UpdateData(TRUE);
				if (m_LutLow < 0)
				{
					AfxMessageBox(_T("Invalid Values"));
					m_LutLow = 0;
				}
				if (m_LutHigh > m_constLutMax)
				{
					AfxMessageBox(_T("Invalid Values"));
					m_LutHigh = m_constLutMax;
				}

				if (m_LutLow > m_LutHigh)
				{
					if (ctrl_ID == IDC_EDIT_AUTOSCALE_LOW)
					{
						m_LutLow = m_LutHigh;
					}
					else if (ctrl_ID == IDC_EDIT_AUTOSCALE_HIGH)
					{
						m_LutHigh = m_LutLow;
					}
					AfxMessageBox(_T("Invalid Values"));
				}

				isUpdatedLut = TRUE;
				GetDlgItem(IDC_EDIT_AUTOSCALE_LOW)->Invalidate(TRUE);
				GetDlgItem(IDC_EDIT_AUTOSCALE_HIGH)->Invalidate(TRUE);
				UpdateDisplayLut();
				UpdateData(FALSE);
			}

			return TRUE;
		}
		default:
			break;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


#if FALSE

int CXRayCameraDlg::GrabOld(bool IsManualGrab)
{
	int nReturn = -1;

	m_bManualGrab = IsManualGrab;
	m_ExposureTimeProgressPos = 0;

	long  nTimeout = (long)m_ExposureTime_ms + g_pConfig->m_dXrayCameraReadTimeout_ms;

	//bGrabComplete = FALSE;
	SingleGrab();

	clock_t clockt = clock();
	clock_t clockt_current;
	BOOL bLoopFlag = 1;
	MSG msg;

	while (bLoopFlag)
	{
		if (m_bStopped)
		{
			nReturn = GRAB_STOPPED;
			break;
		}
		clockt_current = clock();
		if (nTimeout - (clockt_current - clockt) < 0)
		{
			bLoopFlag = 0;
			GrabStop();
			MbufClear(m_MilImage, 0);
			nReturn = GRAB_TIMEOUT;
		}
		//if (bGrabComplete)
		{
			bLoopFlag = 0;
			nReturn = GRAB_OK;
		}
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	// Grab 완료후 이미지 데이터 처리 이쪽으로 이동
	if (nReturn == GRAB_OK)
	{
		// 이미지 화면 업데이트 금지
		MdispControl(m_MilDisplay, M_UPDATE, M_DISABLE);
		// Copy Data
		m_pDlgInst->MilRealloc(m_pDlgInst->m_nImageDataWidth, m_pDlgInst->m_nImageDataHeight);
		//MbufPut2d(m_pDlgInst->m_MilImage, 0, 0, m_pDlgInst->m_MilImageWidth, m_pDlgInst->m_MilImageHeight, &m_pDlgInst->m_ImageData16[0]);
		MbufPut2d(m_pDlgInst->m_MilImageOriginal, 0, 0, m_pDlgInst->m_MilImageWidth, m_pDlgInst->m_MilImageHeight, &m_pDlgInst->m_ImageData16[0]);


		// Bufcopy
		MbufCopy(m_pDlgInst->m_MilImageOriginal, m_pDlgInst->m_MilImage);

		if (m_bCorrectBackGround)
		{
			//BackGround 하단 픽셀
			int BackGroundHeight = g_pConfig->m_nBackGroundHeight;
			MIL_ID milImageBackGround = M_NULL;
			MbufChild2d(m_pDlgInst->m_MilImageOriginal, 0, m_MilImageHeight - BackGroundHeight, m_MilImageWidth, BackGroundHeight, &milImageBackGround);
			MIL_ID MilStatResult = MimAllocResult(m_MilSystem, M_DEFAULT, M_STAT_LIST, M_NULL);
			MimStat(milImageBackGround, MilStatResult, M_MEAN, M_NULL, M_NULL, M_NULL);
			MimGetResult(MilStatResult, M_MEAN + M_TYPE_MIL_DOUBLE, &m_ImageBackGround);
			MimFree(MilStatResult);
			MbufFree(milImageBackGround);
			// Background 보정
			MimArith(m_pDlgInst->m_MilImage, m_ImageBackGround, m_pDlgInst->m_MilImage, M_SUB_CONST + M_SATURATION);
		}


		// Rotation 보정		
		if (m_bCorrectRotation)
		{
			MimRotate(m_pDlgInst->m_MilImage, m_pDlgInst->m_MilImage, m_pDlgInst->m_ImageRotationAngle, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_BICUBIC + M_OVERSCAN_DISABLE);
		}

		m_pDlgInst->UpdateRoi(m_pDlgInst->m_ImageRoiX, m_pDlgInst->m_ImageRoiY, m_pDlgInst->m_ImageRoiWidth, m_pDlgInst->m_ImageRoiHeight);
		// 이미지 화면 업데이트
		MdispControl(m_MilDisplay, M_UPDATE, M_ENABLE);
		//Display 삭제 가능
		//MdispSelectWindow(m_MilDisplay, m_MilImage, m_pWndDisplay->m_hWnd);
		//MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_VISIBLE, M_TRUE);

		CalMilimgaeStatistics();

		if (m_bAutoScaleImage)
		{
			m_LutLow = m_ImageMin;
			m_LutHigh = m_ImageMax;

			UpdateDisplayLut();
		}

		//if (m_bSaveImage && m_bManualGrab)
		if (m_bSaveImage)
		{
			SaveImage();
			m_bManualGrab = FALSE;
		}
		// Histogram graph update
		//PlotHistogram();

		UpdateData(FALSE);
	}
	return nReturn;
}

#endif

int CXRayCameraDlg::Grab(bool IsManualGrab)
{
	int nReturn = -1;

	m_bManualGrab = IsManualGrab;
	m_bProgressBarLoopFlag = GRAB_PROGRESS;

	// Progress bar control
	m_ctrlProgressGrabExposureTime.SetPos(0);
	m_ctrlProgressGrabExposureTime.SetRange32(0, (int)m_ExposureTime_ms);

	m_ctrlProgressReadoutTime.SetPos(0);
	m_ctrlProgressReadoutTime.SetRange32(0, (int)g_pConfig->m_dXrayCameraReadTimeout_ms);

	clock_t clock_start = clock();
	clock_t clock_current;
	clock_t clock_exposure_set = (clock_t)m_ExposureTime_ms;
	clock_t clock_readout_set = (clock_t)g_pConfig->m_dXrayCameraReadTimeout_ms;


	int ExposureTimeProgressPos_ms = 0;
	int ReadTimeProgressPos_ms = 0;

	SingleGrab();

	DWORD nTimeout = m_ExposureTime_ms + g_pConfig->m_dXrayCameraReadTimeout_ms;
	BOOL bLoopFlag = TRUE;
	MSG msg;

	while (bLoopFlag)
	{
		// PumpMessages()
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		clock_t delta_t_ms;
		clock_current = clock();

		delta_t_ms = (double)(clock_current - clock_start);


		if (delta_t_ms <= clock_exposure_set)
		{
			m_ctrlProgressGrabExposureTime.SetPos((int)delta_t_ms);
		}
		else if (delta_t_ms > clock_exposure_set && delta_t_ms < clock_exposure_set + clock_readout_set)
		{
			m_ctrlProgressGrabExposureTime.SetPos((int)delta_t_ms);
			m_ctrlProgressReadoutTime.SetPos((int)(delta_t_ms - clock_exposure_set));
		}
		/*else if (delta_t_ms > clock_exposure_set + clock_readout_set)
		{
			m_bProgressBarLoopFlag = FALSE;
		}*/

		if (m_bStopped)
		{
			nReturn = GRAB_STOPPED;
			return nReturn;
			break;
		}
		// 이거 대기가 계속 초기화 되는거 같음 ihlee
		switch (MsgWaitForMultipleObjects(1, &m_hAcquisitionInactive, FALSE, nTimeout, QS_ALLINPUT))
		{
		case WAIT_OBJECT_0:
			bLoopFlag = FALSE;
			break;
		case WAIT_TIMEOUT:
			GrabStop();
			MbufClear(m_MilImage, 0);
			m_bProgressBarLoopFlag = GRAB_TIMEOUT;
			nReturn = GRAB_TIMEOUT;
			return nReturn;
			break;
		default:
			// PumpMessages()
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			break;

		}
	}

	m_bProgressBarLoopFlag = GRAB_OK;
	nReturn = GRAB_OK;
	// Progressbar reset
	m_ctrlProgressGrabExposureTime.SetPos(0);
	m_ctrlProgressReadoutTime.SetPos(0);


	//Reset		
	// 이미지 화면 업데이트 금지
	MdispControl(m_MilDisplay, M_UPDATE, M_DISABLE);
	// Copy Data
	m_pDlgInst->MilRealloc(m_pDlgInst->m_nImageDataWidth, m_pDlgInst->m_nImageDataHeight);
	//MbufPut2d(m_pDlgInst->m_MilImage, 0, 0, m_pDlgInst->m_MilImageWidth, m_pDlgInst->m_MilImageHeight, &m_pDlgInst->m_ImageData16[0]);
	MbufPut2d(m_pDlgInst->m_MilImageOriginal, 0, 0, m_pDlgInst->m_MilImageWidth, m_pDlgInst->m_MilImageHeight, &m_pDlgInst->m_ImageData16[0]);

	// Bufcopy
	MbufCopy(m_pDlgInst->m_MilImageOriginal, m_pDlgInst->m_MilImage);



	// Background 보정
	if (m_bCorrectBackGround)
	{
#if FALSE
		//BackGround 하단 픽셀
		int BackGroundHeight = g_pConfig->m_nBackGroundHeight;
		MIL_ID milImageBackGround = M_NULL;
		MbufChild2d(m_pDlgInst->m_MilImageOriginal, 0, m_MilImageHeight - BackGroundHeight, m_MilImageWidth, BackGroundHeight, &milImageBackGround);
		MIL_ID MilStatResult = MimAllocResult(m_MilSystem, M_DEFAULT, M_STAT_LIST, M_NULL);
		MimStat(milImageBackGround, MilStatResult, M_MEAN, M_NULL, M_NULL, M_NULL);
		MimGetResult(MilStatResult, M_MEAN + M_TYPE_MIL_DOUBLE, &m_ImageBackGround);
		MimFree(MilStatResult);
		MbufFree(milImageBackGround);
		// Background 보정
		MimArith(m_pDlgInst->m_MilImage, m_ImageBackGround, m_pDlgInst->m_MilImage, M_SUB_CONST + M_SATURATION);
#endif
		MIL_INT SizeX=0, SizeY=0;

		if (m_MilImageBackGround != M_NULL)
		{
			MbufInquire(m_MilImageBackGround, M_SIZE_X, &SizeX);
			MbufInquire(m_MilImageBackGround, M_SIZE_Y, &SizeY);
		}
		
		if (SizeX == m_pDlgInst->m_nImageDataWidth && SizeY == m_pDlgInst->m_nImageDataHeight)
		{
			MimArith(m_pDlgInst->m_MilImage, m_MilImageBackGround, m_pDlgInst->m_MilImage, M_SUB + M_SATURATION);
		}		
		
	}
	// Rotation 보정		
	if (m_bCorrectRotation && m_CcdRoiXBinning == 1)
	{
		MimRotate(m_pDlgInst->m_MilImage, m_pDlgInst->m_MilImage, m_pDlgInst->m_ImageRotationAngle, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_BICUBIC + M_OVERSCAN_DISABLE);
	}


	m_pDlgInst->UpdateRoi(m_pDlgInst->m_ImageRoiX, m_pDlgInst->m_ImageRoiY, m_pDlgInst->m_ImageRoiWidth, m_pDlgInst->m_ImageRoiHeight);
	// 이미지 화면 업데이트
	MdispControl(m_MilDisplay, M_UPDATE, M_ENABLE);
	//Display 삭제 가능
	//MdispSelectWindow(m_MilDisplay, m_MilImage, m_pWndDisplay->m_hWnd);
	//MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_VISIBLE, M_TRUE);

	CalMilimgaeStatistics();

	if (m_bAutoScaleImage)
	{
		m_LutLow = m_ImageMin;
		m_LutHigh = m_ImageMax;

		UpdateDisplayLut();
	}

	//if (m_bSaveImage && m_bManualGrab)
	if (m_bSaveImage)
	{
		SaveImage();
		m_bManualGrab = FALSE;
	}
	// Histogram graph update
	//PlotHistogram();

	UpdateData(FALSE);

	return nReturn;
}



int CXRayCameraDlg::GrabTwice(bool IsManualGrab)
{
	int nReturn = -1;

	nReturn = Grab(IsManualGrab);

	if (nReturn != GRAB_OK)
	{
		nReturn = Grab(IsManualGrab);
	}

	return nReturn;
}

void CXRayCameraDlg::OnBnClickedButtonGrabStop()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedButtonGrabStop() 버튼 클릭!"));

	if (m_hDevice == NULL)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	GrabStop();
	m_bProgressBarLoopFlag = FALSE;
	MbufClear(m_MilImage, 0);
}

void CXRayCameraDlg::OnBnClickedSetSensorTempButton()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedSetSensorTempButton() 버튼 클릭!"));

	if (m_hDevice == NULL)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	UpdateData(TRUE);

	int nRet = SetTemperatureSetPoint(m_TempSetpoint);
	if (nRet != 0)
	{
		AfxMessageBox(_T("Failed to set sensor temperature setpoint."));
		return;
	}

	//m_TempSetpoint = GetTemperature();
	UpdateData(FALSE);
	//SaveParameterData(_T("TEMPERATURE"), _T("SET_POINT"), temp);
}

int CXRayCameraDlg::SetCcdRoi(int x, int y, int width, int height, int x_binning, int y_binning)
{
	int nRet = -1;
	PicamRoi roi;

	roi.x = x;
	roi.y = y;
	roi.width = width;
	roi.height = height;
	roi.x_binning = x_binning;
	roi.y_binning = y_binning;

	nRet = SetRoi(roi);

	if (nRet == 0)
	{
		m_CcdRoiX = x;
		m_CcdRoiY = y;
		m_CcdRoiWidth = width;
		m_CcdRoiHeight = height;
		m_CcdRoiXBinning = x_binning;
		m_CcdRoiYBinning = y_binning;
	}
	else
	{
		AfxMessageBox(_T("Failed to set ROI."));
		GetCcdRoi();
		nRet = -2;
		return nRet;
	}

	if (m_hDevice)
	{
		if (!InitializeCalculatedBufferSize()) return FALSE;
		if (!InitializeImage()) return FALSE;
	}
	else
	{
		nRet = -2;
		return nRet;
	}

	return nRet;
}

void CXRayCameraDlg::GetCcdRoi()
{
	int nRet = -1;

	PicamRoi roi;

	nRet = GetRoi(&roi);

	if (nRet != 0)
	{
		AfxMessageBox(_T("Failed to get ROI."));
	}
	else
	{
		m_CcdRoiX = roi.x;
		m_CcdRoiY = roi.y;
		m_CcdRoiWidth = roi.width;
		m_CcdRoiHeight = roi.height;
		m_CcdRoiXBinning = roi.x_binning;
		m_CcdRoiYBinning = roi.y_binning;
	}
}

void CXRayCameraDlg::OnBnClickedConnectCameraButton()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedConnectCameraButton() 버튼 클릭!"));

	BOOL bCheck = g_pConfig->m_nEquipmentMode != OFFLINE ? TRUE : FALSE;

	if (m_bConnected == TRUE)
	{
		AfxMessageBox(_T("Aready connected."));
		return;
	}

	OpenXrayCamera(bCheck);
}

void CXRayCameraDlg::OnBnClickedDisconnectCameraButton()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedDisconnectCameraButton() 버튼 클릭!"));

	if (m_bConnected == FALSE)
	{
		AfxMessageBox(_T("Aready disconnected."));
		return;
	}

	CloseXrayCamera();
}


int CXRayCameraDlg::MoveGrab(double x, double y)
{

	return 0;
}

INT CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lp, LPARAM pData)
{
	if (uMsg == BFFM_INITIALIZED) SendMessage(hwnd, BFFM_SETSELECTION, TRUE, pData);
	return 0;
}

void CXRayCameraDlg::OnChangeEditSaveFileName()
{
	UpdateData(TRUE);
}

void CXRayCameraDlg::OnClickedCheckAddDateToFilename()
{
	UpdateData(TRUE);
}

void CXRayCameraDlg::OnClickedCheckIncrementFileName()
{
	UpdateData(TRUE);

	m_nIncrementNumber = 1;

	if (m_bAddIncrementToFileName)
	{
		GetDlgItem(IDC_EDIT_INCREMENT_START_NUMBER)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_EDIT_INCREMENT_START_NUMBER)->EnableWindow(FALSE);
	}
	UpdateData(FALSE);
}

void CXRayCameraDlg::OnEnChangeEditIncrementStartNumber()
{
	UpdateData(TRUE);
}

void CXRayCameraDlg::OnCbnSelchangeComboIncrementDigit()
{
	UpdateData(TRUE);
}

void CXRayCameraDlg::SaveImage()
{
	//1.저장할 폴더 날짜별로 생성
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	CString fullfilenameOriginal;
	CString fullfilenameBackground;
	CString fullfilenameResult;
	CString filename;
	CString saveDirectory;
	CString filenameDate;
	CString filenameIncrement;

	saveDirectory = m_pDlgInst->m_strSavePath;

	filenameDate.Format(_T("_%d%02d%02d_%02d%02d%02d"), year, month, day, hour, minute, second);

	filename = m_strSaveFileName;

	if (m_bAddDateToFilename)
	{
		filename = filename + filenameDate;
	}

	if (m_bAddIncrementToFileName)
	{
		filenameIncrement.Format(_T("_%03d"), m_nIncrementNumber);
		m_nIncrementNumber++;
		filename = filename + filenameIncrement;
	}

	//fullfilename = saveDirectory + "\\" + filename + ".raw";

	fullfilenameOriginal = saveDirectory + "\\" + "Origial" + filenameDate + ".mim";
	fullfilenameBackground = saveDirectory + "\\" + "Background" + filenameDate + ".mim";
	fullfilenameResult = saveDirectory + "\\" + "Result" + filenameDate + ".mim";

	if(m_MilImageOriginal != M_NULL)
		MbufSave(fullfilenameOriginal, m_MilImageOriginal);
	if (m_MilImageBackGround != M_NULL)
		MbufSave(fullfilenameBackground, m_MilImageBackGround);
	if (m_MilImage != M_NULL)
		MbufSave(fullfilenameResult, m_MilImage);

	//FILE *pFile;
	//errno_t err;
	//err = fopen_s(&pFile, fullfilename, "wb");
	//if (err == 0)
	//{
	//	if (!fwrite(&m_pDlgInst->m_ImageData16[0], 1, m_pDlgInst->m_nReadoutStride, pFile))
	//		AfxMessageBox(_T("Failed to write image data."));
	//	fclose(pFile);
	//}
	//else
	//	AfxMessageBox(_T("Failed to open file."));

	UpdateData(FALSE);
}

void CXRayCameraDlg::MilInitialize()
{
	m_pWndDisplay = (CWnd *)(GetDlgItem(IDC_XRAY_IMAGE_VIEW));
	//m_pWndDisplay = (CWnd *)(GetDlgItem(IDC_STATIC_TEST));

	//MsysAlloc(M_SYSTEM_HOST, M_DEF_SYSTEM_NUM, M_COMPLETE, &m_MilSystem);	
	m_MilSystem = g_milSystemHost;
	MdispAlloc(m_MilSystem, M_DEFAULT, M_DISPLAY_SETUP, M_DEFAULT, &m_MilDisplay);

	MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);
	MdispControl(m_MilDisplay, M_MOUSE_USE, M_ENABLE);
	MdispControl(m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_ENABLE);
	MdispControl(m_MilDisplay, M_BACKGROUND_COLOR, M_COLOR_BLACK);
	MdispControl(m_MilDisplay, M_CENTER_DISPLAY, M_ENABLE);
	//MdispControl(m_MilDisplay, M_UPDATE, M_DISABLE);
	//MdispControl(m_pDlgInst->m_MilDisplay, M_VIEW_MODE, M_AUTO_SCALE);

	MdispHookFunction(m_MilDisplay, M_MOUSE_MOVE, MouseMoveFct, (void*)this);
	MdispHookFunction(m_MilDisplay, M_MOUSE_LEFT_BUTTON_UP, MouseLefeButtonUpFct, (void*)this);


	MgraAlloc(m_MilSystem, &m_MilGraphContext);
	MgraAllocList(m_MilSystem, M_DEFAULT, &m_MilGraphList);
	MgraColor(m_MilGraphContext, M_COLOR_RED);
	MdispControl(m_MilDisplay, M_ASSOCIATED_GRAPHIC_LIST_ID, m_MilGraphList);
	//MgraHookFunction(m_MilGraphList, M_INTERACTIVE_GRAPHIC_STATE_MODIFIED, GraphicListModifiedHookFct, (void*)this);
	MgraControl(m_MilGraphContext, M_ROTATABLE, M_DISABLE);
	//MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_INTERACTIVE_ANNOTATIONS_COLOR, M_COLOR_DARK_RED);
	MdispControl(m_MilDisplay, M_GRAPHIC_LIST_INTERACTIVE, M_ENABLE);

	//LUT
	m_MilColorLut = MbufAllocColor(m_MilSystem, 3, m_constLutMax + 1, 1, 8 + M_UNSIGNED, M_LUT, M_NULL);

	m_ImageRotationAngle = g_pConfig->m_dImageRotationAngle_deg;
	UpdateDisplayLut();

	//MbufAlloc2d(m_pDlgInst->m_MilSystem, m_pDlgInst->m_MilImageWidth, m_pDlgInst->m_MilImageHeight, 16L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_pDlgInst->m_MilImage);
}

void CXRayCameraDlg::UpdateDisplayLut()
{
	int end = m_constLutMax;

	MIL_ID MilDisplayLutChild;

	if (m_LutLow > 0)
	{
		MilDisplayLutChild = MbufChild1d(m_MilColorLut, 0, m_LutLow, M_NULL);
		MbufClear(MilDisplayLutChild, M_COLOR_DARK_BLUE);
		MbufFree(MilDisplayLutChild);
	}

	if (m_LutHigh - m_LutLow + 1 > 0)
	{
		MilDisplayLutChild = MbufChild1d(m_MilColorLut, m_LutLow, m_LutHigh - m_LutLow + 1, M_NULL);
		MgenLutFunction(MilDisplayLutChild, M_COLORMAP_JET, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT);
		MbufFree(MilDisplayLutChild);
	}

	if (end - m_LutHigh > 0)
	{
		MilDisplayLutChild = MbufChild1d(m_MilColorLut, m_LutHigh + 1, end - m_LutHigh, M_NULL);
		MbufClear(MilDisplayLutChild, M_COLOR_DARK_RED);
		MbufFree(MilDisplayLutChild);
	}

	MdispLut(m_MilDisplay, m_MilColorLut);

}

void CXRayCameraDlg::CalMilimgaeStatistics()
{
	MIL_ID milimage = m_MilImageChild ? m_MilImageChild : m_MilImage;

	MIL_ID MilStatResult = MimAllocResult(m_MilSystem, M_DEFAULT, M_STAT_LIST, M_NULL);

	MimStat(milimage, MilStatResult, M_MIN + M_MAX + M_MEAN + M_SUM, M_NULL, M_NULL, M_NULL);

	MimGetResult(MilStatResult, M_MAX + M_TYPE_MIL_DOUBLE, &m_ImageMax);
	MimGetResult(MilStatResult, M_MIN + M_TYPE_MIL_DOUBLE, &m_ImageMin);
	MimGetResult(MilStatResult, M_MEAN + M_TYPE_MIL_DOUBLE, &m_ImageMean);
	MimGetResult(MilStatResult, M_SUM + M_TYPE_MIL_DOUBLE, &m_ImageSum);
	m_strImageStatistics.Format("Mean:%.0f Min:%.0f Max:%.0f Sum:%.0f", m_ImageMean, m_ImageMin, m_ImageMax, m_ImageSum);

	MimFree(MilStatResult);

	MIL_ID HistResult;
	MimAllocResult(m_MilSystem, m_hisNumOfIntensities, M_HIST_LIST, &HistResult);
	MimHistogram(milimage, HistResult);
	MimGetResult(HistResult, M_VALUE, m_HistValues);
	MimFree(HistResult);
}

void CXRayCameraDlg::MilDestroy()
{
	//if (g_pConfig->m_nEquipmentMode != OFFLINE)
	if (m_MilDisplay != M_NULL)
	{
		MdispHookFunction(m_MilDisplay, M_MOUSE_MOVE + M_UNHOOK, MouseMoveFct, (void*)this);
		MdispHookFunction(m_MilDisplay, M_MOUSE_LEFT_BUTTON_UP + M_UNHOOK, MouseLefeButtonUpFct, (void*)this);
	}
	if (m_MilDisplay != M_NULL)
	{
		MdispControl(m_MilDisplay, M_ASSOCIATED_GRAPHIC_LIST_ID, M_NULL);
	}
	if (m_MilGraphList)
	{
		MgraFree(m_MilGraphList);
		m_MilGraphList = M_NULL;
	}
	if (m_MilGraphContext)
	{
		MgraFree(m_MilGraphContext);
		m_MilGraphContext = M_NULL;
	}
	if (m_MilDisplay != M_NULL)
	{
		MdispFree(m_MilDisplay);
		m_MilDisplay = M_NULL;
	}
	if (m_MilImageChild != M_NULL)
	{
		MbufFree(m_MilImageChild);
		m_MilImageChild = M_NULL;
	}
	if (m_MilImage != M_NULL)
	{
		MbufFree(m_MilImage);
		m_MilImage = M_NULL;
	}
	if (m_MilImageOriginal != M_NULL)
	{
		MbufFree(m_MilImageOriginal);
		m_MilImageOriginal = M_NULL;
	}
	
	if (m_MilImageBackGroundMeasure != M_NULL)
	{
		MbufFree(m_MilImageBackGroundMeasure);
		m_MilImageBackGroundMeasure = M_NULL;
	}

	if (m_MilImageBackGroundAlign != M_NULL)
	{
		MbufFree(m_MilImageBackGroundAlign);
		m_MilImageBackGroundAlign = M_NULL;
	}

	if (m_MilColorLut != M_NULL)
	{
		MbufFree(m_MilColorLut);
		m_MilColorLut = M_NULL;
	}
}


void CXRayCameraDlg::UpdateRoi(int x, int y, int width, int height)
{
	m_ImageRoiX = x;
	m_ImageRoiY = y;
	m_ImageRoiWidth = width;
	m_ImageRoiHeight = height;

	int ImageRoiXend = m_ImageRoiX + m_ImageRoiWidth;
	int ImageRoiYend = m_ImageRoiY + m_ImageRoiHeight;

	if (m_ImageRoiX < 0) m_ImageRoiX = 0;
	if (ImageRoiXend < 0) ImageRoiXend = 0;

	if (m_ImageRoiY < 0) m_ImageRoiY = 0;
	if (ImageRoiYend < 0) ImageRoiYend = 0;

	if (m_ImageRoiX > m_MilImageWidth) m_ImageRoiX = m_MilImageWidth;
	if (ImageRoiXend > m_MilImageWidth) ImageRoiXend = m_MilImageWidth;

	if (m_ImageRoiY > m_MilImageHeight) m_ImageRoiY = m_MilImageHeight;
	if (ImageRoiYend > m_MilImageHeight) ImageRoiYend = m_MilImageHeight;

	m_ImageRoiWidth = ImageRoiXend - m_ImageRoiX;
	m_ImageRoiHeight = ImageRoiYend - m_ImageRoiY;

	MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_POSITION_X, m_ImageRoiX);
	MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_POSITION_Y, m_ImageRoiY);
	MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_RECTANGLE_WIDTH, m_ImageRoiWidth);
	MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_RECTANGLE_HEIGHT, m_ImageRoiHeight);

	if (m_MilImageChild != M_NULL)
	{
		MbufFree(m_MilImageChild);
		m_MilImageChild = M_NULL;
	}

	if (m_ImageRoiWidth != 0 && m_ImageRoiHeight != 0)  // 이때 상황 처리필요 ihlee
	{
		MbufChild2d(m_MilImage, m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight, &m_MilImageChild);
		CalMilimgaeStatistics();

		if (m_pDlgInst->m_bAutoScaleImage)
		{
			m_LutLow = m_ImageMin;
			m_LutHigh = m_ImageMax;
			UpdateDisplayLut();
		}
	}

	UpdateData(FALSE);
	isUpdatedImageRoi = TRUE;
	Invalidate(TRUE);
}

MIL_INT MFTYPE CXRayCameraDlg::MouseMoveFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr)
{
	CXRayCameraDlg* pCurrentView = (CXRayCameraDlg*)UserDataPtr;

	if (pCurrentView)
	{
		MOUSEPOSITION MousePosition;
		MdispGetHookInfo(EventID, M_MOUSE_POSITION_X, &MousePosition.m_DisplayPositionX);
		MdispGetHookInfo(EventID, M_MOUSE_POSITION_Y, &MousePosition.m_DisplayPositionY);
		MdispGetHookInfo(EventID, M_MOUSE_POSITION_BUFFER_X, &MousePosition.m_BufferPositionX);
		MdispGetHookInfo(EventID, M_MOUSE_POSITION_BUFFER_Y, &MousePosition.m_BufferPositionY);

		pCurrentView->SetMousePosition(MousePosition);

		MIL_DOUBLE x, y, width, height;

		MgraInquireList(pCurrentView->m_MilGraphList, M_GRAPHIC_LABEL(pCurrentView->m_RoiLabel), M_DEFAULT, M_POSITION_X + M_TYPE_MIL_DOUBLE, &x);
		MgraInquireList(pCurrentView->m_MilGraphList, M_GRAPHIC_LABEL(pCurrentView->m_RoiLabel), M_DEFAULT, M_POSITION_Y + M_TYPE_MIL_DOUBLE, &y);
		MgraInquireList(pCurrentView->m_MilGraphList, M_GRAPHIC_LABEL(pCurrentView->m_RoiLabel), M_DEFAULT, M_RECTANGLE_WIDTH + M_TYPE_MIL_DOUBLE, &width);
		MgraInquireList(pCurrentView->m_MilGraphList, M_GRAPHIC_LABEL(pCurrentView->m_RoiLabel), M_DEFAULT, M_RECTANGLE_HEIGHT + M_TYPE_MIL_DOUBLE, &height);

		pCurrentView->m_ImageRoiX = round(x);
		pCurrentView->m_ImageRoiY = round(y);
		pCurrentView->m_ImageRoiWidth = round(width);
		pCurrentView->m_ImageRoiHeight = round(height);

		pCurrentView->UpdateData(false);
	}
	return 0;
}

MIL_INT MFTYPE CXRayCameraDlg::MouseLefeButtonUpFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr)
{
	CXRayCameraDlg* pCurrentView = (CXRayCameraDlg*)UserDataPtr;

	if (pCurrentView)
	{
		MIL_INT Selected = -1;

		MgraInquireList(pCurrentView->m_MilGraphList, M_GRAPHIC_LABEL(pCurrentView->m_RoiLabel), M_DEFAULT, M_GRAPHIC_SELECTED + M_TYPE_MIL_INT, &Selected);

		if (Selected == 1)
		{
			pCurrentView->UpdateRoi(pCurrentView->m_ImageRoiX, pCurrentView->m_ImageRoiY, pCurrentView->m_ImageRoiWidth, pCurrentView->m_ImageRoiHeight);
			pCurrentView->UpdateData(false);
		}
	}
	return 0;
}

void CXRayCameraDlg::SetMousePosition(MOUSEPOSITION MousePosition)
{
	UINT16 intensity = 0;
	m_ImageMousePosition = MousePosition;

	int x = round(m_ImageMousePosition.m_BufferPositionX);
	int y = round(m_ImageMousePosition.m_BufferPositionY);

	if (x >= 0 && x <= m_MilImageWidth - 1 && y >= 0 && y <= m_MilImageHeight - 1)
	{
		MbufGet2d(m_MilImage, x, y, 1, 1, &intensity);
		m_strMousePosition.Format("(%d, %d)  Gray:%u", x, y, intensity);
	}

	//int posX = (int)round(m_ImageMousePosition.m_BufferPositionX);
	//int posY = (int)round(m_ImageMousePosition.m_BufferPositionY);

	/*if (posX > 0 && posX < m_MilImageWidth && posY >0 && posY < m_MilImageHeight)
	{
		m_strMousePosition.Format("(%d, %d)  Gray:%u", posX, posY, intensity);
	}*/

	UpdateData(FALSE);
}

void CXRayCameraDlg::OnEnChangeEditImageRoiX()
{
	isUpdatedImageRoi = FALSE;

	GetDlgItem(IDC_EDIT_IMAGE_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_HEIGHT)->Invalidate(FALSE);
	//UpdateData(TRUE);

	//MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_POSITION_X, m_ImageRoiX);
	//UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);

	//UpdateData(FALSE);
}

void CXRayCameraDlg::OnEnChangeEditImageRoiY()
{
	isUpdatedImageRoi = FALSE;

	GetDlgItem(IDC_EDIT_IMAGE_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_HEIGHT)->Invalidate(FALSE);

	/*
		UpdateData(TRUE);

		MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_POSITION_Y, m_ImageRoiY);
		UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);

		UpdateData(FALSE);*/
}

void CXRayCameraDlg::OnEnChangeEditImageRoiWidth()
{
	isUpdatedImageRoi = FALSE;

	GetDlgItem(IDC_EDIT_IMAGE_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_HEIGHT)->Invalidate(FALSE);


	//UpdateData(TRUE);

	//MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_RECTANGLE_WIDTH, m_ImageRoiWidth);
	//UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);

	//UpdateData(FALSE);
}

void CXRayCameraDlg::OnEnChangeEditImageRoiHeight()
{
	isUpdatedImageRoi = FALSE;

	GetDlgItem(IDC_EDIT_IMAGE_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_HEIGHT)->Invalidate(FALSE);

	/*
		UpdateData(TRUE);

		MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_RECTANGLE_HEIGHT, m_ImageRoiHeight);
		UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);

		UpdateData(FALSE);*/
}

void CXRayCameraDlg::OnBnClickedButtonFitDisplay()
{	
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedButtonFitDisplay() 버튼 클릭!"));

	MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);
}

void CXRayCameraDlg::OnBnClickedCheckSaveImage()
{
	UpdateData(TRUE);
}

void CXRayCameraDlg::OnBnClickedCheckAutoScaleImage()
{
	UpdateData(TRUE);

	if (m_pDlgInst->m_bAutoScaleImage)
	{
		m_LutLow = m_ImageMin;
		m_LutHigh = m_ImageMax;

		isUpdatedLut = TRUE;
		GetDlgItem(IDC_EDIT_AUTOSCALE_LOW)->Invalidate(FALSE);
		GetDlgItem(IDC_EDIT_AUTOSCALE_HIGH)->Invalidate(FALSE);
		UpdateDisplayLut();

		GetDlgItem(IDC_EDIT_AUTOSCALE_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_AUTOSCALE_HIGH)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_EDIT_AUTOSCALE_LOW)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_AUTOSCALE_HIGH)->EnableWindow(TRUE);
	}
	UpdateData(FALSE);
}

void CXRayCameraDlg::OnEnChangeEditCameraRoiX()
{
	isUpdatedCcdRoi = FALSE;
	GetDlgItem(IDC_EDIT_CAMERA_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_HEIGHT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_X_BINNING_EDIT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_Y_BINNING_EDIT)->Invalidate(FALSE);
	/*
		UpdateData(TRUE);
		int nReturn = SetCcdRoi(m_CcdRoiX, m_CcdRoiY, m_CcdRoiWidth, m_CcdRoiHeight, m_CcdRoiXBinning, m_CcdRoiYBinning);
		if (nReturn == 0)
		{
			MilRealloc(m_CcdRoiWidth / m_CcdRoiXBinning, m_CcdRoiHeight / m_CcdRoiYBinning);
			UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);
		}
		UpdateData(FALSE);*/
}


void CXRayCameraDlg::OnEnChangeEditCameraRoiY()
{
	isUpdatedCcdRoi = FALSE;
	GetDlgItem(IDC_EDIT_CAMERA_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_HEIGHT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_X_BINNING_EDIT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_Y_BINNING_EDIT)->Invalidate(FALSE);
	/*
	/*UpdateData(TRUE);
	int nReturn = SetCcdRoi(m_CcdRoiX, m_CcdRoiY, m_CcdRoiWidth, m_CcdRoiHeight, m_CcdRoiXBinning, m_CcdRoiYBinning);
	if (nReturn == 0)
	{
		MilRealloc(m_CcdRoiWidth / m_CcdRoiXBinning, m_CcdRoiHeight / m_CcdRoiYBinning);
		UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);
	}
	UpdateData(FALSE);*/
}


void CXRayCameraDlg::OnEnChangeEditCameraRoiWidth()
{
	isUpdatedCcdRoi = FALSE;
	GetDlgItem(IDC_EDIT_CAMERA_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_HEIGHT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_X_BINNING_EDIT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_Y_BINNING_EDIT)->Invalidate(FALSE);
	/*
	/*UpdateData(TRUE);
	int nReturn = SetCcdRoi(m_CcdRoiX, m_CcdRoiY, m_CcdRoiWidth, m_CcdRoiHeight, m_CcdRoiXBinning, m_CcdRoiYBinning);
	if (nReturn == 0)
	{
		MilRealloc(m_CcdRoiWidth / m_CcdRoiXBinning, m_CcdRoiHeight / m_CcdRoiYBinning);
		UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);
	}
	UpdateData(FALSE);*/
}


void CXRayCameraDlg::OnEnChangeEditCameraRoiHeight()
{
	isUpdatedCcdRoi = FALSE;
	GetDlgItem(IDC_EDIT_CAMERA_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_HEIGHT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_X_BINNING_EDIT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_Y_BINNING_EDIT)->Invalidate(FALSE);
	/*
	/*UpdateData(TRUE);
	int nReturn = SetCcdRoi(m_CcdRoiX, m_CcdRoiY, m_CcdRoiWidth, m_CcdRoiHeight, m_CcdRoiXBinning, m_CcdRoiYBinning);
	if (nReturn == 0)
	{
		MilRealloc(m_CcdRoiWidth / m_CcdRoiXBinning, m_CcdRoiHeight / m_CcdRoiYBinning);
		UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);
	}
	UpdateData(FALSE);*/
}


void CXRayCameraDlg::OnEnChangeRoiXBinningEdit()
{
	isUpdatedCcdRoi = FALSE;
	GetDlgItem(IDC_EDIT_CAMERA_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_HEIGHT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_X_BINNING_EDIT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_Y_BINNING_EDIT)->Invalidate(FALSE);
	/*
	/*UpdateData(TRUE);
	int nReturn = SetCcdRoi(m_CcdRoiX, m_CcdRoiY, m_CcdRoiWidth, m_CcdRoiHeight, m_CcdRoiXBinning, m_CcdRoiYBinning);
	if (nReturn == 0)
	{
		MilRealloc(m_CcdRoiWidth / m_CcdRoiXBinning, m_CcdRoiHeight / m_CcdRoiYBinning);
		UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);
	}
	UpdateData(FALSE);*/
}

void CXRayCameraDlg::OnEnChangeRoiYBinningEdit()
{
	isUpdatedCcdRoi = FALSE;
	GetDlgItem(IDC_EDIT_CAMERA_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_HEIGHT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_X_BINNING_EDIT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_Y_BINNING_EDIT)->Invalidate(FALSE);
	/*
	/*UpdateData(TRUE);
	int nReturn = SetCcdRoi(m_CcdRoiX, m_CcdRoiY, m_CcdRoiWidth, m_CcdRoiHeight, m_CcdRoiXBinning, m_CcdRoiYBinning);
	if (nReturn == 0)
	{
		MilRealloc(m_CcdRoiWidth / m_CcdRoiXBinning, m_CcdRoiHeight / m_CcdRoiYBinning);
		UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);
	}
	UpdateData(FALSE);*/
}

void CXRayCameraDlg::OnBnClickedButtonResetCcdRoi()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedButtonResetCcdRoi() 버튼 클릭!"));

	UpdateData(FALSE);
	isUpdatedCcdRoi = TRUE;

	GetDlgItem(IDC_EDIT_CAMERA_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_HEIGHT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_X_BINNING_EDIT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_Y_BINNING_EDIT)->Invalidate(FALSE);
}


void CXRayCameraDlg::OnBnClickedButtonSetCcdRoi()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedButtonSetCcdRoi() 버튼 클릭!"));

	UpdateData(TRUE);

	int nReturn = SetCcdRoi(m_CcdRoiX, m_CcdRoiY, m_CcdRoiWidth, m_CcdRoiHeight, m_CcdRoiXBinning, m_CcdRoiYBinning);
	if (nReturn == 0)
	{
		MdispControl(m_MilDisplay, M_UPDATE, M_DISABLE);
		MilRealloc(m_CcdRoiWidth / m_CcdRoiXBinning, m_CcdRoiHeight / m_CcdRoiYBinning);
		UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);
		MbufClear(m_MilImage, 0);
		MdispControl(m_MilDisplay, M_UPDATE, M_ENABLE);
	}
	UpdateData(FALSE);
	isUpdatedCcdRoi = TRUE;

	GetDlgItem(IDC_EDIT_CAMERA_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_CAMERA_ROI_HEIGHT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_X_BINNING_EDIT)->Invalidate(FALSE);
	GetDlgItem(IDC_ROI_Y_BINNING_EDIT)->Invalidate(FALSE);
}


void CXRayCameraDlg::OnEnChangeEditAutoscaleLow()
{
	UpdateData(TRUE);
	isUpdatedLut = FALSE;
	GetDlgItem(IDC_EDIT_AUTOSCALE_LOW)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_AUTOSCALE_HIGH)->Invalidate(FALSE);
	UpdateData(FALSE);
}


void CXRayCameraDlg::OnEnChangeEditAutoscaleHigh()
{
	UpdateData(TRUE);
	isUpdatedLut = FALSE;
	GetDlgItem(IDC_EDIT_AUTOSCALE_LOW)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_AUTOSCALE_HIGH)->Invalidate(FALSE);
	UpdateData(FALSE);
}




void CXRayCameraDlg::OnEnChangeExposureTimeEdit()
{

	UpdateData(TRUE);

	if (m_hDevice == NULL)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	int nRet = SetExposureTime(m_ExposureTime_ms);

	if (nRet != 0)
	{
		AfxMessageBox(_T("Failed to set exposure time."));
		GetExposureTime(&m_ExposureTime_ms);

		UpdateData(TRUE);
		return;
	}
}




void CXRayCameraDlg::InitPlotHistogram()
{
	XYChart* xyChart = new XYChart(450, 200);

	// Set the plotarea at (50, 30) and of size 500 x 300 pixels, with transparent background and
	// border and light grey (0xcccccc) horizontal grid lines
	xyChart->setPlotArea(80, 15, 300, 150, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);

	// Display the mean and standard deviation on the chart	
	xyChart->addTitle("Histogram", "arial.ttf", 12);

	// Set the x and y axis label font to 12pt Arial
	xyChart->xAxis()->setLabelStyle("arial.ttf", 10);
	xyChart->yAxis()->setLabelStyle("arial.ttf", 10);

	// Set the x and y axis stems to transparent, and the x-axis tick color to grey (0x888888)
	xyChart->xAxis()->setColors(Chart::Transparent, Chart::TextColor, Chart::TextColor, 0x888888);
	xyChart->yAxis()->setColors(Chart::Transparent);

	double histogram[4] = { 1,2,3,4 };
	int slotCount = 4;

	// Draw the histogram as bars in blue (0x6699bb) with dark blue (0x336688) border
	BarLayer *histogramLayer = xyChart->addBarLayer(DoubleArray(histogram, slotCount), 0x6699bb);
	histogramLayer->setBorderColor(0x336688);
	histogramLayer->setXData(4, 7);
	histogramLayer->setBarGap(Chart::TouchBar);
	xyChart->xAxis()->setIndent(true);

	xyChart->makeChart();
	m_ChartHistogram.setChart(xyChart);


	// Include tool tip for the chart
	if (0 == m_ChartHistogram.getImageMapHandler())
	{
		// no existing image map - creates a new one
		//m_ChartHistogram.setImageMap(xyChart->getHTMLImageMap("clickable", "","title='x = {x}, y = {value}'"));
		m_ChartHistogram.setImageMap(xyChart->getHTMLImageMap("", "", "title='x = {x}, y = {value}'"));
	}


}
void CXRayCameraDlg::PlotHistogram()
{
	//
	// Classify the numbers into slots. In this example, the slot width is 5 units.
	//
	// Compute the min and max values, and extend them to the slot boundary.

	double minValue = (double)m_ImageMin;
	double maxValue = (double)m_ImageMax;

	int slotSize = (int)(maxValue - minValue) / 100;
	if (slotSize < 1)
		slotSize = 1;

	double minX = floor(minValue / slotSize) * slotSize;
	double maxX = floor(maxValue / slotSize) * slotSize + slotSize;


	// We can now determine the number of slots
	int slotCount = (int)((maxX - minX + 0.5) / slotSize);
	double *histogram = new double[slotCount];
	memset(histogram, 0, sizeof(*histogram) * slotCount);

	// Count the data points contained in each slot
	for (int i = m_ImageMin; i <= m_ImageMax; i++)
	{
		int slotIndex = (int)((i - minX) / slotSize);
		histogram[slotIndex] = histogram[slotIndex] + m_HistValues[i];
	}

	// Create a XYChart object of size 600 x 360 pixels
	delete m_ChartHistogram.getChart();
	XYChart *xyChart = new XYChart(450, 200);

	// Set the plotarea at (50, 30) and of size 500 x 300 pixels, with transparent background and
	// border and light grey (0xcccccc) horizontal grid lines	
	xyChart->setPlotArea(80, 15, 300, 150, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);

	// Display the mean and standard deviation on the chart	
	xyChart->addTitle("Histogram", "arial.ttf", 12);

	// Set the x and y axis label font to 12pt Arial
	xyChart->xAxis()->setLabelStyle("arial.ttf", 10);
	xyChart->yAxis()->setLabelStyle("arial.ttf", 10);

	// Set the x and y axis stems to transparent, and the x-axis tick color to grey (0x888888)
	xyChart->xAxis()->setColors(Chart::Transparent, Chart::TextColor, Chart::TextColor, 0x888888);
	xyChart->yAxis()->setColors(Chart::Transparent);

	// Draw the histogram as bars in blue (0x6699bb) with dark blue (0x336688) border	
	BarLayer *histogramLayer = xyChart->addBarLayer(DoubleArray(histogram, slotCount), 0x6699bb);
	histogramLayer->setBorderColor(0x336688);
	// The center of the bars span from minX + half_bar_width to maxX - half_bar_width
	histogramLayer->setXData(minX + slotSize / 2.0, maxX - slotSize / 2.0);


	// Configure the bars to touch each other with no gap in between
	histogramLayer->setBarGap(Chart::TouchBar);
	xyChart->xAxis()->setIndent(true);

	//xyChart->addLineLayer()->setXData(minX, maxX);
	xyChart->xAxis()->setLinearScale(minX + slotSize / 2.0, maxX - slotSize / 2.0, slotSize, 0);

	// Output the chart
	xyChart->makeChart();


	//free up resources	
	delete[] histogram;
	//const char *imageMap = xyChart->getHTMLImageMap("clickable");

	// Output the chart	
	m_ChartHistogram.setChart(xyChart);

	// Include tool tip for the chart
	if (0 == m_ChartHistogram.getImageMapHandler())
	{
		// no existing image map - creates a new one
		//m_ChartHistogram.setImageMap(xyChart->getHTMLImageMap("clickable", "","title='x = {x}, y = {value}'"));
		m_ChartHistogram.setImageMap(xyChart->getHTMLImageMap("", "", "title='x = {x}, y = {value}'"));
	}

	//m_ChartHistogram.setImageMap(imageMap);

}

void CXRayCameraDlg::OnBnClickedButtonSelectSavePath()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedButtonSelectSavePath() 버튼 클릭!"));

	CString folder = m_strSavePath;

	BROWSEINFO br;
	ZeroMemory(&br, sizeof(BROWSEINFO));
	br.lpfn = BrowseCallbackProc;
	br.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
	br.hwndOwner = GetSafeHwnd();
	br.lpszTitle = _T("Select Folder To Save Images");
	br.lParam = (LPARAM)folder.GetString();

	LPITEMIDLIST pidl = NULL;
	if ((pidl = SHBrowseForFolder(&br)) != NULL)
	{
		//wchar_t buffer[MAX_PATH];
		TCHAR buffer[MAX_PATH];
		if (SHGetPathFromIDList(pidl, buffer))
		{
			m_strSavePath = buffer;
		}
	}
}

double CXRayCameraDlg::intensity_simulation(double x, double y)
{
	double intensity, intensityX, intensityY;

	// LB Align  mark position
	double LBx = 172.768669;
	double LBy = 139.482797;

	// Align mark사이 간격
	double width = 129.900;
	double height = 103;
	double refABS = 2000, refML = 8000;

	double LTx = LBx;
	double LTy = LBy - height;

	double RTx = LTx - width;
	double RTy = LTy;

	double w = g_pConfig->m_dSlitWidth_um / 1000;
	double d = (g_pConfig->m_dSlitPitch_um - g_pConfig->m_dSlitWidth_um) / 1000;
	double l = g_pConfig->m_dSlitLength_um / 1000;

	double low = LTy;
	double high = LBy;

	if (y < low - l / 2)
		intensityY = refABS;
	else if (y >= low - l / 2 && y < low + l / 2)
		intensityY = (refML - refABS) / l * (y - low + l / 2) + refABS;
	else if (y >= low + l / 2 && y < high - l / 2)
		intensityY = refML;
	else if (y >= high - l / 2 && y < high + l / 2)
		intensityY = -(refML - refABS) / l * (y - high + l / 2) + refML;
	else if (y >= high + l / 2)
		intensityY = refABS;
	else
		intensityY = 0;

	low = RTx;
	high = LBx;

	if (x < low - (w + d / 2))
		intensityX = refABS;
	else if (x >= low - (w + d / 2) && x < low - (d / 2))
		intensityX = (refML - refABS) / w / 2 * (x + d / 2 - low) + (refABS + refML) / 2;
	else if (x >= low - (d / 2) && x < low + (d / 2))
		intensityX = (refABS + refML) / 2;
	else if (x >= low + (d / 2) && x < low + (w + d / 2))
		intensityX = (refML - refABS) / w / 2 * (x - d / 2 - low) + (refABS + refML) / 2;
	else if (x >= low + (w + d / 2) && x < high - (w + d / 2))
		intensityX = refML;
	else if (x >= high - (w + d / 2) && x < high - (d / 2))
		intensityX = -(refML - refABS) / w / 2 * (x + d / 2 - high) + (refABS + refML) / 2;
	else if (x >= high - (d / 2) && x < high + (d / 2))
		intensityX = (refABS + refML) / 2;
	else if (x >= high + (d / 2) && x < high + (w + d / 2))
		intensityX = -(refML - refABS) / w / 2 * (x - d / 2 - high) + (refABS + refML) / 2;
	else if (x >= high + (w + d / 2))
		intensityX = refABS;
	else
		intensityX = 0;

	double intensityXrate = (intensityX - refABS) / (refML - refABS);
	double intensityYrate = (intensityY - refABS) / (refML - refABS);

	double intensityRate = intensityXrate * intensityYrate;
	intensity = intensityRate * (refML - refABS) + refABS;


	// noise 
	random_device rd;
	mt19937 gen(rd());
	uniform_real_distribution<> dist(-1, 1);

	//intensity = intensity * (1 + dist(gen));
	intensity = intensity + dist(gen)*(refABS + refML) / 2 * 0.05;




	return intensity;
}

double CXRayCameraDlg::GetIntensity()
{
	double intensity = 0;
	//intensity = max(m_MilImageMean - m_MilImageBackGround, 0);

	intensity = m_ImageMean;
	//intensity = m_MilImageSum;

	//Test Code Intensity simulation
	if (FALSE)
	{
		double x, y;
		x = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		intensity = intensity_simulation(x, y);
	}


	return intensity;
}


void CXRayCameraDlg::OnBnClickedButtonSetAlign()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedButtonSetAlign() 버튼 클릭!"));

	UpdateData(TRUE);

	SetAlign(g_pConfig->m_dAlignExposureTime);
	/*m_ExposureTime_ms = g_pConfig->m_dAlignExposureTime;
	SetExposureTime(g_pXrayCamera->m_ExposureTime_ms);

	m_TempSetpoint = g_pXrayCamera->SupplymentaryParameter.OperationTemperature;
	SetTemperatureSetPoint(g_pXrayCamera->m_TempSetpoint);
*/
	UpdateData(FALSE);
}


void CXRayCameraDlg::OnBnClickedButtonSetMeasure()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedButtonSetMeasure() 버튼 클릭!"));

	UpdateData(TRUE);
	SetMeasure(g_pConfig->m_dMeasureExposureTime);

	/*m_ExposureTime_ms = g_pConfig->m_dMeasureExposureTime;
	SetExposureTime(g_pXrayCamera->m_ExposureTime_ms);
	m_TempSetpoint = g_pXrayCamera->SupplymentaryParameter.OperationTemperature;
	SetTemperatureSetPoint(g_pXrayCamera->m_TempSetpoint);	*/


	UpdateData(FALSE);
}


int CXRayCameraDlg::SetMeasure(double exposoureTime_ms)
{
	int nReturn = 0;


	if (g_pXrayCamera->m_ExposureTime_ms != exposoureTime_ms)
	{
		g_pXrayCamera->m_ExposureTime_ms = exposoureTime_ms;
		g_pXrayCamera->SetExposureTime(g_pXrayCamera->m_ExposureTime_ms);
		g_pXrayCamera->UpdateData(FALSE);
	}

	if (g_pXrayCamera->m_TempSetpoint != g_pXrayCamera->SupplymentaryParameter.OperationTemperature)
	{
		g_pXrayCamera->m_TempSetpoint = g_pXrayCamera->SupplymentaryParameter.OperationTemperature;
		g_pXrayCamera->SetTemperatureSetPoint(g_pXrayCamera->m_TempSetpoint);
		g_pXrayCamera->UpdateData(FALSE);
	}

	//	if (XrayCameraMode != PHASE_MEASURE)
	if (!(m_CcdRoiX == g_pConfig->m_nMeasureCcdX && m_CcdRoiY == g_pConfig->m_nMeasureCcdY &&  m_CcdRoiWidth == g_pConfig->m_nMeasureCcdWidth && m_CcdRoiHeight == g_pConfig->m_nMeasureCcdHeight
		&& m_CcdRoiXBinning == g_pConfig->m_nMeasureCcdBinning && m_CcdRoiYBinning == g_pConfig->m_nMeasureCcdBinning))
	{
		nReturn = SetCcdRoi(g_pConfig->m_nMeasureCcdX, g_pConfig->m_nMeasureCcdY, g_pConfig->m_nMeasureCcdWidth, g_pConfig->m_nMeasureCcdHeight, g_pConfig->m_nMeasureCcdBinning, m_CcdRoiYBinning = g_pConfig->m_nMeasureCcdBinning);

		if (nReturn == 0)
		{
			MdispControl(m_MilDisplay, M_UPDATE, M_DISABLE);
			MilRealloc(g_pConfig->m_nMeasureCcdWidth / g_pConfig->m_nMeasureCcdBinning, g_pConfig->m_nMeasureCcdHeight / g_pConfig->m_nMeasureCcdBinning);
			UpdateRoi(g_pConfig->m_nMeasureImageX, g_pConfig->m_nMeasureImageY, g_pConfig->m_nMeasureImageWidth, g_pConfig->m_nMeasureImageHeight);
			MbufClear(m_MilImage, 0);
			MdispControl(m_MilDisplay, M_UPDATE, M_ENABLE);
			MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);
		}
		//XrayCameraMode = PHASE_MEASURE;
		UpdateData(FALSE);
	}

	SetMeasureBackGround();

	isUpdatedCcdRoi = TRUE;
	Invalidate(FALSE);

	return nReturn;
}

int CXRayCameraDlg::SetAlign(double exposoureTime_ms)
{
	int nReturn = 0;

	if (g_pXrayCamera->m_ExposureTime_ms != exposoureTime_ms)
	{
		g_pXrayCamera->m_ExposureTime_ms = exposoureTime_ms;
		g_pXrayCamera->SetExposureTime(g_pXrayCamera->m_ExposureTime_ms);
		g_pXrayCamera->UpdateData(FALSE);
	}

	if (g_pXrayCamera->m_TempSetpoint != g_pXrayCamera->SupplymentaryParameter.OperationTemperature)
	{
		g_pXrayCamera->m_TempSetpoint = g_pXrayCamera->SupplymentaryParameter.OperationTemperature;
		g_pXrayCamera->SetTemperatureSetPoint(g_pXrayCamera->m_TempSetpoint);
		g_pXrayCamera->UpdateData(FALSE);
	}

	if (!(m_CcdRoiX == g_pConfig->m_nAlignCcdX && m_CcdRoiY == g_pConfig->m_nAlignCcdY &&  m_CcdRoiWidth == g_pConfig->m_nAlignCcdWidth && m_CcdRoiHeight == g_pConfig->m_nAlignCcdHeight
		&& m_CcdRoiXBinning == g_pConfig->m_nAlignCcdBinning && m_CcdRoiYBinning == g_pConfig->m_nAlignCcdBinning))
		//if (XrayCameraMode != PHASE_ALIGN)
	{
		nReturn = SetCcdRoi(g_pConfig->m_nAlignCcdX, g_pConfig->m_nAlignCcdY, g_pConfig->m_nAlignCcdWidth, g_pConfig->m_nAlignCcdHeight, g_pConfig->m_nAlignCcdBinning, g_pConfig->m_nAlignCcdBinning);

		if (nReturn == 0)
		{
			MdispControl(m_MilDisplay, M_UPDATE, M_DISABLE);
			MilRealloc(g_pConfig->m_nAlignCcdWidth / g_pConfig->m_nAlignCcdBinning, g_pConfig->m_nAlignCcdHeight / g_pConfig->m_nAlignCcdBinning);
			UpdateRoi(g_pConfig->m_nAlignImageX, g_pConfig->m_nAlignImageY, g_pConfig->m_nAlignImageWidth, g_pConfig->m_nAlignImageHeight);
			MbufClear(m_MilImage, 0);
			MdispControl(m_MilDisplay, M_UPDATE, M_ENABLE);
		}

		//XrayCameraMode = PHASE_ALIGN;

		UpdateData(FALSE);
	}

	SetAlignBackGround();

	isUpdatedCcdRoi = TRUE;
	Invalidate(FALSE);

	return nReturn;
}


void CXRayCameraDlg::SetMeasureBackGround()
{
	//MIL_ID m_MilImageBackGroundMeasure = M_NULL;
	
	//if (m_MilImageBackGroundMeasure != M_NULL)
	//{
	//	MbufFree(m_MilImageBackGroundMeasure);
	//	m_MilImageBackGroundMeasure = M_NULL;
	//}

	//char* FileName = "C:\\EUVSolution\\Config\\MeasureBackGround.mim";
	////char* FileName = "C:\\EUVSolution\\Config\\test.mim";

	//MbufRestore(FileName, m_MilSystem, &m_MilImageBackGroundMeasure);
	m_MilImageBackGround = m_MilImageBackGroundMeasure;

}

void CXRayCameraDlg::SetAlignBackGround()
{
	//MIL_ID m_MilImageBackGroundAlign = M_NULL;
	if (m_MilImageBackGroundAlign != M_NULL)
	{
		MbufFree(m_MilImageBackGroundAlign);
		m_MilImageBackGroundAlign = M_NULL;
	}
	char* FileName= "C:\\EUVSolution\\Config\\AlignBackGround.mim";

	MbufRestore(FileName, m_MilSystem, &m_MilImageBackGroundAlign);
	m_MilImageBackGround = m_MilImageBackGroundAlign;
}




int CXRayCameraDlg::MilRealloc(int width, int height)
{
	MIL_INT SizeX, SizeY;

	m_MilImageWidth = width;
	m_MilImageHeight = height;

	if (m_MilImage != M_NULL)
	{
		MbufInquire(m_MilImage, M_SIZE_X, &SizeX);
		MbufInquire(m_MilImage, M_SIZE_Y, &SizeY);

		if (SizeX != m_MilImageWidth || SizeY != m_MilImageHeight)
		{
			if (m_pDlgInst->m_MilImageChild != M_NULL)
			{
				MbufFree(m_pDlgInst->m_MilImageChild);
				m_pDlgInst->m_MilImageChild = M_NULL;
			}
			MbufFree(m_pDlgInst->m_MilImage);
			MbufFree(m_pDlgInst->m_MilImageOriginal);

			MbufAlloc2d(m_MilSystem, m_MilImageWidth, m_MilImageHeight, 16L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImage);
			MbufAlloc2d(m_MilSystem, m_MilImageWidth, m_MilImageHeight, 16L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImageOriginal);
		}
	}

	MdispSelectWindow(m_MilDisplay, m_MilImage, m_pWndDisplay->m_hWnd);

	//if (m_MilImage == M_NULL)
	//{
	//	MbufAlloc2d(m_MilSystem, m_MilImageWidth, m_MilImageHeight, 16L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImage);
	//	MbufAlloc2d(m_MilSystem, m_MilImageWidth, m_MilImageHeight, 16L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImageOriginal);
	//	MdispSelectWindow(m_MilDisplay, m_MilImage, m_pWndDisplay->m_hWnd);
	//}
	//else
	//{
	//	MbufInquire(m_MilImage, M_SIZE_X, &SizeX);
	//	MbufInquire(m_MilImage, M_SIZE_Y, &SizeY);

	//	if (SizeX != m_MilImageWidth || SizeY != m_MilImageHeight)
	//	{
	//		if (m_pDlgInst->m_MilImageChild != M_NULL)
	//		{
	//			MbufFree(m_pDlgInst->m_MilImageChild);
	//			m_pDlgInst->m_MilImageChild = M_NULL;
	//		}
	//		MbufFree(m_pDlgInst->m_MilImage);
	//		MbufAlloc2d(m_MilSystem, m_MilImageWidth, m_MilImageHeight, 16L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImage);

	//		MbufFree(m_pDlgInst->m_MilImageOriginal);
	//		MbufAlloc2d(m_MilSystem, m_MilImageWidth, m_MilImageHeight, 16L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImageOriginal);			
	//	}
	//}

	return 0;
}


void CXRayCameraDlg::trackLineLabel(XYChart *chart, int mouseX)
{
	// Clear the current dynamic layer and get the DrawArea object to draw on it.
	DrawArea *d = chart->initDynamicLayer();

	// The plot area object
	PlotArea *plotArea = chart->getPlotArea();

	// Get the data x-value that is nearest to the mouse, and find its pixel coordinate.
	double xValue = chart->getNearestXValue(mouseX);
	//double yValue = c->getNearestXValue(mouseX);
	int xCoor = chart->getXCoor(xValue);

	// Draw a vertical track line at the x-position
	d->vline(plotArea->getTopY(), plotArea->getBottomY(), xCoor,
		d->dashLineColor(0x000000, 0x0101));


	Layer *layer = chart->getLayer(0);
	int xIndex = layer->getXIndexOf(xValue);
	DataSet *dataSet = layer->getDataSet(0);
	double yValue = dataSet->getValue(xIndex);

	int color = dataSet->getDataColor();
	int yCoor = chart->getYCoor(dataSet->getPosition(xIndex), dataSet->getUseYAxis());


	TTFText *t;
	ostringstream label;
	//TTFText *t; = d->text(label.str().c_str(), "arial.ttf", 8);
	label << "<*font,bgColor=" << hex << color << "*> "
		<< chart->formatValue(xValue, "{value|,}") << " <*font*>";
	t = d->text(label.str().c_str(), "arial.ttf", 8);

	d->circle(xCoor, yCoor, 4, 4, color, color);

	if (xCoor <= (plotArea->getLeftX() + plotArea->getRightX()) / 2)
		t->draw(xCoor + 5, yCoor, 0xffffff, Chart::Left);
	else
		t->draw(xCoor - 5, yCoor, 0xffffff, Chart::Right);

	t->destroy();


}

//void CXRayCameraDlg::updateImageMap(CChartViewer *viewer)
//{
//	if (0 == viewer->getImageMapHandler())
//	{
//		// no existing image map - creates a new one
//		viewer->setImageMap(viewer->getChart()->getHTMLImageMap("clickable", "",
//			"title='x = {x}, y = {value}'"));
//	}
//}


HBRUSH CXRayCameraDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	switch (nCtlColor)
	{
	case CTLCOLOR_EDIT:
	{
		int editID = pWnd->GetDlgCtrlID();
		if (editID == IDC_EDIT_AUTOSCALE_LOW)
		{
			if (!isUpdatedLut)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_EDIT_AUTOSCALE_HIGH)
		{
			if (!isUpdatedLut)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_EDIT_CAMERA_ROI_X)
		{
			if (!isUpdatedCcdRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_EDIT_CAMERA_ROI_Y)
		{
			if (!isUpdatedCcdRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_EDIT_CAMERA_ROI_WIDTH)
		{
			if (!isUpdatedCcdRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_EDIT_CAMERA_ROI_HEIGHT)
		{
			if (!isUpdatedCcdRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_ROI_X_BINNING_EDIT)
		{
			if (!isUpdatedCcdRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_ROI_Y_BINNING_EDIT)
		{
			if (!isUpdatedCcdRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_EDIT_IMAGE_ROI_X)
		{
			if (!isUpdatedImageRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_EDIT_IMAGE_ROI_Y)
		{
			if (!isUpdatedImageRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_EDIT_IMAGE_ROI_WIDTH)
		{
			if (!isUpdatedImageRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
		else if (editID == IDC_EDIT_IMAGE_ROI_HEIGHT)
		{
			if (!isUpdatedImageRoi)
			{
				pDC->SetBkColor(PEACH);
			}
		}
	}
	}

	return hbr;
}


void CXRayCameraDlg::OnBnClickedCheckCorrectBackground()
{
	UpdateData(TRUE);
}


void CXRayCameraDlg::OnBnClickedCheckRotationCorrection()
{
	UpdateData(TRUE);
}







void CXRayCameraDlg::OnBnClickedButtonGrab()
{
	g_pLog->Display(0, _T("CXRayCameraDlg::OnBnClickedButtonGrab() 버튼 클릭!"));

	GetDlgItem(IDC_BUTTON_GRAB)->EnableWindow(FALSE);

	if (m_hDevice == NULL)
	{
		AfxMessageBox(_T("Device not connected."));
		return;
	}

	int grabResult = GrabTwice(TRUE);

	switch (grabResult)
	{
	case GRAB_OK:
		break;
	case GRAB_STOPPED:
		//AfxMessageBox(_T("Grab Stopped!"));
		break;
	case GRAB_TIMEOUT:
		AfxMessageBox(_T("Grab Timeout!"));
		break;
	default:
		break;
	}

	GetDlgItem(IDC_BUTTON_GRAB)->EnableWindow(TRUE);
}



void CXRayCameraDlg::OnBnClickedButtonSetImageRoi()
{
	UpdateData(TRUE);

	MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_POSITION_X, m_ImageRoiX);
	MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_POSITION_Y, m_ImageRoiY);
	MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_RECTANGLE_WIDTH, m_ImageRoiWidth);
	MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_RECTANGLE_HEIGHT, m_ImageRoiHeight);

	UpdateRoi(m_ImageRoiX, m_ImageRoiY, m_ImageRoiWidth, m_ImageRoiHeight);

	UpdateData(FALSE);
	isUpdatedImageRoi = TRUE;

	GetDlgItem(IDC_EDIT_IMAGE_ROI_X)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_Y)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_WIDTH)->Invalidate(FALSE);
	GetDlgItem(IDC_EDIT_IMAGE_ROI_HEIGHT)->Invalidate(FALSE);
}



void CXRayCameraDlg::OnBnClickedButtonXraySetMeasureBackground()
{
	// TODO: Add your control notification handler code here
//static TCHAR BASED_CODE szFilter[] = _T("이미지 파일(*.BMP, *.GIF, *.JPG) | *.BMP;*.GIF;*.JPG;*.bmp;*.jpg;*.gif |모든파일(*.*)|*.*||");

	char szFilter[] = "All Files(*.*) | *.* ||";
	CFileDialog dlg(TRUE, _T("*.*"), _T("*.mim"), OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, _T("*.*"), this);

	if (IDOK == dlg.DoModal())
	{
		if (m_MilImageBackGroundMeasure != M_NULL)
		{
			MbufFree(m_MilImageBackGroundMeasure);
			m_MilImageBackGroundMeasure = M_NULL;
		}

		CString pathName = dlg.GetPathName();
		MbufRestore(pathName, m_MilSystem, &m_MilImageBackGroundMeasure);
		m_MilImageBackGround = m_MilImageBackGroundMeasure;
	}

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SetMeasureBackGround();

}


void CXRayCameraDlg::OnBnClickedButtonXraySetAlignBackground()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SetAlignBackGround();
}


void CXRayCameraDlg::OnBnClickedButtonPhaseSaveMeasureBackground()
{
	//확인 필요 ihlee 21.03.29
	UpdateData(TRUE);
	SetMeasure(m_ExposureTime_ms);
	BOOL bCorrectBackGroundOld = m_bCorrectBackGround;
	m_bCorrectBackGround = FALSE;
	UpdateData(FALSE);

	g_pScanStage->Move_Origin();
	WaitSec(1);

	if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
	{
		return;
	}

	CFileDialog dlg(TRUE, _T("*.*"), _T("*.mim"), OFN_HIDEREADONLY, _T("*.*"), this);

	if (IDOK == dlg.DoModal())
	{
		CString pathName = dlg.GetPathName();
		MbufSave(pathName, m_MilImageOriginal);
	}

	//SetMeasureBackGround();
	m_bCorrectBackGround = bCorrectBackGroundOld;
	UpdateData(FALSE);

	//char* FileName = "C:\\EUVSolution\\Config\\MeasureBackGround.mim";
	//MbufSave(FileName, m_MilImageOriginal);	
}


void CXRayCameraDlg::OnBnClickedButtonPhaseSaveAlignBackground()
{

	//확인 필요 ihlee 21.03.29
	UpdateData(TRUE);
	SetAlign(m_ExposureTime_ms);
	BOOL bCorrectBackGroundOld = m_bCorrectBackGround;

	m_bCorrectBackGround = FALSE;
	UpdateData(FALSE);

	g_pScanStage->Move_Origin();
	WaitSec(1);

	if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
	{
		return;
	}

	char* FileName = "C:\\EUVSolution\\Config\\AlignBackGround.mim";
	MbufSave(FileName, m_MilImageOriginal);
	SetAlignBackGround();

	m_bCorrectBackGround = bCorrectBackGroundOld;
	UpdateData(FALSE);


	//char* FileName = "C:\\EUVSolution\\Config\\AlignBackGround.mim";
	//MbufSave(FileName, m_MilImageOriginal);
}
