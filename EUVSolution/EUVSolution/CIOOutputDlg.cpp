﻿// CIOOutputDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CDigitalOutput 대화 상자

IMPLEMENT_DYNAMIC(CDigitalOutput, CDialogEx)

CDigitalOutput::CDigitalOutput(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_OUTPUT_DIALOG, pParent)
{
}

CDigitalOutput::~CDigitalOutput()
{
	m_brush.DeleteObject();
	m_font.DeleteObject();
}		

void CDigitalOutput::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDigitalOutput, CDialogEx)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y000, &CDigitalOutput::OnBnClickedCheckDigitaloutY000)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y001, &CDigitalOutput::OnBnClickedCheckDigitaloutY001)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y002, &CDigitalOutput::OnBnClickedCheckDigitaloutY002)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y003, &CDigitalOutput::OnBnClickedCheckDigitaloutY003)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y004, &CDigitalOutput::OnBnClickedCheckDigitaloutY004)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y005, &CDigitalOutput::OnBnClickedCheckDigitaloutY005)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y006, &CDigitalOutput::OnBnClickedCheckDigitaloutY006)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y007, &CDigitalOutput::OnBnClickedCheckDigitaloutY007)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y008, &CDigitalOutput::OnBnClickedCheckDigitaloutY008)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y009, &CDigitalOutput::OnBnClickedCheckDigitaloutY009)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y010, &CDigitalOutput::OnBnClickedCheckDigitaloutY010)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y011, &CDigitalOutput::OnBnClickedCheckDigitaloutY011)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y012, &CDigitalOutput::OnBnClickedCheckDigitaloutY012)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y013, &CDigitalOutput::OnBnClickedCheckDigitaloutY013)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y014, &CDigitalOutput::OnBnClickedCheckDigitaloutY014)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y015, &CDigitalOutput::OnBnClickedCheckDigitaloutY015)
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CDigitalOutput 메시지 처리기

BOOL CDigitalOutput::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDigitalOutput::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDigitalOutput::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(0);
}

void CDigitalOutput::InitControls_DO(int nChannel)
{
	m_nChannel = nChannel;

	CString strTemp;
	CString Get_str;

	for (int nIdx = nChannel * DIGITAL_IO_VIEW_NUMBER, nCnt = 0; nIdx < (nChannel * DIGITAL_IO_VIEW_NUMBER) + DIGITAL_IO_VIEW_NUMBER; nIdx++, nCnt++)
	{
		strTemp.Format(_T("Y%04d"), nCnt + (nChannel * 100));
		SetDlgItemText(IDC_STATIC_DIGITALOUT_NUM0 + nCnt, strTemp);
		GetDlgItem(IDC_STATIC_DIGITALOUT_Y000 + nCnt)->SetWindowText(g_pConfig->m_chDo[nIdx]);
		GetDlgItem(IDC_STATIC_DIGITALOUT_Y000 + nCnt)->GetWindowText(Get_str);
		
		if ((Get_str == "EMPTY") || (Get_str == "")) GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nCnt)->EnableWindow(false);
		
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_Y000 + nCnt))->SetIcon(m_LedIcon[0]);

		CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nCnt);
		
		if (g_pIO->m_bDigitalOut[nIdx] == 1)
		{
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}
	}

	SetTimer(0, 100, NULL);
}

void CDigitalOutput::OnTimer(UINT_PTR nIDEvent)
{
	if (g_pIO->m_bCrevis_Open_Port == TRUE) OnUpdateDigitalOutput();

	CDialog::OnTimer(nIDEvent);
}

int CDigitalOutput::SetOutputBitOnOff(int nIndex)
{
	int write_state = 1;

	CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nIndex);

	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		int nCheck = btn->GetCheck();
		int SEL;

		if (nCheck)
			btn->SetWindowTextA(_T("On"));
		else
			btn->SetWindowTextA(_T("Off"));

		SEL = (m_nChannel * DIGITAL_IO_VIEW_NUMBER) + nIndex;

		write_state = WriteOutputBitOnOff(SEL, nCheck);
		if (write_state != 1)
		{
			AfxMessageBox("IO Write 실패 < Error Code ::" + write_state );
			g_pAlarm->SetAlarm((write_state));
		}

		return 0;
	}
	else
	{
		btn->SetWindowTextA(_T("Off"));
		btn->SetCheck(false);
		AfxMessageBox("IO 연결이 되어 있지 않습니다. 연결 확인 후 재 시도 해주세요");
		g_pAlarm->SetAlarm((IO_CONNECTION_ERROR));
		return IO_CONNECTION_ERROR;
	}

}

int CDigitalOutput::WriteOutputBitOnOff(int SEL, int nCheck)
{

	int ADDR =-1;
	int bitdex=-1;
	int nRet, nRet1 = 1;
	int state_return = 1;

	CString event_str, state_str, mode_str;

	CString str;
	CString str_nCheck;
	CString str_nRet;
	CString str_nRet1;


	event_str.Empty();
	state_str.Empty();
	mode_str.Empty();
	str.Empty();
	str_nCheck.Empty();
	str_nRet.Empty();
	str_nRet1.Empty();

	if (g_pIO->m_nIOMode == MAINT_MODE_ON)
	{
		mode_str = _T("Maint Mode");
		switch (SEL)
		{
		case 0: // SIGNAL TOWER LED
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 8;
			bitdex = SEL;
			event_str = _T("00. SIGNAL TOWER LED");
			
			break;
		case 1: // SIGNAL TOWER YELLOW
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 8;
			bitdex = SEL;
			event_str = _T("01. SIGNAL TOWER YELLOW");
			break;
		case 2: // SIGNAL TOWER GREEN
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 8;
			bitdex = SEL;
			event_str = _T("02. SIGNAL TOWER GREEN");
			break;
		case 3: // SIGNAL TOWER BUZZER
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 8;
			bitdex = SEL;
			event_str = _T("03. SIGNAL TOWER BUZZER");
			break;
		case 4: // EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 8;
			bitdex = SEL;
			break;
		case 5: // LL GATE_OPEN
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 8;
			bitdex = SEL;
			nRet = g_pIO->WriteOutputDataBit(8, 6, 0); // LL GATE CLOSE OFF
			event_str = _T("05. LLC Gate Open");

			break;
		case 6: // LL GATE _CLOSE
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 8;
			bitdex = SEL;
			nRet = g_pIO->WriteOutputDataBit(8, 5, 0); // LL GATE OPEN OFF
			event_str = _T("06. LLC Gate Close");
			break;
		case 7: //LLC SLOW ROUGH VALVE OPEN/CLOSE
			event_str = _T("07. LLC Slow Rough Valve Open/Close");
			if (nCheck)
			{
				if ((g_pIO->m_bDigitalIn[106]) == true)
				{
					str = "LLC ROUGH VALVE OPEN 불가 !! LLC FORELINE VALVE OPEN 되어 있습니다";
					::AfxMessageBox(str, MB_ICONSTOP);
				}
				else
				{
					g_pIO->m_bDigitalOut[SEL];
					ADDR = 8;
					bitdex = SEL;
				}
			}
			else
			{
				g_pIO->m_bDigitalOut[SEL];
				ADDR = 8;
				bitdex = SEL;
			}
			break;
		case 8: // LLC FAST ROUGH VALVE OPEN/CLOSE
			event_str = _T("08. LLC Fast Rough Valve Open/Close");
			if (nCheck)
			{
				if (((g_pIO->m_bDigitalIn[106]) == true) && ((g_pIO->m_bDigitalIn[107]) == false))
				{
					str = "LLC ROUGH VALVE OPEN 불가 !! LLC FORELINE VALVE OPEN 되어 있습니다.";
					::AfxMessageBox(str, MB_ICONSTOP);
				}
				else if (((g_pIO->m_bDigitalIn[106]) == false) && ((g_pIO->m_bDigitalIn[107]) == true))
				{
					g_pIO->m_bDigitalOut[SEL];
					ADDR = 9;
					bitdex = SEL - 8;
				}
				else
				{
					str = "LLC FORELINE VALVE 에러 발생";
					::AfxMessageBox(str, MB_ICONSTOP);
				}
			}
			else
			{
				g_pIO->m_bDigitalOut[SEL];
				ADDR = 9;
				bitdex = SEL - 8;
			}
			break;
		case 9: // FAST VENT INLET VALVE OPEN/CLOSE

			g_pIO->m_bDigitalOut[SEL];
			ADDR = 9;
			bitdex = SEL - 8;
			event_str = _T("09. Fast Vent Inlet Valve Opne/Close");
			break;
		case 10: // SLOW VENT INLET VALVE OPEN/CLOSE
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 9;
			bitdex = SEL - 8;
			event_str = _T("10. Slow Vent Inlet Valve Opne/Close");
			break;
		case 11: // MC SLOW ROUGH VALVE OPEN/CLOSE
			event_str = _T("11. MC Slow Rough Valve Opne/Close");
			if (nCheck)
			{
				if ((g_pIO->m_bDigitalIn[108] == true) && (g_pIO->m_bDigitalIn[109] == false))
				{
					str = "MC ROUGH OPEN 불가 !! MC FORELINE VALVE OPEN 되어 있습니다";
					::AfxMessageBox(str, MB_ICONSTOP);
				}
				else if ((g_pIO->m_bDigitalIn[108] == false) && (g_pIO->m_bDigitalIn[109] == true))
				{
					g_pIO->m_bDigitalOut[SEL];
					ADDR = 9;
					bitdex = SEL - 8;
				}
				else
				{
					str = "MC ROUGH OPEN 불가 !! MC FORELINE VALVE 상태가 이상 합니다. VAVLE 확인 해주세욤";
					::AfxMessageBox(str);
				}
			}
			else
			{
				g_pIO->m_bDigitalOut[SEL];
				ADDR = 9;
				bitdex = SEL - 8;
			}
			break;
		case 12: // MC FAST ROUTH VALVE OPEN/CLOSE
			event_str = _T("12. MC Fast Rough Valve Opne/Close");
			if (nCheck)
			{
				if ((g_pIO->m_bDigitalIn[108] == true) && (g_pIO->m_bDigitalIn[109] == false))
				{
					str = "MC ROUGH OPEN 불가 !! MC FORELINE VALVE OPEN 되어 있습니다";
					::AfxMessageBox(str, MB_ICONSTOP);
				}
				else if ((g_pIO->m_bDigitalIn[108] == false) && (g_pIO->m_bDigitalIn[109] == true))
				{
					g_pIO->m_bDigitalOut[SEL];
					ADDR = 9;
					bitdex = SEL - 8;
				}
				else
				{
					str = "MC FORELINE VALVE 에러 발생";
					::AfxMessageBox(str, MB_ICONSTOP);
				}
			}
			else
			{
				g_pIO->m_bDigitalOut[SEL];
				ADDR = 9;
				bitdex = SEL - 8;
			}
			break;
		case 13: // LL FORELIN VALVE OPEN/CLOSE
			event_str = _T("13. LLC Foreline Valve Opne/Close");
			if (nCheck)
			{
				if (g_pIO->m_bDigitalIn[102] == true)
				{
					g_pIO->m_bDigitalOut[SEL];
					ADDR = 9;
					bitdex = SEL - 8;
				}
				else
				{
					str = "LLC FORELINE OPEN 불가 !! LLC Rough Valve Open 되어 있습니다";
					::AfxMessageBox(str, MB_ICONSTOP);
				}
			}
			else
			{
				g_pIO->m_bDigitalOut[SEL];
				ADDR = 9;
				bitdex = SEL - 8;
			}
			break;
		case 14: // MC FORELINE VALVE OPEN/CLOSE
			event_str = _T("14. MC Foreline Valve Opne/Close");
			if (nCheck)
			{
				if (g_pIO->m_bDigitalIn[105] == true)
				{
					g_pIO->m_bDigitalOut[SEL];
					ADDR = 9;
					bitdex = SEL - 8;
				}
				else
				{
					str = "MC FORELINE OPEN 불가 !! MC Rough Valve Open 되어 있습니다";
					::AfxMessageBox(str, MB_ICONSTOP);
				}
			}
			else
			{
				g_pIO->m_bDigitalOut[SEL];
				ADDR = 9;
				bitdex = SEL - 8;
			}
			break;
		case 15: // EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 9;
			bitdex = SEL - 8;
			break;
		case 16:// EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 10;
			bitdex = SEL - 16;
			break;
		case 17:// EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 10;
			bitdex = SEL - 16;
			break;
		case 18:// EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 10;
			bitdex = SEL - 16;
			break;
		case 19: // FAST VENT OUTLET OPEN/CLOSE
			event_str = _T("19. Fast Vent Outlet Valve Opne/Close");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 10;
			bitdex = SEL - 16;
			break;
		case 20: //  SLOW VENT OUTLET OPEN/CLOSE
			event_str = _T("20. Slow Vent Outlet Valve Opne/Close");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 10;
			bitdex = SEL - 16;
			break;
		case 21: // EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 10;
			bitdex = SEL - 16;
			break;
		case 22: // LL TMP GATE VALVE OPEN/CLOSE
			event_str = _T("22. LLC Tmp Gate Valve Opne/Close");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 10;
			bitdex = SEL - 16;
			break;
		case 23: // MC TMP GATE VALVE OPEN/CLOSE
			event_str = _T("23. MC Tmp Gate Valve Opne/Close");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 10;
			bitdex = SEL - 16;
			break;
		case 24: // EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 11;
			bitdex = SEL - 24;
			break;
		case 25: // TR GATE OPEN
			event_str = _T("25. TR Gate Valve Opne");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 11;
			bitdex = SEL - 24;
			nRet = g_pIO->WriteOutputDataBit(11, 2, 0);
			break;
		case 26: // TR GATE CLOSE
			event_str = _T("26. TR Gate Valve Close");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 11;
			bitdex = SEL - 24;
			nRet = g_pIO->WriteOutputDataBit(11, 1, 0);
			break;
		case 27: //EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 11;
			bitdex = SEL - 24;
			break;
		case 28: //DOOR_COVER#1
			event_str = _T("28. Door Cover #1");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 11;
			bitdex = SEL - 24;
			break;
		case 29: //DOOR COVER#2
			event_str = _T("29. Door Cover #2");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 11;
			bitdex = SEL - 24;
			break;
		case 30: //DOOR COVER#3
			event_str = _T("30. Door Cover #3");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 11;
			bitdex = SEL - 24;
			break;
		case 31://DOOR COVER#4
			event_str = _T("31. Door Cover #4");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 11;
			bitdex = SEL - 24;
			break;
		case 32: //DRY PUMP #1 ON/OFF
			event_str = _T("32. LLC Dry Pump #1 On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 12;
			bitdex = SEL - 32;
			break;
		case 33: //DRY PUMP #2 ON / OFF
			event_str = _T("33. MC Dry Pump #1 On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 12;
			bitdex = SEL - 32;
			break;
		case 34:// EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 12;
			bitdex = SEL - 32;
			break;
		case 35: //COVER DOOR OPEN/CLOSE
			event_str = _T("35. Cover Door Open/Close");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 12;
			bitdex = SEL - 32;
			break;
		case 36: // LL CCD CAMERA ON/OFF
			event_str = _T("36. LLC Ccd Camera On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 12;
			bitdex = SEL - 32;
			break;
		case 37: // DVR#1 LAMP ON/OFF
			event_str = _T("37. DVR#1 Lamp On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 12;
			bitdex = SEL - 32;
			break;
		case 38: // DVR#2 LAMP ON/OFF
			event_str = _T("38. DVR#2 Lamp On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 12;
			bitdex = SEL - 32;
			break;
		case 39:  // DVR#\3 LAMP ON/OFF
			event_str = _T("39. DVR#3 Lamp On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 12;
			bitdex = SEL - 32;
			break;
		case 40: // DVR#4 LAMP ON/OFF
			event_str = _T("40. DVR#4 Lamp On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 13;
			bitdex = SEL - 40;
			break;
		case 41: // EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 13;
			bitdex = SEL - 40;
			break;
		case 42: // SHUTTER OPEN / SOURCE 부
			event_str = _T("42. Sutter Open On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 13;
			bitdex = SEL - 40;
			break;
		case 43: // EMPTY
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 13;
			bitdex = SEL - 40;
			break;
		case 44: // WATER SUPPLY VALVE ON/OFF
			event_str = _T("44. Water Supply Valve On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 13;
			bitdex = SEL - 40;
			break;
		case 45: // WATER RETURN VALVE ON/OFF
			event_str = _T("45. Water Return Valve On/Off");
			g_pIO->m_bDigitalOut[SEL];
			ADDR = 13;
			bitdex = SEL - 40;
			break;
		case 46: // EMPTY
			g_pIO->m_bDigitalOut[SEL];
			bitdex = SEL - 40;
			ADDR = 13;
			break;
		case 47: // CAM SEOSOR TRIGGER
			event_str = _T("47. Cam Sensor Trigger");
			g_pIO->m_bDigitalOut[SEL];
			bitdex = SEL - 40;
			ADDR = 13;
			break;
		default:
			break;
		}
		

		nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck);
		
		//////////////////////////////////////////
		// CAMSENSOR TRIGGER IO SIGNAL CHECK
		/////////////////////////////////////////
		if (SEL == 47)
		{
			if (!nRet)
			{
				switch (nCheck)
				{
				case 0:
					//g_pIO->m_bDigitalOut[47] = false
					g_pIO->m_bCamsensor_Trigger_On = false;
					break;
				case 1:
					//g_pIO->m_bDigitalOut[47] = true
					g_pIO->m_bCamsensor_Trigger_On = true;
					break;
				default:
					break;
				}
			}
		}
		
		
		if (nCheck)
		{
			state_str = _T("On/Open");
		}
		else
		{
			state_str = _T("Off/Close");
		}
		

		str_nRet.Format(_T("%d"), nRet);
		g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ "+ mode_str + " ] "+ event_str + " 버튼 " + state_str + " Click Evnet")));	//IO 버튼 Event 상태 기록.
		g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " Click Evnet Return Value :: " + str_nRet)));
		g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " Click Evnet");
		g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " Click Evnet Return Value :: " + str_nRet);

		if (nRet != 0)
		{
			::AfxMessageBox("Write 실패! ", MB_ICONSTOP);
			g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " 실행 실패 ")));	//IO 버튼 Event 결과 상태 기록.
			g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " Click Evnet ====== 실행 실패 ");
			switch (SEL)
			{
				case 0: // SIGNAL TOWER LED
					state_return = -87010;
					break;
				case 1: // SIGNAL TOWER YELLOW
					state_return = -87011;
					break;
				case 2: // SIGNAL TOWER GREEN
					state_return = -87012;
					break;
				case 3: // SIGNAL TOWER BUZZER
					state_return = -87013;
					break;
				case 4: // EMPTY
					break;
				case 5: // LLC GATE_OPEN
					state_return = -87014;
					break;
				case 6: // LLC GATE _CLOSE
					state_return = -87015;
					break;
				case 7: //LLC SLOW ROUGH VALVE OPEN/CLOSE
					state_return = -87016;
					break;
				case 8: // LLC FAST ROUGH VALVE OPEN/CLOSE
					state_return = -87017;
					break;
				case 9: // FAST VENT INLET VALVE OPEN/CLOSE
					state_return = -87018;
					break;
				case 10: // SLOW VENT INLET VALVE OPEN/CLOSE
					state_return = -87019;
					break;
				case 11: // MC SLOW ROUGH VALVE OPEN/CLOSE
					state_return = -87020;
					break;
				case 12: // MC FAST ROUTH VALVE OPEN/CLOSE
					state_return = -87021;
					break;
				case 13: // LL FORELIN VALVE OPEN/CLOSE
					state_return = -87022;
					break;
				case 14: // MC FORELINE VALVE OPEN/CLOSE
					state_return = -87023;
					break;
				case 15: // EMPTY
					break;
				case 16:// EMPTY
					break;
				case 17:// EMPTY
					break;
				case 18:// EMPTY
					break;
				case 19: // FAST VENT OUTLET OPEN/CLOSE
					state_return = -87024;
					break;
				case 20: //  SLOW VENT OUTLET OPEN/CLOSE
					state_return = -87025;
					break;
				case 21: // EMPTY
					break;
				case 22: // LLC TMP GATE VALVE OPEN/CLOSE
					state_return = -87026;
					break;
				case 23: // MC TMP GATE VALVE OPEN/CLOSE
					state_return = -87027;
					break;
				case 24: // EMPTY
					break;
				case 25: // TR GATE OPEN
					state_return = -87028;
					break;
				case 26: // TR GATE CLOSE
					state_return = -87029;
					break;
				case 27: // EMPTY
					break;
				case 28: //DOOR COVER #1
					state_return = -87030;
					break;
				case 29: //DOOR COVER #2
					state_return = -87031;
					break;
				case 30: //DOOR COVER #3
					state_return = -87032;
					break;
				case 31://DOOR COVER #4
					state_return = -87033;
					break;
				case 32: //DRY PUMP #1 ON/OFF
					state_return = -87034;
					break;
				case 33: //DRY PUMP #2 ON / OFF
					state_return = -87035;
					break;
				case 34:// EMPTY
					break;
				case 35: //COVER DOOR OPEN/CLOSE
					state_return = -87036;
					break;
				case 36: // LL CCD CAMERA ON/OFF
					state_return = -87037;
					break;
				case 37: // DVR#1 LAMP ON/OFF
					state_return = -87038;
					break;
				case 38: // DVR#2 LAMP ON/OFF
					state_return = -87039;
					break;
				case 39:  // DVR#\3 LAMP ON/OFF
					state_return = -87040;
					break;
				case 40: // DVR#4 LAMP ON/OFF
					state_return = -87041;
					break;
				case 41: // EMPTY
					break;
				case 42: // SHUTTER OPEN / SOURCE 부
					state_return = -87042;
					break;
				case 43: // EMPTY
					break;
				case 44: // WATER SUPPLY VALVE ON/OFF
					state_return = -87043;
					break;
				case 45: // WATER RETURN VALVE ON/OFF
					state_return = -87044;
					break;
				case 46: // EMPTY
					break;
				case 47: // CAM SEOSOR TRIGGER
					state_return = -87045;
					break;
				default:
					break;
			}

		}
		else
		{
			g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " 실행 완료 ")));	//IO 버튼 Event 결과 상태 기록.
			g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + state_str + " Click Evnet ====== 실행 완료 ");
		}


	}
	else if (g_pIO->m_nIOMode == OPER_MODE_ON)
	{
	mode_str = _T("Oper Mode");
		if (nCheck) str_nCheck = _T(" On 또는 Open");
		else str_nCheck = _T(" Off 또는 Close");

		if (g_pIO->Water_Valve_Open() == VALVE_OPENED)
		{
			switch (SEL)
			{
			case 0: // SIGNAL TOWER LED
				str = "SIGNAL TOWER LED";
				event_str = _T("00. SIGNAL TOWER LED");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (g_pIO->Str_ok_Box(event_str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
					ADDR = 8;
					bitdex = SEL;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
						state_return = -87010;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");

					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 1: // SIGNAL TOWER YELLOW
				str = "SIGNAL TOWER YELLO";
				event_str = _T("01. Signal Tower Yellow");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
					ADDR = 8;
					bitdex = SEL;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
						state_return = -87011;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 2: // SIGNAL TOWER GREEN
				str = "SIGNAL TOWER GREEN";
				event_str = _T("02. Signal Tower Green");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
					ADDR = 8;
					bitdex = SEL;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87012;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
					
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 3: // SIGNAL TOWER BUZZER
				str = "SIGNAL TOWER BUZZER";
				event_str = _T("03. Signal Tower Buzzer");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					ADDR = 8;
					bitdex = SEL;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);

					if (nRet != 0) 
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87013;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}

				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 4: // EMPTY
				str = "EMPTY";
				ADDR = 8;
				bitdex = SEL;
				break;
			case 5: // LLC GATE_OPEN
				str = "LLC GATE OPEN";
				event_str = _T("05. LLC Gate ");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
						if (g_pIO->Open_LLCGateValve() == OPERATION_COMPLETED)
						{
							ADDR = 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet1 = g_pIO->WriteOutputDataBit(ADDR, 6, 0); // LLC GATE CLOSE OFF

							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							bitdex = SEL;
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						
							str_nRet.Format(_T("%d"), nRet);
							str_nRet1.Format(_T("%d"), nRet1);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet + " , " + str_nRet1)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet + " , " + str_nRet1 );


							if ((nRet != 0) || (nRet1 != 0))
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
								state_return = -87014;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else g_pLog->Display(0, g_pIO->Log_str);
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSED)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						ADDR = 8;
						bitdex = SEL;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0) 
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87014;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 6: // LLC GATE _CLOSE
				str = "LLC GATE CLOSE";
				event_str = _T("06. LLC Gate ");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
						if (g_pIO->Close_LLCGateValve() == OPERATION_COMPLETED)
						{
							ADDR = 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet1 = g_pIO->WriteOutputDataBit(ADDR, 5, 0); // LLC GATE OPEN OFF
							str_nRet1.Format(_T("%d"), nRet1);
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet1);
							bitdex = SEL;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet + " , " + str_nRet1);
							if ((nRet1 != 0) || (nRet != 0))
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87015;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else g_pLog->Display(0, g_pIO->Log_str);
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						ADDR = 8;
						bitdex = SEL;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87015;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 7: //LLC SLOW ROUGH VALVE OPEN/CLOSE
				str = "LLC SLOW ROUGH VALVE";
				event_str = _T("07. LLC Slow Rough Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Open_LLC_SlowRoughValve() == OPERATION_COMPLETED)
						{
							ADDR = 8;
							bitdex = SEL;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
								state_return = -87016;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else g_pLog->Display(0, g_pIO->Log_str);
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						if (g_pIO->Close_LLC_SlowRoughValve() == OPERATION_COMPLETED)
						{
							ADDR = 8;
							bitdex = SEL;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
								state_return = -87016;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else g_pLog->Display(0, g_pIO->Log_str);
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 8: // LLC FAST ROUGH VALVE OPEN/CLOSE
				str = "LLC FAST ROUGH VALVE";
				event_str = _T("08. LLC Fast Rough Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)  // LLC FAST ROUGH VALVE OPEN
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Open_LLC_FastRoughValve() == OPERATION_COMPLETED)
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87017;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE) // LLC FAST ROUGH VALVE CLOSE
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						if (g_pIO->Close_LLC_FastRoughValve() == OPERATION_COMPLETED)
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87017;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 9: // FAST VENT INLET VALVE OPEN/CLOSE
				str = "FAST VENT INLET VALVE";
				event_str = _T("09. Fast Vent Inlet Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN) // FAST VENT INLET VALVE OPEN
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Open_FastVent_Inlet_Valve1() == OPERATION_COMPLETED)
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
								state_return = -87018;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE) // FAST VENT INLET VALVE CLOSE
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						if (g_pIO->Close_FastVent_Inlet_Valve() == OPERATION_COMPLETED)
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
								state_return = -87018;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 10: // SLOW VENT INLET VALVE OPEN/CLOSE
				str = "SLOW VENT INLET VALVE";
				event_str = _T("10. Slow Vent Inlet Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Open_SlowVent_Inlet_Valve1() == OPERATION_COMPLETED) // SLOW VENT INLET VALVE OPEN
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87019;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Close_SlowVent_Inlet_Valve1() == OPERATION_COMPLETED) // SLOW VENT INLET VALVE CLOSE
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87019;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 11: // MC SLOW ROUGH VALVE OPEN/CLOSE
				str = "MC SLOW ROUGH VALVE";
				event_str = _T("11. MC Slow Rough Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Open_MC_SlowRoughValve() == OPERATION_COMPLETED) // MC SLOW ROUGH VALVE OPEN
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87020;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						if (g_pIO->Close_MC_SlowRoughValve() == OPERATION_COMPLETED) // MC SLOW ROUGH VALVE CLOSE
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87020;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 12: // MC FAST ROUGH VALVE OPEN/CLOSE
				str = "MC FAST ROUGH VALVE";
				event_str = _T("12. MC Fast Rough Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Open_MC_FastRoughValve() == OPERATION_COMPLETED) // MC FAST ROUGH VALVE OPEN
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87021;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						if (g_pIO->Close_MC_FastRoughValve() == OPERATION_COMPLETED) // MC FAST ROUGH VALVE CLOSE
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87021;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 13: // LLC FORELINE VALVE OPEN/CLOSE
				str = "LLC FORELINE VALVE";
				event_str = _T("13. LLC Foreline Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Open_LLC_TMP_ForelineValve() == OPERATION_COMPLETED) // LLC FORELINE VALVE OPEN
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87022;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Close_LLC_TMP_ForelineValve() == OPERATION_COMPLETED) // LLC FORELINE VALVE CLOSE
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0) 
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87022;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 14: // MC FORELINE VALVE OPEN/CLOSE
				str = "MC FORELINE VALVE";
				event_str = _T("14. MC Foreline Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
						if (g_pIO->Open_MC_TMP1_ForelineValve() == OPERATION_COMPLETED) // MC FORELINE VALVE OPEN
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87023;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
						if (g_pIO->Close_MC_TMP1_ForelineValve() == OPERATION_COMPLETED) // MC FORELINE VALVE CLOSE
						{
							ADDR = 9;
							bitdex = SEL - 8;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
								state_return = -87023;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 15: // EMPTY
				str = "EMPTY";
				ADDR = 9;
				bitdex = SEL - 8;
				break;
			case 16:// EMPTY
				str = "EMPTY";
				ADDR = 10;
				bitdex = SEL - 16;
				break;
			case 17:// EMPTY
				str = "EMPTY";
				ADDR = 10;
				bitdex = SEL - 16;
				break;
			case 18:// EMPTY
				str = "EMPTY";
				ADDR = 10;
				bitdex = SEL - 16;
				break;
			case 19: // FAST VENT OUTLET OPEN/CLOSE
				str = "FAST VENT OUTLET";
				event_str = _T("19. Fast Vent Outlet Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
						if (g_pIO->Open_FastVent_Outlet_Valve1() == OPERATION_COMPLETED) // FAST VENT OUTLET OPEN
						{
							ADDR = 10;
							bitdex = SEL - 16;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87024;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
						if (g_pIO->Close_FastVent_Outlet_Valve() == OPERATION_COMPLETED) // FAST VENT OUTLET CLOSE
						{
							ADDR = 10;
							bitdex = SEL - 16;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87024;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 20: //  SLOW VENT OUTLET OPEN/CLOSE
				str = "SLOW VENT OUTLET";
				event_str = _T("20. Slow Vent Outlet Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
						if (g_pIO->Open_SlowVent_Outlet_Valve1() == OPERATION_COMPLETED) // SLOW VENT OUTLET OPEN
						{
							ADDR = 10;
							bitdex = SEL - 16;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87025;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Close_SlowVent_Outlet_Valve1() == OPERATION_COMPLETED) // SLOW VENT OUTLET CLOSE
						{
							ADDR = 10;
							bitdex = SEL - 16;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
								state_return = -87025;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 21: // EMPTY
				str = "EMPTY";
				ADDR = 10;
				bitdex = SEL - 16;
				break;
			case 22: // LLC TMP GATE VALVE OPEN/CLOSE
				str = "LLC TMP GATE VALVE";
				event_str = _T("22. LLC Tmp Gate Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
						if (g_pIO->Open_LLC_TMP_GateValve() == OPERATION_COMPLETED) // LLC TMP GATE VALVE OPEN
						{
							ADDR = 10;
							bitdex = SEL - 16;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
								state_return = -87026;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
						if (g_pIO->Close_LLC_TMP_GateValve() == OPERATION_COMPLETED) // LLC TMP GATE VALVE CLOSE
						{
							ADDR = 10;
							bitdex = SEL - 16;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87026;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 23: // MC TMP GATE VALVE OPEN/CLOSE
				str = "MC TMP GATE VALVE";
				event_str = _T("23. MC Tmp Gate Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Open_MC_TMP1_GateValve() == OPERATION_COMPLETED) // MC TMP GATE VALVE OPEN
						{
							ADDR = 10;
							bitdex = SEL - 16;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87027;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Close_MC_TMP1_GateValve() == OPERATION_COMPLETED) // MC TMP GATE VALVE CLOSE
						{
							ADDR = 10;
							bitdex = SEL - 16;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87027;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 24: // EMPTY
				str = "EMPTY";
				ADDR = 11;
				bitdex = SEL - 24;
				break;
			case 25: // TR GATE OPEN
				str = "TR GATE OPEN VALVE";
				event_str = _T("25. TR Gate Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Open_TRGateValve() == OPERATION_COMPLETED) // TR GATE OPEN ON
						{
							ADDR = 11;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet1 = g_pIO->WriteOutputDataBit(ADDR, 2, 0);
							str_nRet1.Format(_T("%d"), nRet1);
							bitdex = SEL - 24;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet + " , " + str_nRet1)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet + " , " + str_nRet1);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87028;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE) // TR GATE OPEN OFF
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87028;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							if (g_pIO->Close_TRGateValve() == OPERATION_COMPLETED)
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료에 따른 TR GATE CLOSE 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료에 따른 TR GATE CLOSE 완료");
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료에 따른 TR GATE CLOSE 실패 (ERROR)")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료에 따른 TR GATE CLOSE 실패 (ERROR)");
							}
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 26: // TR GATE CLOSE
				str = "TR GATE CLOSE VALVE";
				event_str = _T("26. TR Gate Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						if (g_pIO->Close_TRGateValve() == OPERATION_COMPLETED) // TR GATE CLOSE ON
						{
							ADDR = 11;
							bitdex = SEL - 24;
							//////////////////////////////////////////////////////////////////////////////////
							//IO WRITE
							//////////////////////////////////////////////////////////////////////////////////
							nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
							str_nRet.Format(_T("%d"), nRet);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
							if (nRet != 0)
							{
								::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
								state_return = -87029;
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
							}
						}
						else
						{
							g_pLog->Display(0, g_pIO->Log_str);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + g_pIO->Log_str)));	//IO 버튼 Event 상태 기록.
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87029;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							if (g_pIO->Open_TRGateValve() == OPERATION_COMPLETED)
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료에 따른 TR GATE OPEN 완료")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료에 따른 TR GATE OPEN 완료");
							}
							else
							{
								g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료에 따른 TR GATE OPEN 실패 (ERROR)")));	//IO 버튼 Event 상태 기록.
								g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료에 따른 TR GATE OPEN 실패 (ERROR)");
							}
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 27: //EMPTY
				str = "EMPTY";
				ADDR = 11;
				bitdex = SEL - 24;
				break;
			case 28: // DOOR COVER #1
				str = "DOOR COVER #1";
				event_str = _T("28. Door Cover #1 ");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87030;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87030;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 29: //DOOR COVER #2
				str = "DOOR COVER #2";
				//ADDR = 11;
				//bitdex = SEL - 24;
				event_str = _T("29. Door Cover #2 ");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87031;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
							state_return = -87031;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 30: //DOOR COVER #3
				str = "DOOR COVER #3";
				//ADDR = 11;
				//bitdex = SEL - 24;
				event_str = _T("30. Door Cover #3 ");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87032;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87032;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 31://DOOR COVER #4
				str = "DOOR COVER #4";
				//ADDR = 11;
				//bitdex = SEL - 24;
				event_str = _T("31. Door Cover #4");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (nCheck == VALVE_OPEN)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
							state_return = -87033;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				else if (nCheck == VALVE_CLOSE)
				{
					if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
						
						ADDR = 11;
						bitdex = SEL - 24;
						//////////////////////////////////////////////////////////////////////////////////
						//IO WRITE
						//////////////////////////////////////////////////////////////////////////////////
						nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
						str_nRet.Format(_T("%d"), nRet);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
						if (nRet != 0)
						{
							::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
							state_return = -87033;
						}
						else
						{
							g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
							g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
						}
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
					}
				}
				break;
			case 32: //DRY PUMP #1 ON/OFF
				str = "DRY PUMP #1";
				event_str = _T("32. Dry Pump #1");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 12;
					bitdex = SEL - 32;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87034;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 33: //DRY PUMP #2 ON / OFF
				str = "DRY PUMP #2";
				event_str = _T("33. Dry Pump #2");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 12;
					bitdex = SEL - 32;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87035;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 34:// EMPTY
				str = "EMPTY";
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					ADDR = 12;
					bitdex = SEL - 32;
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					if (nRet) ::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
				}
				break;
			case 35: //COVER DOOR OPEN/CLOSE
				str = "COVER DOOR";
				event_str = _T("35. Cover Door");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 12;
					bitdex = SEL - 32;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87036;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 36: // LLC CCD CAMERA ON/OFF
				str = "LLC CCD CAMERA";
				event_str = _T("36. LLC Ccd Camera");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 12;
					bitdex = SEL - 32;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
						state_return = -87037;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 37: // DVR#1 LAMP ON/OFF
				str = "DVR#\1 LAMP";
				event_str = _T("37. DVR#1 Lamp");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					ADDR = 12;
					bitdex = SEL - 32;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87038;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 38: // DVR#2 LAMP ON/OFF
				str = "DVR#\2 LAMP";
				event_str = _T("38. DVR#2 Lamp");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 12;
					bitdex = SEL - 32;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
						state_return = -87039;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 39:  // DVR#\3 LAMP ON/OFF
				str = "DVR#\3 LAMP";
				event_str = _T("39. DVR#3 Lamp");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)");
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 12;
					bitdex = SEL - 32;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패");
						state_return = -87040;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 40: // DVR#4 LAMP ON/OFF
				str = "DVR#4 LAMP";
				event_str = _T("40. DVR#4 Lamp");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 13;
					bitdex = SEL - 40;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87041;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 41: // EMPTY
				str = "EMPTY";
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					ADDR = 13;
					bitdex = SEL - 40;
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					if (nRet) ::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
				}
				break;
			case 42: // SHUTTER OPEN / SOURCE 부
				str = " SHUTTER OPEN / SOURCE 부";
				event_str = _T("42. Sutter (Source)");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 13;
					bitdex = SEL - 40;
					if (g_pIO->Is_SourceGate_OpenOn_Check() != VALVE_CLOSED)
					{
						nCheck = 1;
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Source Gate Open 가능")));	//IO 버튼 Event 상태 기록.
		
					}
					else
					{
						nCheck = 0;
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Source Gate Open 불가능 (MC 진공도 확인)")));	//IO 버튼 Event 상태 기록.
					}
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87042;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 43: // EMPTY
				str = "EMPTY";
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					ADDR = 13;
					bitdex = SEL - 40;
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					if (nRet) ::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
				}
				break;
			case 44: // WATER SUPPLY VALVE ON/OFF
				str = " WATER SUPPLY VALVE";
				event_str = _T("44. Water Supply Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 13;
					bitdex = SEL - 40;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87043;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 45: // WATER RETURN VALVE ON/OFF
				str = " WATER RETURN VALVE";
				event_str = _T("45. Water Return Valve");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)"); 
					
					ADDR = 13;
					bitdex = SEL - 40;
					//////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87044;
					}
					else
					{
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			case 46: // EMPTY
				str = "EMPTY";
				ADDR = 13;
				bitdex = SEL - 40;
				break;
			case 47:  // CAM SEOSOR TRIGGER
				str = "CAM SEOSOR TRIGGER";
				event_str = _T("47. Cam Sensor Trigger");
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet")));	//IO 버튼 Event 상태 기록.
				g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes / No)"); 
				if (g_pIO->Str_ok_Box(str, str_nCheck) == RUN)
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (Yes)");
					ADDR = 13;
					bitdex = SEL - 40;
					///////////////////////////////////////////////////////////////////////////////////
					//IO WRITE
					//////////////////////////////////////////////////////////////////////////////////
					nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck); // 정상 WRITE 면 retrun 0 임.
					str_nRet.Format(_T("%d"), nRet);
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet)));
					g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " Click Evnet Return Value :: " + str_nRet);
					if (nRet != 0)
					{
						::AfxMessageBox(str + str_nCheck + "Write 실패! ", MB_ICONSTOP);
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 실패"); 
						state_return = -87045;

					}
					else
					{
						switch (nCheck)
						{
						case 0:
							//g_pIO->m_bDigitalOut[47] = false
							g_pIO->m_bCamsensor_Trigger_On = false;
							break;
						case 1:
							//g_pIO->m_bDigitalOut[47] = true
							g_pIO->m_bCamsensor_Trigger_On = true;
							break;
						default:
							break;
						}
						g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료")));	//IO 버튼 Event 상태 기록.
						g_pLog->Display(0, "EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 완료");
					}
				}
				else
				{
					g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str + " 버튼 " + str_nCheck + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
				}
				break;
			default:
				break;
			}
		}
		else
		{
			event_str = " Water_Valve_Open Error 발생 [ Water Supply, Return On 확인 ]";
			g_pIO->CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + mode_str + " ] " + event_str)));	//IO 버튼 Event 상태 기록.
			::AfxMessageBox(str, MB_ICONSTOP);
		}
	}

	if(!state_return) g_pAlarm->SetAlarm((state_return));
	return state_return;
	//return nRet;
}




bool CDigitalOutput::OnUpdateDigitalOutput()
{
	//for (int nIdx = m_nChannel * DIGITAL_IO_VIEW_NUMBER, nCnt = 0; nIdx < (m_nChannel * DIGITAL_IO_VIEW_NUMBER) + DIGITAL_IO_VIEW_NUMBER; nIdx++, nCnt++)
	//{
	//	if (g_pIO->m_bDigitalOut[nIdx])
	//		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_Y000 + nCnt))->SetIcon(m_LedIcon[1]);
	//	else
	//		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_Y000 + nCnt))->SetIcon(m_LedIcon[0]);
	//}


	for (int nIdx = m_nChannel * DIGITAL_IO_VIEW_NUMBER, nCnt = 0; nIdx < (m_nChannel * DIGITAL_IO_VIEW_NUMBER) + DIGITAL_IO_VIEW_NUMBER; nIdx++, nCnt++)
	{
		if (g_pIO->m_bDigitalOut[nIdx])
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_Y000 + nCnt))->SetIcon(m_LedIcon[1]);
			CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nCnt);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_Y000 + nCnt))->SetIcon(m_LedIcon[0]);
			CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nCnt);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}
	}

	return TRUE;
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY000()
{
	SetOutputBitOnOff(0);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY001()
{
	SetOutputBitOnOff(1);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY002()
{
	SetOutputBitOnOff(2);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY003()
{
	SetOutputBitOnOff(3);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY004()
{
	SetOutputBitOnOff(4);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY005()
{
	SetOutputBitOnOff(5);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY006()
{
	SetOutputBitOnOff(6);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY007()
{
	SetOutputBitOnOff(7);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY008()
{
	SetOutputBitOnOff(8);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY009()
{
	SetOutputBitOnOff(9);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY010()
{
	SetOutputBitOnOff(10);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY011()
{
	SetOutputBitOnOff(11);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY012()
{
	SetOutputBitOnOff(12);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY013()
{
	SetOutputBitOnOff(13);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY014()
{
	SetOutputBitOnOff(14);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY015()
{
	SetOutputBitOnOff(15);
}

HBRUSH CDigitalOutput::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_TEXT)
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
	}

	return hbr;
}
