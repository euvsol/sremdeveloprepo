﻿// CIOInputDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

#define DRY_NO_ERROR		0
#define DRY_LLC_ALARM		1
#define DRY_MC_ALARM		2
#define DRY_LLC_WARNING		3
#define DRY_MC_WARNING		4

// CDigitalInput 대화 상자

IMPLEMENT_DYNAMIC(CDigitalInput, CDialogEx)

CDigitalInput::CDigitalInput(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_INPUT_DIALOG, pParent)
{
}

CDigitalInput::~CDigitalInput()
{
	m_brush.DeleteObject();
	m_font.DeleteObject();
}

void CDigitalInput::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDigitalInput, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CDigitalInput 메시지 처리기

BOOL CDigitalInput::OnInitDialog()
{
	CDialogEx::OnInitDialog(); 
	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);


	cnt_llc_dry_pump_on = 0;
	cnt_llc_dry_pump_alarm = 0;
	cnt_llc_dry_pump_warning = 0;
	cnt_mc_dry_pump_on = 0;
	cnt_mc_dry_pump_alarm = 0;
	cnt_mc_dry_pump_warning = 0;
	cnt_llc_tmp_water_leak = 0;
	cnt_mc_tmp_water_leak = 0;
	cnt_smoke_sensor = 0;
	cnt_water_return_temp_alarm = 0;
	cnt_main_water_alarm = 0;
	cnt_main_air_alarm = 0;

	Dry_Pump_State = DRY_NO_ERROR;

	m_Message_ThreadExitFlag = FALSE;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDigitalInput::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDigitalInput::OnDestroy()
{
	CDialogEx::OnDestroy();
	
	KillTimer(INPUT_DIALOG_TIMER);
}

void CDigitalInput::InitControls_DI(int nChannel)
{
	
	m_nChannel = nChannel;

	CString strTemp;

	for (int nIdx = nChannel * DIGITAL_IO_VIEW_NUMBER, nCnt = 0; nIdx < (nChannel * DIGITAL_IO_VIEW_NUMBER) + DIGITAL_IO_VIEW_NUMBER; nIdx++, nCnt++)
	{
		strTemp.Format(_T("X%04d"), nCnt + (nChannel * 100));
		SetDlgItemText(IDC_STATIC_DIGITALIN_NUM0 + nCnt, strTemp);
		GetDlgItem(IDC_STATIC_DIGITALIN_X000 + nCnt)->SetWindowText(g_pConfig->m_chDi[nIdx]);
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
	}

	SetTimer(INPUT_DIALOG_TIMER, 100, NULL);

}

void CDigitalInput::OnTimer(UINT_PTR nIDEvent)
{

	KillTimer(nIDEvent);

	if (nIDEvent == INPUT_DIALOG_TIMER)
	{
		//if (g_pIO->m_Crevis_Open_Port == TRUE)
		{
			OnUpdateDigitalInput();
			SetTimer(INPUT_DIALOG_TIMER, 100, NULL);
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}

///////////////////////////
// Digital Input Read /////
///////////////////////////

bool CDigitalInput::OnUpdateDigitalInput(void)
{
	CString value_io_str;
	CString cnt_io_str;
	int ErrorCode = 0;
	char* error_str;
	

	for (int nIdx = m_nChannel * DIGITAL_IO_VIEW_NUMBER, nCnt = 0; nIdx < (m_nChannel* DIGITAL_IO_VIEW_NUMBER) + DIGITAL_IO_VIEW_NUMBER; nIdx++, nCnt++)
	{
		if (g_pIO->m_bCrevis_Open_Port == TRUE)
		{
			if (g_pIO->m_bDigitalIn[nIdx])
			{
				//MAIN AIR 
				if (nIdx == 0)
					//if (g_pIO->m_bDigitalIn[0)
				{
					cnt_main_air_alarm = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]); // Main Air = 1 -> ON,  0 -> OFF(알람발생) .
				}

				//Main Water
				else if (nIdx == 1)
				{
					cnt_main_water_alarm = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]); // Main Water = 1 -> ON,  0 -> OFF(알람발생) .
				}

				// LLC DRY PUMP ON STATUS
				else if (nIdx == 16)
					//if (g_pIO->m_bDigitalIn[16])
				{
					cnt_llc_dry_pump_on = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]); // LLC DRY PUMP ON STATUS = 1 -> ON,  0 -> OFF .
				}

				// LLC DRY PUMP ALARM
				else if (nIdx == 17)
					//if (g_pIO->m_bDigitalIn[17])
				{
					cnt_llc_dry_pump_alarm = 0;
					g_pIO->m_nERROR_MODE = RUN_OFF;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // LL DRY PUMP ALARM = 1 -> 정상 구동 상태 , 0 이면 알람 발생
				}

				// LLC DRY PUMP WARNING
				else if (nIdx == 18)
					//else if (g_pIO->m_bDigitalIn[18])
				{
					cnt_llc_dry_pump_warning = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // LL DRY PUMP WARNING = 1 -> 정상 구동 상태, 0 이면 WARNING 발생.
				}

				// MC DRY PUMP ON STATUS
				else if (nIdx == 19)
					//else if (g_pIO->m_bDigitalIn[19])
				{
					cnt_mc_dry_pump_on = 0;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]); // MC DRY PUMP ON STATUS = 1 -> ON, 0 -> OFF.
				}

				// MC DRY PUMP ALARM
				else if (nIdx == 20)
					//else if (g_pIO->m_bDigitalIn[20])
				{
					
					cnt_mc_dry_pump_alarm = 0;
					g_pIO->m_nERROR_MODE = RUN_OFF;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // MC DRY PUMP ALARM = 1 -> 정상 구동 상태 , 0 이면 알람 발생
				}

				// MC DRY PUMP WARNING
				else if (nIdx == 21)
					//else if (g_pIO->m_bDigitalIn[21])
				{
					cnt_mc_dry_pump_warning = 0;
			
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // MC DRY PUMP WARNING = 1->정상 구동 상태, 0 이면 WARNING 발생.
				}

				// WATER LEAK SENSOR LLC TMP
				else if (nIdx == 23)
					//else if (g_pIO->m_bDigitalIn[23])
				{
					cnt_llc_tmp_water_leak = 0;
					g_pIO->m_nERROR_MODE = RUN_OFF;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // WATER LEAK SENSOR LLC TMP = 1 ->  정상 구동 상태, 0 이면 LEAK 발생.
				}

				// WATER LEAK SENSOR MC TMP
				else if (nIdx == 24)
					//else if (g_pIO->m_bDigitalIn[24])
				{
					cnt_mc_tmp_water_leak = 0;
					g_pIO->m_nERROR_MODE = RUN_OFF;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // WATER LEAK SENSOR MC TMP = 1 -> 정상 구동 상태, 0 이면 LEAK  발생.
				}

				// SMOKE DETACT SENSOR (CB)
				else if (nIdx == 26)
					//else if (g_pIO->m_bDigitalIn[26])
				{
					cnt_smoke_sensor = 0;
					g_pIO->m_nERROR_MODE = RUN_OFF;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // SMOKE DETACT SENSOR (CB) = 1- > 정상 구동 상태, 0 이면 DETACT 발생.
				}
				//if (g_pIO->m_bDigitalIn[27]) ((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // SMOKE DETACT SENSOR (VAC ST) = 1 -> 정상 구동 상태, 0 이면 DETACT 발생.

				// WATER RETURN TEMP ALARM 
				else if (nIdx == 69)
					//else if (g_pIO->m_bDigitalIn[69])
				{
					cnt_water_return_temp_alarm = 0;
					g_pIO->m_nERROR_MODE = RUN_OFF;
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // Water Return Temp alarm = 1 -> 정상 구동 상태, 0 이면 Alarm 발생.
				}
				//Green
				else
				{
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[2]);
				}
			}
			else
			{
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// MAIN AIR ALARM 발생 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if (nIdx == 0)
				{
					cnt_main_air_alarm += 1;


					if (cnt_main_air_alarm == 5)
					{
						value_io_str.Format("%d", g_pIO->m_bDigitalIn[0]);
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // MAIN AIR ALARM = 0 이면 알람 발생
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Air Alarm 발생")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Air Alarm IO Intput Value ::  " + value_io_str)));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Main Air Alarm 발생")));
						ErrorCode = -84001;
						g_pIO->Error_Sequence(ErrorCode, _T("Main Air 공급 알람 발생 "));
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// MAIN WATER ALARM 발생 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if (nIdx == 1)
				{
					cnt_main_water_alarm += 1;

					if (cnt_main_water_alarm == 5)
					{
						value_io_str.Format("%d", g_pIO->m_bDigitalIn[1]);
						//cnt_io_str.Format("%d", cnt_llc_dry_pump_alarm);
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // MAIN WATER ALARM = 0 이면 알람 발생
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Water ALARM 발생")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Main Water Alarm IO Intput Value ::  " + value_io_str)));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Main Water Alarm 발생")));
						ErrorCode = -85001;
						g_pIO->Error_Sequence(ErrorCode, _T("Main Water 공급 알람 발생 "));

					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LLC DRY PUMP ON STATUS OFF 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if (nIdx == 16)
					//if (!g_pIO->m_bDigitalIn[16])
				{
					cnt_llc_dry_pump_on += 1;

					value_io_str.Format("%d", g_pIO->m_bDigitalIn[16]);
					cnt_io_str.Format("%d", cnt_llc_dry_pump_on);
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]); // LLC DRY PUMP ON STATUS = 1 -> ON , 0 -> OFF.
					

					if (cnt_llc_dry_pump_on == 5)
					{
						if (g_pIO->m_nERROR_MODE == RUN)
						{
			
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC DRY PUMP OFF 발생")));
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump On Status IO Intput Value ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Alarm 발생에 따른 Dry Pump 동작 OFF")));
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("LLC Dry Pump off 발생")));
							g_pLog->Display(0, "LLC Dry Pump Alarm 발생에 따른 Dry Pump 동작 OFF");

							error_str = _T("LLC Dry Pump Alarm 발생에 따른 Dry Pump 동작 OFF");
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(error_str)));

							/////////////////////////////////////////////////
							///// Error 발생 후 error sequence 등록 부분
							/////////////////////////////////////////////////
							//g_pIO->LLC_Dry_Pump_Error_Sequence();
							ErrorCode = LLC_DRY_PUMP_ERROR_OFF;
							g_pIO->Error_Sequence(ErrorCode, _T("LLC DRY PUMP 강제 종료 발생 "));

						}
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LLC DRY PUMP ALARM 발생 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				if (nIdx == 17)
					//if (!g_pIO->m_bDigitalIn[17])
				{
					cnt_llc_dry_pump_alarm += 1;

					value_io_str.Format("%d", g_pIO->m_bDigitalIn[17]);
					cnt_io_str.Format("%d", cnt_llc_dry_pump_alarm);
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LL DRY PUMP ALARM = 1 -> 정상 구동 상태 , 0 이면 알람 발생
				
					if (cnt_llc_dry_pump_alarm == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC DRY PUMP ALARM 발생")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Alarm IO Intput Value ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Alarm 발생에 따른 Error Sequence 작동")));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("LLC Dry Pump alarm 발생")));
						g_pLog->Display(0, "LLC Dry Pump Alarm 발생에 따른 Error Sequence 작동");
						 
						error_str = _T("LLC Dry Pump Alarm 발생에 따른 Error Sequence 작동");
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(error_str)));

						/////////////////////////////////////////////////
						///// Error 발생 후 error sequence 등록 부분
						/////////////////////////////////////////////////
						//g_pIO->LLC_Dry_Pump_Error_Sequence();

						Dry_Pump_State = DRY_LLC_ALARM;
						m_Message_ThreadExitFlag = FALSE;
						ErrorCode = LLC_DRY_PUMP_ALARM;
						g_pIO->Error_Sequence(ErrorCode, _T("LLC DRY Pump Alarm 발생"));
						
						///////////////////////////////////////////////////
						//Mesaage Box Thread 임시 주석 TEST 필요 
						///////////////////////////////////////////////////
						//m_Message_Thread = AfxBeginThread(Message_UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// LLC DRY PUMP WARNING 발생 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if (nIdx == 18)
					//else if (!g_pIO->m_bDigitalIn[18])
				{
					cnt_llc_dry_pump_warning += 1;

					value_io_str.Format("%d", g_pIO->m_bDigitalIn[18]);
					cnt_io_str.Format("%d", cnt_llc_dry_pump_warning);
					
				
					if (cnt_llc_dry_pump_warning == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC DRY PUMP WARNING 발생")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Warning IO Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Warning 발생")));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("LLC Dry Pump Warning 발생")));
						g_pLog->Display(0, "LLC Dry Pump Warning 발생");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // LL DRY PUMP WARNING = 1 -> 정상 구동 상태, 0 이면 WARNING 발생.

						/////////////////////////////////////////////////
						///// Error 발생 후 error sequence 등록 부분
						/////////////////////////////////////////////////
						Dry_Pump_State = DRY_LLC_WARNING;
						m_Message_ThreadExitFlag = FALSE;
						///////////////////////////////////////////////////
						//Mesaage Box Thread 임시 주석 TEST 필요 
						///////////////////////////////////////////////////
						//m_Message_Thread = AfxBeginThread(Message_UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
					}

				}
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// MC DRY PUMP ON STATUS OFF 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if (nIdx == 19)
				//else if (!g_pIO->m_bDigitalIn[19]) 
				{
					cnt_mc_dry_pump_on += 1;

					value_io_str.Format("%d", g_pIO->m_bDigitalIn[19]);
					cnt_io_str.Format("%d", cnt_mc_dry_pump_on);
					
				
					if (cnt_mc_dry_pump_on == 5)
					{
						if (g_pIO->m_nERROR_MODE == RUN)
						{

							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC DRY PUMP OFF 발생")));	//통신 상태 기록.
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump On Status IO Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
							SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump Alarm 발생에 따른 Dry Pump 동작 OFF")));
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("MC Dry Pump Off 발생")));
							g_pLog->Display(0, "MC Dry Pump Alarm 발생에 따른 Dry Pump 동작 OFF");
							((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // MC DRY PUMP ALARM = 1 -> 정상 구동 상태 , 0 이면 알람 발생



							error_str = _T("MC Dry Pump Alarm 발생에 따른 Dry Pump 동작 OFF");
							SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(error_str)));


							/////////////////////////////////////////////////
							///// Error 발생 후 error sequence 등록 부분
							/////////////////////////////////////////////////
							//g_pIO->MC_Dry_Pump_Error_Sequence();
							ErrorCode = MC_DRY_PUMP_ERROR_OFF;
							g_pIO->Error_Sequence(ErrorCode, _T("MC DRY PUMP 강제 종료 발생 "));
						}
					}
				}


				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// MC DRY PUMP ALARM 발생
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if (nIdx == 20)
					//else if (!g_pIO->m_bDigitalIn[20]) 
				{
					cnt_mc_dry_pump_alarm += 1;

					value_io_str.Format("%d", g_pIO->m_bDigitalIn[20]);
					cnt_io_str.Format("%d", cnt_mc_dry_pump_alarm);
					
				
					if (cnt_mc_dry_pump_alarm == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC DRY PUMP ALARM 발생")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump Alarm IO Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump Alarm 발생에 따른 Error Sequence 작동")));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("MC Dry Pump Ararm 발생")));
						g_pLog->Display(0, "MC Dry Pump Alarm 발생에 따른 Error Sequence 작동");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // MC DRY PUMP ALARM = 1 -> 정상 구동 상태 , 0 이면 알람 발생

						error_str = _T("MC Dry Pump Alarm 발생에 따른 Error Sequence 작동");
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(error_str)));

						/////////////////////////////////////////////////
						///// Error 발생 후 error sequence 등록 부분
						/////////////////////////////////////////////////
						//g_pIO->MC_Dry_Pump_Error_Sequence();
						Dry_Pump_State = DRY_MC_ALARM;
						m_Message_ThreadExitFlag = FALSE;
						ErrorCode = MC_DRY_PUMP_ALARM;
						g_pIO->Error_Sequence(ErrorCode, _T("MC DRY PUMP ALARM 발생 "));
						///////////////////////////////////////////////////
						//Mesaage Box Thread 임시 주석 TEST 필요 
						///////////////////////////////////////////////////
						//m_Message_Thread = AfxBeginThread(Message_UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// MC DRY PUMP WARNING 발생
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if (nIdx == 21)
					//else if (!g_pIO->m_bDigitalIn[21])
				{
					cnt_mc_dry_pump_warning += 1;

					value_io_str.Format("%d", g_pIO->m_bDigitalIn[21]);
					cnt_io_str.Format("%d", cnt_mc_dry_pump_warning);
					
					
					if (cnt_mc_dry_pump_warning == 5)
					{
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC DRY PUMP WARNING 발생")));	//통신 상태 기록.
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump Warning IO Intput Value  ::  " + value_io_str + "  , 반복 횟수 :: " + cnt_io_str + " 회")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC Dry Pump Warning 발생")));
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("MC Dry Pump Warning 발생")));


						g_pLog->Display(0, "MC Dry Pump Warning 발생");
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]); // MC DRY PUMP WARNING = 1->정상 구동 상태, 0 이면 WARNING 발생.

						/////////////////////////////////////////////////
						///// Error 발생 후 error sequence 등록 부분
						/////////////////////////////////////////////////
						Dry_Pump_State = DRY_MC_WARNING;
						m_Message_ThreadExitFlag = FALSE;

						///////////////////////////////////////////////////
						//Mesaage Box Thread 임시 주석 TEST 필요 
						///////////////////////////////////////////////////
						//m_Message_Thread = AfxBeginThread(Message_UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Water Leak Sesnor 작동. (LLC TMP)
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if (nIdx == 23)
					//else if (!g_pIO->m_bDigitalIn[23])
				{
					cnt_llc_tmp_water_leak += 1;

					if (cnt_llc_tmp_water_leak == 5)
					{
						value_io_str.Format("%d", g_pIO->m_bDigitalIn[23]);
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]);  // Water leak sensor (LLC TMP)= 1-> 정상 구동 상태, 0 이면 leak 발생
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("WATER LEAK SENSOR (LLC TMP) 발생")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("LLC TMP Water Leak Sensor IO Intput Value ::" + value_io_str)));	//통신 상태 기록.

						///////////////////////////////////////////////////
						// IO Line 연결 재확인 필요 
						///////////////////////////////////////////////////
						//g_pIO->Error_On(_T("LLC TMP Water Leak Sensor 작동 On"));
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Water Leak Sesnor 작동. (MC TMP)
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				else if (nIdx == 24)
					//else if (!g_pIO->m_bDigitalIn[24])
				{
					cnt_mc_tmp_water_leak += 1;

					if (cnt_mc_tmp_water_leak == 5)
					{
						value_io_str.Format("%d", g_pIO->m_bDigitalIn[24]);
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]);  // Water leak sensor (MC TMP) = 1-> 정상 구동 상태, 0 이면 leak 발생
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("WATER LEAK SENSOR (MC TMP) 발생 ")));
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("MC TMP Water Leak Sensor IO Intput Value ::" + value_io_str)));	//통신 상태 기록.

						///////////////////////////////////////////////////
						// IO Line 연결 재확인 필요 
						///////////////////////////////////////////////////
						//g_pIO->Error_On(_T("MC TMP Water Leak Sensor 작동 On"));
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Smoke detact Sesnor 작동. 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				//else if (!g_pIO->m_bDigitalIn[26])
				else if (nIdx == 26)
				{
					cnt_smoke_sensor += 1;
					if (cnt_smoke_sensor == 5)
					{
						value_io_str.Format("%d", g_pIO->m_bDigitalIn[26]);
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]);
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("SMOKE DETACT SENSOR 감지 발생")));  // smoke detact sensor = 1->정상 구동 상태, 0 이면 leak 발생
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Smoke Detact Sensor IO Intput Value ::" + value_io_str)));	//통신 상태 기록.
						
						///////////////////////////////////////////////////
						// IO Line 연결 재확인 필요 
						///////////////////////////////////////////////////
						//g_pIO->Error_On(_T("Smoke Sensor 작동 On"));
					}
				}

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				// Watewr Return temp alarm. 
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				//else if (g_pIO->m_bDigitalIn[69])
				else if (nIdx == 69)
				{
					cnt_water_return_temp_alarm += 1;
					if (cnt_water_return_temp_alarm == 5)
					{
						value_io_str.Format("%d", g_pIO->m_bDigitalIn[69]);
						((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]);
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Water Return Tamp Alarm 발생")));  // Water Return temp alarm = 1 -> 정상 구동 상태, 0 이면 알람 발생
						SaveLogFile("IO_ALARM_LOG", _T((LPSTR)(LPCTSTR)("Water Return Tamp Alram IO Intput Value ::" + value_io_str)));	//통신 상태 기록.
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Water Return Tamp alarm 발생")));


						error_str = _T("Water Return Tamp alarm 발생에 따른 Error Sequence 작동");
						SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(error_str)));

						///////////////////////////////////////////////////
						// IO Line 연결 재확인 필요 
						///////////////////////////////////////////////////
						ErrorCode = WATER_RETURN_TEMP_ALARM_ERROR;
						g_pIO->Error_Sequence(ErrorCode,_T("Water Return Temp Alarm On"));

					}
				}

				//if (!g_pIO->m_bDigitalIn[27]) ((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[1]);
				else
				{
					((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
				}

			}
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_X000 + nCnt))->SetIcon(m_LedIcon[0]);
		}
	}

	return TRUE;
}


HBRUSH CDigitalInput::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_TEXT)
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
	}
	return hbr;
}

UINT CDigitalInput::Message_UpdataThread(LPVOID pParam)
{
	int ret = 0;

	CDigitalInput*  message_runthread = (CDigitalInput*)pParam;

	while (!message_runthread->m_Message_ThreadExitFlag)
	{
		message_runthread->Message_Thread();
		Sleep(100);
	}

	return 0;
}


void CDigitalInput::Message_Thread()
{

	CString message_str;

	switch (Dry_Pump_State)
	{
	case DRY_NO_ERROR :

		break;
	case DRY_LLC_ALARM :
		message_str = _T("LLC Dry Pump Alarm");
		break;
	case DRY_MC_ALARM :
		message_str = _T("MC Dry Pump Alarm");
		break;
	case DRY_LLC_WARNING :
		message_str = _T("LLC Dry Pump Warning");
		break;
	case DRY_MC_WARNING:
		message_str = _T("LLC Dry Pump Warning");
		break;
	default:
		break;
	}

	if (g_pIO->m_nERROR_MODE == RUN)
	{
		///////////////////////////////////
		// Buzzer On
		///////////////////////////////////
		g_pIO->WriteOutputDataBit(8, 3, 1);

		if (AfxMessageBox(message_str + "\n" + "발생으로 인한 ERROR [설비 비상 정지]", MB_OKCANCEL) == IDOK) // or IDCANCEL
		{
			m_Message_ThreadExitFlag = TRUE;

			///////////////////////////////////
			// Buzzer On
			///////////////////////////////////
			g_pIO->WriteOutputDataBit(8, 3, 0);

			if (m_Message_Thread != NULL)
			{
				HANDLE threadHandle = m_Message_Thread->m_hThread;
				DWORD dwResult;
				dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
				if (dwResult == WAIT_TIMEOUT)
				{
					DWORD dwExitCode = STILL_ACTIVE;
					::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
					if (dwExitCode == STILL_ACTIVE)	//259
					{
						TerminateThread(threadHandle, 0/*dwExitCode*/);
						CloseHandle(threadHandle);
					}
				}
				m_Message_Thread = NULL;
			}
		}
	}
}