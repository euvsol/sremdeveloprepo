﻿#pragma once


// CFilterStageDlg 대화 상자

class CFilterStageDlg : public CDialogEx, public CPIE873Ctrl
{
	DECLARE_DYNAMIC(CFilterStageDlg)

public:
	CFilterStageDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CFilterStageDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FILTER_STAGE_DIALOG };
//#endif

protected:
	
	HICON			m_LedIcon[3];

	BOOL			m_bThreadExitFlag;
	CWinThread*		m_pStatusThread;

	CEdit			m_PosCtrl;
	double			m_dTolerance;

	static UINT FilterStageStatusThread(LPVOID pParam);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();

	int OpenDevice();
	void GetDeviceStatus();

	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonPiFilterSvoOn();
	afx_msg void OnBnClickedButtonPiFilterSvoOff();
	afx_msg void OnBnClickedButtonPiFilterPos1();
	afx_msg void OnBnClickedButtonPiFilterPos2();
	afx_msg void OnBnClickedButtonPiFilterPos3();
	afx_msg void OnBnClickedButtonPiFilterMoveOri();
	afx_msg void OnBnClickedButtonPiFilterMoveN();
	afx_msg void OnBnClickedButtonPiFilterMoveP();
	afx_msg void OnBnClickedButtonFilterAbsMove();
	afx_msg void OnBnClickedButtonFilterSet();
	afx_msg void OnDeltaposSpinPiFilter(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CEdit m_RelMoveCtrl;
	CEdit m_AbsMoveCtrl;
	CEdit m_Pos1Ctrl;
	CEdit m_Pos2Ctrl;
	CEdit m_Pos3Ctrl;

	BOOL Is_FILTER_Stage_Connected() { return m_bConnect; }
};
