/**
 * Process Data Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

class CProcessData : public CFile
{
public:	
	CProcessData();
	virtual ~CProcessData();

	void ResetData();
	int LoadHeaderInfo(char *fpath);
	/** For SREM */
	int LoadMeasureData(char *fpath);
	/** For EPhase */
	int LoadPhaseMeasureData(char *fpath);
	/** For EUVPTR */
	int LoadPTRMeasureData(char *fpath);
	/** For MACRO */
	int LoadMACROMeasureData(char *fpath);
	int LoadMACROCOmmandData(char *fpath);
	/** For ELITHO */
	int LoadLITHOMeasureData(char *fpath);

	int nAlignmentPointTotal;
	int TotalMeasureNum;
	int CurrentMeasureNum;
	int CurrentThroughFocusNum;

	CPath	curFilePath;
	CPath	orgFilePath;
	int		curFileIndex;

	void DeleteDefectRecord();	
	void DeleteMacroRecord();	

	CString RecipeGenDateTime;
	CString EquipmentModelName;
	CString m_strSubstrateMap;
	CString Substrate;
	CString Lot;
	CString Device;
	CString SetupID;
	CString Step;
	CString SubstrateDirection;	
	CString SubstrateID;
	int SubstrateSize;
	int Slot;
	int TotalMeasureSpecNum;
	int	  m_mindiex,m_mindiey;
	int	  m_maxdiex,m_maxdiey;

	struct _Coordinate
	{
		double X;
		double Y;
	}; 
	_Coordinate m_stDiePitch;
	_Coordinate m_stMaskCenterPos_um;
	_Coordinate m_stDieOriginPos_um;

	_Coordinate m_stOriginCoordinate_um;
	_Coordinate m_stReferenceCoord_um; // m_stAlignmentLB_um로 이름 변경?? ihlee
	_Coordinate m_stAlignmentPoint_um[4];
	_Coordinate m_stAlignmentLT_um, m_stAlignmentRT_um, m_stAlignmentRB_um;

	/** For EPhase */
	_Coordinate m_stReferenceFrequencyPos_um;
	_Coordinate m_stBeamDiagnosisPos_um;

	/*  For Litho */
	_Coordinate m_stNotchAlignPos_um[2];
	//_Coordinate m_Notch2AlignPos_um;

	int m_nBeamDiagPosExposureTime_ms;

	/* MACRO SYSTEM (kjh) */
	struct _CommandList
	{
		char Command[20];			// Macro
	};
	_CommandList *CommandList;


	struct _MeasurementList
	{
		int MeasurementNo;		//Phase&SREM&EUVPTR&ELITHO 공통

		/* MACRO SYSTEM (kjh) */
		char Command[20];		//	Macro

		int index;
		double ReferenceCoodX_um;	//Phase&SREM&ELITHO 공통
		double ReferenceCoodY_um;	//Phase&SREM&ELITHO 공통
		int DieXNo;
		int DieYNo;
		double DieXPos;
		double DieYPos;
		double dFOVSize;        //Phase&SREM&EUVPTR 공통
		double dFOVSizeX;        //Litho
		double dFOVSizeY;        //Litho

		double dStepSize;       //Phase&SREM&EUVPTR 공통, Scan Grid
		int		nRepeatNo;		//Phase&SREM&EUVPTR 공통, Scan 횟수
		int nThroughFocusNo;
		double dThroughFocusStep;		

		char	CLASSTYPE[128];
		int nMeasureOK;
		char comments[64];

		// sampling관련
		BOOL bValidPoint;		//측정할지 말지 결정
		int	 nSortID;			//Sorting 필요시 사용
		bool bDefectSelected;	//선택된 point인지 판단

		/** For EPhase */
		//double	dLeftX_um;	// ReferenceCoodX_um로 대체
		//double	dLeftY_um;	// ReferenceCoodY_um로 대체
		double		dRightX_um; // Second로 이름 변경필요
		double		dRightY_um;
		double		dAlignExposureTime;
		double		dMeasureExposureTime;	
		BOOL		bIsUseCoasreAlign;
		double		dLsHalfPitch;

		/** For EUVPTR */
		int			nMeasurementXNo;		// X방향 측정 개수 ex: 100 -> X방향으로 100 point 측정
		//int			nMeasurementXScanNo;		// Scan방식일때 point display 줄이기 위해 임시 사용
		int			nMeasurementYNo;		// Y방향 측정 개수 ex: 100 -> Y방향으로 100 point 측정
		//int			nMeasurementYScanNo;		// Scan방식일때 point display 줄이기 위해 임시 사용
		double		dMeasureDistance_um;	//측정 간격 ex: 1000 -> 1000um 간격으로 측정
		//double		dMeasureDistanceScan_um;	// Scan방식일때 point display 줄이기 위해 임시 사용

		double		m_dCouponXPos_um;		// ReferenceCoodX_um로 대체
		double		m_dCouponYPos_um;		// ReferenceCoodY_um로 대체
		double		dMeasurementXPos_um;	// Coupon안에서의 X 측정 좌표
		double		dMeasurementYPos_um;	// Coupon안에서의 Y 측정 좌표
		double		dExposureTime_msec;		//1 point에서 측정하는 노출시간 ex: 1000 -> 1000ms동안 노출 후 이동		//EUVPTR&ELITHO 공통

		/** For ELitho */
		double		dExposureHeight_um;
		double		dBeam_pitch;
		double		dBeam_yaw;
	};
	_MeasurementList *pMeasureList;

	int m_nPTRTotalMeasureNum;
	struct _PTRMeasurementList
	{
		double dReferenceCoodX_um;	//Coupon Center 좌표 + Customer Measure 좌표를 Start Point로 한다.
		double dReferenceCoodY_um;	//Coupon Center 좌표 + Customer Measure 좌표를 Start Point로 한다.
	};
	_PTRMeasurementList *m_pstPTRMeasureList;	//m_nPTRTotalMeasureNum(=nMeasurementXNo X nMeasurementYNo) 만큼 메모리 생성

	struct _DieInformation
	{
		double DieSizeX;
		double DieSizeY;
		int	nDieX;
		int nDieY;
		double DiePitchX;
		double DiePitchY;
	} DieInformation;

	// Measurement Point LIST
	char	DefListName[20][40];
	char	DefListValue[20][40];

	//코드 분류된 id 개수관련
	int classifiedNum;

	double GetDefectEachValue(char *name);
	CString GetCommandValue(char *name);
	void GetDefectListAllValue(char *value);
	void GetDefectListName(char *name);

	int GetYIndex(double posy);
	int	GetXIndex(double posx);
	double GetXPos(int ndiex, double xrel = 0.0);
	double GetYPos(int ndiey, double yrel = 0.0);

	//측정 파일 Parsing
	FILE* fp;
	BOOL OpenFile(LPCTSTR lpszFileName, UINT nOpenFlags = CFile::modeReadWrite);
	void Close();
	long FindItem(char* Item, int Index = 1);
	void PassCurrentLine(void);//int PassCurrentLine(void);
	BOOL GetLine(char *line);
	int GetItem(char* Buffer);
	int GetInt();
	double GetDouble();
	long CountItem(char* Buffer);
	int GetBuffer(char* Buffer, char Divider, int IncludeDivider = 1);

	double dExposureTime_total;
	int nExposureTime_total;
};
