#pragma once

class Command
{
public:
	virtual BOOL execute() {
		BOOL ReturnFlag = TRUE;
		return ReturnFlag;
	};
	virtual ~Command(void) {};
};

class RecipeStack
{
public:
	
	vector <Command*> vector_commands;

	void addCommand(Command *com);
	BOOL createRecipe();
	void undo();

};


class STAGEMOVE
{
public:
	BOOL StageMoveStart(double x_pos, double y_pos);

};

class EUVCTL
{
public:
	BOOL EuvOn();
	BOOL EuvOff();
};

class LOADCTL
{
public:
	BOOL Load();
	BOOL UnLoad();
};


class SCANCTL
{
public:
	BOOL ScanStart(double Fovsize, double Stepsize, int Repeatno, int Throughfocusno, double Throughfocusstep);
	BOOL ScanStop();
};


class CMacroSystem
{

public:
	CMacroSystem();
	~CMacroSystem();
	
	STAGEMOVE *Stagemove = new STAGEMOVE;
	EUVCTL *EuvCtl = new EUVCTL;
	SCANCTL *ScanCtl = new SCANCTL;
	LOADCTL *LoadCtl = new LOADCTL;

	Command *StageMoveOnCom;
	Command *EuvOn;
	Command *EuvOff;
	Command *ScanStart;
	Command *ScanStop;
	
	RecipeStack _ComList;
	CProcessData  m_ProcessData;

	void GetCommandData(char* com, double x_pos, double y_pos, double FovSize, double StepSize, int Repeatno, int ThroughFocusNo, double ThroughFocusStep);
	void DeleteCom();
	void DeleteMem();
	void NewCom();

};


class StageMoveOnCommand : public Command
{
private :
	STAGEMOVE *StageMove;

public :
	double x;
	double y;

	StageMoveOnCommand(STAGEMOVE *StageMove, double x_pos, double y_pos)
	{
		this->StageMove = StageMove;
		x = x_pos;
		y = y_pos;
	}

	virtual BOOL execute()
	{
		return StageMove->StageMoveStart(x, y);
	}


};

class EuvOnCommand : public Command
{
private:
	EUVCTL *EuvCtl;

public:

	EuvOnCommand(EUVCTL *EuvCtl)
	{
		this->EuvCtl = EuvCtl;
	}

	virtual BOOL execute()
	{
		return EuvCtl->EuvOn();
	}

};

class EuvOffCommand : public Command
{
private:
	EUVCTL *EuvCtl;

public:

	EuvOffCommand(EUVCTL *Command)
	{
		this->EuvCtl = Command;
	}

	virtual BOOL execute()
	{
		return EuvCtl->EuvOff();
	}

};

class ScanStartCommand : public Command
{
private:
	SCANCTL *ScanCtl;

public:
	double fov;
	double step;
	int repeat;
	int throughfocusno;
	double throughfocusstep;

	ScanStartCommand(SCANCTL *Command, double Fovsize, double Stepsize, int Repeatno, int Throughfocusno, double Throughfocusstep)
	{
		this->ScanCtl = Command;
		fov = Fovsize;
		step = Stepsize;
		repeat = Repeatno;
		throughfocusno = Throughfocusno;
		throughfocusstep = Throughfocusstep;
	}

	virtual BOOL execute()
	{
		return ScanCtl->ScanStart(fov, step, repeat, throughfocusno, throughfocusstep);
	}

};

class ScanStopCommand : public Command
{
private:
	SCANCTL *ScanCtl;

public:

	ScanStopCommand(SCANCTL *Command)
	{
		this->ScanCtl = Command;
	}

	virtual BOOL execute()
	{
		return ScanCtl->ScanStop();
	}

};



class MacroCommand : public Command
{

};