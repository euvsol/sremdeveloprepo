#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

#ifdef _DEBUG
#define _DEBUG_WAS_DEFINED 1
#undef _DEBUG
#endif

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy\arrayobject.h>
#include <numpy\npy_3kcompat.h>

#ifdef _DEBUG_WAS_DEFINED
#define _DEBUG
#endif


#ifdef _DEBUG
#define new DEBUG_NEW


#endif


// CEUVSolutionDlg 대화 상자
CEUVSolutionDlg::CEUVSolutionDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_EUVSOLUTION_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CEUVSolutionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EUVSOL_BEVEL1, m_EuvsolBevel1Ctrl);
	DDX_Control(pDX, IDC_EUVSOL_BEVEL2, m_EuvsolBevel2Ctrl);
	DDX_Control(pDX, IDC_CLOCKFRAME, m_Clock);
}

BEGIN_MESSAGE_MAP(CEUVSolutionDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_OPERATOR_BUTTON, &CEUVSolutionDlg::OnBnClickedOperatorButton)
	ON_BN_CLICKED(IDC_ENGINEER_BUTTON, &CEUVSolutionDlg::OnBnClickedEngineerButton)
	ON_BN_CLICKED(IDC_EXIT_BUTTON, &CEUVSolutionDlg::OnBnClickedExitButton)
	ON_BN_CLICKED(IDC_CONFIGURATION_BUTTON, &CEUVSolutionDlg::OnBnClickedConfigurationButton)
	ON_BN_CLICKED(IDC_EVALUATION_BUTTON, &CEUVSolutionDlg::OnBnClickedEvaluationButton)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_MAIN_EUV_ON, &CEUVSolutionDlg::OnBnClickedButtonMainEuvOn)
	ON_BN_CLICKED(IDC_BUTTON_MAIN_EUV_OFF, &CEUVSolutionDlg::OnBnClickedButtonMainEuvOff)
	ON_BN_CLICKED(IDC_BUTTON_MAIN_SHUTTER_OPEN, &CEUVSolutionDlg::OnBnClickedButtonMainShutterOpen)
	ON_BN_CLICKED(IDC_BUTTON_MAIN_SHUTTER_CLOSE, &CEUVSolutionDlg::OnBnClickedButtonMainShutterClose)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()


END_MESSAGE_MAP()


// CEUVSolutionDlg 메시지 처리기

BOOL CEUVSolutionDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	int nRet = 0;
	int nX = 0, nY = 0;

	InitializeControls();

	//ShowWindow(SW_MAXIMIZE);

	//MIL ALLOC 초기화
	MappAlloc(M_DEFAULT, &g_milApplication);
	MsysAlloc(M_SYSTEM_HOST, M_DEF_SYSTEM_NUM, M_COMPLETE, &g_milSystemHost);
	MsysAlloc(M_SYSTEM_DEFAULT, M_DEF_SYSTEM_NUM, M_COMPLETE, &g_milSystemGigEVision);

	// Python 초기화
	int iSInitialized = Py_IsInitialized();
	if (iSInitialized == 0)
	{
		Py_Initialize();
		//import_array();
	}
	// Create Modules
	nRet = CreateModules();

	if (g_pEUVSource->Is_EUV_On() == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_OFF))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_OFF))->SetIcon(m_LedIcon[2]);
	}

	if (g_pEfem->Is_POD_OnLPM() == TRUE)
	{
		if (g_pEfem->Is_POD_Open() == TRUE)
		{
			SetDlgItemText(IDC_POD_STATUS, _T("ON LPM(OPEN)"));
			((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			SetDlgItemText(IDC_POD_STATUS, _T("ON LPM(CLOSE)"));
			((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[0]);
		}
	}
	else
		SetDlgItemText(IDC_POD_STATUS, _T("NOT EXISTS"));

	SetDlgItemText(IDC_MAIN_LOADING_POS, g_pNavigationStage->Is_Loading_Positioin() == TRUE ? _T("YES") : _T("NO"));
	if (g_pNavigationStage->Is_Loading_Positioin() == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_LOADINGPOS))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_LOADINGPOS))->SetIcon(m_LedIcon[0]);

	SetForegroundWindow();

	InitClock();

	SetTimer(MAIN_DIALOG_INFO_TIMER, 100, NULL);

	if (g_pLog != NULL)
		g_pLog->Display(0, _T("EUV Solution Start!"));


	/* 바탕화면 작업표시줄 표기를 위한 UI 수정 2020.11.30 kjh */

	///////////////////////////////////////////////////////////////////////
	//ShowWindow(SW_SHOWMAXIMIZED);

	LONG style = ::GetWindowLong(m_hWnd, GWL_STYLE);

	style &= ~WS_CAPTION;
	style &= ~WS_SYSMENU;

	::SetWindowLong(m_hWnd, GWL_STYLE, style);
	int screenx = GetSystemMetrics(SM_CXSCREEN);
	int screeny = GetSystemMetrics(SM_CYSCREEN);

	SetWindowPos(NULL, -10, -10, screenx + 30, screeny - 30, SWP_NOZORDER);
	/////////////////////////////////////////////////////////////////////


	// 1.H/W Module 초기화 수행
	// 2.System Monotoring 수행 시작(Timer?, Thread?)

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CEUVSolutionDlg::InitializeControls()
{
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_ON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_OFF))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_SHUTTER_OPEN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_SHUTTER_CLOSE))->SetIcon(m_LedIcon[2]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_LOADINGPOS))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_EQ_STATUS))->SetIcon(m_LedIcon[1]);
}

void CEUVSolutionDlg::GetEquipmentInfo()
{
	CString sTemp;

	// MKS 390 Gauge 
	if (g_pGauge_IO->m_bOpenPort_State == TRUE)
	{
		//SetDlgItemText(IDC_LLC_VACCUM_VALUE, g_pGauge_IO->ch_2 + " Torr");
		//SetDlgItemText(IDC_MC_VACCUM_VALUE, g_pGauge_IO->ch_3 + " Torr");

		SetDlgItemText(IDC_LLC_VACCUM_VALUE, g_pGauge_IO->Get_LLC_VacuumRate() + " Torr");
		SetDlgItemText(IDC_MC_VACCUM_VALUE, g_pGauge_IO->Get_MC_VacuumRate() + " Torr");
	}
	else
	{
		SetDlgItemText(IDC_LLC_VACCUM_VALUE, "연결 확인 필요");
		SetDlgItemText(IDC_MC_VACCUM_VALUE, "연결 확인 필요");
	}

	if (g_pEUVSource->Is_EUV_On() == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_OFF))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_EUV_OFF))->SetIcon(m_LedIcon[2]);
	}

	if (g_pEUVSource->Is_Shutter_Opened() == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_SHUTTER_OPEN))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_SHUTTER_CLOSE))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_SHUTTER_OPEN))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_SHUTTER_CLOSE))->SetIcon(m_LedIcon[2]);
	}

	if (g_pEfem->Is_POD_OnLPM() == TRUE)
	{
		if (g_pEfem->Is_POD_Open() == TRUE)
			sTemp = _T("ON LPM(OPEN)");
		else
			sTemp = _T("ON LPM(CLOSE)");
	}
	else
		sTemp = _T("NOT EXISTS");

	SetDlgItemText(IDC_POD_STATUS, sTemp);

	if (g_pEfem->Is_POD_Open() == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_MTSPOD_OPEN))->SetIcon(m_LedIcon[0]);

	if (g_pNavigationStage->Is_Loading_Positioin() == TRUE)
	{
		SetDlgItemText(IDC_MAIN_LOADING_POS, _T("YES"));
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_LOADINGPOS))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		SetDlgItemText(IDC_MAIN_LOADING_POS, _T("NO"));
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_LOADINGPOS))->SetIcon(m_LedIcon[0]);
	}

	int nLoc = g_pConfig->m_nMaterialLocation;

	switch (nLoc)
	{
	case MTS_POD:			sTemp = _T("MTS POD");		break;
	case MTS_ROBOT_HAND:	sTemp = _T("MTS ROBOT");	break;
	case MTS_ROTATOR:		sTemp = _T("MTS ROTATOR");	break;
	case MTS_FLIPPER:		sTemp = _T("MTS FLIPPER");	break;
	case LLC:				sTemp = _T("LLC");			break;
	case VACUUM_ROBOT_HAND: sTemp = _T("VAC ROBOT");	break;
	case CHUCK:				sTemp = _T("CHUCK");		break;
	default:				sTemp = _T("-");			break;
	}

	SetDlgItemText(IDC_MAIN_MASK_LOCATION, sTemp);
}

void CEUVSolutionDlg::SetEquipmentStatus(CString sTemp)
{
	CString sMsg = sTemp + _T(" NG");
	SetDlgItemText(IDC_MAIN_EQ_STATUS, sMsg);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_EQ_STATUS))->SetIcon(m_LedIcon[2]);
}

void CEUVSolutionDlg::InitClock()
{
	//	m_Clock.Start(IDB_CLOCKST_PANE, IDB_CLOCKST_BIG, IDB_CLOCKST_SMALL,true);
	m_Clock.SetOn(FALSE);
	m_Clock.SetTextOffColor(RGB(0, 255, 0));
	m_Clock.SetBackgroundOffColor(RGB(0, 0, 50));
	m_Clock.SetBold();
	m_Clock.SetPointFont(15, "Arial");
	m_Clock.SetModalFrame();
	m_Clock.Start();
}
// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CEUVSolutionDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		DisplayVersion();
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CEUVSolutionDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CEUVSolutionDlg::OnBnClickedExitButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedExitButton() 버튼 클릭!"));

	
	g_pAdam->CloseTcpIpSocket(); //OnDestroy에서 수행시 지연 발생해서 여기에 추가 (ihlee)

	CDialogEx::OnOK();
}

void CEUVSolutionDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(OUTPUT_LINE_PART_DIALOG_TIMER);
	KillTimer(INPUT_LINE_PART_DIALOG_TIMER);
	KillTimer(OUTPUT_PART_DIALOG_TIMER);
	KillTimer(INPUT_PART_DIALOG_TIMER);
	KillTimer(MAIN_DIALOG_INFO_TIMER);
	KillTimer(MAP_UPDATE_TIMER);
	KillTimer(OMIMAGE_DISPLAY_TIMER);	
	KillTimer(ADAMDATA_DISPLAY_TIMER);
	KillTimer(VMTRROBOT_UPDATE_TIMER);
	KillTimer(MTS_UPDATE_TIMER);
	KillTimer(AFM_UPDATE_TIMER);
	KillTimer(LLC_TMP_UPDATE_TIMER);
	KillTimer(MC_TMP_UPDATE_TIMER);
	KillTimer(GAUGE_UPDATE_TIMER);
	KillTimer(IO_UPDATE_TIMER);
	KillTimer(REVOLVER_UPDATE_TIMER);
	KillTimer(EUV_SOURCE_UPDATE_TIMER);
	KillTimer(NAVISTAGE_UPDATE_TIMER);
	KillTimer(SCANSTAGE_UPDATE_TIMER);
	KillTimer(MASK_FLATNESS_MEASUREMENT_TIMER);
	KillTimer(STAGE_MEASUREMENT_TIMER);
	KillTimer(SOURCE_MEASUREMENT_TIMER);
	KillTimer(FILTERSTAGE_UPDATE_TIMER);
	KillTimer(PI_STAGE_MEASUREMENT_TIMER);
	KillTimer(VACCUM_GAUGE_WRITE_TIMER);
	KillTimer(INPUT_DIALOG_TIMER);
	KillTimer(OUTPUT_DIALOG_TIMER);
	KillTimer(XRAY_CAMERA_UPDATE_TIMER);
	KillTimer(MC_SEQUENCE_CHECK_TIMER);
	KillTimer(LLC_SEQUENCE_CHECK_TIMER);
	//KillTimer(REFERENCE_POS_CHECK_TIMER);
	KillTimer(XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING);
	KillTimer(ADAMDATA_CONNECTION_TIMER);
	KillTimer(LITHO_DOSE_TIME_CHECK);
	KillTimer(PORT_CHECK_TIMER);

	WaitSec(1);


	if (g_pAnimationGUI != NULL)	DELETECLASS(g_pAnimationGUI);
	if (g_pXrayCamera != NULL)	DELETEDIALOG(g_pXrayCamera);
	if (g_pPhase != NULL)	DELETEDIALOG(g_pPhase);
	if (g_pXrayCameraConfig != NULL)	DELETEDIALOG(g_pXrayCameraConfig);
	if (g_pPTR != NULL)	DELETEDIALOG(g_pPTR);
	if (g_pRecipe != NULL)	DELETEDIALOG(g_pRecipe);
	if (g_pNavigationStage != NULL)	DELETEDIALOG(g_pNavigationStage);
	if (g_pScanStage != NULL)	DELETEDIALOG(g_pScanStage);
	if (g_pFilterStage != NULL)	DELETEDIALOG(g_pFilterStage);
	if (g_pMaskMap != NULL)	DELETEDIALOG(g_pMaskMap);
	if (g_pCamera != NULL)	DELETEDIALOG(g_pCamera);
	if (g_pCamZoneplate != NULL)	DELETEDIALOG(g_pCamZoneplate);
	if (g_pAdam != NULL)	DELETEDIALOG(g_pAdam);
	if (g_pMaindialog != NULL)	DELETEDIALOG(g_pMaindialog);
	if (g_pIO != NULL)	DELETEDIALOG(g_pIO);
	if (g_pGemAutomation != NULL)	DELETEDIALOG(g_pGemAutomation);
	if (g_pEfem != NULL)	DELETEDIALOG(g_pEfem);
	//DELETEDIALOG(g_pMCTmp);
	if (g_pMCTmp_IO != NULL)	DELETEDIALOG(g_pMCTmp_IO);
	//DELETEDIALOG(g_pMC2Tmp);
	//DELETEDIALOG(g_pLLKTmp);
	if (g_pLLKTmp_IO != NULL)	DELETEDIALOG(g_pLLKTmp_IO);
	//DELETEDIALOG(g_pMCGauge);
	//DELETEDIALOG(g_pLLCGauge);
	if (g_pGauge_IO != NULL)	DELETEDIALOG(g_pGauge_IO);
	if (g_pVacuumRobot != NULL)	DELETEDIALOG(g_pVacuumRobot);
	if (g_pEUVSource != NULL)	DELETEDIALOG(g_pEUVSource);
	if (g_pConfig != NULL)	DELETEDIALOG(g_pConfig);
	if (g_pProcessEditor != NULL)	DELETEDIALOG(g_pProcessEditor);
	if (g_pCommStat != NULL)	DELETEDIALOG(g_pCommStat);
	if (g_pTest != NULL)	DELETEDIALOG(g_pTest);
	if (g_pWarning != NULL)	DELETEDIALOG(g_pWarning);
	if (g_pSubMenu != NULL)	DELETEDIALOG(g_pSubMenu);
	if (g_pAlarm != NULL)	DELETEDIALOG(g_pAlarm);
	if (g_pLightCtrl != NULL)	DELETEDIALOG(g_pLightCtrl);
	if (g_pAP != NULL)	DELETECLASS(g_pAP);
	if (g_pVP != NULL)	DELETECLASS(g_pVP);
	if (g_pChart != NULL)	DELETECLASS(g_pChart);
	if (g_pChartline != NULL)	DELETECLASS(g_pChartline);
	if (g_pChartstage != NULL)	DELETECLASS(g_pChartstage);
	if (g_pSubTestMenu != NULL)	DELETECLASS(g_pSubTestMenu);
	if (g_pScanStageTest != NULL)	DELETECLASS(g_pScanStageTest);
	if (g_pNavigationStageTest != NULL)	DELETECLASS(g_pNavigationStageTest);
	if (g_pSourceTest != NULL)	DELETECLASS(g_pSourceTest);
	if (g_pBeamCon != NULL)	DELETECLASS(g_pBeamCon);

	if (g_pLog != NULL)
		g_pLog->Display(0, _T("EUV Solution END!"));
	DELETEDIALOG(g_pLog);


	//MIL Free
	if (g_milSystemGigEVision != M_NULL)
	{
		MsysFree(g_milSystemGigEVision);
		g_milSystemGigEVision = M_NULL;
	}

	if (g_milSystemHost != M_NULL)
	{
		MsysFree(g_milSystemHost);
		g_milSystemHost = M_NULL;
	}

	if (g_milApplication != M_NULL)
	{
		MappFree(g_milApplication);
		g_milApplication = M_NULL;
	}

	int iSInitialized = Py_IsInitialized();
	if (iSInitialized != 0)
	{
		Py_Finalize();
	}
}

int CEUVSolutionDlg::CreateModules()
{
	int nRet = 0;
	CRect rROI;
	rROI.SetRectEmpty();

	GetDlgItem(IDC_REFERENCE)->GetWindowRect(&rROI);
	ScreenToClient(&rROI);

	CREATEDIALOG(g_pLog, CLogDisplayDlg, this, CPoint(rROI.left + 2140, rROI.top + 1600), SW_HIDE);
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("LogDisplay Creation Success!"));
	else
		g_pLog->Display(0, _T("LogDisplay Creation Fail!"));

	CREATEDIALOG(g_pConfig, CConfigurationEditorDlg, this, CPoint(rROI.left + 400, rROI.top - 20), SW_HIDE);
	if (g_pConfig != NULL)
	{
		//g_pConfig->ReadFile();
		g_pLog->Display(0, _T("ConfigurationEditor Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("ConfigurationEditor Creation Fail!"));
		return nRet;
	}

	CRect rect;
	GetDlgItem(IDC_STATIC_ESOL_LOGO)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	CString sTitle;
	switch (g_pConfig->m_nEquipmentType)
	{
	case SREM033:
		sTitle = _T("SREM Loading ....");
		m_hLogo = (HBITMAP)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_ESOL_LOGO)/*"ESOL_Logo_EPhase.bmp"*/, IMAGE_BITMAP, rect.Width(), rect.Height(), LR_DEFAULTCOLOR /*LR_LOADFROMFILE | LR_CREATEDIBSECTION*/);
		break;
	case PHASE:
		sTitle = _T("EPHASE Loading ....");
		m_hLogo = (HBITMAP)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_ESOL_EPHASE_LOGO)/*"ESOL_Logo_EPhase.bmp"*/, IMAGE_BITMAP, rect.Width(), rect.Height(), LR_DEFAULTCOLOR /*LR_LOADFROMFILE | LR_CREATEDIBSECTION*/);
		break;
	case EUVPTR:
		sTitle = _T("PTR Loading ....");
		m_hLogo = (HBITMAP)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_ESOL_EUVPTR_LOGO)/*"ESOL_Logo_EUVPTR.bmp"*/, IMAGE_BITMAP, rect.Width(), rect.Height(), LR_DEFAULTCOLOR /*LR_LOADFROMFILE | LR_CREATEDIBSECTION*/);
		break;
	case ELITHO:
		sTitle = _T("ELITHO Loading ....");
		m_hLogo = (HBITMAP)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_ESOL_ELITHO_LOGO)/*"ESOL_Logo_EUVPTR.bmp"*/, IMAGE_BITMAP, rect.Width(), rect.Height(), LR_DEFAULTCOLOR /*LR_LOADFROMFILE | LR_CREATEDIBSECTION*/);
		break;
	default:
		break;
	}
	((CStatic*)GetDlgItem(IDC_STATIC_ESOL_LOGO))->SetBitmap(m_hLogo);

	g_pLoadingScreen.Create(IDD_LOADING_SCREEN_DIALOG, this);
	((CStatic*)g_pLoadingScreen.GetDlgItem(IDC_SPLASH))->SetBitmap(m_hLogo);
	g_pLoadingScreen.ShowWindow(SW_SHOW);
	g_pLoadingScreen.SetTitleMessage(sTitle);
	g_pLoadingScreen.SetTextMessage(_T("Create Modules !!"));
	WaitSec(2);

	g_pWarning = new CWarningDlg(this);
	CRect rt;
	g_pWarning->GetWindowRect(&rt);
	rt.OffsetRect(CPoint(rROI.left + 2300, rROI.top + 50));
	g_pWarning->MoveWindow(rt);
	if (g_pWarning != NULL)
	{
		g_pLog->Display(0, _T("Warning Dialog Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("Warning Dialog Creation Fail!"));
		return nRet;
	}


	g_pAlarm = new CAlarmDlg(this);
	if (g_pAlarm != NULL)
	{
		g_pLog->Display(0, _T("Alarm Dialog Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("Alarm Dialog Creation Fail!"));
		return nRet;
	}



	CREATEDIALOG(g_pSubMenu, CSubMenuDlg, this, CPoint(rROI.left + 430, rROI.top + 75), SW_HIDE);
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("SubMenu Creation Success!"));
	else
		g_pLog->Display(0, _T("SubMenu Creation Fail!"));

	CREATEDIALOG(g_pSubTestMenu, CSubTestMenuDlg, this, CPoint(rROI.left + 430, rROI.top + 75), SW_HIDE);
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("SubMenu Creation Success!"));
	else
		g_pLog->Display(0, _T("SubMenu Creation Fail!"));

	CREATEDIALOG(g_pProcessEditor, CProcessEditorDlg, this, CPoint(rROI.left + 500, rROI.top + 200), SW_HIDE);
	if (g_pProcessEditor != NULL)
	{
		g_pLog->Display(0, _T("ProcessEditor Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("ProcessEditor Creation Fail!"));
		return nRet;
	}


	CREATEDIALOG(g_pCommStat, CHWCommStatusDlg, this, CPoint(rROI.left + 500, rROI.top + 200), SW_HIDE);
	if (g_pCommStat != NULL)
	{
		g_pLog->Display(0, _T("HW Communication Status Display Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("HW Communication Status Display Creation Fail!"));
		return nRet;
	}


	CREATECLASS(g_pBeamCon, CBeamConnect);
	if (g_pBeamCon != NULL)
		g_pLog->Display(0, _T("Beam Optimization Connect Creation Success!"));
	else
		g_pLog->Display(0, _T("Beam Optimization Connect Creation Fail!"));


	//////////////////////////////////////////////////////// HW 통신 모듈 Create! ///////////////////////////////////////////////
	CREATECLASS(g_pAnimationGUI, CAnimationGUI);
	if (g_pAnimationGUI != NULL)
	{
		g_pLog->Display(0, _T("Animation GUI Creation Success!"));
		nRet = g_pAnimationGUI->OpenDevice();
		if (nRet == 0)
		{
			if (g_pConfig->m_bUseXGem == TRUE)
			{
				nRet = g_pAnimationGUI->ReqConfigData("CONFIG_GET_ETC");
				if (nRet == 0)
				{
					g_pConfig->ReadETCInfo();
					g_pLog->Display(0, _T("Read Etc Config Data Success!"));
				}
				else
				{
					g_pLog->Display(0, _T("Read Etc Config Data Fail!"));
				}

				nRet = g_pAnimationGUI->ReqConfigData("CONFIG_GET_CALIBRATION");
				if (nRet == 0)
				{
					g_pConfig->ReadCalibrationInfo();
					g_pLog->Display(0, _T("Read Calibration Data Success!"));
				}
				else
				{
					g_pLog->Display(0, _T("Read Calibration Data Fail!"));
				}

				nRet = g_pAnimationGUI->ReqConfigData("CONFIG_GET_RECOVERY");
				if (nRet == 0)
				{
					g_pConfig->ReadRecoveryData();
					g_pLog->Display(0, _T("Read Recovery Data Success!"));
				}
				else
				{
					g_pLog->Display(0, _T("Read Recovery Data Fail!"));
				}

				nRet = g_pAnimationGUI->ReqConfigData("CONFIG_GET_VACUUM");
				if (nRet == 0)
				{
					g_pConfig->ReadVacuumSeqData();
					g_pLog->Display(0, _T("Read Vacuum Seq Data Success!"));
				}
				else
				{
					g_pLog->Display(0, _T("Read Vacuum Seq Data Fail!"));
				}

				nRet = g_pAnimationGUI->ReqConfigData("CONFIG_GET_PTR");
				if (nRet == 0)
				{
					g_pConfig->ReadPTRInfo();
					g_pLog->Display(0, _T("Read Ptr Data Success!"));
				}
				else
				{
					g_pLog->Display(0, _T("Read Ptr Data Fail!"));
				}

				nRet = g_pAnimationGUI->ReqConfigData("CONFIG_GET_PHASE");
				if (nRet == 0)
				{
					g_pConfig->ReadPhaseInfo();
					g_pLog->Display(0, _T("Read Phase Data Success!"));
				}
				else
				{
					g_pLog->Display(0, _T("Read Phase Data Fail!"));
				}

				nRet = g_pAnimationGUI->ReqConfigData("CONFIG_GET_XRAYCAM");
				if (nRet == 0)
				{
					//g_pXrayCamera->ReadConfigFile();
					g_pLog->Display(0, _T("Read Phase Data Success!"));
				}
				else
				{
					g_pLog->Display(0, _T("Read Phase Data Fail!"));
				}
			}
		}
	}
	else
		g_pLog->Display(0, _T("Animation GUI Creation Fail!"));

	CREATEDIALOG(g_pAdam, CADAMDlg, this, CPoint(rROI.left + 2140, rROI.top + 10), SW_HIDE);	 // ADAM DLG 위치가 이상해서 2250에서 1250으로 수정함_KYD_20191024
	if (g_pAdam != NULL)
	{
		g_pLog->Display(0, _T("ADAM Creation Success!"));

		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{

			nRet = g_pAdam->OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_ADAM], g_pConfig->m_nPORT[ETHERNET_ADAM], FALSE, ADAM_RECEIVE_BUFFER_SIZE);
			
			if (nRet == 0)
			{
				WaitSec(0.5);
				g_pAdam->Command_SetAverageCount(g_pAdam->m_AverageCount);
			}

			if (nRet != 0)
			{
				g_pCommStat->ADAMCommStatus(FALSE);
				SetEquipmentStatus(_T("ADAM"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->ADAMCommStatus(TRUE);
			}
		}
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("ADAM Creation Fail!"));
		return nRet;
	}

	CREATEDIALOG(g_pCamera, CCameraDlg, this, CPoint(rROI.left + 2140, rROI.top + 10), SW_HIDE);
	if (g_pCamera != NULL)
	{
		g_pLog->Display(0, _T("OM Camera Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pCamera->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->OmCameraCommStatus(FALSE);
				SetEquipmentStatus(_T("CAMERA"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->OmCameraCommStatus(TRUE);
			}
		}
	}
	else
	{
		g_pLog->Display(0, _T("Camera Creation Fail!"));
	}

	CREATEDIALOG(g_pCamZoneplate, CCamZoneplateDlg, this, CPoint(rROI.left + 2140, rROI.top + 10), SW_HIDE);
	if (g_pCamZoneplate != NULL)
	{
		g_pLog->Display(0, _T("Zoneplate Camera Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			//nRet = g_pCamZoneplate->OpenDevice();
			if (nRet != 0)
			{
				//g_pCommStat->OmCameraCommStatus(FALSE);
				//SetEquipmentStatus(_T("CAMERA"));
				WaitSec(3);
			}
			else
			{
				//g_pCommStat->OmCameraCommStatus(TRUE);
			}
		}
	}
	else
	{
		g_pLog->Display(0, _T("ZP Camera Creation Fail!"));
	}

	CREATEDIALOG(g_pRecipe, CRecipeEditorDlg, this, CPoint(rROI.left + 2130, rROI.top + 1590), SW_HIDE);
	if (g_pRecipe != NULL)
	{
		g_pLog->Display(0, _T("CRecipeEditorDlg Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("CRecipeEditorDlg Creation Fail!"));
		return nRet;
	}

	CREATEDIALOG(g_pXrayCamera, CXRayCameraDlg, this, CPoint(rROI.left + 2140, rROI.top + 10), SW_HIDE);
	if (g_pXrayCamera != NULL)
	{
		g_pLog->Display(0, _T("XRay Camera Creation Success!"));

		if (g_pConfig->m_nEquipmentType == PHASE)
		{
			nRet = g_pXrayCamera->OpenDevice();
			if (nRet == 0)
			{
				g_pCommStat->XRayCameraCommStatus(FALSE);
				SetEquipmentStatus(_T("XRAY"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->XRayCameraCommStatus(TRUE);
			}
		}
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("XRay Camera Creation Fail!"));
		return nRet;
	}

	CREATEDIALOG(g_pPhase, CPhaseDlg, this, CPoint(rROI.left + 2140, rROI.top + 1300), SW_HIDE);
	if (g_pPhase != NULL)
	{
		g_pLog->Display(0, _T("Phase Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("Phase Creation Fail!"));
		return nRet;
	}

	CREATEDIALOG(g_pXrayCameraConfig, CXrayCameraConfigDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pXrayCameraConfig != NULL)
	{
		g_pLog->Display(0, _T("XRay Camera Config Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("XRay Camera Config Creation Fail!"));
		return nRet;
	}

	CREATEDIALOG(g_pPTR, CPTRDlg, this, CPoint(rROI.left + 2110, rROI.top), SW_HIDE);
	if (g_pPTR != NULL)
	{
		g_pLog->Display(0, _T("PTR Dialog Creation Success!"));
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("PTR Dialog Creation Fail!"));
		return nRet;
	}

	CREATEDIALOG(g_pGauge_IO, CVacuumGaugeDlg, this, CPoint(rROI.left + 1550, rROI.top + 200), SW_HIDE);
	if (g_pGauge_IO != NULL)
	{
		g_pLog->Display(0, _T("Vacuum Gauge Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pGauge_IO->OpenPort(g_pConfig->m_chPORT[SERIAL_VACUUMGAUGE], g_pConfig->m_nBAUD_RATE[SERIAL_VACUUMGAUGE], g_pConfig->m_nUSE_BIT[SERIAL_VACUUMGAUGE], g_pConfig->m_nSTOP_BIT[SERIAL_VACUUMGAUGE], g_pConfig->m_nPARITY[SERIAL_VACUUMGAUGE]);
			if (nRet != 0)
			{
				g_pCommStat->LLCGaugeCommStatus(FALSE);
				SetEquipmentStatus(_T("LLC GAUGE"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->LLCGaugeCommStatus(TRUE);
			}
		}
	}
	else
	{
		g_pLog->Display(0, _T("LLC Vacuum Gauge Creation Fail!"));
	}

	CREATEDIALOG(g_pIO, CIODlg, this, CPoint(rROI.left + 500, rROI.top + 200), SW_HIDE);
	if (g_pIO != NULL)
	{
		g_pLog->Display(0, _T("IO Module Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pIO->OpenDevice(g_pConfig->m_chIP[ETHERNET_CREVIS]);
			if (nRet != 0)
			{
				g_pCommStat->CrevisCommStatus(FALSE);
				SetEquipmentStatus(_T("CREVIS"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->CrevisCommStatus(TRUE);
			}
		}
	}
	else
	{
		g_pLog->Display(0, _T("IO Module Creation Fail!"));
	}

	CREATEDIALOG(g_pGemAutomation, CGemAutomationDlg, this, CPoint(rROI.left + 500, rROI.top + 200), SW_HIDE);
	if (g_pGemAutomation != NULL)
		g_pLog->Display(0, _T("GEM Automation Creation Success!"));
	else
		g_pLog->Display(0, _T("GEM Automation Creation Fail!"));

	CREATEDIALOG(g_pEfem, CMaskEFEMDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pEfem != NULL)
	{
		g_pLog->Display(0, _T("Mask Transfer System Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pEfem->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->EfemCommStatus(FALSE);
				SetEquipmentStatus(_T("MTS"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->EfemCommStatus(TRUE);
			}
		}
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("Mask Transfer System Creation Fail!"));
		return nRet;
	}


	CREATEDIALOG(g_pNavigationStage, CNavigationStageDlg, this, CPoint(rROI.left + 430, rROI.top + 1320), SW_HIDE);
	if (g_pNavigationStage != NULL)
	{
		g_pLog->Display(0, _T("XY Stage Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pNavigationStage->ConnectACSController(ETHERNET, g_pConfig->m_chIP[ETHERNET_NAVI_STAGE], g_pConfig->m_nPORT[ETHERNET_NAVI_STAGE]);
			if (nRet == FALSE)
			{
				g_pCommStat->NavigationStageCommStatus(FALSE);
				SetEquipmentStatus(_T("NAVI STAGE"));
				WaitSec(3);
			}
			else if (nRet == TRUE)
			{
				g_pCommStat->NavigationStageCommStatus(TRUE);
				//g_pNavigationStage->SetLaserSwitchingFunction(FALSE);
			}
			else
			{
			}

		}
	}
	else
		g_pLog->Display(0, _T("XY Stage Creation Fail!"));


	CREATEDIALOG(g_pScanStage, CScanStageDlg, this, CPoint(rROI.left + 430, rROI.top + 770), SW_HIDE);
	if (g_pScanStage != NULL)
	{
		g_pLog->Display(0, _T("Scan Stage Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE && g_pConfig->m_nEquipmentType != EUVPTR)
		{
			nRet = g_pScanStage->ConnectComm(ETHERNET, g_pConfig->m_chIP[ETHERNET_SCAN_STAGE], g_pConfig->m_nPORT[ETHERNET_SCAN_STAGE]);
			if (nRet == FALSE)
			{
				g_pCommStat->ScanStageCommStatus(FALSE);
				SetEquipmentStatus(_T("SCAN STAGE"));
				WaitSec(3);
			}
			else if (nRet == TRUE)
			{
				g_pScanStage->m_dInitialTx_urad = g_pConfig->m_dConfigCalTx_urad;
				g_pScanStage->m_dInitialTy_urad = g_pConfig->m_dConfigCalTy_urad;
				g_pScanStage->Initialize_PIStage();
				g_pCommStat->ScanStageCommStatus(TRUE);
			}
			else
			{
			}
		}
	}
	else
		g_pLog->Display(0, _T("Scan Stage Creation Fail!"));

	CREATEDIALOG(g_pFilterStage, CFilterStageDlg, this, CPoint(rROI.left + 1500, rROI.top + 200), SW_HIDE);
	if (g_pFilterStage != NULL)
	{
		g_pLog->Display(0, _T("Filter Stage Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pFilterStage->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->FilterStageCommStatus(FALSE);
				SetEquipmentStatus(_T("FILTER STAGE"));
				WaitSec(3);
			}
			else
			{
				g_pFilterStage->Initialize_PIStage();
				g_pCommStat->FilterStageCommStatus(TRUE);
			}
		}
	}
	else
		g_pLog->Display(0, _T("Filter Stage Creation Fail!"));

	//
	// 사용 예정. IO 와 혼돈 방지를 위한 임시 주석 처리
	//
	//CREATEDIALOG(g_pMCTmp, CTurboPumpMCDlg, this, CPoint(rROI.left + 500, rROI.top + 200), SW_HIDE);
	//if (g_pMCTmp != NULL)
	//	g_pLog->Display(0, _T("MC Turbo Pump Creation Success!"));
	//else
	//	g_pLog->Display(0, _T("MC Turbo Pump Creation Fail!"));
	//

	CREATEDIALOG(g_pMCTmp_IO, CTurboPumpMCDlg, this, CPoint(rROI.left + 450, rROI.top + 200), SW_HIDE);
	if (g_pMCTmp_IO != NULL)
	{
		g_pLog->Display(0, _T("MC Turbo Pump Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pMCTmp_IO->OpenPort(g_pConfig->m_chPORT[SERIAL_MC_TMP], g_pConfig->m_nBAUD_RATE[SERIAL_MC_TMP], g_pConfig->m_nUSE_BIT[SERIAL_MC_TMP], g_pConfig->m_nSTOP_BIT[SERIAL_MC_TMP], g_pConfig->m_nPARITY[SERIAL_MC_TMP]);
			if (nRet != 0)
			{
				g_pCommStat->MCTmpCommStatus(FALSE);
				SetEquipmentStatus(_T("MC TMP"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->MCTmpCommStatus(TRUE);
			}

		}
	}
	else
		g_pLog->Display(0, _T("MC Turbo Pump Creation Fail!"));

	//
	// 사용 예정. IO 와 혼돈 방지를 위한 임시 주석 처리
	//
	//CREATEDIALOG(g_pLLKTmp, CTurboPumpDlg, this, CPoint(rROI.left + 500, rROI.top + 600), SW_HIDE);
	//if (g_pLLKTmp != NULL)
	//	g_pLog->Display(0, _T("LLC Turbo Pump Creation Success!"));
	//else
	//	g_pLog->Display(0, _T("LLC Turbo Pump Creation Fail!"));

	CREATEDIALOG(g_pLLKTmp_IO, CTurboPumpDlg, this, CPoint(rROI.left + 450, rROI.top + 800), SW_HIDE);
	if (g_pLLKTmp_IO != NULL)
	{
		g_pLog->Display(0, _T("LLC Turbo Pump Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pLLKTmp_IO->OpenPort(g_pConfig->m_chPORT[SERIAL_LLC_TMP], g_pConfig->m_nBAUD_RATE[SERIAL_LLC_TMP], g_pConfig->m_nUSE_BIT[SERIAL_LLC_TMP], g_pConfig->m_nSTOP_BIT[SERIAL_LLC_TMP], g_pConfig->m_nPARITY[SERIAL_LLC_TMP]);
			if (nRet != 0)
			{
				g_pCommStat->LLCGaugeCommStatus(FALSE);
				SetEquipmentStatus(_T("LLC TMP"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->LLCGaugeCommStatus(TRUE);
			}
		}
	}
	else
		g_pLog->Display(0, _T("LLC Turbo Pump Creation Fail!"));


	//
	// 사용 예정. IO 와 혼돈 방지를 위한 임시 주석 처리
	//
	//CREATEDIALOG(g_pMCGauge, CVacuumGaugeDlg, this, CPoint(rROI.left + 500, rROI.top + 700), SW_HIDE);
	//if (g_pMCGauge != NULL)
	//{
	//	g_pLog->Display(0, _T("MC Vacuum Gauge Creation Success!"));
	//	if (g_pConfig->m_nEquipmentMode != OFFLINE)
	//	{
	//		nRet = g_pMCGauge->OpenPort(g_pConfig->m_chPORT[SERIAL_VACUUMGAUGE], g_pConfig->m_nBAUD_RATE[SERIAL_VACUUMGAUGE], g_pConfig->m_nUSE_BIT[SERIAL_VACUUMGAUGE], g_pConfig->m_nSTOP_BIT[SERIAL_VACUUMGAUGE], g_pConfig->m_nPARITY[SERIAL_VACUUMGAUGE]);
	//		if (nRet != 0)
	//			g_pCommStat->MCGaugeCommStatus(FALSE);
	//		else
	//			g_pCommStat->MCGaugeCommStatus(TRUE);
	//	}
	//}
	//else
	//{
	//	g_pLog->Display(0, _T("MC Vacuum Gauge Creation Fail!"));
	//}

	//
	// 사용 예정. IO 와 혼돈 방지를 위한 임시 주석 처리
	//
	//CREATEDIALOG(g_pLLCGauge, CVacuumGaugeDlg, this, CPoint(rROI.left + 800, rROI.top + 700), SW_HIDE);
	//if (g_pLLCGauge != NULL)
	//{
	//	g_pLog->Display(0, _T("LLC Vacuum Gauge Creation Success!"));
	//	if (g_pConfig->m_nEquipmentMode != OFFLINE)
	//	{
	//		nRet = g_pLLCGauge->OpenPort(g_pConfig->m_chPORT[SERIAL_VACUUMGAUGE], g_pConfig->m_nBAUD_RATE[SERIAL_VACUUMGAUGE], g_pConfig->m_nUSE_BIT[SERIAL_VACUUMGAUGE], g_pConfig->m_nSTOP_BIT[SERIAL_VACUUMGAUGE], g_pConfig->m_nPARITY[SERIAL_VACUUMGAUGE]);
	//		if (nRet != 0)
	//			g_pCommStat->LLCGaugeCommStatus(FALSE);
	//		else
	//			g_pCommStat->LLCGaugeCommStatus(TRUE);
	//	}
	//}
	//else
	//{
	//	g_pLog->Display(0, _T("LLC Vacuum Gauge Creation Fail!"));
	//}




	CREATEDIALOG(g_pVacuumRobot, CVacuumRobotDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pVacuumRobot != NULL)
	{
		g_pLog->Display(0, _T("Vacuum Robot Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pVacuumRobot->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->VacuumRobotCommStatus(FALSE);
				SetEquipmentStatus(_T("VMTR"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->VacuumRobotCommStatus(TRUE);
			}
		}
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("Vacuum Robot Creation Fail!"));
		return nRet;
	}
	CREATEDIALOG(g_pEUVSource, CEUVSourceDlg, this, CPoint(rROI.left + 450, rROI.top + 200), SW_HIDE);
	if (g_pEUVSource != NULL)
	{
		g_pLog->Display(0, _T("EUVSource Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pEUVSource->OpenDevice();
			if (nRet != 0)
			{
				g_pCommStat->EUVSourceCommStatus(FALSE);
				SetEquipmentStatus(_T("EUV SRC"));
				WaitSec(3);
			}
			else
			{
				g_pCommStat->EUVSourceCommStatus(TRUE);
			}
		}
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("EUVSource Creation Fail!"));
		return nRet;
	}
	CREATEDIALOG(g_pLightCtrl, CLightControllerDlg, this, CPoint(rROI.left + 1400, rROI.top + 200), SW_HIDE);
	if (g_pLightCtrl != NULL)
	{
		g_pLog->Display(0, _T("Light Controller Creation Success!"));
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pLightCtrl->OpenDevice();
			if (nRet != 0)
			{
				//g_pCommStat->EUVSourceCommStatus(FALSE);
				SetEquipmentStatus(_T("LIGHT"));
				WaitSec(3);
			}
			else
			{
				//g_pCommStat->EUVSourceCommStatus(TRUE);
			}
		}
	}
	else
	{
		nRet = -1;
		g_pLog->Display(0, _T("EUVSource Creation Fail!"));
		return nRet;
	}

	//CREATEDIALOG(g_pScanStageTest, CScanStageFlatnessTestDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	CREATEDIALOG(g_pScanStageTest, CScanStageFlatnessTestDlg, this, CPoint(rROI.left + 2140, rROI.top + 10), SW_HIDE);
	if (g_pScanStageTest != NULL)
		g_pLog->Display(0, _T("System Test Scan Stage Test Dialog Creation Success!"));
	else
		g_pLog->Display(0, _T("System Test Scan Stage Test Dialog Creation Fail!"));

	CREATEDIALOG(g_pNavigationStageTest, CNavigationStageTestDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pNavigationStageTest != NULL)
		g_pLog->Display(0, _T("System Test Navigation Stage Test Dialog Creation Success!"));
	else
		g_pLog->Display(0, _T("System Test Navigation Stage Test Dialog Creation Fail!"));

	CREATEDIALOG(g_pSourceTest, CSourceTestDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pSourceTest != NULL)
		g_pLog->Display(0, _T("System Test Source Contamination Test Dialog Creation Success!"));
	else
		g_pLog->Display(0, _T("System Test Source Contamination Test Dialog Creation Fail!"));


	CREATEDIALOG(g_pTest, CSystemTestDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pTest != NULL)
		g_pLog->Display(0, _T("System Test Dialog Creation Success!"));
	else
		g_pLog->Display(0, _T("System Test Dialog Creation Fail!"));
	CREATEDIALOG(g_pMaskMap, CMaskMapDlg, this, CPoint(rROI.left + 430, rROI.top + 10), SW_HIDE);
	if (g_pMaskMap != NULL)
		g_pLog->Display(0, _T("Mask Map Dialog Creation Success!"));
	else
		g_pLog->Display(0, _T("Mask Map Dialog Creation Fail!"));
	CREATECLASS(g_pAP, CAutoProcess);
	if (g_pAP != NULL)
	{
		g_pAP->Initialize();
		g_pLog->Display(0, _T("Auto Process Creation Success!"));
	}
	else
		g_pLog->Display(0, _T("Auto Process Creation Fail!"));


	CREATECLASS(g_pVP, CVacuumProcess);
	if (g_pVP != NULL)
		g_pLog->Display(0, _T("Vacuum Process Creation Success!"));
	else
		g_pLog->Display(0, _T("Vacuum Process Creation Fail!"));
	
	CREATEDIALOG(g_pMaindialog, CMaindialog, this, CPoint(rROI.left + 2135, rROI.top + 50), SW_HIDE);
	if (g_pMaindialog != NULL) g_pLog->Display(0, _T("SREM Main Dialog Creation Success!"));
	else g_pLog->Display(0, _T("SREM Main Dialog Creation Fail!"));

	/* 창 변경 전 440, 1400 */
	//CREATEDIALOG(g_pChart, CChartdirDlg, this, CPoint(rROI.left + 440, rROI.top + 1350), SW_HIDE);
	CREATEDIALOG(g_pChart, CChartdirDlg, this, CPoint(rROI.left + 2140, rROI.top + 1350), SW_HIDE);
	if (g_pChart != NULL) g_pLog->Display(0, _T("Chart Dialog Creation Success!"));
	else g_pLog->Display(0, _T("Chart Dialog Creation Fail!"));

	/* 창 변경 전 1270, 1400 */
	CREATEDIALOG(g_pChartline, CChartdirLineDlg, this, CPoint(rROI.left + 1270, rROI.top + 1350), SW_HIDE);
	if (g_pChartline != NULL) g_pLog->Display(0, _T("Chart Dialog Creation Success!"));
	else g_pLog->Display(0, _T("Chart Dialog Creation Fail!"));

	/* 창 변경 전 430, 200 */
	CREATEDIALOG(g_pChartstage, CChartdirStageDlg, this, CPoint(rROI.left + 430, rROI.top + 200), SW_HIDE);
	if (g_pChartstage != NULL) g_pLog->Display(0, _T("Chart Dialog Creation Success!"));
	else g_pLog->Display(0, _T("Chart Dialog Creation Fail!"));



	WaitSec(3);
	g_pLoadingScreen.SetLoadingComplete();

	SetDisplay(OPERATOR);

	return nRet;
}

BOOL CEUVSolutionDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CEUVSolutionDlg::OnBnClickedOperatorButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedOperatorButton() 버튼 클릭!"));

	SetDisplay(OPERATOR);
}



void CEUVSolutionDlg::OnBnClickedEngineerButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedEngineerButton() 버튼 클릭!"));

	SetDisplay(ENGINEER);
}

void CEUVSolutionDlg::OnBnClickedButtonMainEuvOn()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedButtonMainEuvOn() 버튼 클릭!"));

	if (g_pEUVSource == NULL) return;

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		return;
	}

	// EUV on시 기본은 셔터 닫음
	g_pEUVSource->SetMechShutterOpen(FALSE);

	/*if (g_pConfig->m_nEquipmentType == SREM033)
	{
		g_pEUVSource->SetMechShutterOpen(FALSE);
	}
	else  if (g_pConfig->m_nEquipmentType == PHASE)
	{
		g_pEUVSource->SetMechShutterOpen(FALSE);
	}
	else  if (g_pConfig->m_nEquipmentType == EUVPTR)
	{
		g_pEUVSource->SetMechShutterOpen(FALSE);
	}
	else
	{
		g_pEUVSource->SetMechShutterOpen(FALSE);
	}*/

	if(g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
}


void CEUVSolutionDlg::OnBnClickedButtonMainEuvOff()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedButtonMainEuvOff() 버튼 클릭!"));

	if (g_pEUVSource == NULL) return;

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	g_pEUVSource->SetEUVSourceOn(FALSE);
	g_pEUVSource->SetMechShutterOpen(FALSE);
}


void CEUVSolutionDlg::OnBnClickedButtonMainShutterOpen()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedButtonMainShutterOpen() 버튼 클릭!"));

	if (g_pEUVSource == NULL) return;

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	g_pEUVSource->SetMechShutterOpen(TRUE);
}


void CEUVSolutionDlg::OnBnClickedButtonMainShutterClose()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedButtonMainShutterClose() 버튼 클릭!"));

	if (g_pEUVSource == NULL) return;

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	g_pEUVSource->SetMechShutterOpen(FALSE);
}


int CEUVSolutionDlg::SetDisplay(int Mode)
{
	//	g_pMCTmp->ShowWindow(SW_HIDE);
	//	g_pMC2Tmp->ShowWindow(SW_HIDE);
	//	g_pLLKTmp->ShowWindow(SW_HIDE);
	//	g_pMCGauge->ShowWindow(SW_HIDE);
	//	g_pLLCGauge->ShowWindow(SW_HIDE);
	g_pXrayCamera->ShowWindow(SW_HIDE);
	g_pPhase->ShowWindow(SW_HIDE);
	g_pXrayCameraConfig->ShowWindow(SW_HIDE);
	if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
	g_pCamera->ShowWindow(SW_HIDE);
	g_pCamZoneplate->ShowWindow(SW_HIDE);
	g_pEfem->ShowWindow(SW_HIDE);
	g_pNavigationStage->ShowWindow(SW_HIDE);
	g_pScanStage->ShowWindow(SW_HIDE);
	g_pMCTmp_IO->ShowWindow(SW_HIDE);
	g_pGauge_IO->ShowWindow(SW_HIDE);
	g_pLLKTmp_IO->ShowWindow(SW_HIDE);
	g_pVacuumRobot->ShowWindow(SW_HIDE);
	g_pConfig->ShowWindow(SW_HIDE);
	g_pRecipe->ShowWindow(SW_HIDE);
	g_pIO->ShowWindow(SW_HIDE);
	g_pCommStat->ShowWindow(SW_HIDE);
	g_pGemAutomation->ShowWindow(SW_HIDE);
	g_pProcessEditor->ShowWindow(SW_HIDE);
	g_pTest->ShowWindow(SW_HIDE);
	g_pLog->ShowWindow(SW_HIDE);
	g_pSubMenu->ShowWindow(SW_HIDE);
	g_pEUVSource->ShowWindow(SW_HIDE);
	g_pMaskMap->ShowWindow(SW_HIDE);
	if(g_pMaindialog != NULL) g_pMaindialog->ShowWindow(SW_HIDE);
	g_pChart->ShowWindow(SW_HIDE);
	g_pChartline->ShowWindow(SW_HIDE);
	g_pChartstage->ShowWindow(SW_HIDE);
	g_pFilterStage->ShowWindow(SW_HIDE);
	g_pLightCtrl->ShowWindow(SW_HIDE);
	if (g_pPTR != NULL) g_pPTR->ShowWindow(SW_HIDE);
	g_pNavigationStageTest->ShowWindow(SW_HIDE);
	g_pScanStageTest->ShowWindow(SW_HIDE);
	g_pSourceTest->ShowWindow(SW_HIDE);
	g_pSubTestMenu->ShowWindow(SW_HIDE);
	//g_pPTR->ShowWindow(SW_HIDE);

	//Stage 위치를 확인해서 OM or EUV 영상 Dlg를 띄우자
	double current_posx_mm = 0.0, current_posy_mm = 0.0;
	current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	CPoint pt;
	pt.x = current_posx_mm;
	pt.y = current_posy_mm;
	if (g_pConfig->m_rcEUVStageAreaRect.PtInRect(pt) == TRUE)
	{
		g_pRecipe->ShowWindow(SW_HIDE);
		g_pCamera->ShowWindow(SW_HIDE);

		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_SHOW);
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_SHOW);
			g_pPhase->ShowWindow(SW_SHOW);
			break;
		case EUVPTR:
			if (g_pPTR != NULL) g_pPTR->ShowWindow(SW_SHOW);
			break;
		case ELITHO:
			//g_pXrayCamera->ShowWindow(SW_SHOW);
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}

		g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType = EUV;
		g_pMaskMap->m_CheckOMAdamCtrl.SetCheck(TRUE);
		g_pMaskMap->m_CheckOMAdamCtrl.SetWindowText(_T("To Optic Microscopy"));
	}
	else
	{
		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_HIDE);
			g_pPhase->ShowWindow(SW_HIDE);
			break;
		case EUVPTR:
			if (g_pPTR != NULL) g_pPTR->ShowWindow(SW_HIDE);
			break;
		case ELITHO:
			g_pXrayCamera->ShowWindow(SW_HIDE);
			g_pPhase->ShowWindow(SW_HIDE);
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
			break;
		default:
			break;
		}

		g_pCamera->ShowWindow(SW_SHOW);
		g_pRecipe->ShowWindow(SW_SHOW);

		g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		g_pMaskMap->m_CheckOMAdamCtrl.SetCheck(FALSE);
		g_pMaskMap->m_CheckOMAdamCtrl.SetWindowText(_T("To EUV Microscopy"));
	}

	switch (Mode)
	{
	case OPERATOR:
		//switch (g_pMaskMap->m_CheckOMAdamCtrl.GetCheck())
		//{
		//case BST_CHECKED:
		//	g_pCamera->ShowWindow(SW_HIDE);
		//	g_pAdam->ShowWindow(SW_SHOW);
		//	g_pMaskMap->m_CheckOMAdamCtrl.SetWindowText(_T("To Optic Microscopy"));
		//	break;
		//case BST_UNCHECKED:
		//	g_pAdam->ShowWindow(SW_HIDE);
		//	g_pCamera->ShowWindow(SW_SHOW);
		//	g_pRecipe->ShowWindow(SW_SHOW);
		//	g_pMaskMap->m_CheckOMAdamCtrl.SetWindowText(_T("To EUV Microscopy"));
		//	break;
		//default:
		//	break;
		//}
		g_pMaskMap->ShowWindow(SW_SHOW);
		break;
	case ENGINEER:
		g_pSubMenu->ShowWindow(SW_SHOW);
		//g_pCamera->ShowWindow(SW_SHOW);		
		g_pEfem->ShowWindow(SW_SHOW);
		g_pRecipe->ShowWindow(SW_HIDE);
		g_pLog->ShowWindow(SW_SHOW);
		break;
	case CONFIGURATION:
		g_pCamera->ShowWindow(SW_HIDE);
		g_pConfig->ShowWindow(SW_SHOW);
		g_pRecipe->ShowWindow(SW_HIDE);
		break;
	case EVALUATION:
		g_pTest->ShowWindow(SW_SHOW);
		//g_pCamera->ShowWindow(SW_SHOW);
		//g_pRecipe->ShowWindow(SW_SHOW);
		g_pSubTestMenu->ShowWindow(SW_SHOW);
		//g_pChart->ShowWindow(SW_SHOW);
		//g_pChartline->ShowWindow(SW_SHOW);
		break;
	default:
		break;
	}

	return 0;
}

int CEUVSolutionDlg::DisplayVersion()
{
	CString strText;
	CFont font, *pOldFont;
	CBrush br(BLACK);
	CRect rc;
	strText = GetVersion();
	CStatic* pFrame = (CStatic*)GetDlgItem(IDC_STATIC_ESOL_LOGO);
	pFrame->GetClientRect(rc);
	CClientDC dc(pFrame);
	CBrush* pOld = dc.SelectObject(&br);
	dc.SetBkMode(TRANSPARENT);
	dc.SetTextColor(PURPLE);
	dc.SetTextCharacterExtra(3);
	font.CreateFont(25, 13, 0, 0, FW_NORMAL, TRUE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("HY헤드라인M"));
	pOldFont = (CFont*)dc.SelectObject(&font);

	dc.TextOut(13, 240, strText);
	font.DeleteObject();
	dc.SelectObject(pOld);
	return 0;
}

CString CEUVSolutionDlg::GetVersion(void)
{
	CString strSWVersion;
	CWinApp *pWinApp = AfxGetApp();
	CString sAppName;
	sAppName.Format("%s.exe", pWinApp->m_pszExeName);
	char temp_path[125];
	GetModuleFileName(AfxGetInstanceHandle(), temp_path, sizeof(temp_path));

	// 버전정보를 담을 버퍼
	char* buffer = NULL;

	DWORD infoSize = 0;

	// 파일로부터 버전정보데이터의 크기가 얼마인지를 구합니다.
	infoSize = GetFileVersionInfoSize(temp_path, 0);
	if (infoSize == 0) return "";

	// 버퍼할당
	buffer = new char[infoSize];
	if (buffer)
	{
		// 버전정보데이터를 가져옵니다.
		if (GetFileVersionInfo(temp_path, 0, infoSize, buffer) != 0)
		{
			VS_FIXEDFILEINFO* pFineInfo = NULL;
			UINT bufLen = 0;
			// buffer로 부터 VS_FIXEDFILEINFO 정보를 가져옵니다.
			if (VerQueryValue(buffer, "\\", (LPVOID*)&pFineInfo, &bufLen) != 0)
			{
				WORD majorVer, minorVer, buildNum, revisionNum;
				majorVer = HIWORD(pFineInfo->dwFileVersionMS);
				minorVer = LOWORD(pFineInfo->dwFileVersionMS);
				buildNum = HIWORD(pFineInfo->dwFileVersionLS);
				revisionNum = LOWORD(pFineInfo->dwFileVersionLS);

				strSWVersion.Format("S/W VERSION %d.%d.%d.%d", majorVer, minorVer, buildNum, revisionNum);
			}
		}
		delete[] buffer;
	}

	return strSWVersion;
}


void CEUVSolutionDlg::OnBnClickedConfigurationButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedConfigurationButton() 버튼 클릭!"));

	SetDisplay(CONFIGURATION);
}


void CEUVSolutionDlg::OnBnClickedEvaluationButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedEvaluationButton() 버튼 클릭!"));

	SetDisplay(EVALUATION);
}

void CEUVSolutionDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == MAIN_DIALOG_INFO_TIMER)
	{
		GetEquipmentInfo();
		SetTimer(MAIN_DIALOG_INFO_TIMER, 100, NULL);
	}
	__super::OnTimer(nIDEvent);
}

void CEUVSolutionDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	//상단부 마우스 좌클릭으로 윈도우 이동
	if (point.y < 15)
	{
		SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0);
	}

	__super::OnLButtonDown(nFlags, point);
}


void CEUVSolutionDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	//상단부 마우스 더블클릭시 윈도우 최대화
	if (point.y < 15)
	{
		SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, 0);
		//ShowWindow(SW_SHOWMAXIMIZED);

		LONG style = ::GetWindowLong(m_hWnd, GWL_STYLE);

		style &= ~WS_CAPTION;
		style &= ~WS_SYSMENU;

		::SetWindowLong(m_hWnd, GWL_STYLE, style);
		int screenx = GetSystemMetrics(SM_CXSCREEN);
		int screeny = GetSystemMetrics(SM_CYSCREEN);

		SetWindowPos(NULL, -10, -10, screenx + 30, screeny - 30, SWP_NOZORDER);
	}

	__super::OnLButtonDblClk(nFlags, point);
}
