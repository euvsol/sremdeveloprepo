#pragma once

class CLightControllerDlg :	public CDialogEx , public CLightCtrl
{
	DECLARE_DYNAMIC(CLightControllerDlg)

public:
	CLightControllerDlg(CWnd* pParent = nullptr);
	virtual ~CLightControllerDlg();

	enum { IDD = IDD_LIGHT_CTRL_DIALOG };

protected:
	HICON			m_LedIcon[3];

	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();

	void InitializeControls();
	CSliderCtrl m_LightValueCtrl;
	CEdit m_CurrentValue;
	afx_msg void OnBnClickedLightCtrlOnoffButton();
	afx_msg void OnNMCustomdrawLightCtrlSlider(NMHDR *pNMHDR, LRESULT *pResult);

	int OpenDevice();
	int LightOn();
	int LightOff();
	int SetLightValueDlg(int nValue);
	afx_msg void OnBnClickedLightCtrlSetButton();

	BOOL Is_Connected() { return m_bSerialConnected; }
};

