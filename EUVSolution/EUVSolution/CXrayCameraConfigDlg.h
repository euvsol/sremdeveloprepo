﻿/**
 * Xray Camera Configuration Dialog Class
 *
 * Copyright 2020 by E-SOL, Inc.,
 *
 */
#pragma once

// CXrayCameraConfigDlg 대화 상자

class CXrayCameraConfigDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CXrayCameraConfigDlg)

public:
	static CXrayCameraConfigDlg* m_pDlgInst;

	CXrayCameraConfigDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CXrayCameraConfigDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_XRAY_CAMERA_CONFIG_DIALOG};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:		
	virtual BOOL OnInitDialog();

private:
	CXRayCameraCtrl* m_XrayCameraInstance = NULL;

	void UpdataDeviceDataUi();
	//void GetXrayCameraControlParamterFromConfig();
	void CXrayCameraConfigDlg::UpdateConfigDataUi();
	//void GetXrayCameraControlParamterFromDevice();

public:
	afx_msg void OnBnClickedButtonReadConfigXrayCamera();	
	afx_msg void OnBnClickedButtonReadXrayCamera();
	afx_msg void OnBnClickedButtonSetXrayCamera();
};
