﻿// CScanStageFlatnessTestDlg.cpp: 구현 파일
//


#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <iomanip>


// CScanStageFlatnessTestDlg 대화 상자

IMPLEMENT_DYNAMIC(CScanStageFlatnessTestDlg, CDialogEx)

CScanStageFlatnessTestDlg::CScanStageFlatnessTestDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PI_STAGE_FLATNESS_TEST_DIALOG, pParent)
{
	// PI 스테이지 측정 스레드
	m_pPIstageManualThread = NULL;
	m_PIstageManualThreadExitFlag = TRUE;
}

CScanStageFlatnessTestDlg::~CScanStageFlatnessTestDlg()
{
	if (m_pPIstageManualThread != NULL)
	{
		m_PIstageManualThreadExitFlag = TRUE;
		if (WaitForSingleObject(m_pPIstageManualThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_pPIstageManualThread->m_hThread, 0);
		}
	}
}

void CScanStageFlatnessTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_REF_X2_CTL, m_point_user_num_ctl);
	DDX_Control(pDX, IDC_PROGRESS_MASK_FLAT_CTL, m_progress_mask_flat);
	DDX_Control(pDX, IDC_COMBO_CAP_SEL, m_combox_cap);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_1_X_CTL2_TEST, m_TestXPos[0]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_2_X_CTL2_TEST, m_TestXPos[1]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_3_X_CTL2_TEST, m_TestXPos[2]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_4_X_CTL2_TEST, m_TestXPos[3]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_5_X_CTL2_TEST, m_TestXPos[4]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_6_X_CTL2_TEST, m_TestXPos[5]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_7_X_CTL2_TEST, m_TestXPos[6]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_8_X_CTL2_TEST, m_TestXPos[7]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_9_X_CTL2_TEST, m_TestXPos[8]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_10_X_CTL2_TEST, m_TestXPos[9]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_1_Y_CTL2_TEST, m_TestYPos[0]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_2_Y_CTL2_TEST, m_TestYPos[1]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_3_Y_CTL2_TEST, m_TestYPos[2]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_4_Y_CTL2_TEST, m_TestYPos[3]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_5_Y_CTL2_TEST, m_TestYPos[4]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_6_Y_CTL2_TEST, m_TestYPos[5]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_7_Y_CTL2_TEST, m_TestYPos[6]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_8_Y_CTL2_TEST, m_TestYPos[7]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_9_Y_CTL2_TEST, m_TestYPos[8]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_10_Y_CTL2_TEST, m_TestYPos[9]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_1_CAP2_CTL2_TEST, m_TestCapPos[0]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_2_CAP2_CTL2_TEST, m_TestCapPos[1]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_3_CAP2_CTL2_TEST, m_TestCapPos[2]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_4_CAP2_CTL2_TEST, m_TestCapPos[3]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_5_CAP2_CTL2_TEST, m_TestCapPos[4]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_6_CAP2_CTL2_TEST, m_TestCapPos[5]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_7_CAP2_CTL2_TEST, m_TestCapPos[6]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_8_CAP2_CTL2_TEST, m_TestCapPos[7]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_9_CAP2_CTL2_TEST, m_TestCapPos[8]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_10_CAP2_CTL2_TEST, m_TestCapPos[9]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_1_CAP2_ERROR_TEST, m_TestErrorCap[0]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_2_CAP2_ERROR_TEST, m_TestErrorCap[1]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_3_CAP2_ERROR_TEST, m_TestErrorCap[2]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_4_CAP2_ERROR_TEST, m_TestErrorCap[3]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_5_CAP2_ERROR_TEST, m_TestErrorCap[4]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_6_CAP2_ERROR_TEST, m_TestErrorCap[5]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_7_CAP2_ERROR_TEST, m_TestErrorCap[6]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_8_CAP2_ERROR_TEST, m_TestErrorCap[7]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_9_CAP2_ERROR_TEST, m_TestErrorCap[8]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_10_CAP2_ERROR_TEST, m_TestErrorCap[9]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_1_X_CTL, m_XPos[0]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_2_X_CTL, m_XPos[1]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_3_X_CTL, m_XPos[2]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_4_X_CTL, m_XPos[3]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_5_X_CTL, m_XPos[4]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_6_X_CTL, m_XPos[5]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_7_X_CTL, m_XPos[6]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_8_X_CTL, m_XPos[7]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_9_X_CTL, m_XPos[8]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_10_X_CTL, m_XPos[9]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_1_Y_CTL, m_YPos[0]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_2_Y_CTL, m_YPos[1]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_3_Y_CTL, m_YPos[2]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_4_Y_CTL, m_YPos[3]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_5_Y_CTL, m_YPos[4]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_6_Y_CTL, m_YPos[5]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_7_Y_CTL, m_YPos[6]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_8_Y_CTL, m_YPos[7]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_9_Y_CTL, m_YPos[8]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_10_Y_CTL, m_YPos[9]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_1_CAP2_CTL, m_CapPos[0]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_2_CAP2_CTL, m_CapPos[1]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_3_CAP2_CTL, m_CapPos[2]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_4_CAP2_CTL, m_CapPos[3]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_5_CAP2_CTL, m_CapPos[4]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_6_CAP2_CTL, m_CapPos[5]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_7_CAP2_CTL, m_CapPos[6]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_8_CAP2_CTL, m_CapPos[7]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_9_CAP2_CTL, m_CapPos[8]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_10_CAP2_CTL, m_CapPos[9]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_1_CAP2_ERROR, m_ErrorCap[0]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_2_CAP2_ERROR, m_ErrorCap[1]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_3_CAP2_ERROR, m_ErrorCap[2]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_4_CAP2_ERROR, m_ErrorCap[3]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_5_CAP2_ERROR, m_ErrorCap[4]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_6_CAP2_ERROR, m_ErrorCap[5]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_7_CAP2_ERROR, m_ErrorCap[6]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_8_CAP2_ERROR, m_ErrorCap[7]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_9_CAP2_ERROR, m_ErrorCap[8]);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_POINT_10_CAP2_ERROR, m_ErrorCap[9]);
	DDX_Control(pDX, IDC_COMBO_COORDINATE_SEL, m_combox_coordinate);
}


BEGIN_MESSAGE_MAP(CScanStageFlatnessTestDlg, CDialogEx)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_POINT_1_CAL_CTL, &CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePoint1CalCtl)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_MASK_FLAT_CTL, &CScanStageFlatnessTestDlg::OnBnClickedCheckMaskFlatCtl)
	ON_BN_CLICKED(IDC_CHECK_MASK_FLAT_STOP_CTL, &CScanStageFlatnessTestDlg::OnBnClickedCheckMaskFlatStopCtl)
	ON_BN_CLICKED(IDC_CHECK_MASK_FLAT_DEFAULT_CTL, &CScanStageFlatnessTestDlg::OnBnClickedCheckMaskFlatDefaultCtl)
	ON_BN_CLICKED(IDC_CHECK_MASK_FLAT_SET_CTL, &CScanStageFlatnessTestDlg::OnBnClickedCheckMaskFlatSetCtl)
	ON_BN_CLICKED(IDC_CHECK_STAGE_ADD, &CScanStageFlatnessTestDlg::OnBnClickedCheckStageAdd)
	ON_BN_CLICKED(IDC_CHECK_STAGE_DEL, &CScanStageFlatnessTestDlg::OnBnClickedCheckStageDel)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_POINT_APPLY, &CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePointApply)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_POINT_APPLY2, &CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePointApply2)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_POINT_SAVE, &CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePointSave)
END_MESSAGE_MAP()

BOOL CScanStageFlatnessTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_POINT_1_CTL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_2_CTL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_3_CTL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_4_CTL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_5_CTL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_6_CTL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_7_CTL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_8_CTL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_9_CTL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_10_CTL))->SetIcon(m_LedIcon[0]);

	m_Test_Point_Num = 0;
	m_point_user_num_ctl.SetWindowTextA(_T("3"));

	point_1 = false;
	point_2 = false;
	point_3 = false;

	x1um = 0.0;
	x2um = 0.0;
	x3um = 0.0;
	
	y1um = 0.0;
	y2um = 0.0;
	y3um = 0.0;
	
	z1um = 0.0; 
	z2um = 0.0; 
	z3um = 0.0; 
	
	cap1um = 0.0;
	cap2um = 0.0;
	cap3um = 0.0;
	
	n1_vec = 0.0;
	n2_vec = 0.0;
	n3_vec = 0.0;

	roll = 0.0;
	pitch = 0.0; 

	roll_urad = 0.0;
	pitch_urad = 0.0;

	roll_revision_urad = 0.0;
	pitch_revision_urad = 0.0;

	m_Default_Num = 0;
	m_User_Point_Num = m_Default_Num;
	m_Add_Point_Num = 0;

	m_Mask_Flatness_Measrue_Start_Flag = FALSE;

	mask_flatness_start_default_x_pos = 204.0;
	mask_flatness_start_default_y_pos = 0.0;

	get_x_pos = 0.0;
	get_y_pos = 0.0;

	result_y_pos_um.clear();
	result_x_pos_um.clear();
	result_cap_pos_um.clear();
	test_result_y_pos_um.clear();
	test_result_x_pos_um.clear();
	test_result_cap_pos_um.clear();


	set_x_pos_um.clear();
	set_y_pos_um.clear();
	set_cap1_pos_um.clear();
	set_cap2_pos_um.clear();
	set_cap3_pos_um.clear();
	set_cap4_pos_um.clear();
	set_y_pos_um.emplace_back(0);
	set_x_pos_um.emplace_back(0);
	set_cap1_pos_um.emplace_back(0);
	set_cap2_pos_um.emplace_back(0);
	set_cap3_pos_um.emplace_back(0);
	set_cap4_pos_um.emplace_back(0);
	m_combox_cap.SetCurSel(1);
	m_combox_coordinate.SetCurSel(1);

	InitPosGrid();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CScanStageFlatnessTestDlg::OnDestroy()
{
	CDialogEx::OnDestroy();


}

void CScanStageFlatnessTestDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}

void CScanStageFlatnessTestDlg::PI_Stage_Flatness_Test_Monitor_Check()
{
	double navigatation_x_pos, navigatation_y_pos;
	double mask_center_x_pos, mask_center_y_pos;
	double mask_lb_x_pos, mask_lb_y_pos;
	double scan_x_pos, scan_y_pos, scan_z_pos, scan_tip_pos, scan_tilt_pos;
	double cap1, cap2, cap3, cap4;

	navigatation_x_pos = 0.0;
	navigatation_y_pos = 0.0;
	mask_center_x_pos = 0.0;
	mask_center_y_pos = 0.0;
	mask_lb_x_pos = 0.0;
	mask_lb_y_pos = 0.0;
	scan_x_pos = 0.0;
	scan_y_pos = 0.0;
	scan_z_pos = 0.0;
	scan_tip_pos = 0.0;
	scan_tilt_pos = 0.0;
	cap1 = 0.0;
	cap2 = 0.0;
	cap3 = 0.0;
	cap4 = 0.0;

	CString navigatation_x_pos_str, navigatation_y_pos_str, mask_center_x_pos_str, mask_center_y_pos_str, mask_lb_x_pos_str, mask_lb_y_pos_str;
	CString scan_x_pos_str, scan_y_pos_str, scan_z_pos_str, scan_tip_pos_str, scan_tilt_pos_str;
	CString cap1_str, cap2_str, cap3_str, cap4_str;

	navigatation_x_pos_str.Empty();
	navigatation_y_pos_str.Empty();
	mask_center_x_pos_str.Empty();
	mask_center_y_pos_str.Empty();
	mask_lb_x_pos_str.Empty();
	mask_lb_y_pos_str.Empty();
	scan_x_pos_str.Empty();
	scan_y_pos_str.Empty();
	scan_z_pos_str.Empty();
	scan_tip_pos_str.Empty();
	scan_tilt_pos_str.Empty();

	cap1_str.Empty();
	cap2_str.Empty();
	cap3_str.Empty();
	cap4_str.Empty();

	////////////////////////////////////////////////////////
	//Navigation Stage 기준
	////////////////////////////////////////////////////////
	navigatation_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	navigatation_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	////////////////////////////////////////////////////////
	// Mask (LB0,0)기준
	////////////////////////////////////////////////////////
	mask_lb_x_pos = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - navigatation_x_pos;
	mask_lb_y_pos = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - navigatation_y_pos;

	////////////////////////////////////////////////////////
	// Mask Center 0,0 기준
	////////////////////////////////////////////////////////
	mask_center_x_pos = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - navigatation_x_pos;
	mask_center_y_pos = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - navigatation_y_pos;

	////////////////////////////////////////////////////////
	// Adam Cap Read Command
	////////////////////////////////////////////////////////

	//if (g_pAdam->Command_CapReadTimeout()) //return 0면 정상.. 아니면 error사항.. 
	//{
	//	::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
	//	cap1 = -1.111111;
	//	cap2 = -1.111111;
	//	cap3 = -1.111111;
	//	cap4 = -1.111111;
	//}
	//else
	//{
	//	cap1 = g_pAdam->AdamData.m_dCapsensor1;
	//	cap2 = g_pAdam->AdamData.m_dCapsensor2;
	//	cap3 = g_pAdam->AdamData.m_dCapsensor3;
	//	cap4 = g_pAdam->AdamData.m_dCapsensor4;
	//}
	//

	////////////////////////////////////////////////////////
	// Scan Stage Axis Read Command
	////////////////////////////////////////////////////////
	scan_x_pos = g_pScanStage->m_dPIStage_GetPos[X_AXIS];
	scan_y_pos = g_pScanStage->m_dPIStage_GetPos[Y_AXIS];
	scan_z_pos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
	scan_tip_pos = g_pScanStage->m_dPIStage_GetPos[TIP_AXIS];
	scan_tilt_pos = g_pScanStage->m_dPIStage_GetPos[TILT_AXIS];


	navigatation_x_pos_str.Format("%3.7f", navigatation_x_pos);
	navigatation_y_pos_str.Format("%3.7f", navigatation_y_pos);
	mask_lb_x_pos_str.Format("%3.7f", mask_lb_x_pos);
	mask_lb_y_pos_str.Format("%3.7f", mask_lb_y_pos);
	mask_center_x_pos_str.Format("%3.7f", mask_center_x_pos);
	mask_center_y_pos_str.Format("%3.7f", mask_center_y_pos);

	cap1_str.Format("%3.7f", cap1);
	cap2_str.Format("%3.7f", cap2);
	cap3_str.Format("%3.7f", cap3);
	cap4_str.Format("%3.7f", cap4);

	scan_x_pos_str.Format("%3.7f", scan_x_pos);
	scan_y_pos_str.Format("%3.7f", scan_y_pos);
	scan_z_pos_str.Format("%3.7f", scan_z_pos);
	scan_tip_pos_str.Format("%3.7f", scan_tip_pos);
	scan_tilt_pos_str.Format("%3.7f", scan_tilt_pos);

	//SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_POS_CTL, navigatation_x_pos_str);
	//SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_POS_CTL, navigatation_y_pos_str);
	//SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_LB_POS_CTL, mask_lb_x_pos_str);
	//SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_LB_POS_CTL, mask_lb_y_pos_str);
	//SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_CENTER_POS_CTL, mask_center_x_pos_str);
	//SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_CENTER_POS_CTL, mask_center_y_pos_str);
	//

	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_Z_CTL, scan_z_pos_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_TIP_CTL, scan_tip_pos_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_TILT_CTL, scan_tilt_pos_str);

	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP1, cap1_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP2, cap2_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP3, cap3_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP4, cap4_str);

}

CScanStageFlatnessTestDlg::vectors vec(double x1, double y1, double z1, double x2, double y2, double z2)
{
	CScanStageFlatnessTestDlg::vectors vector;

	vector.a = (x2 - x1);
	vector.b = (y2 - y1);
	vector.c = (z2 - z1);

	return vector;
}

UINT CScanStageFlatnessTestDlg::PI_Stage_Check_Manual_Thread(LPVOID pParam)
{
	CScanStageFlatnessTestDlg*  PIStageCheckManual = (CScanStageFlatnessTestDlg*)pParam;

	PIStageCheckManual->m_PIstageManualThreadExitFlag = FALSE;


	while (!PIStageCheckManual->m_PIstageManualThreadExitFlag)
	{
		if (PIStageCheckManual->PI_Stage_Check_Manual(PIStageCheckManual->m_Test_Point_Num) != RUN)
		{
			return 0;
		}
		Sleep(50);
	}
	PIStageCheckManual->m_pPIstageManualThread = NULL;
	PIStageCheckManual->m_PIstageManualThreadExitFlag = TRUE;
	return 0;

}

int CScanStageFlatnessTestDlg::PI_Stage_Check_Manual(int test_point)
{
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(true);

	/* 실제 이동 위치 변수 */
	double move_x_pos, move_y_pos;

	/* Absolute x, y position */
	double x_pos_abs, y_pos_abs;	
	
	/* Mask LB 0,0 기준 x, y  Position*/
	double x_pos_mask, y_pos_mask;

	/*  Mask Center 0,0  기준 x,y Position */
	double x_pos_mask_center, y_pos_mask_center; 

	double z_pos, tip_pos, tilt_pos, cap;

	vectors vec1, vec2;

	CString n1_str, n2_str, n3_str;
	CString move_x_pos_str, move_y_pos_str;	// 실제 이동 위치 변수
	CString x_pos_abs_str, y_pos_abs_str;	//Absolute x, y
	CString x_pos_rel_str, y_pos_rel_str;	// relative x, y
	CString x_pos_mask_str, y_pos_mask_str;	// Mask x, y
	CString x_pos_mask_center_str, y_pos_mask_center_str;	// Mask Center x, y
	CString z_pos_str, tip_pos_str, tilt_pos_str, cap_str;


	z_pos = 0.0;
	tip_pos = 0.0;
	tilt_pos = 0.0;
	cap = 0.0;


	//////////////////////////////////////
	//SREM 좌표계 기준으로의 이동좌표
	//////////////////////////////////////
	x_pos_abs = 0.0;
	y_pos_abs = 0.0;

	//////////////////////////////////////
	//mask 좌표. (LB 0,0)
	//////////////////////////////////////
	x_pos_mask = 0.0;
	y_pos_mask = 0.0;

	x_pos_abs_str.Empty();
	y_pos_abs_str.Empty();
	x_pos_rel_str.Empty();
	y_pos_rel_str.Empty();
	x_pos_mask_str.Empty();
	y_pos_mask_str.Empty();
	x_pos_mask_center_str.Empty();
	y_pos_mask_center_str.Empty();
	z_pos_str.Empty();
	tip_pos_str.Empty();
	tilt_pos_str.Empty();
	cap_str.Empty();
	n1_str.Empty();
	n2_str.Empty();
	n3_str.Empty();


	CAutoMessageDlg MsgBoxAuto(this);

	/////////////////////////////////////////////////////
	// PI Stage Z 축 원점
	/////////////////////////////////////////////////////

	//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	//MsgBoxAuto.DoModal(_T(" PI Stage Z 축 원점 진행 중 ! "), 5);


	//SetTimer(REFERENCE_POS_CHECK_TIMER, 100, NULL);

	switch (test_point)
	{
	case 1:
		point_1 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[1]);
		////////////////////////////////////////////////////
		//// point 1 로 이동 하는 함수
		/////////////////////////////////////////////////////

		/////////////////////////////////////////////////////
		//레시피 X, Y READ (um)
		/////////////////////////////////////////////////////
		move_x_pos = pi_flat_rcp_x[test_point - 1];
		move_y_pos = pi_flat_rcp_y[test_point - 1];

		/////////////////////////////////////////////////////
		//Stage Mask Center 기준(0,0)으로 이동.
		/////////////////////////////////////////////////////

		g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		MsgBoxAuto.DoModal(_T(" [Point 1] Stage Moving ! "), 7);

		//if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
		//{
		//	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		//	g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		//	MsgBoxAuto.DoModal(_T(" [Point 1] PI Stage Moving ! "), 5);
		//}
		//else
		//{
		//	m_PIstageManualThreadExitFlag = TRUE;
		////	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(false);
		////	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(false);
		////	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(false);
		////	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_AUTO)->EnableWindow(false);
		////	KillTimer(REFERENCE_POS_CHECK_TIMER);
		//	::AfxMessageBox(" Z AXIS 원점이 아님.!");
		//	return -1;
		//}

		////////////////////////////////////////////////////////
		//Navigation Stage 기준
		////////////////////////////////////////////////////////
		x_pos_abs = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos_abs = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		////////////////////////////////////////////////////////
		// Mask (LB0,0)기준
		////////////////////////////////////////////////////////
		x_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - x_pos_abs;
		y_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - y_pos_abs;

		////////////////////////////////////////////////////////
		// Mask Center 0,0 기준
		////////////////////////////////////////////////////////
		x_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - x_pos_abs;
		y_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - y_pos_abs;

		////////////////////////////////////////////////////////
		// Adam Cap Read Command
		////////////////////////////////////////////////////////
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) //return 0면 정상.. 아니면 error사항.. 
		{
			::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
			return -1;
		}
		cap = g_pAdam->AdamData.m_dCapsensor2;


		x_pos_mask_center_str.Format("%3.7f", x_pos_mask_center);
		y_pos_mask_center_str.Format("%3.7f", y_pos_mask_center);

		cap_str.Format("%3.7f", cap);

		////////////////////////////////////////////////////////
		// 실질적 법선벡터 사용 변수에 대입
		////////////////////////////////////////////////////////
		x1um = x_pos_mask_center * 1000;
		y1um = y_pos_mask_center * 1000;
		cap1um = cap;


		////////////////////////////////////////////////////////
		// Viewer
		////////////////////////////////////////////////////////
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_X, x_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_Y, y_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_CAP2, cap_str);


		/////////////////////////////////////////////////////////////////
		// Point 1 에서 스테이지 이동 및 x,y,cap 값 리드 완료 플래그 ON
		/////////////////////////////////////////////////////////////////
		((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[0]);
		point_1 = true;

		if (point_1 && point_2 && point_3)
		{
			((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[0]);
			//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(true);
		}
		else
		{
			//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(false);
		}
		break;
	case 2:

		point_2 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[1]);

		////////////////////////////////////////////////////
		//// point 2 로 이동 하는 함수
		/////////////////////////////////////////////////////
		move_x_pos = pi_flat_rcp_x[test_point - 1];
		move_y_pos = pi_flat_rcp_y[test_point - 1];

		g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		MsgBoxAuto.DoModal(_T(" [Point 2] Stage Moving ! "), 7);

		//if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
		//{
		//	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		//	g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		//	MsgBoxAuto.DoModal(_T(" [Point 2] PI Stage Moving ! "), 10);
		//}
		//else
		//{
		//	m_PIstageManualThreadExitFlag = TRUE;
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_AUTO)->EnableWindow(false);
		//	//	KillTimer(REFERENCE_POS_CHECK_TIMER);
		//	::AfxMessageBox(" Z AXIS 원점이 아님.!");
		//	return -1;
		//}

		////////////////////////////////////////////////////////
		//Navigation Stage 기준
		////////////////////////////////////////////////////////
		x_pos_abs = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos_abs = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		////////////////////////////////////////////////////////
		// Mask (LB0,0)기준
		////////////////////////////////////////////////////////
		x_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - x_pos_abs;
		y_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - y_pos_abs;

		////////////////////////////////////////////////////////
		// Mask Center 0,0 기준
		////////////////////////////////////////////////////////
		x_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - x_pos_abs;
		y_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - y_pos_abs;

		////////////////////////////////////////////////////////
		// Adam Cap Read Command
		////////////////////////////////////////////////////////
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) //return 0면 정상.. 아니면 error사항.. 
		{
			::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
			return -1;
		}
		cap = g_pAdam->AdamData.m_dCapsensor2;


		x_pos_mask_center_str.Format("%3.7f", x_pos_mask_center);
		y_pos_mask_center_str.Format("%3.7f", y_pos_mask_center);
		cap_str.Format("%3.7f", cap);
		////////////////////////////////////////////////////////
		// 실질적 법선벡터 사용 변수에 대입 (u 로 단위 변환)
		////////////////////////////////////////////////////////
		x2um = x_pos_mask_center * 1000;
		y2um = y_pos_mask_center * 1000;
		cap2um = cap;



		////////////////////////////////////////////////////////
		// Viewer
		////////////////////////////////////////////////////////
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_X, x_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_Y, y_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_CAP2, cap_str);


		/////////////////////////////////////////////////////////////////
		// Point 2 에서 스테이지 이동 및 x,y,cap 값 리드 완료 플래그 ON
		/////////////////////////////////////////////////////////////////
		((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[0]);
		point_2 = true;

		if (point_1 && point_2 && point_3)
		{
			((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[0]);
			//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(true);

		}
		else
		{
			//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(false);

		}
		break;
	case 3:
		point_3 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[1]);
		////////////////////////////////////////////////////
		//// point 3 로 이동 하는 함수
		/////////////////////////////////////////////////////
		move_x_pos = pi_flat_rcp_x[test_point - 1];
		move_y_pos = pi_flat_rcp_y[test_point - 1];

		g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		MsgBoxAuto.DoModal(_T(" [Point 3] Stage Moving ! "), 7);

		//if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
		//{
		//	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		//	g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		//	MsgBoxAuto.DoModal(_T(" [Point 3] PI Stage Moving ! "), 5);
		//}
		//else
		//{
		//	m_PIstageManualThreadExitFlag = TRUE;
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_AUTO)->EnableWindow(false);
		//	//	KillTimer(REFERENCE_POS_CHECK_TIMER);
		//	::AfxMessageBox(" Z AXIS 원점이 아님.!");
		//	return -1;
		//}
		//
		////////////////////////////////////////////////////////
		//Navigation Stage 기준
		////////////////////////////////////////////////////////
		x_pos_abs = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos_abs = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		////////////////////////////////////////////////////////
		// Mask (LB0,0)기준
		////////////////////////////////////////////////////////
		x_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - x_pos_abs;
		y_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - y_pos_abs;

		////////////////////////////////////////////////////////
		// Mask Center 0,0 기준
		////////////////////////////////////////////////////////
		x_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - x_pos_abs;
		y_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - y_pos_abs;

		////////////////////////////////////////////////////////
		// Adam Cap Read Command
		////////////////////////////////////////////////////////
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) //return 0면 정상.. 아니면 error사항.. 
		{
			::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
			return -1;
		}
		cap = g_pAdam->AdamData.m_dCapsensor2;


		x_pos_mask_center_str.Format("%3.7f", x_pos_mask_center);
		y_pos_mask_center_str.Format("%3.7f", y_pos_mask_center);
		cap_str.Format("%3.7f", cap);
		////////////////////////////////////////////////////////
		// 실질적 법선벡터 사용 변수에 대입
		////////////////////////////////////////////////////////
		x3um = x_pos_mask_center * 1000;
		y3um = y_pos_mask_center * 1000;
		cap3um = cap;

		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_X, x_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_Y, y_pos_mask_center_str);

		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_CAP2, cap_str);

		((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[0]);

		point_3 = true;
		if (point_1 && point_2 && point_3)
		{

			((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[0]);
			GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(true);

		}
		else
		{
			GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(false);

		}
		break;
	case 4:
		break;
	default:
		break;
	}


	m_PIstageManualThreadExitFlag = TRUE;


	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(false);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(false);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(false);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_AUTO)->EnableWindow(false);

	//KillTimer(REFERENCE_POS_CHECK_TIMER);
	return 0;
}



/*
	3 Point 법선 벡터 구하는 함수 ( 검증 완료 )
	N Point 법선 벡터로 주석 처리 
	2021.01.25
	jhkim
*/


/*
void CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePoint1ManualCtl()
{
	//if (m_bTestStop == FALSE || m_Mask_Measrue_Start == FALSE || m_PI_Stage_Measrue_Start == FALSE)
	//{
	//	::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
	//	return;
	//}
	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pAdam == NULL || g_pConfig == NULL)
	{
		AfxMessageBox(_T("Stage 구동 Error"));
		return;
	}
	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		return;
	}

	if (!g_pScanStage->Is_SCAN_Stage_Connected())
	{
		AfxMessageBox(_T("Scan Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	m_Test_Point_Num = 1;
	m_pPIstageManualThread = ::AfxBeginThread(PI_Stage_Check_Manual_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
}

void CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePoint2ManualCtl()
{
	//if (m_bTestStop == FALSE || m_Mask_Measrue_Start == FALSE || m_PI_Stage_Measrue_Start == FALSE)
	//{
	//	::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
	//	return;
	//}
	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pAdam == NULL || g_pConfig == NULL)
	{
		AfxMessageBox(_T("Stage 구동 Error"));
		return;
	}
	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		return;
	}

	if (!g_pScanStage->Is_SCAN_Stage_Connected())
	{
		AfxMessageBox(_T("Scan Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	m_Test_Point_Num = 2;
	m_pPIstageManualThread = ::AfxBeginThread(PI_Stage_Check_Manual_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
}

void CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePoint3ManualCtl()
{
	//if (m_bTestStop == FALSE || m_Mask_Measrue_Start == FALSE || m_PI_Stage_Measrue_Start == FALSE)
	//{
	//	::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
	//	return;
	//}
	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pAdam == NULL || g_pConfig == NULL)
	{
		AfxMessageBox(_T("Stage 구동 Error"));
		return;
	}
	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		return;
	}

	if (!g_pScanStage->Is_SCAN_Stage_Connected())
	{
		AfxMessageBox(_T("Scan Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	m_Test_Point_Num = 3;
	m_pPIstageManualThread = ::AfxBeginThread(PI_Stage_Check_Manual_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
}

*/

void CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePoint1CalCtl()
{
	//vectors vec1, vec2;

	result_y_pos_um.clear();
	result_x_pos_um.clear();
	result_cap_pos_um.clear();
	error_cap_data_um.clear();

	CString roll_str, pitch_str;
	CString roll_str_revision, pitch_str_revisiton;

	roll_str_revision.Empty();
	pitch_str_revisiton.Empty();
	roll_str.Empty();
	pitch_str.Empty();

	CString user_value;
	user_value.Empty();

	CString n1_str, n2_str, n3_str;
	n1_str.Empty();
	n2_str.Empty();
	n3_str.Empty();

	for (int i = 0; i < 10; i++)
	{
		m_XPos[i].SetWindowTextA("");
		m_YPos[i].SetWindowTextA("");
		m_CapPos[i].SetWindowTextA("");
		m_ErrorCap[i].SetWindowTextA("");
	}

	//m_User_Point_Num = 3;
	if (m_User_Point_Num < 3)
	{
		return;
	}


	////////////////////////
	// 보정 전
	////////////////////////
	CString tmp;
	tmp.Format("%4.4f", g_pScanStage->m_dPIStage_GetPos[TIP_AXIS]);
	((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_POINT_1_TIP_CTL))->SetWindowTextA(tmp);
	tmp.Format("%4.4f", g_pScanStage->m_dPIStage_GetPos[TILT_AXIS]);
	((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_POINT_1_TILT_CTL))->SetWindowTextA(tmp);


	Calculate_Vector(m_User_Point_Num);

	NPointErrorCapDataCalculate(m_User_Point_Num);




	n1_str.Format("%3.9f", n1_vec);
	n2_str.Format("%3.9f", n2_vec);
	n3_str.Format("%3.9f", n3_vec);

	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_NVECTOR_CTL, n1_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_NVECTOR_CTL, n2_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_NVECTOR_CTL, n3_str);

	roll_str.Format("%3.9f", roll_urad);
	pitch_str.Format("%3.9f", pitch_urad);

	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_1_CTL, roll_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_2_CTL, pitch_str);

	roll_revision_urad = -(roll_urad);
	pitch_revision_urad = (pitch_urad);

	roll_str_revision.Format("%3.9f", roll_revision_urad);
	pitch_str_revisiton.Format("%3.9f", pitch_revision_urad);

	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_TY_CTL, roll_str_revision);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_TX_CTL, pitch_str_revisiton);


	//result_y_pos_um.clear();
	//result_x_pos_um.clear();
	//result_cap_pos_um.clear();
	//error_cap_data_um.clear();
}

void CScanStageFlatnessTestDlg::Point_N_Calculate_Result()
{

	CString roll_str, pitch_str;
	CString roll_str_revision, pitch_str_revisiton;

	roll_str_revision.Empty();
	pitch_str_revisiton.Empty();
	roll_str.Empty();
	pitch_str.Empty();

	CString user_value;
	user_value.Empty();

	CString n1_str, n2_str, n3_str;
	n1_str.Empty();
	n2_str.Empty();
	n3_str.Empty();

	Calculate_Vector(m_User_Point_Num);

	n1_str.Format("%3.9f", n1_vec);
	n2_str.Format("%3.9f", n2_vec);
	n3_str.Format("%3.9f", n3_vec);

	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_NVECTOR_CTL, n1_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_NVECTOR_CTL, n2_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_NVECTOR_CTL, n3_str);

	roll_str.Format("%3.9f", roll_urad);
	pitch_str.Format("%3.9f", pitch_urad);

	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_1_CTL, roll_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_2_CTL, pitch_str);

	roll_revision_urad = -(roll_urad);
	pitch_revision_urad = (pitch_urad);

	roll_str_revision.Format("%3.9f", roll_revision_urad);
	pitch_str_revisiton.Format("%3.9f", pitch_revision_urad);

	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_TY_CTL, roll_str_revision);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_TX_CTL, pitch_str_revisiton);


	result_y_pos_um.clear();
	result_x_pos_um.clear();
	result_cap_pos_um.clear();
	error_cap_data_um.clear();
}

void CScanStageFlatnessTestDlg::Calculate_Vector(int n_point)
{
	vectors vec1, vec2;
	CString str;
	str.Empty();

	double cap_pos = 0.0;
	double Xpos_avr, Ypos_avr, Cappos_avr;
	double Xpos_sum, Ypos_sum, Cappos_sum;
	
	Xpos_avr = 0.0;
	Ypos_avr = 0.0;
	Cappos_avr = 0.0;
	Xpos_sum = 0.0;
	Ypos_sum = 0.0;
	Cappos_sum = 0.0;


	roll = 0.0;
	pitch = 0.0;
	roll_urad = 0.0;
	pitch_urad = 0.0;

	xx = 0.0;
	yy = 0.0;
	xy = 0.0;
	yz = 0.0;
	xz = 0.0;



	result_x_pos_um.assign(set_x_pos_um.begin(), set_x_pos_um.end());
	result_y_pos_um.assign(set_y_pos_um.begin(), set_y_pos_um.end());

	Cap_Sel = m_combox_cap.GetCurSel();

	switch (Cap_Sel)
	{
		result_cap_pos_um.clear();
	case 0:
		result_cap_pos_um.assign(set_cap1_pos_um.begin(), set_cap1_pos_um.end());
		break;
	case 1:
		result_cap_pos_um.assign(set_cap2_pos_um.begin(), set_cap2_pos_um.end());
		break;
	case 2:
		result_cap_pos_um.assign(set_cap3_pos_um.begin(), set_cap3_pos_um.end());
		break;
	case 3:
		result_cap_pos_um.assign(set_cap4_pos_um.begin(), set_cap4_pos_um.end());
		break;
	default:
		break;
	}




	for (int i = 0; i < n_point; i++)
	{
		//GetDlgItem(IDC_EDIT_PI_STAGE_POINT_1_CAP2_CTL + i)->GetWindowTextA(user_value);
		//cap_pos = atof(user_value);

		str.Format(_T("%6.6f"), result_x_pos_um.at(i));
		m_XPos[i].SetWindowTextA(str);
		str.Format(_T("%6.6f"), result_y_pos_um.at(i));
		m_YPos[i].SetWindowTextA(str);
		str.Format(_T("%6.6f"), result_cap_pos_um.at(i));
		m_CapPos[i].SetWindowTextA(str);


		//result_cap_pos_um.erase(result_cap_pos_um.begin() + i);
		//result_cap_pos_um.emplace(result_cap_pos_um.begin() + i, cap_pos);
		//result_z_pos.assign(i, cap_pos);
	}

	//TEST KJH

	//result_x_pos_um.clear();
	//result_y_pos_um.clear();
	//result_cap_pos_um.clear();
	//result_x_pos_um.emplace(result_x_pos_um.begin(), -50000.00);
	//result_x_pos_um.emplace(result_x_pos_um.begin()+1, 50000.00);
	//result_x_pos_um.emplace(result_x_pos_um.begin()+2, 0.00);
	//
	//result_y_pos_um.emplace(result_y_pos_um.begin(), 0.00);
	//result_y_pos_um.emplace(result_y_pos_um.begin() + 1, 0.00);
	//result_y_pos_um.emplace(result_y_pos_um.begin() + 2, 50000.0);
	//
	//result_cap_pos_um.emplace(result_cap_pos_um.begin(), 50.00);
	//result_cap_pos_um.emplace(result_cap_pos_um.begin() + 1, -50.00);
	//result_cap_pos_um.emplace(result_cap_pos_um.begin() + 2, 0.0);
	//
	if (n_point >  3)
	{
		//////////////////////////////////////////////////////////////////
		// N Point 법선 벡터 계산
		//////////////////////////////////////////////////////////////////

		//평균
		for (int i = 0; i < n_point; i++)
		{
			Xpos_sum += result_x_pos_um.at(i);
			Ypos_sum += result_y_pos_um.at(i);
			Cappos_sum += result_cap_pos_um.at(i);
		}

		Xpos_avr = Xpos_sum / n_point;
		Ypos_avr = Ypos_sum / n_point;
		Cappos_avr = Cappos_sum / n_point;



		for (int i = 0; i < n_point; i++)
		{
			xx += (result_x_pos_um.at(i) - Xpos_avr) * (result_x_pos_um.at(i) - Xpos_avr);
			yy += (result_y_pos_um.at(i) - Ypos_avr) * (result_y_pos_um.at(i) - Ypos_avr);
			xz += (result_x_pos_um.at(i) - Xpos_avr) * (result_cap_pos_um.at(i) - Cappos_avr);
			xy += (result_x_pos_um.at(i) - Xpos_avr) * (result_y_pos_um.at(i) - Ypos_avr);
			yz += (result_y_pos_um.at(i) - Ypos_avr) * (result_cap_pos_um.at(i) - Cappos_avr);
		}
		
		double a, b, D;
		
		D = xx * yy - xy * xy;
		a= yz * xy - xz * yy;
		b = xy * xz - xx * yz;

		double pow_D_ = pow(D, 2);
		double pow_a_ = pow(a, 2);
		double pow_b_ = pow(b, 2);

		double sqrt_Dab = sqrt(pow_a_ + pow_b_ + pow_D_);

		n1_vec = a / sqrt_Dab;
		n2_vec = b / sqrt_Dab;
		n3_vec = D / sqrt_Dab;

	}
	//////////////////////////////////////////////////////////////////
	// 3 Point 법선 벡터 계산
	//////////////////////////////////////////////////////////////////
	else if (n_point == 3)
	{

		vec1 = vec(result_x_pos_um.at(0), result_y_pos_um.at(0), result_cap_pos_um.at(0), result_x_pos_um.at(1), result_y_pos_um.at(1), result_cap_pos_um.at(1));
		vec2 = vec(result_x_pos_um.at(0), result_y_pos_um.at(0), result_cap_pos_um.at(0), result_x_pos_um.at(2), result_y_pos_um.at(2), result_cap_pos_um.at(2));

		//vec1 = vec(x1um, y1um, cap1um, x2um, y2um, cap2um);
		//vec2 = vec(x1um, y1um, cap1um, x3um, y3um, cap3um);

		n1_vec = vec1.b * vec2.c - vec1.c * vec2.b;
		n2_vec = vec1.c * vec2.a - vec1.a * vec2.c;
		n3_vec = vec1.a * vec2.b - vec1.b * vec2.a;

		double lenth = sqrt(n1_vec * n1_vec + n2_vec * n2_vec + n3_vec * n3_vec);

		n1_vec = n1_vec / lenth;
		n2_vec = n2_vec / lenth;
		n3_vec = n3_vec / lenth;
	}
	
	/* Roll , Pitch 계산 */


	//roll = atan((n1_vec / n3_vec));
	roll = atan2(n1_vec, n3_vec);
	pitch = asin(-n2_vec);
	
	
	/* u 단위 변환 해 줌 */
	roll_urad = roll * 1000000;
	pitch_urad = pitch * 1000000;
	
}

void CScanStageFlatnessTestDlg::ReSetting_Pos_Recipy()
{
	CString strAppName, strString;
	strAppName.Empty();
	strString.Empty();


	CString x_pos_ref_str, y_pos_ref_str;
	x_pos_ref_str.Empty();
	y_pos_ref_str.Empty();

	CString strX, strY;
	strX.Empty();
	strY.Empty();

	double user_x_pos = 0.0, user_y_pos = 0.0;


	int row = 0;
	row = m_StagePosGrid.GetSelectedRow();

	if (row > m_User_Point_Num)
	{
		AfxMessageBox(_T("범위가 잘못 되었습니다"), MB_ICONERROR);
		return;
	}

	user_x_pos = atof(x_pos_ref_str);
	user_y_pos = atof(y_pos_ref_str);

	g_pConfig->m_stStagePos[row - 1].x = user_x_pos;
	g_pConfig->m_stStagePos[row - 1].y = user_y_pos;

	strX.Format(_T("%6.6f"), g_pConfig->m_stStagePos[row - 1].x);
	m_StagePosGrid.SetItemText(row, 1, strX);
	strY.Format(_T("%6.6f"), g_pConfig->m_stStagePos[row - 1].y);
	m_StagePosGrid.SetItemText(row, 2, strY);

	m_StagePosGrid.Invalidate(FALSE);

	strAppName.Format("POINT_%02d", row);
	WritePrivateProfileString(strAppName, "X(um)", x_pos_ref_str, PISTAGE_TEST_POSITION_FILE_FULLPATH);
	WritePrivateProfileString(strAppName, "Y(um)", y_pos_ref_str, PISTAGE_TEST_POSITION_FILE_FULLPATH);

}

void CScanStageFlatnessTestDlg::InitPosGrid()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_STAGEPOSGRID_PI_STAGE_CHECK_CTL)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_StagePosGrid.Create(rect, this, IDC_TEXT_STAGEPOSGRID_PI_STAGE_CHECK_CTL);

	int nGap, col, row;
	col = 0;
	row = 0;
	nGap = 8;

	m_StagePosGrid.SetEditable(false);
	m_StagePosGrid.SetListMode(true);
	m_StagePosGrid.SetSingleRowSelection(false);
	m_StagePosGrid.EnableDragAndDrop(false);
	m_StagePosGrid.SetFixedRowCount(1);
	m_StagePosGrid.SetFixedColumnCount(1);
	m_StagePosGrid.SetColumnCount(7);
	m_StagePosGrid.SetRowCount(m_User_Point_Num + 1);
	//m_StagePosGrid.SetBkColor(RGB(192, 192, 192));
	m_StagePosGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_StagePosGrid.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	Font.CreateFontIndirect(&lf);
	m_StagePosGrid.SetFont(&Font);
	Font.DeleteObject();

	CString strNo, strX, strY, str;
	str.Format("%s", "No");
	m_StagePosGrid.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "X(um)");
	m_StagePosGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Y(um)");
	m_StagePosGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "No");
	m_StagePosGrid.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "X(um)");
	m_StagePosGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Y(um)");
	m_StagePosGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "CapSensor_1(um)");
	m_StagePosGrid.SetItemText(0, 3, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "CapSensor_2(um)");
	m_StagePosGrid.SetItemText(0, 4, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "CapSensor_3(um)");
	m_StagePosGrid.SetItemText(0, 5, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "CapSensor_4(um)");
	m_StagePosGrid.SetItemText(0, 6, (LPSTR)(LPCTSTR)str);

	m_StagePosGrid.SetRowHeight(0, 40);
	m_StagePosGrid.SetColumnWidth(0, 15);
	m_StagePosGrid.SetColumnWidth(1, 60);
	m_StagePosGrid.SetColumnWidth(2, 60);
	m_StagePosGrid.SetColumnWidth(3, 60);
	m_StagePosGrid.SetColumnWidth(4, 60);
	m_StagePosGrid.SetColumnWidth(5, 60);
	m_StagePosGrid.SetColumnWidth(6, 60);
	m_StagePosGrid.ExpandColumnsToFit();
	

	

	for (row = 1; row < m_User_Point_Num + 1; row++)
	{
		strNo.Format("%d", row);
		m_StagePosGrid.SetItemText(row, 0, strNo);

		//m_StagePosGrid.SetItemText(row, 1, g_pConfig->m_stStagePos[row - 1].chStagePositionString);
		//pi_flat_rcp_x[row - 1] = g_pConfig->m_stStageumPos_PI[row - 1].x;
		//pi_flat_rcp_y[row - 1] = g_pConfig->m_stStageumPos_PI[row - 1].y;


		strX.Format("%4.6f", set_x_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 1, strX);
		strY.Format("%4.6f", set_y_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 2, strY);
		str.Format("%4.6f", set_cap1_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 3, str);
		str.Format("%4.6f", set_cap2_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 4, str);
		str.Format("%4.6f", set_cap3_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 5, str);
		str.Format("%4.6f", set_cap4_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 6, str);
	
	}
	//m_StagePosGrid.SetSelectedRow(1);

}


void CScanStageFlatnessTestDlg::OnBnClickedCheckMaskFlatCtl()
{
	//GetDlgItem(IDC_MASK_PRO_CTL)->SetWindowTextA(_T("측정 가능"));
	//GetDlgItem(IDC_BTN_DIGITAL)->EnableActiveAccessibility();
	//GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(false);
	GetDlgItem(IDC_CHECK_MASK_FLAT_CTL)->EnableWindow(false);
	GetDlgItem(IDC_CHECK_MASK_FLAT_SET_CTL)->EnableWindow(false);
	GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT_CTL)->EnableWindow(false);

	CAutoMessageDlg MsgBoxAuto(this);

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pAdam == NULL || g_pConfig == NULL)
	{
		AfxMessageBox(_T("Stage 구동 Error"));
		return;
	}
	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		return;
	}

	if (!g_pScanStage->Is_SCAN_Stage_Connected())
	{
		AfxMessageBox(_T("Scan Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	// PI Stage Flatness 가 측정 중이라면 m_PIstageManualThreadExitFlag 변수 FALSE
	//
	if (m_PIstageManualThreadExitFlag == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		GetDlgItem(IDC_CHECK_MASK_FLAT_SET_CTL)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT_CTL)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT_CTL)->EnableWindow(true);

		return;
	}


	g_pWarning->m_strWarningMessageVal = " Mask Flatness Measurement 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	if (Mask_Flatness() != RUN)
	{
		//	m_progress_mask_flat.SetPos(0);
		//	m_bTestStop = TRUE;
		//	m_Mask_Measrue_Start = TRUE;


		g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
		MsgBoxAuto.DoModal(_T(" EUV To OM Moving ! "), 4);
		WaitSec(2);
		SetDlgItemTextA(IDC_OM_EUV_CTL, "OM");
		g_pWarning->ShowWindow(SW_HIDE);
		::AfxMessageBox(_T("평탄도 측정에 실패 하였습니다"), MB_ICONERROR);
		GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM_CTL)->SetWindowTextA(_T("0"));
		m_progress_mask_flat.SetPos(0);
		GetDlgItem(IDC_CHECK_MASK_FLAT_SET_CTL)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT_CTL)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT_CTL)->EnableWindow(true);


		SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_VIEW_CTL, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_VIEW_CTL, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_Z_VIEW_CTL, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_TX_VIEW_CTL, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_TY_VIEW_CTL, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_CAP1_VIEW_CTL, _T("0.0"));

		m_Mask_Flatness_Measrue_Start_Flag = FALSE;
		KillTimer(MASK_FLATNESS_MEASUREMENT_TIMER);

		return;
	}

	m_progress_mask_flat.SetPos(0);
	GetDlgItem(IDC_CHECK_MASK_FLAT_SET_CTL)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT_CTL)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);

	m_Mask_Flatness_Measrue_Start_Flag = FALSE;
	KillTimer(MASK_FLATNESS_MEASUREMENT_TIMER);
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_VIEW_CTL, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_VIEW_CTL, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_Z_VIEW_CTL, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_TX_VIEW_CTL, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_TY_VIEW_CTL, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_CAP1_VIEW_CTL, _T("0.0"));

	g_pWarning->ShowWindow(SW_HIDE);
	::AfxMessageBox(_T("평탄도 측정이 완료 되었습니다"));
}

int CScanStageFlatnessTestDlg::Mask_Flatness()
{

	int cont = 0;

	double cap1=0.0;
	double cap2=0.0;
	double cap3=0.0;
	double cap4=0.0;



	/* Mask 평탄도 측정 범위 가능 검사 변수 */
	double target_x_pos= 0.0; 
	double target_y_pos= 0.0;

	/* 실제 이동해야 할 위치 저장 변수 (Navigation Stage Position) */
	double move_x_pos= 0.0;	
	double move_y_pos= 0.0;

	/* 현재 위치 값 읽어 상태를 전달 하는 변수 */
	double x_pos= 0.0; 
	double y_pos= 0.0;
	double z_pos= 0.0;
	double tx_pos= 0.0;
	double ty_pos= 0.0;

	CString x_pos_str;
	CString y_pos_str;
	CString z_pos_str;
	CString tx_pos_str;
	CString ty_pos_str;

	CString cap1_str;
	CString cap2_str;
	CString cap3_str;
	CString cap4_str;

	CString position_str;
	CString str;

	x_pos_str.Empty();
	y_pos_str.Empty();
	z_pos_str.Empty();
	tx_pos_str.Empty();
	ty_pos_str.Empty();

	cap1_str.Empty();
	cap2_str.Empty();
	cap3_str.Empty();
	cap4_str.Empty();

	position_str.Empty();
	str.Empty();


	int position = 0;
	int x = 0, y = 0;

	int nRet = RUN;


	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pConfig == NULL)	return -1;



	m_progress_mask_flat.SetPos(0);


	CAutoMessageDlg MsgBoxAuto(this);

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 4);
		return nRet = -1;
	}

	// 측정 중 일 경우 TRUE, 측정 끝나거나 중지되면 FALSE, 
	m_Mask_Flatness_Measrue_Start_Flag = TRUE;
	if (m_Mask_Flatness_Measrue_Start_Flag)	SetTimer(MASK_FLATNESS_MEASUREMENT_TIMER, 100, NULL);
	////////////////////////////////////
	// MASK SIZE :: 152.4 x 152.4 mm
	////////////////////////////////////


	// 마스크 X축 평탄도 측정 가능 범위 확인 
	//
	// 사용자의 별도 좌표값 없을 경우 Default 로 [210 , 0] 좌표로 시작.
	// 사용자의 값이 입력되어 SET 되면 get_x_pos , get_y_pos 값에 반영됨.
	// default_x_pos = 210.0;
	// default_y_pos = 0.0;

	// 210 기준으로 (15mm 씩 10번 움직임 ) 

	target_x_pos = get_x_pos - (10 * 14.5);
	if (target_x_pos < 0)
	{
		MsgBoxAuto.DoModal(_T(" 평탄도 x axis 측정 범위 벗어남 ! "), 4);
		return nRet = -1;
	}

	// 마스크 Y축 평탄도 측정 가능 범위 확인 
	// mask size 152 mm 이므로 0 기준으로 200 넘어가면 의미 없는 수치 값으로 추정.

	target_y_pos = get_y_pos + (10 * 13.5);
	if (target_y_pos > 200)
	{
		MsgBoxAuto.DoModal(_T(" 평탄도 y axis 측정 범위 벗어남 ! "), 4);
		return nRet = -1;
	}

	// OM -> EUV 변환 ( Cap 으로 측정되는 값을 확인 하기 위함 )

	g_pMaskMap->ChangeOMEUVSet(OMTOEUV);
	MsgBoxAuto.DoModal(_T(" OM To EUV Moving ! "), 4);
	WaitSec(2);
	SetDlgItemTextA(IDC_OM_EUV_CTL, "EUV");


	// Z축 0 으로 Setting 후 평탄도 측정을 진행 위함.
	// Z축 위치가 0 되면 Default 값 혹은, 사용자 설정 X, Y 값으로 이동.
	// OM 영역이 아닌, EUV 영역에서 좌표값을 움직인다.
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	MsgBoxAuto.DoModal(_T(" Z 축 home Moving ! "), 4);
	WaitSec(2);

	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
	if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
	{
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(get_x_pos, get_y_pos);
	}
	else
	{
		::AfxMessageBox(" Z AXIS 원점이 아님.!");
		return nRet = -1;
	}

	// 설정된 좌표값으로 이동후 , 좌표값 다시 READ 후 범위 측정 확인.

	double orgin_x_pos = 0.0;
	double orgin_y_pos = 0.0;
	CString start_x_pos_str;
	CString start_y_pos_str;

	start_x_pos_str.Empty();
	start_y_pos_str.Empty();

	orgin_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	orgin_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	start_x_pos_str.Format("%3.7f", orgin_x_pos);
	start_y_pos_str.Format("%3.7f", orgin_y_pos);

	SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_START_CTL, start_x_pos_str);
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_START_CTL, start_y_pos_str);

	// 읽은 좌표값을 기준으로 측정 가능 범위 재 확인 (X, Y)
	target_x_pos = orgin_x_pos - (10 * 15);
	if (target_x_pos < 0)
	{
		MsgBoxAuto.DoModal(_T(" 평탄도 x axis 측정 범위 벗어남 ! "), 4);
		return nRet = -1;
	}

	target_y_pos = orgin_y_pos + (10 * 15);
	if (target_y_pos > 200)
	{
		MsgBoxAuto.DoModal(_T(" 평탄도 y axis 측정 범위 벗어남 ! "), 4);
		return nRet = -1;
	}

	if (m_Mask_Flatness_Measrue_Start_Flag == FALSE)
	{
		::AfxMessageBox(" 마스크 평탄도 STOP 버튼 누름 ! ");
		position = 0;
		return nRet = -1;
	}

	// 측정 전 Z 축 충돌방지를 위한 0 위치 재확인.
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(2);

	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) != 0)
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) != Z_INITIAL_POS_UM)
	if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
	{
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(get_x_pos, get_y_pos);
	}
	else
	{
		::AfxMessageBox(" Z AXIS 원점이 아님.!");
		return nRet = -1;
	}

	str = "측정 시작 !";
	SaveLogFile("마스크 평탄도 측정", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);


	y = 0;
	x = 0;

	position = 0;
	position_str.Format("%d", position);

	GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM_CTL)->SetWindowTextA(position_str);
	//GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 진행 중"));

	int mon = CTime::GetCurrentTime().GetMonth();
	int day = CTime::GetCurrentTime().GetDay();
	int hour = CTime::GetCurrentTime().GetHour();
	int min = CTime::GetCurrentTime().GetMinute();

	CString datafile;
	CString datafile_cap;
	CString datafile_4cap;
	CString path;
	path = LOG_PATH;

	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	path += "\\";
	path += strDate;
	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	datafile.Format(_T("%s\\마스크 평탄도_data_%d_%d_%d_%d.txt"), path, mon, day, hour, min);
	datafile_cap.Format(_T("%s\\cap_data.txt"), path);
	datafile_4cap.Format(_T("%s\\4cap_data.txt"), path);

	std::ofstream flatness_measurement_datafile(datafile);
	std::ofstream flatness_measurement_cap_value(datafile_cap);
	std::ofstream flatness_measurement_4cap_value(datafile_4cap);

	flatness_measurement_datafile << "position" << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "tx_pos" << "\t" << "ty_pos" << "\t" << "z_pos" << "\t" << "cap" << "\n";
	flatness_measurement_4cap_value << "position" << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "tx_pos" << "\t" << "ty_pos" << "\t" << "z_pos" << "\t" << "cap1" << "\t" << "cap2" << "\t" << "cap3" << "\t" << "cap4" << "\t" << "\n";


	if (m_Mask_Flatness_Measrue_Start_Flag == FALSE)
	{
		::AfxMessageBox(" 마스크 평탄도 STOP 버튼 누름 ! ");
		return nRet = -1;
	}

	while (y < 10) // Y축 이동
	{

		if (y == 0)	y_pos = orgin_y_pos;
		else y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);


		while (x < 10) // X 축 이동
		{
			if (x == 0)	x_pos = orgin_x_pos;
			else x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);


			// SCAN STAGE AXIS 값 READ
			z_pos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
			tx_pos = g_pScanStage->m_dPIStage_GetPos[TIP_AXIS];
			ty_pos = g_pScanStage->m_dPIStage_GetPos[TILT_AXIS];

			z_pos_str.Format("%3.7f", z_pos);
			x_pos_str.Format("%3.7f", x_pos);
			y_pos_str.Format("%3.7f", y_pos);
			tx_pos_str.Format("%3.7f", tx_pos);
			ty_pos_str.Format("%3.7f", ty_pos);

			position_str.Format("%d", position);

			g_pAdam->Command_ADAMSimpleRun();
			//WaitSec(2);


			//WaitSec(4);
			cap1 = g_pAdam->AdamData.m_dCapsensor1;
			cap1_str.Format("%.4f", cap1);
			cap2 = g_pAdam->AdamData.m_dCapsensor2;
			cap2_str.Format("%.4f", cap2);
			cap3 = g_pAdam->AdamData.m_dCapsensor3;
			cap3_str.Format("%.4f", cap3);
			cap4 = g_pAdam->AdamData.m_dCapsensor4;
			cap4_str.Format("%.4f", cap4);
			//g_pAdam->Command_ADAMSimpleRun();

			cont = position + 1;
			flatness_measurement_datafile << std::setprecision(3) << position_str << "\t" << x_pos_str << "\t" << y_pos_str << "\t" << tx_pos_str << "\t" << ty_pos_str << "\t" << z_pos_str << "\t" << cap1_str << "\n";
			flatness_measurement_4cap_value << std::setprecision(3) << position_str << "\t" << x_pos_str << "\t" << y_pos_str << "\t" << tx_pos_str << "\t" << ty_pos_str << "\t" << z_pos_str << "\t" << cap1_str << "\t" << cap2_str << "\t" << cap3_str << "\t" << cap4_str << "\n";
			flatness_measurement_cap_value << std::setprecision(3) << cap1_str << "\n";


			CString cont_str;
			cont_str.Format("%d", cont);
			GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM_CTL)->SetWindowTextA(cont_str);

			if (m_Mask_Flatness_Measrue_Start_Flag == FALSE)
			{
				::AfxMessageBox(" 마스크 평탄도 STOP 버튼 누름 ! ");
				position = 0;
				nRet = -2;
				break;
			}

			// 10x10 측정을 위한 15 mm 씩 감소하여 이동.
			move_x_pos = x_pos - 15.0;
			move_y_pos = y_pos;

			WaitSec(2);
			position++; // LOG 기록용 
			x++;
			m_progress_mask_flat.OffsetPos(1);
			if (x == 10) break;
		}


		if (m_Mask_Flatness_Measrue_Start_Flag == FALSE)
		{
			::AfxMessageBox(" 마스크 평탄도 STOP 버튼 누름 ! ");
			position = 0;
			nRet = -1;
			break;
		}
		if (nRet != RUN) break; // x 방향 Error 발생.

		move_x_pos = orgin_x_pos; // X 축 초기 값으로 이동.
		move_y_pos = y_pos + 15.0; // Y 축 15 mm 씩 이동.



		// Z축 충돌 방지를 위한 0 재 확인 후 이동.
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(2);
		//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
		//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
		if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		}
		else
		{
			::AfxMessageBox(" Z AXIS 원점이 아님.!");
			return nRet = -1;
			break;
		}

		WaitSec(2);
		if (nRet != RUN) break; // y 방향 Error 발생.

		x = 0;
		y++;

		if (y == 10) break;
	}



	flatness_measurement_datafile << std::endl;
	if (flatness_measurement_datafile.is_open() == true)
	{
		flatness_measurement_datafile.close();
	}

	flatness_measurement_4cap_value << std::endl;
	if (flatness_measurement_4cap_value.is_open() == true)
	{
		flatness_measurement_4cap_value.close();
	}

	flatness_measurement_cap_value << std::endl;
	if (flatness_measurement_cap_value.is_open() == true)
	{
		flatness_measurement_cap_value.close();
	}

	GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 완료"));
	if (nRet != RUN) return nRet; // Error 발생 시

	// 측정 완료 후 다시 OM 영역으로 복귀
	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	MsgBoxAuto.DoModal(_T(" Euv To Om Moving ! "), 4);
	WaitSec(2);
	SetDlgItemTextA(IDC_OM_EUV_CTL, "OM");

	GetDlgItem(IDC_CHECK_MASK_FLAT_SET_CTL)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT_CTL)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT_CTL)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM_CTL)->SetWindowTextA(_T("0"));
	m_progress_mask_flat.SetPos(0);
	position = 0;

	return nRet;

}

void CScanStageFlatnessTestDlg::OnBnClickedCheckMaskFlatStopCtl()
{
	m_Mask_Flatness_Measrue_Start_Flag = FALSE;

}

void CScanStageFlatnessTestDlg::OnBnClickedCheckMaskFlatDefaultCtl()
{
	CString default_x_str;
	CString default_y_str;

	default_x_str.Empty();
	default_y_str.Empty();

	// Default 누름으로 처음 Setting 값으로 원복.
	get_x_pos = mask_flatness_start_default_x_pos;
	get_y_pos = mask_flatness_start_default_y_pos;

	default_x_str.Format("%.4f", get_x_pos);
	default_y_str.Format("%.4f", get_y_pos);

	SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_POSITION_CTL, default_x_str);
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_POSITION_CTL, default_y_str);
}

void CScanStageFlatnessTestDlg::OnBnClickedCheckMaskFlatSetCtl()
{
	CString user_x_str;
	CString user_y_str;

	// SET 누름으로 사용자 정의 값 적용
	GetDlgItem(IDC_EDIT_TEST_MASK_X_POSITION_CTL)->GetWindowTextA(user_x_str);
	GetDlgItem(IDC_EDIT_TEST_MASK_Y_POSITION_CTL)->GetWindowTextA(user_y_str);
	
	get_x_pos = atof(user_x_str);
	get_y_pos = atof(user_y_str);

}

void CScanStageFlatnessTestDlg::OnBnClickedCheckStageDel()
{
	OnStageTargetDel();
}

void CScanStageFlatnessTestDlg::OnBnClickedCheckStageAdd()
{
	OnStageTargetAdd();
}

void CScanStageFlatnessTestDlg::OnStageTargetAdd()
{

	int row = 0;
	double navigatation_x_pos_um, navigatation_y_pos_um, mask_center_x_pos_um, mask_center_y_pos_um, stage_coordinate_x_pos_um;
	double navigatation_x_pos_mm, navigatation_y_pos_mm, mask_center_x_pos_mm, mask_center_y_pos_mm, stage_coordinate_y_pos_um;
	double cap1_um, cap2_um, cap3_um, cap4_um;
	CString cap1_str, cap2_str, cap3_str, cap4_str;


	CString strAppName, strString;
	strAppName.Empty();
	strString.Empty();


	CString x_pos_ref_str, y_pos_ref_str;
	x_pos_ref_str.Empty();
	y_pos_ref_str.Empty();

	CString str;
	str.Empty();

	navigatation_x_pos_um = 0.0;
	navigatation_y_pos_um = 0.0;
	mask_center_x_pos_um = 0.0;
	mask_center_y_pos_um = 0.0;
	navigatation_x_pos_mm = 0.0;
	navigatation_y_pos_mm = 0.0;
	mask_center_x_pos_mm = 0.0;
	mask_center_y_pos_mm = 0.0;
	stage_coordinate_x_pos_um = 0.0;
	stage_coordinate_y_pos_um = 0.0;

	Stage_Coordinate_Sel = m_combox_coordinate.GetCurSel();

	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		return;
	}
	if (!g_pScanStage->Is_SCAN_Stage_Connected())
	{
		AfxMessageBox(_T("Scan Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	navigatation_x_pos_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	navigatation_y_pos_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	switch (Stage_Coordinate_Sel)
	{
	case 0: // Mask Center Coordinate Reference
		////////////////////////////////////////////////////////
		// Mask Center 0,0 기준
		////////////////////////////////////////////////////////
		
		g_pNavigationStage->ConvertToMaskFromStage(navigatation_x_pos_mm, navigatation_y_pos_mm, mask_center_x_pos_mm, mask_center_y_pos_mm);
		
		mask_center_x_pos_um = mask_center_x_pos_mm * 1000;
		mask_center_y_pos_um = mask_center_y_pos_mm * 1000;
	
		stage_coordinate_x_pos_um = mask_center_x_pos_um;
		stage_coordinate_y_pos_um = mask_center_y_pos_um;

		break;
	case 1: // Stage Encoder Coordinate Reference
		////////////////////////////////////////////////////////
		//Navigation Stage 기준
		////////////////////////////////////////////////////////
		navigatation_x_pos_um = navigatation_x_pos_mm * 1000;
		navigatation_y_pos_um = navigatation_y_pos_mm * 1000;

		stage_coordinate_x_pos_um = navigatation_x_pos_um;
		stage_coordinate_y_pos_um = navigatation_y_pos_um;

		break;
	default:
		break;
	}
	

	//navigatation_x_pos_um *= 1000;
	//navigatation_y_pos_um *= 1000;

	

	////////////////////////////////////////////////////////
	// Adam Cap Read Command
	////////////////////////////////////////////////////////
	if (g_pAdam->Command_AverageRunAfterTimeout() != 0) //return 0면 정상.. 아니면 error사항.. 
	{
		::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
		return;
	}

	//cap = g_pAdam->AdamData.m_dCapsensor2;
	cap1_um = g_pAdam->AdamData.m_dCapsensor1;
	cap2_um = g_pAdam->AdamData.m_dCapsensor2;
	cap3_um = g_pAdam->AdamData.m_dCapsensor3;
	cap4_um = g_pAdam->AdamData.m_dCapsensor4;


	////////////////////////////////////////////////////////
	// 실질적 법선벡터 사용 변수에 대입
	////////////////////////////////////////////////////////
	
	//cap1um = cap;
	
	
	//result_x_pos_um.emplace(result_x_pos_um.begin() + i, x1um);
	//result_y_pos_um.emplace(result_y_pos_um.begin() + i, y1um);
	//result_cap_pos_um.emplace(result_cap_pos_um.begin() + i, cap1um);
	
	//switch (Cap_Sel)
	//{
	//case 0:
	//	cap1 = g_pAdam->AdamData.m_dCapsensor1;
	//	break;
	//case 1:
	//	cap2 = g_pAdam->AdamData.m_dCapsensor2;
	//	break;
	//case 2:
	//	cap3 = g_pAdam->AdamData.m_dCapsensor3;
	//	break;
	//case 3:
	//	cap4 = g_pAdam->AdamData.m_dCapsensor4;
	//	break;
	//default:
	//	break;
	//}


	row = m_StagePosGrid.GetSelectedRow();
	//row = m_StagePosGrid.cur
	//
	//if ((row > m_User_Point_Num) || (row == 0))
	//{
	//	AfxMessageBox(_T("범위가 잘못 되었습니다"), MB_ICONERROR);
	//	return;
	//}

	//
	//user_x_pos = atof(x_pos_ref_str);
	//user_y_pos = atof(y_pos_ref_str);

	//m_TestStageumPos_um[row - 1].x = mask_center_x_pos_um;
	//m_TestStageumPos_um[row - 1].y = mask_center_y_pos_um;


	if(row > m_User_Point_Num)
	{
		m_User_Point_Num++;
		//set_x_pos_um.emplace_back(0);
		//set_y_pos_um.emplace_back(0);
	}
	else
	{
		set_x_pos_um.erase(set_x_pos_um.begin() + row-1);
		set_y_pos_um.erase(set_y_pos_um.begin() + row-1);
		set_cap1_pos_um.erase(set_cap1_pos_um.begin() + row-1);
		set_cap2_pos_um.erase(set_cap2_pos_um.begin() + row-1);
		set_cap3_pos_um.erase(set_cap3_pos_um.begin() + row-1);
		set_cap4_pos_um.erase(set_cap4_pos_um.begin() + row-1);
	}

	//set_y_pos_um.emplace(set_y_pos_um.begin() + row -1, mask_center_y_pos_um);
	//set_x_pos_um.emplace(set_x_pos_um.begin() + row -1, mask_center_x_pos_um);
	set_y_pos_um.emplace(set_y_pos_um.begin() + row - 1, stage_coordinate_x_pos_um);
	set_x_pos_um.emplace(set_x_pos_um.begin() + row - 1, stage_coordinate_y_pos_um);
	set_cap1_pos_um.emplace(set_cap1_pos_um.begin() + row - 1, cap1_um);
	set_cap2_pos_um.emplace(set_cap2_pos_um.begin() + row - 1, cap2_um);
	set_cap3_pos_um.emplace(set_cap3_pos_um.begin() + row - 1, cap3_um);
	set_cap4_pos_um.emplace(set_cap4_pos_um.begin() + row - 1, cap4_um);

	//str.Format(_T("%6.6f"), set_x_pos_um.at(row -1 ));
	//m_XPos[row - 1].SetWindowTextA(str);
	//str.Format(_T("%6.6f"), set_y_pos_um.at(row - 1));
	//m_YPos[row - 1].SetWindowTextA(str);



	//str.Format(_T("%6.6f"), set_x_pos_um.at(row -1 ));
	//m_StagePosGrid.SetItemText(row, 1, str);
	//str.Format(_T("%6.6f"), set_y_pos_um.at(row - 1));
	//m_StagePosGrid.SetItemText(row, 2, str);
	//str.Format("%4.6f", set_cap1_pos_um.at(row - 1));
	//m_StagePosGrid.SetItemText(row, 3, str);
	//str.Format("%4.6f", set_cap2_pos_um.at(row - 1));
	//m_StagePosGrid.SetItemText(row, 4, str);
	//str.Format("%4.6f", set_cap3_pos_um.at(row - 1));
	//m_StagePosGrid.SetItemText(row, 5, str);
	//str.Format("%4.6f", set_cap4_pos_um.at(row - 1));
	//m_StagePosGrid.SetItemText(row, 6, str);
	//strX.Format(_T("%6.6f"), m_TestStageumPos[row - 1].x);
	//m_StagePosGrid.SetItemText(row, 1, strX);
	//strY.Format(_T("%6.6f"), m_TestStageumPos[row - 1].y);
	//m_StagePosGrid.SetItemText(row, 2, strY);

	m_StagePosGrid.Invalidate(FALSE);

	ReReadPointPosGrid();


	//GetStageInf();

}

void CScanStageFlatnessTestDlg::OnStageTargetDel()
{
	int row = 0;
	row = m_StagePosGrid.GetSelectedRow();

	m_User_Point_Num--;

	set_x_pos_um.erase(set_x_pos_um.begin() + row - 1);
	set_y_pos_um.erase(set_y_pos_um.begin() + row - 1);
	set_cap1_pos_um.erase(set_cap1_pos_um.begin() + row - 1);
	set_cap2_pos_um.erase(set_cap2_pos_um.begin() + row - 1);
	set_cap3_pos_um.erase(set_cap3_pos_um.begin() + row - 1);
	set_cap4_pos_um.erase(set_cap4_pos_um.begin() + row - 1);

	//m_XPos[row - 1].SetWindowTextA(" ");
	//m_YPos[row - 1].SetWindowTextA(" ");


	m_StagePosGrid.Invalidate(FALSE);

	ReReadPointPosGrid();
	
	//Point_Check_Enable_Btn(m_User_Point_Num);

}

void CScanStageFlatnessTestDlg::ReReadPointPosGrid()
{
	m_StagePosGrid.SetEditable(false);
	m_StagePosGrid.SetListMode(true);
	m_StagePosGrid.SetSingleRowSelection(false);
	m_StagePosGrid.EnableDragAndDrop(false);
	m_StagePosGrid.SetFixedRowCount(1);
	m_StagePosGrid.SetFixedColumnCount(1);
	m_StagePosGrid.SetColumnCount(7);
	m_StagePosGrid.SetRowCount(m_User_Point_Num+1);

	CString str;
	str.Format("%s", "No");
	m_StagePosGrid.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "X(um)");
	m_StagePosGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Y(um)");
	m_StagePosGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "CapSensor_1(um)");
	m_StagePosGrid.SetItemText(0, 3, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "CapSensor_2(um)");
	m_StagePosGrid.SetItemText(0, 4, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "CapSensor_3(um)");
	m_StagePosGrid.SetItemText(0, 5, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "CapSensor_4(um)");
	m_StagePosGrid.SetItemText(0, 6, (LPSTR)(LPCTSTR)str);
	str.Format("%d", m_User_Point_Num);
	m_point_user_num_ctl.SetWindowTextA(str);


	for (int row = 1; row < m_User_Point_Num + 1; row++)
	{
		str.Format("%d", row);
		m_StagePosGrid.SetItemText(row, 0, str);
		str.Format(_T("%6.6f"), set_x_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 1, str);
		str.Format(_T("%6.6f"), set_y_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 2, str);
		str.Format("%4.6f", set_cap1_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 3, str);
		str.Format("%4.6f", set_cap2_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 4, str);
		str.Format("%4.6f", set_cap3_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 5, str);
		str.Format("%4.6f", set_cap4_pos_um.at(row - 1));
		m_StagePosGrid.SetItemText(row, 6, str);
		//strX.Format("%4.6f", m_TestStageumPos[row].x);
		//m_StagePosGrid.SetItemText(row, 1, strX);
		//strY.Format("%4.6f", m_TestStageumPos[row].y);
		//m_StagePosGrid.SetItemText(row, 2, strY);
	}
}

void CScanStageFlatnessTestDlg::NPointErrorCapDataCalculate(int npoint, bool testmode)
{
	double sum_cap_data = 0.0;
	double avg_cap_data = 0.0;
	double max_cap_data = 0.0;
	double error_cap_sum_data = 0.0;
	double error_cap_avg_data = 0.0;
	double error_cap_max_data = 0.0;
	//error_z_data.resize(npoint);

	CString str, avg, error, max;
	
	str.Empty();
	avg.Empty();
	error.Empty();

	for (int n = 0; n < npoint; n++)
	{
		sum_cap_data += abs(result_cap_pos_um.at(n));
		if (max_cap_data < abs(result_cap_pos_um.at(n)))
		{
			max_cap_data = abs(result_cap_pos_um.at(n));
		}
	}
	avg_cap_data = sum_cap_data / npoint;
	avg.Format("%3.7f", avg_cap_data);
	if (testmode)
	{
		((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_RESULT_STD_AVER_TEST))->SetWindowTextA(avg);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_RESULT_STD_AVER))->SetWindowTextA(avg);
	}

	//max.Format("%3.7f", max_cap_data);
	//((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_RESULT_STD_MAX))->SetWindowTextA(max);

	for (int n = 0; n < npoint; n++)
	{
		error_cap_data_um.emplace(error_cap_data_um.begin() + n, abs((abs(result_cap_pos_um.at(n)) - avg_cap_data)));
		str.Format("%3.7f", error_cap_data_um.at(n));
		if(testmode)
			m_TestErrorCap[n].SetWindowTextA(str);
		else
			m_ErrorCap[n].SetWindowTextA(str);
	}
	
	for (int n = 0; n < npoint; n++)
	{
		error_cap_sum_data += abs(error_cap_data_um.at(n));
		if (error_cap_max_data < abs(error_cap_data_um.at(n)))
		{
			error_cap_max_data = abs(error_cap_data_um.at(n));
		}
	}

	error_cap_avg_data = error_cap_sum_data / npoint;
	error.Format("%3.7f", error_cap_avg_data);
	
	if (testmode)
	{
		((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_RESULT_ERROR_AVER_TEST))->SetWindowTextA(error);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_RESULT_ERROR_AVER))->SetWindowTextA(error);
	}

	error.Format("%3.7f", error_cap_max_data);
	if (testmode)
	{
		((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_RESULT_ERROR_MAX_TEST))->SetWindowTextA(error);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_RESULT_ERROR_MAX))->SetWindowTextA(error);
	}
}

void CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePointApply()
{
	
	// 보정 전
	//CString tmp;
	//tmp.Format("%4.4f",g_pScanStage->m_dPIStage_GetPos[TIP_AXIS]);
	//((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_POINT_1_TIP_CTL))->SetWindowTextA(tmp);
	//tmp.Format("%4.4f", g_pScanStage->m_dPIStage_GetPos[TILT_AXIS]);
	//((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_POINT_1_TILT_CTL))->SetWindowTextA(tmp);
	
	
	OnScanStageApply();


	// 보정 후
	CString tmp2;
	tmp2.Format("%4.4f", g_pScanStage->m_dPIStage_GetPos[TIP_AXIS]);
	((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_POINT_1_TIP_CTL2))->SetWindowTextA(tmp2);
	tmp2.Format("%4.4f", g_pScanStage->m_dPIStage_GetPos[TILT_AXIS]);
	((CStatic*)GetDlgItem(IDC_EDIT_PI_STAGE_POINT_1_TILT_CTL2))->SetWindowTextA(tmp2);
}

void CScanStageFlatnessTestDlg::OnScanStageApply()
{
	double cal_roll, cal_pitch;
	CString str;

	cal_roll = roll_revision_urad;
	cal_pitch = pitch_revision_urad;

	str.Format(" Tx : %f urad\n Ty : %f urad\n 상기의 Tx,Ty 값 으로 Scan Stage 보정 하시겠습니까?", cal_roll, cal_pitch);

	if (IDYES == AfxMessageBox(str, MB_YESNO))
	{
		if (g_pScanStage->MoveRelative(TILT_AXIS, cal_roll) != 0)
		{
			AfxMessageBox(_T("Scan Stage 구동 실패"));
		}
		
		if (g_pScanStage->MoveRelative(TIP_AXIS, cal_pitch) != 0)
		{
			AfxMessageBox(_T("Scan Stage 구동 실패"));
		}
	}


}

void CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePointApply2()
{
	/////////////////////////////
	//Vector ini
	/////////////////////////////
	//set_x_pos_um.clear();
	//set_y_pos_um.clear();
	//set_cap1_pos_um.clear();
	//set_cap2_pos_um.clear();
	//set_cap3_pos_um.clear();
	//set_cap4_pos_um.clear();
	//set_y_pos_um.emplace_back(0);
	//set_x_pos_um.emplace_back(0);
	//set_cap1_pos_um.emplace_back(0);
	//set_cap2_pos_um.emplace_back(0);
	//set_cap3_pos_um.emplace_back(0);
	//set_cap4_pos_um.emplace_back(0);

	// n Point Stage Pos, Cap Read 


	for (int i = 0; i<10; i++)
	{
		m_TestXPos[i].SetWindowTextA("");
		m_TestYPos[i].SetWindowTextA("");
		m_TestCapPos[i].SetWindowTextA("");
		m_TestErrorCap[i].SetWindowTextA("");
	}


	NPoint_Calculate(m_User_Point_Num);
	//NPoint_Calculate(3);

	// Calculate

}

int CScanStageFlatnessTestDlg::NPoint_Calculate(int n_point)
{
	int nRet = 0;

	Cap_Sel = m_combox_cap.GetCurSel();

	/* 실제 이동 위치 변수 */
	double move_x_pos_um, move_y_pos_um;
	double move_x_pos_mm, move_y_pos_mm;

	/* Absolute x, y position */
	double x_pos_mm, y_pos_mm;

	/*  Mask Center 0,0  기준 x,y Position */
	double x_pos_mask_center_um, y_pos_mask_center_um;
	double x_pos_stage_encoder_um, y_pos_stage_encoder_um;
	double x_pos_mask_center_mm, y_pos_mask_center_mm;

	double stage_coordinate_x_pos_um;
	double stage_coordinate_y_pos_um;
	double cap;

	stage_coordinate_x_pos_um = 0.0;
	stage_coordinate_y_pos_um = 0.0;
	x_pos_stage_encoder_um = 0.0;
	y_pos_stage_encoder_um = 0.0;

	CString x_pos_mask_center_str, y_pos_mask_center_str;	// Mask Center x, y
	CString x_pos_stage_encoder_str, y_pos_stage_encoder_str;	// Stage Encoder x, y
	CString x_pos_stage_coordinate_str, y_pos_stage_coordinate_str;	//
	CString roll_str, pitch_str;
	CString roll_str_revision, pitch_str_revisiton;
	CString cap_str;

	roll_str_revision.Empty();
	pitch_str_revisiton.Empty();
	roll_str.Empty();
	pitch_str.Empty();

	cap = 0.0;
	x_pos_mm = 0.0;
	y_pos_mm = 0.0;

	//////////////////////////////////////
	//mask 좌표. (Center 0,0)
	//////////////////////////////////////
	move_x_pos_um = 0.0;
	move_y_pos_um = 0.0;
	x_pos_mask_center_um = 0.0;
	y_pos_mask_center_um = 0.0;
	x_pos_mask_center_str.Empty();
	y_pos_mask_center_str.Empty();
	x_pos_stage_encoder_str.Empty();
	y_pos_stage_encoder_str.Empty();
	cap_str.Empty();


	CAutoMessageDlg MsgBoxAuto(this);


	for (int i = 0; i < n_point; i++)
	{
		((CStatic*)GetDlgItem(IDC_ICON_POINT_1_CTL + i))->SetIcon(m_LedIcon[2]);
	}

	/////////////////////////////////////////////////////
	// PI Stage Z 축 원점
	/////////////////////////////////////////////////////

	//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	//MsgBoxAuto.DoModal(_T(" PI Stage Z 축 원점 진행 중 ! "), 5);


	for (int i = 0; i < n_point; i++)
	{

		/////////////////////////////////////////////////////
		//레시피 X, Y READ (um)
		/////////////////////////////////////////////////////
		move_x_pos_um = set_x_pos_um.at(i);
		move_y_pos_um = set_y_pos_um.at(i);


		switch (Stage_Coordinate_Sel)
		{
		case 0 :
			/////////////////////////////////////////////////////
			//Stage Mask Center 기준(0,0)으로 이동.
			/////////////////////////////////////////////////////
			g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos_um, move_y_pos_um);
			break;
		case 1:
			////////////////////////////////////////////////////////
			//Navigation Stage Encoder 기준
			////////////////////////////////////////////////////////
			g_pNavigationStage->MoveAbsolutePosition(move_x_pos_um, move_y_pos_um);
			break;
		default:
			break;
		}

		
		MsgBoxAuto.DoModal(_T(" Stage Moving ! "), 7);


		x_pos_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		switch (Stage_Coordinate_Sel)
		{
		case 0: // Mask Center Coordinate Reference
			////////////////////////////////////////////////////////
			// Mask Center 0,0 기준
			////////////////////////////////////////////////////////
			g_pNavigationStage->ConvertToMaskFromStage(x_pos_mm, y_pos_mm, x_pos_mask_center_mm, y_pos_mask_center_mm);
			x_pos_mask_center_um = x_pos_mask_center_mm * 1000;
			y_pos_mask_center_um = y_pos_mask_center_mm * 1000;
			stage_coordinate_x_pos_um = x_pos_mask_center_um;
			stage_coordinate_y_pos_um = y_pos_mask_center_um;

			break;
		case 1: // Stage Encoder Coordinate Reference
			
			////////////////////////////////////////////////////////
			//Navigation Stage 기준
			////////////////////////////////////////////////////////
			x_pos_stage_encoder_um = x_pos_mm * 1000;
			y_pos_stage_encoder_um = y_pos_mm * 1000;
			stage_coordinate_x_pos_um = x_pos_stage_encoder_um;
			stage_coordinate_y_pos_um = y_pos_stage_encoder_um;

			break;
		default:
			break;
		}

		////////////////////////////////////////////////////////
		// Adam Cap Read Command
		////////////////////////////////////////////////////////
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) //return 0면 정상.. 아니면 error사항.. 
		{
			::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
			return -1;
		}

		cap1um = g_pAdam->AdamData.m_dCapsensor1;
		cap2um = g_pAdam->AdamData.m_dCapsensor2;
		cap3um = g_pAdam->AdamData.m_dCapsensor3;
		cap4um = g_pAdam->AdamData.m_dCapsensor4;

		////cap = g_pAdam->AdamData.m_dCapsensor2;
		switch (Cap_Sel)
		{
		case 0:
			cap = cap1um;
			break;
		case 1:
			cap = cap2um;
			break;
		case 2:
			cap = cap3um;
			break;
		case 3:
			cap = cap4um;
			break;
		default:
			break;
		}
		
		x_pos_stage_coordinate_str.Format("%3.7f", stage_coordinate_x_pos_um);
		y_pos_stage_coordinate_str.Format("%3.7f", stage_coordinate_y_pos_um);
		cap_str.Format("%3.7f", cap);

		m_TestXPos[i].SetWindowTextA(x_pos_stage_coordinate_str);
		m_TestYPos[i].SetWindowTextA(y_pos_stage_coordinate_str);
		m_TestCapPos[i].SetWindowTextA(cap_str);

		////////////////////////////////////////////////////////
		// 실질적 법선벡터 사용 변수에 대입
		////////////////////////////////////////////////////////
		//x1um = x_pos_mask_center_um;
		//y1um = y_pos_mask_center_um;

		set_x_pos_um.erase(set_x_pos_um.begin() + i);
		set_y_pos_um.erase(set_y_pos_um.begin() + i);
		set_cap1_pos_um.erase(set_cap1_pos_um.begin() + i);
		set_cap2_pos_um.erase(set_cap2_pos_um.begin() + i);
		set_cap3_pos_um.erase(set_cap3_pos_um.begin() + i);
		set_cap4_pos_um.erase(set_cap4_pos_um.begin() + i);

		set_y_pos_um.emplace(set_y_pos_um.begin() + i, stage_coordinate_y_pos_um);
		set_x_pos_um.emplace(set_x_pos_um.begin() + i, stage_coordinate_x_pos_um);
		set_cap1_pos_um.emplace(set_cap1_pos_um.begin() + i, cap1um);
		set_cap2_pos_um.emplace(set_cap2_pos_um.begin() + i, cap2um);
		set_cap3_pos_um.emplace(set_cap3_pos_um.begin() + i, cap3um);
		set_cap4_pos_um.emplace(set_cap4_pos_um.begin() + i, cap4um);

		//test_result_x_pos_um.emplace(test_result_x_pos_um.begin() + i, x1um);
		//test_result_y_pos_um.emplace(test_result_y_pos_um.begin() + i, y1um);
		//test_result_cap_pos_um.emplace(test_result_cap_pos_um.begin() + i, cap1um);



		//result_x_pos[i] = x1um;
		//result_y_pos[i] = y1um;
		//result_z_pos[i] = cap1um;

		((CStatic*)GetDlgItem(IDC_ICON_POINT_1_CTL + i))->SetIcon(m_LedIcon[1]);

	}


	Calculate_Vector(m_User_Point_Num);

	NPointErrorCapDataCalculate(n_point, true);


	roll_str.Format("%3.9f", roll_urad);
	pitch_str.Format("%3.9f", pitch_urad);

	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_1_CTL_TEST, roll_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_2_CTL_TEST, pitch_str);

	roll_revision_urad = -(roll_urad);
	pitch_revision_urad = (pitch_urad);

	roll_str_revision.Format("%3.9f", roll_revision_urad);
	pitch_str_revisiton.Format("%3.9f", pitch_revision_urad);

	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_TY_CTL_TEST, roll_str_revision);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_TX_CTL_TEST, pitch_str_revisiton);


	return nRet;
}

void CScanStageFlatnessTestDlg::OnBnClickedCheckPiStagePointSave()
{
	OnScanStageConfigSave();
}

void CScanStageFlatnessTestDlg::OnScanStageConfigSave()
{
	CString strAppName, strKeyName, strValue;
	double m_Scanstage_tip_absolute_pos, m_Scanstage_tilt_absolute_pos;
	
	/*
	Cofing File 에 보정 값을 넣어야 하는지 ? 
	보정된 값을 넣어야하는지 ? 
	확인 해야함.
	*/
	
	g_pScanStage->GetPosAxesData();
	m_Scanstage_tip_absolute_pos = g_pScanStage->m_dPIStage_GetPos[TIP_AXIS];
	m_Scanstage_tilt_absolute_pos = g_pScanStage->m_dPIStage_GetPos[TILT_AXIS];

	strAppName.Format(_T("CALIBRATION_INFO"));
	strKeyName.Format(_T("Scan_Stage_Tx"));
	strValue.Format("%.1f", m_Scanstage_tip_absolute_pos);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("Scan_Stage_Ty"));
	strValue.Format("%.1f", m_Scanstage_tilt_absolute_pos);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

}