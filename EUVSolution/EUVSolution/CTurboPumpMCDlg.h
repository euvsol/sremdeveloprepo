﻿/**
 * MC Turbo Pump Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CTurboPumpMCDlg 대화 상자

class CTurboPumpMCDlg : public CDialogEx, public CPfeifferHiPace2300TMPCtrl
{
	DECLARE_DYNAMIC(CTurboPumpMCDlg)

public:
	CTurboPumpMCDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CTurboPumpMCDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TURBO_PUMP_MC_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	bool						m_mc_tmp_ThreadExitFlag;
	CWinThread*					m_mc_tmp_Thread;
	static UINT Mc_Tmp_UpdataThread(LPVOID pParam);

	DECLARE_MESSAGE_MAP()
public:
	HICON	m_LedIcon[3];
	virtual int	OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);
	virtual int	ReceiveData(char *lParam, DWORD dwRead);
	void ParsingData(CString strRecvMsg);

	clock_t		m_start_time, m_finish_time;

	bool		m_Mc_Tmp_OpenPort_State;
	bool		m_Mc_Tmp_Speed_SetOn;					//목표 속도 도달 

	int			m_mc_tmp_state;

	int			Mc_Tmp_GetStatus();
	int			Mc_Tmp_ReceiveData();		// Tmp 상태 확인
	void		TmpSendUpdate();			// Tmp 상태 요청
	void		ErrorModeOn();				// Error 발생 시 실행 함수.
	
	int			Mc_Tmp_Off();
	int			Mc_Tmp_On();
	int			m_lock_state;

	CString		str_CR;


	int pre_data;

	//CString		GetCheckSumValue(char *command);
	

	/* Log 기록용 */
	//CString		mc_Tmp_Hz;
	//CString		mc_Tmp_Error_Code;
	//CString		mc_Tmp_State;

	



	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);


	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedMcTmpOn();
	afx_msg void OnBnClickedMcTmpOff();
	afx_msg void OnBnClickedMcTmpErrorReset();
	afx_msg void OnBnClickedMcTmpLock();

	CString						str_mc_tmp_ReceiveDataHz;				// HZ
	CString						str_mc_tmp_ReceiveDataState;			// STATE
	CString						str_mc_tmp_ReceiveDataErrorCode;		//	ERROR CODE
	CString						str_mc_tmp_ReceiveDataErrorState;		//	ERROR STATE 
	CString						str_mc_tmp_ReceiveDataOnOffState;		// ON/OFF STATE
	CString						str_mc_tmp_ReceiveDataRpm;				// 현재 rpm
	CString						str_mc_tmp_ReceiveDataTemperature_1;	//전자 장치 온도
	CString						str_mc_tmp_ReceiveDataTemperature_2;	//펌프 하단부 온도
	CString						str_mc_tmp_ReceiveDataTemperature_3;	//모터온도	
	CString						str_mc_tmp_ReceiveDataTemperature_4;	//베어링 온도	
	CString						str_mc_tmp_Error_War_State;				// Error , Warning 구분
	CString						str_mc_tmp_ReveiveDataSetHz;			// 현재 설정 값 hz
	CString						str_mc_tmp_ReveiveDataSetRpm;			// 현재 설정 값 rpm

	int			Is_MC_TmpWorking() { return m_mc_tmp_state; }
	BOOL		Is_MC_TmpError()	{ return str_mc_tmp_ReceiveDataErrorCode == _T("No Error") ? FALSE : TRUE; }
	BOOL		Is_MC_Tmp_Connected() { return m_Mc_Tmp_OpenPort_State; }
	CString		Get_MC_Tmp_Hz() { return str_mc_tmp_ReceiveDataHz; }								//속도(hz)
	CString		Get_MC_Tmp_Error() { return str_mc_tmp_ReceiveDataErrorCode; }						//Error Code
	CString		Get_MC_Tmp_Error_Comment() { return str_mc_tmp_ReceiveDataErrorState; }				//Error Comment
	CString		Get_MC_Tmp_State() { return str_mc_tmp_ReceiveDataState; }							//Tmp State

	


	
};
