﻿/**
 * Vacuum Gauge Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CVacuumGaugeDlg 대화 상자

class CVacuumGaugeDlg : public CDialogEx , public CMKS390Gauge
{
	DECLARE_DYNAMIC(CVacuumGaugeDlg)

public:
	CVacuumGaugeDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CVacuumGaugeDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VACUUM_GAUGE_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	bool						ThreadExitFlag;
	CWinThread*					m_Thread;
	static UINT UpdataThread(LPVOID pParam);

	DECLARE_MESSAGE_MAP()
public:
	int Vaccum_Gauge_State;
	HICON m_LedIcon[3];

	CString ch1;
	CString ch2;
	CString ch3;

	double m_dPressure_LLC;
	double m_dPressure_MC;

	virtual int	OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//afx_msg void OnTimer(UINT_PTR nIDEvent);

	int							m_nGaugeTimer;				//Timer for Gauge reading 
	int							m_n390GaugeTimer;			//Timer for Gauge reading 
	int							m_nCaugeSendCommandSel;		//Switch mode에 사용

	bool						m_bOpenPort_State;			// Open 여부 판단.

														

	//Vacuum Sequence에서 필요로하는 함수
	int GetStatus();		//Guage 상태를 General_define.h에 정의하고, 현재 상태를 return: ex) GAUGE_NORMAL,GAUGE_OFFLINE,GAUGE_ERROR,...
	
	//Thread 시 사용 함수. 
	int Getupdata();

	// Timer 시 사용 함수.
	void Getup_gauge_data();
	
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CString Get_MC_VacuumRate()		{ return ch_3; }
	CString Get_LLC_VacuumRate()	{ return ch_2; }
		

	double GetMcVacuumRate() { return m_dPressure_MC; }
	double GetLlcVacuumRate() { return m_dPressure_LLC; }

	BOOL Is_GAUGE_Connected() { return m_bOpenPort_State; }

	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton4();
 };
