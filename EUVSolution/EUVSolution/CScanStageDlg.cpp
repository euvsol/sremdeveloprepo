﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CScanStageDlg 대화 상자

IMPLEMENT_DYNAMIC(CScanStageDlg, CDialogEx)

CScanStageDlg::CScanStageDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SCAN_STAGE_DIALOG, pParent)
{
	m_bZHoldOn = FALSE;
}

CScanStageDlg::~CScanStageDlg()
{
}

void CScanStageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PI_XPOS, m_editXmovestepCtrl);
	DDX_Control(pDX, IDC_EDIT_PI_YPOS, m_editYmovestepCtrl);
	DDX_Control(pDX, IDC_EDIT_PI_ZPOS, m_editZmovestepCtrl);
	DDX_Control(pDX, IDC_EDIT_PI_XTHETA_POS, m_editTipmovestepCtrl);
	DDX_Control(pDX, IDC_EDIT_PI_YTHETA_POS, m_editTiltmovestepCtrl);
	DDX_Control(pDX, IDC_EDIT_XPOS2, m_editXAbsolutePosCtrl);
	DDX_Control(pDX, IDC_EDIT_YPOS2, m_editYAbsolutePosCtrl);
	DDX_Control(pDX, IDC_EDIT_ZPOS2, m_editZAbsolutePosCtrl);
	DDX_Control(pDX, IDC_EDIT_XTHETA_POS2, m_editTipAbsolutePosCtrl);
	DDX_Control(pDX, IDC_EDIT_YTHETA_POS2, m_editTiltAbsolutePosCtrl);
	DDX_Control(pDX, IDC_TEXT_GRID_PISTAGELIST, m_StaticPIStatusGridCtrl);
	DDX_Control(pDX, IDC_EDIT_PISTATUS, m_editPIStatusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_YPLUS, m_PIbtnYPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_YMINUS, m_PIbtnYMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_XPLUS, m_PIbtnXPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_XMINUS, m_PIbtnXMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_ZPLUS, m_PIbtnZPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_ZMINUS, m_PIbtnZMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_YTHETAPLUS, m_PIbtnTYPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_YTHETAMINUS, m_PIbtnTYMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_XTHETAPLUS, m_PIbtnTXPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_PI_XTHETAMINUS, m_PIbtnTXMinusCtrl);
}


BEGIN_MESSAGE_MAP(CScanStageDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_PIMOVE_ZERO, &CScanStageDlg::OnBnClickedButtonPimoveZero)
	ON_BN_CLICKED(IDC_BUTTON_PIMOVE_ORIGIN, &CScanStageDlg::OnBnClickedButtonPimoveOrigin)
	ON_BN_CLICKED(IDC_BUTTON_PIDDL_INIT, &CScanStageDlg::OnBnClickedButtonPiddlInit)
	ON_BN_CLICKED(IDC_BUTTON_PI_STAGESCAN, &CScanStageDlg::OnBnClickedButtonPiStagescan)
	ON_BN_CLICKED(IDC_BUTTON_PI_ABSOLUTE_MOVE, &CScanStageDlg::OnBnClickedButtonPiAbsoluteMove)
	ON_BN_CLICKED(IDC_BUTTON_PI_XMINUS, &CScanStageDlg::OnBnClickedButtonPiXminus)
	ON_BN_CLICKED(IDC_BUTTON_PI_XPLUS, &CScanStageDlg::OnBnClickedButtonPiXplus)
	ON_BN_CLICKED(IDC_BUTTON_PI_YPLUS, &CScanStageDlg::OnBnClickedButtonPiYplus)
	ON_BN_CLICKED(IDC_BUTTON_PI_YMINUS, &CScanStageDlg::OnBnClickedButtonPiYminus)
	ON_BN_CLICKED(IDC_BUTTON_PISTAGE_XYORIGIN, &CScanStageDlg::OnBnClickedButtonPistageXyorigin)
	ON_BN_CLICKED(IDC_BUTTON_PI_ZPLUS, &CScanStageDlg::OnBnClickedButtonPiZplus)
	ON_BN_CLICKED(IDC_BUTTON_PI_ZMINUS, &CScanStageDlg::OnBnClickedButtonPiZminus)
	ON_BN_CLICKED(IDC_BUTTON_PISTAGE_ZORIGIN, &CScanStageDlg::OnBnClickedButtonPistageZorigin)
	ON_BN_CLICKED(IDC_BUTTON_PI_XTHETAMINUS, &CScanStageDlg::OnBnClickedButtonPiTipminus)
	ON_BN_CLICKED(IDC_BUTTON_PI_XTHETAPLUS, &CScanStageDlg::OnBnClickedButtonPiTipplus)
	ON_BN_CLICKED(IDC_BUTTON_PI_YTHETAPLUS, &CScanStageDlg::OnBnClickedButtonPiTiltplus)
	ON_BN_CLICKED(IDC_BUTTON_PI_YTHETAMINUS, &CScanStageDlg::OnBnClickedButtonPiTiltminus)
	ON_BN_CLICKED(IDC_BUTTON_PISTAGE_XYTHETAORIGIN, &CScanStageDlg::OnBnClickedButtonPistageXythetaorigin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_PI_X, &CScanStageDlg::OnDeltaposSpinPiX)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_PI_Y, &CScanStageDlg::OnDeltaposSpinPiY)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_PI_Z, &CScanStageDlg::OnDeltaposSpinPiZ)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_PI_XTHETA, &CScanStageDlg::OnDeltaposSpinPiXtheta)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_PI_YTHETA, &CScanStageDlg::OnDeltaposSpinPiYtheta)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_PI_SCANRECORD, &CScanStageDlg::OnBnClickedButtonPiScanrecord)
	ON_BN_CLICKED(IDC_CHECK_SERVO_ONOFF, &CScanStageDlg::OnBnClickedCheckServoOnoff)
	ON_BN_CLICKED(IDC_RE_CONNECTION_SCAN_STAGE, &CScanStageDlg::OnBnClickedReConnectionScanStage)
	ON_BN_CLICKED(IDC_DISCONNECTION_SCAN_STAGE, &CScanStageDlg::OnBnClickedDisconnectionScanStage)
END_MESSAGE_MAP()


// CScanStageDlg 메시지 처리기


BOOL CScanStageDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CScanStageDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	if (g_pConfig->m_nEquipmentMode != OFFLINE)
		Move_Origin();
}

BOOL CScanStageDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	//m_btn_X_Minus_Ctrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2LT));
	//m_btn_Y_Minus_Ctrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2DN));
	//m_btn_X_Plus_Ctrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2RT));
	//m_btn_Y_Plus_Ctrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2UP));

	// ICON
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);


	m_PIbtnYPlusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2UP));
	m_PIbtnYMinusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2DN));
	m_PIbtnXPlusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2RT));
	m_PIbtnXMinusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2LT));
	m_PIbtnZPlusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2UP));
	m_PIbtnZMinusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2DN));
	m_PIbtnTYPlusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2UP));
	m_PIbtnTYMinusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2DN));
	m_PIbtnTXPlusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2RT));
	m_PIbtnTXMinusCtrl.SetIcon(AfxGetApp()->LoadIconA(IDI_ARW2LT));

	CString str;
	str.Format(_T("%10.4f"), m_dPIStage_MovePos[X_AXIS]);
	m_editXmovestepCtrl.SetWindowText(str);
	str.Format(_T("%10.4f"), m_dPIStage_MovePos[Y_AXIS]);
	m_editYmovestepCtrl.SetWindowText(str);
	str.Format(_T("%10.4f"), m_dPIStage_MovePos[Z_AXIS]);
	m_editZmovestepCtrl.SetWindowText(str);
	str.Format(_T("%10.4f"), m_dPIStage_MovePos[TIP_AXIS]);
	m_editTipmovestepCtrl.SetWindowText(str);
	str.Format(_T("%10.4f"), m_dPIStage_MovePos[TILT_AXIS]);
	m_editTiltmovestepCtrl.SetWindowText(str);

	str.Format(_T("%10.4f"), m_dPIStage_MovePos[X_AXIS]);
	m_editXAbsolutePosCtrl.SetWindowText(str);
	str.Format(_T("%10.4f"), m_dPIStage_MovePos[Y_AXIS]);
	m_editYAbsolutePosCtrl.SetWindowText(str);
	str.Format(_T("%10.4f"), m_dPIStage_MovePos[Z_AXIS]);
	m_editZAbsolutePosCtrl.SetWindowText(str);
	str.Format(_T("%10.4f"), m_dPIStage_MovePos[TIP_AXIS]);
	m_editTipAbsolutePosCtrl.SetWindowText(str);
	str.Format(_T("%10.4f"), m_dPIStage_MovePos[TILT_AXIS]);
	m_editTiltAbsolutePosCtrl.SetWindowText(str);

	InitPIStageGrid();

	DDL_DataLoadingFromFile();

	SetTimer(SCANSTAGE_UPDATE_TIMER, 1000, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CScanStageDlg::InitPIStageGrid()
{
	CRect rect;
	int Grid_Size_X, Grid_Size_Y;
	m_StaticPIStatusGridCtrl.GetWindowRect(rect);
	ScreenToClient(&rect);
	m_PIStageGrid.Create(rect, this, IDC_PISTAGELISTGRID);

	Grid_Size_X = 3;
	Grid_Size_Y = 5;

	m_PIStageGrid.SetEditable(true);
	m_PIStageGrid.EnableDragAndDrop(true);
	m_PIStageGrid.GetDefaultCell(TRUE, TRUE)->SetBackClr(RGB(255, 255, 224));
	m_PIStageGrid.SetFixedColumnCount(2);
	m_PIStageGrid.SetListMode(true);
	m_PIStageGrid.SetBkColor(RGB(192, 192, 192));

	m_PIStageGrid.SetColumnCount(Grid_Size_X);
	m_PIStageGrid.SetRowCount(Grid_Size_Y);
	m_PIStageGrid.SetFixedRowSelection(true);
	m_PIStageGrid.SetBkColor(RGB(0xFF, 0xFF, 0xE0));
	m_PIStageGrid.GetDefaultCell(TRUE, TRUE)->SetBackClr(RGB(255, 255, 224));
	m_PIStageGrid.SetRowResize(true);

	LOGFONT lf;
	CFont Font;
	m_PIStageGrid.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	Font.CreateFontIndirect(&lf);
	m_PIStageGrid.SetFont(&Font);
	Font.DeleteObject();

	m_PIStageGrid.SetCompareFunction(m_PIStageGrid.pfnCellNumericCompare);

	CString str;
	str.Format("%s", "X(um)");
	m_PIStageGrid.SetItemText(0, 0, (char*)LPCTSTR(str));
	str.Format("%s", "0~10.000");
	m_PIStageGrid.SetItemText(0, 1, (char*)LPCTSTR(str));
	str.Format("%s", "Y(um)");
	m_PIStageGrid.SetItemText(1, 0, (char*)LPCTSTR(str));
	str.Format("%s", "0~10.000");
	m_PIStageGrid.SetItemText(1, 1, (char*)LPCTSTR(str));
	str.Format("%s", "Z(um)");
	m_PIStageGrid.SetItemText(2, 0, (char*)LPCTSTR(str));
	str.Format("%s", "-500~500");
	m_PIStageGrid.SetItemText(2, 1, (char*)LPCTSTR(str));
	str.Format("%s", "Tip(urad)");
	m_PIStageGrid.SetItemText(3, 0, (char*)LPCTSTR(str));
	str.Format("%s", "-1000~1000");
	m_PIStageGrid.SetItemText(3, 1, (char*)LPCTSTR(str));
	str.Format("%s", "Tilt(urad)");
	m_PIStageGrid.SetItemText(4, 0, (char*)LPCTSTR(str));
	str.Format("%s", "-1000~1000");
	m_PIStageGrid.SetItemText(4, 1, (char*)LPCTSTR(str));

	for (int i = 0; i < 5; i++)
		m_PIStageGrid.SetItemTextFmt(i, 2, "%4.3f", 0.0);

	m_PIStageGrid.AutoSizeColumns();
	m_PIStageGrid.AutoSizeRows();
	m_PIStageGrid.ExpandToFit();

	for (int i = 0; i < Grid_Size_X; i++)
		for (int j = 0; j < Grid_Size_Y; j++)
			m_PIStageGrid.SetItemFormat(j, i, DT_CENTER | DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER);
}

void CScanStageDlg::UpdateDataScanStage()
{
	if (g_pMaskMap == NULL)
		return;
	//g_pMaskMap->m_dPIStagePositionX = m_dPIStage_GetPos[X_AXIS];
	//g_pMaskMap->m_dPIStagePositionY = m_dPIStage_GetPos[Y_AXIS];
	//g_pMaskMap->m_dPIStagePositionZ = m_dPIStage_GetPos[Z_AXIS];
	//g_pMaskMap->m_dPIStagePositionTip = m_dPIStage_GetPos[TIP_AXIS];
	//g_pMaskMap->m_dPIStagePositionTilt = m_dPIStage_GetPos[TILT_AXIS];
	//g_pMaskMap->UpdateData(FALSE);

	CString tmp;
	tmp.Format("%2.4f", m_dPIStage_GetPos[X_AXIS]);
	if (g_pMaskMap->m_EditPIStagePosXCtrl.m_hWnd != NULL)
		g_pMaskMap->m_EditPIStagePosXCtrl.SetWindowText(tmp);
	tmp.Format("%2.4f", m_dPIStage_GetPos[Y_AXIS]);
	if (g_pMaskMap->m_EditPIStagePosYCtrl.m_hWnd != NULL)
		g_pMaskMap->m_EditPIStagePosYCtrl.SetWindowText(tmp);
	tmp.Format("%3.4f", m_dPIStage_GetPos[Z_AXIS]);
	if (g_pMaskMap->m_EditPIStagePosZCtrl.m_hWnd != NULL)
		g_pMaskMap->m_EditPIStagePosZCtrl.SetWindowText(tmp);
	tmp.Format("%4.4f", m_dPIStage_GetPos[TIP_AXIS]);
	if (g_pMaskMap->m_EditPIStagePosTipCtrl.m_hWnd != NULL)
		g_pMaskMap->m_EditPIStagePosTipCtrl.SetWindowText(tmp);
	tmp.Format("%4.4f", m_dPIStage_GetPos[TILT_AXIS]);
	if (g_pMaskMap->m_EditPIStagePosTiltCtrl.m_hWnd != NULL)
		g_pMaskMap->m_EditPIStagePosTiltCtrl.SetWindowText(tmp);

	m_PIStageGrid.SetItemTextFmt(X_AXIS, 2, "%2.4f", m_dPIStage_GetPos[X_AXIS]);
	m_PIStageGrid.SetItemTextFmt(Y_AXIS, 2, "%2.4f", m_dPIStage_GetPos[Y_AXIS]);
	m_PIStageGrid.SetItemTextFmt(Z_AXIS, 2, "%3.4f", m_dPIStage_GetPos[Z_AXIS]);
	m_PIStageGrid.SetItemTextFmt(TIP_AXIS, 2, "%4.4f", m_dPIStage_GetPos[TIP_AXIS]);
	m_PIStageGrid.SetItemTextFmt(TILT_AXIS, 2, "%4.4f", m_dPIStage_GetPos[TILT_AXIS]);

	m_PIStageGrid.Invalidate(false);
}

int CScanStageDlg::Start_Scan(int nScanMode, int nFov_nm, int nStepSize, int nRepeatNo)
{
	int ret = 0;

	// Scan stage 구동 코드
	switch (g_pMaskMap->m_CheckBidirectionCtrl.GetCheck())
	{
	case BST_CHECKED:	//Bi-direction
		if (nFov_nm == 1000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 36;
				else
					g_pScanStage->m_nStrokeNo = 14;
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 37;
				else
					g_pScanStage->m_nStrokeNo = 15;
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 38;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 1500)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 39;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 40;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 41;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 2000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 20;
				else
					g_pScanStage->m_nStrokeNo = 16;
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 21;
				else
					g_pScanStage->m_nStrokeNo = 17;
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 35;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 3000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 42;
				else
					g_pScanStage->m_nStrokeNo = 18;
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 43;
				else
					g_pScanStage->m_nStrokeNo = 19;
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 10000)
		{
			if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 26;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 80)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 27;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 100)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 44;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 200)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 45;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else
		{
			::AfxMessageBox("생성되지 않은 FOV 입니다.");
			return -1;
		}
		break;
	case BST_UNCHECKED:		//Uni-direction
		if (nFov_nm == 1000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 29;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 30;
				else
					g_pScanStage->m_nStrokeNo = 0;
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 31;
				else
					g_pScanStage->m_nStrokeNo = 1;
			}
			else if (nStepSize == 80)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
				else
				{
					g_pScanStage->m_nStrokeNo = 2;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 1500)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 32;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 33;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 34;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 2000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 22;
				else
					g_pScanStage->m_nStrokeNo = 4;
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 23;
				else
					g_pScanStage->m_nStrokeNo = 5;
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 3000)
		{
			if (nStepSize == 10)
			{
				g_pScanStage->m_nStrokeNo = 6;
			}
			else if (nStepSize == 20)
			{
				g_pScanStage->m_nStrokeNo = 7;
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 7000)
		{
			if (nStepSize == 40)
			{
				g_pScanStage->m_nStrokeNo = 8;
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 10000)
		{
			//if (nStepSize == 10)
			//{
			//	g_pScanStage->m_nStrokeNo = 9;
			//}
			if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 28;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 24;
				else
					g_pScanStage->m_nStrokeNo = 11;
			}
			else if (nStepSize == 80)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 25;
				else
					g_pScanStage->m_nStrokeNo = 12;
			}
			//else if (nStepSize == 160)
			//{
			//	g_pScanStage->m_nStrokeNo = 13;
			//}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else
		{
			::AfxMessageBox("생성되지 않은 FOV 입니다.");
			return -1;
		}
		break;
	default:
		break;
	}
	
	CString strLog;

	switch (nScanMode)
	{
	case SCAN_MODE_IMAGING:
		
		g_pMaskMap->m_strStageStatus.Format(_T("Scan Start!"));
		g_pMaskMap->UpdateData(FALSE);
		
		strLog.Format("Begin(Start_Scan): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);


		//1.Encoder 전환		
		strLog.Format("Begin(Encoder_to_Laser): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
		//{
			//g_pNavigationStage->SetEncoderMode();
			g_pNavigationStage->SetEncoderModeWithDriftReset(); //추후 변경 예정
		//}
		strLog.Format("End(Encoder_to_Laser): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		//1. 셔터 오픈
		strLog.Format("Begin(Shutter_Open): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		if (g_pEUVSource != NULL && g_pEUVSource->Is_SRC_Connected() == TRUE)
			g_pEUVSource->SetMechShutterOpen(TRUE);

		strLog.Format("End(Shutter_Open): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		//2. 스캐닝
		strLog.Format("Begin(Scanning_Motion): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		Scanning_Motion(nRepeatNo);
		strLog.Format("End(Scanning_Motion): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);


		//3.Laser전환		
		strLog.Format("Begin(Encoder_to_Laser): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
		//{
			//g_pNavigationStage->SetLaserMode();
			//WaitSec(0.1);
			g_pNavigationStage->SetLaserModeWithDriftCorrection(); //추후 변경 예정 

		//}
		strLog.Format("End(Encoder_to_Laser): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		//4. 셔터 클로즈
		strLog.Format("Begin(Shutter_Close): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		if (g_pEUVSource != NULL && g_pEUVSource->Is_SRC_Connected() == TRUE)
			g_pEUVSource->SetMechShutterOpen(FALSE);
		strLog.Format("End(Shutter_Close): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
	
		
		strLog.Format("End(Start_Scan): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		g_pMaskMap->m_strStageStatus.Format(_T("Scan End!"));

		break;
	case SCAN_MODE_DDL_GERNERATION:
		g_pMaskMap->m_strStageStatus.Format(_T("DDL Generation Start!"));
		g_pMaskMap->UpdateData(FALSE);
		DDL_Generation();
		g_pMaskMap->m_strStageStatus.Format(_T("DDL Generation End!"));
		g_pMaskMap->UpdateData(FALSE);
		break;
	case SCAN_MODE_RECORD:
		g_pMaskMap->m_strStageStatus.Format(_T("Scan Record Start!"));
		g_pMaskMap->UpdateData(FALSE);
		RecordScanProfile();
		g_pMaskMap->m_strStageStatus.Format(_T("Scan Record End!"));
		g_pMaskMap->UpdateData(FALSE);
		break;
	default:
		break;
	}

	g_pMaskMap->UpdateData(FALSE);

	return ret;
}



//2021.03.17 old ihlee
#if  FALSE
int CScanStageDlg::Start_Scan(int nScanMode, int nFov_nm, int nStepSize, int nRepeatNo)
{
	int ret = 0;


	// Scan stage 구동 코드
	switch (g_pMaskMap->m_CheckBidirectionCtrl.GetCheck())
	{
	case BST_CHECKED:	//Bi-direction
		if (nFov_nm == 1000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 36;
				else
					g_pScanStage->m_nStrokeNo = 14;
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 37;
				else
					g_pScanStage->m_nStrokeNo = 15;
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 38;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 1500)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 39;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 40;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 41;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 2000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 20;
				else
					g_pScanStage->m_nStrokeNo = 16;
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 21;
				else
					g_pScanStage->m_nStrokeNo = 17;
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 35;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 3000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 42;
				else
					g_pScanStage->m_nStrokeNo = 18;
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 43;
				else
					g_pScanStage->m_nStrokeNo = 19;
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 10000)
		{
			if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 26;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 80)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 27;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else
		{
			::AfxMessageBox("생성되지 않은 FOV 입니다.");
			return -1;
		}
		break;
	case BST_UNCHECKED:		//Uni-direction
		if (nFov_nm == 1000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 29;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 30;
				else
					g_pScanStage->m_nStrokeNo = 0;
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 31;
				else
					g_pScanStage->m_nStrokeNo = 1;
			}
			else if (nStepSize == 80)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
				else
				{
					g_pScanStage->m_nStrokeNo = 2;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 1500)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 32;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 33;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 34;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 2000)
		{
			if (nStepSize == 10)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 22;
				else
					g_pScanStage->m_nStrokeNo = 4;
			}
			else if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 23;
				else
					g_pScanStage->m_nStrokeNo = 5;
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 3000)
		{
			if (nStepSize == 10)
			{
				g_pScanStage->m_nStrokeNo = 6;
			}
			else if (nStepSize == 20)
			{
				g_pScanStage->m_nStrokeNo = 7;
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 7000)
		{
			if (nStepSize == 40)
			{
				g_pScanStage->m_nStrokeNo = 8;
			}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else if (nFov_nm == 10000)
		{
			//if (nStepSize == 10)
			//{
			//	g_pScanStage->m_nStrokeNo = 9;
			//}
			if (nStepSize == 20)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
				{
					g_pScanStage->m_nStrokeNo = 28;
				}
				else
				{
					::AfxMessageBox("생성되지 않은 FOV 입니다.");
					return -1;
				}
			}
			else if (nStepSize == 40)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 24;
				else
					g_pScanStage->m_nStrokeNo = 11;
			}
			else if (nStepSize == 80)
			{
				if (g_pMaskMap->m_bLaserFrequency5Kz == TRUE)
					g_pScanStage->m_nStrokeNo = 25;
				else
					g_pScanStage->m_nStrokeNo = 12;
			}
			//else if (nStepSize == 160)
			//{
			//	g_pScanStage->m_nStrokeNo = 13;
			//}
			else
			{
				::AfxMessageBox("생성되지 않은 FOV 입니다.");
				return -1;
			}
		}
		else
		{
			::AfxMessageBox("생성되지 않은 FOV 입니다.");
			return -1;
		}
		break;
	default:
		break;
	}

	CString strLog;

	switch (nScanMode)
	{
	case SCAN_MODE_IMAGING:

		g_pMaskMap->m_strStageStatus.Format(_T("Scan Start!"));
		g_pMaskMap->UpdateData(FALSE);


		strLog.Format("Begin(Start_Scan): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		//1.ADAM으로부터 Laseer 읽어오기				
		strLog.Format("Begin(Laser_Read1): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		g_pAdam->Command_AverageRunAfterTimeout();
		double laserXnm_old, laserYnm_old;
		laserXnm_old = g_pAdam->AdamData.m_dX_position;
		laserYnm_old = g_pAdam->AdamData.m_dY_position;

		strLog.Format("End(Laser_Read1): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		//2.엔코더전환
		strLog.Format("Begin(Laser_to_Encoder): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
		{
			g_pNavigationStage->SetEncoderMode();
			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
		}
		strLog.Format("End(Laser_to_Encoder): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		strLog.Format("Begin(ADAMRunStart): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		g_pAdam->ADAMRunStart();
		strLog.Format("End(ADAMRunStart): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);


		strLog.Format("Begin(Shutter_Open): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		if (g_pEUVSource != NULL && g_pEUVSource->Is_SRC_Connected() == TRUE)
			g_pEUVSource->SetMechShutterOpen(TRUE);

		strLog.Format("End(Shutter_Open): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		strLog.Format("Begin(Scanning_Motion): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		Scanning_Motion(nRepeatNo);
		strLog.Format("End(Scanning_Motion): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		WaitSec(1);
		strLog.Format("Begin(Command_ADAMStop): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		g_pAdam->Command_ADAMStop();
		strLog.Format("End(Command_ADAMStop): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		strLog.Format("Begin(Shutter_Close): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		if (g_pEUVSource != NULL && g_pEUVSource->Is_SRC_Connected() == TRUE)
			g_pEUVSource->SetMechShutterOpen(FALSE);
		strLog.Format("End(Shutter_Close): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		strLog.Format("Begin(WaitSec): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		WaitSec(1);// Scan Stage Y축 복귀시간  ihlee
		strLog.Format("End(WaitSec): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);


		//4.Laser전환		
		strLog.Format("Begin(Encoder_to_Laser): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
		{
			g_pNavigationStage->SetLaserMode();
			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
		}
		strLog.Format("End(Encoder_to_Laser): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		/*	g_pAdam->Command_AverageRunAfterTimeout();
			double laserXnm_current1, laserYnm_current1;
			laserXnm_current1 = g_pAdam->AdamData.m_dX_position;
			laserYnm_current1 = g_pAdam->AdamData.m_dY_position;*/

			//3.ADAM으로부터 Laseer 읽어오기
		strLog.Format("Begin(Laser_Read2): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		g_pAdam->Command_AverageRunAfterTimeout();
		double laserXnm_current, laserYnm_current;
		laserXnm_current = g_pAdam->AdamData.m_dX_position;
		laserYnm_current = g_pAdam->AdamData.m_dY_position;
		strLog.Format("End(Laser_Read2): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		//5.SetPos 델타, int CACSMC4UCtrl::SetPosmm(int axis, double pos_mm)
		double delXnm, delYnm;
		delXnm = laserXnm_current - laserXnm_old;
		delYnm = laserYnm_current - laserYnm_old;

		//GetPosmm
		/*double LinearScaleXmm, LinearScaleYmm;
		LinearScaleXmm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		LinearScaleYmm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);*/

		//아래로 하면 더 정확???
		double LinearScaleXmm, LinearScaleYmm;
		LinearScaleXmm = g_pNavigationStage->GetTargetPosmm(STAGE_X_AXIS); //GetTargetPosmm  GetPosmm
		LinearScaleYmm = g_pNavigationStage->GetTargetPosmm(STAGE_Y_AXIS);
		/*double errx, erry;
		errx = (LinearScaleXmm2 - LinearScaleXmm) * 1000000;
		erry = (LinearScaleYmm2 - LinearScaleYmm) * 1000000;*/

		strLog.Format("Begin(SetPosmm): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		g_pNavigationStage->SetPosmm(STAGE_X_AXIS, LinearScaleXmm - delXnm / 1000000.0);	//X는 Stage와 반대방향
		g_pNavigationStage->SetPosmm(STAGE_Y_AXIS, LinearScaleYmm + delYnm / 1000000.0);
		strLog.Format("End(SetPosmm): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		//6.Delta 만큼 이동(보상)
		strLog.Format("Begin(Stage_Correction): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(LinearScaleXmm, LinearScaleYmm);
		strLog.Format("End(Stage_Correction): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		strLog.Format("End(Start_Scan): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogScanStage", strLog);

		g_pMaskMap->m_strStageStatus.Format(_T("Scan End!"));


		break;
	case SCAN_MODE_DDL_GERNERATION:
		g_pMaskMap->m_strStageStatus.Format(_T("DDL Generation Start!"));
		g_pMaskMap->UpdateData(FALSE);
		DDL_Generation();
		g_pMaskMap->m_strStageStatus.Format(_T("DDL Generation End!"));
		g_pMaskMap->UpdateData(FALSE);
		break;
	case SCAN_MODE_RECORD:
		g_pMaskMap->m_strStageStatus.Format(_T("Scan Record Start!"));
		g_pMaskMap->UpdateData(FALSE);
		RecordScanProfile();
		g_pMaskMap->m_strStageStatus.Format(_T("Scan Record End!"));
		g_pMaskMap->UpdateData(FALSE);
		break;
	default:
		break;
	}

	g_pMaskMap->UpdateData(FALSE);

	return ret;
}
#endif
int CScanStageDlg::Stop_Scan()
{
	int ret = 0;

	m_bScanStop = TRUE;
	//WaitSec(0.2);

	return ret;
}

void CScanStageDlg::OnBnClickedButtonPimoveZero()
{
	g_pLog->Display(0, _T("CScanStageDlg::OnBnClickedButtonPimoveZero() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	if (pwdlg.m_strTxt != "srem")
	{
		::AfxMessageBox("Password가 일치 하지 않습니다.");
		return;
	}

	Move_AllAxis_Zero_Position();
}


void CScanStageDlg::OnBnClickedButtonPimoveOrigin()
{
	g_pLog->Display(0, _T("CScanStageDlg::OnBnClickedButtonPimoveOrigin() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	Move_Origin();
}


void CScanStageDlg::OnBnClickedButtonPiddlInit()
{
	g_pLog->Display(0, _T("CScanStageDlg::OnBnClickedButtonPiddlInit() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	//1. 물류 Thread 가동 중인지 확인

	DDL_Generation();
}

void CScanStageDlg::OnBnClickedButtonPiStagescan()
{
	g_pLog->Display(0, _T("OnBnClickedButtonPiStagescan() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	// Laser Interferometer 확인해서 값이 이상없으면 Scan 시작하자

	CString strStageStatus;
	strStageStatus.Format("%s", "Scan Start");
	m_editPIStatusCtrl.SetWindowText(strStageStatus);

	WaveGeneration();
	LoadDDLdatatoController();
	Scanning_Motion(1);

	strStageStatus.Format("%s", "Scan End");
	m_editPIStatusCtrl.SetWindowText(strStageStatus);

}

void CScanStageDlg::OnBnClickedButtonPiScanrecord()
{
	g_pLog->Display(0, _T("OnBnClickedButtonPiScanrecord() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStageStatus;
	strStageStatus.Format("%s", "Scan Record Start");
	m_editPIStatusCtrl.SetWindowText(strStageStatus);

	WaveGeneration();
	LoadDDLdatatoController();
	RecordScanProfile();

	strStageStatus.Format("%s", "Scan Record End");
	m_editPIStatusCtrl.SetWindowText(strStageStatus);

}

int CScanStageDlg::Move_Origin()
{
	int ret = 0;

	MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	Move_AllAxis_Initial_Position();

	return ret;
}

int CScanStageDlg::MoveRelative(int axis, double pos_um)
{
	int ret = 0;
	int i = 0;
	double absolute_pos;

	GetPosAxesData();
	absolute_pos = pos_um + m_dPIStage_GetPos[axis];

	if (absolute_pos >= m_dPIStage_MinusLimitPos[axis] && absolute_pos <= m_dPIStage_PlusLimitPos[axis])
	{
		for (i = 0; i < AXIS_NUMBER; i++)
			m_dPIStage_MovePos[i] = 0;

		m_dPIStage_MovePos[axis] = pos_um;

		PI_Move_Relative();
		GetPosAxesData();
		UpdateDataScanStage();
	}

	return ret;
}


void CScanStageDlg::MoveThroughfocusZ()
{
	//물류동작 안하는지, 마스크 있는지 확인하고 아래 실행
	//double position_inc;

	//m_focus_step.GetWindowText(m_str);
	//position_inc = atof(m_str);
	//MoveZRelative_SlowInterlock(position_inc, MINUS_DIRECTION);
}

void CScanStageDlg::OnBnClickedButtonPiXminus()
{
	g_pLog->Display(0, _T("CScanStageDlg::OnBnClickedButtonPiXminus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_editXmovestepCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (MoveRelative(X_AXIS, -dInc_position) != 0)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CScanStageDlg::OnBnClickedButtonPiXplus()
{
	g_pLog->Display(0, _T("[PI E712] X PLUS 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_editXmovestepCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (MoveRelative(X_AXIS, dInc_position) != 0)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CScanStageDlg::OnBnClickedButtonPiYplus()
{
	g_pLog->Display(0, _T("[PI E712] Y PLUS 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_editXmovestepCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (MoveRelative(Y_AXIS, dInc_position) != 0)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CScanStageDlg::OnBnClickedButtonPiYminus()
{
	g_pLog->Display(0, _T("[PI E712] Y MINUS 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_editXmovestepCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (MoveRelative(Y_AXIS, -dInc_position) != 0)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CScanStageDlg::OnBnClickedButtonPistageXyorigin()
{
	GetPosAxesData();
	UpdateDataScanStage();
}

void CScanStageDlg::OnBnClickedButtonPiZplus()
{
	CString str;
	m_editZmovestepCtrl.GetWindowText(str);
	double dInc_position;
	dInc_position = atof(str);
	str.Format(_T("[PI E712] Z PLUS %.3f um 버튼 클릭!"), dInc_position);
	g_pLog->Display(0, str);

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	MoveZRelative_SlowInterlock(dInc_position, PLUS_DIRECTION);
}

void CScanStageDlg::OnBnClickedButtonPiZminus()
{
	g_pLog->Display(0, _T("[PI E712] Z MINUS 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	double dInc_position;
	CString str;
	m_editZmovestepCtrl.GetWindowText(str);
	dInc_position = atof(str);

	MoveZRelative_SlowInterlock(dInc_position, MINUS_DIRECTION);
}

void CScanStageDlg::OnBnClickedButtonPistageZorigin()
{
	GetPosAxesData();
	UpdateDataScanStage();
}


void CScanStageDlg::OnBnClickedButtonPiTipminus()
{
	g_pLog->Display(0, _T("[PI E712] TIP axis MINUS 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_editXmovestepCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (MoveRelative(TIP_AXIS, -dInc_position) != 0)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CScanStageDlg::OnBnClickedButtonPiTipplus()
{
	g_pLog->Display(0, _T("[PI E712] TIP axis PLUS 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_editXmovestepCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (MoveRelative(TIP_AXIS, dInc_position) != 0)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CScanStageDlg::OnBnClickedButtonPiTiltplus()
{
	g_pLog->Display(0, _T("[PI E712] TILT axis PLUS 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_editXmovestepCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (MoveRelative(TILT_AXIS, dInc_position) != 0)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CScanStageDlg::OnBnClickedButtonPiTiltminus()
{
	g_pLog->Display(0, _T("[PI E712] TILT axis MINUS 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_editXmovestepCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (MoveRelative(TILT_AXIS, -dInc_position) != 0)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CScanStageDlg::OnBnClickedButtonPistageXythetaorigin()
{
	GetPosAxesData();
	UpdateDataScanStage();
}


void CScanStageDlg::OnBnClickedButtonPiAbsoluteMove()
{
	g_pLog->Display(0, _T("CScanStageDlg::OnBnClickedButtonPiAbsoluteMove() 버튼 클릭!"));

	//물류 Thread 진행 중일때는 동작하면 안됨.
	//Z axis은 Slip 발생할 수 있으니 천천히 움직여야 함.

	CString str;
	m_editXAbsolutePosCtrl.GetWindowText(str);
	m_dPIStage_MovePos[X_AXIS] = atof(str);
	m_editYAbsolutePosCtrl.GetWindowText(str);
	m_dPIStage_MovePos[Y_AXIS] = atof(str);
	m_editZAbsolutePosCtrl.GetWindowText(str);
	m_dPIStage_MovePos[Z_AXIS] = atof(str);
	m_editTipAbsolutePosCtrl.GetWindowText(str);
	m_dPIStage_MovePos[TIP_AXIS] = atof(str);
	m_editTiltAbsolutePosCtrl.GetWindowText(str);
	m_dPIStage_MovePos[TILT_AXIS] = atof(str);

	if (m_dPIStage_MovePos[X_AXIS] < 0)
		m_dPIStage_MovePos[X_AXIS] = 0;
	else if (m_dPIStage_MovePos[X_AXIS] > X_PLUSLIMIT)
		m_dPIStage_MovePos[X_AXIS] = X_PLUSLIMIT;

	if (m_dPIStage_MovePos[Y_AXIS] < 0)
		m_dPIStage_MovePos[Y_AXIS] = 0;
	else if (m_dPIStage_MovePos[Y_AXIS] > Y_PLUSLIMIT)
		m_dPIStage_MovePos[Y_AXIS] = Y_PLUSLIMIT;

	if (m_dPIStage_MovePos[Z_AXIS] < Z_MINUSLIMIT)
		m_dPIStage_MovePos[Z_AXIS] = Z_MINUSLIMIT;
	else if (m_dPIStage_MovePos[Z_AXIS] > m_dZUpperLimit)
		m_dPIStage_MovePos[Z_AXIS] = m_dZUpperLimit;

	if (m_dPIStage_MovePos[TIP_AXIS] < TIP_MINUSLIMIT)
		m_dPIStage_MovePos[TIP_AXIS] = TIP_MINUSLIMIT;
	else if (m_dPIStage_MovePos[TIP_AXIS] > TIP_PLUSLIMIT)
		m_dPIStage_MovePos[TIP_AXIS] = TIP_PLUSLIMIT;

	if (m_dPIStage_MovePos[TILT_AXIS] < TILT_MINUSLIMIT)
		m_dPIStage_MovePos[TILT_AXIS] = TILT_MINUSLIMIT;
	else if (m_dPIStage_MovePos[TILT_AXIS] > TILT_PLUSLIMIT)
		m_dPIStage_MovePos[TILT_AXIS] = TILT_PLUSLIMIT;

	str.Format(_T("%.4f"), m_dPIStage_MovePos[X_AXIS]);
	m_editXAbsolutePosCtrl.SetWindowText(str);

	str.Format(_T("%.4f"), m_dPIStage_MovePos[Y_AXIS]);
	m_editYAbsolutePosCtrl.SetWindowText(str);

	str.Format(_T("%.4f"), m_dPIStage_MovePos[Z_AXIS]);
	m_editZAbsolutePosCtrl.SetWindowText(str);

	str.Format(_T("%.4f"), m_dPIStage_MovePos[TIP_AXIS]);
	m_editTipAbsolutePosCtrl.SetWindowText(str);

	str.Format(_T("%.4f"), m_dPIStage_MovePos[TILT_AXIS]);
	m_editTiltAbsolutePosCtrl.SetWindowText(str);

	MoveZAbsolute_SlowInterlock(m_dPIStage_MovePos[Z_AXIS]);
	//PI_Move_Absolute();
}


void CScanStageDlg::OnDeltaposSpinPiX(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	double position;
	CString str;

	m_editXmovestepCtrl.GetWindowText(str);
	position = atof(str);

	if (pNMUpDown->iDelta < 0)
	{
		position++;

		if (position > X_PLUSLIMIT)
			position = X_PLUSLIMIT;

		str.Format(_T("%10.4f"), position);
		m_editXmovestepCtrl.SetWindowText(str);
	}
	else
	{
		position--;

		if (position < 0)
			position = 0;

		str.Format(_T("%10.4f"), position);
		m_editXmovestepCtrl.SetWindowText(str);
	}

	*pResult = 0;
}


void CScanStageDlg::OnDeltaposSpinPiY(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	double position;
	CString str;

	m_editYmovestepCtrl.GetWindowText(str);
	position = atof(str);

	if (pNMUpDown->iDelta < 0)
	{
		position++;

		if (position > Y_PLUSLIMIT)
			position = Y_PLUSLIMIT;

		str.Format(_T("%10.4f"), position);
		m_editYmovestepCtrl.SetWindowText(str);
	}
	else
	{
		position--;

		if (position < 0)
			position = 0;

		str.Format(_T("%10.4f"), position);
		m_editYmovestepCtrl.SetWindowText(str);
	}

	*pResult = 0;
}


void CScanStageDlg::OnDeltaposSpinPiZ(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	double position;
	CString str;

	m_editZmovestepCtrl.GetWindowText(str);
	position = atof(str);

	if (pNMUpDown->iDelta < 0)
	{
		position++;

		if (position > m_dZUpperLimit)
			position = m_dZUpperLimit;

		str.Format(_T("%10.4f"), position);
		m_editZmovestepCtrl.SetWindowText(str);
	}
	else
	{
		position--;

		if (position < 0)
			position = 0;

		str.Format(_T("%10.4f"), position);
		m_editZmovestepCtrl.SetWindowText(str);
	}

	*pResult = 0;
}


void CScanStageDlg::OnDeltaposSpinPiXtheta(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	double position;
	CString str;

	m_editTipmovestepCtrl.GetWindowText(str);
	position = atof(str);

	if (pNMUpDown->iDelta < 0)
	{
		position++;

		if (position > TIP_PLUSLIMIT)
			position = TIP_PLUSLIMIT;

		str.Format(_T("%10.4f"), position);
		m_editTipmovestepCtrl.SetWindowText(str);
	}
	else
	{
		position--;

		if (position < 0)
			position = 0;

		str.Format(_T("%10.4f"), position);
		m_editTipmovestepCtrl.SetWindowText(str);
	}

	*pResult = 0;
}


void CScanStageDlg::OnDeltaposSpinPiYtheta(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	double position;
	CString str;

	m_editTiltmovestepCtrl.GetWindowText(str);
	position = atof(str);

	if (pNMUpDown->iDelta < 0)
	{
		position++;

		if (position > TILT_PLUSLIMIT)
			position = TILT_PLUSLIMIT;

		str.Format(_T("%10.4f"), position);
		m_editTiltmovestepCtrl.SetWindowText(str);
	}
	else
	{
		position--;

		if (position < 0)
			position = 0;

		str.Format(_T("%10.4f"), position);
		m_editTiltmovestepCtrl.SetWindowText(str);
	}

	*pResult = 0;
}

void CScanStageDlg::OnTimer(UINT_PTR nIDEvent)
{
	int refCap;
	double refCapPos;
	double measureCapPos;

	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case SCANSTAGE_UPDATE_TIMER:
		//Stage_Connected_Check();
		if (m_bConnect)
		{
			GetPosAxesData();
			UpdateDataScanStage();
		}
		SetTimer(nIDEvent, 1000, NULL);
		break;
	case SCANSTAGE_ZHOLD_TIMER:
		if (m_bConnect)
		{
			//Z_Hold 조건: 1. m_bZHoldOn = TRUE, 2.Stage가 EUV 영역, 3.ELitho설비(?), 4.EUV Shutter Open(?)
			CPoint pt;
			pt.x = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			pt.y = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
			if (m_bZHoldOn == TRUE && g_pConfig->m_rcEUVStageAreaRect.PtInRect(pt) == TRUE /*&& g_pConfig->m_nEquipmentType == ELITHO && g_pEUVSource->Is_Shutter_Opened() == TRUE*/)
			{
				//g_pAdam->MoveZToGapPosition(m_ElithoGap_um);
				g_pAdam->MoveZCapsensorFocusPosition(0, 0, 0, &refCap, &refCapPos, &measureCapPos, 0);
				g_pNavigationStage->m_bMovingAvailable == FALSE;
			}
			else
			{
				g_pNavigationStage->m_bMovingAvailable == TRUE;
			}
		}
		SetTimer(nIDEvent, 5000, NULL);
		break;
		//case COMMAND_PI_TIMER:
		//	ScanStart();
		//	break;
	default:
		break;
	}
	__super::OnTimer(nIDEvent);
}

void CScanStageDlg::SetElithoGap(double gap_um)
{
	if (gap_um < g_pAdam->m_refGap_um)
		gap_um = g_pAdam->m_refGap_um;

	m_ElithoGap_um = gap_um;
}


void CScanStageDlg::OnBnClickedCheckServoOnoff()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CScanStageDlg::OnBnClickedReConnectionScanStage()
{
	int nRet = -1;

	CButton* btn = (CButton*)GetDlgItem(IDC_RE_CONNECTION_SCAN_STAGE);
	btn->SetCheck(false);

	nRet = DisconnectComm();
	if (nRet == FALSE)
	{
		g_pCommStat->ScanStageCommStatus(FALSE);
		g_pLoadingScreen.SetTextMessage(_T("Scan Stage DisConnection Fail!"));
		WaitSec(3);
		return;
	}
	else if (nRet == TRUE)
	{
		g_pCommStat->ScanStageCommStatus(TRUE);
		g_pLoadingScreen.SetTextMessage(_T("Scan Stage DisConnection Success!"));

	}

	if (g_pConfig->m_nEquipmentMode != OFFLINE && g_pConfig->m_nEquipmentType != EUVPTR)
	{
		nRet = ConnectComm(ETHERNET, g_pConfig->m_chIP[ETHERNET_SCAN_STAGE], g_pConfig->m_nPORT[ETHERNET_SCAN_STAGE]);
		if (nRet == FALSE)
		{
			g_pCommStat->ScanStageCommStatus(FALSE);
			g_pLoadingScreen.SetTextMessage(_T("Scan Stage Connection Fail!"));
			WaitSec(3);
		}
		else if (nRet == TRUE)
		{
			g_pScanStage->m_dInitialTx_urad = g_pConfig->m_dConfigCalTx_urad;
			g_pScanStage->m_dInitialTy_urad = g_pConfig->m_dConfigCalTy_urad;
			g_pScanStage->Initialize_PIStage();
			g_pCommStat->ScanStageCommStatus(TRUE);
			g_pLoadingScreen.SetTextMessage(_T("Scan Stage Connection Success!"));
		}
	}
}


void CScanStageDlg::OnBnClickedDisconnectionScanStage()
{
	int nRet = -1;
	CButton* btn = (CButton*)GetDlgItem(IDC_DISCONNECTION_SCAN_STAGE);
	btn->SetCheck(false);

	if (g_pConfig->m_nEquipmentMode != OFFLINE && g_pConfig->m_nEquipmentType != EUVPTR)
	{
		nRet = DisconnectComm();
		if (nRet == FALSE)
		{
			g_pCommStat->ScanStageCommStatus(FALSE);
			g_pLoadingScreen.SetTextMessage(_T("Scan Stage DisConnection Fail!"));
			WaitSec(3);
		}
		else if (nRet == TRUE)
		{
			g_pCommStat->ScanStageCommStatus(TRUE);
			g_pLoadingScreen.SetTextMessage(_T("Scan Stage DisConnection Success!"));
		}
	}
}

void CScanStageDlg::Stage_Connected_Check()
{
	if (!PI_Stage_Connected_Check())
	{
		//m_bConnect = FALSE;
		((CStatic*)GetDlgItem(IDC_ICON_CONNECT))->SetIcon(m_LedIcon[1]);
		GetDlgItem(IDC_RE_CONNECTION_SCAN_STAGE)->EnableWindow(true);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_CONNECT))->SetIcon(m_LedIcon[0]);
		GetDlgItem(IDC_RE_CONNECTION_SCAN_STAGE)->EnableWindow(false);

	}
}
