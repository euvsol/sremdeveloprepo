﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CMaskMapDlg 대화 상자

IMPLEMENT_DYNAMIC(CMaskMapDlg, CDialogEx)

CMaskMapDlg::CMaskMapDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MASK_MAP_DIALOG, pParent)
	, m_DateTimeStart(COleDateTime::GetCurrentTime())
	, m_DateTimeEnd(COleDateTime::GetCurrentTime())
	, m_bShowcode(FALSE)
	, m_bSelZoomCheck(FALSE)
	, m_bMaskMap(TRUE)
	, m_strStagePosName(_T("TBD"))
	, m_strStageStatus(_T("Stage Not Connected"))
	, m_strStageMovingDistance(_T("1.000000"))
	, m_strXMovePos(_T("0.0"))
	, m_strYMovePos(_T("0.0"))
	, m_dZInterlockPos(120)
	, m_bOMDisplay(FALSE)
	, m_nAutoRunRepeatSetNo(1)
	, m_bLaserSwichFunction(FALSE)
	, m_nAutoRunRepeatResultNo(0)
	, m_nMeasurementPointNo(1)
	, m_dMaskCoordinateX_mm(0.0)
	, m_dMaskCoordinateY_mm(0.0)
	, m_dZThroughFocusHeight(0.0)
	, m_dZFocusPos(0.0)
	, m_bBiDirectionFlag(TRUE)
	, m_bOMAlignOnly(FALSE)
	, m_bManualAlign(FALSE)
	, m_bLaserFeedback(FALSE)
	, m_bLaserFrequency5Kz(TRUE)
	, m_bReverseCoordinate(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	bFinding = false;

	m_nMapWidthPixelNo = 0;
	m_nMapHeightPixelNo = 0;

	m_dStageMovingDistance = 1.000000;
	m_bOMAlignComplete = FALSE;
	m_bEUVAlignComplete = FALSE;
	m_bAutoSequenceProcessing = FALSE;

	m_dOMAlignPointLB_posx_mm = 0.0, m_dOMAlignPointLB_posy_mm = 0.0;
	m_dOMAlignPointLT_posx_mm = 0.0, m_dOMAlignPointLT_posy_mm = 0.0;
	m_dOMAlignPointRT_posx_mm = 0.0, m_dOMAlignPointRT_posy_mm = 0.0;
	m_dEUVAlignPointLB_posx_mm = 0.0, m_dEUVAlignPointLB_posy_mm = 0.0;
	m_dEUVAlignPointLT_posx_mm = 0.0, m_dEUVAlignPointLT_posy_mm = 0.0;
	m_dEUVAlignPointRT_posx_mm = 0.0, m_dEUVAlignPointRT_posy_mm = 0.0;

	m_dNotchAlignPoint1_posx_mm = 0.0, m_dNotchAlignPoint1_posy_mm = 0.0;
	m_dNotchAlignPoint2_posx_mm = 0.0, m_dNotchAlignPoint2_posy_mm = 0.0;

	m_nEuvLitho_ScanGrid = 100;
}

CMaskMapDlg::~CMaskMapDlg()
{
	m_MaskMapWnd.m_MacroSystem.DeleteCom();
}


// CMaskMapDlg 메시지 처리기



void CMaskMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DATETIME_START, m_DateTimeStartCtrl);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_START, m_DateTimeStart);
	DDX_Control(pDX, IDC_DATETIME_END, m_DateTimeEndCtrl);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_END, m_DateTimeEnd);
	DDX_Control(pDX, IDC_LIST_FILELIST, m_ListCtrlRecipeFile);
	DDX_Control(pDX, IDC_BUTTON_SEARCHFILE, m_BtnSearchFileCtrl);
	DDX_Check(pDX, IDC_CHECK_DEFECTCODEDISPLAY, m_bShowcode);
	DDX_Control(pDX, IDC_EDIT_INCREMENT_UMSTEP, m_CtrlStepSizeum);
	DDX_Control(pDX, IDC_EDIT_INCREMENT_ANGLESTEP, m_CtrlStepSizeurad);
	DDX_Control(pDX, IDC_EDIT_Z_FOCUSPOS, m_ZFocus2Ctrl);
	DDX_Control(pDX, IDC_EDIT_Z_INTERLOCKPOS, m_z_focus);
	DDX_Control(pDX, IDC_BUTTON_X_MINUS, m_btnXMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_X_PLUS, m_btnXPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_Y_PLUS, m_btnYPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_Y_MINUS, m_btnYMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_XMINUS, m_btnPIXMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_YMINUS, m_btnPIYMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_ZMINUS, m_btnPIZMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_TXMINUS, m_btnPIRXMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_TYMINUS, m_btnPIRYMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_XPLUS, m_btnPIXPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_YPLUS, m_btnPIYPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_ZPLUS, m_btnPIZPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_TXPLUS, m_btnPIRXPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_MOVE_TYPLUS, m_btnPIRYPlusCtrl);
	DDX_Text(pDX, IDC_EDIT_STAGE_POSNAME, m_strStagePosName);
	DDX_Text(pDX, IDC_EDIT_STAGE_STATUS, m_strStageStatus);
	DDX_Text(pDX, IDC_EDIT_INCREMENT_STEPSIZE, m_strStageMovingDistance);
	DDX_Control(pDX, IDC_EDIT_INCREMENT_UMSTEPZ, m_EditStepSizeCtrl);
	DDX_Text(pDX, IDC_EDIT_X_MOVEPOS, m_strXMovePos);
	DDX_Text(pDX, IDC_EDIT_Y_MOVEPOS, m_strYMovePos);
	DDX_Control(pDX, IDC_EDIT_X_FPOS, m_EditStagePosXCtrl);
	DDX_Control(pDX, IDC_EDIT_Y_FPOS, m_EditStagePosYCtrl);
	DDX_Control(pDX, IDC_EDIT_X_PIPOS, m_EditPIStagePosXCtrl);
	DDX_Control(pDX, IDC_EDIT_Y_PIPOS, m_EditPIStagePosYCtrl);
	DDX_Control(pDX, IDC_EDIT_Z_PIPOS, m_EditPIStagePosZCtrl);
	DDX_Control(pDX, IDC_EDIT_TX_PIPOS, m_EditPIStagePosTipCtrl);
	DDX_Control(pDX, IDC_EDIT_TY_PIPOS, m_EditPIStagePosTiltCtrl);
	DDX_Text(pDX, IDC_EDIT_Z_INTERLOCKPOS, m_dZInterlockPos);
	DDX_Control(pDX, IDC_CHECK_OMADAM, m_CheckOMAdamCtrl);
	DDX_Check(pDX, IDC_CHECK_OMADAM, m_bOMDisplay);
	DDX_Text(pDX, IDC_EDIT_SCAN_REPEAT_NUMBER, m_nAutoRunRepeatSetNo);
	DDX_Control(pDX, IDC_CHECK_DEFECTCODEDISPLAY, m_bCheckCommentDisplayCtrl);
	DDX_Control(pDX, IDC_EDIT_DEFECTCOORDX, m_EditLBAlignPointCoordXCtrl);
	DDX_Control(pDX, IDC_EDIT_DEFECTCOORDY, m_EditLBAlignPointCoordYCtrl);
	DDX_Control(pDX, IDC_EDIT_STAGECOORDX, m_EditMaskCenterCoordXCtrl);
	DDX_Control(pDX, IDC_EDIT_STAGECOORDY, m_EditMaskCenterCoordYCtrl);
	DDX_Control(pDX, IDC_EDIT_ORIGINCOORDX, m_EditMaskLBCoordXCtrl);
	DDX_Control(pDX, IDC_EDIT_ORIGINCOORDY, m_EditMaskLBCoordYCtrl);
	DDX_Check(pDX, IDC_CHECK_LASERSWICHING_BUTTON, m_bLaserSwichFunction);
	DDX_Control(pDX, IDC_CHECK_LASERSWICHING_BUTTON, m_CheckLaserSwichingCtrl);
	DDX_Text(pDX, IDC_EDIT_SCAN_REPEAT_NUMBER2, m_nAutoRunRepeatResultNo);
	DDX_Text(pDX, IDC_EDIT_POINTNO, m_nMeasurementPointNo);
	DDX_Text(pDX, IDC_EDIT_MASKCODX, m_dMaskCoordinateX_mm);
	DDX_Text(pDX, IDC_EDIT_MASKCODY, m_dMaskCoordinateY_mm);
	DDX_Control(pDX, IDC_CHECK_LASER_SWITCH_ONOFF, m_CheckSwitchingOnOffCtrl);
	DDX_Text(pDX, IDC_EDIT_Z_THROUGHFOCUS_HEIGHT, m_dZThroughFocusHeight);
	DDX_Text(pDX, IDC_EDIT_Z_FOCUSPOS, m_dZFocusPos);
	DDX_Control(pDX, IDC_CHECK_BIDIRECTION_SCAN, m_CheckBidirectionCtrl);
	DDX_Control(pDX, IDC_CHECK_LASER_FREQUENCY, m_CheckLaserFrequency);
	DDX_Control(pDX, IDC_CHECK_SCAN_ONLY, m_CheckScanOnlyCtrl);
	DDX_Check(pDX, IDC_CHECK_BIDIRECTION_SCAN, m_bBiDirectionFlag);
	DDX_Control(pDX, IDC_CHECK_OMALIGNONLY, m_CheckOMAlignOnlyCtrl);
	DDX_Check(pDX, IDC_CHECK_OMALIGNONLY, m_bOMAlignOnly);
	DDX_Control(pDX, IDC_CHECK_ALIGN_AUTOMANUAL, m_CheckManualAlignCtrl);
	DDX_Check(pDX, IDC_CHECK_ALIGN_AUTOMANUAL, m_bManualAlign);
	DDX_Check(pDX, IDC_CHECK_LASER_SWITCH_ONOFF, m_bLaserFeedback);
	DDX_Control(pDX, IDC_CHECK_OM_ADAM_UI, m_CheckOMAdamUi);
	DDX_Check(pDX, IDC_CHECK_LASER_FREQUENCY, m_bLaserFrequency5Kz);
	DDX_Check(pDX, IDC_CHECK_MASKMAP_REVERSE_COORDINATE, m_bReverseCoordinate);
	DDX_Control(pDX, IDC_COMBO_SCANGRID_LITHO, m_comboScanGridAtLitho);
	DDX_Control(pDX, IDC_ICON_NAVIGATIONSTAGE_CONNECT_CHECK, m_Icon_NaviStageConnectState);
}

BEGIN_MESSAGE_MAP(CMaskMapDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_MAPFILELOAD, &CMaskMapDlg::OnBnClickedButtonRecipeFileLoad)
	ON_BN_CLICKED(IDC_BUTTON_MAPSAVE, &CMaskMapDlg::OnBnClickedButtonMapSave)
	ON_BN_CLICKED(IDC_CHECK_DEFECTCODEDISPLAY, &CMaskMapDlg::OnBnClickedCheckCodeShow)
	ON_NOTIFY(NM_CLICK, IDC_LIST_FILELIST, &CMaskMapDlg::OnNMClickListFilelist)
	ON_BN_CLICKED(IDC_BUTTON_SEARCHFILE, &CMaskMapDlg::OnBnClickedButtonRecipeFileSearch)
	ON_BN_CLICKED(IDC_BUTTON_MESUREPOINTMOVE, &CMaskMapDlg::OnBnClickedButtonMesurepointmove)
	ON_BN_CLICKED(IDC_BUTTON_MASKCOORDMOVE, &CMaskMapDlg::OnBnClickedButtonMaskcoordmove)
	ON_EN_CHANGE(IDC_EDIT_POINTNO, &CMaskMapDlg::OnEnChangeEditPointno)
	ON_EN_CHANGE(IDC_EDIT_MASKCODX, &CMaskMapDlg::OnEnChangeEditMaskcodx)
	ON_EN_CHANGE(IDC_EDIT_MASKCODY, &CMaskMapDlg::OnEnChangeEditMaskcody)
	ON_BN_CLICKED(IDC_CHECK_OMADAM, &CMaskMapDlg::OnBnClickedCheckOmadam)
	ON_BN_CLICKED(IDC_BUTTON_GET_POSITION, &CMaskMapDlg::OnBnClickedButtonGetPosition)
	ON_EN_CHANGE(IDC_EDIT_INCREMENT_STEPSIZE, &CMaskMapDlg::OnEnChangeEditIncrementStepsize)
	ON_BN_CLICKED(IDC_BUTTON_X_MINUS, &CMaskMapDlg::OnBnClickedButtonXMinus)
	ON_BN_CLICKED(IDC_BUTTON_X_PLUS, &CMaskMapDlg::OnBnClickedButtonXPlus)
	ON_BN_CLICKED(IDC_BUTTON_Y_PLUS, &CMaskMapDlg::OnBnClickedButtonYPlus)
	ON_BN_CLICKED(IDC_BUTTON_Y_MINUS, &CMaskMapDlg::OnBnClickedButtonYMinus)
	ON_BN_CLICKED(IDC_BUTTON_STAGE_ORIGIN, &CMaskMapDlg::OnBnClickedButtonStageOrigin)
	ON_BN_CLICKED(IDC_BUTTON_MOVEORIGIN, &CMaskMapDlg::OnBnClickedButtonMoveorigin)
	ON_BN_CLICKED(IDC_BUTTON_DDLINIT, &CMaskMapDlg::OnBnClickedButtonDdlinit)
	ON_BN_CLICKED(IDC_BUTTON_SCAN, &CMaskMapDlg::OnBnClickedButtonScan)
	ON_EN_CHANGE(IDC_EDIT_INCREMENT_UMSTEP, &CMaskMapDlg::OnEnChangeEditIncrementUmstep)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_XMINUS, &CMaskMapDlg::OnBnClickedButtonMoveXminus)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_XPLUS, &CMaskMapDlg::OnBnClickedButtonMoveXplus)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_YPLUS, &CMaskMapDlg::OnBnClickedButtonMoveYplus)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_YMINUS, &CMaskMapDlg::OnBnClickedButtonMoveYminus)
	ON_BN_CLICKED(IDC_BUTTON_XY_ORIGIN, &CMaskMapDlg::OnBnClickedButtonXyOrigin)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_ZPLUS, &CMaskMapDlg::OnBnClickedButtonMoveZplus)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_ZMINUS, &CMaskMapDlg::OnBnClickedButtonMoveZminus)
	ON_BN_CLICKED(IDC_BUTTON_Z_ORIGIN, &CMaskMapDlg::OnBnClickedButtonZOrigin)
	ON_EN_CHANGE(IDC_EDIT_INCREMENT_ANGLESTEP, &CMaskMapDlg::OnEnChangeEditIncrementAnglestep)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_TXMINUS, &CMaskMapDlg::OnBnClickedButtonMoveTxminus)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_TXPLUS, &CMaskMapDlg::OnBnClickedButtonMoveTxplus)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_TYPLUS, &CMaskMapDlg::OnBnClickedButtonMoveTyplus)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_TYMINUS, &CMaskMapDlg::OnBnClickedButtonMoveTyminus)
	ON_BN_CLICKED(IDC_BUTTON_TXTY_ORIGIN, &CMaskMapDlg::OnBnClickedButtonTxtyOrigin)
	ON_EN_CHANGE(IDC_EDIT_Z_FOCUSPOS, &CMaskMapDlg::OnEnChangeEditZFocuspos)
	ON_WM_UNICHAR()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_TEXT_STAGEPOSGRID, OnStagePosGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_TEXT_STAGEPOSGRID, OnStagePosGridDblClick)
	ON_NOTIFY(NM_RCLICK, IDC_TEXT_STAGEPOSGRID, OnStagePosGridRClick)
	ON_NOTIFY(GVN_BEGINLABELEDIT, IDC_TEXT_STAGEPOSGRID, OnStagePosGridStartEdit)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_TEXT_STAGEPOSGRID, OnStagePosGridEndEdit)
	ON_NOTIFY(UDN_DELTAPOS, IDC_INCREMENT_STEPSPIN, &CMaskMapDlg::OnDeltaposIncrementStepspin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_INCREMENT_UMSTEPSPIN, &CMaskMapDlg::OnDeltaposIncrementUmstepspin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_INCREMENT_URADSTEPSPIN, &CMaskMapDlg::OnDeltaposIncrementUradstepspin)
	ON_BN_CLICKED(IDC_BUTTON_RECORD, &CMaskMapDlg::OnBnClickedButtonRecord)
	ON_BN_CLICKED(IDC_BUTTON_MASKLOAD, &CMaskMapDlg::OnBnClickedButtonMaskload)
	ON_BN_CLICKED(IDC_BUTTON_MASKALIGN, &CMaskMapDlg::OnBnClickedButtonMaskalign)
	ON_BN_CLICKED(IDC_BUTTON_MASKMEASURE_START, &CMaskMapDlg::OnBnClickedButtonMaskmeasureStart)
	ON_BN_CLICKED(IDC_BUTTON_MASKUNLOAD, &CMaskMapDlg::OnBnClickedButtonMaskunload)
	ON_EN_CHANGE(IDC_EDIT_SCAN_REPEAT_NUMBER, &CMaskMapDlg::OnEnChangeEditScanRepeatNumber)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_POSITION, &CMaskMapDlg::OnBnClickedButtonMovePosition)
	ON_NOTIFY(UDN_DELTAPOS, IDC_INCREMENT_UMSTEPSPINZ, &CMaskMapDlg::OnDeltaposIncrementUmstepspinz)
	ON_BN_CLICKED(IDC_BUTTON_XY_MOVE, &CMaskMapDlg::OnBnClickedButtonXyMove)
	ON_EN_CHANGE(IDC_EDIT_X_MOVEPOS, &CMaskMapDlg::OnEnChangeEditXMovepos)
	ON_EN_CHANGE(IDC_EDIT_Y_MOVEPOS, &CMaskMapDlg::OnEnChangeEditYMovepos)
	ON_BN_CLICKED(IDC_BUTTON_ZOOMRESET, &CMaskMapDlg::OnBnClickedButtonZoomreset)
	ON_EN_CHANGE(IDC_EDIT_Z_INTERLOCKPOS, &CMaskMapDlg::OnEnChangeEditZInterlockpos)
	ON_BN_CLICKED(IDC_BUTTON_MASKMEASURE_AUTOSTART, &CMaskMapDlg::OnBnClickedButtonMaskmeasureAutostart)
	ON_BN_CLICKED(IDC_BUTTON_STOP_AUTOSEQUENCE, &CMaskMapDlg::OnBnClickedButtonStopAutosequence)
	ON_BN_CLICKED(IDC_CHECK_MAGNIFICATION, &CMaskMapDlg::OnBnClickedCheckMagnification)
	ON_BN_CLICKED(IDC_CHECK_LASERSWICHING_BUTTON, &CMaskMapDlg::OnBnClickedCheckLaserswichingButton)
	ON_BN_CLICKED(IDC_CHECK_LASER_SWITCH_ONOFF, &CMaskMapDlg::OnBnClickedCheckLaserSwitchOnoff)
	ON_EN_CHANGE(IDC_EDIT_Z_THROUGHFOCUS_HEIGHT, &CMaskMapDlg::OnEnChangeEditZThroughfocusHeight)
	ON_BN_CLICKED(IDC_CHECK_BIDIRECTION_SCAN, &CMaskMapDlg::OnBnClickedCheckBidirectionScan)
	ON_BN_CLICKED(IDC_CHECK_SCAN_ONLY, &CMaskMapDlg::OnBnClickedCheckScanOnly)
	ON_BN_CLICKED(IDC_CHECK_OMALIGNONLY, &CMaskMapDlg::OnBnClickedCheckOmalignonly)
	ON_BN_CLICKED(IDC_CHECK_ALIGN_AUTOMANUAL, &CMaskMapDlg::OnBnClickedCheckAlignAutomanual)
	ON_BN_CLICKED(IDC_CHECK_OM_ADAM_UI, &CMaskMapDlg::OnBnClickedCheckOmAdamUi)
	ON_BN_CLICKED(IDC_CHECK_LASER_FREQUENCY, &CMaskMapDlg::OnBnClickedCheckLaserFrequency)
	ON_BN_CLICKED(IDC_BUTTON_MASKMEASURE_MACRO_START, &CMaskMapDlg::OnBnClickedButtonMaskmeasureMacroStart)
	ON_BN_CLICKED(IDC_CHECK_MASKMAP_REVERSE_COORDINATE, &CMaskMapDlg::OnBnClickedCheckMaskmapReverseCoordinate)
	ON_CBN_CLOSEUP(IDC_COMBO_SCANGRID_LITHO, &CMaskMapDlg::OnCloseupComboScangridLitho)
	ON_BN_CLICKED(IDC_BUTTON_NAVIGATIONSTAGE_CONNECT, &CMaskMapDlg::OnBnClickedButtonNavigationstageConnect)
END_MESSAGE_MAP()


// CMaskMapDlg 메시지 처리기

BOOL CMaskMapDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);

	m_Icon_NaviStageConnectState.SetIcon((m_LedIcon[0]));

	// List initialization
	m_ListCtrlRecipeFile.InsertColumn(0, "File Name", LVCFMT_LEFT, 330, 0);
	m_ListCtrlRecipeFile.InsertColumn(1, "Size", LVCFMT_LEFT, 120, 3);
	//m_ListCtrlRecipeFile.InsertColumn(0, "File Name", LVCFMT_LEFT, 180, 0);
	//m_ListCtrlRecipeFile.InsertColumn(1, "Date", LVCFMT_LEFT, 140, 1);
	//m_ListCtrlRecipeFile.InsertColumn(2, "Size", LVCFMT_LEFT, 80, 3);

	// Set callback function
	m_filefinder.SetCallback(FileSearching, this);

	RecipeHeaderDisplay();

	CRect rect;
	CWnd *pWnd = GetDlgItem(IDC_PICTURE_MAP);
	pWnd->GetClientRect(rect);
	m_MaskMapWnd.Create(WS_CHILD | WS_VISIBLE /*| WS_VSCROLL | WS_HSCROLL*/, rect, pWnd);
	m_MaskMapWnd.ShowWindow(SW_SHOW);
	m_nMapWidthPixelNo = rect.Width();
	m_nMapHeightPixelNo = m_nMapWidthPixelNo;

	//CString str;
	//str.Format(_T("%2.1f"), g_pScanStage->m_dPIStage_MovePos[X_AXIS]);
	m_CtrlStepSizeum.SetWindowText("1.000");
	//str.Format(_T("%2.1f"), g_pScanStage->m_dPIStage_MovePos[TIP_AXIS]);
	m_CtrlStepSizeurad.SetWindowText("10");
	//str.Format(_T("%3.1f"), 10.0);
	m_EditStepSizeCtrl.SetWindowText("10");

	InitButtonSkin();
	InitStagePosGrid();

	m_strStagePosName = g_pConfig->m_stStagePos[0].chStagePositionString;
	m_strXMovePos.Format(_T("%3.3f"), g_pConfig->m_stStagePos[0].x);
	m_strYMovePos.Format(_T("%3.3f"), g_pConfig->m_stStagePos[0].y);


	if (g_pConfig != NULL)
	{
		g_pConfig->ReadRecoveryData();
		m_dOMAlignPointLB_posx_mm = g_pConfig->m_dOMAlignPointLB_X_mm;
		m_dOMAlignPointLB_posy_mm = g_pConfig->m_dOMAlignPointLB_Y_mm;
		m_dOMAlignPointLT_posx_mm = g_pConfig->m_dOMAlignPointLT_X_mm;
		m_dOMAlignPointLT_posy_mm = g_pConfig->m_dOMAlignPointLT_Y_mm;
		m_dOMAlignPointRT_posx_mm = g_pConfig->m_dOMAlignPointRT_X_mm;
		m_dOMAlignPointRT_posy_mm = g_pConfig->m_dOMAlignPointRT_Y_mm;
		m_dEUVAlignPointLB_posx_mm = g_pConfig->m_dEUVAlignPointLB_X_mm;
		m_dEUVAlignPointLB_posy_mm = g_pConfig->m_dEUVAlignPointLB_Y_mm;
		m_dEUVAlignPointLT_posx_mm = g_pConfig->m_dEUVAlignPointLT_X_mm;
		m_dEUVAlignPointLT_posy_mm = g_pConfig->m_dEUVAlignPointLT_Y_mm;
		m_dEUVAlignPointRT_posx_mm = g_pConfig->m_dEUVAlignPointRT_X_mm;
		m_dEUVAlignPointRT_posy_mm = g_pConfig->m_dEUVAlignPointRT_Y_mm;
		m_dNotchAlignPoint1_posx_mm = g_pConfig->m_dNotchAlignPoint1_X_mm;
		m_dNotchAlignPoint1_posy_mm = g_pConfig->m_dNotchAlignPoint1_Y_mm;
		m_dNotchAlignPoint2_posx_mm = g_pConfig->m_dNotchAlignPoint2_X_mm;
		m_dNotchAlignPoint2_posy_mm = g_pConfig->m_dNotchAlignPoint2_X_mm;
		m_bOMAlignComplete = g_pConfig->m_bOMAlignCompleteFlag;
		m_bEUVAlignComplete = g_pConfig->m_bEUVAlignCompleteFlag;
		//m_bLaserSwichFunction = g_pConfig->m_bLaserFeedbackAvailable_Flag;
		m_bLaserFeedback = g_pConfig->m_bLaserFeedback_Flag;
		m_dZInterlockPos = g_pConfig->m_dZInterlock_um;
		g_pScanStage->m_dZUpperLimit = g_pConfig->m_dZInterlock_um;
		m_dZFocusPos = g_pConfig->m_dZFocusPos_um;
	}

	UpdateData(FALSE);
	m_MaskMapWnd.Invalidate(FALSE);

	m_comboScanGridAtLitho.AddString(_T("10 nm"));
	m_comboScanGridAtLitho.AddString(_T("20 nm"));
	m_comboScanGridAtLitho.AddString(_T("30 nm"));
	m_comboScanGridAtLitho.AddString(_T("40 nm"));
	m_comboScanGridAtLitho.AddString(_T("50 nm"));
	m_comboScanGridAtLitho.AddString(_T("80 nm"));
	m_comboScanGridAtLitho.AddString(_T("100 nm"));
	m_comboScanGridAtLitho.AddString(_T("150 nm"));
	m_comboScanGridAtLitho.AddString(_T("200 nm"));
	m_comboScanGridAtLitho.SetCurSel(6);
	if (g_pConfig->m_nEquipmentType != ELITHO)
		m_comboScanGridAtLitho.EnableWindow(FALSE);


	m_CheckOMAdamUi.SetCheck(TRUE);

	switch (g_pConfig->m_nEquipmentType)
	{
	case SREM033:
		if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Adam"));
		break;
	case PHASE:
		g_pXrayCamera->ShowWindow(SW_HIDE);
		g_pPhase->ShowWindow(SW_HIDE);
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Phase"));
		break;
	case EUVPTR:
		g_pPTR->ShowWindow(SW_HIDE);
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("PTR"));
		break;
	case ELITHO:
		//g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Litho"));
		if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
		break;
	default:
		break;
	}

	m_CheckLaserFrequency.SetCheck(m_bLaserFrequency5Kz); //UI 연동

	switch (m_CheckLaserFrequency.GetCheck())
	{
	case BST_CHECKED:
		m_CheckLaserFrequency.SetWindowText(_T("5 Khz"));
		break;
	case BST_UNCHECKED:
		m_CheckLaserFrequency.SetWindowText(_T("1 Khz"));
		break;
	default:
		break;
	}

	m_AdamOmtiltSwitch = 0;


	switch (m_CheckBidirectionCtrl.GetCheck())
	{
	case BST_CHECKED:
		m_CheckBidirectionCtrl.SetWindowText(_T("BI-DIRECTION SCAN"));
		g_pAdam->m_nScanType = BI_DIRECTION;
		break;
	case BST_UNCHECKED:
		m_CheckBidirectionCtrl.SetWindowText(_T("UNI-DIRECTION SCAN"));
		g_pAdam->m_nScanType = UNI_DIRECTION;
		break;
	default:
		break;
	}

	GetDlgItem(IDC_BUTTON_MASKMEASURE_MACRO_START)->EnableWindow(TRUE);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}
void CMaskMapDlg::InitButtonSkin()
{
	m_btnXMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2LT));
	m_btnYMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2DN));
	m_btnXPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2RT));
	m_btnYPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2UP));
	//m_btnXMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARWLT));
	//m_btnYMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARWDN));
	//m_btnXPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARWRT));
	//m_btnYPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARWUP));

	m_btnPIXMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2LT));
	m_btnPIYMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2DN));
	m_btnPIXPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2RT));
	m_btnPIYPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2UP));
	m_btnPIZMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2DN));
	m_btnPIZPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2UP));
	m_btnPIRXMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2LT));
	m_btnPIRYMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2DN));
	m_btnPIRXPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2RT));
	m_btnPIRYPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2UP));
	//m_btnPIRXMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2LT));
	//m_btnPIRYMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2DN));
	//m_btnPIRXPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2RT));
	//m_btnPIRYPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2UP));

}

void CMaskMapDlg::InitStagePosGrid()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_STAGEPOSGRID)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_StagePosGrid.Create(rect, this, IDC_TEXT_STAGEPOSGRID);

	int nGap, col, row;
	col = 0;
	row = 0;
	nGap = 8;

	m_StagePosGrid.SetEditable(false);
	m_StagePosGrid.SetListMode(true);
	m_StagePosGrid.SetSingleRowSelection(false);
	m_StagePosGrid.EnableDragAndDrop(false);
	m_StagePosGrid.SetFixedRowCount(1);
	m_StagePosGrid.SetFixedColumnCount(1);
	m_StagePosGrid.SetColumnCount(4);
	m_StagePosGrid.SetRowCount(MAX_STAGE_POSITION + 1);
	//m_StagePosGrid.SetBkColor(RGB(192, 192, 192));
	m_StagePosGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_StagePosGrid.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	//lf.lfHeight=10;
	//strcpy(lf.lfFaceName,"Verdana");
	Font.CreateFontIndirect(&lf);
	m_StagePosGrid.SetFont(&Font);
	Font.DeleteObject();

	CString str;
	str.Format("%s", "No");
	m_StagePosGrid.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Stage Position Name");
	m_StagePosGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "X(mm)");
	m_StagePosGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Y(mm)");
	m_StagePosGrid.SetItemText(0, 3, (LPSTR)(LPCTSTR)str);

	m_StagePosGrid.SetRowHeight(0, 35);
	m_StagePosGrid.SetColumnWidth(0, 10);
	m_StagePosGrid.SetColumnWidth(1, 400);
	m_StagePosGrid.SetColumnWidth(2, 70);
	m_StagePosGrid.SetColumnWidth(3, 70);
	m_StagePosGrid.ExpandColumnsToFit();

	CString strNo, strX, strY;
	for (row = 1; row < MAX_STAGE_POSITION + 1; row++)
	{
		strNo.Format("%d", row - 1);
		m_StagePosGrid.SetItemText(row, 0, strNo);
		m_StagePosGrid.SetItemText(row, 1, g_pConfig->m_stStagePos[row - 1].chStagePositionString);
		strX.Format("%4.6f", g_pConfig->m_stStagePos[row - 1].x);
		m_StagePosGrid.SetItemText(row, 2, strX);
		strY.Format("%4.6f", g_pConfig->m_stStagePos[row - 1].y);
		m_StagePosGrid.SetItemText(row, 3, strY);
	}
	//m_StagePosGrid.SetSelectedRow(1);
}

void CMaskMapDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMaskMapDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMaskMapDlg::RecipeHeaderDisplay()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_INFORMATION)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	m_reCtrl.Create(ES_READONLY | ES_LEFT | ES_MULTILINE, rect, this, 1);
	CHARFORMAT charformat;
	m_reCtrl.GetDefaultCharFormat(charformat);
	if (charformat.dwEffects & CFE_AUTOCOLOR) charformat.dwEffects -= CFE_AUTOCOLOR;
	charformat.crTextColor = WHITE;
	m_reCtrl.SetDefaultCharFormat(charformat);
	m_reCtrl.SetBackgroundColor(false, DARK_GRAY);
	m_reCtrl.ShowWindow(SW_SHOW);
}

void CMaskMapDlg::OnBnClickedButtonRecipeFileLoad()
{
	GetDlgItem(IDC_BUTTON_MASKMEASURE_MACRO_START)->EnableWindow(TRUE);
	LoadProcessData();	
}

int CMaskMapDlg::LoadProcessData()
{
	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	GetDlgItem(IDC_BUTTON_MASKMEASURE_MACRO_START)->EnableWindow(TRUE);

	if (m_MaskMapWnd.m_ProcessData.curFilePath.ExistFile() == FALSE)
	{
		return FALSE;
	}

	CString	buf, loadname;
	buf = m_MaskMapWnd.m_ProcessData.curFilePath.GetExtName();
	buf.MakeLower();
	loadname = m_MaskMapWnd.m_ProcessData.curFilePath.GetLongPath();
	int ret = 0;

	m_MaskMapWnd.m_bDrawMeasurePoint = FALSE;
	m_MaskMapWnd.m_bDrawSelectedIDRect = FALSE;
	m_MaskMapWnd.m_ProcessData.ResetData();
	ret = m_MaskMapWnd.m_ProcessData.LoadHeaderInfo((char*)(LPCTSTR)loadname);
	if (ret == TRUE)
	{
		if (g_pConfig->m_nEquipmentType == SREM033 && m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_SREM_033")
		{
			ret &= m_MaskMapWnd.m_ProcessData.LoadMeasureData((char*)(LPCTSTR)loadname);
		}
		else if (g_pConfig->m_nEquipmentType == PHASE && m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_EPHASE_033")
		{
			ret &= m_MaskMapWnd.m_ProcessData.LoadPhaseMeasureData((char*)(LPCTSTR)loadname);
		}
		else if (g_pConfig->m_nEquipmentType == EUVPTR && m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_EUVPTR")
		{
			ret &= m_MaskMapWnd.m_ProcessData.LoadPTRMeasureData((char*)(LPCTSTR)loadname);
		}
		else if (g_pConfig->m_nEquipmentType == SREM033 && m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_MACRO")
		{
			/* MACRO SYSTEM (kjh) */
			ret &= m_MaskMapWnd.m_ProcessData.LoadMACROMeasureData((char*)(LPCTSTR)loadname);
			GetDlgItem(IDC_BUTTON_MASKMEASURE_MACRO_START)->EnableWindow(true);
		}
		else if (g_pConfig->m_nEquipmentType == ELITHO && m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_LITHO")
		{
			ret &= m_MaskMapWnd.m_ProcessData.LoadLITHOMeasureData((char*)(LPCTSTR)loadname);
		}
		else
		{
			MsgBoxAuto.DoModal(_T(" 본 설비와 호환되지 않는 Recipe 입니다 ! "), 2);
			return FALSE;
		}

		if (ret == TRUE)
		{
			m_MaskMapWnd.m_bShowcode = FALSE;
			m_bCheckCommentDisplayCtrl.SetCheck(TRUE);
			//m_bShowcode = m_MaskMapWnd.m_bShowcode;
			//UpdateData(TRUE);
			m_MaskMapWnd.m_bDrawMeasurePoint = TRUE;
			m_MaskMapWnd.m_bDrawSelectedIDRect = TRUE;
			m_MaskMapWnd.Invalidate(FALSE);
		}
		else
			return FALSE;
	}

	return TRUE;
}

void CMaskMapDlg::FileSearchInFolder(CString strSearchFile, CString strBaseFolder)
{
	if (strSearchFile.IsEmpty())
	{
		strSearchFile = _T("*.*");
	}

	if (bFinding)
	{
		m_filefinder.StopSearch();
		return;
	}

	m_ListCtrlRecipeFile.DeleteAllItems();

	SYSTEMTIME buftime;
	m_DateTimeStartCtrl.GetTime(&buftime);
	buftime.wHour = 0;
	buftime.wMinute = 0;
	buftime.wSecond = 0;
	m_DateTimeStart = buftime;

	m_DateTimeEndCtrl.GetTime(&buftime);
	buftime.wHour = 23;
	buftime.wMinute = 59;
	buftime.wSecond = 59;
	m_DateTimeEnd = buftime;

	BOOL ret = SetCurrentDirectory(strBaseFolder);

	CFileFinder::CFindOpts	opts;

	// Set CFindOpts object
	opts.strBaseFolder = strBaseFolder;
	opts.sFileMask.Format("*%s*", strSearchFile);
	opts.bSubfolders = 0;
	opts.FindNormalFiles();
	opts.tMinModified = m_DateTimeStart;
	opts.tMaxModified = m_DateTimeEnd;
	opts.dwOptionsFlags |= FIND_DATEMODIFIED;

	if (ret > 0)
	{
		BeginWaitCursor();
		bFinding = true;
		m_filefinder.RemoveAll();
		m_filefinder.Find(opts);
		bFinding = false;
		SetStatus(m_filefinder.GetFileCount());
		EndWaitCursor();
	}
}

void CMaskMapDlg::FileSearching(CFileFinder *pfilefinder, DWORD dwCode, void *pCustomParam)
{
	CString			sText, sNewFile;
	CMaskMapDlg	*pDlg = (CMaskMapDlg *)pCustomParam;
	int				nListIndex;

	switch (dwCode)
	{
	case FF_FOUND:
		// Update list		
		sNewFile = pfilefinder->GetFilePath(pfilefinder->GetFileCount() - 1);
		nListIndex = pDlg->FindInList(sNewFile);
		if (nListIndex == -1) pDlg->AddFileToList(sNewFile);
		break;
	case FF_FOLDER:
		pDlg->SetStatus(pfilefinder->GetFileCount(), pfilefinder->GetSearchingFolder());
		break;
	default:
		break;
	}
}

int	CMaskMapDlg::FindInList(LPCTSTR szFilename)
{
	int		nIndex = 0;
	bool	bFound = false;

	RestoreWaitCursor();

	int count = m_ListCtrlRecipeFile.GetItemCount();
	for (nIndex = 0; nIndex < m_ListCtrlRecipeFile.GetItemCount(); nIndex++)
	{
		bFound = (GetListFilename(nIndex).CompareNoCase(szFilename) == 0);
		if (bFound) break;
	}

	return (bFound ? nIndex : -1);
}

void CMaskMapDlg::AddFileToList(LPCTSTR szFilename)
{
	int		nIndex;
	CPath	path(szFilename);
	__int64	nSize64;
	long	nSize;
	CString	sText;
	CTime	tModified;

	// File name
	if (path.IsFilePath() == TRUE)
	{
		nIndex = m_ListCtrlRecipeFile.InsertItem(m_ListCtrlRecipeFile.GetItemCount(), path.GetFileName(), 0);
	}
	else
	{
		nIndex = m_ListCtrlRecipeFile.InsertItem(m_ListCtrlRecipeFile.GetItemCount(), path.GetRelativePath(_T("C:\\EUVSolution\\Recipe")), 0);
	}

	// File modified date
	path.GetFileTime(tModified);
	//m_ListCtrlRecipeFile.SetItemText(nIndex, 1, tModified.FormatGmt("%y:%m:%d-%I:%M %p"));

	// File size
	path.GetFileSize(nSize64);
	nSize = (long)(nSize64 / (__int64)1024);
	if (nSize < 10)
		sText.Format("%ld Byte", nSize64);
	else
		sText.Format("%ld KB", nSize);

	m_ListCtrlRecipeFile.SetItemText(nIndex, 1, sText);

}

void CMaskMapDlg::SetStatus(int nCount, LPCTSTR szFolder)
{
	CString sStatus;

	if (szFolder != NULL)
		sStatus.Format("(%d) - %s", nCount, szFolder);
	else
		sStatus.Format("%d ea Recipe Files", nCount);

	//m_TxtSearchResultCtrl.SetWindowText(sStatus);
}

CString	CMaskMapDlg::GetListFilename(int nIndex)
{
	return m_ListCtrlRecipeFile.GetItemText(nIndex, 1) + m_ListCtrlRecipeFile.GetItemText(nIndex, 0);
}

CPath CMaskMapDlg::GetCurSelListCtrl()
{
	int nSelected = -1;
	// Get the selected items in the control
	POSITION p = m_ListCtrlRecipeFile.GetFirstSelectedItemPosition();
	while (p)
	{
		nSelected = m_ListCtrlRecipeFile.GetNextSelectedItem(p);
		// Do something with item nSelected
	}
	TCHAR szBuffer[1024];
	DWORD cchBuf(1024);
	LVITEM lvi;
	if (nSelected >= 0)
	{
		lvi.iItem = nSelected;
		lvi.iSubItem = 0;
		lvi.mask = LVIF_TEXT;
		lvi.pszText = szBuffer;
		lvi.cchTextMax = cchBuf;
		m_ListCtrlRecipeFile.GetItem(&lvi);
		return lvi.pszText;
	}
	return "";
}

void CMaskMapDlg::OnBnClickedButtonMapSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMaskMapDlg::OnBnClickedCheckCodeShow()
{
	UpdateData(TRUE);
	m_MaskMapWnd.m_bShowcode = m_bShowcode;
	m_MaskMapWnd.Invalidate(FALSE);
}

void CMaskMapDlg::OnBnClickedButtonRecipeFileSearch()
{
	FileSearchInFolder("*.rcp", _T("C:\\EUVSolution\\Recipe"));
}

void CMaskMapDlg::OnNMClickListFilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);



	int col, row;
	row = pNMItemActivate->iItem;
	col = pNMItemActivate->iSubItem;
	if (row == -1 || col != 0)
		return;

	int ret = 0;
	m_MaskMapWnd.m_ProcessData.curFilePath = GetCurSelListCtrl();
	if (m_MaskMapWnd.m_ProcessData.curFilePath.ExistFile() == FALSE)
	{
		FileSearchInFolder("", m_MaskMapWnd.m_ProcessData.curFilePath.GetLongPath());
		return;
	}

	m_MaskMapWnd.m_ProcessData.orgFilePath = m_MaskMapWnd.m_ProcessData.curFilePath;

	CString str = m_MaskMapWnd.m_ProcessData.curFilePath.GetLongPath();

	CString strExt = m_MaskMapWnd.m_ProcessData.curFilePath.GetExtension();
	strExt.MakeUpper();
	m_MaskMapWnd.m_bDrawAlignPoint = FALSE;
	m_MaskMapWnd.m_bDrawMeasurePoint = FALSE;
	m_MaskMapWnd.m_bDrawSelectedIDRect = FALSE;
	m_MaskMapWnd.m_ProcessData.ResetData();

	CTime	FileGenTime;
	m_MaskMapWnd.m_ProcessData.curFilePath.GetFileTime(FileGenTime);
	m_MaskMapWnd.m_ProcessData.RecipeGenDateTime = FileGenTime.FormatGmt("%y:%m:%d-%I:%M %p");

	char fpath[256];
	strcpy(fpath, (char*)(LPCTSTR)(m_MaskMapWnd.m_ProcessData.curFilePath.GetLongPath()));
	ret = OpenFile(fpath);
	if (ret == TRUE)
	{
		if (m_MaskMapWnd.m_MilMapDisplay != M_NULL)
		{
			MbufFree(m_MaskMapWnd.m_MilMapDisplay);
			m_MaskMapWnd.m_MilMapDisplay = M_NULL;
		}
		MbufAllocColor(g_milSystemHost, 3, 1024, 1024, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB, &m_MaskMapWnd.m_MilMapDisplay);
		if (!m_MaskMapWnd.m_ProcessData.m_strSubstrateMap.IsEmpty())
		{
			//sprintf(fpath, "%s\\%s", RECIPE_MAP_IMAGE_PATH, m_MaskMapWnd.m_ProcessData.m_strSubstrateMap);
			//MbufLoad(fpath, m_MaskMapWnd.m_MilMapDisplay);
			CString str;
			str.Format(_T("%s\\%s"), RECIPE_MAP_IMAGE_PATH, m_MaskMapWnd.m_ProcessData.m_strSubstrateMap);
			if (IsFileExist(str))
				MbufLoad((char*)(LPCTSTR)str, m_MaskMapWnd.m_MilMapDisplay);
		}
		m_MaskMapWnd.m_bDrawAlignPoint = TRUE;
		m_MaskMapWnd.Invalidate(FALSE);
	}

	*pResult = 0;
}

BOOL CMaskMapDlg::OpenFile(char *fpath)
{
	CString recipe_header_contents;
	CString str, temp, commnd;
	int ret = 0;

	BeginWaitCursor();
	ret = m_MaskMapWnd.m_ProcessData.LoadHeaderInfo(fpath);
	EndWaitCursor();

	m_reCtrl.Clear();
	recipe_header_contents.Empty();
	recipe_header_contents = "=> Recipe Generation Date :  " + m_MaskMapWnd.m_ProcessData.RecipeGenDateTime + "\n";
	recipe_header_contents += "=> Equipment :  " + m_MaskMapWnd.m_ProcessData.EquipmentModelName + "\n";

	/* MACRO SYSTEM (kjh) */
	if (m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_MACRO")
	{
		recipe_header_contents += "=> Substrate :  " + m_MaskMapWnd.m_ProcessData.Substrate + "\n";
		recipe_header_contents += "=> Substrate ID :  " + m_MaskMapWnd.m_ProcessData.SubstrateID + "\n";
		recipe_header_contents += "=> Alignment Points No :  " + str + " ea \n";
		str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.X, m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.Y);
		recipe_header_contents += "       1  :  " + str + "\n";
		str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.X, m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.Y);
		recipe_header_contents += "       2  :  " + str + "\n";
		str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.X, m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.Y);
		recipe_header_contents += "       3  :  " + str + "\n";
		//recipe_header_contents += "\n";
		ret &= m_MaskMapWnd.m_ProcessData.LoadMACROCOmmandData((char*)(LPCTSTR)fpath);
		for (int i = 0; i < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; i++)
		{
			CString cnt;
			cnt.Format("%d", i + 1);

			commnd = m_MaskMapWnd.m_ProcessData.CommandList[i].Command;
			recipe_header_contents += "=> " + cnt + "_Command ID : " + commnd + "\n";
		}
		//recipe_header_contents += "\n";

	}
	else
	{
		recipe_header_contents += "=> Substrate :  " + m_MaskMapWnd.m_ProcessData.Substrate + "\n";
		recipe_header_contents += "=> Substrate ID :  " + m_MaskMapWnd.m_ProcessData.SubstrateID + "\n";
		str.Format("%d %d", m_MaskMapWnd.m_ProcessData.SubstrateSize, m_MaskMapWnd.m_ProcessData.SubstrateSize);
		recipe_header_contents += "=> Substrate Size(um) :  " + str + "\n";
		recipe_header_contents += "=> Lot :  " + m_MaskMapWnd.m_ProcessData.Lot + "\n";
		recipe_header_contents += "=> Step :  " + m_MaskMapWnd.m_ProcessData.Step + "\n";
		recipe_header_contents += "=> Device :  " + m_MaskMapWnd.m_ProcessData.Device + "\n";
		str.Format("%d", m_MaskMapWnd.m_ProcessData.nAlignmentPointTotal);
		recipe_header_contents += "=> Alignment Points No :  " + str + " ea \n";
		str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.X, m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.Y);
		recipe_header_contents += "       1  :  " + str + "\n";
		str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.X, m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.Y);
		recipe_header_contents += "       2  :  " + str + "\n";
		str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.X, m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.Y);
		recipe_header_contents += "       3  :  " + str + "\n";
	}

	if (m_MaskMapWnd.m_ProcessData.nAlignmentPointTotal == 4)
	{
		str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stAlignmentRB_um.X, m_MaskMapWnd.m_ProcessData.m_stAlignmentRB_um.Y);
		recipe_header_contents += "       3  :  " + str + "\n";
	}

	if (m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_SREM_033")	//Phase 설비에서만 Display
	{
	}
	else if (m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_EPHASE_033")	//Phase 설비에서만 Display
	{
		str.Format("%.3f   %.3f", m_MaskMapWnd.m_ProcessData.m_stReferenceFrequencyPos_um.X, m_MaskMapWnd.m_ProcessData.m_stReferenceFrequencyPos_um.Y);
		recipe_header_contents += "=> Reference Frequency Pos(um) :  " + str + "\n";
		str.Format("%.3f   %.3f", m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y);
		recipe_header_contents += "=> Beam Diagnosis Pos(um) :  " + str + "\n";
	}
	else if (m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_EUVPTR")
	{
		str.Format("%.3f   %.3f", m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y);
		recipe_header_contents += "=> Beam Diagnosis Pos(um) :  " + str + "\n";
		str.Format("%d", m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms);
		recipe_header_contents += "=> Beam Diagnosis Exposure Time(msec) :  " + str + "\n";
	}
	else if (m_MaskMapWnd.m_ProcessData.EquipmentModelName == " E-SOL_LITHO")
	{
		str.Format("%.3f   %.3f", m_MaskMapWnd.m_ProcessData.m_stNotchAlignPos_um[0].X, m_MaskMapWnd.m_ProcessData.m_stNotchAlignPos_um[0].Y);
		recipe_header_contents += "=> Notch1Coordinate Pos(um) :  " + str + "\n";
		str.Format("%.3f   %.3f", m_MaskMapWnd.m_ProcessData.m_stNotchAlignPos_um[1].X, m_MaskMapWnd.m_ProcessData.m_stNotchAlignPos_um[1].Y);
		recipe_header_contents += "=> Notch2Coordinate Pos(um) :  " + str + "\n";
	}
	str.Format("%d", m_MaskMapWnd.m_ProcessData.TotalMeasureNum);
	temp += "=> Total Measurement No :  " + str + " ea \n";
	recipe_header_contents += temp;



	m_reCtrl.SetWindowText(recipe_header_contents);
	m_reCtrl.ShowWindow(SW_SHOW);

	return TRUE;
}

BOOL CMaskMapDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		case VK_LBUTTON:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CMaskMapDlg::OnBnClickedCheckOmadam()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckOmadam() Check Button Click!"));
	UpdateData(TRUE);

	g_pScanStageTest->ShowWindow(SW_HIDE);
	g_pChart->ShowWindow(SW_HIDE);

	switch (m_CheckOMAdamCtrl.GetCheck())
	{
	case BST_CHECKED:
		ChangeOMEUVSet(OMTOEUV);
		break;
	case BST_UNCHECKED:
		ChangeOMEUVSet(EUVTOOM);
		break;
	default:
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////// Stage Moving /////////////////////////////////////////////////////////////////////////////////////////////////
void CMaskMapDlg::OnEnChangeEditPointno()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMaskMapDlg::OnBnClickedButtonMesurepointmove()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMesurepointmove() Check Button Click!"));
	UpdateData(TRUE);
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_nMeasurementPointNo < 1)
	{
		MsgBoxAuto.DoModal(_T(" 측정 불가능한 번호입니다. 1번부터 진행 가능 ! "), 2);
		return;
	}

	if (m_bAutoSequenceProcessing == FALSE)
		g_pNavigationStage->MoveToSelectedPointNo(m_nMeasurementPointNo - 1);

}

void CMaskMapDlg::OnEnChangeEditMaskcodx()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::OnEnChangeEditMaskcody()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMaskMapDlg::OnBnClickedButtonMaskcoordmove()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskcoordmove() Check Button Click!"));
	UpdateData(TRUE);
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bAutoSequenceProcessing == FALSE)
		g_pNavigationStage->MoveToMaskCenterCoordinate(m_dMaskCoordinateX_mm * 1000. + g_pConfig->m_dInspectorOffsetX_um, m_dMaskCoordinateY_mm * 1000. + g_pConfig->m_dInspectorOffsetY_um);
}


void CMaskMapDlg::OnBnClickedButtonGetPosition()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonGetPosition() Button Click!"));
	UpdateData(true);

	CString strX, strY;
	int row = 0;
	row = m_StagePosGrid.GetSelectedRow();

	if (row > MAX_STAGE_POSITION - 20)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "srem")
		{
			::AfxMessageBox("Password가 일치 하지 않습니다.");
			return;
		}
	}

	sprintf(g_pConfig->m_stStagePos[row - 1].chStagePositionString, "%s", m_strStagePosName);
	g_pConfig->m_stStagePos[row - 1].x = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	g_pConfig->m_stStagePos[row - 1].y = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	m_StagePosGrid.SetItemText(row, 1, m_strStagePosName);
	strX.Format(_T("%4.6f"), g_pConfig->m_stStagePos[row - 1].x);
	m_StagePosGrid.SetItemText(row, 2, strX);
	strY.Format(_T("%4.6f"), g_pConfig->m_stStagePos[row - 1].y);
	m_StagePosGrid.SetItemText(row, 3, strY);

	m_StagePosGrid.Invalidate(FALSE);

	g_pConfig->SaveStagePositionToDB();
}


void CMaskMapDlg::OnEnChangeEditIncrementStepsize()
{
	UpdateData(TRUE);
	m_dStageMovingDistance = atof(m_strStageMovingDistance);
}


void CMaskMapDlg::OnBnClickedButtonXMinus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonXMinus() Button Click!"));

	if (g_pNavigationStage->Simulator_Mode)
	{
		if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (!m_bReverseCoordinate)
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
		else
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
	}
}


void CMaskMapDlg::OnBnClickedButtonXPlus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonXPlus() Button Click!"));


	if (g_pNavigationStage->Simulator_Mode)
	{
		if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (!m_bReverseCoordinate)
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
		else
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}

	}
}


void CMaskMapDlg::OnBnClickedButtonYPlus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonYPlus() Button Click!"));

	if (g_pNavigationStage->Simulator_Mode)
	{
		if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (!m_bReverseCoordinate)
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
		else
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}

	}
}


void CMaskMapDlg::OnBnClickedButtonYMinus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonYMinus() Button Click!"));

	if (g_pNavigationStage->Simulator_Mode)
	{
		if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (!m_bReverseCoordinate)
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
		else
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}


	}
}


void CMaskMapDlg::OnBnClickedButtonStageOrigin()
{
}


void CMaskMapDlg::OnBnClickedButtonMoveorigin()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveorigin() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	g_pScanStage->Move_Origin();
}


void CMaskMapDlg::OnBnClickedButtonDdlinit()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonDdlinit() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	if (pwdlg.m_strTxt != "1234")
	{
		::AfxMessageBox("Password가 일치 하지 않습니다.");
		return;
	}

	//2. 물류 Thread 가동 중인지 확인

	// Scan Stage DDL Generation Run
	if (g_pScanStage != NULL)
	{
		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			g_pScanStage->Start_Scan(SCAN_MODE_DDL_GERNERATION, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);
			break;
		case ELITHO:
			g_pScanStage->Start_Scan(SCAN_MODE_DDL_GERNERATION, 10000, m_nEuvLitho_ScanGrid, 1);
			break;
		default:
			break;
		}
	}

}


void CMaskMapDlg::OnBnClickedButtonScan()
{
	g_pLog->Display(0, _T("CScanStageDlg::OnBnClickedButtonScan() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	//1. 물류 Thread 가동 중인지 확인

	// Scan Stage Run
	if (g_pScanStage != NULL)
	{
		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);
			break;
		case ELITHO:

			int RepeatNo;
			double FovX_um, FovY_um, height_um, StepSize_um;

			FovX_um = m_MaskMapWnd.m_ProcessData.pMeasureList[0].dFOVSizeX;
			FovY_um = m_MaskMapWnd.m_ProcessData.pMeasureList[0].dFOVSizeY;
			RepeatNo = m_MaskMapWnd.m_ProcessData.pMeasureList[0].nRepeatNo;
			height_um = m_MaskMapWnd.m_ProcessData.pMeasureList[0].dExposureHeight_um;
			StepSize_um = m_MaskMapWnd.m_ProcessData.pMeasureList[0].dStepSize;

			g_pNavigationStage->Start_Scan(FovX_um, FovY_um, StepSize_um, RepeatNo);

			break;
		default:
			break;
		}
	}

}

void CMaskMapDlg::OnBnClickedButtonRecord()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonRecord() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	//1. 물류 Thread 가동 중인지 확인

	// Scan Stage Record Run
	if (g_pScanStage != NULL)
	{
		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			g_pScanStage->Start_Scan(SCAN_MODE_RECORD, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);
			break;
		case ELITHO:
			g_pScanStage->Start_Scan(SCAN_MODE_RECORD, 10000, m_nEuvLitho_ScanGrid, 1);
			break;
		default:
			break;
		}
	}

}

void CMaskMapDlg::OnEnChangeEditIncrementUmstep()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::OnBnClickedButtonMoveXminus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveXminus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_CtrlStepSizeum.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (g_pScanStage->MoveRelative(X_AXIS, -dInc_position) != 0)
	{
		//::AfxMessageBox(" Stage 동작 불가!");
	}

}


void CMaskMapDlg::OnBnClickedButtonMoveXplus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveXplus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_CtrlStepSizeum.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (g_pScanStage->MoveRelative(X_AXIS, dInc_position) != 0)
	{
		//::AfxMessageBox(" Stage 동작 불가!");
	}
}


void CMaskMapDlg::OnBnClickedButtonMoveYplus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveYplus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_CtrlStepSizeum.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (g_pScanStage->MoveRelative(Y_AXIS, dInc_position) != 0)
	{
		//::AfxMessageBox(" Stage 동작 불가!");
	}
}


void CMaskMapDlg::OnBnClickedButtonMoveYminus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveYminus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_CtrlStepSizeum.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (g_pScanStage->MoveRelative(Y_AXIS, -dInc_position) != 0)
	{
		//::AfxMessageBox(" Stage 동작 불가!");
	}
}


void CMaskMapDlg::OnBnClickedButtonXyOrigin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::OnBnClickedButtonMoveZplus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveZplus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_EditStepSizeCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	g_pScanStage->MoveZRelative_SlowInterlock(dInc_position, PLUS_DIRECTION);
}


void CMaskMapDlg::OnBnClickedButtonMoveZminus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveZminus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_EditStepSizeCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	g_pScanStage->MoveZRelative_SlowInterlock(dInc_position, MINUS_DIRECTION);
}


void CMaskMapDlg::OnBnClickedButtonZOrigin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::OnEnChangeEditIncrementAnglestep()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::OnBnClickedButtonMoveTxminus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveTxminus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_CtrlStepSizeurad.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (g_pScanStage->MoveRelative(TIP_AXIS, -dInc_position) != 0)
	{
		//::AfxMessageBox(" Stage 동작 불가!");
	}

}


void CMaskMapDlg::OnBnClickedButtonMoveTxplus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveTxplus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_CtrlStepSizeurad.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (g_pScanStage->MoveRelative(TIP_AXIS, dInc_position) != 0)
	{
		//::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CMaskMapDlg::OnBnClickedButtonMoveTyplus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveTyplus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_CtrlStepSizeurad.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (g_pScanStage != NULL)
		g_pScanStage->MoveRelative(TILT_AXIS, dInc_position);
}


void CMaskMapDlg::OnBnClickedButtonMoveTyminus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMoveTyminus() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CString strStep;
	double dInc_position;
	m_CtrlStepSizeurad.GetWindowText(strStep);
	dInc_position = atof(strStep);

	if (g_pScanStage != NULL)
		g_pScanStage->MoveRelative(TILT_AXIS, -dInc_position);
}


void CMaskMapDlg::OnBnClickedButtonTxtyOrigin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMaskMapDlg::OnEnChangeEditZInterlockpos()
{
	UpdateData(TRUE);
	if (g_pConfig != NULL)
	{
		g_pConfig->m_dZInterlock_um = m_dZInterlockPos;
		g_pConfig->SaveRecoveryData();
	}
	if (g_pScanStage != NULL)
		g_pScanStage->m_dZUpperLimit = m_dZInterlockPos;
}

void CMaskMapDlg::OnEnChangeEditZFocuspos()
{
	UpdateData(TRUE);
	if (g_pConfig != NULL)
	{
		g_pConfig->m_dZFocusPos_um = m_dZFocusPos;
		g_pConfig->SaveRecoveryData();
	}
}

void CMaskMapDlg::OnEnChangeEditZThroughfocusHeight()
{
	UpdateData(TRUE);
}


void CMaskMapDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case LITHO_DOSE_TIME_CHECK:
		if (g_pWarning->n_bMessageMoxFlag)
		{
			LithoDoseTimeCheck();
			SetTimer(nIDEvent, 1000, NULL);
		}
		else
		{
			g_pWarning->n_bMessageMoxFlag = true;
		}
		break;
	default:
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}

void CMaskMapDlg::OnStagePosGridClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;

	m_strStagePosName = g_pConfig->m_stStagePos[row - 1].chStagePositionString;
	m_strXMovePos.Format(_T("%3.7f"), g_pConfig->m_stStagePos[row - 1].x);
	m_strYMovePos.Format(_T("%3.7f"), g_pConfig->m_stStagePos[row - 1].y);
	//m_StagePosGrid.SetEditable(false);

	UpdateData(false);

}

void CMaskMapDlg::OnStagePosGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;

	if (g_pLog == NULL || g_pConfig == NULL || g_pNavigationStage == NULL)
		return;

	g_pLog->Display(0, _T("CMaskMapDlg::OnStagePosGridDblClick !"));

	if (g_pNavigationStage->Simulator_Mode)
	{
		g_pNavigationStage->MoveStageDBPositionNo(row - 1);
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (g_pConfig->m_nEquipmentType == PHASE)
			g_pNavigationStage->MoveStageDBPositionNo(row - 1, FALSE);
		else
			g_pNavigationStage->MoveStageDBPositionNo(row - 1);
	}
}

void CMaskMapDlg::OnStagePosGridRClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;
	//if (col == 1)
	//{
	//	if (row > MAX_STAGE_POSITION - 20)
	//	{
	//		CPasswordDlg pwdlg(this);
	//		pwdlg.DoModal();
	//		if (pwdlg.m_strTxt != "srem")
	//		{
	//			::AfxMessageBox("Password가 일치 하지 않습니다.");
	//			return;
	//		}
	//	}

	//	m_StagePosGrid.SetEditable(true);
	//}
}

void CMaskMapDlg::OnStagePosGridStartEdit(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;
}

void CMaskMapDlg::OnStagePosGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;

	//row = m_StagePosGrid.GetSelectedRow();
	sprintf(g_pConfig->m_stStagePos[row - 1].chStagePositionString, "%s", m_StagePosGrid.GetItemText(row, col));
	m_StagePosGrid.SetEditable(false);
}

void CMaskMapDlg::OnDeltaposIncrementStepspin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(true);

	if (pNMUpDown->iDelta < 0)
	{
		if (m_dStageMovingDistance >= 0.000001 && m_dStageMovingDistance < 1) m_dStageMovingDistance *= 10;
		else if (m_dStageMovingDistance >= 1 && m_dStageMovingDistance < 10) m_dStageMovingDistance += 1;
		else if (m_dStageMovingDistance >= 10 && m_dStageMovingDistance < SOFT_LIMIT_PLUS_X)	m_dStageMovingDistance += 10;

		if (m_dStageMovingDistance > SOFT_LIMIT_PLUS_X)
			m_dStageMovingDistance = SOFT_LIMIT_PLUS_X;
	}
	else
	{
		if (m_dStageMovingDistance > 0.000001 && m_dStageMovingDistance <= 1) m_dStageMovingDistance /= 10;
		else if (m_dStageMovingDistance > 1 && m_dStageMovingDistance <= 10) m_dStageMovingDistance -= 1;
		else if (m_dStageMovingDistance > 10 && m_dStageMovingDistance <= SOFT_LIMIT_PLUS_X) m_dStageMovingDistance -= 10;

		if (m_dStageMovingDistance < 0.000001)
			m_dStageMovingDistance = 0.000001;
	}
	m_strStageMovingDistance.Format("%0.6f", m_dStageMovingDistance);

	UpdateData(false);

	*pResult = 0;
}

void CMaskMapDlg::OnBnClickedButtonMaskmeasureAutostart()
{
	CString strLog;

	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskmeasureAutostart() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	if (IDYES != AfxMessageBox("자동 Sequence를 진행하시겠습니까?", MB_YESNO)) return;

	strLog.Format(_T("AutoRun LongRun Test Start Button Click"));
	SaveLogFile("AutoRun_LongRunTestLog", strLog);
	AutoRun(m_nAutoRunRepeatSetNo);
}

void CMaskMapDlg::OnBnClickedButtonMaskload()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskload() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 3);
		return;
	}


	////////////////////////////////////////
	// OPER MODE ON 동작 확인 ( 임시 삭제 )
	////////////////////////////////////////
	//if (g_pIO->IO_MODE != OPER_MODE_ON)
	//{
	//	MsgBoxAuto.DoModal(_T(" OPERATOR MODE가 아닙니다. OPERATOR MODE로 변경 후 진행해주세요. "), 3);
	//	return;
	//}

	if (IDYES != AfxMessageBox("Mask를 Loading 하시겠습니까?", MB_YESNO)) return;

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != TRUE)
	{
		if (IDYES != AfxMessageBox("캠센서 인터락이 해제되어 있습니다. 계속 진행하시겠습니까?", MB_YESNO)) return;
	}

	MaskLoad();
}

void CMaskMapDlg::OnBnClickedButtonMaskalign()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskalign() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	if (IDYES != AfxMessageBox("Mask Align을 진행 하시겠습니까?", MB_YESNO)) return;

	int ret = 0;
	switch (g_pConfig->m_nEquipmentType)
	{
	case SREM033:
		//ret = MaskZInterlock();
		if (ret == 0)
			ret = MaskAlign_OM(m_bManualAlign);
		if (ret == 0 && !m_bOMAlignOnly)
			ret = MaskAlign_EUV_SREM(m_bManualAlign);
		break;
	case PHASE:
		ret = MaskAlign_OM(m_bManualAlign);
		if (ret == 0 && !m_bOMAlignOnly)
			ret = MaskAlign_EUV_Phase(m_bManualAlign);
		break;
	case EUVPTR:
		ret = MaskAlign_OM(m_bManualAlign);
		if (ret == 0)//EUV 영역은 OM-EUV Offset을 이용해서 Align 완료
		{
			m_dEUVAlignPointRT_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
			m_dEUVAlignPointRT_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;
			g_pConfig->m_dEUVAlignPointRT_X_mm = m_dEUVAlignPointRT_posx_mm;
			g_pConfig->m_dEUVAlignPointRT_Y_mm = m_dEUVAlignPointRT_posy_mm;
			m_dEUVAlignPointLT_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
			m_dEUVAlignPointLT_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;
			g_pConfig->m_dEUVAlignPointLT_X_mm = m_dEUVAlignPointLT_posx_mm;
			g_pConfig->m_dEUVAlignPointLT_Y_mm = m_dEUVAlignPointLT_posy_mm;
			m_dEUVAlignPointLB_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
			m_dEUVAlignPointLB_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;
			g_pConfig->m_dEUVAlignPointLB_X_mm = m_dEUVAlignPointLB_posx_mm;
			g_pConfig->m_dEUVAlignPointLB_Y_mm = m_dEUVAlignPointLB_posy_mm;

			m_bEUVAlignComplete = TRUE;
			g_pConfig->m_bEUVAlignCompleteFlag = m_bEUVAlignComplete;
			g_pConfig->SaveRecoveryData();
			m_bAutoSequenceProcessing = FALSE;
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Mask Align이 완료되었습니다! "), 2);
			m_strStageStatus.Format(_T("Mask Align(EUV) 완료!"));
			m_MaskMapWnd.Invalidate(FALSE);
			UpdateData(FALSE);
			g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_SREM() End!"));
		}
		break;
	case ELITHO:
		//ret = MaskZInterlock();

		/* ELITHO Align Manual로 진행*/
		/* ELITHO Align Only OM 으로 진행*/
		ret = MaskAlign_OM(TRUE);
		if (ret == 0 && m_bOMAlignOnly)
			ret = WaferAlign_Notch_Litho(TRUE);

		/* TEST */
		//g_pNavigationStage->MoveToMaskCenterCoordinate(2.0, 2.0, FALSE);

		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("MaskAlign return: %d"), ret);
	g_pLog->Display(0, str);
}

void CMaskMapDlg::OnBnClickedButtonMaskmeasureStart()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskmeasureStart() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	int ret = 0;

	switch (g_pConfig->m_nEquipmentType)
	{
	case SREM033:
		if (IDYES != AfxMessageBox("EUV Imaging Process를 진행 하시겠습니까?", MB_YESNO)) return;
		if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
		{
			MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
			return;
		}

		if (m_MaskMapWnd.m_ProcessData.pMeasureList[0].dFOVSize * 1000 == 3000 && m_MaskMapWnd.m_ProcessData.pMeasureList[0].dStepSize == 10 && g_pAdam->m_IsUse3umFOV)
		{
			ret = MaskImagingProcess(); //ihlee 21.05.27  임시로 막음
			//ret = MaskImagingProcessNew();
		}
		else
		{
			ret = MaskImagingProcess();
		}
		break;
	case PHASE:
		if (IDYES != AfxMessageBox("EUV Phase 측정 Process를 진행 하시겠습니까?", MB_YESNO)) return;
		ret = MaskPhaseProcess();
		break;
	case EUVPTR:
		if (IDYES != AfxMessageBox("EUV PTR 측정 Process를 진행 하시겠습니까?", MB_YESNO)) return;
		ret = PtrProcess();
		break;
	case ELITHO:
		if (IDYES != AfxMessageBox("EUV 노광 Process를 진행 하시겠습니까?", MB_YESNO)) return;
		//ret = LithoDoseProcess();
		ret = LithoGratingProcess();
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("MaskImagingProcess return: %d"), ret);
	g_pLog->Display(0, str);
}

void CMaskMapDlg::OnBnClickedButtonMaskunload()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskunload() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 3);
		return;
	}

	////////////////////////////////////////
	// OPER MODE ON 동작 확인 ( 임시 삭제 )
	////////////////////////////////////////
	//if (g_pIO->IO_MODE != OPER_MODE_ON)
	//{
	//	MsgBoxAuto.DoModal(_T(" OPERATOR MODE가 아닙니다. OPERATOR MODE로 변경 후 진행해주세요. "), 3);
	//	return;
	//}

	if (IDYES != AfxMessageBox("Mask를 Unloading 하시겠습니까?", MB_YESNO)) return;

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != TRUE)
	{
		if (IDYES != AfxMessageBox("캠센서 인터락이 해제되어 있습니다. 계속 진행하시겠습니까?", MB_YESNO)) return;
	}

	MaskUnload();
}


void CMaskMapDlg::OnBnClickedButtonStopAutosequence()
{

	//vector<double> a = { 3, 2,0 };
	//vector<double> b = { 1, 4,0 };
	//vector<double> c = { 5, 4,0 };
	//CenterOfCircumCircle(a, b, c);

	g_pAP->StopSequence();
	m_bAutoSequenceProcessing = FALSE;

	g_pPhase->m_IsStop = TRUE;

	g_pScanStage->Stop_Scan();
	g_pNavigationStage->Stop_Scan();
	g_pAdam->ADAMAbort();
	g_pAdam->KillTimer(ADAMDATA_DISPLAY_TIMER);
}

void CMaskMapDlg::OnEnChangeEditScanRepeatNumber()
{
	UpdateData(TRUE);
}


void CMaskMapDlg::OnBnClickedButtonMovePosition()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMovePosition() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;
}


void CMaskMapDlg::OnDeltaposIncrementUmstepspin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strStep;
	double position;

	m_CtrlStepSizeum.GetWindowText(strStep);
	position = atof(strStep);

	if (pNMUpDown->iDelta < 0)
	{
		if (position >= 0.001 && position < 1) position *= 10;
		else position++;

		if (position > X_PLUSLIMIT)
			position = X_PLUSLIMIT;

		strStep.Format(_T("%2.3f"), position);
		m_CtrlStepSizeum.SetWindowText(strStep);
	}
	else
	{
		if (position > 0.001 && position <= 1)	position /= 10;
		else position--;

		if (position < 0.001)
			position = 0.001;

		strStep.Format(_T("%2.3f"), position);
		m_CtrlStepSizeum.SetWindowText(strStep);
	}

	*pResult = 0;
}

void CMaskMapDlg::OnDeltaposIncrementUmstepspinz(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strStep;
	double position;

	m_EditStepSizeCtrl.GetWindowText(strStep);
	position = atof(strStep);

	if (pNMUpDown->iDelta < 0)
	{
		position++;

		if (position > g_pScanStage->m_dZUpperLimit)
			position = g_pScanStage->m_dZUpperLimit;

		strStep.Format(_T("%3.1f"), position);
		m_EditStepSizeCtrl.SetWindowText(strStep);
	}
	else
	{
		position--;

		if (position < Z_MINUSLIMIT)
			position = Z_MINUSLIMIT;

		strStep.Format(_T("%3.1f"), position);
		m_EditStepSizeCtrl.SetWindowText(strStep);
	}

	*pResult = 0;
}

void CMaskMapDlg::OnDeltaposIncrementUradstepspin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	CString strStep;
	double position;

	m_CtrlStepSizeurad.GetWindowText(strStep);
	position = atof(strStep);

	if (pNMUpDown->iDelta < 0)
	{
		position++;

		if (position > TIP_PLUSLIMIT)
			position = TIP_PLUSLIMIT;

		strStep.Format(_T("%4.1f"), position);
		m_CtrlStepSizeurad.SetWindowText(strStep);
	}
	else
	{
		position--;

		if (position < 0)
			position = 0;

		strStep.Format(_T("%4.1f"), position);
		m_CtrlStepSizeurad.SetWindowText(strStep);
	}

	*pResult = 0;
}

void CMaskMapDlg::OnBnClickedButtonXyMove()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonXyMove() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	UpdateData(true);


	if (g_pNavigationStage != NULL)
	{
		if (g_pConfig->m_nEquipmentType == PHASE)
		{
			g_pNavigationStage->MoveAbsolutePosition(atof(m_strXMovePos), atof(m_strYMovePos), FALSE);
		}
		else
		{
			g_pNavigationStage->MoveAbsolutePosition(atof(m_strXMovePos), atof(m_strYMovePos));
		}
	}


}

void CMaskMapDlg::OnEnChangeEditXMovepos()
{
	UpdateData(true);
}


void CMaskMapDlg::OnEnChangeEditYMovepos()
{
	UpdateData(true);
}


void CMaskMapDlg::OnBnClickedButtonZoomreset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::ChangeOMEUVSet(int direction)
{
	if (g_pNavigationStage == NULL || g_pAdam == NULL || g_pScanStage == NULL || g_pCamera == NULL || g_pRecipe == NULL || g_pConfig == NULL || g_pAP == NULL)
		return;

	//Global Offset만을 적용시켜서 이동시키기 위해//
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double current_posx_mm = 0.0, current_posy_mm = 0.0;
	current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	CPoint pt;
	pt.x = current_posx_mm;
	pt.y = current_posy_mm;

	switch (direction)
	{
	case OMTOEUV:
		g_pCamera->ShowWindow(SW_HIDE);
		g_pRecipe->ShowWindow(SW_HIDE);
		//20201221 jkseo, Om->Euv 시 조명 Off
		g_pLightCtrl->LightOff();

		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			g_pAdam->ShowWindow(SW_SHOW);
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_SHOW);
			g_pPhase->ShowWindow(SW_SHOW);
			break;
		case EUVPTR:
			g_pPTR->ShowWindow(SW_SHOW);
			break;
		case ELITHO:
			g_pAdam->ShowWindow(SW_SHOW);
			//g_pXrayCamera->ShowWindow(SW_SHOW);
			//g_pPhase->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}

		m_MaskMapWnd.m_nMicroscopyType = EUV;
		m_AdamOmtiltSwitch = 1;
		OnBnClickedCheckOmAdamUi();

		m_CheckOMAdamCtrl.SetWindowText(_T("To Optic Microscopy"));
		target_posx_mm = current_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
		target_posy_mm = current_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;

		//asdf
		//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0 && g_pConfig->m_rcOMStageAreaRect.PtInRect(pt) == TRUE && g_pAP->Check_STAGE_MovePossible() == XY_NAVISTAGE_OK)
		//ihlee 추가
		if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) > Z_INITIAL_POS_UM)	// ihlee 변경
		{
			g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
			WaitSec(2);
		}
		if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= Z_INITIAL_POS_UM && g_pConfig->m_rcOMStageAreaRect.PtInRect(pt) == TRUE && g_pAP->Check_STAGE_MovePossible() == XY_NAVISTAGE_OK)
		{
			if (g_pWarning != NULL)
			{
				g_pWarning->m_strWarningMessageVal = "Stage 이동 중입니다. 잠시 기다려 주세요!";
				g_pWarning->UpdateData(FALSE);
				g_pWarning->ShowWindow(SW_SHOW);
			}
			m_bAutoSequenceProcessing = TRUE;
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
			if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
				g_pNavigationStage->SetLaserMode();
			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			WaitSec(1);
			if (g_pConfig->m_nEquipmentType == SREM033)
			{
				g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos);
			}
			else if (g_pConfig->m_nEquipmentType == PHASE)
			{
				//Z축 이동 없음
			}

			m_bAutoSequenceProcessing = FALSE;
			if (g_pWarning != NULL)
				g_pWarning->ShowWindow(SW_HIDE);
		}
		break;
	case EUVTOOM:
		g_pCamera->ShowWindow(SW_SHOW);
		//20201221 jkseo, Euv->Om 시 조명 On
		g_pLightCtrl->LightOn();

		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			g_pAdam->ShowWindow(SW_HIDE);
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_HIDE);
			g_pPhase->ShowWindow(SW_HIDE);
			break;
		case EUVPTR:
			g_pPTR->ShowWindow(SW_HIDE);
			break;
		case ELITHO:
			g_pAdam->ShowWindow(SW_HIDE);
			//g_pXrayCamera->ShowWindow(SW_HIDE);
			//g_pPhase->ShowWindow(SW_HIDE);
			break;
		default:
			break;
		}


		m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		g_pRecipe->ShowWindow(SW_SHOW);
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("OM"));
		m_AdamOmtiltSwitch = 0;
		OnBnClickedCheckOmAdamUi();

		m_CheckOMAdamCtrl.SetWindowText(_T("To EUV Microscopy"));
		target_posx_mm = current_posx_mm + g_pConfig->m_dGlobalOffsetX_mm;
		target_posy_mm = current_posy_mm - g_pConfig->m_dGlobalOffsetY_mm;

		if (g_pConfig->m_rcEUVStageAreaRect.PtInRect(pt) == TRUE && g_pAP->Check_STAGE_MovePossible() == XY_NAVISTAGE_OK)
		{
			if (g_pWarning != NULL)
			{
				g_pWarning->m_strWarningMessageVal = "Stage 이동 중입니다. 잠시 기다려 주세요!";
				g_pWarning->UpdateData(FALSE);
				g_pWarning->ShowWindow(SW_SHOW);
			}
			m_bAutoSequenceProcessing = TRUE;

			//asdf
			//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) > 0)	//Z=-300에서 안전하므로 Optic 기구 조정전까지 임시로 막아둠 by smchoi	
			if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) > Z_INITIAL_POS_UM)	// ihlee 변경
			{
				g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
				WaitSec(2);
			}
			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
			g_pNavigationStage->SetEncoderMode();
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//OM 영역으로 이동
			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
			m_bAutoSequenceProcessing = FALSE;
			g_pWarning->ShowWindow(SW_HIDE);
		}
		break;
	default:
		break;
	}
	m_MaskMapWnd.Invalidate(FALSE);
}

int CMaskMapDlg::AutoRun(int nRepeatNo)
{
	CString strLog;

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -1;
	}

	m_strStageStatus.Format(_T("Auto Sequence 가동 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_nAutoRunRepeatResultNo = 0;
	m_bAutoSequenceProcessing = TRUE;

	while (m_bAutoSequenceProcessing == TRUE && m_nAutoRunRepeatResultNo < nRepeatNo)
	{
		strLog.Format(("AutoRun LongRun Test %d Start"), m_nAutoRunRepeatResultNo);
		SaveLogFile("AutoRun_LongRunTestLog", strLog);

		WaitSec(3);	//혹시 가동 취소 버튼 누를 시간 확보
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			strLog.Format(_T("AutoRun LongRun Test Stop Button Click "));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			return -1;
		}

		//if (g_pConfig->m_nMaterialLocation == MTS_POD)
		//{
			m_strStageStatus.Format(_T("Mask Loading 시작!"));
			SaveLogFile("AutoRun_LongRunTestLog", m_strStageStatus);
			UpdateData(FALSE);
			g_pWarning->m_strWarningMessageVal = " Mask Loading 중입니다! ";
			g_pWarning->UpdateData(FALSE);
			if (g_pAP->MaskLoadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				strLog.Format(_T("AutoRun LongRun Test 중 Mask Loading Error 발생으로 Sequence 중지 "));
				SaveLogFile("AutoRun_LongRunTestLog", strLog);
				return -1;
			}
			while (TRUE)
			{
				//ProcessMessages();
				//Sleep(10);
				WaitSec(1);
				if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE)
					break;

				if (m_bAutoSequenceProcessing == FALSE)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					return -1;
				}

				if (g_pAP->m_nProcessErrorCode != 0)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					m_bAutoSequenceProcessing = FALSE;
					return -1;
				}
			}
			m_strStageStatus.Format(_T("Mask Loading 완료!"));
			SaveLogFile("AutoRun_LongRunTestLog", m_strStageStatus);
			UpdateData(FALSE);
			WaitSec(3);
			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				return -1;
			}
		//}

		int ret = 0;
		if (g_pConfig->m_nMaterialLocation == CHUCK)
		{
			g_pWarning->m_strWarningMessageVal = " Mask Align 중입니다! ";
			strLog.Format(_T("Mask Align 중입니다!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			g_pWarning->UpdateData(FALSE);
			ret = MaskZInterlock();

			//////////////////////////////////////////////////////////////////
			// 2021.06.16
			// Long Run Test 를 위해 형식적인 Align 진행 (jhkim)
			//
			ret = MaskAlign_OM_OnlyLongTest(TRUE);
			//ret = MaskAlign_OM(m_bManualAlign);
			
			if (!m_bOMAlignOnly)
			{
				switch (g_pConfig->m_nEquipmentType)
				{
				case SREM033:
					ret = MaskAlign_EUV_SREM(m_bManualAlign);
					break;
				case PHASE:
					ret = MaskAlign_EUV_Phase(m_bManualAlign);
					break;
				default:
					break;
				}
			}
			//MaskAlign(TRUE, SCOPE_OM4X);
			//MaskAlign(TRUE, SCOPE_ALL);
			g_pWarning->ShowWindow(SW_SHOW);
			m_bAutoSequenceProcessing = TRUE;	//MaskAlign() 내부에서 끝날때 FALSE를 만들기때문에 여기서 다시 TRUE로 변경
			WaitSec(3);
			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				return -1;
			}

			g_pWarning->m_strWarningMessageVal = " Image 자동 측정 중입니다! ";
			g_pWarning->UpdateData(FALSE);
			strLog.Format(_T("Image 자동 측정 중입니다!!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			//////////////////////////////////////////////////////////////////
			// 2021.06.16
			// Euv 없이 Scan 진행 Test (jhkim)
			//
			strLog.Format(_T("Euv 없이 Test 위한 Scan 진행!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			NoEuvMaskImagingProcess();
			// MaskImagingProcess();
			//////////////////////////////////////////////////////////////////
			g_pWarning->ShowWindow(SW_SHOW);
			m_bAutoSequenceProcessing = TRUE;	//MaskImagingProcess() 내부에서 끝날때 FALSE를 만들기때문에 여기서 다시 TRUE로 변경
			WaitSec(3);
			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				return -1;
			}

			g_pEUVSource->SetMechShutterOpen(FALSE);
			g_pEUVSource->SetEUVSourceOn(FALSE);
			WaitSec(3);



			g_pWarning->m_strWarningMessageVal = " Mask Unloading 중입니다! ";
			g_pWarning->UpdateData(FALSE);

			strLog.Format(_T("Mask Unloading 중입니다!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);

			if (g_pAP->MaskUnloadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				return -1;
			}
			while (TRUE)
			{
				//ProcessMessages();
				//Sleep(10);
				WaitSec(1);
				if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE)
					break;

				if (m_bAutoSequenceProcessing == FALSE)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					return -1;
				}

				if (g_pAP->m_nProcessErrorCode != 0)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					m_bAutoSequenceProcessing = FALSE;
					return -1;
				}
			}
			WaitSec(3);

			strLog.Format(_T("Mask Unloading 완료 되었습니다!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
		}



		m_nAutoRunRepeatResultNo++;
		UpdateData(FALSE);
	}

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	m_strStageStatus.Format(_T("Auto Sequence 완료!"));
	strLog.Format(_T("Auto Sequence 완료!!"));
	SaveLogFile("AutoRun_LongRunTestLog", strLog);
	UpdateData(FALSE);
}

int CMaskMapDlg::MaskLoad()
{
	if (g_pAP == NULL || g_pLog == NULL || g_pWarning == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_bAutoSequenceProcessing = TRUE;

	WaitSec(3);	//혹시 가동 취소 버튼 누를 시간 확보
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
		return -1;
	}

	g_pWarning->m_strWarningMessageVal = " Mask Loading 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	if (g_pAP->MaskLoadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
		m_bAutoSequenceProcessing = FALSE;
		return -1;
	}

	while (TRUE)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
		if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE)
			break;

		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
			return -1;
		}

		if (g_pAP->m_nProcessErrorCode != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			g_pAlarm->SetAlarm(g_pAP->m_nProcessErrorCode);
			m_bAutoSequenceProcessing = FALSE;
			return -1;
		}
	}
	WaitSec(3);

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Loading이 완료되었습니다! "), 3);
	return 0;
}

int CMaskMapDlg::MaskUnload()
{
	if (g_pAP == NULL || g_pLog == NULL || g_pWarning == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_bAutoSequenceProcessing = TRUE;

	WaitSec(3);
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
		return -1;
	}

	g_pWarning->m_strWarningMessageVal = " Mask Unloading 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	if (g_pAP->MaskUnloadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
		m_bAutoSequenceProcessing = FALSE;
		return -1;
	}
	while (TRUE)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
		if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE)
			break;

		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
			return -1;
		}

		if (g_pAP->m_nProcessErrorCode != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			g_pAlarm->SetAlarm(g_pAP->m_nProcessErrorCode);
			m_bAutoSequenceProcessing = FALSE;
			return -1;
		}
	}
	WaitSec(3);

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Unloading이 완료되었습니다! "), 3);
	return 0;
}


//2021.03.17 백업 ihlee
int CMaskMapDlg::MaskImagingProcess()
{
	CString strLog;

	strLog.Format("Begin(MaskImagingProcess)");
	SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}

	if (m_bEUVAlignComplete == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" Mask Align부터 진행해주세요! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::MaskImagingProcess() Start!"));

	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("Auto Image 측정 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " EUV Image 생성 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	g_pAdam->m_bEUVCrossLineDisplay = FALSE;
	//g_pAdam->Invalidate(FALSE);
	g_pAdam->UpdateData(FALSE);



	// Laser Feedback으로 전환
	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	{
		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
		g_pNavigationStage->SetLaserMode();
	}

	CString strStatus;

	for (int i = 0; i < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; i++)
	{
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -5;
		}


		m_MaskMapWnd.m_ProcessData.CurrentMeasureNum = i + 1;
		m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum = 1;

		strStatus.Format("%d/%d", i + 1, m_MaskMapWnd.m_ProcessData.TotalMeasureNum);
		g_pAdam->m_ctrlMeasureStatus.SetWindowTextA(strStatus);

		strStatus.Format("%d/%d", 1, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo);
		g_pAdam->m_ctrlThroughFocusStatus.SetWindowTextA(strStatus);

		// Set FOV info and Update ADAM UI
		g_pAdam->m_nEuvImage_Fov = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dFOVSize*1000.;
		g_pAdam->m_nEuvImage_ScanGrid = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dStepSize;
		g_pAdam->m_nEuvImage_ScanNumber = m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;
		g_pAdam->SetAdamFovInfoUi();

		strLog.Format("Begin(MoveToSelectedPointNo): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

		g_pNavigationStage->MoveToSelectedPointNo(i);

		strLog.Format("End(MoveToSelectedPointNo): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

		double refY = g_pNavigationStage->GetTargetPosmm(STAGE_Y_AXIS);

		//Focus Position으로 이동  21.01.17

		strLog.Format("Begin(MoveZCapsensorFocusPosition): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

		//int nRetFocus = g_pAdam->MoveZCapsensorFocusPosition(TRUE);// Interlock은 세팅 안함... TF시 첫번째 scan위치로 이동으로 변경 (최적화)


		// 이미지 저장용 파라미터
		int nRefCap = 0;
		double refCapPos = 0;
		double measureCapPos = 0;
		double deltaFocusPos = 0;

		int nRetFocus = g_pAdam->MoveZCapsensorFocusPosition(TRUE, FALSE, 5, &nRefCap, &refCapPos);

		if (nRetFocus != 0)
		{
			// Focus 이동 실패
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! z축 제어 에러 "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
			return -6;
		};

		// Z축 Stage 위치 세팅
		g_pScanStage->GetPosAxesData();
		g_pMaskMap->m_dZFocusPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
		g_pConfig->m_dZFocusPos_um = g_pMaskMap->m_dZFocusPos;
		g_pConfig->SaveRecoveryData();

		strLog.Format("End(MoveZCapsensorFocusPosition): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogMaskImagingProcess ", strLog);


		// Through Focus
		for (int j = 0; j < m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo; j++)
		{
			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
				g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
				return -7;
			}

			m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum = j + 1;

			strStatus.Format("%d/%d", j + 1, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo);
			g_pAdam->m_ctrlThroughFocusStatus.SetWindowTextA(strStatus);

			if (m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo > 1)
			{

				double y;
				double delta_z;
				int step_z;

				if (g_pAdam->m_IsCrossStep)// 임시 적용 ihlee
				{
					step_z = ((j + 1) / 2) * pow(-1, j)*-1;
					delta_z = (m_MaskMapWnd.m_ProcessData.pMeasureList[i].dThroughFocusStep) * step_z;
				}
				else
				{
					delta_z = m_dZThroughFocusHeight - m_MaskMapWnd.m_ProcessData.pMeasureList[i].dThroughFocusStep * j;
				}

				deltaFocusPos = delta_z;
				measureCapPos = refCapPos + delta_z;
				//z축 이동
				strLog.Format("Begin(Z_axix_Control): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

				g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos + delta_z);
				g_pScanStage->GetPosAxesData();
				g_pScanStage->UpdateDataScanStage();

				strLog.Format("End(Z_axix_Control): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess ", strLog);


				//y축 보상
				strLog.Format("Begin(Y_axis_Correction): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

				y = refY + delta_z * 0.105 / 1000.0;
				g_pNavigationStage->MoveAbsolute(STAGE_Y_AXIS, y);
				g_pNavigationStage->WaitInPosition(STAGE_Y_AXIS, y);

				strLog.Format("End(Y_axis_Correction): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

			}
			else
			{

				deltaFocusPos = 0;
				measureCapPos = refCapPos;
				//strLog.Format("Begin(Z_axix_Control): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				//SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

				//g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos); //삭제 가능..ihlee
				//g_pScanStage->GetPosAxesData(); //삭제 가능..ihlee
				//g_pScanStage->UpdateDataScanStage(); //삭제 가능..ihlee

				//strLog.Format("End(Z_axix_Control): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				//SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			}

			/*	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
				{
					g_pNavigationStage->SetEncoderMode();
				}*/

			g_pAdam->ADAMRunStart();

			// Scan
			strLog.Format("Begin(Scan): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

			g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
			g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);

			strLog.Format("End(Scan): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

			strLog.Format("Begin(WaitScaningOn): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			while (g_pAdam->m_bIsScaningOn)
			{
				//ProcessMessages();
				//Sleep(10);
				WaitSec(1);
			}
			strLog.Format("End(WaitScaningOn): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
			//{
			//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			//	g_pNavigationStage->SetLaserMode();
			//}

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
				g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
				return -8;
			}

			//if (g_pAdam->m_nEuvImage_Fov == 1000 || g_pAdam->m_nEuvImage_Fov == 1500 || g_pAdam->m_nEuvImage_Fov == 2000 || g_pAdam->m_nEuvImage_Fov == 3000)

			strLog.Format("Begin(SaveThreadRun): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			if (g_pAdam->m_bIsUseInterpolation)
			{
				g_pAdam->FilteredImageFileSaveByThread(50, m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum, measureCapPos, nRefCap, deltaFocusPos);
			}
			strLog.Format("End(SaveThreadRun): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
		}
	}

	g_pAdam->ADAMAbort();

	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Auto Image 측정이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("Auto Image 측정 완료!"));
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskImagingProcess() End!"));

	g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
	g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);


	strLog.Format("End(MaskImagingProcess)");
	SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

	return 0;
}

int CMaskMapDlg::MaskImagingProcessNew()
{

	//임시.. 

	CString strLog;

	strLog.Format("Begin(MaskImagingProcess)");
	SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}



	g_pLog->Display(0, _T("CMaskMapDlg::MaskImagingProcess() Start!"));

	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("Auto Image 측정 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " EUV Image 생성 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	g_pAdam->m_bEUVCrossLineDisplay = FALSE;
	//g_pAdam->Invalidate(FALSE);
	g_pAdam->UpdateData(FALSE);


	CString strStatus;

	for (int i = 0; i < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; i++)
	{
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -5;
		}


		m_MaskMapWnd.m_ProcessData.CurrentMeasureNum = i + 1;
		m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum = 1;

		strStatus.Format("%d/%d", i + 1, m_MaskMapWnd.m_ProcessData.TotalMeasureNum);
		g_pAdam->m_ctrlMeasureStatus.SetWindowTextA(strStatus);

		strStatus.Format("%d/%d", 1, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo);
		g_pAdam->m_ctrlThroughFocusStatus.SetWindowTextA(strStatus);

		// Set FOV info and Update ADAM UI
		g_pAdam->m_nEuvImage_Fov = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dFOVSize*1000.;
		g_pAdam->m_nEuvImage_ScanGrid = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dStepSize;
		g_pAdam->m_nEuvImage_ScanNumber = m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;
		g_pAdam->SetAdamFovInfoUi();



		double refY = g_pNavigationStage->GetTargetPosmm(STAGE_Y_AXIS);


		// 이미지 저장용 파라미터

		int nRet = g_pAdam->Command_AverageRunTimeout();

		int nRefCap = 0;
		double refCapPos = 0;
		double measureCapPos = 0;
		double deltaFocusPos = 0;


		// Through Focus
		for (int j = 0; j < m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo; j++)
		{
			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
				g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
				return -7;
			}

			m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum = j + 1;

			strStatus.Format("%d/%d", j + 1, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo);
			g_pAdam->m_ctrlThroughFocusStatus.SetWindowTextA(strStatus);



			deltaFocusPos = 0;
			measureCapPos = refCapPos;


			g_pAdam->ADAMRunStart();

			// Scan
			strLog.Format("Begin(Scan): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

			g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
			g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);

			strLog.Format("End(Scan): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

			strLog.Format("Begin(WaitScaningOn): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			while (g_pAdam->m_bIsScaningOn)
			{
				//ProcessMessages();
				//Sleep(10);
				WaitSec(1);
			}
			strLog.Format("End(WaitScaningOn): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
			//{
			//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			//	g_pNavigationStage->SetLaserMode();
			//}

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
				g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
				return -8;
			}

			//if (g_pAdam->m_nEuvImage_Fov == 1000 || g_pAdam->m_nEuvImage_Fov == 1500 || g_pAdam->m_nEuvImage_Fov == 2000 || g_pAdam->m_nEuvImage_Fov == 3000)

			strLog.Format("Begin(SaveThreadRun): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			if (g_pAdam->m_bIsUseInterpolation)
			{
				g_pAdam->FilteredImageFileSaveByThread(50, m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum, measureCapPos, nRefCap, deltaFocusPos);
			}
			strLog.Format("End(SaveThreadRun): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
		}
	}

	g_pAdam->ADAMAbort();

	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Auto Image 측정이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("Auto Image 측정 완료!"));
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskImagingProcess() End!"));

	g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
	g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);


	strLog.Format("End(MaskImagingProcess)");
	SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

	return 0;

	//CString strLog;
	//CString strStatus;
	//int nRet = 0;
	//double refY;

	//strLog.Format("Begin(MaskImagingProcess)");
	//SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//if (g_pConfig->m_nEquipmentMode == OFFLINE)
	//	return -1;

	//if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL)
	//	return -2;

	//CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	//if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	//{
	//	MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
	//	return -3;
	//}

	//if (m_bEUVAlignComplete == FALSE)
	//{
	//	MsgBoxAuto.DoModal(_T(" Mask Align부터 진행해주세요! "), 2);
	//	return -4;
	//}

	//g_pLog->Display(0, _T("CMaskMapDlg::MaskImagingProcess() Start!"));

	////1. 초기화(1)
	//ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동
	//m_strStageStatus.Format(_T("Auto Image 측정 시작!"));
	//UpdateData(FALSE);
	//g_pWarning->m_strWarningMessageVal = " EUV Image 생성 중입니다! ";
	//g_pWarning->UpdateData(FALSE);
	//g_pWarning->ShowWindow(SW_SHOW);
	//m_bAutoSequenceProcessing = TRUE;

	//int TotalPointNum = m_MaskMapWnd.m_ProcessData.TotalMeasureNum;
	//g_pAdam->AdamUIControl(FALSE);
	//g_pAdam->m_IsMeasureComplete = FALSE;
	//g_pAdam->m_bAbort = FALSE;
	//g_pAdam->m_bEUVCrossLineDisplay = FALSE;
	//g_pAdam->UpdateData(FALSE);

	//// Laser Feedback으로 전환
	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	//{
	//	g_pNavigationStage->SetLaserMode();
	//}


	//// 아랫부분 Thread로 빼야함..
	//deque<std::thread> PointMeasureThreadQue;

	//for (int pointNum = 0; pointNum < TotalPointNum; pointNum++)
	//{
	//	if (m_bAutoSequenceProcessing == TRUE)
	//	{
	//		//2. 초기화(2) - Measure Point

	//		m_MaskMapWnd.m_ProcessData.CurrentMeasureNum = pointNum + 1;
	//		m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum = 1;

	//		strStatus.Format("%d/%d", pointNum + 1, m_MaskMapWnd.m_ProcessData.TotalMeasureNum);
	//		g_pAdam->m_ctrlMeasureStatus.SetWindowTextA(strStatus);

	//		strStatus.Format("%d/%d", 1, m_MaskMapWnd.m_ProcessData.pMeasureList[pointNum].nThroughFocusNo);
	//		g_pAdam->m_ctrlThroughFocusStatus.SetWindowTextA(strStatus);

	//		// Set FOV info and Update ADAM UI
	//		g_pAdam->m_nEuvImage_Fov = m_MaskMapWnd.m_ProcessData.pMeasureList[pointNum].dFOVSize*1000.;
	//		g_pAdam->m_nEuvImage_ScanGrid = m_MaskMapWnd.m_ProcessData.pMeasureList[pointNum].dStepSize;
	//		g_pAdam->m_nEuvImage_ScanNumber = m_MaskMapWnd.m_ProcessData.pMeasureList[pointNum].nRepeatNo;

	//		// Pixel size를 정의한 부분임
	//		g_pAdam->m_nRawImage_PixelWidth = g_pAdam->m_nEuvImage_Fov / g_pAdam->m_nEuvImage_ScanGrid;
	//		g_pAdam->m_nRawImage_PixelHeight = g_pAdam->m_nEuvImage_Fov / g_pAdam->m_nEuvImage_ScanGrid;

	//		g_pAdam->m_nReGridImage_PixelWidth = g_pAdam->m_nEuvImage_Fov / g_pAdam->m_nEUVImage_InterpolationGrid;
	//		g_pAdam->m_nReGridImage_PixelHeight = g_pAdam->m_nEuvImage_Fov / g_pAdam->m_nEUVImage_InterpolationGrid;
	//		g_pAdam->SetAdamFovInfoUi();

	//		strLog.Format("Begin(MoveToSelectedPointNo): Point:%d", pointNum);
	//		SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//		//3. Measure Point로  Stage 이동 
	//		g_pNavigationStage->MoveToSelectedPointNo(pointNum);
	//		refY = g_pNavigationStage->GetTargetPosmm(STAGE_Y_AXIS);

	//		strLog.Format("End(MoveToSelectedPointNo): Point:%d", pointNum);
	//		SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//		//4. 기준 Focus Position 이동
	//		int nRetFocus = g_pAdam->MoveZCapsensorFocusPosition(TRUE);// Interlock은 세팅 안함... TF시 첫번째 scan위치로 이동으로 변경 (최적화)
	//		if (nRetFocus != 0)
	//		{
	//			// Focus 이동 실패
	//			g_pWarning->ShowWindow(SW_HIDE);
	//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! z축 제어 에러 "), 2);
	//			m_bAutoSequenceProcessing = FALSE;
	//			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
	//			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
	//			return -6;
	//		};

	//		// Z축 Stage 위치 세팅
	//		g_pScanStage->GetPosAxesData();
	//		g_pMaskMap->m_dZFocusPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
	//		g_pConfig->m_dZFocusPos_um = g_pMaskMap->m_dZFocusPos;
	//		g_pConfig->SaveRecoveryData();
	//	}

	//	int TotalThroughFocusNum = m_MaskMapWnd.m_ProcessData.pMeasureList[pointNum].nThroughFocusNo;

	//	for (int throughFocusNum = 0; throughFocusNum < TotalThroughFocusNum; throughFocusNum++)
	//	{
	//		if (m_bAutoSequenceProcessing == TRUE)
	//		{
	//			m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum = throughFocusNum + 1;

	//			strStatus.Format("%d/%d", throughFocusNum + 1, m_MaskMapWnd.m_ProcessData.pMeasureList[pointNum].nThroughFocusNo);
	//			g_pAdam->m_ctrlThroughFocusStatus.SetWindowTextA(strStatus);

	//			strLog.Format("Begin(MoveFocusPosition): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//			//5. Focus Position Z축 이동
	//			if (m_MaskMapWnd.m_ProcessData.pMeasureList[pointNum].nThroughFocusNo > 1)
	//			{
	//				double y;
	//				double delta_z;
	//				int step_z;

	//				if (g_pAdam->m_IsCrossStep)// 임시 적용 ihlee
	//				{
	//					step_z = ((throughFocusNum + 1) / 2) * pow(-1, throughFocusNum)*-1;
	//					delta_z = (m_MaskMapWnd.m_ProcessData.pMeasureList[pointNum].dThroughFocusStep) * step_z;
	//				}
	//				else
	//				{
	//					delta_z = m_dZThroughFocusHeight - m_MaskMapWnd.m_ProcessData.pMeasureList[pointNum].dThroughFocusStep * throughFocusNum;
	//				}

	//				//z축 이동	
	//				g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos + delta_z);
	//				g_pScanStage->GetPosAxesData();
	//				g_pScanStage->UpdateDataScanStage();


	//				//y축 보상
	//				y = refY + delta_z * 0.105 / 1000.0;
	//				g_pNavigationStage->MoveAbsolute(STAGE_Y_AXIS, y);
	//				g_pNavigationStage->WaitInPosition(STAGE_Y_AXIS, y);

	//			}
	//			else
	//			{
	//				// Threough Focus 수행 안함  m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo=0;
	//			}
	//		}

	//		strLog.Format("End(MoveFocusPosition): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//		SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//		/////////////////////
	//		//6. UI 관련 함수			
	//		g_pAdam->CurrentScanNumUiUpdate(); // UI관련 함수로 UI 쓰레드와 충돌남
	//		g_pAdam->InitAdamDlgForScan(); //UI 관련 함수 이동, 수치 업데이트 확인


	//		int TotalScanNum = g_pAdam->m_nEuvImage_ScanNumber;//m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;						
	//		int PointMeasureBufIndex;
	//		vector <int> vecScanBufIndex;
	//		vecScanBufIndex.reserve(TotalScanNum);

	//		if (m_bAutoSequenceProcessing == TRUE)
	//		{
	//			strLog.Format("Begin(MemoryAcquireAndInitForPointMeasure): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//			//7. Point Measure 메모리 요청(메모리 확보 될때까지 기다림)		
	//			g_pAdam->MemoryAcquireAndInitForPointMeasure(TotalScanNum, PointMeasureBufIndex, vecScanBufIndex);

	//			strLog.Format("End(MemoryAcquireAndInitForPointMeasure): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//			strLog.Format("Begin(PointMeasureThread): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//			//8. Point Measure 시작후 바로 리턴		==> 정상종료 알아야함..Que로 변경
	//			PointMeasureThreadQue.emplace_back([=] {g_pAdam->PointMeasureThreadFun(PointMeasureBufIndex, vecScanBufIndex, pointNum, TotalPointNum, TotalScanNum, throughFocusNum, TotalThroughFocusNum, TRUE); });
	//			g_pAdam->InitAdamDlgForScan();
	//			g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);//이거 타이머 안쓰는 방식으로 변경필요 ihlee
	//			//thread PointMeasureThread = thread([=] {g_pAdam->PointMeasureThreadFun(PointMeasureBufIndex, vecScanBufIndex, pointNum, TotalPointNum, TotalScanNum, throughFocusNum, TotalThroughFocusNum); });
	//			//PointMeasureThread.detach();

	//			strLog.Format("End(PointMeasureThread): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);
	//		}

	//		if (m_bAutoSequenceProcessing == TRUE)
	//		{
	//			//while (!g_pAdam->m_bIsScaningOn)
	//			//{		
	//				//	//WaitSec(1);
	//			//}
	//			strLog.Format("Begin(m_bIsScaningOn_Wait): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//			//9. Adam Start Wait
	//			// Adam 시작된거 확인후..스캔 시작해야함..
	//			{
	//				unique_lock<mutex> lock(g_pAdam->mutexAdamStart);
	//				g_pAdam->cvAdamStart.wait(lock, [this] { return g_pAdam->IsAdamStart; });
	//				g_pAdam->IsAdamStart = FALSE; // 있어야 하나? 
	//			}

	//			strLog.Format("Begin(m_bIsScaningOn_Wait): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//			strLog.Format("Begin(Scan): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//			//10. Scan Start

	//			g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);

	//			strLog.Format("End(Scan): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);


	//			strLog.Format("Begin(m_bIsScaningOff_Wait): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//			//11. Scan 종료(Adam에서 데이터 받아짐, Adam Stop. 기다림 삭제 해야함)
	//			while (g_pAdam->m_bIsScaningOn)
	//			{
	//				WaitSec(1);
	//			}

	//			strLog.Format("End(m_bIsScaningOff_Wait): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
	//			SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//		}
	//	}
	//}

	////12. 모든 측정 Point 완료
	//while (!g_pAdam->m_IsMeasureComplete)
	//{
	//	WaitSec(1);
	//}

	////13. 모든 쓰레드 종료
	//for (thread& pointMeasureThread : PointMeasureThreadQue)
	//{
	//	pointMeasureThread.join();
	//}

	//// 14. 종료 프로세스
	//if (m_bAutoSequenceProcessing == FALSE)
	//{
	//	g_pWarning->ShowWindow(SW_HIDE);
	//	MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//}

	//// Process Start 버튼 Enable, Disable 삽입 필요..
	//g_pAdam->AdamUIControl(TRUE);
	//g_pAdam->ADAMAbort();
	//g_pEUVSource->SetMechShutterOpen(FALSE);
	//g_pEUVSource->SetEUVSourceOn(FALSE);
	//m_bAutoSequenceProcessing = FALSE;
	//g_pWarning->ShowWindow(SW_HIDE);
	//MsgBoxAuto.DoModal(_T(" Auto Image 측정이 완료되었습니다! "), 2);
	//m_strStageStatus.Format(_T("Auto Image 측정 완료!"));
	//UpdateData(FALSE);
	//g_pLog->Display(0, _T("CMaskMapDlg::MaskImagingProcess() End!"));

	//g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
	//g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);

	//strLog.Format("End(MaskImagingProcess)");
	//SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	//return 0;
}

//2021.06.16 Euv 없이 Scan Long Run Test 를 하기 위함 (jhkim)
int CMaskMapDlg::NoEuvMaskImagingProcess()
{
	CString strLog;

	strLog.Format("Begin(MaskImagingProcess)");
	SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
	SaveLogFile("AutoRun_LongRunTestLog", strLog);
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::NoEuvMaskImagingProcess() Start!"));

	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("Auto Image 측정 시작!"));
	SaveLogFile("AutoRun_LongRunTestLog", m_strStageStatus);
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " EUV Image 생성 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	g_pAdam->m_bEUVCrossLineDisplay = FALSE;
	g_pAdam->UpdateData(FALSE);



	// Laser Feedback으로 전환
	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	{

		strLog.Format(_T("Lase Feedback 으로 전환!"));
		SaveLogFile("AutoRun_LongRunTestLog", strLog);
		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
		g_pNavigationStage->SetLaserMode();
	}

	CString strStatus;

	for (int i = 0; i < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; i++)
	{
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -5;
		}


		m_MaskMapWnd.m_ProcessData.CurrentMeasureNum = i + 1;
		m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum = 1;

		strStatus.Format("%d/%d", i + 1, m_MaskMapWnd.m_ProcessData.TotalMeasureNum);
		g_pAdam->m_ctrlMeasureStatus.SetWindowTextA(strStatus);

		strStatus.Format("%d/%d", 1, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo);
		g_pAdam->m_ctrlThroughFocusStatus.SetWindowTextA(strStatus);

		// Set FOV info and Update ADAM UI
		g_pAdam->m_nEuvImage_Fov = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dFOVSize*1000.;
		g_pAdam->m_nEuvImage_ScanGrid = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dStepSize;
		g_pAdam->m_nEuvImage_ScanNumber = m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;
		g_pAdam->SetAdamFovInfoUi();

		strLog.Format("Begin(MoveToSelectedPointNo): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
		SaveLogFile("AutoRun_LongRunTestLog", strLog);



		//ORG
		//g_pNavigationStage->MoveToSelectedPointNo(i);0
		// Z축 Stage 위치 세팅

		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		MsgBoxAuto.DoModal(_T(" Z 축 home Moving ! "), 4);
		WaitSec(2);

		g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um);

		strLog.Format("End(MoveToSelectedPointNo): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
		SaveLogFile("AutoRun_LongRunTestLog", strLog);

		double refY = g_pNavigationStage->GetTargetPosmm(STAGE_Y_AXIS);

		//Focus Position으로 이동  21.01.17

		strLog.Format("Begin(MoveZCapsensorFocusPosition): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
		SaveLogFile("AutoRun_LongRunTestLog", strLog);

		//int nRetFocus = g_pAdam->MoveZCapsensorFocusPosition(TRUE);// Interlock은 세팅 안함... TF시 첫번째 scan위치로 이동으로 변경 (최적화)


		// 이미지 저장용 파라미터
		int nRefCap = 0;
		double refCapPos = 0;
		double measureCapPos = 0;
		double deltaFocusPos = 0;

		//int nRetFocus = g_pAdam->MoveZCapsensorFocusPosition(TRUE, FALSE, 5, &nRefCap, &refCapPos);
		
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		MsgBoxAuto.DoModal(_T(" Z 축 home Moving ! "), 4);
		WaitSec(2);

		int nRetFocus = 0;

		if (nRetFocus != 0)
		{
			// Focus 이동 실패
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! z축 제어 에러 "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
			return -6;
		};

		// Z축 Stage 위치 세팅

		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		MsgBoxAuto.DoModal(_T(" Z 축 home Moving ! "), 4);
		WaitSec(2);

		// ORG 
		//g_pScanStage->GetPosAxesData();
		//g_pMaskMap->m_dZFocusPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
		//g_pConfig->m_dZFocusPos_um = g_pMaskMap->m_dZFocusPos;
		//g_pConfig->SaveRecoveryData();

		strLog.Format("End(MoveZCapsensorFocusPosition): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
		SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
		SaveLogFile("AutoRun_LongRunTestLog", strLog);


		// Through Focus
		for (int j = 0; j < m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo; j++)
		{
			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
				g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
				return -7;
			}

			m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum = j + 1;

			strStatus.Format("%d/%d", j + 1, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo);
			g_pAdam->m_ctrlThroughFocusStatus.SetWindowTextA(strStatus);

			if (m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo > 1)
			{

				double y;
				double delta_z;
				int step_z;

				if (g_pAdam->m_IsCrossStep)// 임시 적용 ihlee
				{
					step_z = ((j + 1) / 2) * pow(-1, j)*-1;
					delta_z = (m_MaskMapWnd.m_ProcessData.pMeasureList[i].dThroughFocusStep) * step_z;
				}
				else
				{
					delta_z = m_dZThroughFocusHeight - m_MaskMapWnd.m_ProcessData.pMeasureList[i].dThroughFocusStep * j;
				}

				deltaFocusPos = delta_z;
				measureCapPos = refCapPos + delta_z;
				//z축 이동
				strLog.Format("Begin(Z_axix_Control): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
				SaveLogFile("AutoRun_LongRunTestLog", strLog);

				g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos + delta_z);
				g_pScanStage->GetPosAxesData();
				g_pScanStage->UpdateDataScanStage();

				strLog.Format("End(Z_axix_Control): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
				SaveLogFile("AutoRun_LongRunTestLog", strLog);


				//y축 보상
				strLog.Format("Begin(Y_axis_Correction): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
				SaveLogFile("AutoRun_LongRunTestLog", strLog);

				y = refY + delta_z * 0.105 / 1000.0;
				g_pNavigationStage->MoveAbsolute(STAGE_Y_AXIS, y);
				g_pNavigationStage->WaitInPosition(STAGE_Y_AXIS, y);

				strLog.Format("End(Y_axis_Correction): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
				SaveLogFile("AutoRun_LongRunTestLog", strLog);
			}
			else
			{

				deltaFocusPos = 0;
				measureCapPos = refCapPos;
				//strLog.Format("Begin(Z_axix_Control): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				//SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

				//g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos); //삭제 가능..ihlee
				//g_pScanStage->GetPosAxesData(); //삭제 가능..ihlee
				//g_pScanStage->UpdateDataScanStage(); //삭제 가능..ihlee

				//strLog.Format("End(Z_axix_Control): Point:%d Through_Focus:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
				//SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			}

			/*	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
				{
					g_pNavigationStage->SetEncoderMode();
				}*/

			g_pAdam->ADAMRunStart();

			// Scan
			strLog.Format("Begin(Scan): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			SaveLogFile("AutoRun_LongRunTestLog", strLog);

			g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
			g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);

			strLog.Format("End(Scan): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			SaveLogFile("AutoRun_LongRunTestLog", strLog);

			strLog.Format("Begin(WaitScaningOn): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			while (g_pAdam->m_bIsScaningOn)
			{
				//ProcessMessages();
				//Sleep(10);
				WaitSec(1);
			}
			strLog.Format("End(WaitScaningOn): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			SaveLogFile("AutoRun_LongRunTestLog", strLog);

			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
			//{
			//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			//	g_pNavigationStage->SetLaserMode();
			//}

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
				g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
				return -8;
			}

			//if (g_pAdam->m_nEuvImage_Fov == 1000 || g_pAdam->m_nEuvImage_Fov == 1500 || g_pAdam->m_nEuvImage_Fov == 2000 || g_pAdam->m_nEuvImage_Fov == 3000)

			strLog.Format("Begin(SaveThreadRun): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			if (g_pAdam->m_bIsUseInterpolation)
			{
				g_pAdam->FilteredImageFileSaveByThread(50, m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum, measureCapPos, nRefCap, deltaFocusPos);
			}
			strLog.Format("End(SaveThreadRun): Point:%d Through_Focus:%d", m_MaskMapWnd.m_ProcessData.CurrentMeasureNum, m_MaskMapWnd.m_ProcessData.CurrentThroughFocusNum);
			SaveLogFile("ProcessLogMaskImagingProcess ", strLog);
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
		}
	}

	g_pAdam->ADAMAbort();

	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Auto Image 측정이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("Auto Image 측정 완료!"));
	SaveLogFile("AutoRun_LongRunTestLog", m_strStageStatus);
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::NoEuvMaskImagingProcess() End!"));

	g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
	g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);


	strLog.Format("End(MaskImagingProcess)");
	SaveLogFile("AutoRun_LongRunTestLog", strLog);
	SaveLogFile("ProcessLogMaskImagingProcess ", strLog);

	return 0;
}


int CMaskMapDlg::MaskPhaseProcess()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pXrayCamera == NULL || g_pWarning == NULL || g_pPhase == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}

	if (m_bEUVAlignComplete == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" Mask Align부터 진행해주세요! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::MaskPhaseProcess() Start!"));

	//ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("Phase Auto Process 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " EUV Phase 자동 측정 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	{
		g_pNavigationStage->SetLaserMode();
		WaitSec(1);
	}

	// EUV on 임시
	//if (g_pEUVSource->Is_EUV_On() == FALSE)
	//{
	//	g_pWarning->m_strWarningMessageVal = " EUV On 중입니다. 잠시 기다려 주세요! ";
	//	g_pWarning->UpdateData(FALSE);
	//	g_pEUVSource->SetMechShutterOpen(FALSE);
	//	g_pEUVSource->SetEUVSourceOn(TRUE);
	//	WaitSec(3);
	//	// On이 안되었으면? 
	//}

	g_pWarning->m_strWarningMessageVal = " EUV Phase 자동 측정 중입니다! 잠시 기다려 주세요! ";
	g_pWarning->UpdateData(FALSE);

	//////////////////////////// Reference Frequency  측정 시작 /////////////////////////////	
	m_strStageStatus.Format(_T("Phase Auto Process: Reference Frequency  측정 시작!"));
	UpdateData(FALSE);
	// 0. 카메라 세팅
	g_pXrayCamera->SetMeasure(g_pConfig->m_dMeasureExposureTime);

	// 1. Center Frequnecy 측정 지점  Stage 이동  	
	if (g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stReferenceFrequencyPos_um.X, m_MaskMapWnd.m_ProcessData.m_stReferenceFrequencyPos_um.Y) != XY_NAVISTAGE_OK)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Stage 이동에 실패하였습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -5;
	}
	// 2. Z축 이동 
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE) != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Z축 이동 및 인터락 설정에 실패하였습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -5;
	}
	// 3. Center Frequency 측정 

	if (g_pPhase->SetCenterFrequency() != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Center Frequency 설정에 실패하였습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pScanStage->Move_Origin();

		return -6;
	}
	m_strStageStatus.Format(_T("Phase Auto Process: Reference Frequency  측정 완료!"));
	UpdateData(FALSE);



	//////////////////////////// Phase 측정 시작 ///////////////////////////////////////////
	// 임시 설정값 ihlee
	BOOL isUseFineAlign = TRUE;
	BOOL isUseCoarseAlign = FALSE;;

	int coarseAlignType = COARSE_ALIGN_PAD; //COARSE_ALIGN_PAD, COARSE_ALIGN_LS
	int dir = X_DIRECTION;

	m_strStageStatus.Format(_T("Phase Auto Process: Phase 측정 시작!"));
	UpdateData(FALSE);
	g_pPhase->m_listPhaseResult.ResetContent();


	///////////// 파일 오픈  ////////////////////////
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	CString fullfilename;
	CString filename;
	CString saveDirectory;
	CString filenameDate;

	saveDirectory = _T("C:\\Phase Result");
	filenameDate.Format(_T("_%d%02d%02d_%02d%02d%02d"), year, month, day, hour, minute, second);
	filename = _T("phase_result");
	filename = filename + filenameDate;
	fullfilename = saveDirectory + "\\" + filename + ".txt";

	int i = 0, j = 0;
	for (i = 0; i < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; i++)
	{
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pScanStage->Move_Origin();

			return -6;
		}

		CString strValue;
		double phase_correction = 0;
		double phase_measure = 0;

		double *phase1, *phase2, *phaseDel, *phaseDel1, *phaseDel2, *phaseDelCorrect;
		phase1 = new double[m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo];
		phase2 = new double[m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo];
		phaseDel = new double[m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo];
		phaseDel1 = new double[m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo];
		phaseDel2 = new double[m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo];
		phaseDelCorrect = new double[m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo];

		for (j = 0; j < m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo; j++)
		{

			m_strStageStatus.Format(_T("Phase Auto Process: Point %d/%d: Repeat: %d/%d 측정 시작"), m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.TotalMeasureNum, j + 1, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo);
			UpdateData(FALSE);

			//4. 1st Point 

			//4.1 이동
			g_pNavigationStage->MoveToSelectedPointNo(i, FIRSRT_POINT);
			WaitSec(1);


			//여기 수정 필요
			//g_pNavigationStage->ConvertToStageFromMask(g_pPhase->m_MeasPt1PosX_mm, g_pPhase->m_MeasPt1PosY_mm, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um / 1000.0, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um  / 1000.0);

			g_pPhase->m_MeasPt1PosX_mm = g_pNavigationStage->GetCurrentPosX();
			g_pPhase->m_MeasPt1PosY_mm = g_pNavigationStage->GetCurrentPosY();
			g_pPhase->UpdateData(FALSE);

			//4.2 j==0 일떄 Algin 수행
			if (j == 0)
			{
				// 카메라 세팅(Align) 
				if (isUseCoarseAlign || isUseFineAlign)
					g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);

				//// 2. Z축 이동 생략 가능? 가능..
				//if (g_pAdam->MoveZCapsensorFocusPosition(TRUE) != 0)
				//{
				//	g_pWarning->ShowWindow(SW_HIDE);
				//	MsgBoxAuto.DoModal(_T(" Z축 이동 및 인터락 설정에 실패하였습니다! "), 2);
				//	m_bAutoSequenceProcessing = FALSE;
				//	return -5;
				//}

				//4.3. Coarse align
				if (isUseCoarseAlign)
				{
					int nRetCoarseAlign = 0;

					tuple<int, double, int, double, double> coarseReturn;

					coarseReturn = g_pPhase->EuvPhaseCoarseAlign(g_pPhase->m_MeasPt1PosX_mm, g_pPhase->m_MeasPt1PosY_mm, dir, coarseAlignType, m_MaskMapWnd.m_ProcessData.pMeasureList[i].dLsHalfPitch, FALSE);

					nRetCoarseAlign = get<0>(coarseReturn);

					if (nRetCoarseAlign == 0)
					{
						g_pPhase->m_MeasPt1PosX_mm = get<1>(coarseReturn);
						g_pPhase->UpdateData(FALSE);
						g_pNavigationStage->MoveAbsolutePosition(g_pPhase->m_MeasPt1PosX_mm, g_pPhase->m_MeasPt1PosY_mm); //생략 가능
					}
					else
					{
						g_pWarning->ShowWindow(SW_HIDE);
						MsgBoxAuto.DoModal(_T(" Coarse Align에 실패하였습니다! "), 2);
						m_bAutoSequenceProcessing = FALSE;
						delete[] phase1;
						delete[] phase2;
						delete[] phaseDel;
						delete[] phaseDel1;
						delete[] phaseDel2;
						delete[] phaseDelCorrect;

						g_pScanStage->Move_Origin();

						return -6;
					}
				}
				//4.4 Fine align
				if (isUseFineAlign)
				{
					int nRetFineAlign = 0;

					tuple<int, double> fineResult;
					int slope = -1;
					double refAbs = 0;
					double refMl = 260;

					// Fast mode, 결과 포인트로 이동
					fineResult = g_pPhase->EuvPhaseFineAlign(g_pPhase->m_MeasPt1PosX_mm, g_pPhase->m_MeasPt1PosY_mm, dir, slope, refAbs, refMl, TRUE, TRUE);

					nRetFineAlign = get<0>(fineResult);

					if (nRetFineAlign == 0)
					{
						g_pPhase->m_MeasPt1PosX_mm = get<1>(fineResult);
						g_pPhase->UpdateData(FALSE);
						//g_pNavigationStage->MoveAbsolutePosition(g_pPhase->m_MeasPt1PosX_mm, g_pPhase->m_MeasPt1PosY_mm);

					}
					else
					{
						g_pWarning->ShowWindow(SW_HIDE);
						MsgBoxAuto.DoModal(_T(" Fine Align에 실패하였습니다! "), 2);
						m_bAutoSequenceProcessing = FALSE;
						delete[] phase1;
						delete[] phase2;
						delete[] phaseDel;
						delete[] phaseDel1;
						delete[] phaseDel2;
						delete[] phaseDelCorrect;
						g_pScanStage->Move_Origin();

						return -7;
					}
				}

			}

			//4.5 카메라 세팅(Measure)
			g_pXrayCamera->SetMeasure(g_pConfig->m_dMeasureExposureTime);
			//4.6 좌표 변환
			g_pNavigationStage->ConvertToMaskFromStage(g_pPhase->m_MeasPt1PosX_mm, g_pPhase->m_MeasPt1PosY_mm, g_pPhase->m_MeaPt1MaskCoordX, g_pPhase->m_MeaPt1MaskCoordY);
			g_pPhase->UpdateData(FALSE);
			//4.7 측정
			phase1[j] = g_pPhase->PhaseMeasure(g_pPhase->m_MeasPt1PosX_mm, g_pPhase->m_MeasPt1PosY_mm);
			g_pPhase->m_PhasePoint1 = phase1[j];
			g_pPhase->UpdateData(FALSE);

			//5. 2nd Point

			//5.1 이동
			g_pNavigationStage->MoveToSelectedPointNo(i, SECOND_POINT);
			WaitSec(1);

			//g_pNavigationStage->ConvertToStageFromMask(g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].dRightX_um / 1000.0, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].dRightY_um / 1000.0);

			g_pPhase->m_MeasPt2PosX_mm = g_pNavigationStage->GetCurrentPosX();
			g_pPhase->m_MeasPt2PosY_mm = g_pNavigationStage->GetCurrentPosY();
			g_pPhase->UpdateData(FALSE);


			//5.2 j==0 일떄 Algin 수행
			if (j == 0)
			{
				// 카메라 세팅(Align) 
				if (isUseCoarseAlign || isUseFineAlign)
					g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);

				//Z축 이동 생략 가능?
			/*	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE) != 0)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Z축 이동 및 인터락 설정에 실패하였습니다! "), 2);
					m_bAutoSequenceProcessing = FALSE;
					return -5;
				}*/

				//5.3. Coarse align
				if (isUseCoarseAlign)
				{
					int nRetCoarseAlign = 0;

					tuple<int, double, int, double, double> coarseReturn;

					coarseReturn = g_pPhase->EuvPhaseCoarseAlign(g_pPhase->m_MeasPt2PosX_mm, g_pPhase->m_MeasPt2PosY_mm, dir, coarseAlignType, m_MaskMapWnd.m_ProcessData.pMeasureList[i].dLsHalfPitch, FALSE);

					nRetCoarseAlign = get<0>(coarseReturn);

					if (nRetCoarseAlign == 0)
					{
						g_pPhase->m_MeasPt2PosX_mm = get<1>(coarseReturn);
						g_pPhase->UpdateData(FALSE);
						g_pNavigationStage->MoveAbsolutePosition(g_pPhase->m_MeasPt2PosX_mm, g_pPhase->m_MeasPt1PosY_mm); //생략 가능
					}
					else
					{
						g_pWarning->ShowWindow(SW_HIDE);
						MsgBoxAuto.DoModal(_T(" Coarse Align에 실패하였습니다! "), 2);
						m_bAutoSequenceProcessing = FALSE;
						delete[] phase1;
						delete[] phase2;
						delete[] phaseDel;
						delete[] phaseDel1;
						delete[] phaseDel2;
						delete[] phaseDelCorrect;
						g_pScanStage->Move_Origin();
						return -6;
					}
				}

				// 5.4 Fine Align (2nd pt)
				if (isUseFineAlign)
				{
					int nRetFineAlign = 0;

					tuple<int, double> fineResult;
					int slope = 1;
					double refAbs = 0;
					double refMl = 260;

					// Fast mode, 결과 포인트로 이동
					fineResult = g_pPhase->EuvPhaseFineAlign(g_pPhase->m_MeasPt2PosX_mm, g_pPhase->m_MeasPt2PosY_mm, dir, slope, refAbs, refMl, TRUE, TRUE);

					nRetFineAlign = get<0>(fineResult);

					if (nRetFineAlign == 0)
					{
						g_pPhase->m_MeasPt2PosX_mm = get<1>(fineResult);
						g_pPhase->UpdateData(FALSE);
						//g_pNavigationStage->MoveAbsolutePosition(g_pPhase->m_MeasPt2PosX_mm, g_pPhase->m_MeasPt2PosY_mm);
					}
					else
					{
						g_pWarning->ShowWindow(SW_HIDE);
						MsgBoxAuto.DoModal(_T(" Fine Align에 실패하였습니다! "), 2);
						m_bAutoSequenceProcessing = FALSE;
						delete[] phase1;
						delete[] phase2;
						delete[] phaseDel;
						delete[] phaseDel1;
						delete[] phaseDel2;
						delete[] phaseDelCorrect;
						g_pScanStage->Move_Origin();
						return -7;
					}
				}

			}



			//5.5 카메라 세팅(Measure)
			g_pXrayCamera->SetMeasure(g_pConfig->m_dMeasureExposureTime);
			//5.6 좌표 변환
			g_pNavigationStage->ConvertToMaskFromStage(g_pPhase->m_MeasPt2PosX_mm, g_pPhase->m_MeasPt2PosY_mm, g_pPhase->m_MeaPt2MaskCoordX, g_pPhase->m_MeaPt2MaskCoordY);
			g_pPhase->UpdateData(FALSE);

			//5.7 측정
			phase2[j] = g_pPhase->PhaseMeasure(g_pPhase->m_MeasPt2PosX_mm, g_pPhase->m_MeasPt2PosY_mm);
			g_pPhase->m_PhasePoint2 = phase2[j];
			g_pPhase->UpdateData(FALSE);



			//6. 계산
			phaseDel1[j] = g_pPhase->AngleDiffrence(g_pPhase->m_ReferencePhase, phase1[j]);
			phaseDel2[j] = g_pPhase->AngleDiffrence(phase2[j], g_pPhase->m_ReferencePhase);

			phase_measure = (phaseDel1[j] + phaseDel2[j]) / 2.0*180.0 / 3.14159265358979323846;

			phaseDel[j] = phase_measure;
			phase_correction = phase_measure * 13.56 / g_pPhase->m_CenterWaveLengthFromCw;
			phaseDelCorrect[j] = phase_correction;
			g_pPhase->m_PhaseResult = phase_correction;

			strValue.Format("%d \t%d: \t%3.3f \t%3.3f \t%3.3f \t%3.3f \t%3.3f \t%3.3f \t%3.3f", i + 1, j + 1, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j]);
			g_pPhase->m_listPhaseResult.AddString(strValue);
			g_pPhase->UpdateData(FALSE);

			// 결과 저장
			FILE *fp;
			errno_t err;
			err = fopen_s(&fp, fullfilename, "a");
			fprintf(fp, "\t%d \t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f\n", i + 1, j + 1, g_pPhase->m_CenterFreqIndex, g_pConfig->m_dContinuousWave, g_pPhase->m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], g_pPhase->m_MeaPt2MaskCoordX, g_pPhase->m_MeaPt2MaskCoordY);
			fclose(fp);

			//CString strResult;
			//strResult.Format("\t%d \t%d: \t%3.3f \t%3.3f \t%3.3f \t%3.3f \t%3.3f \t%3.3f \t%3.3f \t%.6f \t%.6f", i, j, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], g_pPhase->m_MeaPt1MaskCoordX, g_pPhase->m_MeaPt1MaskCoordY);
			//g_pPhase->SaveLogFile(_T("phase_result_log.txt"), LPSTR(LPCTSTR(strResult)));			

			CString strResult;
			strResult.Format("\t%d \t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f \t%.6f \t%.6f", i + 1, j + 1, g_pPhase->m_CenterFreqIndex, g_pConfig->m_dContinuousWave, g_pPhase->m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], g_pPhase->m_MeaPt2MaskCoordX, g_pPhase->m_MeaPt2MaskCoordY, g_pPhase->m_MeaPt1MaskCoordX, g_pPhase->m_MeaPt1MaskCoordY);
			g_pPhase->SaveLogFile(_T("phase_result_log.txt"), LPSTR(LPCTSTR(strResult)));


			m_strStageStatus.Format(_T("Phase Auto Process: Point %d/%d: Repeat: %d/%d 측정 완료"), m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.TotalMeasureNum, j + 1, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo);
			UpdateData(FALSE);

		}


		delete[] phase1;
		delete[] phase2;
		delete[] phaseDel;
		delete[] phaseDel1;
		delete[] phaseDel2;
		delete[] phaseDelCorrect;
	}


	// Z축 origin
	g_pScanStage->Move_Origin();

	m_strStageStatus.Format(_T("Phase Auto Process: Phase 측정 종료!"));
	UpdateData(FALSE);
	g_pPhase->UpdateData(FALSE);
	//////////////////////////// Phase 측정 종료 ///////////////////////////////////////////

	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Auto Phase 측정이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("Phase Auto Process 완료!"));
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskPhaseProcess() End!"));
	return 0;
}

int CMaskMapDlg::PtrProcess()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pNavigationStage == NULL || g_pRecipe == NULL || g_pWarning == NULL || g_pPTR == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Pellicle 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() Start!"));

	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("PTR Auto Process 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " PTR 자동 측정 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;


	// EUV 연결 체크
	if (g_pEUVSource == NULL) return -4;

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return -5;
	}

	if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		return -6;
	}

	//////////////////////////////// 측정 Process Start ////////////////////////////////////////

	////UI reset
	g_pPTR->ClearGrid();
	g_pPTR->ResetPtrDisplay(&m_MaskMapWnd.m_ProcessData);
	//1. EUV On
	////1) EUV shutter close
	g_pEUVSource->SetMechShutterOpen(FALSE);
	WaitSec(1);
	////2) EUV on:  
	if (g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		return -999;
	}
	WaitSec(10);


	int poitIndex = 0;

	for (int IndexCoupon = 0; IndexCoupon < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; IndexCoupon++)
	{
		// Coupon 마다 EUV calibration 수행
		//2. Background 측정
		g_pEUVSource->SetMechShutterOpen(FALSE);
		WaitSec(5);
		// 버튼 정지
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pEUVSource->SetMechShutterOpen(FALSE);
			g_pEUVSource->SetEUVSourceOn(FALSE);
			return -5;
		}
		////3) Background 측정위치 이동
		g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
		////4) 측정	
		g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;
		g_pPTR->ReadSensor();

		g_pAdam->mD1BackGround = g_pAdam->mD1;
		g_pAdam->mD2BackGround = g_pAdam->mD2;
		g_pAdam->mD3BackGround = g_pAdam->mD3;

		g_pAdam->mD1BackGraoundStd = g_pAdam->mD1Std;
		g_pAdam->mD2BackGraoundStd = g_pAdam->mD2Std;
		g_pAdam->mD3BackGraoundStd = g_pAdam->mD3Std;

		g_pPTR->SensorGridUpdate();
		g_pPTR->UpdateData(FALSE);


		// 버튼 정지
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pEUVSource->SetMechShutterOpen(FALSE);
			g_pEUVSource->SetEUVSourceOn(FALSE);
			return -5;
		}

		//3. Through Pellicle 측정
		////1) Through Pellicle 측정위치 이동
		g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
		////2) EUV shutter open
		g_pEUVSource->SetMechShutterOpen(TRUE);
		WaitSec(5);
		////3) 측정
		g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;
		g_pPTR->ReadSensor();

		g_pAdam->mD1WithoutPellicle = g_pAdam->mD1;
		g_pAdam->mD2WithoutPellicle = g_pAdam->mD2;
		g_pAdam->mD3WithoutPellicle = g_pAdam->mD3;

		g_pAdam->mD1WithoutPellicleStd = g_pAdam->mD1Std;
		g_pAdam->mD2WithoutPellicleStd = g_pAdam->mD2Std;
		g_pAdam->mD3WithoutPellicleStd = g_pAdam->mD3Std;

		g_pPTR->SensorGridUpdate();
		g_pPTR->UpdateData(FALSE);


		// 결과 화면 선택
		g_pPTR->SelectDisp(IndexCoupon);

		//int couponNum = IndexCoupon;
		int totalPointInCoupon = m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementXNo *m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementYNo;
		int currentPointInCoupon = 0;


		// 이동시 Shutter close
		g_pEUVSource->SetMechShutterOpen(FALSE);
		WaitSec(2);
		g_pNavigationStage->MoveToMaskCenterCoordinate(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodY_um, TRUE); //+ 방향
		g_pEUVSource->SetMechShutterOpen(TRUE);
		WaitSec(5);
		//

		for (int indexY = 0; indexY < m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementYNo; indexY++)
		{
			for (int indexX = 0; indexX < m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementXNo; indexX++)
			{
				// 3. Pellicle 측정
				////1) 측정위치 이동
				g_pNavigationStage->MoveToMaskCenterCoordinate(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodY_um, TRUE); //+ 방향

				for (int repeatIndex = 0; repeatIndex < m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nRepeatNo; repeatIndex++)
				{
					m_strStageStatus.Format(_T("PTR Auto Process: Coupon: %d  Point: %d/%d 측정 시작"), IndexCoupon + 1, currentPointInCoupon + 1, totalPointInCoupon);

					UpdateData(FALSE);
					////2) EUV shutter open
					//g_pEUVSource->SetMechShutterOpen(TRUE);
					//WaitSec(1);
					////3) 측정			
					g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].dExposureTime_msec;

					g_pPTR->ReadSensor();

					g_pAdam->mD1WithPellicle = g_pAdam->mD1;
					g_pAdam->mD2WithPellicle = g_pAdam->mD2;
					g_pAdam->mD3WithPellicle = g_pAdam->mD3;

					g_pAdam->mD1WithPellicleStd = g_pAdam->mD1Std;
					g_pAdam->mD2WithPellicleStd = g_pAdam->mD2Std;
					g_pAdam->mD3WithPellicleStd = g_pAdam->mD3Std;

					g_pPTR->SensorGridUpdate();
					g_pPTR->UpdateData(FALSE);
					////4) EUV shutter off
					//g_pEUVSource->SetMechShutterOpen(FALSE);
					////5) 계산
					g_pPTR->CalculatePTR(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodY_um);

					// 6)Display Data updata
					g_pPTR->PushDispData(IndexCoupon, indexX, indexY, g_pAdam->mT);
					g_pPTR->UpdateLut();

					m_strStageStatus.Format(_T("PTR Auto Process: Coupon: %d  Point: %d/%d 측정 완료"), IndexCoupon + 1, currentPointInCoupon + 1, totalPointInCoupon);
					UpdateData(FALSE);
				}

				poitIndex++;
				currentPointInCoupon++;

				// 버튼 정지
				if (m_bAutoSequenceProcessing == FALSE)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					m_bAutoSequenceProcessing = FALSE;
					g_pEUVSource->SetMechShutterOpen(FALSE);
					g_pEUVSource->SetEUVSourceOn(FALSE);
					return -5;
				}

			}
		}

		/*	if (g_pPTR->m_bAutoScaleImage)
			{
				g_pPTR->UpdateLut();
			}		*/
	}

	UpdateData(FALSE);
	g_pPTR->UpdateData(FALSE);

	//////////////////////////// 측정 종료 ///////////////////////////////////////////////////		
	//EUV off
	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Auto PTR 측정이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("PTR Auto Process 완료!"));
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() End!"));

	return 0;
}

//
//int CMaskMapDlg::PTRProcessOld()
//{
//	if (g_pConfig->m_nEquipmentMode == OFFLINE)
//		return -1;
//
//	if (g_pNavigationStage == NULL || g_pRecipe == NULL || g_pWarning == NULL || g_pPTR == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
//		return -2;
//
//	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
//	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
//	{
//		MsgBoxAuto.DoModal(_T(" Pellicle 측정 위치 정보가 없습니다! "), 2);
//		return -3;
//	}
//
//	g_pLog->Display(0, _T("CMaskMapDlg::PTRProcess() Start!"));
//
//	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동
//
//	m_strStageStatus.Format(_T("PTR Auto Process 시작!"));
//	UpdateData(FALSE);
//	g_pWarning->m_strWarningMessageVal = " PTR 자동 측정 중입니다! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//	m_bAutoSequenceProcessing = TRUE;
//
//
//	// EUV 연결 체크
//	if (g_pEUVSource == NULL) return -4;
//
//	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
//	{
//		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
//		return -5;
//	}
//
//	if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
//	{
//		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
//		return -6;
//	}
//
//	//////////////////////////////// 측정 Process Start ////////////////////////////////////////
//
//	////UI reset
//	g_pPTR->ClearGrid();
//
//	g_pPTR->ResetPtrDisplay(&m_MaskMapWnd.m_ProcessData);
//
//
//	//1. Background 측정
//	////1) EUV shutter close
//	g_pEUVSource->SetMechShutterOpen(FALSE);
//	////2) EUV on:  
//	g_pEUVSource->SetEUVSourceOn(TRUE);
//	WaitSec(10);
//
//	// 버튼 정지
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pEUVSource->SetMechShutterOpen(FALSE);
//		g_pEUVSource->SetEUVSourceOn(FALSE);
//		return -5;
//	}
//	////3) Background 측정위치 이동
//	g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
//	////4) 측정	
//	g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;
//	g_pPTR->ReadSensor();
//
//	g_pAdam->mD1BackGround = g_pAdam->mD1;
//	g_pAdam->mD2BackGround = g_pAdam->mD2;
//	g_pAdam->mD3BackGround = g_pAdam->mD3;
//
//	g_pAdam->mD1BackGraoundStd = g_pAdam->mD1Std;
//	g_pAdam->mD2BackGraoundStd = g_pAdam->mD2Std;
//	g_pAdam->mD3BackGraoundStd = g_pAdam->mD3Std;
//
//	g_pPTR->SensorGridUpdate();
//	g_pPTR->UpdateData(FALSE);
//
//
//	// 버튼 정지
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pEUVSource->SetMechShutterOpen(FALSE);
//		g_pEUVSource->SetEUVSourceOn(FALSE);
//		return -5;
//	}
//
//	//2. Through Pellicle 측정
//	////1) Through Pellicle 측정위치 이동
//	g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
//	////2) EUV shutter open
//	g_pEUVSource->SetMechShutterOpen(TRUE);
//	WaitSec(2);
//	////3) 측정
//	g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;
//	g_pPTR->ReadSensor();
//
//	g_pAdam->mD1WithoutPellicle = g_pAdam->mD1;
//	g_pAdam->mD2WithoutPellicle = g_pAdam->mD2;
//	g_pAdam->mD3WithoutPellicle = g_pAdam->mD3;
//
//	g_pAdam->mD1WithoutPellicleStd = g_pAdam->mD1Std;
//	g_pAdam->mD2WithoutPellicleStd = g_pAdam->mD2Std;
//	g_pAdam->mD3WithoutPellicleStd = g_pAdam->mD3Std;
//
//	g_pPTR->SensorGridUpdate();
//	g_pPTR->UpdateData(FALSE);
//
//	////4) EUV shutter close
//	//g_pEUVSource->SetMechShutterOpen(FALSE);	
//	//WaitSec(1);
//
//	int poitIndex = 0;
//
//	for (int IndexCoupon = 0; IndexCoupon < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; IndexCoupon++)
//	{
//		// 결과 화면 선택
//		g_pPTR->SelectDisp(IndexCoupon);
//
//		//int couponNum = IndexCoupon;
//		int totalPointInCoupon = m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementXNo *m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementYNo;
//		int currentPointInCoupon = 0;
//
//		for (int indexY = 0; indexY < m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementYNo; indexY++)
//		{
//			for (int indexX = 0; indexX < m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementXNo; indexX++)
//			{
//				// 3. Pellicle 측정
//				////1) 측정위치 이동
//				g_pNavigationStage->MoveToMaskCenterCoordinate(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodY_um, TRUE); //+ 방향
//
//				for (int repeatIndex = 0; repeatIndex < m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nRepeatNo; repeatIndex++)
//				{
//					m_strStageStatus.Format(_T("PTR Auto Process: Coupon: %d  Point: %d/%d 측정 시작"), IndexCoupon + 1, currentPointInCoupon + 1, totalPointInCoupon);
//
//					UpdateData(FALSE);
//					////2) EUV shutter open
//					//g_pEUVSource->SetMechShutterOpen(TRUE);
//					//WaitSec(1);
//					////3) 측정			
//					g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].dExposureTime_msec;
//
//					g_pPTR->ReadSensor();
//
//					g_pAdam->mD1WithPellicle = g_pAdam->mD1;
//					g_pAdam->mD2WithPellicle = g_pAdam->mD2;
//					g_pAdam->mD3WithPellicle = g_pAdam->mD3;
//
//					g_pAdam->mD1WithPellicleStd = g_pAdam->mD1Std;
//					g_pAdam->mD2WithPellicleStd = g_pAdam->mD2Std;
//					g_pAdam->mD3WithPellicleStd = g_pAdam->mD3Std;
//
//					g_pPTR->SensorGridUpdate();
//					g_pPTR->UpdateData(FALSE);
//					////4) EUV shutter off
//					//g_pEUVSource->SetMechShutterOpen(FALSE);
//					////5) 계산
//					g_pPTR->CalculatePTR(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodY_um);
//
//					// 6)Display Data updata
//					g_pPTR->PushDispData(IndexCoupon, indexX, indexY, g_pAdam->mT);
//
//					m_strStageStatus.Format(_T("PTR Auto Process: Coupon: %d  Point: %d/%d 측정 완료"), IndexCoupon + 1, currentPointInCoupon + 1, totalPointInCoupon);
//					UpdateData(FALSE);
//				}
//
//				poitIndex++;
//				currentPointInCoupon++;
//
//				// 버튼 정지
//				if (m_bAutoSequenceProcessing == FALSE)
//				{
//					g_pWarning->ShowWindow(SW_HIDE);
//					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//					m_bAutoSequenceProcessing = FALSE;
//					g_pEUVSource->SetMechShutterOpen(FALSE);
//					g_pEUVSource->SetEUVSourceOn(FALSE);
//					return -5;
//				}
//
//			}
//		}
//
//
//		if (g_pPTR->m_bAutoScaleImage)
//		{
//			g_pPTR->UpdateLut();
//		}		
//	}	
//
//	UpdateData(FALSE);
//	g_pPTR->UpdateData(FALSE);
//
//	//////////////////////////// 측정 종료 ///////////////////////////////////////////////////		
//	//EUV off
//	g_pEUVSource->SetMechShutterOpen(FALSE);
//	g_pEUVSource->SetEUVSourceOn(FALSE);
//	m_bAutoSequenceProcessing = FALSE;
//	g_pWarning->ShowWindow(SW_HIDE);
//	MsgBoxAuto.DoModal(_T(" Auto PTR 측정이 완료되었습니다! "), 2);
//	m_strStageStatus.Format(_T("PTR Auto Process 완료!"));
//	UpdateData(FALSE);
//	g_pLog->Display(0, _T("CMaskMapDlg::PTRProcess() End!"));
//
//	return 0;
//}

int CMaskMapDlg::PtrScanProcess()
{
	BOOL isSimulation = FALSE;

	if (!isSimulation)
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return -1;
	}

	if (g_pNavigationStage == NULL || g_pRecipe == NULL || g_pWarning == NULL || g_pPTR == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Pellicle 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() Start!"));

	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("PTR Auto Process 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " PTR 자동 측정 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	// EUV 연결 체크
	if (!isSimulation)
	{
		if (g_pEUVSource == NULL) return -4;

		if (g_pEUVSource->Is_SRC_Connected() != TRUE)
		{
			AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
			return -5;
		}

		if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
		{
			AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
			return -6;
		}
	}
	//////////////////////////////// 측정 Process Start ////////////////////////////////////////

	////UI reset
	g_pPTR->ClearGrid();

	//g_pPTR->ResetPtrDisplay(&m_MaskMapWnd.m_ProcessData);  수정필요


	//1. Background 측정
	////1) EUV shutter close
	g_pEUVSource->SetMechShutterOpen(FALSE);
	////2) EUV on:  
	if (g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		return -999;
	}
	if (!isSimulation)
	{
		WaitSec(10);
	}

	// 버튼 정지
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pEUVSource->SetMechShutterOpen(FALSE);
		g_pEUVSource->SetEUVSourceOn(FALSE);
		return -5;
	}
	////3) Background 측정위치 이동
	g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
	////4) 측정	
	g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;

	if (!isSimulation)
	{
		g_pPTR->ReadSensor();
	}

	g_pAdam->mD1BackGround = g_pAdam->mD1;
	g_pAdam->mD2BackGround = g_pAdam->mD2;
	g_pAdam->mD3BackGround = g_pAdam->mD3;

	g_pAdam->mD1BackGraoundStd = g_pAdam->mD1Std;
	g_pAdam->mD2BackGraoundStd = g_pAdam->mD2Std;
	g_pAdam->mD3BackGraoundStd = g_pAdam->mD3Std;

	g_pPTR->SensorGridUpdate();
	g_pPTR->UpdateData(FALSE);


	// 버튼 정지
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pEUVSource->SetMechShutterOpen(FALSE);
		g_pEUVSource->SetEUVSourceOn(FALSE);
		return -5;
	}

	//2. Through Pellicle 측정
	////1) Through Pellicle 측정위치 이동
	g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
	////2) EUV shutter open
	g_pEUVSource->SetMechShutterOpen(TRUE);
	WaitSec(2);
	////3) 측정
	g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;
	if (!isSimulation)
	{
		g_pPTR->ReadSensor();
	}

	g_pAdam->mD1WithoutPellicle = g_pAdam->mD1;
	g_pAdam->mD2WithoutPellicle = g_pAdam->mD2;
	g_pAdam->mD3WithoutPellicle = g_pAdam->mD3;

	g_pAdam->mD1WithoutPellicleStd = g_pAdam->mD1Std;
	g_pAdam->mD2WithoutPellicleStd = g_pAdam->mD2Std;
	g_pAdam->mD3WithoutPellicleStd = g_pAdam->mD3Std;

	g_pPTR->SensorGridUpdate();
	g_pPTR->UpdateData(FALSE);

	////4) EUV shutter close
	//g_pEUVSource->SetMechShutterOpen(FALSE);	
	//WaitSec(1);

	int LineNum = g_pPTR->m_NumOfYGrid;
	double x_Margin_um = 100;
	int x_GridMargin = x_Margin_um / g_pPTR->m_XInterval_um;
	int PointNum = g_pPTR->m_NumOfXGrid + x_GridMargin;


	g_pPTR->m_NumOfXGridwithMargin = PointNum;
	g_pPTR->m_NumOfYGridwithMargin = LineNum;

	g_pPTR->AllocForScan(g_pPTR->m_NumOfXGridwithMargin, g_pPTR->m_NumOfYGridwithMargin);


	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() Scan START!"));

	//g_pPTR->SelectDisp(IndexCoupon);   수정필요
	for (int IndexLine = 0; IndexLine < LineNum; IndexLine++)
	{
		////1) 측정위치 이동
		g_pNavigationStage->MoveToMaskCenterCoordinate(g_pPTR->m_XStart_um - x_Margin_um, g_pPTR->m_YStart_um + IndexLine * g_pPTR->m_YInterval_um, TRUE); //+ 방향
		////2) ScanStart, AdamStart
		m_strStageStatus.Format(_T("PTR Auto Process:  Scanline: %d/%d 측정 시작"), IndexLine + 1, LineNum);
		UpdateData(FALSE);

		//3)Stage Start
		double xpos_tartget, ypos_target;
		double currentStartX_um, currentStartY_um;

		currentStartX_um = g_pPTR->m_XEnd_um + x_Margin_um;
		currentStartY_um = g_pPTR->m_YStart_um + IndexLine * g_pPTR->m_YInterval_um;

		g_pNavigationStage->MoveToMaskCoordinateWithoutInpositionForPTR(currentStartX_um, currentStartY_um, &xpos_tartget, &ypos_target); //+ 방향

		//4)Adam Set and Start
		g_pPTR->m_nPTRDataNum = PointNum;
		if (!isSimulation)
		{
			g_pPTR->ReadSensor();
		}

		//5)Stage Wait
		g_pNavigationStage->WaitInPosition(STAGE_X_AXIS, xpos_tartget);
		g_pNavigationStage->WaitInPosition(STAGE_Y_AXIS, ypos_target);

		m_MaskMapWnd.Invalidate(FALSE);

		for (int IndexPoint = 0; IndexPoint < PointNum; IndexPoint++)
		{
			//5) 결과 update
			g_pAdam->mD1WithPellicle = g_pAdam->m_dRawImageForDisplay[DETECTOR_INTENSITY_1][IndexPoint];
			g_pAdam->mD2WithPellicle = g_pAdam->m_dRawImageForDisplay[DETECTOR_INTENSITY_2][IndexPoint];
			g_pAdam->mD3WithPellicle = g_pAdam->m_dRawImageForDisplay[DETECTOR_INTENSITY_3][IndexPoint];

			g_pAdam->mD1WithPellicleStd = g_pAdam->mD1Std;
			g_pAdam->mD2WithPellicleStd = g_pAdam->mD2Std;
			g_pAdam->mD3WithPellicleStd = g_pAdam->mD3Std;

			g_pPTR->SensorGridUpdate();

			////6) 계산
			double x = currentStartX_um + g_pPTR->m_XInterval_um;
			double y = currentStartY_um;
			//g_pPTR->CalculatePTR(x, y);

			g_pPTR->CalculatePtrForScan(x, y);
		}


		//g_pPTR->MakeSmallDataForScan(IndexLine);

		g_pPTR->UpdateData(FALSE);

		// 7)Display Data updata
		//g_pPTR->PushDispData(IndexCoupon, currentPointInCoupon, g_pAdam->mT);

		m_strStageStatus.Format(_T("PTR Auto Process:  Scanline: %d/%d 측정 종료"), IndexLine + 1, LineNum);
		UpdateData(FALSE);

		// 버튼 정지
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pEUVSource->SetMechShutterOpen(FALSE);
			g_pEUVSource->SetEUVSourceOn(FALSE);

			g_pPTR->DeleteForScan();
			return -5;
		}
	}

	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() Scan END!"));

	//g_pPTR->MakeSmallDataForScan();

	g_pPTR->SaveFileForScan();
	//if (g_pPTR->m_bAutoScaleImage)
	//{
	//	g_pPTR->UpdateDataMinMaxFromData(IndexCoupon);
	//}
	//g_pPTR->UpdateU16Image(g_pPTR->m_DataMinArray[IndexCoupon], g_pPTR->m_DataMaxArray[IndexCoupon], IndexCoupon);
	//MdispControl(g_pPTR->m_MilDisplay, M_UPDATE, M_NOW);


	UpdateData(FALSE);
	g_pPTR->UpdateData(FALSE);

	//////////////////////////// 측정 종료 ///////////////////////////////////////////////////		
	//EUV off
	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Auto PTR 측정이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("PTR Auto Process 완료!"));
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() End!"));

	g_pPTR->DeleteForScan();
	return 0;
}

int CMaskMapDlg::MaskZInterlock() // 동작확인 필요 ihlee
{

	return 0;

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::MaskZInterlock() Start!"));

	m_strStageStatus.Format(_T("Mask Z Interlock 설정 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = "Mask Z Interlock Setting 중입니다. 잠시 기다려 주세요!";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
	{
		g_pNavigationStage->SetEncoderMode();
		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
		WaitSec(1);
	}

	//1. EUV 영역 마스크 센터에서 Z interlock 및 Focus 위치 Setting////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	double focusposz = 0.0;
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(2);
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0)//asdf
	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= Z_INITIAL_POS_UM) // ihlee 변경
	{
		if (g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].x, g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].y) != XY_NAVISTAGE_OK)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -5;
		}
		CString str;
		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			if (g_pAdam->Command_AverageRunAfterTimeout() != 0)
			{
				return -10;
			}
			//g_pAdam->Command_ADAMSimpleRun();
			//WaitSec(2);
			focusposz = g_pConfig->m_dZdistanceCap2nStage_um - g_pAdam->AdamData.m_dCapsensor2; // 2021.01.14 Z_INITIAL_POS_UM=0 일때만 적용가능...확인 필요, ihlee

			return -100; // 2021.01.14 확인 필요, ihlee // 2021.01.14 Z_INITIAL_POS_UM=0 일때만 적용가능...확인 필요, ihlee 중요...

			str.Format(_T("focusposz = %.3f, AdamData.m_dCapsensor2 = %.3f"), focusposz, g_pAdam->AdamData.m_dCapsensor2);
			SaveLogFile("Zvalue", (LPSTR)(LPCTSTR)str);
			//g_pAdam->Command_ADAMSimpleRun();
			m_dZFocusPos = focusposz;
			if (m_dZFocusPos < 70 || m_dZFocusPos > 300)	//이부분은 추후 어떻게 바꿀지 고민 필요. 척이 바뀔때마다 다를 수 있다.
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Z Interlock 및 Focus 위치 확인 필요. Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				return -6;
			}
			g_pConfig->m_dZFocusPos_um = m_dZFocusPos;
			m_dZInterlockPos = m_dZFocusPos + 20.0;
			g_pConfig->m_dZInterlock_um = m_dZInterlockPos;
			g_pConfig->SaveRecoveryData();
			g_pScanStage->m_dZUpperLimit = m_dZInterlockPos;
			UpdateData(FALSE);
			break;
		case PHASE:
			return -100;
#if FALSE
			m_dZFocusPos = 0;

			if (m_dZFocusPos < g_pConfig->m_dZStageLimitMin_um || m_dZFocusPos > g_pConfig->m_dZStageLimitMax_um)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Z Interlock 및 Focus 위치 확인 필요. Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				return -6;
			}
			g_pConfig->m_dZFocusPos_um = m_dZFocusPos;
			m_dZInterlockPos = m_dZFocusPos + g_pConfig->m_dInterlockMargin_um;
			g_pConfig->m_dZInterlock_um = m_dZInterlockPos;
			g_pConfig->SaveRecoveryData();
			g_pScanStage->m_dZUpperLimit = m_dZInterlockPos;
			UpdateData(FALSE);
#endif
			break;
		default:
			break;
		}
	}

	m_strStageStatus.Format(_T("Mask Z Interlock 설정 완료!"));
	UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Z Interlock Setting 완료! "), 2);
	m_bAutoSequenceProcessing = FALSE;
	return 0;
}
int CMaskMapDlg::MaskAlign_OM_OnlyLongTest(BOOL bManual)
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_OM_Test() Start!"));

	m_strStageStatus.Format(_T("Mask Align(OM) 시작!"));
	SaveLogFile("AutoRun_LongRunTestLog", m_strStageStatus);
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = "Mask Align(OM) 중입니다. 잠시 기다려 주세요!";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bOMAlignComplete = FALSE;
	m_bAutoSequenceProcessing = TRUE;

	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) > Z_INITIAL_POS_UM)
	{
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(2);
	}

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -8;
	}
	WaitSec(1);

	//2. OM 영역이 아니면 OM영역으로 이동
	g_pWarning->m_strWarningMessageVal = "Mask Align 중입니다. 잠시 기다려 주세요!";
	g_pWarning->UpdateData(FALSE);
	ChangeOMEUVSet(EUVTOOM);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	CMessageDlg MsgBox;
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double current_posx_mm = 0.0, current_posy_mm = 0.0;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -9;
	}

	//1. OM RT Align Start
	CString str;
	str.Format(_T("OM_RT Align Start"));
	SaveLogFile("AutoRun_LongRunTestLog", _T((LPSTR)(LPCTSTR)(str)));

	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= Z_INITIAL_POS_UM) // ihlee 변경
	{
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.X / 1000.),
			g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.Y / 1000.));
	}

	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" OM RT Align Sequence가 진행 중 입니다 "), 4);
	g_pWarning->ShowWindow(SW_SHOW);

	m_dOMAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointRT_X_mm = m_dOMAlignPointRT_posx_mm;
	g_pConfig->m_dOMAlignPointRT_Y_mm = m_dOMAlignPointRT_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -10;
	}

	
	str.Format(_T("OM_RT_X = %.6f, OM_RT_Y = %.6f"), m_dOMAlignPointRT_posx_mm, m_dOMAlignPointRT_posy_mm);
	SaveLogFile("Acuuracy", _T((LPSTR)(LPCTSTR)(str)));
	SaveLogFile("AutoRun_LongRunTestLog", _T((LPSTR)(LPCTSTR)(str)));

	//2. OM LT Align Start
	str.Format(_T("OM_LT Align Start"));
	SaveLogFile("AutoRun_LongRunTestLog", _T((LPSTR)(LPCTSTR)(str)));

	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.X / 1000.),
		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.Y / 1000.));

	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" OM LT Align Sequence가 진행 중 입니다  "), 4);
	g_pWarning->ShowWindow(SW_SHOW);

	m_dOMAlignPointLT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointLT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointLT_X_mm = m_dOMAlignPointLT_posx_mm;
	g_pConfig->m_dOMAlignPointLT_Y_mm = m_dOMAlignPointLT_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -11;
	}

	str.Format(_T("OM_LT_X = %.6f, OM_LT_Y = %.6f"), m_dOMAlignPointLT_posx_mm, m_dOMAlignPointLT_posy_mm);
	SaveLogFile("Acuuracy", _T((LPSTR)(LPCTSTR)(str)));
	SaveLogFile("AutoRun_LongRunTestLog", _T((LPSTR)(LPCTSTR)(str)));

	//3. OM LB Align Start
	str.Format(_T("OM_LB Align Start"));
	SaveLogFile("AutoRun_LongRunTestLog", _T((LPSTR)(LPCTSTR)(str)));

	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.X / 1000.),
		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.Y / 1000.));

	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" OM LT Align Sequence가 진행 중 입니다 "), 4);
	g_pWarning->ShowWindow(SW_SHOW);

	m_dOMAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointLB_X_mm = m_dOMAlignPointLB_posx_mm;
	g_pConfig->m_dOMAlignPointLB_Y_mm = m_dOMAlignPointLB_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -12;
	}

	str.Format(_T("OM_LB_X = %.6f, OM_LB_Y = %.6f"), m_dOMAlignPointLB_posx_mm, m_dOMAlignPointLB_posy_mm);
	SaveLogFile("Acuuracy", _T((LPSTR)(LPCTSTR)(str)));
	SaveLogFile("AutoRun_LongRunTestLog", _T((LPSTR)(LPCTSTR)(str)));

	m_bOMAlignComplete = TRUE;
	g_pConfig->m_bOMAlignCompleteFlag = m_bOMAlignComplete;
	g_pConfig->SaveRecoveryData();

	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" OM Align이 완료되었습니다! "), 2);
	m_bAutoSequenceProcessing = FALSE;
	m_strStageStatus.Format(_T("Mask OM Align 완료!"));
	SaveLogFile("AutoRun_LongRunTestLog", _T((LPSTR)(LPCTSTR)(m_strStageStatus)));
	m_MaskMapWnd.Invalidate(FALSE);
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_OM() End!"));
	return 0;

}

int CMaskMapDlg::MaskAlign_OM(BOOL bManual)
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_OM() Start!"));

	m_strStageStatus.Format(_T("Mask Align(OM) 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = "Mask Align(OM) 중입니다. 잠시 기다려 주세요!";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bOMAlignComplete = FALSE;
	m_bAutoSequenceProcessing = TRUE;

	//smchoi: g_pScanStage->Start_Scan() 안에서 진행하도록 변경 20210219
	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
	//{
	//	g_pNavigationStage->SetEncoderMode();
	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
	//	WaitSec(1);
	//}

	//asdf
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) > 0)	//Z=-300에서 안전하므로 Optic 기구 조정전까지 임시로 막아둠 by smchoi
	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) > Z_INITIAL_POS_UM)
	{
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(2);
	}

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -8;
	}
	WaitSec(1);

	//2. OM 영역이 아니면 OM영역으로 이동
	g_pWarning->m_strWarningMessageVal = "Mask Align 중입니다. 잠시 기다려 주세요!";
	g_pWarning->UpdateData(FALSE);
	ChangeOMEUVSet(EUVTOOM);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	CMessageDlg MsgBox;
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double current_posx_mm = 0.0, current_posy_mm = 0.0;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -9;
	}

	//1. OM RT Align Start
	//g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0)//asdf
	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= Z_INITIAL_POS_UM) // ihlee 변경
	{
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.X / 1000.),
			g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.Y / 1000.));
	}

	if (bManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" OM 1st Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -10;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
		g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
		g_pRecipe->WaitSec(0.5);
		g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
		g_pRecipe->WaitSec(0.5);
		g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
	}

	m_dOMAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointRT_X_mm = m_dOMAlignPointRT_posx_mm;
	g_pConfig->m_dOMAlignPointRT_Y_mm = m_dOMAlignPointRT_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -10;
	}

	CString str;
	str.Format(_T("OM_RT_X = %.6f, OM_RT_Y = %.6f"), m_dOMAlignPointRT_posx_mm, m_dOMAlignPointRT_posy_mm);
	SaveLogFile("Acuuracy", _T((LPSTR)(LPCTSTR)(str)));

	//2. OM LT Align Start
	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.X / 1000.),
		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.Y / 1000.));

	if (bManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" OM 2nd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -10;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
		g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
		g_pRecipe->WaitSec(0.5);
		g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
		g_pRecipe->WaitSec(0.5);
		g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
	}

	m_dOMAlignPointLT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointLT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointLT_X_mm = m_dOMAlignPointLT_posx_mm;
	g_pConfig->m_dOMAlignPointLT_Y_mm = m_dOMAlignPointLT_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -11;
	}


	//3. OM LB Align Start
	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.X / 1000.),
		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.Y / 1000.));

	if (bManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" OM 3rd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -10;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
		g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
		g_pRecipe->WaitSec(0.5);
		g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
		g_pRecipe->WaitSec(0.5);
		g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
	}

	m_dOMAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointLB_X_mm = m_dOMAlignPointLB_posx_mm;
	g_pConfig->m_dOMAlignPointLB_Y_mm = m_dOMAlignPointLB_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -12;
	}

	m_bOMAlignComplete = TRUE;
	g_pConfig->m_bOMAlignCompleteFlag = m_bOMAlignComplete;
	g_pConfig->SaveRecoveryData();

	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" OM Align이 완료되었습니다! "), 2);
	m_bAutoSequenceProcessing = FALSE;
	m_strStageStatus.Format(_T("Mask OM Align 완료!"));
	m_MaskMapWnd.Invalidate(FALSE);
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_OM() End!"));
	return 0;
}

int CMaskMapDlg::MaskAlign_EUV_SREM(BOOL bManual)
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_SREM() Start!"));

	CMessageDlg MsgBox;
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double current_posx_mm = 0.0, current_posy_mm = 0.0;

	m_strStageStatus.Format(_T("Mask Align(EUV) 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = "Mask Align(EUV) 중입니다. 잠시 기다려 주세요!";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bOMAlignComplete = FALSE;
	m_bAutoSequenceProcessing = TRUE;

	// 삭제 ihlee
	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
	//{
	//	g_pNavigationStage->SetEncoderMode();
	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
	//	//WaitSec(1);
	//}

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -8;
	}
	WaitSec(1);

	m_bEUVAlignComplete = FALSE;

	g_pCamera->ShowWindow(SW_HIDE);
	g_pRecipe->ShowWindow(SW_HIDE);
	g_pAdam->ShowWindow(SW_SHOW);
	m_MaskMapWnd.m_nMicroscopyType = EUV;
	m_CheckOMAdamCtrl.SetWindowText(_T("To Optic Microscopy"));
	g_pAdam->m_bEUVCrossLineDisplay = TRUE;
	g_pAdam->m_comboFovSelect.SetCurSel(5); // 10000 nm
	g_pAdam->m_nEuvImage_Fov = g_pConfig->m_nEUVAlignFOV; // 10000 nm
	//g_pAdam->m_nEuvImage_Old_Fov = g_pAdam->m_nEuvImage_Fov;
	g_pAdam->m_nEuvImage_ScanGrid = g_pConfig->m_nEUVAlignGrid;
	if (g_pConfig->m_nEUVAlignGrid == 40)
		g_pAdam->m_comboScanGridSelect.SetCurSel(3);
	else if (g_pConfig->m_nEUVAlignGrid == 80)
		g_pAdam->m_comboScanGridSelect.SetCurSel(4);
	g_pAdam->m_comboScanNumber.SetCurSel(0);
	g_pAdam->m_nEuvImage_ScanNumber = 1;

	g_pAdam->m_bIsUseHighFovInterpolationOld = g_pAdam->m_bIsUseHighFovInterpolation;
	g_pAdam->m_bIsUseHighFovInterpolation = FALSE;
	g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
	g_pAdam->Invalidate(FALSE);

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		return -13;
	}

	if (g_pEUVSource->Is_EUV_On() == FALSE)
	{
		g_pWarning->m_strWarningMessageVal = " EUV On 중입니다. 잠시 기다려 주세요! ";
		g_pWarning->UpdateData(FALSE);
		g_pEUVSource->SetMechShutterOpen(TRUE);
		if (g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
		{
			return -999;
		}
	}

	g_pWarning->m_strWarningMessageVal = " Mask Align(EUV) 중입니다. 잠시 기다려 주세요! ";
	g_pWarning->UpdateData(FALSE);


	//4. EUV LB Align Start
	current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE)
	{
		target_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dLBOffsetX_mm + g_pConfig->m_dLB_LaserSwitching_OffsetX_mm;
		target_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dLBOffsetY_mm + g_pConfig->m_dLB_LaserSwitching_OffsetY_mm;
	}
	else
	{
		target_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dLBOffsetX_mm;
		target_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dLBOffsetY_mm;
	}

	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0)//이부분 확인 필요. IHLEE//asdf
	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= Z_INITIAL_POS_UM) // ihlee 변경
	{
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
		WaitSec(1);
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
		//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
		//{
		//	g_pNavigationStage->SetLaserMode();
		//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
		//	WaitSec(1);
		//}
		g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos);
		WaitSec(2);


		//// 하이닉스 마스크용 임시코드: Cap2번으로 Focus Setting //////////////////////////////////////////////////
		//double dDifferencez = 0.0;
		//if (g_pAdam->Command_CapReadTimeout() != 0)
		//{
		//	return -10;
		//}
		////WaitSec(1);
		////dDifferencez = g_pConfig->m_dZCapStageOffset_um - g_pAdam->AdamData.m_dCapsensor2;
		//dDifferencez = g_pConfig->m_dZdistanceCap2nStage_um - g_pAdam->AdamData.m_dCapsensor2;
		////g_pAdam->Command_ADAMSimpleRun();
		//if (g_pScanStage->m_dPIStage_GetPos[Z_AXIS] + dDifferencez < g_pConfig->m_dZInterlock_um + 10)
		//{
		//	m_dZFocusPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS] + dDifferencez;
		//	UpdateData(FALSE);
		//}
		//g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos);
		//WaitSec(2);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// 21.01.17 Z축 제어 변경, 인터락 세팅 안함. 
		int nRetFocus = g_pAdam->MoveZCapsensorFocusPosition(TRUE);// Interlock은 세팅 안함..
		WaitSec(2);
		if (nRetFocus != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다!(Z축 제어 에러) "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
			return -17;
		}
	}

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		return -14;
	}

	WaitSec(1);

	MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
	g_pAdam->ADAMRunStart();
	g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
	g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, 1);
	WaitSec(1);

	while (g_pAdam->m_bIsScaningOn)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
	}
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
		return -15;
	}
	WaitSec(1);
	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	//{
	//	g_pNavigationStage->SetLaserMode();
	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
	//	WaitSec(1);
	//}

	if (bManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" EUV First Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
			MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
			return -16;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pAdam->GetCornerPixel(&g_pAdam->m_nEdgeFindResultX, &g_pAdam->m_nEdgeFindResultY);
		g_pAdam->Invalidate(FALSE);
		if (g_pAdam->m_bEUVEdgeFindSuccess == TRUE)
		{
			int center_pixel_x = (int)(g_pAdam->m_nEdgeFindResultX - g_pAdam->m_nRawImage_PixelWidth / 2);
			int center_pixel_y = (int)(g_pAdam->m_nEdgeFindResultY - g_pAdam->m_nRawImage_PixelHeight / 2);
			double nCenter_DistanceX_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
			double nCenter_DistanceY_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);

			g_pAdam->EUVMoveLineDraw(center_pixel_x, center_pixel_y);

			current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
			target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
			target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;
			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
			//{
			//	g_pNavigationStage->SetLaserMode();
			//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			//	WaitSec(1);
			//}
			g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
			WaitSec(1);
			g_pAdam->m_bEUVEdgeFindSuccess = FALSE;
		}
	}

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
		return -16;
	}
	WaitSec(1);

	m_dEUVAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dEUVAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dEUVAlignPointLB_X_mm = m_dEUVAlignPointLB_posx_mm;
	g_pConfig->m_dEUVAlignPointLB_Y_mm = m_dEUVAlignPointLB_posy_mm;

	//5. EUV LT Align Start

	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
	{
		target_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dLTOffsetX_mm + g_pConfig->m_dLT_LaserSwitching_OffsetX_mm;
		target_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dLTOffsetY_mm + g_pConfig->m_dLT_LaserSwitching_OffsetY_mm;
	}
	else
	{
		target_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dLTOffsetX_mm;
		target_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dLTOffsetY_mm;
	}
	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	//{
	//	g_pNavigationStage->SetLaserMode();
	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
	//	WaitSec(1);
	//}
	g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
	WaitSec(1);
	g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
	WaitSec(1);

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
		return -17;
	}

	WaitSec(1);

	MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
	g_pAdam->ADAMRunStart();
	g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
	g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, 1);
	WaitSec(1);

	while (g_pAdam->m_bIsScaningOn)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
	}
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
		return -18;
	}
	WaitSec(1);
	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	//{
	//	g_pNavigationStage->SetLaserMode();
	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
	//	WaitSec(1);
	//}

	if (bManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" EUV 2nd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
			MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
			return -16;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pAdam->GetCornerPixel(&g_pAdam->m_nEdgeFindResultX, &g_pAdam->m_nEdgeFindResultY);
		g_pAdam->Invalidate(FALSE);
		if (g_pAdam->m_bEUVEdgeFindSuccess == TRUE)
		{
			int center_pixel_x = (int)(g_pAdam->m_nEdgeFindResultX - g_pAdam->m_nRawImage_PixelWidth / 2);
			int center_pixel_y = (int)(g_pAdam->m_nEdgeFindResultY - g_pAdam->m_nRawImage_PixelHeight / 2);
			double nCenter_DistanceX_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
			double nCenter_DistanceY_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);

			g_pAdam->EUVMoveLineDraw(center_pixel_x, center_pixel_y);

			current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
			target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
			target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;
			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
			//{
			//	g_pNavigationStage->SetLaserMode();
			//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			//	WaitSec(1);
			//}
			g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
			WaitSec(1);
			g_pAdam->m_bEUVEdgeFindSuccess = FALSE;
		}
	}

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
		return -19;
	}
	WaitSec(1);

	m_dEUVAlignPointLT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dEUVAlignPointLT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dEUVAlignPointLT_X_mm = m_dEUVAlignPointLT_posx_mm;
	g_pConfig->m_dEUVAlignPointLT_Y_mm = m_dEUVAlignPointLT_posy_mm;

	//6. EUV RT Align Start
	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
	{
		target_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dRTOffsetX_mm + g_pConfig->m_dRT_LaserSwitching_OffsetX_mm;
		target_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dRTOffsetY_mm + g_pConfig->m_dRT_LaserSwitching_OffsetY_mm;
	}
	else
	{
		target_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dRTOffsetX_mm;
		target_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dRTOffsetY_mm;
	}
	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	//{
	//	g_pNavigationStage->SetLaserMode();
	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
	//	WaitSec(1);
	//}
	g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
	WaitSec(1);
	g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
	WaitSec(1);

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_TRUE);
		return -20;
	}

	WaitSec(1);

	MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
	g_pAdam->ADAMRunStart();
	g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
	g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, 1);
	WaitSec(1);

	while (g_pAdam->m_bIsScaningOn)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
	}
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
		return -21;
	}
	WaitSec(1);
	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	//{
	//	g_pNavigationStage->SetLaserMode();
	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
	//	WaitSec(1);
	//}

	if (bManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" EUV 3rd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
			MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
			return -16;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pAdam->GetCornerPixel(&g_pAdam->m_nEdgeFindResultX, &g_pAdam->m_nEdgeFindResultY);
		g_pAdam->Invalidate(FALSE);
		if (g_pAdam->m_bEUVEdgeFindSuccess == TRUE)
		{
			int center_pixel_x = (int)(g_pAdam->m_nEdgeFindResultX - g_pAdam->m_nRawImage_PixelWidth / 2);
			int center_pixel_y = (int)(g_pAdam->m_nEdgeFindResultY - g_pAdam->m_nRawImage_PixelHeight / 2);
			double nCenter_DistanceX_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
			double nCenter_DistanceY_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);

			g_pAdam->EUVMoveLineDraw(center_pixel_x, center_pixel_y);

			current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
			target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
			target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;
			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
			//{
			//	g_pNavigationStage->SetLaserMode();
			//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			//	WaitSec(1);
			//}
			g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
			WaitSec(1);
			g_pAdam->m_bEUVEdgeFindSuccess = FALSE;
		}
	}

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
		return -22;
	}
	WaitSec(1);

	m_dEUVAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dEUVAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dEUVAlignPointRT_X_mm = m_dEUVAlignPointRT_posx_mm;
	g_pConfig->m_dEUVAlignPointRT_Y_mm = m_dEUVAlignPointRT_posy_mm;




	g_pEUVSource->SetMechShutterOpen(FALSE);
	//g_pEUVSource->SetEUVSourceOn(FALSE); //ihlee 임시

	m_bEUVAlignComplete = TRUE;
	g_pConfig->m_bEUVAlignCompleteFlag = m_bEUVAlignComplete;
	g_pConfig->SaveRecoveryData();
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Align이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("Mask Align(EUV) 완료!"));
	m_MaskMapWnd.Invalidate(FALSE);
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_SREM() End!"));

	g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
	g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
	MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);

	return 0;
}

//int CMaskMapDlg::MaskAlign(BOOL bAuto, int nScopeType)
//{
//	if (g_pConfig->m_nEquipmentMode == OFFLINE)
//		return -1;
//
//	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
//		return -2;
//
//	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
//	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
//	{
//		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
//		return -3;
//	}
//	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
//	{
//		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
//		return -4;
//	}
//
//	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign() Start!"));
//
//	m_strStageStatus.Format(_T("Mask Align 시작!"));
//	UpdateData(FALSE);
//	g_pWarning->m_strWarningMessageVal = "Mask Align(Z Interlock Setting) 중입니다. 잠시 기다려 주세요!";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//	m_bOMAlignComplete = FALSE;
//	m_bAutoSequenceProcessing = TRUE;
//
//	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
//	{
//		g_pNavigationStage->SetEncoderMode();
//		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
//		WaitSec(1);
//	}
//
//	//1. EUV 영역 마스크 센터에서 Z interlock 및 Focus 위치 Setting////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	double focusposz = 0.0;
//	g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
//	WaitSec(2);
//	if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0)
//	{
//		if (g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].x, g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].y) != XY_NAVISTAGE_OK)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -5;
//		}
//		CString str;
//		switch (g_pConfig->m_nEquipmentType)
//		{
//		case SREM033:
//			g_pAdam->Command_ADAMSimpleRun();
//			WaitSec(2);
//			focusposz = g_pConfig->m_dZdistanceCap2nStage_um - g_pAdam->AdamData.m_dCapsensor2;
//			str.Format(_T("focusposz = %.3f, AdamData.m_dCapsensor2 = %.3f"), focusposz, g_pAdam->AdamData.m_dCapsensor2);
//			SaveLogFile("Zvalue", (LPSTR)(LPCTSTR)str);
//			g_pAdam->Command_ADAMSimpleRun();
//			m_dZFocusPos = focusposz;
//			if (m_dZFocusPos < 70 || m_dZFocusPos > 200)
//			{
//				g_pWarning->ShowWindow(SW_HIDE);
//				MsgBoxAuto.DoModal(_T(" Z Interlock 및 Focus 위치 확인 필요. Auto Sequence가 중지되었습니다! "), 2);
//				m_bAutoSequenceProcessing = FALSE;
//				return -6;
//			}
//			g_pConfig->m_dZFocusPos_um = m_dZFocusPos;
//			m_dZInterlockPos = m_dZFocusPos + 20.0;
//			g_pConfig->m_dZInterlock_um = m_dZInterlockPos;
//			g_pConfig->SaveRecoveryData();
//			g_pScanStage->m_dZUpperLimit = m_dZInterlockPos;
//			UpdateData(FALSE);
//			break;
//		case PHASE:
//			m_dZFocusPos = 0;
//
//			if (m_dZFocusPos < g_pConfig->m_dZStageLimitMin_um || m_dZFocusPos > g_pConfig->m_dZStageLimitMax_um)
//			{
//				g_pWarning->ShowWindow(SW_HIDE);
//				MsgBoxAuto.DoModal(_T(" Z Interlock 및 Focus 위치 확인 필요. Auto Sequence가 중지되었습니다! "), 2);
//				m_bAutoSequenceProcessing = FALSE;
//				return -6;
//			}
//			g_pConfig->m_dZFocusPos_um = m_dZFocusPos;
//			m_dZInterlockPos = m_dZFocusPos + g_pConfig->m_dInterlockMargin_um;
//			g_pConfig->m_dZInterlock_um = m_dZInterlockPos;
//			g_pConfig->SaveRecoveryData();
//			g_pScanStage->m_dZUpperLimit = m_dZInterlockPos;
//			UpdateData(FALSE);
//			break;
//		default:
//			return -23;
//			break;
//		}
//	}
//	else
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		return -7;
//	}
//	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		return -8;
//	}
//	WaitSec(1);
//
//	//2. OM 영역이 아니면 OM영역으로 이동
//	g_pWarning->m_strWarningMessageVal = "Mask Align 중입니다. 잠시 기다려 주세요!";
//	g_pWarning->UpdateData(FALSE);
//	ChangeOMEUVSet(EUVTOOM);
//	g_pWarning->ShowWindow(SW_SHOW);
//	m_bAutoSequenceProcessing = TRUE;
//
//	double target_posx_mm = 0.0, target_posy_mm = 0.0;
//	double current_posx_mm = 0.0, current_posy_mm = 0.0;
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		return -9;
//	}
//
//	//1. OM RT Align Start
//	g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
//	if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0)
//	{
//		//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[1].x, g_pConfig->m_stStagePos[1].y);	//임시로 사용
//		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.X / 1000.),
//			g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.Y / 1000.));
//	}
//
//	CMessageDlg MsgBox;
//	if (m_bManualAlign)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		if (MsgBox.DoModal(" OM 1st Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
//		{
//			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -10;
//		}
//		g_pWarning->ShowWindow(SW_SHOW);
//	}
//	else
//	{
//		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
//		g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
//		g_pRecipe->WaitSec(0.5);
//		g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
//		g_pRecipe->WaitSec(0.5);
//		g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
//		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
//	}
//
//	m_dOMAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//	m_dOMAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//	g_pConfig->m_dOMAlignPointRT_X_mm = m_dOMAlignPointRT_posx_mm;
//	g_pConfig->m_dOMAlignPointRT_Y_mm = m_dOMAlignPointRT_posy_mm;
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		return -10;
//	}
//
//	CString str;
//	str.Format(_T("OM_RT_X = %.6f, OM_RT_Y = %.6f"), m_dOMAlignPointRT_posx_mm, m_dOMAlignPointRT_posy_mm);
//	SaveLogFile("Acuuracy", _T((LPSTR)(LPCTSTR)(str)));
//
//	//2. OM LT Align Start
//	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[2].x, g_pConfig->m_stStagePos[2].y);	//임시로 사용
//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.X / 1000.),
//		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.Y / 1000.));
//
//	if (m_bManualAlign)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		if (MsgBox.DoModal(" OM 2nd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
//		{
//			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -10;
//		}
//		g_pWarning->ShowWindow(SW_SHOW);
//	}
//	else
//	{
//		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
//		g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
//		g_pRecipe->WaitSec(0.5);
//		g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
//		g_pRecipe->WaitSec(0.5);
//		g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
//		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
//	}
//
//	m_dOMAlignPointLT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//	m_dOMAlignPointLT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//	g_pConfig->m_dOMAlignPointLT_X_mm = m_dOMAlignPointLT_posx_mm;
//	g_pConfig->m_dOMAlignPointLT_Y_mm = m_dOMAlignPointLT_posy_mm;
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		return -11;
//	}
//
//
//	//3. OM LB Align Start
//	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[3].x, g_pConfig->m_stStagePos[3].y);	//임시로 사용
//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.X / 1000.),
//		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.Y / 1000.));
//
//	if (m_bManualAlign)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		if (MsgBox.DoModal(" OM 3rd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
//		{
//			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -10;
//		}
//		g_pWarning->ShowWindow(SW_SHOW);
//	}
//	else
//	{
//		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
//		g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
//		g_pRecipe->WaitSec(0.5);
//		g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
//		g_pRecipe->WaitSec(0.5);
//		g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
//		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
//	}
//
//	m_dOMAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//	m_dOMAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//	g_pConfig->m_dOMAlignPointLB_X_mm = m_dOMAlignPointLB_posx_mm;
//	g_pConfig->m_dOMAlignPointLB_Y_mm = m_dOMAlignPointLB_posy_mm;
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		return -12;
//	}
//
//	m_bOMAlignComplete = TRUE;
//	g_pConfig->m_bOMAlignCompleteFlag = m_bOMAlignComplete;
//	g_pConfig->SaveRecoveryData();
//
//	if (nScopeType == SCOPE_OM4X)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" OM Align이 완료되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		m_strStageStatus.Format(_T("Mask Align 완료!"));
//		UpdateData(FALSE);
//		g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign() OM End!"));
//		return 1;
//	}
//
//	//if (IDYES != AfxMessageBox("EUV ALIGN을 수행 하시겠습니까?", MB_YESNO))
//	//{
//	//	//OM만 align하고 cancel하는 경우 OM에서 구한 Align좌표와 Offset값을 활용해도 대략 근처로 감.
//	//	m_bOMAlignComplete = TRUE;
//	//	g_pWarning->ShowWindow(SW_HIDE);
//	//	MsgBoxAuto.DoModal(_T(" OM Align이 완료되었습니다! "), 2);
//	//	m_bAutoSequenceProcessing = FALSE;
//	//	return -1;
//	//}
//
//
//
//
//
//	switch (g_pConfig->m_nEquipmentType)
//	{
//	case SREM033:
//		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	///////////////////////////////////////////////////// EUV Align Start ////////////////////////////////////////////////////////////////////////
//	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		m_bEUVAlignComplete = FALSE;
//
//		g_pCamera->ShowWindow(SW_HIDE);
//		g_pRecipe->ShowWindow(SW_HIDE);
//		g_pAdam->ShowWindow(SW_SHOW);
//		m_MaskMapWnd.m_nMicroscopyType = EUV;
//		m_CheckOMAdamCtrl.SetWindowText(_T("To Optic Microscopy"));
//		g_pAdam->m_bEUVCrossLineDisplay = TRUE;
//		g_pAdam->m_comboFovSelect.SetCurSel(5);
//		g_pAdam->m_nEuvImage_Fov = g_pConfig->m_nEUVAlignFOV;
//		g_pAdam->m_nEuvImage_Old_Fov = g_pAdam->m_nEuvImage_Fov;
//		g_pAdam->m_comboScanGridSelect.SetCurSel(4);
//		g_pAdam->m_nEuvImage_ScanGrid = g_pConfig->m_nEUVAlignGrid;
//		g_pAdam->m_comboScanNumber.SetCurSel(0);
//		g_pAdam->m_nEuvImage_ScanNumber = 1;
//		g_pAdam->Invalidate(FALSE);
//
//		if (g_pEUVSource->Is_SRC_Connected() != TRUE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -13;
//		}
//
//		if (g_pEUVSource->Is_EUV_On() == FALSE)
//		{
//			g_pWarning->m_strWarningMessageVal = " EUV On 중입니다. 잠시 기다려 주세요! ";
//			g_pWarning->UpdateData(FALSE);
//			g_pEUVSource->SetMechShutterOpen(TRUE);
//			g_pEUVSource->SetEUVSourceOn(TRUE);
//		}
//		g_pWarning->m_strWarningMessageVal = " Mask Align 중입니다. 잠시 기다려 주세요! ";
//		g_pWarning->UpdateData(FALSE);
//
//		//4. EUV LB Align Start
//		current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//		current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE)
//		{
//			target_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dLBOffsetX_mm + g_pConfig->m_dLB_LaserSwitching_OffsetX_mm;
//			target_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dLBOffsetY_mm + g_pConfig->m_dLB_LaserSwitching_OffsetY_mm;
//		}
//		else
//		{
//			target_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dLBOffsetX_mm;
//			target_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dLBOffsetY_mm;
//		}
//
//		if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0)
//		{
//			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
//			WaitSec(1);
//			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
//			if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//			{
//				g_pNavigationStage->SetLaserMode();
//				//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//				WaitSec(1);
//			}
//			g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos);
//			WaitSec(1);
//			//// 하이닉스 마스크용 임시코드: Cap2번으로 Focus Setting //////////////////////////////////////////////////
//			double dDifferencez = 0.0;
//			g_pAdam->Command_ADAMSimpleRun();
//			WaitSec(1);
//			//dDifferencez = g_pConfig->m_dZCapStageOffset_um - g_pAdam->AdamData.m_dCapsensor2;
//			dDifferencez = g_pConfig->m_dZdistanceCap2nStage_um - g_pAdam->AdamData.m_dCapsensor2;
//			g_pAdam->Command_ADAMSimpleRun();
//			if (g_pScanStage->m_dPIStage_GetPos[Z_AXIS] + dDifferencez < g_pConfig->m_dZInterlock_um + 10)
//			{
//				m_dZFocusPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS] + dDifferencez;
//				UpdateData(FALSE);
//			}
//			g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos);
//			WaitSec(1);
//			////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		}
//		//return 0;
//		if (m_bAutoSequenceProcessing == FALSE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -14;
//		}
//		WaitSec(1);
//
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
//		{
//			g_pNavigationStage->SetEncoderMode();
//			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
//			WaitSec(1);
//		}
//		g_pAdam->Command_ADAMStart();
//		WaitSec(0.5);
//		g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
//		g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, 1);
//		WaitSec(1);
//		while (g_pAdam->m_bIsScaningOn)
//		{
//			ProcessMessages();
//			Sleep(10);
//		}
//		if (m_bAutoSequenceProcessing == FALSE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -15;
//		}
//		WaitSec(1);
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//		{
//				g_pNavigationStage->SetLaserMode();
//				//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//			WaitSec(1);
//		}
//
//		if (m_bManualAlign)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			if (MsgBox.DoModal(" EUV First Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
//			{
//				MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
//				m_bAutoSequenceProcessing = FALSE;
//				return -16;
//			}
//			g_pWarning->ShowWindow(SW_SHOW);
//		}
//		else
//		{
//			g_pAdam->GetCornerPixel(&g_pAdam->m_nEdgeFindResultX, &g_pAdam->m_nEdgeFindResultY);
//			g_pAdam->Invalidate(FALSE);
//			if (g_pAdam->m_bEUVEdgeFindSuccess == TRUE)
//			{
//				int center_pixel_x = (int)(g_pAdam->m_nEdgeFindResultX - g_pAdam->m_nRawImage_PixelWidth / 2);
//				int center_pixel_y = (int)(g_pAdam->m_nEdgeFindResultY - g_pAdam->m_nRawImage_PixelHeight / 2);
//				double nCenter_DistanceX_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
//				double nCenter_DistanceY_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);
//
//				current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//				current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//				target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
//				target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;
//				if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//				{
//					g_pNavigationStage->SetLaserMode();
//					//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//					WaitSec(1);
//				}
//				g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//				WaitSec(1);
//				g_pAdam->m_bEUVEdgeFindSuccess = FALSE;
//			}
//		}
//
//		if (m_bAutoSequenceProcessing == FALSE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -16;
//		}
//		WaitSec(1);
//
//		m_dEUVAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//		m_dEUVAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//		g_pConfig->m_dEUVAlignPointLB_X_mm = m_dEUVAlignPointLB_posx_mm;
//		g_pConfig->m_dEUVAlignPointLB_Y_mm = m_dEUVAlignPointLB_posy_mm;
//
//		//5. EUV LT Align Start
//
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
//		{
//			target_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dLTOffsetX_mm + g_pConfig->m_dLT_LaserSwitching_OffsetX_mm;
//			target_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dLTOffsetY_mm + g_pConfig->m_dLT_LaserSwitching_OffsetY_mm;
//		}
//		else
//		{
//			target_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dLTOffsetX_mm;
//			target_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dLTOffsetY_mm;
//		}
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//		{
//			g_pNavigationStage->SetLaserMode();
//			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//			WaitSec(1);
//		}
//		g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//		WaitSec(1);
//		g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//		WaitSec(1);
//
//		if (m_bAutoSequenceProcessing == FALSE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -17;
//		}
//		WaitSec(1);
//
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
//		{
//			g_pNavigationStage->SetEncoderMode();
//			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
//			WaitSec(1);
//		}
//		g_pAdam->Command_ADAMStart();
//		WaitSec(0.5);
//		g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
//		g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, 1);
//		WaitSec(1);
//		while (g_pAdam->m_bIsScaningOn)
//		{
//			ProcessMessages();
//			Sleep(10);
//		}
//		if (m_bAutoSequenceProcessing == FALSE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -18;
//		}
//		WaitSec(1);
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//		{
//			g_pNavigationStage->SetLaserMode();
//			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//			WaitSec(1);
//		}
//
//		if (m_bManualAlign)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			if (MsgBox.DoModal(" EUV 2nd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
//			{
//				MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
//				m_bAutoSequenceProcessing = FALSE;
//				return -16;
//			}
//			g_pWarning->ShowWindow(SW_SHOW);
//		}
//		else
//		{
//			g_pAdam->GetCornerPixel(&g_pAdam->m_nEdgeFindResultX, &g_pAdam->m_nEdgeFindResultY);
//			g_pAdam->Invalidate(FALSE);
//			if (g_pAdam->m_bEUVEdgeFindSuccess == TRUE)
//			{
//				int center_pixel_x = (int)(g_pAdam->m_nEdgeFindResultX - g_pAdam->m_nRawImage_PixelWidth / 2);
//				int center_pixel_y = (int)(g_pAdam->m_nEdgeFindResultY - g_pAdam->m_nRawImage_PixelHeight / 2);
//				double nCenter_DistanceX_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
//				double nCenter_DistanceY_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);
//
//				current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//				current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//				target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
//				target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;
//				if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//				{
//					g_pNavigationStage->SetLaserMode();
//					//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//					WaitSec(1);
//				}
//				g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//				WaitSec(1);
//				g_pAdam->m_bEUVEdgeFindSuccess = FALSE;
//			}
//		}
//
//		if (m_bAutoSequenceProcessing == FALSE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -19;
//		}
//		WaitSec(1);
//
//		m_dEUVAlignPointLT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//		m_dEUVAlignPointLT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//		g_pConfig->m_dEUVAlignPointLT_X_mm = m_dEUVAlignPointLT_posx_mm;
//		g_pConfig->m_dEUVAlignPointLT_Y_mm = m_dEUVAlignPointLT_posy_mm;
//
//		//6. EUV RT Align Start
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
//		{
//			target_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dRTOffsetX_mm + g_pConfig->m_dRT_LaserSwitching_OffsetX_mm;
//			target_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dRTOffsetY_mm + g_pConfig->m_dRT_LaserSwitching_OffsetY_mm;
//		}
//		else
//		{
//			target_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dRTOffsetX_mm;
//			target_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dRTOffsetY_mm;
//		}
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//		{
//			g_pNavigationStage->SetLaserMode();
//			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//			WaitSec(1);
//		}
//		g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//		WaitSec(1);
//		g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//		WaitSec(1);
//
//		if (m_bAutoSequenceProcessing == FALSE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -20;
//		}
//		WaitSec(1);
//
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
//		{
//			g_pNavigationStage->SetEncoderMode();
//			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
//			WaitSec(1);
//		}
//		g_pAdam->Command_ADAMStart();
//		WaitSec(0.5);
//		g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
//		g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, 1);
//		WaitSec(1);
//		while (g_pAdam->m_bIsScaningOn)
//		{
//			ProcessMessages();
//			Sleep(10);
//		}
//		if (m_bAutoSequenceProcessing == FALSE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -21;
//		}
//		WaitSec(1);
//		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//		{
//			g_pNavigationStage->SetLaserMode();
//			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//			WaitSec(1);
//		}
//
//		if (m_bManualAlign)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			if (MsgBox.DoModal(" EUV 3rd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
//			{
//				MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
//				m_bAutoSequenceProcessing = FALSE;
//				return -16;
//			}
//			g_pWarning->ShowWindow(SW_SHOW);
//		}
//		else
//		{
//			g_pAdam->GetCornerPixel(&g_pAdam->m_nEdgeFindResultX, &g_pAdam->m_nEdgeFindResultY);
//			g_pAdam->Invalidate(FALSE);
//			if (g_pAdam->m_bEUVEdgeFindSuccess == TRUE)
//			{
//				int center_pixel_x = (int)(g_pAdam->m_nEdgeFindResultX - g_pAdam->m_nRawImage_PixelWidth / 2);
//				int center_pixel_y = (int)(g_pAdam->m_nEdgeFindResultY - g_pAdam->m_nRawImage_PixelHeight / 2);
//				double nCenter_DistanceX_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
//				double nCenter_DistanceY_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);
//
//				current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//				current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//				target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
//				target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;
//				if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//				{
//					g_pNavigationStage->SetLaserMode();
//					//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//					WaitSec(1);
//				}
//				g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//				WaitSec(1);
//				g_pAdam->m_bEUVEdgeFindSuccess = FALSE;
//			}
//		}
//
//		if (m_bAutoSequenceProcessing == FALSE)
//		{
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			return -22;
//		}
//		WaitSec(1);
//
//		m_dEUVAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//		m_dEUVAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//		g_pConfig->m_dEUVAlignPointRT_X_mm = m_dEUVAlignPointRT_posx_mm;
//		g_pConfig->m_dEUVAlignPointRT_Y_mm = m_dEUVAlignPointRT_posy_mm;
//
//
//		m_bEUVAlignComplete = TRUE;
//		g_pConfig->m_bEUVAlignCompleteFlag = m_bEUVAlignComplete;
//		g_pConfig->SaveRecoveryData();
//		m_bAutoSequenceProcessing = FALSE;
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Mask Align이 완료되었습니다! "), 2);
//		m_strStageStatus.Format(_T("Mask Align 완료!"));
//		UpdateData(FALSE);
//		g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign() End!"));
//
//		return 0;
//
//		break;
//	case PHASE:
//		return MaskAlign_EUV_Phase(bAuto);
//		break;
//	default:
//		return -23;
//		break;
//	}
//
//}


int CMaskMapDlg::MaskAlignPhase(BOOL bAuto, int nScopeType)	//nScopeType: 0(EUV), 1(SCOPE_OM4X), 2(SCOPE_OM100X), 3(SCOPE_ALL)
{
	int nReturn = 0;

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL || g_pXrayCamera == NULL || g_pXrayCameraConfig == NULL || g_pPhase == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
		return -4;
	}

	// Log
	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlignPhase() Start!"));
	// 화면
	m_strStageStatus.Format(_T("Mask Align 시작!"));
	UpdateData(FALSE);

	g_pWarning->m_strWarningMessageVal = "Mask Align(Z Interlock Setting) 중입니다. 잠시 기다려 주세요!";
	g_pWarning->ShowWindow(SW_SHOW);
	m_bOMAlignComplete = FALSE;
	m_bAutoSequenceProcessing = TRUE;

	//Laser Feedback 모드 확인 m_bLaserSwichingModeFlag-> UI 버튼으로 세팅, m_bLaserFeedbackFlag-> 주기적으로 확인
	//OM영역일떄 이렇게 하는게 맞는거? 확인 필요.. 제거 또는  FEEDBACK_LASER로 세팅??
	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
	{
		g_pNavigationStage->SetEncoderMode();
		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
		//WaitSec(1);
	}

	//1. 일단 EUV 영역으로 이동, EUV 영역에서 Z축 Focus 및 인터락 임시세팅
	//(m_dZFocusPos = 0) EUV 영역으로 넘어가는은 제거해도 되나 (ChangeOMEUVSet 때문에 삽입했음)		 
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(2);

	//asdf
	return -1200;
	//아래 확인 필요
	if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	{
		if (g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].x, g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].y) != XY_NAVISTAGE_OK)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -5;
		}
		m_dZFocusPos = Z_INITIAL_POS_UM;



		// 현재 위치에서 약간 위로 인터락 세팅 21.03.23
		g_pConfig->m_dZFocusPos_um = m_dZFocusPos;
		m_dZInterlockPos = m_dZFocusPos + 5;
		g_pConfig->m_dZInterlock_um = m_dZInterlockPos;
		g_pConfig->SaveRecoveryData();
		g_pScanStage->m_dZUpperLimit = m_dZInterlockPos;

		UpdateData(FALSE);
	}
	else
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -7;
	}

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -8;
	}
	WaitSec(1);


	// OM Align
	nReturn = MaskAlignOmPhase();

	if (nReturn != 0)
		return -1;


	if (nScopeType == SCOPE_OM4X)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" OM Align이 완료되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		m_strStageStatus.Format(_T("Mask Align 완료!"));
		UpdateData(FALSE);
		g_pLog->Display(0, _T("CMaskMapDlg::MaskAlignPhase() OM End!"));
		return 1;
	}

	// EUV Align
	nReturn = MaskAlign_EUV_Phase(bAuto);

	if (nReturn != 0)
		return -2;

	return nReturn;
}

int CMaskMapDlg::MaskAlignOmPhase()
{
	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	//2. OM영역으로 이동
	g_pWarning->m_strWarningMessageVal = "Mask Align 중입니다. 잠시 기다려 주세요!";
	g_pWarning->UpdateData(FALSE);
	ChangeOMEUVSet(EUVTOOM); //z축 자동으로 0으로, OMTOEUV시에는 z축 0에서 실행해야함
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double current_posx_mm = 0.0, current_posy_mm = 0.0;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -9;
	}


	//1. OM RT Align Start
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(1);

	//asdf
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM) // ihlee
	{
		//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[1].x, g_pConfig->m_stStagePos[1].y);	//임시로 사용
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.X / 1000.),
			g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentRT_um.Y / 1000.));
	}

	g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
	g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
	g_pRecipe->WaitSec(0.5);
	g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
	g_pRecipe->WaitSec(0.5);
	g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
	g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);

	m_dOMAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointRT_X_mm = m_dOMAlignPointRT_posx_mm;
	g_pConfig->m_dOMAlignPointRT_Y_mm = m_dOMAlignPointRT_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -10;
	}

	CString str;
	str.Format(_T("OM_RT_X = %.6f, OM_RT_Y = %.6f"), m_dOMAlignPointRT_posx_mm, m_dOMAlignPointRT_posy_mm);
	SaveLogFile("Acuuracy", _T((LPSTR)(LPCTSTR)(str)));

	//2. OM LT Align Start
	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[2].x, g_pConfig->m_stStagePos[2].y);	//임시로 사용
	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.X / 1000.),
		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentLT_um.Y / 1000.));

	g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
	g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
	g_pRecipe->WaitSec(0.5);
	g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
	g_pRecipe->WaitSec(0.5);
	g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
	g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);

	m_dOMAlignPointLT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointLT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointLT_X_mm = m_dOMAlignPointLT_posx_mm;
	g_pConfig->m_dOMAlignPointLT_Y_mm = m_dOMAlignPointLT_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -11;
	}


	//3. OM LB Align Start
	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[3].x, g_pConfig->m_stStagePos[3].y);	//임시로 사용
	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.X / 1000.),
		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stReferenceCoord_um.Y / 1000.));

	g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
	g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
	g_pRecipe->WaitSec(0.5);
	g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
	g_pRecipe->WaitSec(0.5);
	g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
	g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);

	m_dOMAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointLB_X_mm = m_dOMAlignPointLB_posx_mm;
	g_pConfig->m_dOMAlignPointLB_Y_mm = m_dOMAlignPointLB_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -12;
	}

	m_bOMAlignComplete = TRUE;
	g_pConfig->m_bOMAlignCompleteFlag = m_bOMAlignComplete;
	g_pConfig->SaveRecoveryData();

}

int CMaskMapDlg::MaskAlign_EUV_Phase(BOOL isManual)
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pXrayCamera == NULL || g_pWarning == NULL || g_pPhase == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_Phase() Start!"));

	CMessageDlg MsgBox;
	m_strStageStatus.Format(_T("EUV Align Process 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " Mask Align(EUV) 중입니다. 잠시 기다려 주세요! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;


	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -8;
	}

	int nRet = 0;
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double current_posx_mm = 0.0, current_posy_mm = 0.0;

	//1. Phase EUV Align Start 	
	m_bEUVAlignComplete = FALSE;
	g_pCamera->ShowWindow(SW_HIDE);
	g_pRecipe->ShowWindow(SW_HIDE);

	g_pXrayCamera->ShowWindow(SW_SHOW);
	g_pPhase->ShowWindow(SW_SHOW);
	m_MaskMapWnd.m_nMicroscopyType = EUV;

	//XRAY_CAMERA_DIALOG, PHSE_DIALOG UI 업데이트
	g_pXrayCamera->UpdateData(FALSE);
	g_pPhase->UpdateData(FALSE);
	g_pXrayCamera->Invalidate(TRUE);
	g_pPhase->Invalidate(TRUE);

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;

		nRet = -1;
		return nRet;
	}

	if (g_pEUVSource->Is_EUV_On() == FALSE)
	{
		g_pWarning->m_strWarningMessageVal = " EUV On 중입니다. 잠시 기다려 주세요! ";
		g_pWarning->UpdateData(FALSE);
		g_pEUVSource->SetMechShutterOpen(FALSE);
		if (g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
			return -999;
		// On이 안되었으면? 
	}
	g_pWarning->m_strWarningMessageVal = " Mask Align(EUV) 중입니다. 잠시 기다려 주세요! ";
	g_pWarning->UpdateData(FALSE);

	// Camera Setting
	g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);


	///////////////////////////////////////////////////////////////////////////////////////////////
	///////// Phase EUV Align Focus Setting ///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////	
	double result_posx_mm;
	double result_posy_mm;

	double search_posX_mm;
	double search_posY_mm;

	int slope;
	double refABS, refML;
	int retValue;
	tuple<int, double, int, double, double> coarseReturn;
	tuple<int, double> fineReturn;

	double OmAlingPosXmm[4], OmAlingPosYmm[4], OffsetXmm[4], OffsetYmm[4], LaserSwitchingOffsetXmm[4], LaserSwitchingOffsetYmm[4];
	int AlignMarkType[4];

	double AlignResultXmm[4];
	double AlignResultYmm[4];

	AlignMarkType[LB] = UP_RIGHT; //┗(up right  
	AlignMarkType[LT] = DOWN_RIGHT; //┏ down right
	AlignMarkType[RT] = DOWN_LEFT; // ┓down left



	OmAlingPosXmm[LB] = m_dOMAlignPointLB_posx_mm;
	OmAlingPosXmm[LT] = m_dOMAlignPointLT_posx_mm;
	OmAlingPosXmm[RT] = m_dOMAlignPointRT_posx_mm;

	OmAlingPosYmm[LB] = m_dOMAlignPointLB_posy_mm;
	OmAlingPosYmm[LT] = m_dOMAlignPointLT_posy_mm;
	OmAlingPosYmm[RT] = m_dOMAlignPointRT_posy_mm;


	//m_dGlobalOffsetX_mm
	//m_dGlobalOffsetX_mm

	//m_dRTOffsetX_mm m_dRTOffsetY_mm

	OffsetXmm[LB] = g_pConfig->m_dGlobalOffsetX_mm;
	OffsetXmm[LT] = g_pConfig->m_dGlobalOffsetX_mm;
	OffsetXmm[RT] = g_pConfig->m_dGlobalOffsetX_mm;

	OffsetYmm[LB] = g_pConfig->m_dGlobalOffsetY_mm;
	OffsetYmm[LT] = g_pConfig->m_dGlobalOffsetY_mm;
	OffsetYmm[RT] = g_pConfig->m_dGlobalOffsetY_mm;

	//OffsetXmm[LB] = g_pConfig->m_dLBOffsetX_mm;
	//OffsetXmm[LT] = g_pConfig->m_dLTOffsetX_mm;
	//OffsetXmm[RT] = g_pConfig->m_dRTOffsetX_mm;

	//OffsetYmm[LB] = g_pConfig->m_dLBOffsetY_mm;
	//OffsetYmm[LT] = g_pConfig->m_dLTOffsetY_mm;
	//OffsetYmm[RT] = g_pConfig->m_dRTOffsetY_mm;


	LaserSwitchingOffsetXmm[LB] = g_pConfig->m_dLB_LaserSwitching_OffsetX_mm;
	LaserSwitchingOffsetXmm[LT] = g_pConfig->m_dLT_LaserSwitching_OffsetX_mm;
	LaserSwitchingOffsetXmm[RT] = g_pConfig->m_dRT_LaserSwitching_OffsetX_mm;

	LaserSwitchingOffsetYmm[LB] = g_pConfig->m_dLB_LaserSwitching_OffsetY_mm;
	LaserSwitchingOffsetYmm[LT] = g_pConfig->m_dLT_LaserSwitching_OffsetY_mm;
	LaserSwitchingOffsetYmm[RT] = g_pConfig->m_dRT_LaserSwitching_OffsetY_mm;



	for (int alignKey = LB; alignKey <= RT; alignKey++)
	{
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -8;
		}

		//1. 이동할 좌표 계산
		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE)
		{
			target_posx_mm = OmAlingPosXmm[alignKey] - OffsetXmm[alignKey] + LaserSwitchingOffsetXmm[alignKey];
			target_posy_mm = OmAlingPosYmm[alignKey] + OffsetYmm[alignKey] + LaserSwitchingOffsetYmm[alignKey];
		}
		else
		{
			target_posx_mm = OmAlingPosXmm[alignKey] - OffsetXmm[alignKey];
			target_posy_mm = OmAlingPosYmm[alignKey] + OffsetYmm[alignKey];

		}

		//2. Z축 이동
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(1);

		if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
			WaitSec(1);
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
			WaitSec(1);
		}
		else
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			nRet = -3;
			return nRet;
		}

		//3. Laser Feedback 전환 (LB에서 한번 실행됨)
		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
		{
			g_pNavigationStage->SetLaserMode();
			WaitSec(1);
		}

		//4. Focus 위치로 이동
		m_strStageStatus.Format(_T("EUV Align Process(LB): Z축 세팅 시작!"));
		UpdateData(FALSE);
		if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &g_pPhase->m_CapSensorNumber, &g_pPhase->m_CapSensorSetValue, &g_pPhase->m_CapSensorGetValue) != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -2;
			return nRet;
		}
		m_strStageStatus.Format(_T("EUV Align Process(LB): Z축 세팅 완료!"));
		g_pPhase->UpdateData(FALSE);
		UpdateData(FALSE);


		//5. Align 수행
		if (isManual)
		{
			//5.1 수동 Align
			g_pWarning->ShowWindow(SW_HIDE);
			if (MsgBox.DoModal(" EUV 1st Align Mark를 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				nRet = -9;
				return nRet;
			}

			AlignResultXmm[alignKey] = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			AlignResultYmm[alignKey] = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

			g_pWarning->ShowWindow(SW_SHOW);
		}
		else
		{
			//5.2 자동 Align
			switch (AlignMarkType[alignKey])
			{
			case DOWN_RIGHT://┏ down right
				search_posX_mm = target_posx_mm - g_pConfig->m_dHorizentalMargin_um / 1000.0;
				search_posY_mm = target_posy_mm + g_pConfig->m_dVerticalMargin_um / 1000.0;
				break;

			case DOWN_LEFT:// ┓down left
				search_posX_mm = target_posx_mm + g_pConfig->m_dHorizentalMargin_um / 1000.0;
				search_posY_mm = target_posy_mm + g_pConfig->m_dVerticalMargin_um / 1000.0;
				break;

			case UP_RIGHT://┗(up right  
				search_posX_mm = target_posx_mm - g_pConfig->m_dHorizentalMargin_um / 1000.0;
				search_posY_mm = target_posy_mm - g_pConfig->m_dVerticalMargin_um / 1000.0;
				break;

			case UP_LEFT:// ┛up left
				search_posX_mm = target_posx_mm + g_pConfig->m_dHorizentalMargin_um / 1000.0;
				search_posY_mm = target_posy_mm - g_pConfig->m_dVerticalMargin_um / 1000.0;
				break;

			default:
				search_posX_mm = target_posx_mm;
				search_posY_mm = target_posy_mm;
				break;
			}

			//5.2.1 X축  Align
			m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Coarse Align  시작!"));
			UpdateData(FALSE);

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				return -8;
			}

			coarseReturn = g_pPhase->EuvPhaseCoarseAlign(target_posx_mm, search_posY_mm, X_DIRECTION, COARSE_ALIGN_PAD);
			retValue = get<0>(coarseReturn);
			result_posx_mm = get<1>(coarseReturn);
			slope = get<2>(coarseReturn);
			refABS = get<3>(coarseReturn);
			refML = get<4>(coarseReturn);

			if (retValue != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;

				nRet = -3;
				return nRet;
			}
			m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Coarse Align  완료!"));
			UpdateData(FALSE);

			m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Fine Align  시작!"));
			UpdateData(FALSE);

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				return -8;
			}

			fineReturn = g_pPhase->EuvPhaseFineAlign(result_posx_mm, search_posY_mm, X_DIRECTION, slope, refABS, refML);
			retValue = get<0>(fineReturn);
			result_posx_mm = get<1>(fineReturn);
			if (retValue != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;

				nRet = -4;
				return nRet;
			}
			m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Fine Align  완료!"));
			UpdateData(FALSE);

			//5.2.2 Y축  Align
			m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Coarse Align  시작!"));
			UpdateData(FALSE);

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				return -8;
			}
			coarseReturn = g_pPhase->EuvPhaseCoarseAlign(search_posX_mm, target_posy_mm, Y_DIRECTION, COARSE_ALIGN_PAD);
			retValue = get<0>(coarseReturn);
			result_posy_mm = get<1>(coarseReturn);
			slope = get<2>(coarseReturn);
			refABS = get<3>(coarseReturn);
			refML = get<4>(coarseReturn);

			if (retValue != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;

				nRet = -5;
				return nRet;
			}

			m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Coarse Align  완료!"));
			UpdateData(FALSE);

			m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Fine Align  시작!"));
			UpdateData(FALSE);

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				return -8;
			}
			fineReturn = g_pPhase->EuvPhaseFineAlign(search_posX_mm, result_posy_mm, Y_DIRECTION, slope, refABS, refML);
			retValue = get<0>(fineReturn);
			result_posy_mm = get<1>(fineReturn);
			if (retValue != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;

				nRet = -6;
				return nRet;
			}
			g_pNavigationStage->MoveAbsolutePosition(result_posx_mm, result_posy_mm);
			WaitSec(1);

			m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Fine Align  완료!"));
			UpdateData(FALSE);
		}

		AlignResultXmm[alignKey] = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		AlignResultYmm[alignKey] = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	}


	//m_dEUVAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	//m_dEUVAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	//g_pConfig->m_dEUVAlignPointRT_X_mm = m_dEUVAlignPointRT_posx_mm;
	//g_pConfig->m_dEUVAlignPointRT_Y_mm = m_dEUVAlignPointRT_posy_mm;


	m_dEUVAlignPointLB_posx_mm = AlignResultXmm[LB];
	m_dEUVAlignPointLB_posy_mm = AlignResultYmm[LB];
	g_pConfig->m_dEUVAlignPointLB_X_mm = m_dEUVAlignPointLB_posx_mm;
	g_pConfig->m_dEUVAlignPointLB_Y_mm = m_dEUVAlignPointLB_posy_mm;

	m_dEUVAlignPointLT_posx_mm = AlignResultXmm[LT];
	m_dEUVAlignPointLT_posy_mm = AlignResultYmm[LT];
	g_pConfig->m_dEUVAlignPointLT_X_mm = m_dEUVAlignPointLT_posx_mm;
	g_pConfig->m_dEUVAlignPointLT_Y_mm = m_dEUVAlignPointLT_posy_mm;

	m_dEUVAlignPointRT_posx_mm = AlignResultXmm[RT];
	m_dEUVAlignPointRT_posy_mm = AlignResultYmm[RT];
	g_pConfig->m_dEUVAlignPointRT_X_mm = m_dEUVAlignPointRT_posx_mm;
	g_pConfig->m_dEUVAlignPointRT_Y_mm = m_dEUVAlignPointRT_posy_mm;

	g_pConfig->SaveRecoveryData();

	m_bEUVAlignComplete = TRUE;
	g_pConfig->m_bEUVAlignCompleteFlag = m_bEUVAlignComplete;

	//g_pEUVSource->SetEUVSourceOn(FALSE);

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Align이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("EUV Align Process 완료!"));
	m_MaskMapWnd.Invalidate(FALSE);
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_Phase() End!"));

	return nRet;
}

#if FALSE
int CMaskMapDlg::MaskAlign_EUV_Phase(BOOL isManual)
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pXrayCamera == NULL || g_pWarning == NULL || g_pPhase == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_Phase() Start!"));

	CMessageDlg MsgBox;
	m_strStageStatus.Format(_T("EUV Align Process 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " Mask Align(EUV) 중입니다. 잠시 기다려 주세요! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;


	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -8;
	}

	int nRet = 0;
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double current_posx_mm = 0.0, current_posy_mm = 0.0;

	//1. Phase EUV Align Start 	
	m_bEUVAlignComplete = FALSE;
	g_pCamera->ShowWindow(SW_HIDE);
	g_pRecipe->ShowWindow(SW_HIDE);

	g_pXrayCamera->ShowWindow(SW_SHOW);
	g_pPhase->ShowWindow(SW_SHOW);
	m_MaskMapWnd.m_nMicroscopyType = EUV;

	//XRAY_CAMERA_DIALOG, PHSE_DIALOG UI 업데이트
	g_pXrayCamera->UpdateData(FALSE);
	g_pPhase->UpdateData(FALSE);
	g_pXrayCamera->Invalidate(TRUE);
	g_pPhase->Invalidate(TRUE);

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;

		nRet = -1;
		return nRet;
	}

	if (g_pEUVSource->Is_EUV_On() == FALSE)
	{
		g_pWarning->m_strWarningMessageVal = " EUV On 중입니다. 잠시 기다려 주세요! ";
		g_pWarning->UpdateData(FALSE);
		g_pEUVSource->SetMechShutterOpen(FALSE);
		g_pEUVSource->SetEUVSourceOn(TRUE);
		// On이 안되었으면? 
	}
	g_pWarning->m_strWarningMessageVal = " Mask Align(EUV) 중입니다. 잠시 기다려 주세요! ";
	g_pWarning->UpdateData(FALSE);

	// Camera Setting
	g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);




	///////////////////////////////////////////////////////////////////////////////////////////////
	///////// Phase EUV Align Focus Setting ///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////	
	double result_posx_mm;
	double result_posy_mm;

	double search_posX_mm;
	double search_posY_mm;

	int slope;
	double refABS, refML;
	int retValue;
	tuple<int, double, int, double, double> coarseReturn;
	tuple<int, double> fineReturn;

	double OmAlingPosXmm[4], OmAlingPosYmm[4], OffsetXmm[4], OffsetYmm[4], LaserSwitchingOffsetXmm[4], LaserSwitchingOffsetYmm[4];
	int AlignMarkType[4];

	AlignMarkType[LB] = UP_RIGHT; //┗(up right  
	AlignMarkType[LT] = DOWN_RIGHT; //┏ down right
	AlignMarkType[RB] = DOWN_LEFT; // ┓down left



	OmAlingPosXmm[LB] = m_dOMAlignPointLB_posx_mm;
	OmAlingPosXmm[LT] = m_dOMAlignPointLT_posx_mm;
	OmAlingPosXmm[RT] = m_dOMAlignPointRT_posx_mm;

	OmAlingPosYmm[LB] = m_dOMAlignPointLB_posy_mm;
	OmAlingPosYmm[LT] = m_dOMAlignPointLT_posy_mm;
	OmAlingPosYmm[RT] = m_dOMAlignPointRT_posy_mm;

	OffsetXmm[LB] = g_pConfig->m_dLBOffsetX_mm;
	OffsetXmm[LT] = g_pConfig->m_dLTOffsetX_mm;
	OffsetXmm[RT] = g_pConfig->m_dRTOffsetX_mm;

	OffsetYmm[LB] = g_pConfig->m_dLBOffsetY_mm;
	OffsetYmm[LT] = g_pConfig->m_dLTOffsetY_mm;
	OffsetYmm[RT] = g_pConfig->m_dRTOffsetY_mm;


	LaserSwitchingOffsetXmm[LB] = g_pConfig->m_dLB_LaserSwitching_OffsetX_mm;
	LaserSwitchingOffsetXmm[LT] = g_pConfig->m_dLT_LaserSwitching_OffsetX_mm;
	LaserSwitchingOffsetXmm[RT] = g_pConfig->m_dRT_LaserSwitching_OffsetX_mm;

	LaserSwitchingOffsetYmm[LB] = g_pConfig->m_dLB_LaserSwitching_OffsetY_mm;
	LaserSwitchingOffsetYmm[LT] = g_pConfig->m_dLT_LaserSwitching_OffsetY_mm;
	LaserSwitchingOffsetYmm[RT] = g_pConfig->m_dRT_LaserSwitching_OffsetY_mm;



	for (int alignKey = LB; alignKey <= RT; alignKey++)
	{
		//1. 이동할 좌표 계산
		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE)
		{
			target_posx_mm = OmAlingPosXmm[alignKey] - OffsetXmm[alignKey] + LaserSwitchingOffsetXmm[alignKey];
			target_posy_mm = OmAlingPosYmm[alignKey] + OffsetYmm[alignKey] + LaserSwitchingOffsetYmm[alignKey];
		}
		else
		{
			target_posx_mm = OmAlingPosXmm[alignKey] - OffsetXmm[alignKey];
			target_posy_mm = OmAlingPosYmm[alignKey] + OffsetYmm[alignKey];

		}

		//2. Z축 이동
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(1);

		if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
			WaitSec(1);
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
			WaitSec(1);
		}
		else
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			nRet = -3;
			return nRet;
		}

		//3. Laser Feedback 전환 (LB에서 한번 실행됨)
		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
		{
			g_pNavigationStage->SetLaserMode();
			WaitSec(1);
		}

		//4. Focus 위치로 이동
		m_strStageStatus.Format(_T("EUV Align Process(LB): Z축 세팅 시작!"));
		UpdateData(FALSE);
		if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &g_pPhase->m_CapSensorNumber, &g_pPhase->m_CapSensorSetValue, &g_pPhase->m_CapSensorGetValue) != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -2;
			return nRet;
		}
		m_strStageStatus.Format(_T("EUV Align Process(LB): Z축 세팅 완료!"));
		g_pPhase->UpdateData(FALSE);
		UpdateData(FALSE);


		//5. Align 수행
		if (isManual)
		{
			//5.1 수동 Align
			g_pWarning->ShowWindow(SW_HIDE);
			if (MsgBox.DoModal(" EUV 1st Align Mark를 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				nRet = -9;
				return nRet;
			}
			g_pWarning->ShowWindow(SW_SHOW);
		}
		else
		{
			//5.2 자동 Align
			switch (AlignMarkType[alignKey])
			{
			case DOWN_RIGHT://┏ down right
				search_posX_mm = target_posx_mm - g_pConfig->m_dHorizentalMargin_um / 1000.0;
				search_posY_mm = target_posy_mm + g_pConfig->m_dVerticalMargin_um / 1000.0;
				break;

			case DOWN_LEFT:// ┓down left
				search_posX_mm = target_posx_mm + g_pConfig->m_dHorizentalMargin_um / 1000.0;
				search_posY_mm = target_posy_mm + g_pConfig->m_dVerticalMargin_um / 1000.0;
				break;

			case UP_RIGHT://┗(up right  
				search_posX_mm = target_posx_mm - g_pConfig->m_dHorizentalMargin_um / 1000.0;
				search_posY_mm = target_posy_mm - g_pConfig->m_dVerticalMargin_um / 1000.0;
				break;

			case UP_LEFT:// ┛up left
				search_posX_mm = target_posx_mm + g_pConfig->m_dHorizentalMargin_um / 1000.0;
				search_posY_mm = target_posy_mm - g_pConfig->m_dVerticalMargin_um / 1000.0;
				break;

			default:
				search_posX_mm = target_posx_mm;
				search_posY_mm = target_posy_mm;
				break;
			}

			//5.2.1 X축  Align
			m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Coarse Align  시작!"));
			UpdateData(FALSE);

			coarseReturn = g_pPhase->EuvPhaseCoarseAlign(target_posx_mm, search_posY_mm, X_DIRECTION, COARSE_ALIGN_PAD);
			retValue = get<0>(coarseReturn);
			result_posx_mm = get<1>(coarseReturn);
			slope = get<2>(coarseReturn);
			refABS = get<3>(coarseReturn);
			refML = get<4>(coarseReturn);

			if (retValue != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;

				nRet = -3;
				return nRet;
			}
			m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Coarse Align  완료!"));
			UpdateData(FALSE);

			m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Fine Align  시작!"));
			UpdateData(FALSE);
			fineReturn = g_pPhase->EuvPhaseFineAlign(result_posx_mm, search_posY_mm, X_DIRECTION, slope, refABS, refML);
			retValue = get<0>(fineReturn);
			result_posx_mm = get<1>(fineReturn);
			if (retValue != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;

				nRet = -4;
				return nRet;
			}
			m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Fine Align  완료!"));
			UpdateData(FALSE);

			//5.2.2 Y축  Align
			m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Coarse Align  시작!"));
			UpdateData(FALSE);

			coarseReturn = g_pPhase->EuvPhaseCoarseAlign(search_posX_mm, target_posy_mm, Y_DIRECTION, COARSE_ALIGN_PAD);
			retValue = get<0>(coarseReturn);
			result_posy_mm = get<1>(coarseReturn);
			slope = get<2>(coarseReturn);
			refABS = get<3>(coarseReturn);
			refML = get<4>(coarseReturn);

			if (retValue != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;

				nRet = -5;
				return nRet;
			}

			m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Coarse Align  완료!"));
			UpdateData(FALSE);

			m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Fine Align  시작!"));
			UpdateData(FALSE);

			fineReturn = g_pPhase->EuvPhaseFineAlign(search_posX_mm, result_posy_mm, Y_DIRECTION, slope, refABS, refML);
			retValue = get<0>(fineReturn);
			result_posy_mm = get<1>(fineReturn);
			if (retValue != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;

				nRet = -6;
				return nRet;
			}
			g_pNavigationStage->MoveAbsolutePosition(result_posx_mm, result_posy_mm);
			WaitSec(1);

			m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Fine Align  완료!"));
			UpdateData(FALSE);
		}

		m_dEUVAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		m_dEUVAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		g_pConfig->m_dEUVAlignPointLB_X_mm = m_dEUVAlignPointLB_posx_mm;
		g_pConfig->m_dEUVAlignPointLB_Y_mm = m_dEUVAlignPointLB_posy_mm;

	}





	// 2. EuvPhaseAlign LB
	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE)
	{
		target_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dLBOffsetX_mm + g_pConfig->m_dLB_LaserSwitching_OffsetX_mm;
		target_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dLBOffsetY_mm + g_pConfig->m_dLB_LaserSwitching_OffsetY_mm;
	}
	else
	{
		target_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dLBOffsetX_mm;
		target_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dLBOffsetY_mm;
	}
	//XY move	
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(2);
	//asdf
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
	{
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
		WaitSec(1);
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
		WaitSec(1);
	}
	else
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		nRet = -3;
		return nRet;
	}

	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	{
		g_pNavigationStage->SetLaserMode();
		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
		WaitSec(1);
	}

	//focus 세팅	
	m_strStageStatus.Format(_T("EUV Align Process(LB): Z축 세팅 시작!"));
	UpdateData(FALSE);
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE) != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;

		nRet = -2;
		return nRet;
	}
	m_strStageStatus.Format(_T("EUV Align Process(LB): Z축 세팅 완료!"));
	UpdateData(FALSE);

	if (isManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" EUV 1st Align Mark를 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			nRet = -9;
			return nRet;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		//X	
		m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Coarse Align  시작!"));
		UpdateData(FALSE);

		search_pos = target_posy_mm - g_pConfig->m_dVerticalMargin_um / 1000.0;
		coarseReturn = g_pPhase->EuvPhaseCoarseAlign(target_posx_mm, search_pos, X_DIRECTION, COARSE_ALIGN_PAD);
		retValue = get<0>(coarseReturn);
		result_posx_mm = get<1>(coarseReturn);
		slope = get<2>(coarseReturn);
		refABS = get<3>(coarseReturn);
		refML = get<4>(coarseReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -3;
			return nRet;
		}
		m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Coarse Align  완료!"));
		UpdateData(FALSE);

		m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Fine Align  시작!"));
		UpdateData(FALSE);
		fineReturn = g_pPhase->EuvPhaseFineAlign(result_posx_mm, search_pos, X_DIRECTION, slope, refABS, refML);
		retValue = get<0>(fineReturn);
		result_posx_mm = get<1>(fineReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -4;
			return nRet;
		}
		m_strStageStatus.Format(_T("EUV Align Process(LB): X축 Fine Align  완료!"));
		UpdateData(FALSE);
		//Y
		m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Coarse Align  시작!"));
		UpdateData(FALSE);

		search_pos = target_posx_mm - g_pConfig->m_dHorizentalMargin_um / 1000.0;
		coarseReturn = g_pPhase->EuvPhaseCoarseAlign(search_pos, target_posy_mm, Y_DIRECTION, COARSE_ALIGN_PAD);
		retValue = get<0>(coarseReturn);
		result_posy_mm = get<1>(coarseReturn);
		slope = get<2>(coarseReturn);
		refABS = get<3>(coarseReturn);
		refML = get<4>(coarseReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -5;
			return nRet;
		}
		m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Coarse Align  완료!"));
		UpdateData(FALSE);

		m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Fine Align  시작!"));
		UpdateData(FALSE);

		fineReturn = g_pPhase->EuvPhaseFineAlign(search_pos, result_posy_mm, Y_DIRECTION, slope, refABS, refML);
		retValue = get<0>(fineReturn);
		result_posy_mm = get<1>(fineReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -6;
			return nRet;
		}
		g_pNavigationStage->MoveAbsolutePosition(result_posx_mm, result_posy_mm);
		WaitSec(1);

		m_strStageStatus.Format(_T("EUV Align Process(LB): Y축 Fine Align  완료!"));
		UpdateData(FALSE);
	}

	m_dEUVAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dEUVAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dEUVAlignPointLB_X_mm = m_dEUVAlignPointLB_posx_mm;
	g_pConfig->m_dEUVAlignPointLB_Y_mm = m_dEUVAlignPointLB_posy_mm;


	//3. EuvPhaseAlign LT
	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
	{
		target_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dLTOffsetX_mm + g_pConfig->m_dLT_LaserSwitching_OffsetX_mm;
		target_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dLTOffsetY_mm + g_pConfig->m_dLT_LaserSwitching_OffsetY_mm;
	}
	else
	{
		target_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dLTOffsetX_mm;
		target_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dLTOffsetY_mm;
	}
	//XY move	
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(2);

	//asdf
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM) //ihlee
	{
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
		WaitSec(1);
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
		WaitSec(1);
	}
	else
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		nRet = -3;
		return nRet;
	}

	//focus 세팅
	m_strStageStatus.Format(_T("EUV Align Process(LT): Z축 세팅 완료!"));
	UpdateData(FALSE);
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE) != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;

		nRet = -7;
		return nRet;
	}
	m_strStageStatus.Format(_T("EUV Align Process(LT): Z축 세팅 완료!"));
	UpdateData(FALSE);

	if (isManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" EUV 2nd Align Mark를 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			nRet = -9;
			return nRet;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		//X
		m_strStageStatus.Format(_T("EUV Align Process(LT): X축 Coarse Align  시작!"));
		UpdateData(FALSE);

		search_pos = target_posy_mm - g_pConfig->m_dVerticalMargin_um / 1000.0;
		coarseReturn = g_pPhase->EuvPhaseCoarseAlign(target_posx_mm, search_pos, X_DIRECTION, COARSE_ALIGN_PAD);
		retValue = get<0>(coarseReturn);
		result_posx_mm = get<1>(coarseReturn);
		slope = get<2>(coarseReturn);
		refABS = get<3>(coarseReturn);
		refML = get<4>(coarseReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -8;
			return nRet;
		}

		m_strStageStatus.Format(_T("EUV Align Process(LT): X축 Coarse Align  완료!"));
		UpdateData(FALSE);

		m_strStageStatus.Format(_T("EUV Align Process(LT): X축 Fine Align  시작!"));
		UpdateData(FALSE);

		fineReturn = g_pPhase->EuvPhaseFineAlign(result_posx_mm, search_pos, X_DIRECTION, slope, refABS, refML);
		retValue = get<0>(fineReturn);
		result_posx_mm = get<1>(fineReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -8;
			return nRet;
		}
		m_strStageStatus.Format(_T("EUV Align Process(LT): X축 Fine Align  완료!"));
		UpdateData(FALSE);

		//Y
		m_strStageStatus.Format(_T("EUV Align Process(LT): Y축 Coarse Align  시작!"));
		UpdateData(FALSE);
		search_pos = target_posx_mm - g_pConfig->m_dHorizentalMargin_um / 1000.0;
		coarseReturn = g_pPhase->EuvPhaseCoarseAlign(search_pos, target_posy_mm, Y_DIRECTION, COARSE_ALIGN_PAD);
		retValue = get<0>(coarseReturn);
		result_posy_mm = get<1>(coarseReturn);
		slope = get<2>(coarseReturn);
		refABS = get<3>(coarseReturn);
		refML = get<4>(coarseReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -10;
			return nRet;
		}
		m_strStageStatus.Format(_T("EUV Align Process(LT): Y축 Coarse Align  완료!"));
		UpdateData(FALSE);

		m_strStageStatus.Format(_T("EUV Align Process(LT): Y축 Fine Align  시작!"));
		UpdateData(FALSE);

		fineReturn = g_pPhase->EuvPhaseFineAlign(search_pos, result_posy_mm, Y_DIRECTION, slope, refABS, refML);
		retValue = get<0>(fineReturn);
		result_posy_mm = get<1>(fineReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -11;
			return nRet;
		}

		g_pNavigationStage->MoveAbsolutePosition(result_posx_mm, result_posy_mm);
		WaitSec(1);
		m_strStageStatus.Format(_T("EUV Align Process(LT): Y축 Fine Align  완료!"));
		UpdateData(FALSE);
	}
	m_dEUVAlignPointLT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dEUVAlignPointLT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dEUVAlignPointLT_X_mm = m_dEUVAlignPointLT_posx_mm;
	g_pConfig->m_dEUVAlignPointLT_Y_mm = m_dEUVAlignPointLT_posy_mm;


	//4. EuvPhaseAlign RT
	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
	{
		target_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dRTOffsetX_mm + g_pConfig->m_dRT_LaserSwitching_OffsetX_mm;
		target_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dRTOffsetY_mm + g_pConfig->m_dRT_LaserSwitching_OffsetY_mm;
	}
	else
	{
		target_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dRTOffsetX_mm;
		target_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dRTOffsetY_mm;
	}
	//XY move	
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(2);
	//asdf
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)//ihlee
	{
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
		WaitSec(1);
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
		WaitSec(1);
	}
	else
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		nRet = -3;
		return nRet;
	}

	//focus 세팅
	m_strStageStatus.Format(_T("EUV Align Process(RT): Z축 세팅 완료!"));
	UpdateData(FALSE);
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE) != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;

		nRet = -12;
		return nRet;
	}
	m_strStageStatus.Format(_T("EUV Align Process(RT): Z축 세팅 완료!"));
	UpdateData(FALSE);

	if (isManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" EUV 3rd Align Mark를 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			nRet = -9;
			return nRet;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		//X
		m_strStageStatus.Format(_T("EUV Align Process(RT): X축 Coarse Align  시작!"));
		UpdateData(FALSE);

		search_pos = target_posy_mm - g_pConfig->m_dVerticalMargin_um / 1000.0;
		coarseReturn = g_pPhase->EuvPhaseCoarseAlign(target_posx_mm, search_pos, X_DIRECTION, COARSE_ALIGN_PAD);
		retValue = get<0>(coarseReturn);
		result_posx_mm = get<1>(coarseReturn);
		slope = get<2>(coarseReturn);
		refABS = get<3>(coarseReturn);
		refML = get<4>(coarseReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -13;
			return nRet;
		}

		m_strStageStatus.Format(_T("EUV Align Process(RT): X축 Coarse Align  완료!"));
		UpdateData(FALSE);

		m_strStageStatus.Format(_T("EUV Align Process(RT): X축 Fine Align  시작!"));
		UpdateData(FALSE);

		fineReturn = g_pPhase->EuvPhaseFineAlign(result_posx_mm, search_pos, X_DIRECTION, slope, refABS, refML);
		retValue = get<0>(fineReturn);
		result_posx_mm = get<1>(fineReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -14;
			return nRet;
		}
		m_strStageStatus.Format(_T("EUV Align Process(RT): X축 Fine Align  완료!"));
		UpdateData(FALSE);
		//Y
		m_strStageStatus.Format(_T("EUV Align Process(RT): Y축 Coarse Align  시작!"));
		UpdateData(FALSE);

		search_pos = target_posx_mm - g_pConfig->m_dHorizentalMargin_um / 1000.0;
		coarseReturn = g_pPhase->EuvPhaseCoarseAlign(search_pos, target_posy_mm, Y_DIRECTION, COARSE_ALIGN_PAD);
		retValue = get<0>(coarseReturn);
		result_posy_mm = get<1>(coarseReturn);
		slope = get<2>(coarseReturn);
		refABS = get<3>(coarseReturn);
		refML = get<4>(coarseReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -15;
			return nRet;
		}
		m_strStageStatus.Format(_T("EUV Align Process(RT): Y축 Coarse Align  완료!"));
		UpdateData(FALSE);

		m_strStageStatus.Format(_T("EUV Align Process(RT): Y축 Fine Align  시작!"));
		UpdateData(FALSE);

		fineReturn = g_pPhase->EuvPhaseFineAlign(search_pos, result_posy_mm, Y_DIRECTION, slope, refABS, refML);
		retValue = get<0>(fineReturn);
		result_posy_mm = get<1>(fineReturn);
		if (retValue != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;

			nRet = -16;
			return nRet;
		}

		g_pNavigationStage->MoveAbsolutePosition(result_posx_mm, result_posy_mm);
		WaitSec(1);

		m_strStageStatus.Format(_T("EUV Align Process(RT): Y축 Fine Align  완료!"));
		UpdateData(FALSE);
	}

	m_dEUVAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dEUVAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dEUVAlignPointRT_X_mm = m_dEUVAlignPointRT_posx_mm;
	g_pConfig->m_dEUVAlignPointRT_Y_mm = m_dEUVAlignPointRT_posy_mm;


	m_bEUVAlignComplete = TRUE;
	g_pConfig->m_bEUVAlignCompleteFlag = m_bEUVAlignComplete;
	g_pConfig->SaveRecoveryData();
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Align이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("EUV Align Process 완료!"));
	m_MaskMapWnd.Invalidate(FALSE);
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_Phase() End!"));

	return nRet;

}
#endif

void CMaskMapDlg::OnBnClickedCheckMagnification()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMaskMapDlg::OnBnClickedCheckLaserswichingButton()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckLaserswichingButton() Check Button Click!"));
	UpdateData(TRUE);

	switch (m_CheckLaserSwichingCtrl.GetCheck())
	{
	case BST_CHECKED:
		g_pNavigationStage->SetLaserSwitchingFunction(TRUE);
		//if (g_pNavigationStage->SetLaserSwitchingFunction(TRUE) == XY_NAVISTAGE_OK)
		//	g_pConfig->m_bLaserFeedbackAvailable_Flag = TRUE;
		//m_CheckLaserSwichingCtrl.SetWindowText(_T("Feedback Switching Ready"));
		//g_pNavigationStage->m_bLaserSwichingModeFlag = TRUE;
		break;
	case BST_UNCHECKED:
		g_pNavigationStage->SetLaserSwitchingFunction(FALSE);
		//if (g_pNavigationStage->SetLaserSwitchingFunction(FALSE) == XY_NAVISTAGE_OK)
		//	g_pConfig->m_bLaserFeedbackAvailable_Flag = FALSE;
		//m_CheckLaserSwichingCtrl.SetWindowText(_T("Feedback Switching Not Ready"));
		//g_pNavigationStage->m_bLaserSwichingModeFlag = FALSE;
		break;
	default:
		break;
	}

	//if (g_pConfig != NULL)
	//	g_pConfig->SaveRecoveryData();
}

void CMaskMapDlg::OnBnClickedCheckLaserSwitchOnoff()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckLaserSwitchOnoff() Check Button Click!"));
	UpdateData(TRUE);

	switch (m_CheckSwitchingOnOffCtrl.GetCheck())
	{
	case BST_CHECKED:
		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
			g_pNavigationStage->SetLaserMode();
		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
	//m_CheckSwitchingOnOffCtrl.SetWindowText(_T("Laser Feedback"));
		break;
	case BST_UNCHECKED:
		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
			g_pNavigationStage->SetEncoderMode();
		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
	//m_CheckSwitchingOnOffCtrl.SetWindowText(_T("Encoder Feedback"));
		break;
	default:
		break;
	}
}
//
//int CMaskMapDlg::SetZInterlock()
//{
//	int nRet = 0;
//
//	if (g_pAdam == NULL || g_pScanStage == NULL)
//		return -1;
//
//	//1. 물류동작 및 Auto Sequence 동작 중이 아니고 Mask 유무확인
//
//	//2. Cap Sensor값 읽기 시작
//	if (g_pAdam->Command_CapReadTimeout() != 0)
//	{
//		return -10;
//	}
//
//	//3. Z axis를 0으로 이동
//	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
//
//	//3. EUV 영역 Mask Center로 이동
//
//	//4. Cap Sensor들 4개 값이 정상인지 Check(10um 올려서 4ea Cap의 값이 같이 변화되면 정상)
//
//	//5. 거리 계산해서 Cap1이 312가 되는 위치로 Z 이동(한방에 근처로 가서, 1um씩 올리고, 0.1um씩 올리자)
//	g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos);	//한방에 근처로 이동
//	g_pScanStage->MoveZRelative_SlowInterlock(1, PLUS_DIRECTION);	//1um씩 이동
//	g_pScanStage->MoveZRelative_SlowInterlock(0.1, PLUS_DIRECTION); //0.1um씩 미세조정
//	//for (i = 0; i < (Z_MAX - ccap_val); i++)
//	//{
//	//	if ((pIARS->m_rcv_disp[FCAP2] < pSystemIniData->m_fZInterlockDown) || (pIARS->m_rcv_disp[CCAP2] < -105))
//	//	{
//	//		pLogManager->WaitTime(1);
//	//		double gap = pIARS->m_rcv_disp[FCAP2] - pSystemIniData->m_fZInterlockDown;
//
//	//		if (gap > 0)
//	//		{
//	//			dir = 1;
//	//		}
//	//		else
//	//		{
//	//			gap = gap * -1.0;
//	//			dir = -1;
//	//		}
//	//		strlog_value.Format(_T("Z Interlock Processing.Gap\t%.4f\tDir\t%d"), gap, dir);
//	//		pLogManager->SaveLogFileName("ZInterlock", (char *)(LPCTSTR)strlog_value);
//
//	//		pFineStage->Mov_Stage_Z_Relative(gap, dir);
//	//		Sleep(1000);	//값 바꾸면 쳐박을 수 있음.
//	//		break;
//	//	}
//	//	else
//	//	{
//	//		if (pIARS->m_rcv_disp[FCAP2] < pSystemIniData->m_fZInterlockDown + 2.0)// 설정값하고 1um정도로 가까워지면 0.1씩 올림
//	//			pFineStage->Mov_Stage_Z_Relative(0.1, 1);
//	//		else // 아니면 2씩 올림
//	//			pFineStage->Mov_Stage_Z_Relative(2, 1);
//
//	//		Sleep(1000);
//	//		if ((pIARS->m_rcv_disp[FCAP1] < sensor_int) || pIARS->m_rcv_disp[FCAP4] < sensor_int))	//capsensor 4개중 1개라도 0보다 작으면 이상있음.
//	//		{
//	//			pFineStage->Mov_Stage_Z_Absolute_Interlock(0);
//
//	//			m_str.Format(_T("%d"), Z_ZERO + 10);
//	//			pFineStage->m_z_focus.SetWindowText(m_str);
//	//			m_str.Format(_T("%d"), Z_ZERO);
//	//			pFineStage->m_ZFocus2Ctrl.SetWindowText(m_str);
//	//			pFineStage->m_dZFocusPos = Z_ZERO;
//	//			pSystemIniData->m_dZFocus = Z_ZERO;
//	//			pSystemIniData->m_dZLimit = Z_ZERO + 10;
//	//			pSystemIniData->SaveMaskZInterlockHistory();
//
//	//			bRet = false;
//	//			return bRet;
//	//		}
//	//	}
//	//}
//
//	//6. Cap4개를 이용해 잘 이동했는지 검증
//
//
//	//7.Z Interlock값 설정
//	g_pScanStage->GetPosAxesData();
//	m_dZInterlockPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS] + 20;
//	g_pConfig->m_dZInterlock_um = m_dZInterlockPos;
//	g_pScanStage->m_dZUpperLimit = m_dZInterlockPos;
//	m_dZFocusPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
//	//m_dZFocusPos.Format(_T("%.3f"), g_pScanStage->m_dPIStage_GetPos[Z_AXIS]);
//	UpdateData(FALSE);
//
//	return nRet;
//}


void CMaskMapDlg::OnBnClickedCheckBidirectionScan()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckBidirectionScan() 버튼 클릭!"));

	UpdateData(TRUE);

	switch (m_CheckBidirectionCtrl.GetCheck())
	{
	case BST_CHECKED:
		m_CheckBidirectionCtrl.SetWindowText(_T("BI-DIRECTION SCAN"));
		g_pAdam->m_nScanType = BI_DIRECTION;
		break;
	case BST_UNCHECKED:
		m_CheckBidirectionCtrl.SetWindowText(_T("UNI-DIRECTION SCAN"));
		g_pAdam->m_nScanType = UNI_DIRECTION;
		break;
	default:
		break;
	}
}

void CMaskMapDlg::OnBnClickedCheckLaserFrequency()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckLaserFrequency() 버튼 클릭!"));

	UpdateData(TRUE);

	switch (m_CheckLaserFrequency.GetCheck())
	{
	case BST_CHECKED:
		m_CheckLaserFrequency.SetWindowText(_T("5 Khz"));
		break;
	case BST_UNCHECKED:
		m_CheckLaserFrequency.SetWindowText(_T("1 Khz"));
		break;
	default:
		break;
	}



	//int test = 0;

	//int k = NUMBEROFPACKET - 1;

	//for (int i = 0; i < PACKETSIZE; i++)
	//{
	//	g_pAdam->m_chReceivedbuf[k][i] = i;
	//}

	//// ADAMDATA_SIZE =4
	//for (int i = 0; i < ADAMDATA_SIZE; i++)
	//{
	//	g_pAdam->m_chReceivebuf[0][i] = i * 10;							// 패킷 내부 데이터 사이즈에대한 정의가 필요함
	//}

	//int nRet = g_pAdam->ReceivedData_UniD(g_pAdam->m_chReceivedbuf[k]);
	////while (1)
	////{
	////	ProcessMessages();
	////	Sleep(1);
	////}
	////WaitSec(0.01);
	////WaitSec(0.01);
	//g_pAdam->GetCurrentCapSensorValueWithoutAverage();

	//double m_capRead1 = g_pAdam->m_dCurrentCapsensorValue1;
	//double m_capRead2 = g_pAdam->m_dCurrentCapsensorValue2;
	//double m_capRead3 = g_pAdam->m_dCurrentCapsensorValue3;
	//double m_capRead4 = g_pAdam->m_dCurrentCapsensorValue4;

}

void CMaskMapDlg::OnBnClickedCheckScanOnly()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckOmadam() Check Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
		m_CheckScanOnlyCtrl.SetCheck(FALSE);
		return;
	}

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	CPoint pt;
	pt.x = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	pt.y = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	if (g_pConfig->m_rcEUVStageAreaRect.PtInRect(pt) == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" Stage를 EUV Area로 이동 후 가동해주세요! "), 2);
		m_CheckScanOnlyCtrl.SetCheck(FALSE);
		return;
	}

	switch (m_CheckScanOnlyCtrl.GetCheck())
	{
	case BST_CHECKED:
		g_pScanStage->m_bZHoldOn = TRUE;
		m_CheckScanOnlyCtrl.SetWindowText(_T("Z Hold On"));
		g_pScanStage->SetTimer(SCANSTAGE_ZHOLD_TIMER, 5000, NULL);
		break;
	case BST_UNCHECKED:
		g_pScanStage->m_bZHoldOn = FALSE;
		m_CheckScanOnlyCtrl.SetWindowText(_T("Z Hold Off"));
		g_pScanStage->KillTimer(SCANSTAGE_ZHOLD_TIMER);
		break;
	default:
		break;
	}
}

//g_pNavigationStage->RunBuffer(6);
//double realvalue1 = 0.0, realvalue2 = 0.0;
//realvalue1 = g_pNavigationStage->GetGlobalRealVariable("DOUT_0");
//realvalue2 = g_pNavigationStage->GetGlobalRealVariable("DOUT_1");
//g_pNavigationStage->StopBuffer(6);


void CMaskMapDlg::OnBnClickedCheckOmalignonly()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckOmalignonly() 버튼 클릭!"));

	UpdateData(TRUE);

	switch (m_CheckOMAlignOnlyCtrl.GetCheck())
	{
	case BST_CHECKED:
		m_CheckOMAlignOnlyCtrl.SetWindowText(_T("OM ALIGN"));
		break;
	case BST_UNCHECKED:
		m_CheckOMAlignOnlyCtrl.SetWindowText(_T("OM-EUV ALIGN"));
		break;
	default:
		break;
	}
}


void CMaskMapDlg::OnBnClickedCheckAlignAutomanual()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckAlignAutomanual() 버튼 클릭!"));

	UpdateData(TRUE);

	switch (m_CheckManualAlignCtrl.GetCheck())
	{
	case BST_CHECKED:
		m_CheckManualAlignCtrl.SetWindowText(_T("MANUAL"));
		break;
	case BST_UNCHECKED:
		m_CheckManualAlignCtrl.SetWindowText(_T("AUTO"));
		break;
	default:
		break;
	}
}


void CMaskMapDlg::OnStnClickedTextStageposgrid()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::OnBnClickedCheckOmAdamUi()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckOmAdamUi() 버튼 클릭!"));
	UpdateData(TRUE);

	//	jhkim -2021.01.25
	//	Scan Stage Roll, Pitch 보정 UI 임시 작업으로 수정.
	switch (m_AdamOmtiltSwitch)
	{
	case 0:
		// OM UI		
		g_pCamera->ShowWindow(SW_SHOW);
		g_pRecipe->ShowWindow(SW_SHOW);
		g_pScanStageTest->ShowWindow(SW_HIDE);
		g_pChart->ShowWindow(SW_HIDE);
		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("OM"));
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_HIDE);
			g_pPhase->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Phase"));
			break;
		case EUVPTR:
			g_pPTR->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("PTR"));
			break;
		case ELITHO:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("OM"));
			break;
		}
		m_AdamOmtiltSwitch++;
		break;
	case 1:
		// ADAM
		g_pCamera->ShowWindow(SW_HIDE);
		g_pRecipe->ShowWindow(SW_HIDE);
		g_pScanStageTest->ShowWindow(SW_HIDE);
		g_pChart->ShowWindow(SW_HIDE);

		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_SHOW);
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_SHOW);
			g_pPhase->ShowWindow(SW_SHOW);
			break;
		case EUVPTR:
			if (g_pPTR != NULL) g_pPTR->ShowWindow(SW_SHOW);
			break;
		case ELITHO:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_SHOW);
			break;

		default:
			break;
		}
		m_AdamOmtiltSwitch++;
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Adam"));
		break;
	case 2:
		//TILT
		g_pCamera->ShowWindow(SW_HIDE);
		g_pRecipe->ShowWindow(SW_HIDE);
		g_pAdam->ShowWindow(SW_HIDE);
		g_pPTR->ShowWindow(SW_HIDE);
		g_pPhase->ShowWindow(SW_HIDE);
		g_pXrayCamera->ShowWindow(SW_HIDE);

		g_pScanStageTest->ShowWindow(SW_SHOW);
		g_pChart->ShowWindow(SW_SHOW);

		m_AdamOmtiltSwitch = 0;
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("TILT"));

		break;
	default:
		break;
	}


	/*

	jhkim -2021.01.25
	Scan Stage Roll, Pitch 보정 UI 임시 작업으로 수정.


	switch (m_CheckOMAdamUi.GetCheck())
	{
	case BST_CHECKED:
		// OM UI
		g_pCamera->ShowWindow(SW_SHOW);
		g_pRecipe->ShowWindow(SW_SHOW);


		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Adam"));
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_HIDE);
			g_pPhase->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Phase"));
			break;
		case EUVPTR:
			g_pPTR->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("PTR"));
			break;
		default:
			break;
		}

		break;
	case BST_UNCHECKED:
		g_pCamera->ShowWindow(SW_HIDE);
		g_pRecipe->ShowWindow(SW_HIDE);

		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_SHOW);
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_SHOW);
			g_pPhase->ShowWindow(SW_SHOW);
			break;
		case EUVPTR:
			if (g_pPTR != NULL) g_pPTR->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("OM"));

		break;
	default:
		break;
	}
	*/



	/*switch (m_CheckOMAdamCtrl.GetCheck())
	{
	case BST_CHECKED:
		ChangeOMEUVSet(OMTOEUV);
		break;
	case BST_UNCHECKED:
		ChangeOMEUVSet(EUVTOOM);
		break;
	default:
		break;
	}*/

}


void CMaskMapDlg::OnBnClickedButtonMaskmeasureMacroStart()
{
	MacroStart();

	//double z_pos_um = 0;
	//g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskmeasureMacroStart() 버튼 클릭!"));

	//if (g_pConfig->m_nEquipmentMode == OFFLINE)
	//	return;

	//CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	//if (m_bAutoSequenceProcessing == TRUE)
	//{
	//	MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
	//	return;
	//}

	//if (IDYES != AfxMessageBox("자동 Imaging 진행 하시겠습니까?", MB_YESNO)) return;

	//m_strStageStatus.Format(_T("자동 Imaging 측정 시작!"));

	//m_bAutoSequenceProcessing = TRUE;


	//CString strCap;
	//m_nAutoRunRepeatResultNo = 0;

	//for (int i = 0; i < m_nAutoRunRepeatSetNo; i++)
	//{
	//	if (i > 0)
	//	{
	//		g_pAdam->m_IsResetStartPos = FALSE;
	//		g_pAdam->UpdateData(FALSE);
	//	}
	//	else
	//	{
	//		g_pAdam->m_IsResetStartPos = TRUE;
	//		g_pAdam->UpdateData(FALSE);
	//	}

	//	if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
	//	{
	//		g_pWarning->ShowWindow(SW_HIDE);
	//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//		m_bAutoSequenceProcessing = FALSE;
	//		return;
	//	}

	//	if (i > 0)
	//	{
	//		for (int j = 0; j < 10; j++)
	//		{
	//			if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
	//			{
	//				g_pWarning->ShowWindow(SW_HIDE);
	//				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//				m_bAutoSequenceProcessing = FALSE;
	//				return;
	//			}
	//			g_pAdam->MoveZCapsensorFocusPosition();
	//			z_pos_um = g_pAdam->AdamData.m_dCapsensor2;
	//			WaitSec(5);
	//		}
	//	}
	//	else
	//	{
	//		g_pAdam->MoveZCapsensorFocusPosition();
	//		z_pos_um = g_pAdam->AdamData.m_dCapsensor2;
	//	}

	//	if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
	//	{
	//		g_pWarning->ShowWindow(SW_HIDE);
	//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//		m_bAutoSequenceProcessing = FALSE;
	//		return;
	//	}
	//	WaitSec(1);

	//	strCap.Format("Z=%.3f, Cap1=%.3f, Cap2=%.3f, Cap3=%.3f, Cap4=%.3f,", g_pScanStage->m_dPIStage_GetPos[Z_AXIS], g_pAdam->AdamData.m_dCapsensor1, g_pAdam->AdamData.m_dCapsensor2, g_pAdam->AdamData.m_dCapsensor3, g_pAdam->AdamData.m_dCapsensor4);
	//	SaveLogFile("HeightValue", (LPSTR)(LPCTSTR)strCap);

	//	g_pAdam->OnBnClickedAdamRunButton();

	//	while (g_pAdam->m_bIsScaningOn)
	//	{
	//		if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
	//		{
	//			g_pWarning->ShowWindow(SW_HIDE);
	//			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//			m_bAutoSequenceProcessing = FALSE;
	//			return;
	//		}
	//		WaitSec(1);
	//	}

	//	if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
	//	{
	//		g_pWarning->ShowWindow(SW_HIDE);
	//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//		m_bAutoSequenceProcessing = FALSE;
	//		return;
	//	}
	//	WaitSec(1);

	//	g_pAdam->FilteredImageFileSave_Resize(50, 0, g_pAdam->m_dFilteredImage_Data3D[0], z_pos_um);

	//	if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
	//	{
	//		g_pWarning->ShowWindow(SW_HIDE);
	//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//		m_bAutoSequenceProcessing = FALSE;
	//		return;
	//	}
	//	WaitSec(1);

	//	m_nAutoRunRepeatResultNo++;
	//	UpdateData(FALSE);

	//}

	//m_strStageStatus.Format(_T("자동 Imaging 측정 End!"));
	//g_pEUVSource->SetEUVSourceOn(FALSE);
	//g_pEUVSource->SetMechShutterOpen(FALSE);
	//g_pAdam->m_IsResetStartPos = TRUE;
	//g_pAdam->UpdateData(FALSE);
	//m_bAutoSequenceProcessing = FALSE;
}

void CMaskMapDlg::MacroStart()
{
	char Macro_Command[20] = { 0, };
	//	m_MaskMapWnd.m_MacroSystem._ComList.vector_commands.clear();

	for (int i = 0; i < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; i++)
	{
		strcpy(Macro_Command, m_MaskMapWnd.m_ProcessData.pMeasureList[i].Command);
		m_MaskMapWnd.m_MacroSystem.GetCommandData(Macro_Command, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um, m_MaskMapWnd.m_ProcessData.pMeasureList[i].dFOVSize,
			m_MaskMapWnd.m_ProcessData.pMeasureList[i].dStepSize, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nThroughFocusNo, m_MaskMapWnd.m_ProcessData.pMeasureList[i].dThroughFocusStep);
	}

	if (m_MaskMapWnd.m_MacroSystem._ComList.createRecipe() != TRUE)
	{
		AfxMessageBox("MACRO SYSTEM INSTALL FAIL", MB_ICONERROR);
	}
	m_MaskMapWnd.m_MacroSystem.DeleteCom();
	m_MaskMapWnd.m_MacroSystem.DeleteMem();
}

void CMaskMapDlg::OnBnClickedCheckMaskmapReverseCoordinate()
{
	//m_bReverseCoordinate;
	UpdateData(TRUE);
}

double Norm(vector<double> const& u) {
	double accum = 0.;
	for (double x : u) {
		accum += x * x;
	}
	return sqrt(accum);
}

vector<double> Cross(vector<double> const &a, vector<double> const &b)
{
	vector<double> r(a.size());

	r[0] = a[1] * b[2] - a[2] * b[1];
	r[1] = a[2] * b[0] - a[0] * b[2];
	r[2] = a[0] * b[1] - a[1] * b[0];
	return r;
}

double Dot(vector<double> const &a, vector<double> const &b)
{
	double sum = 0;

	for (int i = 0; i < a.size(); i++)
	{
		sum = sum + a[i] * b[i];
	}

	return sum;
}

vector<double> Subtract(vector<double> const &a, vector<double> const &b)
{
	vector<double> r(a.size());
	r[0] = a[0] - b[0];
	r[1] = a[1] - b[1];
	r[2] = a[2] - b[2];
	return r;
}

vector<double> CMaskMapDlg::CenterOfCircumCircle(vector<double> P1, vector<double> P2, vector<double> P3)
{
	double r = Norm(Subtract(P1, P2))*Norm(Subtract(P2, P3))*Norm(Subtract(P3, P1)) / 2 / Norm(Cross(Subtract(P2, P1), Subtract(P2, P3)));


	vector<double> center(P1.size());

	double alpha = Norm(Subtract(P2, P3)) * Norm(Subtract(P2, P3)) * Dot(Subtract(P1, P2), Subtract(P1, P3)) / 2 / (Norm(Cross(Subtract(P1, P2), Subtract(P2, P3)))*Norm(Cross(Subtract(P1, P2), Subtract(P2, P3))));
	double beta = Norm(Subtract(P1, P3)) * Norm(Subtract(P1, P3)) * Dot(Subtract(P2, P1), Subtract(P2, P3)) / 2 / (Norm(Cross(Subtract(P1, P2), Subtract(P2, P3)))*Norm(Cross(Subtract(P1, P2), Subtract(P2, P3))));
	double gama = Norm(Subtract(P1, P2)) * Norm(Subtract(P1, P2)) * Dot(Subtract(P3, P1), Subtract(P3, P2)) / 2 / (Norm(Cross(Subtract(P1, P2), Subtract(P2, P3)))*Norm(Cross(Subtract(P1, P2), Subtract(P2, P3))));

	//double  beta = Norm(Subtract(P1 , P3)) ^ 2 * Dot(P2 - P1, P2 - P3) / 2 / Norm(Cross(P1 - P2, P2 - P3)) ^ 2;
	//double  gama = Norm(Subtract(P1 , P2)) ^ 2 * Dot(P3 - P1, P3 - P2) / 2 / Norm(Cross(P1 - P2, P2 - P3)) ^ 2;

	center[0] = alpha * P1[0] + beta * P2[0] + gama * P3[0];
	center[1] = alpha * P1[1] + beta * P2[1] + gama * P3[1];
	center[2] = alpha * P1[2] + beta * P2[2] + gama * P3[2];

	double x = center[0];
	double y = center[1];

	return center;
}

double CMaskMapDlg::FindRotation(double x1, double y1, double x2, double y2)
{
	double pi = 3.14159265358979323846;
	//double rotation_rad = atan2(y2 - y1, x2 - x1);
	double rotation_rad = atan((y2 - y1) / (x2 - x1));

	return rotation_rad;
}


int CMaskMapDlg::WaferAlign_Notch_Litho(BOOL isManual)
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pXrayCamera == NULL || g_pWarning == NULL || g_pPhase == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::WaferAlignLitho() Start!"));

	CMessageDlg MsgBox;
	m_strStageStatus.Format(_T("Wafer Notch Align Process 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " Wafer Align 중입니다. 잠시 기다려 주세요! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;
	m_bOMAlignComplete = FALSE;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -8;
	}

	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) > Z_INITIAL_POS_UM)
	{
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(2);
	}

	int nRet = 0;
	double target_posx_um = 0.0, target_posy_um = 0.0;
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double maskposx_mm = 0.0, maskposy_mm = 0.0;
	double NotchOmAlingPosXmm[2], NotchOmAlingPosYmm[2];


	for (int i = 0; i < 2; i++)
	{
		if (isManual)
		{
			target_posx_um = (m_MaskMapWnd.m_ProcessData.m_stNotchAlignPos_um[i].X);
			target_posy_um = (m_MaskMapWnd.m_ProcessData.m_stNotchAlignPos_um[i].Y);

			target_posx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - target_posx_um / 1000.0;
			target_posy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - target_posy_um / 1000.0;

			// Z축 이동 후 STATE 이동
			g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
			WaitSec(1);
			if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
			{
				g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);
				WaitSec(1);
				g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);
				WaitSec(1);
			}
			else
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				nRet = -3;
				return nRet;
			}

			g_pWarning->ShowWindow(SW_HIDE);
			if (MsgBox.DoModal(" OM Notch Align 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
			{
				MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				return -10;
			}
			g_pWarning->ShowWindow(SW_SHOW);
		}
		else
		{

		}

		target_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		target_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		NotchOmAlingPosXmm[i] = target_posx_mm;
		NotchOmAlingPosYmm[i] = target_posy_mm;

		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(1);
		if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
			WaitSec(1);
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
			WaitSec(1);
		}
		else
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			nRet = -3;
			return nRet;
		}
	}

	g_pConfig->m_dNotchAlignPoint1_X_mm = NotchOmAlingPosXmm[0];
	g_pConfig->m_dNotchAlignPoint1_Y_mm = NotchOmAlingPosYmm[0];
	g_pConfig->m_dNotchAlignPoint2_X_mm = NotchOmAlingPosXmm[1];
	g_pConfig->m_dNotchAlignPoint2_Y_mm = NotchOmAlingPosYmm[1];

	m_bOMAlignComplete = TRUE;
	m_bEUVAlignComplete = TRUE;

	g_pConfig->m_bOMAlignCompleteFlag = m_bOMAlignComplete;
	g_pConfig->m_bEUVAlignCompleteFlag = m_bEUVAlignComplete;
	g_pConfig->SaveRecoveryData();

	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Wafer Notch OM Align이 완료되었습니다! "), 2);
	m_bAutoSequenceProcessing = FALSE;
	m_strStageStatus.Format(_T("Wafer Notch OM Align 완료!"));
	m_MaskMapWnd.Invalidate(FALSE);
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_OM() End!"));

	return nRet;
}

int CMaskMapDlg::LithoDoseProcess()
{

	CString strLog;

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}

	if (m_bOMAlignComplete == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" Mask Align부터 진행해주세요! "), 2);
		return -4;
	}

	if (g_pEUVSource->Is_EUV_On() == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" EUV를 On 시키고 진행해주세요! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::LithoDoseProcess() Start!"));

	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("Auto Litho_Dose Process 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " Litho 진행 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	// Laser Feedback으로 전환
	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	{
		g_pNavigationStage->SetLaserMode();
	}

	///////////// 파일 오픈  ////////////////////////
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	CString fullfilename;
	CString filename;
	CString saveDirectory;
	CString filenameDate;

	saveDirectory = _T("C:\\Litho Result");
	filenameDate.Format(_T("_%d%02d%02d_%02d%02d%02d"), year, month, day, hour, minute, second);
	filename = _T("elitho_result");
	filename = filename + filenameDate;
	fullfilename = saveDirectory + "\\" + filename + ".txt";

	//CString strStatus;
	int RepeatNo;
	double FovX_um, FovY_um, height_um, StepSize_um;

	for (int i = 0; i < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; i++)
	{
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -5;
		}

		/*m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um;
		m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um;
		m_MaskMapWnd.m_ProcessData.pMeasureList[i].dStepSize;
		m_MaskMapWnd.m_ProcessData.pMeasureList[i].dExposureTime_msec;
		m_MaskMapWnd.m_ProcessData.pMeasureList[i].dExposureHeight_um;*/


		FovX_um = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dFOVSizeX;
		FovY_um = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dFOVSizeY;
		RepeatNo = m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;
		height_um = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dExposureHeight_um;
		StepSize_um = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dStepSize;


		m_strStageStatus.Format("%d/%d", m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.TotalMeasureNum);
		UpdateData(FALSE);


		//1. Litho할 좌표로 이동
		g_pNavigationStage->MoveToSelectedPointNo(i);


		//1.1 Z축 이동 
		//if( height_um <-30 )
		g_pScanStage->MoveZAbsolute_SlowInterlock(height_um);
		WaitSec(1);

		if (RepeatNo > 0)
		{
			//2. Shutter Open
			g_pAdam->Command_LIFReset();
			g_pEUVSource->SetMechShutterOpen(TRUE);
			g_pAdam->ADAMRunStartForElitho();

			//3. Scan
			if (RepeatNo != 0)
			{
				g_pNavigationStage->Start_Scan(FovX_um, FovY_um, StepSize_um, RepeatNo);

			}

			//2. Shutter Close
			g_pAdam->ADAMAbortForElitho();

			g_pEUVSource->SetMechShutterOpen(FALSE);
			WaitSec(1);
		}

		//3. z축 복귀
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(1);

		FILE *fp;
		errno_t err;
		err = fopen_s(&fp, fullfilename, "a");
		fprintf(fp, "\t%d  \t%.6f \t%.6f \t%d \t%.6f \n", m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo, g_pAdam->D3Average);
		fclose(fp);

		strLog.Format("\t%d  \t%.6f \t%.6f \t%d \t%.6f", m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um, m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo, g_pAdam->D3Average);
		SaveLogFile("LithoResult", strLog);

	}

	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Auto Litho_Dose Process가 완료되었습니다! "), 2);

	m_strStageStatus.Format(_T("Auto Litho_Dose Process 완료!"));
	UpdateData(FALSE);

	return 0;
}

int CMaskMapDlg::LithoGratingProcess()
{
	BOOL IsUseSq1 = FALSE;
	CString str;

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL)
		return -2;

	CString strLog;
	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}

	if (m_bOMAlignComplete == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" Mask Align부터 진행해주세요! "), 2);
		return -4;
	}

	if (g_pEUVSource->Is_EUV_On() == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" EUV를 On 시키고 진행해주세요! "), 2);
		return -5;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::LithoGratingProcess() Start!"));

	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("Auto Litho_Grating Process 시작!"));
	UpdateData(FALSE);


	KillTimer(LITHO_DOSE_TIME_CHECK);
	g_pWarning->n_bMessageMoxFlag = true;
	m_nTimeSecCount = 0;
	m_nTimeMinCount = 0;
	LithoEndHour = 0;
	LithoEndMin = 0;

	LithoEndHour = CTime::GetCurrentTime().GetHour();
	LithoEndMin = CTime::GetCurrentTime().GetMinute();

	if (m_MaskMapWnd.m_ProcessData.nExposureTime_total > 60)
	{
		LithoEndHour += ((m_MaskMapWnd.m_ProcessData.nExposureTime_total) / 60);
	}

	LithoEndMin += ((m_MaskMapWnd.m_ProcessData.nExposureTime_total) % 60);
	if (LithoEndMin > 60)
	{
		LithoEndHour += LithoEndMin / 60;
		LithoEndMin = LithoEndMin % 60;
	}

	if (LithoEndHour > 24)
	{
		LithoEndHour = LithoEndHour - 24;
	}
	

	SetTimer(LITHO_DOSE_TIME_CHECK, 1000, NULL);
	//str.Format(_T(" Litho 진행 중입니다!(약 %d분 소모예정) "), m_MaskMapWnd.m_ProcessData.nExposureTime_total);
	//g_pWarning->m_strWarningMessageVal = str;
	//g_pWarning->UpdateData(FALSE);
	//g_pWarning->ShowWindow(SW_SHOW);

	
	m_bAutoSequenceProcessing = TRUE;

	// Laser Feedback으로 전환
	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	{
		g_pNavigationStage->SetLaserMode();
	}

	///////////// 파일 오픈  ////////////////////////
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	CString fullfilename;
	CString filename;
	CString saveDirectory;
	CString filenameDate;

	saveDirectory = _T("C:\\Litho Result");
	filenameDate.Format(_T("_%d%02d%02d_%02d%02d%02d"), year, month, day, hour, minute, second);
	filename = _T("elitho_result");
	filename = filename + filenameDate;
	fullfilename = saveDirectory + "\\" + filename + ".txt";

	double height_um = 0, exposuretime_msec = 0, beam_pitch = 0, beam_yaw = 0, beam_pitch_old = 0, beam_yaw_old = 0;
	double diff_time = 0.0;

	if (IsUseSq1)
	{
		g_pAdam->m_Sq1.InitialData.X = -0.2701;
		g_pAdam->m_Sq1.InitialData.Y = -3.0020;
		g_pAdam->m_Sq1.InitialData.Z = 5.9499;

		g_pAdam->m_Sq1.InitialData.Rx = 0.650;
		g_pAdam->m_Sq1.InitialData.Ry = 0.550;
		g_pAdam->m_Sq1.InitialData.Rz = 0.000;

		g_pAdam->m_Sq1.InitialData.Cx = -983.0;
		g_pAdam->m_Sq1.InitialData.Cy = 450.0;
		g_pAdam->m_Sq1.InitialData.Cz = 1600.0;
	}

	//beam_pitch_old = g_pAdam->m_Sq1.InitialData.Rx;
	//beam_yaw_old = g_pAdam->m_Sq1.InitialData.Ry;

	beam_pitch_old = 0;
	beam_yaw_old = 0;

	if (IsUseSq1)
	{
		g_pAdam->m_Sq1.SetUntilInposition(g_pAdam->m_Sq1.InitialData.X, g_pAdam->m_Sq1.InitialData.Y, g_pAdam->m_Sq1.InitialData.Z, g_pAdam->m_Sq1.InitialData.Rx, g_pAdam->m_Sq1.InitialData.Ry, g_pAdam->m_Sq1.InitialData.Rz, g_pAdam->m_Sq1.InitialData.Cx, g_pAdam->m_Sq1.InitialData.Cy, g_pAdam->m_Sq1.InitialData.Cz);
	}

	clock_t starttime_msec, endtime_msec;
	double difftime_msec = 0;

	for (int i = 0; i < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; i++)
	{
		if (m_bAutoSequenceProcessing == FALSE)
		{
			if (IsUseSq1)
			{
				g_pAdam->m_Sq1.SetUntilInposition(g_pAdam->m_Sq1.InitialData.X, g_pAdam->m_Sq1.InitialData.Y, g_pAdam->m_Sq1.InitialData.Z, g_pAdam->m_Sq1.InitialData.Rx, g_pAdam->m_Sq1.InitialData.Ry, g_pAdam->m_Sq1.InitialData.Rz, g_pAdam->m_Sq1.InitialData.Cx, g_pAdam->m_Sq1.InitialData.Cy, g_pAdam->m_Sq1.InitialData.Cz);
			}			
			g_pWarning->ShowWindow(SW_HIDE);
			KillTimer(LITHO_DOSE_TIME_CHECK);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pEUVSource->SetMechShutterOpen(FALSE);
			g_pEUVSource->SetEUVSourceOn(FALSE);
			m_strStageStatus.Format(_T("Auto Sequence가 중지되었습니다!"));
			UpdateData(FALSE);
			g_pScanStage->m_bZHoldOn = FALSE;
			g_pScanStage->KillTimer(SCANSTAGE_ZHOLD_TIMER);
			m_CheckScanOnlyCtrl.SetWindowText(_T("Z Hold Off"));
			m_CheckScanOnlyCtrl.SetCheck(FALSE);
			return -5;
		}

		m_strStageStatus.Format("%d/%d", m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.TotalMeasureNum);
		UpdateData(FALSE);

		//1. Litho할 좌표로 이동
		g_pNavigationStage->MoveToSelectedPointNo(i);

		//2. Z축 이동 후 Z Hold On
		g_pAdam->MoveZCapsensorFocusPosition();		
		WaitSec(1);
		g_pScanStage->m_bZHoldOn = TRUE;
		g_pScanStage->SetTimer(SCANSTAGE_ZHOLD_TIMER, 5000, NULL);
		m_CheckScanOnlyCtrl.SetWindowText(_T("Z Hold On"));
		m_CheckScanOnlyCtrl.SetCheck(TRUE);

		//3. SQR1 PITCH,YAW 이동
		if (IsUseSq1)
		{
			beam_pitch = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dBeam_pitch;
			beam_yaw = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dBeam_yaw;
			if (beam_pitch_old != beam_pitch || beam_yaw_old != beam_yaw)
			{
				if (g_pAdam->m_Sq1.SetUntilInposition(g_pAdam->m_Sq1.InitialData.X, g_pAdam->m_Sq1.InitialData.Y, g_pAdam->m_Sq1.InitialData.Z, beam_pitch, beam_yaw, g_pAdam->m_Sq1.InitialData.Rz,
					g_pAdam->m_Sq1.InitialData.Cx, g_pAdam->m_Sq1.InitialData.Cy, g_pAdam->m_Sq1.InitialData.Cz) != 0)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					KillTimer(LITHO_DOSE_TIME_CHECK);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					m_bAutoSequenceProcessing = FALSE;
					g_pEUVSource->SetMechShutterOpen(FALSE);
					g_pEUVSource->SetEUVSourceOn(FALSE);
					m_strStageStatus.Format(_T("Auto Sequence가 중지되었습니다!"));
					UpdateData(FALSE);
					g_pScanStage->m_bZHoldOn = FALSE;
					g_pScanStage->KillTimer(SCANSTAGE_ZHOLD_TIMER);
					m_CheckScanOnlyCtrl.SetWindowText(_T("Z Hold Off"));
					m_CheckScanOnlyCtrl.SetCheck(FALSE);
					return -5;
				}
			}
			beam_pitch_old = beam_pitch;
			beam_yaw_old = beam_yaw;

			//SQ1 KILL
			//g_pAdam->m_Sq1.Stop();

		}
		//4. Shutter Open
		exposuretime_msec = m_MaskMapWnd.m_ProcessData.pMeasureList[i].dExposureTime_msec;
		g_pAdam->Command_LIFReset();

		starttime_msec = clock();
		g_pEUVSource->SetMechShutterOpen(TRUE);
		g_pAdam->ADAMRunStartForElitho();

		WaitSec(exposuretime_msec / 1000.);

		//5. Shutter Close
		g_pEUVSource->SetMechShutterOpen(FALSE);
		endtime_msec = clock();
		difftime_msec = endtime_msec - starttime_msec;

		g_pAdam->ADAMAbortForElitho();
		WaitSec(1);

		//6.Z Hold Off
		g_pScanStage->m_bZHoldOn = FALSE;
		g_pScanStage->KillTimer(SCANSTAGE_ZHOLD_TIMER);
		m_CheckScanOnlyCtrl.SetWindowText(_T("Z Hold Off"));
		m_CheckScanOnlyCtrl.SetCheck(FALSE);
		WaitSec(1);

		//7. z축 복귀
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(1);

		FILE *fp;
		errno_t err;
		err = fopen_s(&fp, fullfilename, "a");
		fprintf(fp, "No=\t%d \tx=\t%.6f \ty=\t%.6f \tD3Avg=\t%.6f \texposure_set_time=\t%.6f \tsqr1_pitch=\t%.6f \tsqr1_yaw=\t%.6f \tdiff_time=\t%.6f\n", m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um, g_pAdam->D3Average, exposuretime_msec, beam_pitch, beam_yaw, difftime_msec);
		fclose(fp);

		strLog.Format("No=\t%d \tx=\t%.6f \ty=\t%.6f \tD3Avg=\t%.6f \texposure_set_time=\t%.6f \tsqr1_pitch=\t%.6f \tsqr1_yaw=\t%.6f \tdiff_time=\t%.6f", m_MaskMapWnd.m_ProcessData.pMeasureList[i].MeasurementNo, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um, m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um, g_pAdam->D3Average, exposuretime_msec, beam_pitch, beam_yaw, difftime_msec);
		SaveLogFile("LithoResult", strLog);
	}

	if (IsUseSq1)
	{
		g_pAdam->m_Sq1.SetUntilInposition(g_pAdam->m_Sq1.InitialData.X, g_pAdam->m_Sq1.InitialData.Y, g_pAdam->m_Sq1.InitialData.Z, g_pAdam->m_Sq1.InitialData.Rx, g_pAdam->m_Sq1.InitialData.Ry, g_pAdam->m_Sq1.InitialData.Rz, g_pAdam->m_Sq1.InitialData.Cx, g_pAdam->m_Sq1.InitialData.Cy, g_pAdam->m_Sq1.InitialData.Cz);
	}

	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	KillTimer(LITHO_DOSE_TIME_CHECK);
	MsgBoxAuto.DoModal(_T(" Auto Litho_Grating Process가 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("Auto Litho_Grating Process 완료!"));
	UpdateData(FALSE);

	return 0;
}

void CMaskMapDlg::OnCloseupComboScangridLitho()
{
	CString strScanGridData;
	m_comboScanGridAtLitho.GetLBText(m_comboScanGridAtLitho.GetCurSel(), strScanGridData);
	int m = atoi(strScanGridData);
	m_nEuvLitho_ScanGrid = m;
}


void CMaskMapDlg::OnBnClickedButtonNavigationstageConnect()
{
	if (g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		if (g_pNavigationStage->m_hComm != ACSC_INVALID)
			g_pNavigationStage->DisconnectComm();
	}

	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			if (!g_pNavigationStage->ConnectACSController(ETHERNET, g_pConfig->m_chIP[ETHERNET_NAVI_STAGE], g_pConfig->m_nPORT[ETHERNET_NAVI_STAGE]))
			{
				g_pCommStat->NavigationStageCommStatus(FALSE);
				g_pLoadingScreen.SetTextMessage(_T("Navigation Stage Connection Fail!"));
				AfxMessageBox(_T("Navigation Stage Connection Fail!"), MB_ICONERROR);
				WaitSec(3);
			}
			else
			{
				g_pLoadingScreen.SetTextMessage(_T("Navigation Stage Connection Success!"));
				AfxMessageBox(_T("Navigation Stage Connection Success!"), MB_ICONINFORMATION);
			}
		}
	}
}

void CMaskMapDlg::LithoDoseTimeCheck()
{
	CString str;

	m_nTimeSecCount++;
	if (m_nTimeSecCount == 60)
	{
		m_nTimeSecCount = 0;
		m_nTimeMinCount++;
	}

	str.Format(_T(" Litho 진행 중입니다! ( 약 %d시 %d분 종료예정 (%d분 소모), 현재 진행 시간 : %d분 %d초 ) "),LithoEndHour, LithoEndMin, m_MaskMapWnd.m_ProcessData.nExposureTime_total, m_nTimeMinCount,m_nTimeSecCount);


	if (g_pWarning->n_bMessageMoxFlag == false)
	{
		KillTimer(LITHO_DOSE_TIME_CHECK);
		g_pWarning->n_bMessageMoxFlag = true;
	}
	g_pWarning->m_strWarningMessageVal = str;
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
}