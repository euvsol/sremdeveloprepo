﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CTurboPumpDlg 대화 상자

IMPLEMENT_DYNAMIC(CTurboPumpDlg, CDialogEx)

CTurboPumpDlg::CTurboPumpDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TURBO_PUMP_DIALOG, pParent)
{
	m_LLCTmpThread = NULL;
	m_LLCTmpThreadExitFlag = FALSE;
	m_LLC_Tmp_State = 0;
}

CTurboPumpDlg::~CTurboPumpDlg()
{
}

void CTurboPumpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTurboPumpDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_LLC_TMP_BTN_ON, &CTurboPumpDlg::OnBnClickedLlcTmpBtnOn)
	ON_BN_CLICKED(IDC_LLC_TMP_BTN_OFF, &CTurboPumpDlg::OnBnClickedLlcTmpBtnOff)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CTurboPumpDlg 메시지 처리기


BOOL CTurboPumpDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CTurboPumpDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	memset(m_BYTE, '\0', 15);

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);


	m_LLCTmpConnectStateFlag = FALSE;
	m_LLCTmpThreadExitFlag = FALSE;

	StrLLCTmpReceiveDataHz.Empty();
	StrLLCTmpReceiveDataRpm.Empty();
	StrLLCTmpReceiveDataErrorCode.Empty();
	StrLLCTmpReceiveDataErrorState.Empty();
	StrLLCTmpReceiveDataTemperature.Empty();
	StrLLCTmpReceiveDataTemperatureHeatsink.Empty();
	StrLLCTmpReceiveDataTemperatureAir.Empty();
	StrLLCTmpReceiveDataState.Empty();
	StrLLCTmpState.Empty();

	((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_LLC_ICON_CON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);
	
	StrLLCTmpReceiveDataErrorState = _T("No Error");

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CTurboPumpDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	m_LLCTmpThreadExitFlag = TRUE;

	if (m_LLCTmpThread != NULL)
	{
		HANDLE threadHandle = m_LLCTmpThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_LLCTmpThread = NULL;
	}
}

int CTurboPumpDlg::OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
{

	int nRet = -1;

	nRet = CAgilentTwisTorr304TMPCtrl::OpenPort(sPortName, dwBaud, wByte, wStop, wParity);

	if (nRet != 0) {
		m_LLCTmpConnectStateFlag = FALSE;
		SetDlgItemText(IDC_LLC_TMP_HZ, "Connection Fail");
		SetDlgItemText(IDC_LLC_TMP_STATIC2, "Connection Fail");
		SetDlgItemText(IDC_LLC_TMP_STATIC3, "Connection Fail");
		SetDlgItemText(IDC_LLC_TMP_STATIC4, "Connection Fail");
		((CStatic*)GetDlgItem(IDC_LLC_ICON_CON))->SetIcon(m_LedIcon[2]);
	}
	else {
		m_LLCTmpConnectStateFlag = TRUE;
		m_LLCTmpThread = AfxBeginThread(Updata_tmp_Thread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
		SetTimer(LLC_TMP_UPDATE_TIMER, 100, NULL);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_CON))->SetIcon(m_LedIcon[1]);
	}
	
	return nRet;
}



UINT CTurboPumpDlg::Updata_tmp_Thread(LPVOID pParam)
{
	CTurboPumpDlg* tmprunthread = (CTurboPumpDlg*)pParam;

	while (!tmprunthread->m_LLCTmpThreadExitFlag)
	{
		tmprunthread->LLCTmpSendUpdate();
		Sleep(1000);
	}

	return 0;
}


void CTurboPumpDlg::OnTimer(UINT_PTR nIDEvent)
{

	KillTimer(nIDEvent);
	
	if (nIDEvent == LLC_TMP_UPDATE_TIMER)
	{
		if (!m_LLCTmpThreadExitFlag)
		{
			LLCTmpReceiveDataUpdate();
			SetTimer(nIDEvent, 100, NULL);
		}
	}

	__super::OnTimer(nIDEvent);
}

void CTurboPumpDlg::LLCErrorModeOn()
{
	CString log_str;

	if (g_pIO->LLC_Dry_Pump_off())
	{
		log_str = " MAIN_ERROR_ON() : Error 발생 후 LLC TMP STOP , LLC Dry Pump Off 실행 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
		//////////////////////////////////////////////////////////////////
		// TMP 완전 멈춤, 그후 DRY PUMP OFF 후 MC FORELINE GATE CLOSE
		//////////////////////////////////////////////////////////////////

		//////////////////////////
		// LLC FORELINE GATE CLOSE
		//////////////////////////
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤, 그 후 LLC Foreline Gate  Close !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
		
		if (g_pIO->Close_LLC_TMP_ForelineValve() != OPERATION_COMPLETED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤. 그 후 LLC Foreline Gate Close 명령 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, log_str);
			g_pLog->Display(0, g_pIO->Log_str);
			if (g_pIO->Close_LLC_TMP_ForelineValve() != OPERATION_COMPLETED)
			{
				log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤. 그 후 LLC Foreline Gate Close 명령 에러에 따른 재 실행 !";
				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
				g_pLog->Display(0, log_str);
				g_pLog->Display(0, g_pIO->Log_str);
				return;
			}
		}
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_CLOSE) break;
		}

		if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤. 그 후 LLC Foreline Gate Close 확인 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			g_pLog->Display(0, log_str);

			return ;
		}

		log_str = "MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤. 그 후 LLC Foreline Gate Close 확인 완료!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

	}
	else
	{
		log_str = " MAIN_ERROR_ON() : Error 발생 후 LLC TMP STOP , LLC Dry Pump Off 실행 Error (확인필요) !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}


}

int CTurboPumpDlg::LLCTmpReceiveDataUpdate()
{
	int ret = 0;


	StrLLCTmpState = StrLLCTmpReceiveDataState;

	SetDlgItemText(IDC_LLC_TMP_HZ, StrLLCTmpReceiveDataHz);
	SetDlgItemText(IDC_LLC_TMP_RPM, StrLLCTmpReceiveDataRpm);
	SetDlgItemText(IDC_LLC_TMP_STATIC2, StrLLCTmpReceiveDataState);
	SetDlgItemText(IDC_LLC_TMP_TEMP1, StrLLCTmpReceiveDataTemperature);
	SetDlgItemText(IDC_LLC_TMP_TEMP2, StrLLCTmpReceiveDataTemperatureHeatsink);
	SetDlgItemText(IDC_LLC_TMP_TEMP3, StrLLCTmpReceiveDataTemperatureAir);
	SetDlgItemText(IDC_LLC_TMP_STATIC4, StrLLCTmpReceiveDataErrorCode);
	//SaveLogFile("LLC_TMP_LOG", _T((LPSTR)(LPCTSTR)(" LLC_TMP_HZ : " + StrLLCTmpReceiveDataHz + ",   LLC_TMP_STATE : " + StrLLCTmpReceiveDataState + ", LLC_TMP_ERROR : " + StrLLCTmpReceiveDataErrorCode + ", LLC_TMP_TEMP : " + StrLLCTmpReceiveDataTemperature))); //통신 상태 기록.
	
	//StrLogTemp.Format(_T("LLC_TMP_HZ : %s , LLC_TMP_STATE : %s , LLC_TMP_ERROR : %s , LLC_TMP_TEMP : %s"),StrLLCTmpReceiveDataHz, StrLLCTmpReceiveDataState, StrLLCTmpReceiveDataErrorCode, StrLLCTmpReceiveDataTemperature);
	//SaveLogFile("LLC_TMP_LOG", StrLogTemp); //통신 상태 기록.
	//SaveLogFile("LLC_TMP_LOG", _T((LPSTR)(LPCTSTR)(" LLC_TMP_HZ : " + StrLLCTmpReceiveDataHz + ",   LLC_TMP_STATE : " + StrLLCTmpReceiveDataState + ", LLC_TMP_ERROR : " + StrLLCTmpReceiveDataErrorCode + ", LLC_TMP_TEMP : " + StrLLCTmpReceiveDataTemperature))); //통신 상태 기록.
	//SaveLogFile("LLC_TMP_LOG", _T((LPSTR)(LPCTSTR)(" LLC_TMP_HZ : " + StrLLCTmpReceiveDataHz + ",   LLC_TMP_STATE : " + StrLLCTmpReceiveDataState + ", LLC_TMP_ERROR : " + StrLLCTmpReceiveDataErrorCode + ", LLC_TMP_TEMP : " + StrLLCTmpReceiveDataTemperature))); //통신 상태 기록.
	//SaveLogFile("LLC_TMP_LOG", _T((" LLC_TMP_HZ : " + StrLLCTmpReceiveDataHz + ",   LLC_TMP_STATE : " + StrLLCTmpReceiveDataState + ", LLC_TMP_ERROR : " + StrLLCTmpReceiveDataErrorCode + ", LLC_TMP_TEMP : " + StrLLCTmpReceiveDataTemperature)); //통신 상태 기록.
	if (StrLLCTmpState == "Stop")
	{
		m_LLC_Tmp_State = TMP_OFFLINE;
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);

		/////////////////////////////////////////////////////////////////
		// LLC TMP STOP -> 0 hz 일때 메세지 발생 하는지 확인 필요.
		/////////////////////////////////////////////////////////////////

		if (g_pIO->m_nERROR_MODE == RUN)
		{
			
		}
	}
	else if (StrLLCTmpState == "Starting")
	{
		m_LLC_Tmp_State = TMP_ACCELERATION;
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);
	}
	else if (StrLLCTmpState == "Breaking")
	{
		m_LLC_Tmp_State = TMP_DECELERATION;
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);
	}
	else if (StrLLCTmpState == "Running")
	{
		m_LLC_Tmp_State = TMP_NORMAL;
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);
	}
	else if (StrLLCTmpState == "Auto Tuning")
	{
		m_LLC_Tmp_State = TMP_AUTOTUNING;
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[2]);
	}
	else if (StrLLCTmpState == "Working Intlk")
	{
		m_LLC_Tmp_State = TMP_WORKINGINTLK;
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[2]);
	}
	else if (StrLLCTmpState == "Fail" || LLC_TMP_Error_On)
	{
		m_LLC_Tmp_State = TMP_ERROR;
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[2]);
	}
	
	return ret;
}

//int CTurboPumpDlg::GetUpdate()
//{
//	int ret = 0;
//	char Buff[10];
//
//
//		// TMP_MONITOR
//		Buff[0] = 0x02;
//		Buff[1] = 0x80;
//		Buff[2] = 0x32;
//		Buff[3] = 0x30;
//		Buff[4] = 0x35;
//		Buff[5] = 0x30;
//		Buff[6] = 0x03;
//		Buff[7] = 0x38;
//		Buff[8] = 0x34;
//
//		ret = SendData((LPSTR)(LPCTSTR)Buff, 500);
//
//		// TMP HZ READ
//		Buff[0] = 0x02; //STX
//		Buff[1] = 0x80; //ADDR
//		Buff[2] = 0x32; //WINDOW
//		Buff[3] = 0x30; //WINDOW
//		Buff[4] = 0x33; //WINDOW
//		Buff[5] = 0x30; //RD
//		Buff[6] = 0x03; //ETX
//		Buff[7] = 0x38; //CRC
//		Buff[8] = 0x32; //CRC
//
//		////////////////////////////////////////////////////////////////////////
//		//
//		//	STX 이후의 값을 EXT 까지 XOR 하여 나온 값의 아스키 코드 16진수로 할당.
//		//	Example
//		//		0x80 XOR 0x32 XOR 0x30 Xor 0x33 Xor 0x30 Xor 0x03 = 82
//		//		8 문자열의 아스키코드 16진수 값  0x38
//		//		2 문자열의 아스키코드 16진수 값  0x32
//		///////////////////////////////////////////////////////////////////////
//
//		ret = SendData((LPSTR)(LPCTSTR)Buff, 500);
//
//		//TMP Rpm Read [85]
//		Buff[0] = 0x02;
//		Buff[1] = 0x80;
//		Buff[2] = 0x32;
//		Buff[3] = 0x32;
//		Buff[4] = 0x36;
//		Buff[5] = 0x30;
//		Buff[6] = 0x03;
//		Buff[7] = 0x38;
//		Buff[8] = 0x35;
//
//		ret = SendData((LPSTR)(LPCTSTR)Buff, 500);
//
//
//		//TMP_ERROR CODE [87]
//		Buff[0] = 0x02;
//		Buff[1] = 0x80;
//		Buff[2] = 0x32;
//		Buff[3] = 0x30;
//		Buff[4] = 0x36;
//		Buff[5] = 0x30;
//		Buff[6] = 0x03;
//		Buff[7] = 0x38;
//		Buff[8] = 0x37;
//
//		ret = SendData((LPSTR)(LPCTSTR)Buff, 500);
//
//		//TMP_temprerature [85]
//		Buff[0] = 0x02;
//		Buff[1] = 0x80;
//		Buff[2] = 0x32;
//		Buff[3] = 0x30;
//		Buff[4] = 0x34;
//		Buff[5] = 0x30;
//		Buff[6] = 0x03;
//		Buff[7] = 0x38;
//		Buff[8] = 0x35;
//
//		ret = SendData((LPSTR)(LPCTSTR)Buff, 500);
//
//		// Controller Heatsink Temperature (°C) [81]
//		Buff[0] = 0x02;
//		Buff[1] = 0x80;
//		Buff[2] = 0x32;
//		Buff[3] = 0x31;
//		Buff[4] = 0x31;
//		Buff[5] = 0x30;
//		Buff[6] = 0x03;
//		Buff[7] = 0x38;
//		Buff[8] = 0x31;
//
//		ret = SendData((LPSTR)(LPCTSTR)Buff, 500);
//
//		//Controller Air Temperature (°C). [86]
//		Buff[0] = 0x02;
//		Buff[1] = 0x80;
//		Buff[2] = 0x32;
//		Buff[3] = 0x31;
//		Buff[4] = 0x36;
//		Buff[5] = 0x30;
//		Buff[6] = 0x03;
//		Buff[7] = 0x38;
//		Buff[8] = 0x36;
//
//		ret = SendData((LPSTR)(LPCTSTR)Buff, 500);
//
//
//		
//	return ret;
//}

int CTurboPumpDlg::GetStatus()
{
	int ret = TMP_NORMAL;

	switch (m_LLC_Tmp_State)
	{
	case TMP_ERROR:
		ret = TMP_ERROR;
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[2]);
		break;
	case TMP_OFFLINE :
		ret = TMP_OFFLINE;
		break;
	case TMP_NORMAL:
		ret = TMP_NORMAL;
		break;
	case TMP_ACCELERATION :
		ret = TMP_ACCELERATION;
		break;
	case  TMP_DECELERATION:
		ret = TMP_DECELERATION;
		break;
	case  TMP_AUTOTUNING:
		ret = TMP_AUTOTUNING;
		break;
	case  TMP_WORKINGINTLK:
		ret = TMP_WORKINGINTLK;
		break;
	default:
		break;
	}
	
	return ret;
}

void CTurboPumpDlg::OnBnClickedLlcTmpBtnOn()
{
	//START
	// RE : 02 80 06 03 38 35

	int ret = 0;

	//char Buff[10];
	//Buff[0] = 0x02;
	//Buff[1] = 0x80;
	//Buff[2] = 0x30;
	//Buff[3] = 0x30;
	//Buff[4] = 0x30;
	//Buff[5] = 0x31;
	//Buff[6] = 0x31;
	//Buff[7] = 0x03;
	//Buff[8] = 0x42;
	//Buff[9] = 0x33;
	//ret = SendData((LPSTR)(LPCTSTR)Buff, 1000);
	
	ret = g_pLLKTmp_IO->LLCTmp_On_Com();
	if (ret != 0)
	{
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[0]);

	}
	else
	{
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	}
}

void CTurboPumpDlg::OnBnClickedLlcTmpBtnOff()
{
	// STOP
	//  RE : 02 80 06 03 38 35
	int ret = 0;

	//char Buff[10];
	//Buff[0] = 0x02;
	//Buff[1] = 0x80;
	//Buff[2] = 0x30;
	//Buff[3] = 0x30;
	//Buff[4] = 0x30;
	//Buff[5] = 0x31;
	//Buff[6] = 0x30;
	//Buff[7] = 0x03;
	//Buff[8] = 0x42;
	//Buff[9] = 0x32;
	//ret = SendData((LPSTR)(LPCTSTR)Buff, 1000);

	ret = LLCTmp_Off_Com();

	if (ret != 0)
	{
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[0]);

	}
	else
	{
		((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
	}
}

int CTurboPumpDlg::LLC_Tmp_Off()
{

	int ret = 0;

	//BYTE Buff[10];
	//Buff[0] = 0x02;
	//Buff[1] = 0x80;
	//Buff[2] = 0x30;
	//Buff[3] = 0x30;
	//Buff[4] = 0x30;
	//Buff[5] = 0x31;
	//Buff[6] = 0x30;
	//Buff[7] = 0x03;
	//Buff[8] = 0x42;
	//Buff[9] = 0x32;
	//
	//ret = SendData(sizeof(Buff), Buff);

	ret = LLCTmp_Off_Com();

	if (ret != 0)
	{
		AfxMessageBox(_T("LLC TMP OFF 에 실패 하였습니다"));
		return RUN;
	}
	else return RUN_OFF;

}

int CTurboPumpDlg::LLC_Tmp_On()
{
	int ret = 0;
	//char Buff[10];
	//
	//Buff[0] = 0x02;
	//Buff[1] = 0x80;
	//Buff[2] = 0x30;
	//Buff[3] = 0x30;
	//Buff[4] = 0x30;
	//Buff[5] = 0x31;
	//Buff[6] = 0x31;
	//Buff[7] = 0x03;
	//Buff[8] = 0x42;
	//Buff[9] = 0x33;

//	ret = SendData((LPSTR)(LPCTSTR)Buff, 1000);

	ret = LLCTmp_On_Com();

	if (ret != 0)
	{
		return RUN_OFF;
	}
	else
	{
		return RUN;
	}

}

void CTurboPumpDlg::GetStatus_Logging()
{
	//SetDlgItemText(IDC_LLC_TMP_STATIC, ReData_value);
	//SetDlgItemText(IDC_LLC_TMP_RPM, ReData_value_rpm);
	//SetDlgItemText(IDC_LLC_TMP_STATIC2, ReData_state);
	//SetDlgItemText(IDC_LLC_TMP_TEMP1, ReData_Tamp);
	//SetDlgItemText(IDC_LLC_TMP_TEMP2, ReData_Tamp_Controller_Heatsink);
	//SetDlgItemText(IDC_LLC_TMP_TEMP3, ReData_Tamp_Controller_air);
	//SetDlgItemText(IDC_LLC_TMP_STATIC4, ReData_Error);

	SaveLogFile("LLC_TMP_LOG", _T((LPSTR)(LPCTSTR)(" LLC_TMP_HZ : " + StrLLCTmpReceiveDataHz + ",   LLC_TMP_STATE : " + StrLLCTmpReceiveDataState + ", LLC_TMP_ERROR : " + StrLLCTmpReceiveDataErrorCode + ", LLC_TMP_TEMP : " + StrLLCTmpReceiveDataTemperature))); //통신 상태 기록.
}

void CTurboPumpDlg::LLCTmpSendUpdate()
{
	int ret = 0;

	ret = LLCTmp_Monitor_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Monitor_Com() _ Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Monitor_Com() ")));	//통신 상태 기록.
	}

	ret = LLCTmp_HzRead_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_HzRead_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_HzRead_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_RpmRead_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_RpmRead_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_RpmRead_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_ErrorCode_Check_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_ErrorCode_Check_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_ErrorCode_Check_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_Temprerature_Check_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Temprerature_Check_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Temprerature_Check_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_Controller_Heatsink_Temprerature_Check_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Controller_Heatsink_Temprerature_Check_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Controller_Heatsink_Temprerature_Check_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_Controller_Air_Temprerature_Check_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Controller_Air_Temprerature_Check_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Controller_Air_Temprerature_Check_Com() ")));	//통신 상태 기록.
	}

}


int CTurboPumpDlg::ReceiveData(char *lParam, DWORD dwRead)
{
	int nRet = 0;
	CString		StrLogTemp;

	memset(m_BYTE, '\0', 15);
	memcpy(m_BYTE, lParam, sizeof(BYTE) * 15);

	CString StrTemp = TEXT("");
	CString StrRecevieData = TEXT("");
	CString StrRecevieDataMid = TEXT("");
	CString StrRevevieDataResult = TEXT("");

	for (int i = 0; i < 15; i++)            //String으로 변환
	{
		if (m_BYTE[i] == 0) StrTemp = "0 ";
		else StrTemp.Format("%02x", m_BYTE[i]);
		StrRecevieData += StrTemp;
	}

	SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrRecevieData)));	//통신 상태 기록.

	if (m_BYTE[2] == 0x06)
	{
		StrLLCTmpReceiveDataState = "실행 완료"; // 명령수행완료 리턴 값
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x35)
	{
		if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x30)
		{
			StrLLCTmpReceiveDataState = "Stop";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x31)
		{
			StrLLCTmpReceiveDataState = "Working Intlk";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x32)
		{
			StrLLCTmpReceiveDataState = "Starting";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x33)
		{
			StrLLCTmpReceiveDataState = "Auto Tuning";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x34)
		{
			StrLLCTmpReceiveDataState = "Breaking";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x35)
		{
			StrLLCTmpReceiveDataState = "Running";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x36)
		{
			StrLLCTmpReceiveDataState = "Fail";
		}
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.

	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x33) // hz
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataHz = StrRevevieDataResult + " hz"; // 현재 Hz 값
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataHz)));	//통신 상태 기록.

	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x32 && m_BYTE[4] == 0x36) // rpm
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataRpm = StrRevevieDataResult + " Rpm"; // 현재 Hz 값

		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataRpm)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x36) // Error code view
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataErrorCode = StrRevevieDataResult; // 현재 Error Code 값

		if (StrLLCTmpReceiveDataErrorCode == "0000000")
		{
			StrLLCTmpReceiveDataErrorState = _T("No Error");
			LLC_TMP_Error_On = FALSE;
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000001")
		{
			StrLLCTmpReceiveDataErrorState = _T("No Connection");
			LLC_TMP_Error_On = FALSE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000002") // HEX 2 , DEC 2, BIN 0000 0010
		{
			StrLLCTmpReceiveDataErrorState = _T("Pump OverTemp");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000004")// HEX 4 , DEC 4, BIN 0000 0100
		{
			StrLLCTmpReceiveDataErrorState = _T("Controll OverTemp");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000008")// HEX 8 , DEC 8, BIN 0000 1000
		{
			StrLLCTmpReceiveDataErrorState = _T("Power Fail");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000016") // HEX 10 , DEC 16, BIN 0001 0000
		{
			StrLLCTmpReceiveDataErrorState = _T("Aux Fail");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000032")// HEX 20 , DEC 32, BIN 0010 0000
		{
			StrLLCTmpReceiveDataErrorState = _T("Over Voltage");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000064")// HEX 40 , DEC 64, BIN 0100 0000
		{
			StrLLCTmpReceiveDataErrorState = _T("Over Voltage");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000128")// HEX 80 , DEC 128, BIN 1000 0000
		{
			StrLLCTmpReceiveDataErrorState = _T("Too High Load");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else
		{
			StrLLCTmpReceiveDataErrorState = _T("Error 발생 ( Define 되지 않은 Error) ");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}

		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataErrorCode + " ::: " + StrLLCTmpReceiveDataErrorState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x34) // TMP_temprerature 
	{

		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataTemperature = StrRevevieDataResult; // 현재 온도 값
		StrLLCTmpReceiveDataTemperature += _T(" ℃");
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataTemperature)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x31 && m_BYTE[4] == 0x31) //  Controller Heatsink Temperature 
	{

		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataTemperatureHeatsink = StrRevevieDataResult; // 현재 온도 값
		StrLLCTmpReceiveDataTemperatureHeatsink += " ℃";
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataTemperatureHeatsink)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x31 && m_BYTE[4] == 0x36) // Controller Air Temperature (°C). [86]
	{

		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataTemperatureAir = StrRevevieDataResult; // 현재 온도 값
		StrLLCTmpReceiveDataTemperatureAir += " ℃";
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataTemperatureAir)));	//통신 상태 기록.
	}
	else
	{
		StrLLCTmpReceiveDataHz = "Send Error";
		StrLLCTmpReceiveDataState = "Send Error";
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): UnKnown " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}

	StrLogTemp.Format(_T("LLC_TMP_HZ : %s , LLC_TMP_STATE : %s , LLC_TMP_ERROR : %s , LLC_TMP_TEMP : %s"), StrLLCTmpReceiveDataHz, StrLLCTmpReceiveDataState, StrLLCTmpReceiveDataErrorCode, StrLLCTmpReceiveDataTemperature);
	SaveLogFile("LLC_TMP_LOG", StrLogTemp); //통신 상태 기록.

	return nRet;
}