#include "stdafx.h"
#include "Include.h"
#include "Extern.h"



CAutoProcess::CAutoProcess()
{
	m_nRotateAngle		= 0;
	m_bUseRotate		= FALSE;
	m_bUseFlip			= FALSE;
	m_bAtmState			= FALSE;
	m_bLoadingSeq		= TRUE;

	m_nProcessErrorCode =  0;

	m_bThreadStop	= TRUE;
	m_pAutoThread	= NULL;	

	m_hLLC_Vent_Finish = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hLLC_Pump_Finish = CreateEvent(NULL, TRUE, FALSE, NULL);

	CurrentProcess = NOTHING;
}

CAutoProcess::~CAutoProcess()
{
	if (m_hLLC_Vent_Finish != NULL)
		CloseHandle(m_hLLC_Vent_Finish);
	if (m_hLLC_Pump_Finish != NULL)
		CloseHandle(m_hLLC_Pump_Finish);
}

int CAutoProcess::Initialize()
{
	int ret = 0;

	if (g_pLog == NULL || g_pWarning == NULL)
		return -999;

	//g_pWarning->m_strWarningMessageVal = "System 초기화 중입니다. 잠시 기다려 주세요!";
	//g_pWarning->UpdateData(false);
	//g_pWarning->ShowWindow(SW_SHOW);

	//1.모든 Thread를 중지 시킨다.

	//2.모든 통신 Port를 초기화 시킨다.

	//3.모든 진공 Valve 닫는다.(Interlock 상황 고려) 

	//4.모든 H/W Module을 초기화 시킨다.(MTS,VMTR,Stage,AF,...)

	//g_pWarning->ShowWindow(SW_HIDE);

	return ret;
}

int CAutoProcess::Is_HWModule_OK()
{
	int ret = 0;

	return ret;
}

int CAutoProcess::ThreadStop()
{
	int ret = 0;

	if (g_pLog == NULL || g_pWarning == NULL)
		return -999;

	//1.Thread Stop(Loading/Unloading/Pumping/Venting/Align/ReviewProcess/ZInterlock)

	m_pAutoThread = NULL;

	return ret;
}

void CAutoProcess::StopSequence()
{
	CString log;
	m_bThreadStop = TRUE;
}

int CAutoProcess::MaskLoadingStart(BOOL bUseFlip, BOOL bUseRotate, int nAngle)
{
	int ret = 0; CString log;

	if (g_pLog == NULL || g_pWarning == NULL)
		return -999;	//Error Code정의 후 변경

	//1.THREAD가 이미 가동중이면 이를 차단..
	if (m_pAutoThread != NULL)
	{
		log = _T("이미 다른 작업이 수행 중이므로 Mask를 Loading할 수 없습니다!");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_ALREADY_JOB_WORKING;
	}

	//20200129 jkseo, 진공인지 대기인지 확인 후 로딩/언로딩 시 진공 시퀀스 동작 여부 결정
	if (g_pIO->Is_MC_Atm_Check() == TRUE && g_pIO->Is_MC_Vac_Check() == FALSE && g_pIO->Is_LLC_Atm_Check() == TRUE)
	{
		log = _T("대기 상태에서 로딩/언로딩 시퀀스 진행");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		m_bAtmState = TRUE;
	}
	else if (g_pIO->Is_MC_Atm_Check() == FALSE && g_pIO->Is_MC_Vac_Check() == TRUE)
	{
		log = _T("진공 상태에서 로딩/언로딩 시퀀스 진행");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		m_bAtmState = FALSE;
	}
	else
	{
		log = _T("대기 혹은 진공 상태인지 확인 할 수 없습니다!");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_UNKNOWN_VACUUM_STATE;
	}

	//현재 마스크 위치 확인해서 MaskState 업데이트
	m_bUseFlip		= bUseFlip;
	m_bUseRotate	= bUseRotate;
	m_nRotateAngle	= nAngle;
	CurrentProcess  = ON_MTS_POD_LOADING;
	m_bLoadingSeq	= TRUE;
	
	m_pAutoThread = AfxBeginThread(MaskAutoThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);	//한번 더 수행해본다.
	if (m_pAutoThread == NULL)
	{
		log = _T("MaskAutoThread 실행 실패!");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_FAIL_LOADING_PROCESS;
	}
	else
	{
		log = _T("MaskAutoThread 실행 성공!");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
	}

	return ret;
}

int CAutoProcess::MaskUnloadingStart(BOOL bUseFlip, BOOL bUseRotate, int nAngle)
{
	int ret = 0; CString log;

	if (g_pLog == NULL || g_pWarning == NULL)
		return -999;	//Error Code정의 후 변경

	//1.THREAD가 이미 가동중이면 이를 차단..
	if (m_pAutoThread != NULL)
	{
		log = _T("이미 다른 작업이 수행 중이므로 Mask를 Unloading할 수 없습니다!");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_ALREADY_JOB_WORKING;
	}

	//20200129 jkseo, 진공인지 대기인지 확인 후 로딩/언로딩 시 진공 시퀀스 동작 여부 결정
	if (g_pIO->Is_MC_Atm_Check() == TRUE && g_pIO->Is_MC_Vac_Check() == FALSE && g_pIO->Is_LLC_Atm_Check() == TRUE)
	{
		log = _T("대기 상태에서 로딩/언로딩 시퀀스 진행");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		m_bAtmState = TRUE;
	}
	else if (g_pIO->Is_MC_Atm_Check() == FALSE && g_pIO->Is_MC_Vac_Check() == TRUE)
	{
		log = _T("진공 상태에서 로딩/언로딩 시퀀스 진행");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		m_bAtmState = FALSE;
	}
	else
	{
		log = _T("대기 혹은 진공 상태인지 확인 할 수 없습니다!");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_UNKNOWN_VACUUM_STATE;
	}

	//현재 마스크 위치 확인해서 MaskState 업데이트
	m_bUseFlip		= bUseFlip;
	m_bUseRotate	= bUseRotate;
	m_nRotateAngle	= nAngle;
	CurrentProcess  = ON_CHUCK;
	m_bLoadingSeq	= FALSE;
	
	m_pAutoThread = AfxBeginThread(MaskAutoThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);	//한번 더 수행해본다.
	if (m_pAutoThread == NULL)
	{
		log = _T("MaskAutoThread 실행 실패!");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_FAIL_UNLOADING_PROCESS;
	}
	else
	{
		log = _T("MaskAutoThread 실행 성공!");
		g_pLog->Display(0, log);
		SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
	}

	return 0;
}


UINT CAutoProcess::MaskAutoThread(LPVOID pParam)
{
	CString log;

	if (g_pLog == NULL || g_pWarning == NULL)
		return -999;	//Error Code정의 후 변경

	CAutoProcess*  g_pAP = (CAutoProcess*)pParam;

	g_pAP->m_bThreadStop = FALSE;

	log = _T("MaskAutoThread 시작!");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	g_pAP->m_nProcessErrorCode = 0;

	//RECOVERY FUNCTION
	if (g_pAP->CurrentProcess != (TransferState)g_pConfig->m_nPreviousProcess)
	{
		if (g_pAP->m_bLoadingSeq == TRUE)
			g_pAP->m_nProcessErrorCode = g_pAP->Loading_Recovery_Work();
		else
			g_pAP->m_nProcessErrorCode = g_pAP->Unloading_Recovery_Work();

		if (g_pAP->m_nProcessErrorCode != 0)
		{
			log = _T("Recovery Fail.");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

			g_pAP->m_pAutoThread = NULL;
			return g_pAP->m_nProcessErrorCode;
		}
	}
	else
	{
		if (g_pAP->m_bLoadingSeq == TRUE)
		{
			g_pConfig->m_bMtsRotateDone_Flag	= FALSE;
			g_pConfig->m_bMtsFlipDone_Flag		= FALSE;
			g_pConfig->SaveMtsRecoveryData();
		}
	}

	while (!g_pAP->m_bThreadStop)
	{
		Sleep(100);
		
		g_pAP->m_nProcessErrorCode = g_pAP->Auto_Loop();

		//1.MaskState History 기록 : 마지막 상태를 프로그램 시작 시 읽어와서 추후 Recovery에서 활용하자.

		if (g_pAP->m_nProcessErrorCode != 0 || g_pAP->CurrentProcess == NOTHING)
		{
			g_pAP->m_bThreadStop = TRUE;
			log.Format("MaskAutoThread Fail! Error Code %d번", g_pAP->m_nProcessErrorCode);
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		}

		if (g_pAP->m_nProcessErrorCode == 0 && g_pAP->CurrentProcess == LOADING_COMPLETE)
		{
			g_pLightCtrl->LightOn();
			g_pConfig->SaveCurrentProcess(ON_CHUCK);

			g_pAP->m_bThreadStop = TRUE;
			log = _T("MaskLoading 완료!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

			log = _T("MaskAutoThread 정상 종료!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

			g_pAP->Loading_Complete();	//Loading 완료되면 해야할 것들(align data 초기화 등) by smchoi
		}

		if (g_pAP->m_nProcessErrorCode == 0 && g_pAP->CurrentProcess == UNLOADING_COMPLETE)
		{
			g_pLightCtrl->LightOff();
			g_pConfig->SaveCurrentProcess(ON_MTS_POD_LOADING);

			g_pAP->m_bThreadStop = TRUE;
			log = _T("MaskUnloading 완료!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			
			log = _T("MaskAutoThread 정상 종료!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		}
	}

	g_pAP->m_pAutoThread = NULL;

	log = _T("MaskAutoThread 완료!");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return 0;
}

int CAutoProcess::Auto_Loop()
{
	int	ret = 0;

	switch (CurrentProcess)
	{
	case NOTHING:
		g_pLog->Display(0, _T("Mask Nothing!"));
		break;
	case ON_MTS_POD_LOADING:
		ret = Loading_PreWork();  //VENTING
		if (ret == 0)
			CurrentProcess = ON_ATRHAND_LOADING;
		break;
	case ON_ATRHAND_LOADING:
		ret = Loading_PODtoATR();	//POD->ATR
		if (ret == 0)
		{
			if (m_bUseFlip == TRUE)
				CurrentProcess = ON_FLIPPER_LOADING;
			else
			{
				if (m_bUseRotate == TRUE)
					CurrentProcess = ON_ROTATOR_LOADING;
				else
					CurrentProcess = ON_LLC_LOADING;
			}
		}
		break;
	case ON_FLIPPER_LOADING:
		ret = Loading_ATRtoFLIPPER();
		if (ret == 0)
			CurrentProcess = ON_FLIPPER_AFTERFLIP_LOADING;
		break;
	case ON_FLIPPER_AFTERFLIP_LOADING:
		ret = Loading_MASK_Flip();
		if (ret == 0)
			CurrentProcess = ON_ATR_AFTERFLIP_LOADING;
		break;
	case ON_ATR_AFTERFLIP_LOADING:
		ret = Loading_FLIPPERtoATR();
		if (ret == 0)
		{
			if (m_bUseRotate == TRUE)
				CurrentProcess = ON_ROTATOR_LOADING;
			else
				CurrentProcess = ON_LLC_LOADING;
		}
		break;
	case ON_ROTATOR_LOADING:
		ret = Loading_ATRtoROTATOR();	//ATR->ROTATOR
		if(ret == 0)
			CurrentProcess = ON_ROTATOR_AFTERROTATE_LOADING;
		break;
	case ON_ROTATOR_AFTERROTATE_LOADING:
		ret = Loading_MASK_Rotate(m_nRotateAngle);	//ROTATOR
		if (ret == 0)
			CurrentProcess = ON_ATR_AFTERROTATE_LOADING;
		break;
	case ON_ATR_AFTERROTATE_LOADING:
		ret = Loading_ROTATORtoATR();	//ROTATOR->ATR
		if (ret == 0)
			CurrentProcess = ON_LLC_LOADING;
		break;
	case ON_LLC_LOADING:
		ret = Loading_ATRtoLLC(); //ATR -> LLC
		if (ret == 0)
			CurrentProcess = ON_LLC_VACUUM_LOADING;
		break;
	case ON_LLC_VACUUM_LOADING:
		ret = Loading_LLC_Rough();  //MASK IN LLC
		if (ret == 0)
			CurrentProcess = ON_VMTRHAND_LOADING;
		break;
	case ON_VMTRHAND_LOADING:
		ret = Loading_LLCtoVMTR();	//MASK LLC -> VMTR ROBOT
		if (ret == 0)
			CurrentProcess = ON_CHUCK_LOADING;
		break;
	case ON_CHUCK_LOADING:
		ret = Loading_VMTRtoCHUCK();	//MASK VMTR ROBOT -> CHUCK
		if (ret == 0)
			CurrentProcess = LOADING_COMPLETE;
		break;
	case LOADING_COMPLETE:
		Loading_Complete();	//MASK ON CHUCK
		break;
	case ON_CHUCK:
 		ret = Unloading_PreWork();	//PUMPING
		if (ret == 0)
			CurrentProcess = ON_VMTR_UNLOADING;
		break;
	case ON_VMTR_UNLOADING:
		ret = Unloading_CHUCKtoVMTR();	//MASK CHUCK -> VMTR
		if (ret == 0)
			CurrentProcess = ON_LLC_UNLOADING;
		break;
	case ON_LLC_UNLOADING:
		ret = Unloading_VMTRtoLLC();	//MASK VMTR -> LLC
		if (ret == 0)
			CurrentProcess = ON_LLC_VENT_UNLOADING;
		break;
	case ON_LLC_VENT_UNLOADING:
		ret = Unloading_LLC_Vent();	//MASK LLC, VENTING
		if (ret == 0)
			CurrentProcess = ON_ATRHAND_UNLOADING;
		break;
	case ON_ATRHAND_UNLOADING:  //MASK LLC -> ATR, PUMPING
		ret = Unloading_LLCtoATR();
		if (ret == 0)
			CurrentProcess = ON_LLC_VACUUM_UNLOADING;
		break;
	case ON_LLC_VACUUM_UNLOADING:
		ret = Unloading_LLC_Rough();
		if (ret == 0)
		{
			if (m_bUseRotate == TRUE)
				CurrentProcess = ON_ROTATOR_UNLOADING;
			else
			{
				if (m_bUseFlip == TRUE)
					CurrentProcess = ON_FLIPPER_UNLOADING;
				else
					CurrentProcess = ON_MTS_POD_UNLOADING;
			}
		}
		break;
	case ON_ROTATOR_UNLOADING:	//MASK ATR -> ROTATOR
		ret = Unloading_ATRtoROTATOR();
		if (ret == 0)
			CurrentProcess = ON_ROTATOR_AFTERROTATE_UNLOADING;
		break;
	case ON_ROTATOR_AFTERROTATE_UNLOADING: //ROTATE
		ret = Unloading_MASK_Rotate(m_nRotateAngle);
		if (ret == 0)
			CurrentProcess = ON_ATR_AFTERROTATE_UNLOADING;
		break;
	case ON_ATR_AFTERROTATE_UNLOADING: //MASK ROTATOR -> ATR
		ret = Unloading_ROTATORtoATR();
		if (ret == 0)
		{
			if (m_bUseFlip == TRUE)
				CurrentProcess = ON_FLIPPER_UNLOADING;
			else
				CurrentProcess = ON_MTS_POD_UNLOADING;
		}
		break;
	case ON_FLIPPER_UNLOADING:
		ret = Unloading_ATRtoFLIPPER();
		if (ret == 0)
			CurrentProcess = ON_FLIPPER_AFTERFLIP_UNLOADING;
		break;
	case ON_FLIPPER_AFTERFLIP_UNLOADING:
		ret = Unloading_MASK_Flip();
		if (ret == 0)
			CurrentProcess = ON_ATR_AFTERFLIP_UNLOADING;
		break;
	case ON_ATR_AFTERFLIP_UNLOADING:
		ret = Unloading_FLIPPERtoATR();
		if (ret == 0)
			CurrentProcess = ON_MTS_POD_UNLOADING;
		break;
	case ON_MTS_POD_UNLOADING:
		ret = Unloading_ATRtoPOD();
		if (ret == 0)
			CurrentProcess = UNLOADING_COMPLETE;
		break;
	case UNLOADING_COMPLETE:	//MASK ATR -> POD
		Unloading_Complete();
		break;
	default:
		break;
	}

	//20200203 jkseo, 현재 진행 프로세스 저장
	g_pConfig->SaveCurrentProcess(CurrentProcess);

	return ret;
}

int CAutoProcess::Loading_Recovery_Work()
{
	CString log;

	int ret = 0; 
	int nMaskPosition = 0;
	int nMaskCount	  = 0;

	log = _T("Function : Loading_Recovery_Work Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	//1. 저장된 MaskState 읽어오고
	//2. 센싱된 마스크 위치를 확인 (CHUCK의 마스크는 로딩포지션 이동 필요)
	//3. 1,2 상태를 보고 시퀀스 진행
	//4. 수동으로 빼야할 상황이면 MaskState 값은????

	ret = g_pAP->Is_HWModule_OK();
	if (ret != 0)
		return ret;

	//ret = g_pAdam->Stop();
	//if (ret != 0)
	//	return ret;

	ret = g_pScanStage->Stop_Scan();
	if (ret != 0)
	{
		log = _T("Error : Stop scan stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	ret = g_pScanStage->Move_Origin();
	if (ret != 0)
	{
		log = _T("Error : Move origin scan stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	ret = g_pNavigationStage->Move_Loading_Position();
	if (ret != 0)
	{
		log = _T("Error : Move loading position navigation stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		log = _T("Error : Not loading position!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SYSTEM_STAGE_NOT_LOADING_POS;
	}

	ret = g_pIO->Is_Mask_Check_OnChuck();
	if (ret != MASK_NONE)
	{
		log = _T("Exist mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = CHUCK;
		++nMaskCount;
	}

	if (g_pIO->Is_VAC_Robot_Arm_Retract() != TRUE)
	{
		log = _T("Error : Vac robot not retracted!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_VAC_ROBOT_NOT_RETRACTED;
	}

	ret = g_pIO->SetTriggerCamSensor(TRUE);
	if (ret != 0)
	{
		log = _T("Error : Cam Sensor trigger on!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
	}

	Sleep(500);

	if (g_pIO->Is_Mask_OnVMTR() != FALSE)
	{
		log = _T("Exist mask on vac robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = VACUUM_ROBOT_HAND;
		++nMaskCount;
	}

	ret = g_pIO->SetTriggerCamSensor(FALSE);
	if (ret != 0)
	{
		log = _T("Error : Cam Sensor trigger off!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
	}

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_NONE)
	{
		log = _T("Exist mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = LLC;
		++nMaskCount;
	}

	if (g_pEfem->Is_POD_OnLPM() != TRUE)
	{
		log = _T("Error : No pod on LPM!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_POD_ON_LPM;
	}

	if (g_pEfem->Is_POD_Open() != TRUE)
	{
		ret = g_pEfem->Load_POD();
		if (ret != 0)
		{
			log = _T("Error : Load pod!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	ret;
		}
	}

	if (g_pEfem->Is_MASK_InPOD() != FALSE)
	{
		log = _T("Exist mask in pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = MTS_POD;
		++nMaskCount;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = MTS_ROBOT_HAND;
		++nMaskCount;
	}

	if (g_pEfem->Is_MASK_InFlipper() != FALSE)
	{
		log = _T("Exist mask on mts flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = MTS_FLIPPER;
		++nMaskCount;
	}

	if (g_pEfem->Is_MASK_InRotator() != FALSE)
	{
		log = _T("Exist mask on mts rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = MTS_ROTATOR;
		++nMaskCount;
	}

	if (nMaskCount > 1)
	{
		log.Format("Detected %d Mask in equipment.", nMaskCount);
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_DETECTED_SEVERAL_MASK_IN_EQ;
	}
	
	if (nMaskCount == 0)
	{
		log.Format("No mask in equipment.", nMaskCount);
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_IN_EQ;
	}
	
	if (nMaskPosition == CHUCK)
		return SEQ_EXIST_MASK_ON_CHUCK;

	if (nMaskPosition != g_pConfig->m_nMaterialLocation)
	{
		if (IDYES != AfxMessageBox("저장된 Mask 위치와 센싱된 Mask 위치가 다릅니다. 센싱된 Mask 위치를 기준으로 로딩을 진행할까요?", MB_YESNO))
		{
			log = _T("Cancel loading sequence!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return -999;
		}
	}

	if (nMaskPosition == MTS_POD)
	{
		CurrentProcess = ON_MTS_POD_LOADING;
	}
	else if (nMaskPosition == MTS_ROBOT_HAND)
	{
		if (m_bUseFlip == TRUE)
		{
			if (g_pConfig->m_bMtsFlipDone_Flag == TRUE)	//Flip 동작 완료한 경우
			{
				if (m_bUseRotate == TRUE)
				{
					if (g_pConfig->m_bMtsRotateDone_Flag == TRUE)
						CurrentProcess = ON_LLC_LOADING;
					else
						CurrentProcess = ON_ROTATOR_LOADING;
				}
				else
					CurrentProcess = ON_LLC_LOADING;
			}	//Flip 동작 완료하지 않은 경우
			else
				CurrentProcess = ON_FLIPPER_LOADING;
		}
		else
		{
			if (m_bUseRotate == TRUE)
			{
				if (g_pConfig->m_bMtsRotateDone_Flag == TRUE)
					CurrentProcess = ON_LLC_LOADING;
				else
					CurrentProcess = ON_ROTATOR_LOADING;
			}
			else
				CurrentProcess = ON_LLC_LOADING;
		}
	}
	else if (nMaskPosition == MTS_ROTATOR)
	{
		if (m_bUseRotate == TRUE)
		{
			if (g_pConfig->m_bMtsRotateDone_Flag == TRUE)
				CurrentProcess = ON_ATR_AFTERROTATE_LOADING;
			else
				CurrentProcess = ON_ROTATOR_AFTERROTATE_LOADING;
		}
		else
			CurrentProcess = ON_ATR_AFTERROTATE_LOADING;
	}
	else if (nMaskPosition == MTS_FLIPPER)
	{
		if (m_bUseFlip == TRUE)
		{
			if (g_pConfig->m_bMtsFlipDone_Flag == TRUE)
				CurrentProcess = ON_ATR_AFTERFLIP_LOADING;
			else
				CurrentProcess = ON_FLIPPER_AFTERFLIP_LOADING;
		}
		else
			CurrentProcess = ON_ATR_AFTERFLIP_LOADING;
	}
	else if (nMaskPosition == LLC)
	{
		CurrentProcess = ON_LLC_VACUUM_LOADING;
	}
	else if (nMaskPosition == VACUUM_ROBOT_HAND)
	{
		CurrentProcess = ON_CHUCK_LOADING;
	}
		
	log = _T("Function : Loading_Recovery_Work End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return 0;
}

int CAutoProcess::Unloading_Recovery_Work()
{
	CString log;

	int ret = 0;
	int nMaskPosition = 0;
	int nMaskCount = 0;

	log = _T("Function : Unloading_Recovery_Work Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);


#pragma region Mask 센싱 위치 찾기

	ret = g_pAP->Is_HWModule_OK();
	if (ret != 0) return ret;

	//ret = g_pAdam->Stop();
	//if (ret != 0)
	//	return ret;

	ret = g_pScanStage->Stop_Scan();
	if (ret != 0)
	{
		log = _T("Error : Stop scan stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	ret = g_pScanStage->Move_Origin();
	if (ret != 0)
	{
		log = _T("Error : Move origin scan stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	ret = g_pNavigationStage->Move_Loading_Position();
	if (ret != 0)
	{
		log = _T("Error : Move loading position navigation stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		log = _T("Error : Not loading position!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SYSTEM_STAGE_NOT_LOADING_POS;
	}

	ret = g_pIO->Is_Mask_Check_OnChuck();
	if (ret != MASK_NONE)
	{
		log = _T("Exist mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = CHUCK;
		++nMaskCount;
	}

	ret = g_pIO->Is_MC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		log = _T("Error : Tilt mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_TILT_MASK_ON_CHUCK;
	}

	if (g_pIO->Is_VAC_Robot_Arm_Retract() != TRUE)
	{
		log = _T("Error : Vac robot not retracted!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_VAC_ROBOT_NOT_RETRACTED;
	}

	ret = g_pIO->SetTriggerCamSensor(TRUE);
	if (ret != 0)
	{
		log = _T("Error : Cam Sensor trigger on!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
	}

	Sleep(500);

	if (g_pIO->Is_Mask_OnVMTR() != FALSE)
	{
		log = _T("Exist mask on vac robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = VACUUM_ROBOT_HAND;
		++nMaskCount;
	}

	ret = g_pIO->SetTriggerCamSensor(FALSE);
	if (ret != 0)
	{
		log = _T("Error : Cam Sensor trigger off!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
	}

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_NONE)
	{
		log = _T("Exist mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = LLC;
		++nMaskCount;
	}

	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		log = _T("Error : Tilt mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_TILT_MASK_ON_LLC;
	}

	if (g_pEfem->Is_POD_OnLPM() != TRUE)
	{
		log = _T("Error : No pod on LPM!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_POD_ON_LPM;
	}

	if (g_pEfem->Is_POD_Open() != TRUE)
	{
		ret = g_pEfem->Load_POD();
		if (ret != 0)
		{
			log = _T("Error : Load pod!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	ret;
		}
	}

	if (g_pEfem->Is_MASK_InPOD() != FALSE)
	{
		log = _T("Exist mask in pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		nMaskPosition = MTS_POD;
		++nMaskCount;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = MTS_ROBOT_HAND;
		++nMaskCount;
	}

	if (g_pEfem->Is_MASK_InFlipper() != FALSE)
	{
		log = _T("Exist mask on mts flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = MTS_FLIPPER;
		++nMaskCount;
	}

	if (g_pEfem->Is_MASK_InRotator() != FALSE)
	{
		log = _T("Exist mask on mts rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		nMaskPosition = MTS_ROTATOR;
		++nMaskCount;
	}

	if (nMaskCount == 0)
	{
		log.Format("No mask in equipment.");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_IN_EQ;
	}
	else if (nMaskCount > 1)
	{
		log.Format("Detected %d Mask in equipment.", nMaskCount);
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_DETECTED_SEVERAL_MASK_IN_EQ;
	}

	if (nMaskPosition == MTS_POD)
		return SEQ_EXIST_MASK_IN_POD;

#pragma endregion

	if (nMaskPosition != g_pConfig->m_nMaterialLocation)
	{
		if (IDYES != AfxMessageBox("저장된 Mask 위치와 센싱된 Mask 위치가 다릅니다. 센싱된 Mask 위치를 기준으로 언로딩을 진행할까요?", MB_YESNO))
		{
			log = _T("Cancel loading sequence!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return -999;
		}
	}

	if (nMaskPosition == MTS_ROBOT_HAND)
	{
		if (m_bUseRotate == TRUE)
		{
			if (g_pConfig->m_bMtsRotateDone_Flag == TRUE)
				CurrentProcess = ON_ROTATOR_UNLOADING;
			else
			{
				if (m_bUseFlip == TRUE)
				{
					if (g_pConfig->m_bMtsFlipDone_Flag == TRUE)
						CurrentProcess = ON_FLIPPER_UNLOADING;
					else
						CurrentProcess = ON_MTS_POD_UNLOADING;
				}
				else
					CurrentProcess = ON_MTS_POD_UNLOADING;
			}
		}
		else
		{
			if (m_bUseFlip == TRUE)
			{
				if (g_pConfig->m_bMtsFlipDone_Flag == TRUE)
					CurrentProcess = ON_FLIPPER_UNLOADING;
				else
					CurrentProcess = ON_MTS_POD_UNLOADING;
			}
			else
				CurrentProcess = ON_MTS_POD_UNLOADING;
		}
	}
	else if (nMaskPosition == MTS_ROTATOR)
	{
		if (m_bUseRotate == TRUE)
		{
			if (g_pConfig->m_bMtsRotateDone_Flag == TRUE)
				CurrentProcess = ON_ROTATOR_AFTERROTATE_UNLOADING;
			else
				CurrentProcess = ON_ATR_AFTERROTATE_UNLOADING;
		}
		else
			CurrentProcess = ON_ATR_AFTERROTATE_UNLOADING;
	}
	else if (nMaskPosition == MTS_FLIPPER)
	{
		if (m_bUseFlip == TRUE)
		{
			if (g_pConfig->m_bMtsFlipDone_Flag == TRUE)
				CurrentProcess = ON_FLIPPER_AFTERFLIP_UNLOADING;
			else
				CurrentProcess == ON_ATR_AFTERFLIP_UNLOADING;
		}
		else
			CurrentProcess = ON_ATR_AFTERFLIP_UNLOADING;
	}
	else if (nMaskPosition == LLC)
	{
		CurrentProcess = ON_LLC_VENT_UNLOADING;
	}
	else if (nMaskPosition == VACUUM_ROBOT_HAND)
	{
		CurrentProcess = ON_LLC_UNLOADING;
	}
	else if (nMaskPosition == CHUCK)
	{
		CurrentProcess = ON_VMTR_UNLOADING;
	}

	log = _T("Function : Unloading_Recovery_Work End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////// Mask Loading Operation /// ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
int CAutoProcess::Loading_PreWork()
{
	int ret = 0; CString log;

	//1. H/W 모듈들 정상상태인지 확인(ADAM,Scan Stage,Navigation Stage,VMTR,MTS,Vacuum Gauge,TMP,Drypump,OM Module 이상유무 확인)
	//2. ADAM과의 통신 정지
	//3. Scan Stage 정지시키고, 원점 좌표로 이동.
	//4. Navigation Stage Load Position으로 이동 후 Mask 없음 확인
	//5. MTS에 POD 있는지 확인
	//6. LLC Venting & LLC Gate Valve Open

	log = _T("Function : Loading_PreWork Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pIO == NULL || g_pEfem == NULL || g_pVP == NULL || g_pAdam == NULL)
		return	-999;	//추후 Error Code 정의

	ret = g_pAP->Is_HWModule_OK();
	if (ret != 0)
		return ret;

	//ret = g_pAdam->Stop();
	//if (ret != 0)
	//	return ret;

	ret = g_pScanStage->Stop_Scan();
	if (ret != 0)
	{
		log = _T("Error : Stop scan stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	ret = g_pScanStage->Move_Origin();
	if (ret != 0)
	{
		log = _T("Error : Move origin scan stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	ret = g_pNavigationStage->Move_Loading_Position();
	if (ret != 0)
	{
		log = _T("Error : Move loading position navigation stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	WaitSec(1);

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		log = _T("Error : Not loading position!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SYSTEM_STAGE_NOT_LOADING_POS;
	}

	ret = g_pIO->Is_Mask_Check_OnChuck();
	if (ret != MASK_NONE)
	{
		log = _T("Error : Exist mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_CHUCK;
	}

	if (g_pEfem->Is_POD_OnLPM() != TRUE)
	{
		log = _T("Error : No pod on LPM!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_POD_ON_LPM;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPENED)
	{
		if (!m_bAtmState)
		{
			if (g_pVP->m_pVaccumThread != NULL)
				g_pVP->VacThreadStop();

			ResetEvent(m_hLLC_Vent_Finish);
			ret = g_pVP->LLC_Venting_Start();		//여기서 Vent Thread를 가동시켜두고, MTS를 동시에 움직이는게 좋을듯...by smchoi
		}
		else
		{
			if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
			{
				if (g_pIO->Open_TRGateValve() != OPERATION_COMPLETED)
				{
					log = _T("Error : Open TR gate valve!");
					g_pLog->Display(0, log);
					g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
					return	SEQ_OPEN_TR_GATE_VALVE_ERROR;
				}
			}

			if (g_pIO->Open_LLCGateValve() != OPERATION_COMPLETED)
			{
				log = _T("Error : Open LLC gate valve!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	SEQ_OPEN_LLC_GATE_VALVE_ERROR;
			}
		}
	}

	log = _T("Function : Loading_PreWork End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Loading_PODtoATR()
{
	int ret = 0; CString log;

	//1. POD Open
	//2. POD에 Mask 있음 확인
	//3. MTS Robot에 Mask 없음 확인
	//4. POD에서 LLC 앞으로 마스크 이동 후 이상없는지 확인

	log = _T("Function : Loading_PODtoATR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	ret = g_pEfem->Load_POD();
	if (ret != 0)
	{
		log = _T("Error : Load pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	if (g_pEfem->Is_MASK_InPOD() != TRUE)
	{
		log = _T("Error : No mask in pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_IN_POD;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}
	
	if (g_pEfem->Is_POD_Open() != TRUE)
	{
		log = _T("Error : Pod door not opened!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_POD_NOT_OPENED;
	}

	ret = g_pEfem->Transfer_Mask(MTS_POD, MTS_ROBOT_HAND);
	if (ret != 0)
	{
		log = _T("Error : Transfer to mts robot from pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	if (g_pEfem->Is_MASK_InPOD() != FALSE)
	{
		log = _T("Error : Exist mask in pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_IN_POD;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	log = _T("Function : Loading_PODtoATR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Loading_ATRtoFLIPPER()
{
	int ret = 0; CString log;

	log = _T("Function : Loading_ATRtoFLIPPER Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InFlipper() != FALSE)
	{
		log = _T("Error : Exist mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_FLIPPER;
	}

	if (g_pEfem->Is_Flipper_Working() == TRUE)
	{
		log = _T("Error : Flipper is working!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_MTS_FLIPPER_WORKING;
	}

	ret = g_pEfem->Transfer_Mask(MTS_ROBOT_HAND, MTS_FLIPPER);
	if (ret != 0)
	{
		log = _T("Error : Transfer to flipper from mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InFlipper() != TRUE)
	{
		log = _T("Error : No mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_FLIPPER;
	}

	log = _T("Function : Loading_ATRtoFLIPPER End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Loading_MASK_Flip()
{
	int ret = 0; CString log;

	log = _T("Function : Loading_MASK_Flip Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_InFlipper() != TRUE)
	{
		log = _T("Error : No mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_FLIPPER;
	}

	ret = g_pEfem->Flip_Mask();
	if (ret != 0)
	{
		log = _T("Error : Flip mask!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	g_pConfig->m_bMtsFlipDone_Flag = TRUE;
	g_pConfig->SaveMtsRecoveryData();

	if (g_pEfem->Is_MASK_InFlipper() != TRUE)
	{
		log = _T("Error : No mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_FLIPPER;
	}

	log = _T("Function : Loading_MASK_Flip End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Loading_FLIPPERtoATR()
{
	int ret = 0; CString log;

	log = _T("Function : Loading_FLIPPERtoATR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InFlipper() != TRUE)
	{
		log = _T("Error : No mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_FLIPPER;
	}

	if (g_pEfem->Is_Flipper_Working() == TRUE)
	{
		log = _T("Error : Flipper is working!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_MTS_FLIPPER_WORKING;
	}

	ret = g_pEfem->Transfer_Mask(MTS_FLIPPER, MTS_ROBOT_HAND);
	if (ret != 0)
	{
		log = _T("Error : Transfer to mts robot from flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InFlipper() != FALSE)
	{
		log = _T("Error : Exist mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_FLIPPER;
	}

	log = _T("Function : Loading_FLIPPERtoATR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Loading_ATRtoROTATOR()
{
	int ret = 0; CString log;

	log = _T("Function : Loading_ATRtoROTATOR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InRotator() != FALSE)
	{
		log = _T("Error : Exist mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_ROTATOR;
	}

	if (g_pEfem->Is_Rotator_Working() == TRUE)
	{
		log = _T("Error : Rotator is working!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_MTS_ROTATOR_WORKING;
	}

	ret = g_pEfem->Transfer_Mask(MTS_ROBOT_HAND, MTS_ROTATOR);
	if (ret != 0)
	{
		log = _T("Error : Transfer to rotator from mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InRotator() != TRUE)
	{
		log = _T("Error : No mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_ROTATOR;
	}

	log = _T("Function : Loading_ATRtoROTATOR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Loading_MASK_Rotate(int nAngle)
{
	int ret = 0; CString log;

	log = _T("Function : Loading_MASK_Rotate Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
	
	if (g_pEfem->Is_MASK_InRotator() != TRUE)
	{
		log = _T("Error : No mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_ROTATOR;
	}

	ret = g_pEfem->Rotate_Mask(nAngle, TRUE);
	if (ret != 0)
	{
		log = _T("Error : Rotate mask!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	g_pConfig->m_bMtsRotateDone_Flag = TRUE;
	g_pConfig->SaveMtsRecoveryData();

	if (g_pEfem->Is_MASK_InRotator() != TRUE)
	{
		log = _T("Error : No mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_ROTATOR;
	}

	log = _T("Function : Loading_MASK_Rotate End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Loading_ROTATORtoATR()
{
	int ret = 0; CString log;

	log = _T("Function : Loading_ROTATORtoATR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InRotator() != TRUE)
	{
		log = _T("Error : No mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_ROTATOR;
	}

	if (g_pEfem->Is_Rotator_Working() == TRUE)
	{
		log = _T("Error : Rotator is working!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_MTS_ROTATOR_WORKING;
	}

	ret = g_pEfem->Transfer_Mask(MTS_ROTATOR, MTS_ROBOT_HAND);
	if (ret != 0)
	{
		log = _T("Error : Transfer to mts robot from rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InRotator() != FALSE)
	{
		log = _T("Error : Exist mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_ROTATOR;
	}

	log = _T("Function : Loading_ROTATORtoATR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
	
	return ret;
}

int CAutoProcess::Loading_ATRtoLLC()
{
	int ret = 0; CString log;

	//1. MTS Robot에 Mask 있음..
	//2. LLC에 Mask 없음.
	//3. Air Damper On
	//4. LLC Gate Valve Open 확인 후 Close면 기다린다.
	//5. Robot에서 LLC로 마스크 이동 후 이상없는지 확인

	log = _T("Function : Loading_ATRtoLLC Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_NONE)
	{
		log = _T("Error : Exist mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_LLC;
	}

	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if(ret != FALSE)
	{
		log = _T("Error : Tilt mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_TILT_MASK_ON_LLC;
	}

	if (g_pIO->Is_Isolator_Up() != TRUE)
	{
		log = _T("Error : Isolator does not work!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SYSTEM_ISOLATOR_NOT_WORKING;
	}

	ret = g_pEfem->MoveReadyPos_toFrontLLC();
	if(ret != 0)
	{
		log = _T("Error : Move ready position to front LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	ret;	
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPENED)
	{
		if (!m_bAtmState)
		{
			if (WaitForSingleObject(m_hLLC_Vent_Finish, g_pConfig->m_nTimeout_sec_LLKVentEnd * 1000) != WAIT_OBJECT_0)
			{
				log = _T("Error : LLC venting timeout!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return LLC_VENTING_TIMEOUT;
			}
		}
		else
		{
			if (g_pIO->Open_LLCGateValve() != OPERATION_COMPLETED)
			{
				log = _T("Error : Open LLC gate valve!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return SEQ_OPEN_LLC_GATE_VALVE_ERROR;
			}
		}
	}

	//20200204 jkseo, delay 추가
	WaitSec(5);

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPENED)
	{
		log = _T("Error : LLC gate valve not opened!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_LLC_GATE_VALVE_NOT_OPENED;
	}

	ret = g_pEfem->Transfer_Mask(MTS_ROBOT_HAND, LLC);
	if (ret != 0)
	{
		log = _T("Error : Transfer mask to LLC from mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_ON)
	{
		log = _T("Error : No mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_LLC;
	}
		
	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	log = _T("Function : Loading_ATRtoLLC End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return 0;
}

int CAutoProcess::Loading_LLC_Rough()
{
	int ret = 0; CString log;

	//1. MTS Robot 접힘상태 확인
	//2. VMTR 접힘상태 확인
	//3. LLC Pumping 수행 및 Transfer Valve Open

	log = _T("Function : Loading_LLC_Rough Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
	
	if (g_pIO->Is_ATM_Robot_Arm_Retract() == FALSE)
		return	SEQ_ATM_ROBOT_NOT_RETRACTED;

	if (g_pIO->Is_VAC_Robot_Arm_Retract() == FALSE)
		return	SEQ_VAC_ROBOT_NOT_RETRACTED;

	if (!m_bAtmState)
	{
		if (g_pVP->m_pVaccumThread != NULL)
			g_pVP->VacThreadStop();

		ResetEvent(m_hLLC_Pump_Finish);
		ret = g_pVP->LLC_Pumping_Start();	//LLC Pumping and TR Gate Valve Open
		if (ret != 0)
		{
			SetEvent(m_hLLC_Pump_Finish);
			log = _T("Error : LLC pumping!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_FAIL_LLC_PUMPING_PROCESS;
		}
	}
	else
	{
		if (g_pIO->Close_LLCGateValve() != OPERATION_COMPLETED)
		{
			log = _T("Error : Close LLC gate valve!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CLOSE_LLC_GATE_VALVE_ERROR;
		}
	}

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		if (!m_bAtmState)
		{
			if (WaitForSingleObject(m_hLLC_Pump_Finish, g_pConfig->m_nTimeout_sec_LLKRoughEnd * 1000) != WAIT_OBJECT_0)
			{
				log = _T("Error : LLC pumping timeout!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return LLC_PUMPING_TIMEOUT;
			}
		}
		else
		{
			if (g_pIO->Open_TRGateValve() != OPERATION_COMPLETED)
			{
				log = _T("Error : Open TR gate valve!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	SEQ_OPEN_TR_GATE_VALVE_ERROR;
			}
		}
	}
	
	log = _T("Function : Loading_LLC_Rough End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Loading_LLCtoVMTR()
{
	int ret = 0; CString log;

	//1. LLC에 Mask 있음 확인
	//2. VMTR에 Mask 없음 확인
	//3. Transfer Valve Open 확인
	//4. LLC에서 VMTR로 마스크 이동 후 이상없는지 확인

	log = _T("Function : Loading_LLCtoVMTR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_ON)
	{
		log = _T("Error : No mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_LLC;
	}

	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if(ret != FALSE)
	{
		log = _T("Error : Tilt mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_TILT_MASK_ON_LLC;
	}

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != FALSE)
		{
			log = _T("Error : Exist mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_EXIST_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		log = _T("Error : TR gate not opened!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_TR_GATE_VALVE_NOT_OPENED;
	}

	ret = g_pVacuumRobot->Transfer_Mask(LLC, VACUUM_ROBOT_HAND);
	if (ret != 0)
	{
		log.Format(_T("Warning : Transfer mask to vac robot from LLC! (%d)"), ret);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		ret = g_pVacuumRobot->Transfer_Mask(LLC, VACUUM_ROBOT_HAND);
		if (ret != 0)
		{
			log.Format(_T("Warning : Transfer mask to vac robot from LLC! (%d)"), ret);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

			ret = g_pVacuumRobot->Transfer_Mask(LLC, VACUUM_ROBOT_HAND);
			if (ret != 0)
			{
				log.Format(_T("Error : Transfer mask to vac robot from LLC! (%d)"), ret);
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	ret;
			}
		}
	}

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_NONE)
	{
		log = _T("Error : Exist mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_LLC;
	}

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != TRUE)
		{
			log = _T("Error : No mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_NO_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}
	else
		ret = 0;

	log = _T("Function : Loading_LLCtoVMTR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Loading_VMTRtoCHUCK()
{
	int ret = 0; CString log;

	//1. VMTR에 Mask 있음..
	//2. Stage Loading Position
	//3. Stage에 Mask 없음...
	//4. VMTR에서 CHUCK으로 마스크 이동 후 이상없는지 확인

	log = _T("Function : Loading_VMTRtoCHUCK Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != TRUE)
		{
			log = _T("Error : No mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_NO_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		log = _T("Error : Not loading position!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SYSTEM_STAGE_NOT_LOADING_POS;
	}

	ret = g_pIO->Is_Mask_Check_OnChuck();
	if (ret != MASK_NONE)
	{
		log = _T("Error : Exist mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_CHUCK;
	}

	ret = g_pIO->Is_MC_Mask_Slant_Check();
	if(ret != FALSE)
	{
		log = _T("Error : Tilt mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_TILT_MASK_ON_CHUCK;
	}

	ret = g_pVacuumRobot->MovePlaceReadyPos(CHUCK);
	if (ret != 0)
	{
		log.Format(_T("Warning : Move place ready position! (%d)"), ret);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		ret = g_pVacuumRobot->MovePlaceReadyPos(CHUCK);
		if (ret != 0)
		{
			log.Format(_T("Warning : Move place ready position! (%d)"), ret);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

			ret = g_pVacuumRobot->MovePlaceReadyPos(CHUCK);
			if (ret != 0)
			{
				log.Format(_T("Error : Move place ready position! (%d)"), ret);
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	ret;
			}
		}
	}

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != TRUE)
		{
			log = _T("Error : No mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_NO_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}

	Sleep(1000);
	ret = g_pVacuumRobot->Transfer_Mask(VACUUM_ROBOT_HAND, CHUCK);
	if (ret != 0)
	{
		log.Format(_T("Warning : Transfer mask to chuck from vac robot! (%d)"), ret);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		ret = g_pVacuumRobot->Transfer_Mask(VACUUM_ROBOT_HAND, CHUCK);
		if (ret != 0)
		{
			log.Format(_T("Warning : Transfer mask to chuck from vac robot! (%d)"), ret);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

			ret = g_pVacuumRobot->Transfer_Mask(VACUUM_ROBOT_HAND, CHUCK);
			if (ret != 0)
			{
				log.Format(_T("Error : Transfer mask to chuck from vac robot! (%d)"), ret);
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return ret;
			}
		}
	}

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != FALSE)
		{
			log = _T("Error : Exist mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_EXIST_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}

	ret = g_pIO->Is_Mask_Check_OnChuck();
	if (ret != MASK_ON)
	{
		log = _T("Error : No mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_CHUCK;
	}

	if (g_pIO->Is_VAC_Robot_Arm_Retract() != TRUE)
		return	SEQ_VAC_ROBOT_NOT_RETRACTED;

	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		ret = g_pIO->Close_TRGateValve();
		if (ret != OPERATION_COMPLETED)
		{
			log = _T("Error : Close transfer gate valve!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return ret;
		}
	}

	//20201110 jkseo, 로딩완료 후 DVR #1 OFF 요청으로 추가
	g_pIO->Set_DVR_Lamp(1, false);

	log = _T("Function : Loading_VMTRtoCHUCK End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return 0;
}

int CAutoProcess::Loading_Complete()
{
	CString log;
	log = _T("Function : Loading_Complete!");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	// Align Data 초기화
	if (g_pConfig != NULL)
	{

		g_pConfig->m_dOMAlignPointLB_X_mm = 0.0;
		g_pConfig->m_dOMAlignPointLB_Y_mm = 0.0;
		g_pConfig->m_dOMAlignPointLT_X_mm = 0.0;
		g_pConfig->m_dOMAlignPointLT_Y_mm = 0.0;
		g_pConfig->m_dOMAlignPointRT_X_mm = 0.0;
		g_pConfig->m_dOMAlignPointRT_Y_mm = 0.0;
		g_pConfig->m_dEUVAlignPointLB_X_mm = 0.0;
		g_pConfig->m_dEUVAlignPointLB_Y_mm = 0.0;
		g_pConfig->m_dEUVAlignPointLT_X_mm = 0.0;
		g_pConfig->m_dEUVAlignPointLT_Y_mm = 0.0;
		g_pConfig->m_dEUVAlignPointRT_X_mm = 0.0;
		g_pConfig->m_dEUVAlignPointRT_Y_mm = 0.0;
		g_pConfig->m_bOMAlignCompleteFlag = FALSE;
		g_pConfig->m_bEUVAlignCompleteFlag = FALSE;
		//g_pConfig->m_nEUVAlignFOV = 10000;	//nm
		//g_pConfig->m_nEUVAlignGrid = 40;
		g_pConfig->SaveRecoveryData();
		g_pMaskMap->m_dOMAlignPointLB_posx_mm = g_pConfig->m_dOMAlignPointLB_X_mm;
		g_pMaskMap->m_dOMAlignPointLB_posy_mm = g_pConfig->m_dOMAlignPointLB_Y_mm;
		g_pMaskMap->m_dOMAlignPointLT_posx_mm = g_pConfig->m_dOMAlignPointLT_X_mm;
		g_pMaskMap->m_dOMAlignPointLT_posy_mm = g_pConfig->m_dOMAlignPointLT_Y_mm;
		g_pMaskMap->m_dOMAlignPointRT_posx_mm = g_pConfig->m_dOMAlignPointRT_X_mm;
		g_pMaskMap->m_dOMAlignPointRT_posy_mm = g_pConfig->m_dOMAlignPointRT_Y_mm;
		g_pMaskMap->m_dEUVAlignPointLB_posx_mm = g_pConfig->m_dEUVAlignPointLB_X_mm;
		g_pMaskMap->m_dEUVAlignPointLB_posy_mm = g_pConfig->m_dEUVAlignPointLB_Y_mm;
		g_pMaskMap->m_dEUVAlignPointLT_posx_mm = g_pConfig->m_dEUVAlignPointLT_X_mm;
		g_pMaskMap->m_dEUVAlignPointLT_posy_mm = g_pConfig->m_dEUVAlignPointLT_Y_mm;
		g_pMaskMap->m_dEUVAlignPointRT_posx_mm = g_pConfig->m_dEUVAlignPointRT_X_mm;
		g_pMaskMap->m_dEUVAlignPointRT_posy_mm = g_pConfig->m_dEUVAlignPointRT_Y_mm;
		g_pMaskMap->m_bOMAlignComplete = g_pConfig->m_bOMAlignCompleteFlag;
		g_pMaskMap->m_bEUVAlignComplete = g_pConfig->m_bEUVAlignCompleteFlag;
	}

	TempFunction();

	//2. 로딩 시간 기록
	//3. Z Interlock 높이 설정

	return 0;
}
int CAutoProcess::TempFunction()
{
	return 0;
}
//////////////////////////////////////////////////////////////////////// Mask Loading Operation End //////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////// Mask Unloading Operation /// //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
int CAutoProcess::Unloading_PreWork()
{
	int ret = 0; CString log;

	//1. ADAM 통신 중단
	//2. EUV Off
	//3. Fine Stage 좌표 Origin Point로(1,1,0,0,0) 이동.
	//4. Course Stage Load Position으로 이동 후 Mask 있음 확인
	//5. POD가 있고, OPEN상태인지 확인
	//6. OM Lamp OFF
	//7. DVR Camera On
	//8. TR Gate Valve가 닫혀있으면 Open

	log = _T("Function : Unloading_PreWork Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	ret = g_pAP->Is_HWModule_OK();
	if (ret != 0)
		return ret;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pIO == NULL || g_pEfem == NULL || g_pVP == NULL || g_pAdam == NULL)
		return	-999;	//추후 Error Code 정의

	//ret = g_pAdam->Stop();
	if (ret != 0)
		return ret;

	//ret = g_pEUVSource->EUV_Off();
	if (ret != 0)
		return ret;

	ret = g_pScanStage->Stop_Scan();
	if (ret != 0)
	{
		log = _T("Error : Stop scan stage!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	ret = g_pScanStage->Move_Origin();
	if (ret != 0)
	{
		log = _T("Error : Move origin!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	ret;
	}

	ret = g_pNavigationStage->Move_Loading_Position();
	if (ret != 0)
	{
		log = _T("Error : Move loading position!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	WaitSec(1);

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		log = _T("Error : Not loading position!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SYSTEM_STAGE_NOT_LOADING_POS;
	}

	ret = g_pIO->Is_Mask_Check_OnChuck();
	if (ret != MASK_ON)
	{
		log = _T("Error : No mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_CHUCK;
	}

	if (g_pEfem->Is_POD_OnLPM() != TRUE)
	{
		log = _T("Error : No pod on LPM!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_POD_ON_LPM;
	}

	if (g_pEfem->Is_POD_Open() != TRUE)
	{
		log = _T("Error : Pod door not opened!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_POD_NOT_OPENED;
	}

	if (g_pEfem->Is_MASK_InPOD() != FALSE)
	{
		log = _T("Error : Exist mask in pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_IN_POD;
	}

	//if (g_pLamp->Light_Off() == FALSE)
	//	return	-999;	//추후 Error Code 정의

	//if (g_pIO->DVR_Off() == FALSE)
	//	return	-999;	//추후 Error Code 정의

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		if (!m_bAtmState)
		{
			if (g_pVP->m_pVaccumThread != NULL)
				g_pVP->VacThreadStop();
			
			ResetEvent(m_hLLC_Pump_Finish);
			ret = g_pVP->LLC_Pumping_Start();	//LLC Pumping and TR Gate Valve Open
			if (ret != 0)
			{
				SetEvent(m_hLLC_Pump_Finish);
				log = _T("Error : LLC pumping!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	SEQ_FAIL_LLC_PUMPING_PROCESS;
			}
		}
		else
		{
			if (g_pIO->Open_TRGateValve() != OPERATION_COMPLETED)
			{
				log = _T("Error : Open TR gate valve!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	SEQ_OPEN_TR_GATE_VALVE_ERROR;
			}
		}
	}

	log = _T("Function : Unloading_PreWork End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return 0;
}

int CAutoProcess::Unloading_CHUCKtoVMTR()
{
	int ret = 0; CString log;

	//1. Stage가 Load Position에 있음 확인
	//2. Stage에 Mask 있음 확인
	//3. VMTR에 마스크 없음 확인
	//4. CHUCK에서 VMTR로 마스크 이동 후 이상없는지 확인

	log = _T("Function : Unloading_CHUCKtoVMTR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != FALSE)
		{
			log = _T("Error : Exsit mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_EXIST_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		log = _T("Error : Not loading position!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SYSTEM_STAGE_NOT_LOADING_POS;
	}

	ret = g_pIO->Is_Mask_Check_OnChuck();
	if (ret != MASK_ON)
	{
		log = _T("Error : No mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_CHUCK;
	}

	ret = g_pIO->Is_MC_Mask_Slant_Check();
	if(ret != FALSE)
	{
		log = _T("Error : Tilt mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_TILT_MASK_ON_CHUCK;
	}

	ret = g_pVacuumRobot->Transfer_Mask(CHUCK, VACUUM_ROBOT_HAND);
	if (ret != 0)
	{
		log.Format(_T("Warning : Transfer mask to vac robot from chuck! (%d)"), ret);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		ret = g_pVacuumRobot->Transfer_Mask(CHUCK, VACUUM_ROBOT_HAND);
		if (ret != 0)
		{
			log.Format(_T("Warning : Transfer mask to vac robot from chuck! (%d)"), ret);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

			ret = g_pVacuumRobot->Transfer_Mask(CHUCK, VACUUM_ROBOT_HAND);
			if (ret != 0)
			{
				log.Format(_T("Error : Transfer mask to vac robot from chuck! (%d)"), ret);
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return ret;
			}
		}
	}
		
	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != TRUE)
		{
			log = _T("Error : No mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_NO_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}

	ret = g_pIO->Is_Mask_Check_OnChuck();
	if (ret != MASK_NONE)
	{
		log = _T("Error : Exist mask on chuck!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_CHUCK;
	}

	log = _T("Function : Unloading_CHUCKtoVMTR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return 0;
}

int CAutoProcess::Unloading_VMTRtoLLC()
{
	int ret = 0; CString log;

	//1. LLC에 Mask 없음
	//2. VMTR에 Mask 있음
	//3. Transfer Valve Open
	//4. VMTR에서 LLC로 마스크 이동 후 이상없는지 확인

	log = _T("Function : Unloading_VMTRtoLLC Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_NONE)
	{
		log = _T("Error : Exist mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_LLC;
	}

	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if(ret != FALSE)
	{
		log = _T("Error : Tilt mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_TILT_MASK_ON_LLC;
	}			

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != TRUE)
		{
			log = _T("Error : No mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_NO_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}
	
	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		if (!m_bAtmState)
		{
			if (WaitForSingleObject(m_hLLC_Pump_Finish, g_pConfig->m_nTimeout_sec_LLKRoughEnd * 1000) != WAIT_OBJECT_0)
			{
				log = _T("Error : LLC pumping timeout!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return LLC_PUMPING_TIMEOUT;
			}

			if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
			{
				log = _T("Error : TR gate valve not opened!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return SEQ_TR_GATE_VALVE_NOT_OPENED;
			}
		}
		else
		{
			if (g_pIO->Open_TRGateValve() != OPERATION_COMPLETED)
			{
				log = _T("Error : Open TR gate valve!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	SEQ_OPEN_TR_GATE_VALVE_ERROR;
			}
		}
	}

	ret = g_pVacuumRobot->MovePlaceReadyPos(LLC);
	if (ret != 0)
	{
		log.Format(_T("Warning : Move place ready position! (%d)"), ret);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		ret = g_pVacuumRobot->MovePlaceReadyPos(LLC);
		if (ret != 0)
		{
			log.Format(_T("Warning : Move place ready position! (%d)"), ret);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

			ret = g_pVacuumRobot->MovePlaceReadyPos(LLC);
			if (ret != 0)
			{
				log.Format(_T("Error : Move place ready position! (%d)"), ret);
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return ret;
			}
		}
	}
	
	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != TRUE)
		{
			log = _T("Error : No mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_NO_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		log = _T("Error : TR gate valve not opened!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_TR_GATE_VALVE_NOT_OPENED;
	}

	Sleep(1000);
	ret = g_pVacuumRobot->Transfer_Mask(VACUUM_ROBOT_HAND, LLC);
	if (ret != 0)
	{
		log.Format(_T("Warning : Transfer mask to LLC from vac robot! (%d)"), ret);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

		ret = g_pVacuumRobot->Transfer_Mask(VACUUM_ROBOT_HAND, LLC);
		if (ret != 0)
		{
			log.Format(_T("Warning : Transfer mask to LLC from vac robot! (%d)"), ret);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

			ret = g_pVacuumRobot->Transfer_Mask(VACUUM_ROBOT_HAND, LLC);
			if (ret != 0)
			{
				log.Format(_T("Error : Transfer mask to LLC from vac robot! (%d)"), ret);
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return ret;
			}
		}
	}

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_ON)
	{
		log = _T("Error : No mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_LLC;
	}

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != FALSE)
	{
		ret = g_pIO->SetTriggerCamSensor(TRUE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger on!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_ON_ERROR;
		}

		Sleep(500);

		if (g_pIO->Is_Mask_OnVMTR() != FALSE)
		{
			log = _T("Error : Exist mask on vac robot!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_EXIST_MASK_ON_VAC_ROBOT;
		}

		ret = g_pIO->SetTriggerCamSensor(FALSE);
		if (ret != 0)
		{
			log = _T("Error : Cam Sensor trigger off!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR;
		}
	}
	else
		ret = 0;
	

	log = _T("Function : Unloading_VMTRtoLLC End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Unloading_LLC_Vent()
{
	int ret = 0; CString log;

	//1. VMTR 접힘상태 확인
	//2. LLC Venting 수행 및 LLC Gate Valve Open

	log = _T("Function : Unloading_LLC_Vent Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pIO->Is_VAC_Robot_Arm_Retract() != TRUE)
		return	SEQ_VAC_ROBOT_NOT_RETRACTED;

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPENED)
	{
		if (!m_bAtmState)
		{
			if (g_pVP->m_pVaccumThread != NULL)
				g_pVP->VacThreadStop();

			ResetEvent(m_hLLC_Vent_Finish);
			ret = g_pVP->LLC_Venting_Start();	//LLC Venting and LLC Gate Valve Open
			if (ret != 0)
			{
				SetEvent(m_hLLC_Pump_Finish);
				log = _T("Error : LLC venting!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	SEQ_FAIL_LLC_VENTING_PROCESS;
			}
		}
		else
		{
			if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED)
			{
				if (g_pIO->Close_TRGateValve() != OPERATION_COMPLETED)
				{
					log = _T("Error : Close TR gate valve!");
					g_pLog->Display(0, log);
					g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
					return	SEQ_CLOSE_TR_GATE_VALVE_ERROR;
				}
			}

			if (g_pIO->Open_LLCGateValve() != OPERATION_COMPLETED)
			{
				log = _T("Error : Open LLC gate valve!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	SEQ_OPEN_LLC_GATE_VALVE_ERROR;
			}
		}
	}

	log = _T("Function : Unloading_LLC_Vent End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int	CAutoProcess::Unloading_LLCtoATR()
{
	int ret = 0; CString log;

	log = _T("Function : Unloading_LLCtoATR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_POD_OnLPM() != TRUE)
	{
		log = _T("Error : No pod on LPM!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_POD_ON_LPM;
	}

	if (g_pEfem->Is_POD_Open() != TRUE)
	{
		log = _T("Error : Pod not load!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_POD_NOT_OPENED;
	}

	if (g_pEfem->Is_MASK_InPOD() != FALSE)
	{
		log = _T("Error : Exist mask in pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_IN_POD;
	}

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_ON)
	{
		log = _T("Error : No mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_LLC;
	}

	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if(ret != FALSE)
	{
		log = _T("Error : Tilt mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_TILT_MASK_ON_LLC;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pIO->Is_Isolator_Up() != TRUE)
	{
		log = _T("Error : Isolator not working!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SYSTEM_ISOLATOR_NOT_WORKING;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPENED)
	{
		if (!m_bAtmState)
		{
			if (WaitForSingleObject(m_hLLC_Vent_Finish, g_pConfig->m_nTimeout_sec_LLKVentEnd * 1000) != WAIT_OBJECT_0)
			{
				log = _T("Error : LLC venting timeout!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return LLC_VENTING_TIMEOUT;
			}
		}
		else
		{
			if (g_pIO->Open_LLCGateValve() != OPERATION_COMPLETED)
			{
				log = _T("Error : Open LLC gate valve!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	SEQ_OPEN_LLC_GATE_VALVE_ERROR;
			}
		}
	}

	//20200204 jkseo, delay 추가
	WaitSec(5);

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPENED)
	{
		log = _T("Error : LLC gate valve not opened!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_LLC_GATE_VALVE_NOT_OPENED;
	}

	ret = g_pEfem->Transfer_Mask(LLC, MTS_ROBOT_HAND);
	if (ret != 0)
	{
		log = _T("Error : Transfer mask to mts robot from LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	ret = g_pIO->Is_Mask_Check_OnLLC();
	if (ret != MASK_NONE)
	{
		log = _T("Error : Exist mask on LLC!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_LLC;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	log = _T("Function : Unloading_LLCtoATR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
	
	return 0;
}

int CAutoProcess::Unloading_LLC_Rough()
{
	int ret = 0; CString log;

	log = _T("Function : Unloading_LLC_Rough Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pIO->Is_ATM_Robot_Arm_Retract() == FALSE)
		return	SEQ_ATM_ROBOT_NOT_RETRACTED;
		
	if (!m_bAtmState)
	{
		if (g_pVP->m_pVaccumThread != NULL)
			g_pVP->VacThreadStop();

		ResetEvent(m_hLLC_Pump_Finish);
		ret = g_pVP->LLC_Pumping_Start();	//LLC Pumping and TR Gate Valve Open
		if (ret != 0)
		{
			log = _T("Error : LLC pumping sequence!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return SEQ_FAIL_LLC_PUMPING_PROCESS;
		}
	}
	else
	{
		if (g_pIO->Close_LLCGateValve() != OPERATION_COMPLETED)
		{
			log = _T("Error : Close LLC gate valve!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	SEQ_CLOSE_LLC_GATE_VALVE_ERROR;
		}

		if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
		{
			if (g_pIO->Open_TRGateValve() != OPERATION_COMPLETED)
			{
				log = _T("Error : Open TR gate valve!");
				g_pLog->Display(0, log);
				g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
				return	SEQ_OPEN_TR_GATE_VALVE_ERROR;
			}
		}
	}

	log = _T("Function : Unloading_LLC_Rough End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int	CAutoProcess::Unloading_ATRtoROTATOR()
{
	int ret = 0; CString log;

	log = _T("Function : Unloading_ATRtoROTATOR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InRotator() != FALSE)
	{
		log = _T("Error : Exist mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_ROTATOR;
	}

	if (g_pEfem->Is_Rotator_Working() == TRUE)
	{
		log = _T("Error : Rotator is working!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_MTS_ROTATOR_WORKING;
	}

	ret = g_pEfem->Transfer_Mask(MTS_ROBOT_HAND, MTS_ROTATOR);
	if (ret != 0)
	{
		log = _T("Error : Tramsfer mask to rotator from mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	ret;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InRotator() != TRUE)
	{
		log = _T("Error : No mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_ROTATOR;
	}

	log = _T("Function : Unloading_ATRtoROTATOR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}
int	CAutoProcess::Unloading_MASK_Rotate(int nAngle)
{
	int ret = 0; CString log;

	log = _T("Function : Unloading_MASK_Rotate Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
	
	if (g_pEfem->Is_MASK_InRotator() != TRUE)
	{
		log = _T("Error : No mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_ROTATOR;
	}

	ret = g_pEfem->Rotate_Mask(nAngle, FALSE);
	if (ret != 0)
	{
		log = _T("Error : Rotate mask!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	g_pConfig->m_bMtsRotateDone_Flag = FALSE;
	g_pConfig->SaveMtsRecoveryData();
	
	if (g_pEfem->Is_MASK_InRotator() != TRUE)
	{
		log = _T("Error : No mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_ROTATOR;
	}

	log = _T("Function : Unloading_MASK_Rotate End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}
int	CAutoProcess::Unloading_ROTATORtoATR()
{
	int ret = 0; CString log;

	log = _T("Function : Unloading_ROTATORtoATR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InRotator() != TRUE)
	{
		log = _T("Error : No mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_ROTATOR;
	}

	if (g_pEfem->Is_Rotator_Working() == TRUE)
	{
		log = _T("Error : Rotator is working!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_MTS_ROTATOR_WORKING;
	}

	ret = g_pEfem->Transfer_Mask(MTS_ROTATOR, MTS_ROBOT_HAND);
	if (ret != 0)
	{
		log = _T("Error : Transfer mask to mts robot from rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	ret;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InRotator() != FALSE)
	{
		log = _T("Error : Exist mask on rotator!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_ROTATOR;
	}

	log = _T("Function : Unloading_ROTATORtoATR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}
int CAutoProcess::Unloading_ATRtoFLIPPER()
{
	int ret = 0; CString log;

	log = _T("Function : Unloading_ATRtoFLIPPER Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InFlipper() != FALSE)
	{
		log = _T("Error : Exist mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_FLIPPER;
	}

	if (g_pEfem->Is_Flipper_Working() == TRUE)
	{
		log = _T("Error : Flipper is working!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_MTS_FLIPPER_WORKING;
	}

	ret = g_pEfem->Transfer_Mask(MTS_ROBOT_HAND, MTS_FLIPPER);
	if (ret != 0)
	{
		log = _T("Error : Tramsfer mask to flipper from mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	ret;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InFlipper() != TRUE)
	{
		log = _T("Error : No mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_FLIPPER;
	}

	log = _T("Function : Unloading_ATRtoFLIPPER End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}
int CAutoProcess::Unloading_MASK_Flip()
{
	int ret = 0; CString log;

	log = _T("Function : Unloading_MASK_Flip Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_InFlipper() != TRUE)
	{
		log = _T("Error : No mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_FLIPPER;
	}

	ret = g_pEfem->Flip_Mask();
	if (ret != 0)
	{
		log = _T("Error : Flip mask!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	g_pConfig->m_bMtsFlipDone_Flag = FALSE;
	g_pConfig->SaveMtsRecoveryData();
	
	if (g_pEfem->Is_MASK_InFlipper() != TRUE)
	{
		log = _T("Error : No mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_FLIPPER;
	}

	log = _T("Function : Unloading_MASK_Flip End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}
int CAutoProcess::Unloading_FLIPPERtoATR()
{
	int ret = 0; CString log;

	log = _T("Function : Unloading_FLIPPERtoATR Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InFlipper() != TRUE)
	{
		log = _T("Error : No mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_FLIPPER;
	}

	if (g_pEfem->Is_Flipper_Working() == TRUE)
	{
		log = _T("Error : Flipper is working!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_MTS_FLIPPER_WORKING;
	}

	ret = g_pEfem->Transfer_Mask(MTS_FLIPPER, MTS_ROBOT_HAND);
	if (ret != 0)
	{
		log = _T("Error : Transfer mask to mts robot from flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	ret;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InFlipper() != FALSE)
	{
		log = _T("Error : Exist mask on flipper!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_FLIPPER;
	}

	log = _T("Function : Unloading_FLIPPERtoATR End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}
int	CAutoProcess::Unloading_ATRtoPOD()
{
	int ret = 0; CString log;

	log = _T("Function : Unloading_ATRtoPOD Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		log = _T("Error : No mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_POD_OnLPM() != TRUE)
	{
		log = _T("Error : No pod on LPM!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_POD_ON_LPM;
	}

	if (g_pEfem->Is_POD_Open() != TRUE)
	{
		ret = g_pEfem->Load_POD();
		if (ret != 0)
		{
			log = _T("Error : Load Pod!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			return	ret;
		}
	}

	if (g_pEfem->Is_MASK_InPOD() != FALSE)
	{
		log = _T("Error : Exist mask in pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_IN_POD;
	}

	if (g_pEfem->Is_POD_Open() == FALSE)
	{
		log = _T("Error : Pod door not opened!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_POD_NOT_OPENED;
	}

	ret = g_pEfem->Transfer_Mask(MTS_ROBOT_HAND, MTS_POD);
	if (ret != 0)
	{
		log = _T("Error : Transfer mask to pod from mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	ret;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		log = _T("Error : Exist mask on mts robot!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_EXIST_MASK_ON_MTS_ROBOT;
	}

	if (g_pEfem->Is_MASK_InPOD() != TRUE)
	{
		log = _T("Error : No mask in pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return	SEQ_NO_MASK_IN_POD;
	}

	ret = g_pEfem->Unload_POD();
	if (ret != 0)
	{
		log = _T("Error : Close pod!");
		g_pLog->Display(0, log);
		g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
		return ret;
	}

	log = _T("Function : Unloading_ATRtoPOD End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Unloading_Complete()
{
	int ret = 0;

	CString log;
	//1.Pumping 완료 확인
	//2.History기록(본 Mask 가동시간, 일일 가동시간, 일일 Mask 매수)
	//3.Mask Load 버튼 활성화

	log = _T("Function : Unloading_Complete Start");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	if (!m_bAtmState)
	{
		if (WaitForSingleObject(m_hLLC_Pump_Finish, g_pConfig->m_nTimeout_sec_LLKRoughEnd * 1000) != WAIT_OBJECT_0)
		{
			log = _T("Error : LLC pumping timeout!");
			g_pLog->Display(0, log);
			g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);
			ret = LLC_PUMPING_TIMEOUT;
			return ret;
		}
	}
	
	log = _T("Function : Unloading_Complete End");
	g_pLog->Display(0, log);
	g_pAP->SaveLogFile("LoadingSequence", (LPSTR)(LPCTSTR)log);

	return ret;
}

int CAutoProcess::Check_STAGE_MovePossible()
{
	if (g_pVacuumRobot->Is_VMTR_Working())
		return -1;
	else if (!g_pIO->Is_VAC_Robot_Arm_Retract()) 
		return -2;
	//else if (g_pIO->Is_MC_Mask_Slant1_Check()) 
	//	return -3;
	else if (g_pIO->Is_MC_Mask_Slant2_Check()) 
		return -4;
	else 
		return XY_NAVISTAGE_OK;
}

/////////////////// Mask Unloading Operation End /////////////////////////////////
