﻿// CMaindialog.cpp: 구현 파일
//


#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <iomanip>

IMPLEMENT_DYNAMIC(CMaindialog, CDialogEx)

CMaindialog::CMaindialog(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SREM_DIALOG, pParent)
	, m_dMcStandbyVentValue(0)
	, m_dMcSlowVentValue(0)
	, m_dMcFastVentValue(0)
	, m_dMcSlowRoughValue(0)
	, m_dMcFastRoughValue(0)
	, m_dMcTmpRoughValue(0)
	, m_nMcTmpRoughTime(0)
	, m_nMcSlowVentTime(0)
	, m_nMcFastRoughTime(0)
	, m_nMcFastVentTime(0)
	, m_nMcSlowRoughTime(0)
	, m_nMcStandbyVentTime(0)
	, m_dLlcSlowRoughValue(0)
	, m_dLlcFastRoughValue(0)
	, m_dLlcTmpRoughValue(0)
	, m_dLlcStandbyVentValue(0)
	, m_dLlcSlowVentValue(0)
	, m_dLlcFastVentValue(0)
	, m_nLlcSlowRoughTime(0)
	, m_nLlcFastRoughTime(0)
	, m_nLlcTmpRoughTime(0)
	, m_nLlcStandbyVentTime(0)
	, m_nLlcSlowVentTime(0)
	, m_nLlcFastVentTime(0)
{

	m_maindialog_pThread = NULL;
	m_maindialog_bThreadExitFlag = FALSE;

	m_pVacuumWriteThread = NULL;
	m_pVacuumWriteStop = FALSE;
}

CMaindialog::~CMaindialog()
{
	m_brush.DeleteObject();
	m_font.DeleteObject();

	m_main_dialog_brush.DeleteObject();

	m_pVacuumWriteStop = TRUE;
	if (m_pVacuumWriteThread != NULL)
	{
		HANDLE threadHandle = m_pVacuumWriteThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pVacuumWriteThread = NULL;
	}

}

void CMaindialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_ICON_STAGE_LOG_ICON, m_stage_log_icon);
}

BEGIN_MESSAGE_MAP(CMaindialog, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_MC_PUMP, &CMaindialog::OnBnClickedMcPump)
	ON_BN_CLICKED(IDC_MC_VENT, &CMaindialog::OnBnClickedMcVent)
	ON_BN_CLICKED(IDC_MC_STOP, &CMaindialog::OnBnClickedMcStop)
	ON_BN_CLICKED(IDC_LLC_PUMP, &CMaindialog::OnBnClickedLlcPump)
	ON_BN_CLICKED(IDC_LLC_VENT, &CMaindialog::OnBnClickedLlcVent)
	ON_BN_CLICKED(IDC_LLC_STOP, &CMaindialog::OnBnClickedLlcStop)
	ON_BN_CLICKED(IDC_MC_ON, &CMaindialog::OnBnClickedMcOn)
	ON_BN_CLICKED(IDC_MC_DEFAULT, &CMaindialog::OnBnClickedMcDefault)
	ON_BN_CLICKED(IDC_LLC_ON, &CMaindialog::OnBnClickedLlcOn)
	ON_BN_CLICKED(IDC_LLC_DEFAULT, &CMaindialog::OnBnClickedLlcDefault)
	ON_BN_CLICKED(IDC_MC_AUTO_SEQ_STATE_RESET, &CMaindialog::OnBnClickedMcAutoSeqStateReset)
	ON_BN_CLICKED(IDC_LLC_AUTO_SEQ_STATE_RESET, &CMaindialog::OnBnClickedLlcAutoSeqStateReset)
	ON_BN_CLICKED(IDC_LLC_TMP_ON, &CMaindialog::OnBnClickedLlcTmpOn)
	ON_BN_CLICKED(IDC_LLC_TMP_OFF, &CMaindialog::OnBnClickedLlcTmpOff)
	ON_BN_CLICKED(IDC_MC_TMP_ON, &CMaindialog::OnBnClickedMcTmpOn)
	ON_BN_CLICKED(IDC_MC_TMP_OFF, &CMaindialog::OnBnClickedMcTmpOff)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_VACCUM_GAUGE_WRITE_START, &CMaindialog::OnBnClickedVaccumGaugeWriteStart)
	ON_BN_CLICKED(IDC_VACCUM_GAUGE_WRITE_STOP, &CMaindialog::OnBnClickedVaccumGaugeWriteStop)
	ON_BN_CLICKED(IDC_STAGE_LOG_WRITE_START, &CMaindialog::OnBnClickedStageLogWriteStart)
	ON_BN_CLICKED(IDC_STAGE_LOG_WRITE_STOP, &CMaindialog::OnBnClickedStageLogWriteStop)
	ON_STN_CLICKED(IDC_ICON_TRGATE, &CMaindialog::OnStnClickedIconTrgate)
	ON_STN_CLICKED(IDC_ICON_LLCGATE, &CMaindialog::OnStnClickedIconLlcgate)
	ON_STN_CLICKED(IDC_ICON_LLC_TMP_GATE, &CMaindialog::OnStnClickedIconLlcTmpGate)
	ON_STN_CLICKED(IDC_ICON_MC_TMP_GATE, &CMaindialog::OnStnClickedIconMcTmpGate)
	ON_STN_CLICKED(IDC_ICON_MC_ROUGH_VALVE, &CMaindialog::OnStnClickedIconMcRoughValve)
	ON_STN_CLICKED(IDC_ICON_MC_FAST_ROUGH, &CMaindialog::OnStnClickedIconMcFastRough)
	ON_STN_CLICKED(IDC_ICON_MC_FORELINE, &CMaindialog::OnStnClickedIconMcForeline)
	ON_STN_CLICKED(IDC_ICON_LLC_FORELINE, &CMaindialog::OnStnClickedIconLlcForeline)
	ON_STN_CLICKED(IDC_ICON_LLC_ROUGH_VALVE, &CMaindialog::OnStnClickedIconLlcRoughValve)
	ON_STN_CLICKED(IDC_ICON_LLC_FAST_ROUGH, &CMaindialog::OnStnClickedIconLlcFastRough)
	ON_STN_CLICKED(IDC_ICON_SLOW_MFC_IN, &CMaindialog::OnStnClickedIconSlowMfcIn)
	ON_STN_CLICKED(IDC_ICON_SLOW_MFC_OUT, &CMaindialog::OnStnClickedIconSlowMfcOut)
	ON_STN_CLICKED(IDC_ICON_FAST_MFC_IN, &CMaindialog::OnStnClickedIconFastMfcIn)
	ON_STN_CLICKED(IDC_ICON_FAST_MFC_OUT, &CMaindialog::OnStnClickedIconFastMfcOut)
END_MESSAGE_MAP()

// CMaindialog 메시지 처리기

BOOL CMaindialog::OnInitDialog()
{
	m_CheckTimer = 1;

	CString ini;
	ini.Empty();

	ini = _T("0");
	Cnt_Error_Slow_MFC = 0;
	Cnt_Error_Fast_MFC = 0;

	/* KJH BIT MAP TEST
	
	CRect m_rect;
	GetDlgItem(IDC_SREM_MAIN_BITMAP)->GetWindowRect(m_rect);
	ScreenToClient(m_rect);
	
	CDC *dc = GetDC();
	CDC memDC;
	
	HANDLE m_hImage;
	BITMAP bmpinfo;
	
	memDC.CreateCompatibleDC(dc);
	m_hImage = LoadImage(0, "main.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	HBITMAP bmpOld = (HBITMAP)memDC.SelectObject(m_hImage);
	
	GetObject(m_hImage, sizeof(BITMAP), &bmpinfo);
	
	dc->StretchBlt(m_rect.left, m_rect.top, m_rect.Width(), m_rect.Height(), &memDC, 0, 0, bmpinfo.bmWidth, bmpinfo.bmHeight, SRCCOPY);
	
	memDC.SelectObject(bmpOld);
	memDC.DeleteDC();
	dc->DeleteDC();

	*/



	// ICON
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);

	// TR, LLC GATE 
	m_GateBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_GATE_CLOSE), IMAGE_BITMAP, 15, 120, LR_DEFAULTCOLOR);
	m_GateBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_GATE_OPEN), IMAGE_BITMAP, 15, 120, LR_DEFAULTCOLOR);

	// TMP GATE VALVE
	m_GateValveBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_GATE_VALVE_CLOSE), IMAGE_BITMAP, 100, 30, LR_DEFAULTCOLOR);
	m_GateValveBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_GATE_VALVE_OPEN), IMAGE_BITMAP, 100, 30, LR_DEFAULTCOLOR);
	
	// VALVE
	m_ValveBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_VALVE_CLOSE), IMAGE_BITMAP, 70, 30, LR_DEFAULTCOLOR);
	m_ValveBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_VALVE_OPEN), IMAGE_BITMAP, 70, 30, LR_DEFAULTCOLOR);
	
	// TMP
	m_TmpBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_TMP_OFF), IMAGE_BITMAP, 120, 20, LR_DEFAULTCOLOR);
	m_TmpBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_TMP_ON), IMAGE_BITMAP, 120, 20, LR_DEFAULTCOLOR);

	// MFC
	m_MFCBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_MFC_OFF), IMAGE_BITMAP, 50, 20, LR_DEFAULTCOLOR);
	m_MFCBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_MFC_ON), IMAGE_BITMAP, 50, 20, LR_DEFAULTCOLOR);

	GetDlgItem(IDC_SREM_MAIN_BITMAP)->ModifyStyle(0, WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0);
	GetDlgItem(IDC_ICON_TRGATE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLCGATE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_TMP_GATE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_TMP_GATE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_ROUGH_VALVE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_FAST_ROUGH)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_FORELINE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_FAST_ROUGH)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_FORELINE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_TMP)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_TMP)->BringWindowToTop();
	GetDlgItem(IDC_ICON_SLOW_MFC_IN)->BringWindowToTop();
	GetDlgItem(IDC_ICON_SLOW_MFC)->BringWindowToTop();
	GetDlgItem(IDC_ICON_SLOW_MFC_OUT)->BringWindowToTop();
	GetDlgItem(IDC_ICON_FAST_MFC_IN)->BringWindowToTop();
	GetDlgItem(IDC_ICON_FAST_MFC)->BringWindowToTop();
	GetDlgItem(IDC_ICON_FAST_MFC_OUT)->BringWindowToTop();


	/* KJH BITMAP INI TEST

	((CStatic*)GetDlgItem(IDC_ICON_TRGATE))->SetIcon(m_GateIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_TRGATE))->SetBitmap(m_GateBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLCGATE))->SetBitmap(m_GateBITMAP[1]);
	
	
	((CStatic*)GetDlgItem(IDC_ICON_MC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[1]);
	
	((CStatic*)GetDlgItem(IDC_ICON_MC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[1]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_FORELINE))->SetBitmap(m_ValveBITMAP[1]);
	
	
	((CStatic*)GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[1]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_FORELINE))->SetBitmap(m_ValveBITMAP[1]);
	
	((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP))->SetBitmap(m_TmpBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_TMP))->SetBitmap(m_TmpBITMAP[1]);
	
	((CStatic*)GetDlgItem(IDC_ICON_MC_DRYPUMP))->SetIcon(m_LedIcon[1]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_DRYPUMP))->SetIcon(m_LedIcon[2]);
	
	((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_IN))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC))->SetBitmap(m_MFCBITMAP[1]);
	((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_OUT))->SetBitmap(m_ValveBITMAP[1]);
	
	((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_IN))->SetBitmap(m_ValveBITMAP[1]);
	((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC))->SetBitmap(m_MFCBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_OUT))->SetBitmap(m_ValveBITMAP[0]);

	*/


	m_brush.CreateSolidBrush(RGB(0, 0, 50)); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial")); 
	// Gague 글자
	GetDlgItem(IDC_STATIC_LLC_VACCUM_GAUGE)->SetFont(&m_font);
	GetDlgItem(IDC_STATIC_MC_VACCUM_GAUGE)->SetFont(&m_font);

	m_main_dialog_brush.CreateSolidBrush(RGB(255, 234, 234)); // Dialog 배경 색

	CString sTempValue;

	g_pVP->m_nLLC_seq_state = State_IDLE_State;
	g_pVP->m_nMC_seq_state = State_IDLE_State;

	m_dMcStandbyVentValue = g_pConfig->m_dPressure_ChangeToVent;
	m_dMcSlowVentValue = g_pConfig->m_dPressure_ChangeToSlow_Vent;
	m_dMcFastVentValue = g_pConfig->m_dPressure_Vent_End;
	m_dMcSlowRoughValue = g_pConfig->m_dPressure_ChangeToFast_MC_Rough;
	m_dMcFastRoughValue = g_pConfig->m_dPressure_ChangeToMCTMP_Rough;
	m_dMcTmpRoughValue = g_pConfig->m_dPressure_Rough_End;

	m_dLlcStandbyVentValue = g_pConfig->m_dPressure_ChangeToVent_LLC;
	m_dLlcSlowVentValue = g_pConfig->m_dPressure_ChangeToSlow2_Vent;
	m_dLlcFastVentValue = g_pConfig->m_dPressure_Vent_End;
	m_dLlcSlowRoughValue = g_pConfig->m_dPressure_ChangeToFast_Rough;
	m_dLlcFastRoughValue = g_pConfig->m_dPressure_ChangeToTMP_Rough;
	m_dLlcTmpRoughValue = g_pConfig->m_dPressure_Rough_End;

	m_nMcStandbyVentTime = g_pConfig->m_nTimeout_sec_MCLLCVent;
	m_nMcSlowVentTime = g_pConfig->m_nTimeout_sec_MCSlow1Vent;
	m_nMcFastVentTime = g_pConfig->m_nTimeout_sec_MCFastVent;
	m_nMcSlowRoughTime = g_pConfig->m_nTimeout_sec_MCSlowRough;
	m_nMcFastRoughTime = g_pConfig->m_nTimeout_sec_MCFastRough;
	m_nMcTmpRoughTime = g_pConfig->m_nTimeout_sec_MCTmpEnd;

	m_nLlcStandbyVentTime = g_pConfig->m_nTimeout_sec_LLKStandbyVent;
	m_nLlcSlowVentTime = g_pConfig->m_nTimeout_sec_LLKSlow1Vent;
	m_nLlcFastVentTime = g_pConfig->m_nTimeout_sec_LLKFastVent;
	m_nLlcSlowRoughTime = g_pConfig->m_nTimeout_sec_LLKSlowRough;
	m_nLlcFastRoughTime = g_pConfig->m_nTimeout_sec_LLKFastRough;
	m_nLlcTmpRoughTime = g_pConfig->m_nTimeout_sec_LLKRoughEnd;

	sTempValue.Format("%lf", m_dMcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nMcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);

	sTempValue.Format("%lf", m_dLlcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nLlcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);



	((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[0]);  // VACCUM GAGUE WRITE ON / OFF
	((CStatic*)GetDlgItem(IDC_ICON_STAGE_LOG_ICON))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(ini);

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);

	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		//m_maindialog_pThread = AfxBeginThread(IoCheckThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
		SetTimer(m_CheckTimer, 100, NULL);
	}
	else
	{
		initial();
	}
	return TRUE;
}

UINT CMaindialog::IoCheckThread(LPVOID pParam)
{
	CMaindialog*  pIoThread = (CMaindialog*)pParam;

	while (!pIoThread->m_maindialog_bThreadExitFlag)
	{
		pIoThread->Update();

		Sleep(50);
	}

	return 0;
}

void CMaindialog::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(m_CheckTimer);
	KillTimer(LLC_SEQUENCE_CHECK_TIMER);
	KillTimer(MC_SEQUENCE_CHECK_TIMER);
	KillTimer(XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING);

	m_maindialog_bThreadExitFlag = TRUE;

	if (m_maindialog_pThread != NULL)
	{
		HANDLE threadHandle = m_maindialog_pThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_maindialog_pThread = NULL;
	}


}

void CMaindialog::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == m_CheckTimer)
	{
		Update();
		SetTimer(m_CheckTimer, 100, NULL);
	}
	else if (nIDEvent == VACCUM_GAUGE_WRITE_TIMER)
	{
		Vacuum_Gague_Write();
		SetTimer(VACCUM_GAUGE_WRITE_TIMER, 30000, NULL);
	}
	else if (nIDEvent == MC_SEQUENCE_CHECK_TIMER)
	{
		MC_Sequence_Time_Check();
		SetTimer(MC_SEQUENCE_CHECK_TIMER, 100, NULL);
	}
	else if (nIDEvent == LLC_SEQUENCE_CHECK_TIMER)
	{
		LLC_Sequence_Time_Check();
		SetTimer(LLC_SEQUENCE_CHECK_TIMER, 100, NULL);
	}
	else if (nIDEvent == XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING)
	{
		XrayCameraGetTempe();
		SetTimer(XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING, 100, NULL);
	}
	CDialogEx::OnTimer(nIDEvent);
}

void CMaindialog::initial()
{
	SetDlgItemText(IDC_MC_SEQUENCE_STATE, "연결 확인 필요");
	SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "연결 확인 필요");

	SetDlgItemText(IDC_SLOW_MFC_INPUT, "연결 확인 필요");
	SetDlgItemText(IDC_FAST_MFC_INPUT, "연결 확인 필요");
	SetDlgItemText(IDC_SLOW_MFC_OUTPUT, "연결 확인 필요");
	SetDlgItemText(IDC_FAST_MFC_OUTPUT, "연결 확인 필요");

	SetDlgItemText(IDC_MC_LINE_STATE_1, "연결 확인 필요");
	SetDlgItemText(IDC_MC_LINE_STATE_2, "연결 확인 필요");
	SetDlgItemText(IDC_LLC_LINE_STATE_1, "연결 확인 필요");
	SetDlgItemText(IDC_LLC_LINE_STATE_2, "연결 확인 필요");

	SetDlgItemText(IDC_LLC_VACCUM_GAUGE, "연결 확인 필요");
	SetDlgItemText(IDC_MC_VACCUM_GAUGE, "연결 확인 필요");

	SetDlgItemText(IDC_STATIC_LLC_VACCUM_GAUGE, "연결 확인 필요");
	SetDlgItemText(IDC_STATIC_MC_VACCUM_GAUGE, "연결 확인 필요");
	SetDlgItemText(IDC_STATIC_LLC_VACCUM_GAUGE_STATE, "연결 확인 필요");
	SetDlgItemText(IDC_STATIC_MC_VACCUM_GAUGE_STATE, "연결 확인 필요");
	SetDlgItemText(IDC_STATIC_MC_TMP_STATE, "연결 확인 필요");
	SetDlgItemText(IDC_STATIC_LLC_TMP_STATE, "연결 확인 필요");

	((CStatic*)GetDlgItem(IDC_ICON_TRGATE))->SetBitmap(m_GateBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_TMP))->SetBitmap(m_TmpBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_FORELINE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_LINE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP))->SetBitmap(m_TmpBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_FORELINE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_LINE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLCGATE))->SetBitmap(m_GateBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_IN))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC))->SetBitmap(m_MFCBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_OUT))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_IN))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC))->SetBitmap(m_MFCBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_OUT))->SetBitmap(m_ValveBITMAP[0]);

	((CStatic*)GetDlgItem(IDC_ICON_MC_DRYPUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_DRYPUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_SUPPLY))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_AIR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_WATER))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_LLC_TMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_MC_TMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_CB))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_VAC_ST))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_MC))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_LLC))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN_TEMP_ALARM))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SOURCE_GATE_ON))->SetIcon(m_LedIcon[0]); 
	((CStatic*)GetDlgItem(IDC_ICON_MC_VAC_EXTEND))->SetIcon(m_LedIcon[0]);			// VAC ROBOT HAND EXTEND STATUS OFF (MC)
	((CStatic*)GetDlgItem(IDC_ICON_LLC_VAC_EXTEND))->SetIcon(m_LedIcon[0]);			// VAC ROBOT HAND EXTEND STATUS OFF (LLC)
	((CStatic*)GetDlgItem(IDC_ICON_VAC_EXTEND))->SetIcon(m_LedIcon[0]);				// VAC ROBOT ARM EXTEND STATUS OFF
	((CStatic*)GetDlgItem(IDC_ICON_VAC_RETRACT))->SetIcon(m_LedIcon[0]);			// VAC ROBOT ARM RETRACT STATUS ON
	((CStatic*)GetDlgItem(IDC_ICON_ATM_RETRACT))->SetIcon(m_LedIcon[0]);			// ATM ROBOT ARM RETRACT STATUS ON
	((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[0]);			// VACCUM GAGUE WRITE ON / OFF
	((CStatic*)GetDlgItem(IDC_ICON_STAGE_LOG_ICON))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_FLIPPER))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_LOADING_POSITION))->SetIcon(m_LedIcon[0]);

	

	
	
}

void CMaindialog::Update()
{
	//Main Stage Loading Position Check
	Main_Stage_Loading_Position_Check();

	//Mask location Check
	Mask_Location_Check();

	//IO state check
	IO_State_Update();

	//Line state check _ mks 722b sensor
	Line_State_Update();

	//Vent State
	Vent_State_Update();

	//LLC TMP 
	LLC_Tmp_State_Update();

	//MC TMP 
	MC_Tmp_State_Update();

	//Gauge
	Gauge_State_Update();

	// MAIN VALVE CHECK

	//g_pIO->Main_Error_Status();

	// Pumping , Venting Auto Sequence 상태 표기
	switch (g_pVP->m_nMC_seq_state)
	{
	case Pumping_ERROR_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Pumping 중 ERROR 발생");
		break;
	case Venting_ERROR_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Venting 중 ERROR 발생");
		break;
	case State_IDLE_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Sequence 동작 가능 상태");
		break;
	case Pumping_START_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Pumping Start");
		break;
	case Venting_START_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Pumping Start");
		break;
	case Pumping_SLOWROUGH_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Slow Rough 진행 중");
		break;
	case Pumping_FASTROUGH_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Fast Rough 진행 중");
		break;
	case Pumping_TMPROUGH_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Tmp Rough 진행 중");
		break;
	case Pumping_COMPLETE_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Pumping 완료");
		WaitSec(5);
		g_pVP->m_nMC_seq_state = State_IDLE_State;
		break;
	case Venting_SLOWVENT_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Slow Vent 진행 중");
		break;
	case Venting_FASTVENT_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Fast Vent 진행 중");
		break;
	case Venting_COMPLETE_State:
		SetDlgItemText(IDC_MC_SEQUENCE_STATE, "MC Venting 완료");
		WaitSec(5);
		g_pVP->m_nMC_seq_state = State_IDLE_State;
		break;
	}

	if ((g_pVP->m_nMC_seq_state == Venting_SLOWVENT_State) || (g_pVP->m_nMC_seq_state == Venting_FASTVENT_State ))
	{
		 g_pVP->m_nLLC_seq_state = State_IDLE_State;
		 SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "MC Venting 진행 중(LLC 동작 불가)");
	}
	else
	{
		switch (g_pVP->m_nLLC_seq_state)
		{
		case LLC_Venting_PreWork_Error_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Venting PreWork Error 발생");
			break;
		case LLC_Venting_SlowVent_Error_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Venting SlowVent Error 발생");
			break;
		case LLC_Venting_FastVent_Error_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Venting FastVent Error 발생");
			break;
		case LLC_Pumping_PreWork_Error_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Pumping PreWork Error 발생");
			break;
		case LLC_Pumping_Slow_Rough_Error_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Pumping SlowRough Error 발생");
			break;
		case LLC_Pumping_Fast_Rough_Error_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Pumping FastRougn Error 발생");
			break;
		case LLC_Pumping_Tmp_Rough_Error_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Pumping TmpRough Error 발생");
			break;
		case LLC_Pumping_Complete_Error_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Pumping Complete Error 발생");
			break;
		case Pumping_ERROR_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Pumping 중 ERROR 발생");
			break;
		case Venting_ERROR_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Venting 중 ERROR 발생");
			break;
		case State_IDLE_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Sequence 동작 가능 상태");
			break;
		case Pumping_START_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Pumping Start");
			break;
		case Venting_START_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Pumping Start");
			break;
		case Pumping_SLOWROUGH_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Slow Rough 진행 중");
			break;
		case Pumping_FASTROUGH_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Fast Rough 진행 중");
			break;
		case Pumping_TMPROUGH_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Tmp Rough 진행 중");
			break;
		case Pumping_COMPLETE_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Pumping 완료");
			WaitSec(5);
			g_pVP->m_nLLC_seq_state = State_IDLE_State;
			break;
		case Venting_SLOWVENT_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Slow Vent 진행 중");
			break;
		case Venting_FASTVENT_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Fast Vent 진행 중");
			break;
		case Venting_COMPLETE_State:
			SetDlgItemText(IDC_LLC_SEQUENCE_STATE, "LLC Venting 완료");
			WaitSec(5);
			g_pVP->m_nLLC_seq_state = State_IDLE_State;
			break;
		}
	}
}

void CMaindialog::IO_State_Update()
{

	if (g_pIO->m_bDigitalIn[77] == true) // MAIN MASK 유무 감지
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_MC))->SetIcon(m_LedIcon[1]);
	}
	else if (g_pIO->m_bDigitalIn[77] == false) // 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_MC))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[74] == true) // LLC MASK 유무 감지 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_LLC))->SetIcon(m_LedIcon[1]);
	}
	else if (g_pIO->m_bDigitalIn[74] == false) // 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_LLC))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[78] == true) // MC MASK 기울어짐 #1
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR))->SetIcon(m_LedIcon[2]);
	}
	else if (g_pIO->m_bDigitalIn[78] == false) // 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[79] == true)  // MC MASK 기울어짐 #2
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR2))->SetIcon(m_LedIcon[2]);
	}
	else if (g_pIO->m_bDigitalIn[79] == false) // 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR2))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[75] == true) // LLC MASK 기울어짐 #1
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR))->SetIcon(m_LedIcon[2]);
	}
	else if (g_pIO->m_bDigitalIn[75] == false) // 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[76] == true) // LLC MASK 기울어짐 #2
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR2))->SetIcon(m_LedIcon[2]);
	}
	else if (g_pIO->m_bDigitalIn[76] == false) // 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR2))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[23] == true) // WATER LEAK SENSOR (LLC TMP)
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_LLC_TMP))->SetIcon(m_LedIcon[0]);
	}
	else if (g_pIO->m_bDigitalIn[23] == false)
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_LLC_TMP))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[24] == true) // WATER LEAK SENSOR (MC TMP)
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_MC_TMP))->SetIcon(m_LedIcon[0]);
	}
	else if (g_pIO->m_bDigitalIn[24] == false)
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_MC_TMP))->SetIcon(m_LedIcon[2]);
	}
	if (g_pIO->m_bDigitalIn[26] == true) // SMOKE DETACT SENSOR (CB)
	{
		((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_CB))->SetIcon(m_LedIcon[0]);
	}
	else if (g_pIO->m_bDigitalIn[26] == false)
	{
		((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_CB))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[27] == true) // SMOKE DETACT SENSOR (VAC ST)
	{
		((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_VAC_ST))->SetIcon(m_LedIcon[2]);
	}
	else if (g_pIO->m_bDigitalIn[27] == false)
	{
		((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_VAC_ST))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[69] == true) // WATER RUTRUN TEMP ALARM
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN_TEMP_ALARM))->SetIcon(m_LedIcon[0]);
	}
	else if (g_pIO->m_bDigitalIn[69] == false)
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN_TEMP_ALARM))->SetIcon(m_LedIcon[2]);
	}
	if (g_pIO->m_bDigitalOut[44] == true) // WATER SUPPLY VALVE ON / OFF
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_SUPPLY))->SetIcon(m_LedIcon[1]);
	}
	else if (g_pIO->m_bDigitalOut[44] == false)
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_SUPPLY))->SetIcon(m_LedIcon[2]);
	}
	if (g_pIO->m_bDigitalOut[45] == true) // WATER RETURN VALVE ON / OFF
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN))->SetIcon(m_LedIcon[1]);
	}
	else if (g_pIO->m_bDigitalOut[45] == false)
	{
		((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN))->SetIcon(m_LedIcon[2]);
	}
	if (g_pIO->m_bDigitalIn[0] == true) // Main Air ON / OFF
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_AIR))->SetIcon(m_LedIcon[1]);
	}
	else if (g_pIO->m_bDigitalOut[0] == false)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_AIR))->SetIcon(m_LedIcon[2]);
	}
	if (g_pIO->m_bDigitalIn[1] == true) // Main Water ON / OFF
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_WATER))->SetIcon(m_LedIcon[1]);
	}
	else if (g_pIO->m_bDigitalOut[1] == false)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_WATER))->SetIcon(m_LedIcon[2]);
	}


	if ((g_pIO->m_bDigitalIn[12] == true) && (g_pIO->m_bDigitalIn[13] == false))
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[1]); // MC TMP GATE OPEN
	}
	if ((g_pIO->m_bDigitalIn[12] == false) && (g_pIO->m_bDigitalIn[13] == true))
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[0]); // MC TMP GATE CLOSE
	}
	if ((g_pIO->m_bDigitalIn[108] == true) && (g_pIO->m_bDigitalIn[109] == false))
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_FORELINE))->SetBitmap(m_ValveBITMAP[1]); // MC FORELINE GATE OPEN
	}
	if ((g_pIO->m_bDigitalIn[108] == false) && (g_pIO->m_bDigitalIn[109] == true))
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_FORELINE))->SetBitmap(m_ValveBITMAP[0]); // MC FORELINE GATE CLOSE
	}
	if ((g_pIO->m_bDigitalIn[104] == true) && (g_pIO->m_bDigitalIn[105] == false))
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[1]); // MC FAST ROUGH OPEN
	}
	if ((g_pIO->m_bDigitalIn[104] == false) && (g_pIO->m_bDigitalIn[105] == true))
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[0]); // MC FAST ROUGH CLOSE
	}
	if (g_pIO->m_bDigitalOut[11] == true) {
		((CStatic*)GetDlgItem(IDC_ICON_MC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[1]); // MC SLOW ROUGH OPEN
	}
	if (g_pIO->m_bDigitalOut[11] == false) {
		((CStatic*)GetDlgItem(IDC_ICON_MC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]); // MC SLOW ROUGH CLOSE
	}
	if ((g_pIO->m_bDigitalIn[4] == true) && (g_pIO->m_bDigitalIn[5] == false))
	{
		((CStatic*)GetDlgItem(IDC_ICON_TRGATE))->SetBitmap(m_GateBITMAP[1]);// TR GATE OPEN
	}
	if ((g_pIO->m_bDigitalIn[4] == false) && (g_pIO->m_bDigitalIn[5] == true))
	{
		((CStatic*)GetDlgItem(IDC_ICON_TRGATE))->SetBitmap(m_GateBITMAP[0]); // TR GATE CLOSE
	}
	if ((g_pIO->m_bDigitalIn[10] == true) && (g_pIO->m_bDigitalIn[11] == false))
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[1]); // LLC TMP GATE OPEN
	}
	if ((g_pIO->m_bDigitalIn[10] == false) && (g_pIO->m_bDigitalIn[11] == true))
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[0]); // LLC TMP GATE CLOSE
	}
	if ((g_pIO->m_bDigitalIn[106] == true) && (g_pIO->m_bDigitalIn[107] == false))
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_FORELINE))->SetBitmap(m_ValveBITMAP[1]); // LLC FORELINE OPEN
	}
	if ((g_pIO->m_bDigitalIn[106] == false) && (g_pIO->m_bDigitalIn[107] == true))
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_FORELINE))->SetBitmap(m_ValveBITMAP[0]);  // LLC FORELINE CLOSE
	}
	if ((g_pIO->m_bDigitalIn[2] == true) && (g_pIO->m_bDigitalIn[3] == false))
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLCGATE))->SetBitmap(m_GateBITMAP[1]);// LLC GATE OPEN
	}
	if ((g_pIO->m_bDigitalIn[2] == false) && (g_pIO->m_bDigitalIn[3] == true))
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLCGATE))->SetBitmap(m_GateBITMAP[0]); // LLC GATE CLOSE
	}
	if (g_pIO->m_bDigitalIn[101] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[1]); // LLC FAST ROUGH OPEN
	}
	if (g_pIO->m_bDigitalIn[102] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[0]); // LLC FAST ROUGH CLOSE
	}
	if (g_pIO->m_bDigitalOut[7] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[1]);// LLC SLOW ROUGH OPEN
	}
	if (g_pIO->m_bDigitalOut[7] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]); // LLC SLOW ROUGH CLOSE
	}
	if (g_pIO->m_bDigitalOut[19] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_OUT))->SetBitmap(m_ValveBITMAP[1]); // MFC FAST OUTLET OPEN
	}
	if (g_pIO->m_bDigitalOut[19] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_OUT))->SetBitmap(m_ValveBITMAP[0]);  // MFC FAST OUTLET CLOSE
	}
	if (g_pIO->m_bDigitalOut[20] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_OUT))->SetBitmap(m_ValveBITMAP[1]); // MFC SLOW OUTLET OPEN
	}
	if (g_pIO->m_bDigitalOut[20] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_OUT))->SetBitmap(m_ValveBITMAP[0]); // MFC SLOW OUTLET CLOSE
	}
	if (g_pIO->m_bDigitalOut[9] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_IN))->SetBitmap(m_ValveBITMAP[1]);  // MFC FAST INLET OPEN
	}
	if (g_pIO->m_bDigitalOut[9] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_IN))->SetBitmap(m_ValveBITMAP[0]);  // MFC FAST INLET CLOSE
	}
	if (g_pIO->m_bDigitalOut[10] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_IN))->SetBitmap(m_ValveBITMAP[1]); // MFC SLOW INLET OPEN
	}
	if (g_pIO->m_bDigitalOut[10] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_IN))->SetBitmap(m_ValveBITMAP[0]); // MFC SLOW INLET CLOSE
	}
	if (g_pIO->m_bDigitalIn[16] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_DRYPUMP))->SetIcon(m_LedIcon[1]); // LLC DRY PUMP ON
	}
	if (g_pIO->m_bDigitalIn[16] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_DRYPUMP))->SetIcon(m_LedIcon[2]); // LLC DRY PUMP OFF
	}
	if (g_pIO->m_bDigitalIn[19] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_DRYPUMP))->SetIcon(m_LedIcon[1]); // MC DRY PUMP ON
	}
	if (g_pIO->m_bDigitalIn[19] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_DRYPUMP))->SetIcon(m_LedIcon[2]);  // MC DRY PUMP OFF
	}
	if (g_pIO->m_bDigitalOut[42] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_GATE_ON))->SetIcon(m_LedIcon[1]);  // SOURCE GATE ON
	}
	if (g_pIO->m_bDigitalOut[42] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_GATE_ON))->SetIcon(m_LedIcon[0]);  // SOURCE GATE OFF
	}
	if (g_pIO->m_bDigitalIn[81] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_VAC_EXTEND))->SetIcon(m_LedIcon[1]);  // VAC ROBOT HAND EXTEND STATUS ON (MC)
	}
	if (g_pIO->m_bDigitalIn[81] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_VAC_EXTEND))->SetIcon(m_LedIcon[0]);  // VAC ROBOT HAND EXTEND STATUS OFF (MC)
	}
	if (g_pIO->m_bDigitalIn[73] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_VAC_EXTEND))->SetIcon(m_LedIcon[1]);  // VAC ROBOT HAND EXTEND STATUS ON (LLC)
	}
	if (g_pIO->m_bDigitalIn[73] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_VAC_EXTEND))->SetIcon(m_LedIcon[0]);  // VAC ROBOT HAND EXTEND STATUS OFF (LLC)
	}
	if (g_pIO->m_bDigitalIn[87] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_VAC_EXTEND))->SetIcon(m_LedIcon[1]);  // VAC ROBOT ARM EXTEND STATUS ON
	}
	if (g_pIO->m_bDigitalIn[87] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_VAC_EXTEND))->SetIcon(m_LedIcon[0]);  // VAC ROBOT ARM EXTEND STATUS OFF
	}
	if (g_pIO->m_bDigitalIn[88] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_VAC_RETRACT))->SetIcon(m_LedIcon[1]);  // VAC ROBOT ARM RETRACT STATUS ON
	}
	if (g_pIO->m_bDigitalIn[88] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_VAC_RETRACT))->SetIcon(m_LedIcon[0]);  // VAC ROBOT ARM RETRACT STATUS ON
	}
	if (g_pIO->m_bDigitalIn[90] == true) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_ATM_RETRACT))->SetIcon(m_LedIcon[1]);  // ATM ROBOT ARM RETRACT STATUS ON
	}
	if (g_pIO->m_bDigitalIn[90] == false) 
	{
		((CStatic*)GetDlgItem(IDC_ICON_ATM_RETRACT))->SetIcon(m_LedIcon[0]);  // ATM ROBOT ARM RETRACT STATUS ON
	}
	if (g_pLLKTmp_IO->m_LLC_Tmp_State == TMP_NORMAL)
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP))->SetBitmap(m_TmpBITMAP[1]); // LLC TMP ON
	}
	if (g_pLLKTmp_IO->m_LLC_Tmp_State != TMP_NORMAL)
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP))->SetBitmap(m_TmpBITMAP[0]); // LLC TMP OFF
	}
	if (g_pMCTmp_IO->m_mc_tmp_state == TMP_NORMAL)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_TMP))->SetBitmap(m_TmpBITMAP[1]); // MC TMP ON
	}
	if (g_pMCTmp_IO->m_mc_tmp_state != TMP_NORMAL)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MC_TMP))->SetBitmap(m_TmpBITMAP[0]);// MC TMP OFF
	}

	//GetDlgItem(IDC_SREM_MAIN_BITMAP)->ModifyStyle(0, WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0);
	//GetDlgItem(IDC_ICON_TRGATE)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_LLCGATE)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_MC_TMP_GATE)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_LLC_TMP_GATE)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_MC_ROUGH_VALVE)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_MC_FAST_ROUGH)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_MC_FORELINE)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_LLC_FAST_ROUGH)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_LLC_FORELINE)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_LLC_TMP)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_MC_TMP)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_SLOW_MFC_IN)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_SLOW_MFC)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_SLOW_MFC_OUT)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_FAST_MFC_IN)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_FAST_MFC)->BringWindowToTop();
	//GetDlgItem(IDC_ICON_FAST_MFC_OUT)->BringWindowToTop();
}

void CMaindialog::Line_State_Update()
{
	CString nCh_1_str;
	CString nCh_2_str;

	double nCh_1 = (double)(g_pIO->m_nDecimal_Analog_in_ch_1) / 1638.0; // LLC FORELINE GAUGE 722b monitor
	double nCh_2 = (double)(g_pIO->m_nDecimal_Analog_in_ch_2) / 1638.0; // MC FORELINE GAUGE 722b monitor

	nCh_1_str.Format("%0.2f", nCh_1);
	nCh_2_str.Format("%0.2f", nCh_2);

	double nCh_1_torr = nCh_1 / 10;
	double nCh_2_torr = nCh_2 / 10;

	if (nCh_1_torr == 0.0)
	{
		nCh_1_torr = 0.0005;
	}

	if (nCh_2_torr == 0.0)
	{
		nCh_2_torr = 0.0005;
	}

	//double nCh_1_torr = 0.0000456;
	//double nCh_2_torr = 7600;

	CString nch_1_torr_string;
	CString nch_2_torr_string;

	nch_1_torr_string.Format("%.2E", nCh_1_torr);
	nch_2_torr_string.Format("%.2E", nCh_2_torr);


	// MC, LLC LINE MKS 722B GAUGE
	SetDlgItemText(IDC_LLC_LINE_STATE_1, nch_1_torr_string + " Torr");
	SetDlgItemText(IDC_MC_LINE_STATE_1, nch_2_torr_string + " Torr");


	// LLC LINE STATE (MKS 722B)
	if (nCh_1 > 9.0)
	{
		SetDlgItemText(IDC_LLC_LINE_STATE_2, "Line 대기상태");
		((CStatic*)GetDlgItem(IDC_ICON_LLC_LINE))->SetIcon(m_LedIcon[2]);
	}
	else if (nCh_1 < 9.0 && nCh_1 > 2.0)
	{
		SetDlgItemText(IDC_LLC_LINE_STATE_2, "Line Pumping 중");
		((CStatic*)GetDlgItem(IDC_ICON_LLC_LINE))->SetIcon(m_LedIcon[2]);
	}
	else if (nCh_1 < 2.0)
	{
		SetDlgItemText(IDC_LLC_LINE_STATE_2, "Line 진공상태");
		((CStatic*)GetDlgItem(IDC_ICON_LLC_LINE))->SetIcon(m_LedIcon[1]);
	}

	// MC LINE STATE (MKS 722B)
	if (nCh_2 > 9.0)
	{
		SetDlgItemText(IDC_MC_LINE_STATE_2, "Line 대기상태");
		((CStatic*)GetDlgItem(IDC_ICON_MC_LINE))->SetIcon(m_LedIcon[2]);
	}
	else if (nCh_2 < 9.0 && nCh_2 > 2.0)
	{
		SetDlgItemText(IDC_MC_LINE_STATE_2, "Line Pumping 중");
		((CStatic*)GetDlgItem(IDC_ICON_MC_LINE))->SetIcon(m_LedIcon[2]);
	}
	else if (nCh_2 < 2.0)
	{
		SetDlgItemText(IDC_MC_LINE_STATE_2, "Line 진공상태");
		((CStatic*)GetDlgItem(IDC_ICON_MC_LINE))->SetIcon(m_LedIcon[1]);
	}

}

void CMaindialog::Vent_State_Update()
{
	CString str;
	CString nCh_5_str;
	CString nCh_6_str;
	CString nCh_5_str_sccm;
	CString nCh_6_str_sccm;
	CString nCh_5_out_str_sccm;
	CString nCh_6_out_str_sccm;

	unsigned long long mfc_ret = 0x00;

	double nCh_5_diff;
	double nCh_6_diff;

	double nCh_5 = (double)(g_pIO->m_nDecimal_Analog_in_ch_5) / 1638.0; // #mfc#2 N2 10 sccm
	double nCh_6 = (double)(g_pIO->m_nDecimal_Analog_in_ch_6) / 1638.0; // #mfc#1 N2 20000 sccm

	double nCh_5_out = (double)(g_pIO->m_nDecimal_Analog_out_ch_1) / 409.5; // #mfc#2 N2 10 sccm
	double nCh_6_out = (double)(g_pIO->m_nDecimal_Analog_out_ch_2) / 409.5; // #mfc#1 N2 20000 sccm


	double nCh_5_MFC2 = nCh_5 * 2;
	double nCh_6_MFC1 = nCh_6 * 4000;

	double nCh_5_MFC2_OUT = nCh_5_out * 2;
	double nCh_6_MFC1_OUT = nCh_6_out * 4000;



	// (MFC 입력값 - MFC 출력값)
	nCh_5_diff = (nCh_5_MFC2_OUT - nCh_5_MFC2);
	nCh_6_diff = (nCh_6_MFC1_OUT - nCh_6_MFC1);

	//nCh_5_diff = (nCh_5_MFC2 - nCh_5_MFC2_OUT);
	//nCh_6_diff = (nCh_6_MFC1 - nCh_6_MFC1_OUT);

	nCh_5_str.Format("%0.2f", nCh_5);
	nCh_6_str.Format("%0.2f", nCh_6);

	nCh_5_str_sccm.Format("%0.2f", nCh_5_MFC2);
	nCh_6_str_sccm.Format("%0.1f", nCh_6_MFC1);

	nCh_5_out_str_sccm.Format("%0.2f", nCh_5_MFC2_OUT);
	nCh_6_out_str_sccm.Format("%0.1f", nCh_6_MFC1_OUT);


	// MFC STATE 
	SetDlgItemText(IDC_SLOW_MFC_INPUT, nCh_5_str_sccm);
	SetDlgItemText(IDC_FAST_MFC_INPUT, nCh_6_str_sccm);
	SetDlgItemText(IDC_SLOW_MFC_OUTPUT, nCh_5_out_str_sccm);
	SetDlgItemText(IDC_FAST_MFC_OUTPUT, nCh_6_out_str_sccm);

	//SaveLogFile("SLOW_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_5_str_sccm + " Output : " + nCh_5_out_str_sccm))); // input mfc
	//SaveLogFile("FAST_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_6_str_sccm + " Output : " + nCh_6_out_str_sccm))); // input mfc

	// Sequence 시 MFC 유량 Log 
	if ((g_pVP->m_bVacuumThreadStop == FALSE) || (g_pVP->m_b_MC_VacuumThreadStop == FALSE)) // MC 또는 LLC sequence 가 실행 중이라면..
	{
		if ((g_pVP->m_Mc_Vacuum_State == g_pVP->MC_Venting_SLOWVENT) || (g_pVP->m_Mc_Vacuum_State == g_pVP->MC_Venting_FASTVENT) || (g_pVP->m_Vacuum_State == g_pVP->Venting_SLOWVENT) || (g_pVP->m_Vacuum_State == g_pVP->Venting_FASTVENT))
		{
			SaveLogFile("SLOW_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_5_str_sccm + " Output : " + nCh_5_out_str_sccm))); // input mfc
			SaveLogFile("FAST_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_6_str_sccm + " Output : " + nCh_6_out_str_sccm))); // input mfc
		}
	}

	// SLOW VENT
	if (g_pIO->Is_SlowVentValve1_Open() == VALVE_OPENED)
	{
		SetDlgItemText(IDC_SLOW_MFC_1, nCh_5_str + " V");
		SetDlgItemText(IDC_SLOW_MFC_2, nCh_5_str_sccm + " Sccm");
		if (nCh_5 > 0.1)
			((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC))->SetBitmap(m_MFCBITMAP[1]);
		else if (nCh_5 < 0.1)
			((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC))->SetBitmap(m_MFCBITMAP[0]);
	}
	else if (g_pIO->Is_SlowVentValve1_Open() == VALVE_CLOSED)
	{
	
		SetDlgItemText(IDC_SLOW_MFC_1, " 0 V");
		SetDlgItemText(IDC_SLOW_MFC_2, "0 Sccm");
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC))->SetBitmap(m_MFCBITMAP[0]);
		Cnt_Error_Slow_MFC = 0;
	}
	
	
	// FAST VENT
	if (g_pIO->Is_FastVentValve_Open() == VALVE_OPENED)
	{
		SetDlgItemText(IDC_FAST_MFC_1, nCh_6_str + " V");
		SetDlgItemText(IDC_FAST_MFC_2, nCh_6_str_sccm + " Sccm");
		if (nCh_6 > 0.1)
			((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC))->SetBitmap(m_MFCBITMAP[1]);
		else if (nCh_6 < 0.1)
			((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC))->SetBitmap(m_MFCBITMAP[0]);
	}
	else if (g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED)
	{
		SetDlgItemText(IDC_FAST_MFC_1, " 0 V");
		SetDlgItemText(IDC_FAST_MFC_2, " 0 Sccm");
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC))->SetBitmap(m_MFCBITMAP[0]);
		Cnt_Error_Fast_MFC = 0;
	}
	

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 질소 유량 적을 시 알람 띄우고 멈춤 함수
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	///////////////////////
	// SLOW MFC 유량 확인
	///////////////////////

	if ((SLOW_MFC_LIMIT < nCh_5_diff))
	{
		Cnt_Error_Slow_MFC++;
		if (CHECK_MFC_NUM < Cnt_Error_Slow_MFC)
		{
			if (g_pIO->Is_SlowVentValve1_Open() == VALVE_OPENED)
			{
				SetDlgItemText(IDC_SLOW_MFC_STATE, "ERROR : 질소 확인 필요");
				str = " Slow Vent Error :: 질소 유량 부족에 따른 Error 발생";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				str = " Slow Vent Error :: 질소 유량 부족에 따른 Slow Vent Valve Close";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				/////////////////////////////////////
				// 별도의 ERROR SEQUENCE 추가 해야함.
				/////////////////////////////////////
				if (g_pIO->Close_SlowVentValve1() != TRUE)
				{
					str = " Slow Vent Error :: 질소 확인 ERROR 에 따른 SLOW VENT VALVE CLOSE 실패";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
				}
				g_pLog->Display(0, g_pIO->Log_str);
	
				if (g_pVP->m_bVacuumThreadStop == FALSE) // LLC sequence 가 실행 중이라면..
				{
					str = " Slow Vent Error :: 질소 유량 부족에 따른 LLC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_pVaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_pVaccumThread, &dwResult);
	
					delete g_pVP->m_pVaccumThread;
					g_pVP->m_pVaccumThread = NULL;
	
					g_pVP->m_Vacuum_State = g_pVP->Pumping_ERROR;

					str = " [LLC Venting] Slow Vent Error :: 질소 유량 부족에 따른 LLC Vacuum Sequence 정지";
					SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
	
					//m_VentingState = FALSE;

					//g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
					//g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
				}
				else if (g_pVP->m_b_MC_VacuumThreadStop == FALSE) // MC sequence 가 실행 중이라면..
				{
					str = " Slow Vent Error :: 질소 유량 부족에 따른 MC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_p_MC_VaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_p_MC_VaccumThread, &dwResult);
	
					delete g_pVP->m_p_MC_VaccumThread;
					g_pVP->m_p_MC_VaccumThread = NULL;
					g_pVP->m_Mc_Vacuum_State = g_pVP->MC_Pumping_ERROR;

					str = " [MC Venting] Slow Vent Error :: 질소 유량 부족에 따른 MC Vacuum Sequence 정지";
					SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
					g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
				}

				str = " Slow Vent Error :: 질소 유량 부족에 따른 Slow MFC Module 정지";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				
				g_pIO->WriteOutputData(0, &mfc_ret, 2);  //Slow Mfc 유량 0 으로 reset
				::AfxMessageBox("질소 부족 Slow Vent Error", MB_ICONERROR);
				Cnt_Error_Slow_MFC = 0;
			}
			else
			{
				SetDlgItemText(IDC_SLOW_MFC_STATE, "ERROR : Valve Line 확인 필요");
				str = " Slow Vent Error :: Valve Line 확인 필요 ";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				Cnt_Error_Slow_MFC = 0;
				//m_VentingState = FALSE;
			}
		}
	}
	else
	{
		SetDlgItemText(IDC_SLOW_MFC_STATE, "정상 작동 중");
		Cnt_Error_Slow_MFC = 0;
		//m_VentingState = TRUE;
	}


	/////////////////////////
	//// FAST MFC 유량 확인
	/////////////////////////

	if ((FAST_MFC_LIMIT < nCh_6_diff))
	{
		Cnt_Error_Fast_MFC++;
		if (CHECK_MFC_NUM < Cnt_Error_Fast_MFC)
		{
			if (g_pIO->Is_FastVentValve_Open() == VALVE_OPENED)
			{
				SetDlgItemText(IDC_FAST_MFC_STATE, "ERROR : 질소 확인 필요");
				str = " Fast Vent Error :: 질소 유량 부족에 따른 Error 발생";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				str = " Fast Vent Error :: 질소 유량 부족에 따른 Fast Vent Valve Close";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				/////////////////////////////////////////////////
				// Fast Vent Valve 가 Open 되어 있다면 Close.
				/////////////////////////////////////////////////
				if (g_pIO->Close_FastVentValve() != OPERATION_COMPLETED)
				{
					str = " Fast Vent Error :: 질소 확인 ERROR 에 따른 FAST VENT VALVE CLOSE 실패";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				}
				g_pLog->Display(0, g_pIO->Log_str);

				/////////////////////////////////////////////////
				// LLC sequence 가 실행 중이라면..Sequence 종료
				/////////////////////////////////////////////////
				if (g_pVP->m_bVacuumThreadStop == FALSE) 
				{

					str = " Fast Vent Error :: 질소 유량 부족에 따른 LLC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_pVaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_pVaccumThread, &dwResult);
	
					delete g_pVP->m_pVaccumThread;
					g_pVP->m_pVaccumThread = NULL;
					g_pVP->m_Vacuum_State = g_pVP->Pumping_ERROR;

					str = " [LLC Venting] Fast Vent Error :: 질소 유량 부족에 따른 LLC Vacuum Sequence 정지";
					SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
					//m_VentingState = FALSE;
					g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
				}
				/////////////////////////////////////////////////
				// MC sequence 가 실행 중이라면...Sequence 종료
				/////////////////////////////////////////////////
				else if (g_pVP->m_b_MC_VacuumThreadStop == FALSE) 
				{

					str = " Fast Vent Error :: 질소 유량 부족에 따른 MC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_p_MC_VaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_p_MC_VaccumThread, &dwResult);
	
					delete g_pVP->m_p_MC_VaccumThread;
					g_pVP->m_p_MC_VaccumThread = NULL;
					g_pVP->m_Mc_Vacuum_State = g_pVP->MC_Pumping_ERROR;

					str = " [MC Venting] Fast Vent Error :: 질소 유량 부족에 따른 MC Vacuum Sequence 정지";
					SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);

					g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
				}


				str = " Fast Vent Error :: 질소 유량 부족에 따른 Fast MFC Module 정지";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				g_pIO->WriteOutputData(2, &mfc_ret, 2); //Fast Mfc 유량 0 으로 reset
				::AfxMessageBox("질소 부족 Fast Vent Error",MB_ICONERROR);
				Cnt_Error_Fast_MFC = 0;
			}
			else
			{
				SetDlgItemText(IDC_FAST_MFC_STATE, "ERROR : Valve Line 확인 필요");
				str = " Fast Vent Error :: Valve Line 확인 필요 ";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				Cnt_Error_Fast_MFC = 0;
			}
		}
	}
	else
	{
		SetDlgItemText(IDC_FAST_MFC_STATE, "정상 작동 중");
		Cnt_Error_Fast_MFC = 0;
	}
	

	if (nCh_5 < 0.5)
	{
		SetDlgItemText(IDC_SLOW_MFC_STATE, "동작 대기 중");
	}


	if (nCh_6 < 0.5)
	{
		SetDlgItemText(IDC_FAST_MFC_STATE, "동작 대기 중");
	}

}

void CMaindialog::Gauge_State_Update()
{
	// MKS 390 Gauge 

	SetDlgItemText(IDC_LLC_VACCUM_GAUGE, g_pGauge_IO->ch_2 + "Torr");
	SetDlgItemText(IDC_MC_VACCUM_GAUGE, g_pGauge_IO->ch_3 + "Torr");
	SetDlgItemText(IDC_STATIC_LLC_VACCUM_GAUGE, g_pGauge_IO->ch_2 + "Torr");
	SetDlgItemText(IDC_STATIC_MC_VACCUM_GAUGE, g_pGauge_IO->ch_3 + "Torr");
	SetDlgItemText(IDC_STATIC_LLC_VACCUM_GAUGE_STATE, g_pGauge_IO->State);
	SetDlgItemText(IDC_STATIC_MC_VACCUM_GAUGE_STATE, g_pGauge_IO->State);

	double LLC_vaccum = g_pGauge_IO->m_dPressure_LLC;
	double MC_vaccum = g_pGauge_IO->m_dPressure_MC;

	double LLC_MC = LLC_vaccum - MC_vaccum;
	double MC_LLC = MC_vaccum - LLC_vaccum;

	CString llc_mc_str;
	CString mc_llc_str;

	llc_mc_str.Format("%f", LLC_MC);
	mc_llc_str.Format("%f", MC_LLC);


}

void CMaindialog::LLC_Tmp_State_Update()
{
	//LLC TMP 
	SetDlgItemText(IDC_STATIC_LLC_TMP_HZ, g_pLLKTmp_IO->StrLLCTmpReceiveDataHz);
	SetDlgItemText(IDC_STATIC_LLC_TMP_ERROR, g_pLLKTmp_IO->StrLLCTmpReceiveDataErrorState);
	SetDlgItemText(IDC_STATIC_LLC_TMP_STATE, g_pLLKTmp_IO->StrLLCTmpReceiveDataState);
}

void CMaindialog::MC_Tmp_State_Update()
{
	//MC TMP
	SetDlgItemText(IDC_STATIC_MC_TMP_HZ, g_pMCTmp_IO->str_mc_tmp_ReceiveDataHz);
	SetDlgItemText(IDC_STATIC_MC_TMP_ERROR, g_pMCTmp_IO->str_mc_tmp_ReceiveDataErrorCode);
	SetDlgItemText(IDC_STATIC_MC_TMP_STATE, g_pMCTmp_IO->str_mc_tmp_ReceiveDataState);
}

void CMaindialog::Mask_Location_Check()
{
	int state = g_pConfig->m_nMaterialLocation;

	switch (state)
	{
	case MTS_POD:
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_FLIPPER))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	case MTS_ROBOT_HAND:
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_FLIPPER))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);

		break;
	case MTS_ROTATOR:
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_FLIPPER))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	case MTS_FLIPPER:
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_FLIPPER))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	case LLC :
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_FLIPPER))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	case VACUUM_ROBOT_HAND :
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_FLIPPER))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[1]);
		break;
	case CHUCK :
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_FLIPPER))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	default:
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_FLIPPER))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	}
	
}

void CMaindialog::Main_Stage_Loading_Position_Check()
{
	//if (g_pIO->Get_Loading_Position_Status()!=FALSE)
	if (g_pNavigationStage->Is_Loading_Positioin() == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LOADING_POSITION))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LOADING_POSITION))->SetIcon(m_LedIcon[0]);
	}
}

//void CMaindialog::OnBnClickedTrgate()
//{
//	double m_mc_Pressure = 0.0;
//	double m_llc_Presure = 0.0;
//	double m_mc_llc_presure = 0.0;
//	double m_llc_mc_presure = 0.0;
//
//	m_mc_Pressure = (g_pGauge_IO->m_dPressure_MC);
//	m_llc_Presure = (g_pGauge_IO->m_dPressure_LLC);
//	m_mc_llc_presure = (m_mc_Pressure - m_llc_Presure);
//	m_llc_mc_presure = (m_llc_Presure - m_mc_Pressure);
//
//	CButton* btn = (CButton*)GetDlgItem(IDC_TRGATE);
//	int nCheck = btn->GetCheck();
//	int nRet = -1;
//
//	if (g_pIO->IO_MODE == MAINT_MODE_ON)
//	{
//
//		if (nCheck)
//		{
//
//			if ((0 < m_mc_llc_presure < 100) && (0 < m_llc_mc_presure < 100))
//			{
//				nRet = g_pIO->WriteOutputDataBit(11, 2, 0);
//				Sleep(10);
//				nRet = g_pIO->WriteOutputDataBit(11, 1, 1);
//
//				m_list_log.InsertString(-1, "MAINT_MODE : TR GATE OPNE");
//				m_list_log.SetCurSel(m_list_log.GetCount() - 1);
//			}
//			else
//			{
//				m_list_log.InsertString(-1, "MAINT_MODE : TR GATE OPNE FAIL -> OPEN 가능 진공 범위가 아님");
//				m_list_log.SetCurSel(m_list_log.GetCount() - 1);
//			}
//		}
//		else
//		{
//			nRet = g_pIO->WriteOutputDataBit(11, 1, 0);
//			Sleep(10);
//			nRet = g_pIO->WriteOutputDataBit(11, 2, 1);
//			m_list_log.InsertString(-1, "MAINT_MODE : TR GATE CLOSE");
//			m_list_log.SetCurSel(m_list_log.GetCount() - 1);
//
//		}
//	}
//	else
//	{
//		if (nCheck)
//		{
//			nRet = g_pIO->Open_TRGateValve();
//			if (nRet)
//			{
//				m_list_log.InsertString(-1, "OPER_MODE : TR GATE OPEN");
//				m_list_log.InsertString(-1, g_pIO->Log_str);
//				m_list_log.SetCurSel(m_list_log.GetCount() - 1);
//			}
//			else
//			{
//				m_list_log.InsertString(-1, "OPER_MODE : TR GATE OPEN FAIL");
//				m_list_log.InsertString(-1, g_pIO->Log_str);
//				m_list_log.SetCurSel(m_list_log.GetCount() - 1);
//			}
//		}
//		else
//		{
//			nRet = g_pIO->Close_TRGateValve();
//			if (nRet)
//			{
//				m_list_log.InsertString(-1, "OPER_MODE : TR GATE CLOSE");
//				m_list_log.InsertString(-1, g_pIO->Log_str);
//				m_list_log.SetCurSel(m_list_log.GetCount() - 1);
//			}
//			else
//			{
//				m_list_log.InsertString(-1, "OPER_MODE : TR GATE CLOSE FAIL");
//				m_list_log.InsertString(-1, g_pIO->Log_str);
//				m_list_log.SetCurSel(m_list_log.GetCount() - 1);
//			}
//		}
//	}
//}

void CMaindialog::OnBnClickedMcPump()
{

	CString str = "MC Pumping 을 실행하시겠습니까?";
	g_pLog->Display(0, str);
	if (AfxMessageBox(str, MB_YESNO) == IDYES)
	{

		g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(false);
		g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(false);
		//g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
		//g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);
		SetTimer(MC_SEQUENCE_CHECK_TIMER, 100, NULL);
		MC_Sequence_Time_Ini();
		g_pVP->MC_Pumping_Start();
	}

	//KillTimer(MC_SEQUENCE_CHECK_TIMER);
}

void CMaindialog::OnBnClickedMcVent()
{
	CString str;
	CString str_XrayTemper;

	if (g_pXrayCamera->m_bConnected)
	{
		double XrayCameraTemperValue = 0.0;
		double VentingReferenceTemperValue = 22.0;
		double VentingSettingTemperValue = 28.0;

		if (g_pConfig->m_nEquipmentType == PHASE)
		{
			str = "Phase Mode 입니다";
			g_pLog->Display(0, str);
		
		}
		else if (g_pConfig->m_nEquipmentType == SREM033)
		{
			str = "SREM Mode 입니다";
			g_pLog->Display(0, str);

			//if (AfxMessageBox(str + "\n" + "XRayCamera가 연결 되어 있습니다" +"\n"+"XRayCamera를 사용하시겠습니까?" + "\n", MB_YESNO) != IDYES)
			//{
			//	str = "XRayCamera 사용 없이, MC Venting을 실행하시겠습니까?";
			//	g_pLog->Display(0, str);
			//	if (AfxMessageBox(str, MB_YESNO) == IDYES)
			//	{
			//		MCVentingStart();
			//	}
			//	return;
			//}
		}

		XrayCameraTemperValue = g_pXrayCamera->GetTemperature();
		str_XrayTemper.Format("%f", XrayCameraTemperValue);
		if (XrayCameraTemperValue > VentingReferenceTemperValue)
		{
			if (AfxMessageBox(str + "\n" + "XrayCamera 온도 " + str_XrayTemper + " ℃ 로 MC Veting 이 가능상태 입니다." + "\n" + "MC Venting 을 실행 하시겠습니까?" + "\n", MB_YESNO) == IDYES)
			{
				MCVentingStart();
			}
			return;
		}
		else
		{
			if (AfxMessageBox(str + "\n" + "XrayCamera 온도 " + str_XrayTemper + "℃ 로 MC Veting 이 불가능상태 입니다." + "\n" + "온도를 Setting 하시겠습니까?" + "\n",  MB_YESNO) == IDYES)
			{
				XrayCameraTempeSet(VentingSettingTemperValue);

				GetDlgItem(IDC_MC_PUMP)->EnableWindow(false);
				GetDlgItem(IDC_MC_VENT)->EnableWindow(false);
				GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
				GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);

				SetTimer(XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING, 100, NULL);

			}
			return;
		}
	}
	else
	{
		str = "XRayCamera 온도가 22 이상 입니까?";
		g_pLog->Display(0, str);
		if (AfxMessageBox(str + "\n" + "(온도 확인 후 Venting 진행 하세요)" + "\n", MB_YESNO) == IDYES)
		{

			str = "MC Venting을 실행하시겠습니까?";
			g_pLog->Display(0, str);
			if (AfxMessageBox(str, MB_YESNO) == IDYES)
			{
				MCVentingStart();
			}
			//KillTimer(MC_SEQUENCE_CHECK_TIMER);
			return;
		}
	}
}

void CMaindialog::MCVentingStart()
{
	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(false);
	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(false);
	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);
	SetTimer(MC_SEQUENCE_CHECK_TIMER, 100, NULL);
	MC_Sequence_Time_Ini();
	LLC_Sequence_Time_Ini();
	g_pVP->MC_Venting_Start();
}

void CMaindialog::XrayCameraTempeSet(double SetPoint)
{
	g_pXrayCamera->SetTemperatureSetPoint(SetPoint);
}

void CMaindialog::XrayCameraGetTempe()
{
	CString str_XrayCameraTemperValue;
	double XrayCameraTemperValue;

	XrayCameraTemperValue = g_pXrayCamera->GetTemperature();
	if (XrayCameraTemperValue < 22.0)
	{
		str_XrayCameraTemperValue.Format("%f", XrayCameraTemperValue);

		g_pWarning->m_strWarningMessageVal = " X_rayCamera 온도 상승 진행 중 입니다 :: " + str_XrayCameraTemperValue + "℃";
		g_pWarning->UpdateData(FALSE);
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pWarning->ShowWindow(SW_HIDE);

		GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
		GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
		GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
		GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
	}

}

void CMaindialog::OnBnClickedMcStop()
{
	KillTimer(MC_SEQUENCE_CHECK_TIMER);
	KillTimer(XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING);

	if (g_pVP->m_p_MC_VaccumThread == NULL)
	{
		::AfxMessageBox("Sequence 가 이미 종료 되었거나, 실행중인 Sequence 가 없습니다", MB_ICONEXCLAMATION);
	}
	else
	{
		g_pVP->m_p_MC_VaccumThread->SuspendThread();
		DWORD dwResult;
		::GetExitCodeThread(g_pVP->m_p_MC_VaccumThread, &dwResult);

		delete g_pVP->m_p_MC_VaccumThread;
		g_pVP->m_p_MC_VaccumThread = NULL;

		g_pVP->m_Mc_Vacuum_State = g_pVP->MC_Pumping_ERROR;
		::AfxMessageBox("실행 중인 Sequence 가 종료 되었습니다",MB_ICONINFORMATION);
	}

	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
}

void CMaindialog::OnBnClickedLlcPump()
{

	CString str = "LLC Pumping 을 실행하시겠습니까?";
	g_pLog->Display(0, str);
	if (AfxMessageBox(str, MB_YESNO) == IDYES)
	{

		//g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(false);
		//g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(false);
		g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
		g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);
		SetTimer(LLC_SEQUENCE_CHECK_TIMER, 100, NULL);
		LLC_Sequence_Time_Ini();
		g_pVP->LLC_Pumping_Start();
	}
	//KillTimer(LLC_SEQUENCE_CHECK_TIMER);
}

void CMaindialog::OnBnClickedLlcVent()
{

	CString str = "LLC Venting 을 실행하시겠습니까?";
	g_pLog->Display(0, str);
	if (AfxMessageBox(str, MB_YESNO) == IDYES)
	{

		//g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(false);
		//g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(false);
		g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
		g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);
		SetTimer(LLC_SEQUENCE_CHECK_TIMER, 100, NULL);
		LLC_Sequence_Time_Ini();
		g_pVP->LLC_Venting_Start();
	}
//	KillTimer(LLC_SEQUENCE_CHECK_TIMER);
}

void CMaindialog::OnBnClickedLlcStop()
{
	KillTimer(LLC_SEQUENCE_CHECK_TIMER);

	if (g_pVP->m_pVaccumThread == NULL)
	{
		::AfxMessageBox("Sequence 가 이미 종료 되었거나, 실행중인 Sequence 가 없습니다", MB_ICONEXCLAMATION);
	}
	else
	{
		g_pVP->m_pVaccumThread->SuspendThread();
		DWORD dwResult;
		::GetExitCodeThread(g_pVP->m_pVaccumThread, &dwResult);

		delete g_pVP->m_pVaccumThread;
		g_pVP->m_pVaccumThread = NULL;

		g_pVP->m_Vacuum_State = g_pVP->Pumping_ERROR;

		::AfxMessageBox("실행 중인 Sequence 가 종료 되었습니다", MB_ICONINFORMATION);
	}
	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
}

void CMaindialog::OnBnClickedMcOn()
{
	CString sTempValue;

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcStandbyVentValue);
	m_dMcStandbyVentValue = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcSlowVentValue);
	m_dMcSlowVentValue = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcFastVentValue);
	m_dMcFastVentValue = atof(sTempValue);


	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcSlowRoughValue);
	m_dMcSlowRoughValue = atof(sTempValue);


	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcFastRoughValue);
	m_dMcFastRoughValue = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcTmpRoughValue);
	m_dMcTmpRoughValue = atof(sTempValue);


	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcStandbyVentTime);
	m_nMcStandbyVentTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcSlowVentTime);
	m_nMcSlowVentTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcFastVentTime);
	m_nMcFastVentTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcSlowRoughTime);
	m_nMcSlowRoughTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcFastRoughTime);
	m_nMcFastRoughTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcTmpRoughTime);
	m_nMcTmpRoughTime = atof(sTempValue);


	g_pConfig->m_dPressure_ChangeToVent = m_dMcStandbyVentValue;
	g_pConfig->m_dPressure_ChangeToSlow_Vent = m_dMcSlowVentValue;
	g_pConfig->m_dPressure_Vent_End = m_dMcFastVentValue;
	g_pConfig->m_dPressure_ChangeToFast_MC_Rough = m_dMcSlowRoughValue;
	g_pConfig->m_dPressure_ChangeToMCTMP_Rough = m_dMcFastRoughValue;
	g_pConfig->m_dPressure_Rough_End = m_dMcTmpRoughValue;
	
	g_pConfig->m_nTimeout_sec_MCLLCVent = m_nMcStandbyVentTime;
	g_pConfig->m_nTimeout_sec_MCSlow1Vent = m_nMcSlowVentTime;
	g_pConfig->m_nTimeout_sec_MCFastVent = m_nMcFastVentTime;
	g_pConfig->m_nTimeout_sec_MCSlowRough = m_nMcSlowRoughTime;
	g_pConfig->m_nTimeout_sec_MCFastRough = m_nMcFastRoughTime;
	g_pConfig->m_nTimeout_sec_MCTmpEnd = m_nMcTmpRoughTime;
	

	g_pConfig->SaveVacuumSeqData();

	
	sTempValue.Format("%.6f", m_dMcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nMcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
}

void CMaindialog::OnBnClickedMcDefault()
{
	CString sTempValue;

	m_dMcStandbyVentValue = g_pConfig->m_dPressure_ChangeToVent			 = g_pConfig->m_dPressure_ChangeToVent_ini;
	m_dMcSlowVentValue	  = g_pConfig->m_dPressure_ChangeToSlow_Vent	 = g_pConfig->m_dPressure_ChangeToSlow_Vent_ini;
	m_dMcFastVentValue    = g_pConfig->m_dPressure_Vent_End				 = g_pConfig->m_dPressure_Vent_End_ini;
	m_dMcSlowRoughValue   = g_pConfig->m_dPressure_ChangeToFast_MC_Rough = g_pConfig->m_dPressure_ChangeToFast_MC_Rough_ini;
	m_dMcFastRoughValue   = g_pConfig->m_dPressure_ChangeToMCTMP_Rough   = g_pConfig->m_dPressure_ChangeToMCTMP_Rough_ini;
	m_dMcTmpRoughValue    = g_pConfig->m_dPressure_Rough_End			 = g_pConfig->m_dPressure_Rough_End_ini;
	 
	m_nMcStandbyVentTime = g_pConfig->m_nTimeout_sec_MCLLCVent	 = g_pConfig->m_nTimeout_sec_MCLLCVent_ini;
	m_nMcSlowVentTime    = g_pConfig->m_nTimeout_sec_MCSlow1Vent = g_pConfig->m_nTimeout_sec_MCSlow1Vent_ini;
	m_nMcFastVentTime    = g_pConfig->m_nTimeout_sec_MCFastVent  = g_pConfig->m_nTimeout_sec_MCFastVent_ini;
	m_nMcSlowRoughTime   = g_pConfig->m_nTimeout_sec_MCSlowRough = g_pConfig->m_nTimeout_sec_MCSlowRough_ini;
	m_nMcFastRoughTime   = g_pConfig->m_nTimeout_sec_MCFastRough = g_pConfig->m_nTimeout_sec_MCFastRough_ini;
	m_nMcTmpRoughTime    = g_pConfig->m_nTimeout_sec_MCTmpEnd    = g_pConfig->m_nTimeout_sec_MCTmpEnd_ini;
	

	sTempValue.Format("%.6f", m_dMcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nMcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);

	g_pConfig->SaveVacuumSeqData();
}

void CMaindialog::OnBnClickedLlcOn()
{
	CString sTempValue;

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC))->GetWindowTextA(sTempValue);
	m_dLlcStandbyVentValue = atof(sTempValue);
	//sscanf_s(sTempValue, "%3.6f", &m_dLlcStandbyVentValue);

	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC))->GetWindowTextA(sTempValue);
	m_dLlcSlowVentValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcSlowVentValue);
	
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC))->GetWindowTextA(sTempValue);
	m_dLlcFastVentValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcFastVentValue);
	
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC))->GetWindowTextA(sTempValue);
	m_dLlcSlowRoughValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcSlowRoughValue);
	
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC))->GetWindowTextA(sTempValue);
	m_dLlcFastRoughValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcFastRoughValue);
	
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC))->GetWindowTextA(sTempValue);
	m_dLlcTmpRoughValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcTmpRoughValue);

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcStandbyVentTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcStandbyVentTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcSlowVentTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcSlowVentTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcFastVentTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcFastVentTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcSlowRoughTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcSlowRoughTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcFastRoughTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcFastRoughTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcTmpRoughTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcTmpRoughTime);

	g_pConfig->m_dPressure_ChangeToVent_LLC = m_dLlcStandbyVentValue;
	g_pConfig->m_dPressure_ChangeToSlow2_Vent = m_dLlcSlowVentValue;
	g_pConfig->m_dPressure_Vent_End = m_dLlcFastVentValue;
	g_pConfig->m_dPressure_ChangeToFast_Rough = m_dLlcSlowRoughValue;
	g_pConfig->m_dPressure_ChangeToTMP_Rough = m_dLlcFastRoughValue;
	g_pConfig->m_dPressure_Rough_End = m_dLlcTmpRoughValue;
	
	g_pConfig->m_nTimeout_sec_LLKStandbyVent = m_nLlcStandbyVentTime;
	g_pConfig->m_nTimeout_sec_LLKSlow1Vent = m_nLlcSlowVentTime;
	g_pConfig->m_nTimeout_sec_LLKFastVent = m_nLlcFastVentTime;
	g_pConfig->m_nTimeout_sec_LLKSlowRough = m_nLlcSlowRoughTime;
	g_pConfig->m_nTimeout_sec_LLKFastRough = m_nLlcFastRoughTime;
	g_pConfig->m_nTimeout_sec_LLKRoughEnd = m_nLlcTmpRoughTime;
	
	g_pConfig->SaveVacuumSeqData();

	sTempValue.Format("%.6f", m_dLlcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nLlcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
}

void CMaindialog::OnBnClickedLlcDefault()
{
	CString sTempValue;

	m_dLlcStandbyVentValue = g_pConfig->m_dPressure_ChangeToVent_LLC   = g_pConfig->m_dPressure_ChangeToVent_LLC_ini;
	m_dLlcSlowVentValue    = g_pConfig->m_dPressure_ChangeToSlow2_Vent = g_pConfig->m_dPressure_ChangeToSlow2_Vent_ini;
	m_dLlcFastVentValue    = g_pConfig->m_dPressure_Vent_End		   = g_pConfig->m_dPressure_Vent_End_ini;
	m_dLlcSlowRoughValue   = g_pConfig->m_dPressure_ChangeToFast_Rough = g_pConfig->m_dPressure_ChangeToFast_Rough_ini;
	m_dLlcFastRoughValue   = g_pConfig->m_dPressure_ChangeToTMP_Rough  = g_pConfig->m_dPressure_ChangeToTMP_Rough_ini;
	m_dLlcTmpRoughValue    = g_pConfig->m_dPressure_Rough_End		   = g_pConfig->m_dPressure_Rough_End_ini;
	
	m_nLlcStandbyVentTime = g_pConfig->m_nTimeout_sec_LLKStandbyVent = g_pConfig->m_nTimeout_sec_LLKStandbyVent_ini;
	m_nLlcSlowVentTime    = g_pConfig->m_nTimeout_sec_LLKSlow1Vent   = g_pConfig->m_nTimeout_sec_LLKSlow1Vent_ini;
	m_nLlcFastVentTime    = g_pConfig->m_nTimeout_sec_LLKFastVent    = g_pConfig->m_nTimeout_sec_LLKFastVent_ini;
	m_nLlcSlowRoughTime   = g_pConfig->m_nTimeout_sec_LLKSlowRough   = g_pConfig->m_nTimeout_sec_LLKSlowRough_ini;
	m_nLlcFastRoughTime   = g_pConfig->m_nTimeout_sec_LLKFastRough   = g_pConfig->m_nTimeout_sec_LLKFastRough_ini;
	m_nLlcTmpRoughTime    = g_pConfig->m_nTimeout_sec_LLKRoughEnd    = g_pConfig->m_nTimeout_sec_LLKRoughEnd_ini;
	

	sTempValue.Format("%.6f", m_dLlcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	
	sTempValue.Format("%d", m_nLlcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);

	g_pConfig->SaveVacuumSeqData();
}

void CMaindialog::OnBnClickedMcAutoSeqStateReset()
{
	if ((g_pVP->m_nMC_seq_state == Pumping_ERROR_State) || (g_pVP->m_nMC_seq_state == Venting_ERROR_State))	
		g_pVP->m_nMC_seq_state = State_IDLE_State;
	else if ((g_pVP->m_nMC_seq_state == Pumping_COMPLETE_State) || (g_pVP->m_nMC_seq_state == Venting_COMPLETE_State)) 
		g_pVP->m_nMC_seq_state = State_IDLE_State;
}

void CMaindialog::OnBnClickedLlcAutoSeqStateReset()
{
	if ((g_pVP->m_nLLC_seq_state == Pumping_ERROR_State) || (g_pVP->m_nLLC_seq_state == Venting_ERROR_State)) 
		g_pVP->m_nLLC_seq_state = State_IDLE_State;
	else if ((g_pVP->m_nLLC_seq_state == Pumping_COMPLETE_State) || (g_pVP->m_nLLC_seq_state == Venting_COMPLETE_State)) 
		g_pVP->m_nLLC_seq_state = State_IDLE_State;
}

void CMaindialog::OnBnClickedLlcTmpOn()
{
	CString str;

	if (g_pLLKTmp_IO->LLC_Tmp_On() != RUN)
	{
		::AfxMessageBox("LLC TMP ON ERROR");
		str = "LLC TMP ON 실패 하였습니다.";
		g_pLog->Display(0, str);
	}
	else
	{
		str = "LLC TMP ON 되었습니다.";
		g_pLog->Display(0, str);

	}
}

void CMaindialog::OnBnClickedLlcTmpOff()
{
	CString str;

	if (g_pLLKTmp_IO->LLC_Tmp_Off() != RUN_OFF)
	{
		::AfxMessageBox("LLC TMP OFF ERROR");
		str = "LLC TMP OFF 실패 하였습니다.";
		g_pLog->Display(0, str);
	}
	else
	{
		str = "LLC TMP OFF 되었습니다.";
		g_pLog->Display(0, str);

	}
}

void CMaindialog::OnBnClickedMcTmpOn()
{
	CString str;

	if (g_pMCTmp_IO->Mc_Tmp_On() != RUN)
	{
		::AfxMessageBox("MC TMP ON ERROR");
		str = "MC TMP ON 실패 하였습니다.";
		g_pLog->Display(0, str);
	}
	else
	{
		str = "MC TMP ON 되었습니다.";
		g_pLog->Display(0, str);

	}
}

void CMaindialog::OnBnClickedMcTmpOff()
{
	CString str;

	if (g_pMCTmp_IO->Mc_Tmp_Off() != RUN_OFF)
	{
		::AfxMessageBox("MC TMP OFF ERROR");
		str = "MC TMP OFF 실패 하였습니다.";
		g_pLog->Display(0, str);

	}
	else
	{
		str = "MC TMP OFF 되었습니다.";
		g_pLog->Display(0, str);

	}
}

HBRUSH CMaindialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (pWnd->GetDlgCtrlID() == IDC_STATIC_LLC_VACCUM_GAUGE)
		{
			pDC->SetBkColor(RGB(0, 0, 50));  
			pDC->SetTextColor(RGB(0, 255, 0)); 
			return m_brush;
		}
		else if (pWnd->GetDlgCtrlID() == IDC_STATIC_MC_VACCUM_GAUGE)
		{
			pDC->SetBkColor(RGB(0, 0, 50));  
			pDC->SetTextColor(RGB(0, 255, 0));  
			return m_brush;
		}
		else if ((pWnd->GetDlgCtrlID() == IDC_MC_VACCUM_GAUGE) 
			|| (pWnd->GetDlgCtrlID() == IDC_LLC_VACCUM_GAUGE)
			|| (pWnd->GetDlgCtrlID() == IDC_MC_LINE_STATE_1) 
			|| (pWnd->GetDlgCtrlID() == IDC_MC_LINE_STATE_2) 
			|| (pWnd->GetDlgCtrlID() == IDC_LLC_LINE_STATE_1) 
			|| (pWnd->GetDlgCtrlID() == IDC_LLC_LINE_STATE_2) 
			|| (pWnd->GetDlgCtrlID() == IDC_SLOW_MFC_1) 
			|| (pWnd->GetDlgCtrlID() == IDC_SLOW_MFC_2)
			|| (pWnd->GetDlgCtrlID() == IDC_FAST_MFC_1) 
			|| (pWnd->GetDlgCtrlID() == IDC_FAST_MFC_2))
		{
			pDC->SetBkColor(RGB(255, 234, 234));
			pDC->SetTextColor(RGB(0, 0, 0));
			return m_main_dialog_brush;
		}
	
	}
	return hbr;
}

void CMaindialog::OnBnClickedVaccumGaugeWriteStart()
{
	if (g_pGauge_IO->m_bOpenPort_State == TRUE)
	{
		//	SetTimer(VACCUM_GAUGE_WRITE_TIMER, 30000, NULL);
		((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[1]);  // VACCUM GAGUE WRITE ON / OFF
		m_pVacuumWriteStop = FALSE;
		if (m_pVacuumWriteThread == NULL)
		{
			m_pVacuumWriteThread = ::AfxBeginThread(VacuumWrite_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
		}


	}
	else
	{
		AfxMessageBox(_T("Gauge 가 연결되지 않았습니다"));
		((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[0]);  // VACCUM GAGUE WRITE ON / OFF
	}


	
	

	
	//
	//CString path = LOG_RUN_PATH;
	//
	//if (!g_pLog->IsFolderExist(path))
	//{
	//	CreateDirectory(path, NULL);
	//}
	//
	//if (g_pGauge_IO->OpenPort_State == TRUE)
	//{
	//	mon = CTime::GetCurrentTime().GetMonth();
	//	day = CTime::GetCurrentTime().GetDay();
	//	hour = CTime::GetCurrentTime().GetHour();
	//	min = CTime::GetCurrentTime().GetMinute();
	//	sec = CTime::GetCurrentTime().GetSecond();
	//	
	//
	//	vacuum_data_file.Format(_T("%s\\Vacuum__data__%d_%d_%d_%d_start.txt"), path, mon, day, hour, min);
	//
	//	vacuum_data_write_file << "mon" << "\t" << "day" << "\t" << "hour" << "\t" << "min" << "\t" << "sec" << "\t" << "MC Vacuum Value" << "\t" << "LLC Vacuum Value" << "\n";
	//
	//	SetTimer(VACCUM_GAUGE_WRITE_TIMER, 30000, NULL);
	//	((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[1]);  // VACCUM GAGUE WRITE ON / OFF
	//}
	//else
	//{
	//	AfxMessageBox(_T("Gauge 가 연결되지 않았습니다"));
	//	((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[0]);  // VACCUM GAGUE WRITE ON / OFF
	//}
}

UINT CMaindialog::VacuumWrite_Thread(LPVOID pParam)
{
	int ret = 0;


	CMaindialog*  g_pVaccumWrite = (CMaindialog*)pParam;

	g_pVaccumWrite->m_pVacuumWriteStop = FALSE;
	while (!g_pVaccumWrite->m_pVacuumWriteStop)
	{
		g_pVaccumWrite->Vacuum_Gague_Write();
	}

	return ret;
}

void CMaindialog::OnBnClickedVaccumGaugeWriteStop()
{
	//KillTimer(VACCUM_GAUGE_WRITE_TIMER);
	m_pVacuumWriteStop = TRUE;

	((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[0]);  // VACCUM GAGUE WRITE ON / OFF
	m_pVacuumWriteThread = NULL;
}

void CMaindialog::Vacuum_Gague_Write()
{
	int cnt = 0;
	int cont = 1;
	CString path = LOG_RUN_PATH;
	CString strThisPath;
	CString Date;
	CString Time;

	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
		GetCurrentDirectory(10, LOG_RUN_PATH);
		UpdateData(TRUE);
	}
	

	CTyear = CTime::GetCurrentTime().GetYear();
	CTmon = CTime::GetCurrentTime().GetMonth();
	CTday = CTime::GetCurrentTime().GetDay();
	CThour = CTime::GetCurrentTime().GetHour();
	CTmin = CTime::GetCurrentTime().GetMinute();
	CTsec = CTime::GetCurrentTime().GetSecond();

	if (g_pGauge_IO->m_bOpenPort_State == TRUE)
	{
		vacuum_data_file.Format(_T("%s\\Vacuum__data__%d_%d_%d_%d_start.txt"), path, CTmon, CTday, CThour, CTmin);
		
	
		///////////////////////////////
		//**************Excel
		///////////////////////////////
		//strThisPath.Format("%s\\Vacuum__Write_Loggin_Data_%d_%d_%d_%d_start.xlsx", path, CTmon, CTday, CThour, CTmin);
		//CXLEzAutomation XL(FALSE);
		//XL.SetCellValue(1, 1, "날짜");
		//XL.SetCellValue(2, 1, "시간");
		//XL.SetCellValue(3, 1, "MC_Vacuum_Value");
		//XL.SetCellValue(4, 1, "LLC_Vaccum_Vale");
		
		std::ofstream vacuum_data_write_file(vacuum_data_file);
		vacuum_data_write_file << std::setprecision(3) << "Date" << "\t" << "Time" << "\t" << "MC Vacuum Value" << "\t" << "LLC Vacuum Value" << "\n" ;

		((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[1]);  // VACCUM GAGUE WRITE ON / OFF
		while (m_pVacuumWriteStop != TRUE)
		{
			cnt++;
			CTmon = CTime::GetCurrentTime().GetMonth();
			CTday = CTime::GetCurrentTime().GetDay();
			CThour = CTime::GetCurrentTime().GetHour();
			CTmin = CTime::GetCurrentTime().GetMinute();
			CTsec = CTime::GetCurrentTime().GetSecond();

			if (cnt == 30)
			{
				//vacuum_data_write_file << std::setprecision(3) << year << "." << mon << "." << day << "." << hour << ":" << min << ":" << sec << "\t" << g_pGauge_IO->ch_2 << "\t" << g_pGauge_IO->ch_3 << "\n";
				vacuum_data_write_file << std::setprecision(3) << CTyear << "." << CTmon << "." << CTday << "\t" << CThour << ":" << CTmin << ":" << CTsec << "\t" << g_pGauge_IO->ch_3 << "\t" << g_pGauge_IO->ch_2 << "\n";

				///////////////////////////////
				//**************Excel
				///////////////////////////////
				//Date.Format(_T("%d.%d.%d", CTyear, CTmon, CTday));
				//Time.Format(_T("%d.%d.%d", CThour, CTmin, CTsec));
				//
				//XL.SetCellValue(1, cont, Date);
				//XL.SetCellValue(1, cont, Time);
				//XL.SetCellValue(4, cont, g_pGauge_IO->ch_3);
				//XL.SetCellValue(7, cont, g_pGauge_IO->ch_2);
				cnt = 0;
				cont++;
			}
			WaitSec(1);
		}
		//SaveLogFile("[LLC&MC]_Vaccum_Gauge_Value_", _T((LPSTR)(LPCTSTR)(" MC Vaccum Value : " + g_pGauge_IO->ch_2 + ",   LLC Vaccum Value : " + g_pGauge_IO->ch_3 )));
	
		cont = 1;
		cnt = 0;

		m_pVacuumWriteStop = TRUE;
		((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[0]);  // VACCUM GAGUE WRITE ON / OFF
		vacuum_data_write_file << std::endl;
		if (vacuum_data_write_file.is_open() == true)
		{
			vacuum_data_write_file.close();
		}
		///////////////////////////////
		//**************Excel
		///////////////////////////////
		//XL.SaveFileAs(strThisPath);
		//XL.ReleaseExcel();

		m_pVacuumWriteThread = NULL;
	
	}
	

	m_pVacuumWriteStop = TRUE;
	((CStatic*)GetDlgItem(IDC_ICON_VACCUM_GAUGE))->SetIcon(m_LedIcon[0]);  // VACCUM GAGUE WRITE ON / OFF
	m_pVacuumWriteThread = NULL;

		//KillTimer(VACCUM_GAUGE_WRITE_TIMER);


}

void CMaindialog::OnBnClickedStageLogWriteStart()
{
	if (g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		((CStatic*)GetDlgItem(IDC_ICON_STAGE_LOG_ICON))->SetIcon(m_LedIcon[1]);
		g_pChartstage->OnBnClickedBtnLongrun();
	}
	else
	{
		AfxMessageBox(_T("Stage 가 연결되지 않았습니다"));
		((CStatic*)GetDlgItem(IDC_ICON_STAGE_LOG_ICON))->SetIcon(m_LedIcon[0]);
	}


	///////////////////
	//TEST BENCH
	//////////////////
	//g_pChartstage->OnBnClickedBtnLongrun();
	//((CStatic*)GetDlgItem(IDC_ICON_STAGE_LOG_ICON))->SetIcon(m_LedIcon[1]);
}

void CMaindialog::OnBnClickedStageLogWriteStop()
{
	g_pChartstage->OnBnClickedBtnStop();
	((CStatic*)GetDlgItem(IDC_ICON_STAGE_LOG_ICON))->SetIcon(m_LedIcon[0]);
}

void CMaindialog::MC_Sequence_factory_initialization_Value_Time()
{

	// Initial Value (공정값으로 초기화)
	g_pConfig->m_dPressure_ChangeToFast_MC_Rough_ini = 50.0;	// MC Slow rough -> Fast Rough 범위 (50 Torr 까지 Slow Rough 진행)
	g_pConfig->m_dPressure_ChangeToMCTMP_Rough_ini = 0.035;		// MC Fast rough -> Tmp Rough 범위 (0.035 Torr 까지 Fast Rough 진행)
	g_pConfig->m_dPressure_Rough_End_ini = 0.000009;				// Tmp Rough -> Pumping 완료 범위 (0.000009 Torr 까지 Fast Rough 진행)
	g_pConfig->m_dPressure_ChangeToVent_ini = 0.003;			// 대기 Venting -> SlowVent 범위 (0.003 Torr 부터 Slow Vent 진행)
	g_pConfig->m_dPressure_ChangeToSlow_Vent_ini = 0.1;			// Slow Venting  ( 0.1 까지 Slow Vent 진행)	
	g_pConfig->m_dPressure_Vent_End_ini = 760.0;				// Fast Venting  (760 Torr 까지 Fast Vent 진행)
	// Initial Time (공정값으로 초기화)
	g_pConfig->m_nTimeout_sec_MCSlowRough_ini = 600;			// MC Slow rough -> 600 초 10분 동안 진행
	g_pConfig->m_nTimeout_sec_MCFastRough_ini = 600;			// MC Fast rough -> 600 초 10분 동안 진행
	g_pConfig->m_nTimeout_sec_MCTmpEnd_ini = 3600;				// MC Tmp rough -> 3600 초 60분 동안 진행
	g_pConfig->m_nTimeout_sec_MCLLCVent_ini = 1800;				// MC Vent 시 TMP Gate Close 후 대기 venting 시간.(30분)
	g_pConfig->m_nTimeout_sec_MCSlow1Vent_ini = 1800;			// MC Slow Vent -> 1800 초 30분 동안 진행
	g_pConfig->m_nTimeout_sec_MCFastVent_ini = 1800;			// MC Fast Vent -> 1800 초 30분 동안 진행

}
//void CMaindialog::OnBnClickedMcInitial()
//{
//	//
//	// Pumping 
//	//
//
//	CString m_slow_rough;	// MC Pumping Slow Rough 시작 범위 값
//	CString m_fast_rough;	// MC Pumping Fast Rough 시작 범위 값
//	CString m_tmp_rough;	// MC Pumping Tmp Rough 시작 범위 값
//	CString m_mc_vent;		// MC Venting 대기 벤팅 진행 범위 값 ( 대기 벤팅의 마지막 지점 값 지정 )
//	CString m_slow_vent;	// MC Venting Slow Vent 진행 범위 값
//	CString m_mc_fast_vent;	// MC Vetning Fast Vent 진행 범위 값
//	CString strSector, strKey;
//
//	//Time
//	CString mc_slow_rough_time;
//	CString mc_fast_rough_time;
//	CString mc_tmp_rough_time;
//	CString mc_vent_time;
//	CString mc_slow_vent_time;
//	CString mc_fast_vent_time;
//
//	MC_Sequence_factory_initialization_Value_Time();
//
//	g_pConfig->m_dPressure_ChangeToFast_MC_Rough = g_pConfig->m_dPressure_ChangeToFast_MC_Rough_ini;
//	g_pConfig->m_dPressure_ChangeToMCTMP_Rough = g_pConfig->m_dPressure_ChangeToMCTMP_Rough_ini;
//	g_pConfig->m_dPressure_Rough_End = g_pConfig->m_dPressure_Rough_End_ini;
//	g_pConfig->m_dPressure_ChangeToVent = g_pConfig->m_dPressure_ChangeToVent_ini;
//	g_pConfig->m_dPressure_ChangeToSlow_Vent = g_pConfig->m_dPressure_ChangeToSlow_Vent_ini;
//	g_pConfig->m_dPressure_Vent_End = g_pConfig->m_dPressure_Vent_End_ini;
//
//
//	g_pConfig->m_nTimeout_sec_MCSlowRough = g_pConfig->m_nTimeout_sec_MCSlowRough_ini;
//	g_pConfig->m_nTimeout_sec_MCFastRough = g_pConfig->m_nTimeout_sec_MCFastRough_ini;
//	g_pConfig->m_nTimeout_sec_MCTmpEnd = g_pConfig->m_nTimeout_sec_MCTmpEnd_ini;
//	g_pConfig->m_nTimeout_sec_MCLLCVent = g_pConfig->m_nTimeout_sec_MCLLCVent_ini;
//	g_pConfig->m_nTimeout_sec_MCSlow1Vent = g_pConfig->m_nTimeout_sec_MCSlow1Vent_ini;
//	g_pConfig->m_nTimeout_sec_MCFastVent = g_pConfig->m_nTimeout_sec_MCFastVent_ini;
//
//	m_slow_rough.Format("%f", g_pConfig->m_dPressure_ChangeToFast_MC_Rough);
//	m_fast_rough.Format("%f", g_pConfig->m_dPressure_ChangeToMCTMP_Rough);
//	m_tmp_rough.Format("%f", g_pConfig->m_dPressure_Rough_End);
//	m_mc_vent.Format("%f", g_pConfig->m_dPressure_ChangeToVent);
//	m_slow_vent.Format("%f", g_pConfig->m_dPressure_ChangeToSlow_Vent);
//	m_mc_fast_vent.Format("%f", g_pConfig->m_dPressure_Vent_End);
//
//	mc_slow_rough_time.Format("%d", g_pConfig->m_nTimeout_sec_MCSlowRough);
//	mc_fast_rough_time.Format("%d", g_pConfig->m_nTimeout_sec_MCFastRough);
//	mc_tmp_rough_time.Format("%d", g_pConfig->m_nTimeout_sec_MCTmpEnd);
//	mc_vent_time.Format("%d", g_pConfig->m_nTimeout_sec_MCLLCVent);
//	mc_slow_vent_time.Format("%d", g_pConfig->m_nTimeout_sec_MCSlow1Vent);
//	mc_fast_vent_time.Format("%d", g_pConfig->m_nTimeout_sec_MCFastVent);
//
//	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC))->SetWindowTextA(m_slow_rough);
//	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC))->SetWindowTextA(m_fast_rough);
//	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC))->SetWindowTextA(m_tmp_rough);
//	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC))->SetWindowTextA(m_mc_vent);
//	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC))->SetWindowTextA(m_slow_vent);
//	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC))->SetWindowTextA(m_mc_fast_vent);
//
//	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW))->SetWindowTextA(m_slow_rough);
//	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW))->SetWindowTextA(m_fast_rough);
//	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW))->SetWindowTextA(m_tmp_rough);
//	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW))->SetWindowTextA(m_mc_vent);
//	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW))->SetWindowTextA(m_slow_vent);
//	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW))->SetWindowTextA(m_mc_fast_vent);
//
//	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_TIME))->SetWindowTextA(mc_slow_rough_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_TIME))->SetWindowTextA(mc_fast_rough_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_TIME))->SetWindowTextA(mc_tmp_rough_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_TIME))->SetWindowTextA(mc_vent_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_TIME))->SetWindowTextA(mc_slow_vent_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_TIME))->SetWindowTextA(mc_fast_vent_time);
//
//	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME))->SetWindowTextA(mc_slow_rough_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME))->SetWindowTextA(mc_fast_rough_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME))->SetWindowTextA(mc_tmp_rough_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME))->SetWindowTextA(mc_vent_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME))->SetWindowTextA(mc_slow_vent_time);
//	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME))->SetWindowTextA(mc_fast_vent_time);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_StandBy_Vent_Value"));
//	WritePrivateProfileString(strSector, strKey, m_mc_vent, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Slow_Vent_Value"));
//	WritePrivateProfileString(strSector, strKey, m_slow_vent, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Fast_Vent_Value"));
//	WritePrivateProfileString(strSector, strKey, m_mc_fast_vent, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Slow_Rough_Value"));
//	WritePrivateProfileString(strSector, strKey, m_slow_rough, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Fast_Rough_Value"));
//	WritePrivateProfileString(strSector, strKey, m_fast_rough, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Tmp_Rough_Value"));
//	WritePrivateProfileString(strSector, strKey, m_tmp_rough, CONFIG_FILE_FULLPATH);
//
//	//Time
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Slow_Rough_Time"));
//	WritePrivateProfileString(strSector, strKey, mc_slow_rough_time, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Fast_Rough_Time"));
//	WritePrivateProfileString(strSector, strKey, mc_fast_rough_time, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Tmp_Rough_Time"));
//	WritePrivateProfileString(strSector, strKey, mc_tmp_rough_time, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_StandBy_Vent_Time"));
//	WritePrivateProfileString(strSector, strKey, mc_vent_time, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Slow_Vent_Time"));
//	WritePrivateProfileString(strSector, strKey, mc_slow_vent_time, CONFIG_FILE_FULLPATH);
//
//	strSector.Format(_T("VACUUM_SEQUENCE_INFO"));
//	strKey.Format(_T("Mc_Fast_Vent_Time"));
//	WritePrivateProfileString(strSector, strKey, mc_fast_vent_time, CONFIG_FILE_FULLPATH);
//}


void CMaindialog::MC_Sequence_Time_Check()
{
	CString mc_slow_rough_time_cnt_str, mc_fast_rough_time_cnt_str, mc_tmp_rough_time_cnt_str;
	CString mc_standby_vent_time_cnt_str, mc_slow_vent_time_cnt_str, mc_fast_vent_time_cnt_str;
	CString llc_slow_rough_time_cnt_str, llc_fast_rough_time_cnt_str, llc_tmp_rough_time_cnt_str;
	CString llc_standby_vent_time_cnt_str, llc_slow_vent_time_cnt_str, llc_fast_vent_time_cnt_str;


	mc_slow_rough_time_cnt_str.Empty();
	mc_fast_rough_time_cnt_str.Empty();
	mc_tmp_rough_time_cnt_str.Empty();
	mc_standby_vent_time_cnt_str.Empty();
	mc_slow_vent_time_cnt_str.Empty();
	mc_fast_vent_time_cnt_str.Empty();
	llc_slow_rough_time_cnt_str.Empty();
	llc_fast_rough_time_cnt_str.Empty();
	llc_tmp_rough_time_cnt_str.Empty();
	llc_standby_vent_time_cnt_str.Empty();
	llc_slow_vent_time_cnt_str.Empty();
	llc_fast_vent_time_cnt_str.Empty();

	mc_slow_rough_time_cnt_str.Format("%d", g_pVP->m_nMc_slow_rough_time_cnt);
	mc_fast_rough_time_cnt_str.Format("%d", g_pVP->m_nMc_fast_rough_time_cnt);
	mc_tmp_rough_time_cnt_str.Format("%d", g_pVP->m_nMc_tmp_rough_time_cnt);
	mc_standby_vent_time_cnt_str.Format("%d", g_pVP->m_nMc_standby_vent_time_cnt);
	mc_slow_vent_time_cnt_str.Format("%d", g_pVP->m_nMc_slow_vent_time_cnt);
	mc_fast_vent_time_cnt_str.Format("%d", g_pVP->m_nMc_fast_vent_time_cnt);
	llc_slow_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_slow_rough_time_cnt);
	llc_fast_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_fast_rough_time_cnt);
	llc_tmp_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_tmp_rough_time_cnt);
	llc_standby_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_standby_vent_time_cnt);
	llc_slow_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_slow_vent_time_cnt);
	llc_fast_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_fast_vent_time_cnt);


	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(mc_slow_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(mc_fast_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(mc_tmp_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME2))->SetWindowTextA(mc_standby_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME2))->SetWindowTextA(mc_slow_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME2))->SetWindowTextA(mc_fast_vent_time_cnt_str);

	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_slow_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_fast_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_tmp_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_standby_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_slow_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_fast_vent_time_cnt_str);

}
void CMaindialog::LLC_Sequence_Time_Check()
{
	CString llc_slow_rough_time_cnt_str, llc_fast_rough_time_cnt_str, llc_tmp_rough_time_cnt_str;
	CString llc_standby_vent_time_cnt_str, llc_slow_vent_time_cnt_str, llc_fast_vent_time_cnt_str;


	llc_slow_rough_time_cnt_str.Empty();
	llc_fast_rough_time_cnt_str.Empty();
	llc_tmp_rough_time_cnt_str.Empty();
	llc_standby_vent_time_cnt_str.Empty();
	llc_slow_vent_time_cnt_str.Empty();
	llc_fast_vent_time_cnt_str.Empty();

	llc_slow_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_slow_rough_time_cnt);
	llc_fast_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_fast_rough_time_cnt);
	llc_tmp_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_tmp_rough_time_cnt);
	llc_standby_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_standby_vent_time_cnt);
	llc_slow_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_slow_vent_time_cnt);
	llc_fast_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_fast_vent_time_cnt);



	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_slow_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_fast_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_tmp_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_standby_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_slow_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_fast_vent_time_cnt_str);
}
void CMaindialog::MC_Sequence_Time_Ini()
{
	CString ini;
	ini.Empty();
	ini = _T("0");

	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);

}

void CMaindialog::LLC_Sequence_Time_Ini()
{
	CString ini;
	ini.Empty();
	ini = _T("0");

	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
}

//void CMaindialog::OnPaint()
//{
////	CPaintDC dc(this); // device context for painting
//					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
//					   // 그리기 메시지에 대해서는 __super::OnPaint()을(를) 호출하지 마십시오.
//	if (IsIconic()) {
//		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.
//		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);
//		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
//		int cxIcon = GetSystemMetrics(SM_CXICON);
//		int cyIcon = GetSystemMetrics(SM_CYICON);
//		CRect rect;
//		GetClientRect(&rect);
//		int x = (rect.Width() - cxIcon + 1) / 2;
//		int y = (rect.Height() - cyIcon + 1) / 2;
//		// 아이콘을 그립니다.
//		dc.DrawIcon(x, y, m_hIcon);
//
//		if (m_image.IsNull()) {  // 이미 그림이 읽혀졌는지 체크한다.
//			m_image.Load("main.bmp"); // 파일을 읽어서 비트맵 객체를 구성한다.
//			((CStatic *)GetDlgItem(IDC_SREM_MAIN_BITMAP))->SetBitmap(m_image);
//		}
//	}
//	else {
//		CDialogEx::OnPaint();
//	}
//}


void CMaindialog::OnStnClickedIconTrgate()
{
	// TRGATE OPEN / CLOSE

	//if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED)
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[4] == true) && (g_pIO->m_bDigitalIn[5] == false)) // TR  GATE OPEN
		{
			if (AfxMessageBox("TR Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_TRGateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("TR Gate Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("TR Gate Close 완료");
				}

			}
		}
		//else if (g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED)
		else if ((g_pIO->m_bDigitalIn[4] == false) && (g_pIO->m_bDigitalIn[5] == true)) // TR GATE CLOSE
		{
			if (AfxMessageBox("TR Gate Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_TRGateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("TR Gate Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("TR Gate Open 완료");
				}

			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}

}

void CMaindialog::OnStnClickedIconLlcgate()
{
	// LLCGATE OPEN / CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[2] == true) && (g_pIO->m_bDigitalIn[3] == false)) // LLC GATE OPEN
		{
			if (AfxMessageBox("LLC Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_LLCGateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Gate Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Gate Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[2] == false) && (g_pIO->m_bDigitalIn[3] == true)) // LLC GATE CLOSE
		{
			if (AfxMessageBox("LLC Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_LLCGateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Gate Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Gate Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconLlcTmpGate()
{
	// LLC TMP GATE OPEN / CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[10] == true) && (g_pIO->m_bDigitalIn[11] == false)) // LLC TMP GATE OPEN
		{
			if (AfxMessageBox("LLC Tmp Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Close_LLC_TMP_GateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Tmp Gate Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Tmp Gate Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[10] == false) && (g_pIO->m_bDigitalIn[11] == true)) // LLC TMP GATE CLOSE
		{
			if (AfxMessageBox("LLC Tmp Gate Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Open_LLC_TMP_GateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Tmp Gate Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Tmp Gate Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconMcTmpGate()
{
	// MC TMP GATE OPEN / CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[12] == true) && (g_pIO->m_bDigitalIn[13] == false))// MC TMP GATE OPEN
		{
			if (AfxMessageBox("MC Tmp Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Close_MC_TMP1_GateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Tmp Gate Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Tmp Gate Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[12] == false) && (g_pIO->m_bDigitalIn[13] == true))// MC TMP GATE CLOSE
		{
			if (AfxMessageBox("MC Tmp Gate Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_MC_TMP1_GateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Tmp Gate Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Tmp Gate Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconMcRoughValve() 
{
	// MC SLOW ROUGH VALVE OPEN / CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if (g_pIO->m_bDigitalOut[11] == true) // MC SLOW ROUGH OPEN
		{
			if (AfxMessageBox("MC Slow Rough Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Close_MC_SlowRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Slow Rough Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Slow Rough Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalOut[11] == false) // MC SLOW ROUGH CLOSE
		{
			if (AfxMessageBox("MC Slow Rough Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Open_MC_SlowRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Slow Rough Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Slow Rough Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconMcFastRough()
{
	// MC FAST ROUGH VALVE OPEN / CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[104] == true) && (g_pIO->m_bDigitalIn[105] == false)) // MC FAST ROUGH OPEN
		{
			if (AfxMessageBox("MC Fast Rough Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Close_MC_FastRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Fast Rough Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Fast Rough Valve Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[104] == false) && (g_pIO->m_bDigitalIn[105] == true)) // MC FAST ROUGH CLOSE
		{
			if (AfxMessageBox("MC Fast Rough Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Open_MC_FastRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Fast Rough Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Fast Rough Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconMcForeline()
{
	// MC FORELINE VALVE OPEN/ CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[108] == true) && (g_pIO->m_bDigitalIn[109] == false)) // MC FORELINE GATE OPEN
		{
			if (AfxMessageBox("MC Tmp Foreline Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_MC_TMP1_ForelineValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Tmp Foreline Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Tmp Foreline Valve Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[108] == false) && (g_pIO->m_bDigitalIn[109] == true))  // MC FORELINE GATE CLOSE
		{
			if (AfxMessageBox("MC Tmp Foreline Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_MC_TMP1_ForelineValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Tmp Foreline Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Tmp Foreline Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconLlcForeline()
{
	// LLC FORELINE VAVLE OPEN / CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[106] == true) && (g_pIO->m_bDigitalIn[107] == false)) // LLC FORELINE OPEN
		{
			if (AfxMessageBox("LLC Tmp Foreline Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_LLC_TMP_ForelineValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Tmp Foreline Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Tmp Foreline Valve Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[106] == false) && (g_pIO->m_bDigitalIn[107] == true))// LLC FORELINE CLOSE
		{
			if (AfxMessageBox("LLC Tmp Foreline Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_LLC_TMP_ForelineValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Tmp Foreline Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Tmp Foreline Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconLlcRoughValve()
{
	// LLC SLOW ROUGH VALVE OPEN / CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if (g_pIO->m_bDigitalOut[7] == true)// LLC SLOW ROUGH OPEN
		{
			if (AfxMessageBox("LLC Slow Rough Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_LLC_SlowRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Slow Rough Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Slow Rough Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalOut[7] == false)// LLC SLOW ROUGH CLOSE
		{
			if (AfxMessageBox("LLC Slow Rough Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_LLC_SlowRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Slow Rough Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Slow Rough Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconLlcFastRough()
{
	// LLC FAST ROUGH VALVE OPEN / CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if (g_pIO->m_bDigitalIn[101] == true)// LLC FAST ROUGH OPEN
		{
			if (AfxMessageBox("LLC Fast Rough Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_LLC_FastRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Fast Rough Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Fast Rough Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalIn[102] == true)// LLC FAST ROUGH CLOSE
		{
			if (AfxMessageBox("LLC Fast Rough Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_LLC_FastRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Fast Rough Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Fast Rough Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}



void CMaindialog::OnStnClickedIconSlowMfcIn()
{
	// SLOW VENT INLET
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if (g_pIO->m_bDigitalOut[10] == true)// MFC SLOW INLET OPEN
		{
			if (AfxMessageBox("MFC Slow Vent Inlet Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_SlowVent_Inlet_Valve1() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Slow Vent Inlet Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Slow Vent Inlet Valve Close 완료");
				}
			}
		}
		if (g_pIO->m_bDigitalOut[10] == false)// MFC SLOW INLET CLOSE
		{
			if (AfxMessageBox("MFC Slow Vent Inlet Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_SlowVent_Inlet_Valve1() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Slow Vent Inlet Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Slow Vent Inlet Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}


void CMaindialog::OnStnClickedIconSlowMfcOut()
{
	// SLOW VENT OUTLET
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if (g_pIO->m_bDigitalOut[20] == true)// MFC SLOW OUTLET OPEN
		{
			if (AfxMessageBox("MFC Slow Vent Outlet Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_SlowVent_Outlet_Valve1() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Slow Vent Outlet Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Slow Vent Outlet Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalOut[20] == false)// MFC SLOW OUTLET CLOSE
		{
			if (AfxMessageBox("MFC Slow Vent Outlet Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_SlowVent_Outlet_Valve1() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Slow Vent Outlet Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Slow Vent Outlet Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}


void CMaindialog::OnStnClickedIconFastMfcIn()
{
	// FAST VENT INLET
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if (g_pIO->m_bDigitalOut[10] == true)// MFC SLOW INLET OPEN
		{
			if (AfxMessageBox("MFC Fast Vent Inlet Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_FastVent_Inlet_Valve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Fast Vent Inlet Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Fast Vent Inlet Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalOut[10] == false)// MFC SLOW INLET CLOSE
		{
			if (AfxMessageBox("MFC Fast Vent Inlet Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_FastVent_Inlet_Valve1() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Fast Vent Inlet Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Fast Vent Inlet Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}


void CMaindialog::OnStnClickedIconFastMfcOut()
{
	// FAST VENT OUTLET
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if (g_pIO->m_bDigitalOut[19] == true)// MFC FAST OUTLET OPEN
		{
			if (AfxMessageBox("MFC Fast Vent Outlet Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_FastVent_Outlet_Valve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Fast Vent Outlet Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Fast Vent Outlet Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalOut[19] == false)// MFC FAST OUTLET CLOSE
		{
			if (AfxMessageBox("MFC Fast Vent Inlet Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES, MB_ICONQUESTION)
			{
				if (g_pIO->Open_FastVent_Outlet_Valve1() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Fast Vent Inlet Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Fast Vent Inlet Valve Open 완료", MB_ICONINFORMATION);
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}
