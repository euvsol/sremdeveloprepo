﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CVacuumGaugeDlg 대화 상자

IMPLEMENT_DYNAMIC(CVacuumGaugeDlg, CDialogEx)

CVacuumGaugeDlg::CVacuumGaugeDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_VACUUM_GAUGE_DIALOG, pParent)
{
	m_Thread = NULL;
}

CVacuumGaugeDlg::~CVacuumGaugeDlg()
{

	ThreadExitFlag = TRUE;

	if (m_Thread != NULL)
	{
		HANDLE threadHandle = m_Thread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_Thread = NULL;
	}
}

void CVacuumGaugeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CVacuumGaugeDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON1, &CVacuumGaugeDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON4, &CVacuumGaugeDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// CVacuumGaugeDlg 메시지 처리기


BOOL CVacuumGaugeDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CVacuumGaugeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_dPressure_LLC = -1.0;
	m_dPressure_MC = -1.0;

	m_bOpenPort_State = FALSE;

	m_nCaugeSendCommandSel = 0;
	ThreadExitFlag = FALSE;

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32,32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_LLC_GAUGE_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_GAUGE_STATE))->SetIcon(m_LedIcon[0]);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CVacuumGaugeDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(GAUGE_UPDATE_TIMER);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}




int CVacuumGaugeDlg::OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
{
	int nRet = -1;
	nRet = CMKS390Gauge::OpenPort(sPortName, dwBaud, wByte, wStop, wParity);

	if (nRet != 0) {
		m_bOpenPort_State = FALSE;
		((CStatic*)GetDlgItem(IDC_ICON_LLC_GAUGE_STATE))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_GAUGE_STATE))->SetIcon(m_LedIcon[2]);
		SetDlgItemText(IDC_LLC_GAUGE_VIEWER, "Connection Fail");
		SetDlgItemText(IDC_MC_GAUGE_VIEWER, "Connection Fail");
		State = "Connection Fail";
	}
	else 
	{
		m_bOpenPort_State = TRUE;

		((CStatic*)GetDlgItem(IDC_ICON_LLC_GAUGE_STATE))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_GAUGE_STATE))->SetIcon(m_LedIcon[1]);

		m_Thread = AfxBeginThread(UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
		SetTimer(GAUGE_UPDATE_TIMER, 10, NULL);
	}
	return nRet;
}


void CVacuumGaugeDlg::Getup_gauge_data()
{

	//SetDlgItemText(IDC_LLC_GAUGE_VIEWER, Get_LLC_VacuumRate() + " TORR");
	//SetDlgItemText(IDC_MC_GAUGE_VIEWER, Get_MC_VacuumRate() + " TORR");

	SetDlgItemText(IDC_LLC_GAUGE_VIEWER, ch_2 + " TORR");
	SetDlgItemText(IDC_MC_GAUGE_VIEWER, ch_3 + " TORR");

	State = "Running";

	if (Get_LLC_VacuumRate() == _T("SYNTX ER") || Get_MC_VacuumRate() == _T("SYNTX ER"))
	{
		Vaccum_Gauge_State = GAUGE_ERROR;
		State = "Gauge Syntx Error";
	}
}

int CVacuumGaugeDlg::Getupdata()
{
	int ret = 0;
	int ret_ch1 = -1;
	int ret_ch2 = -1;
	char*	str_ch1;					// ch1 & ch2 send 명령 함수.
	char*	str_ch2;


	if (m_bOpenPort_State == TRUE)
	{
		ThreadExitFlag = FALSE;
		Vaccum_Gauge_State = GAUGE_NORMAL;

		ch3 = ch_3; //MC ---> MKS GAUGE ADDRESS 3
		ch2 = ch_2; //LLC --> MKS GAUGE ADDRESS 2

		//m_dPressure_LLC = atof(Get_LLC_VacuumRate());
		//m_dPressure_MC = atof(Get_MC_VacuumRate());
	
		m_dPressure_LLC = atof(ch2);
		m_dPressure_MC = atof(ch3);

		switch (m_nCaugeSendCommandSel)
		{
		case 0: //LLC&MC GAUGE VALUE READ
			str_ch1 = "#02RD\r";
			str_ch2 = "#03RD\r";
			break;
		case 1: //
			break;
		case 2: //
			break;
		default:
			break;
		}

		ret_ch1 = CMKS390Gauge::SendData((LPSTR)(LPCTSTR)str_ch1, 500);
		Sleep(10);
		ret_ch2 = CMKS390Gauge::SendData((LPSTR)(LPCTSTR)str_ch2, 500);

	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_GAUGE_STATE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_GAUGE_STATE))->SetIcon(m_LedIcon[0]);

		ThreadExitFlag = TRUE;
		Vaccum_Gauge_State = GAUGE_OFFLINE;
	}

	if (!ret_ch1 && !ret_ch2)  ret = 0;
	else ret = -1;

	return ret;
}


UINT CVacuumGaugeDlg::UpdataThread(LPVOID pParam)
{
	CVacuumGaugeDlg*  runthread = (CVacuumGaugeDlg*)pParam;

	while (!runthread->ThreadExitFlag)
	{
		runthread->Getupdata();
		Sleep(100);
	}

	return 0;
}

int CVacuumGaugeDlg::GetStatus()
{
	int ret = GAUGE_ERROR;
	switch (Vaccum_Gauge_State)
	{
	case GAUGE_ERROR :
		ret = GAUGE_ERROR;
		break;
	case GAUGE_OFFLINE:
		ret =  GAUGE_OFFLINE;
		break;
	case GAUGE_NORMAL :
		ret = GAUGE_NORMAL;
		break;
	case GAUGE_UPDATE_TIMER:
		ret = GAUGE_UPDATE_TIMER;
		break;
	default:
		break;
	}
	return ret;
}

void CVacuumGaugeDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == GAUGE_UPDATE_TIMER)
	{
		if (!ThreadExitFlag)
		{
			Getup_gauge_data();
			SetTimer(GAUGE_UPDATE_TIMER, 10, NULL);
		}
	}


	__super::OnTimer(nIDEvent);
}


void CVacuumGaugeDlg::OnBnClickedButton1()
{
	int ret = 1;
	char* str_ch1;

	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)(" LLC Carlibrate 를 진행 하시겠습니까 ? (대기상태확인 760Torr에서 진행요망) ")));	
	//if (pwdlg.m_strTxt != "srem")
	if (pwdlg.m_strTxt != "1234")
	{
		CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)(" Password 가 일치 하지 않습니다 ")));	//통신 상태 기록.
		::AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
		return;
	}
	else if (pwdlg.m_strTxt == "1234")
	{
		if (m_bOpenPort_State == TRUE)
		{
			CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC Carlibrate 를 진행 합니다.")));	//통신 상태 기록.
			::AfxMessageBox("Carlibrate를 진행 합니다.", MB_ICONINFORMATION);
			if (g_pIO->Is_LLC_Atm_Check())
			{
				CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("ATM Sensor 확인 완료")));	//통신 상태 기록
				if (m_dPressure_LLC > 755)
				{
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("진공값 755 이상 확인 완료")));	//통신 상태 기록
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC Carlibrate 시작")));	//통신 상태 기록
					str_ch1 = "#02TS\r";
					ret = CMKS390Gauge::SendData((LPSTR)(LPCTSTR)str_ch1, 500);
					Sleep(10);
					if (!ret)
					{
						CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC Carlibrate 완료")));	//통신 상태 기록
						::AfxMessageBox("LLC Carlibrate 완료", MB_ICONINFORMATION);
					}
					else
					{
						CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC Carlibrate 실패")));	//통신 상태 기록
						::AfxMessageBox("LLC Carlibrate 실패", MB_ICONINFORMATION);
					}
				}
				else
				{
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC 진공값 755 이하 이므로 Carlibrate 진행 불가")));	//통신 상태 기록
					::AfxMessageBox("LLC 진공값 755 이하 이므로 Carlibrate 진행 불가.", MB_ICONINFORMATION);
				}
			}
			else
			{
				CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC ATM Sensor 감지 Error ( 진공상태 확인요망 ) Carlibrate 진행 불가")));	//통신 상태 기록.
				::AfxMessageBox("LLC ATM Sensor 감지 Error ( 진공상태 확인요망 ) Carlibrate 진행 불가", MB_ICONINFORMATION);
			}
		}
		else
		{
			CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("Gauge Port 연결 에러발생! 연결 확인 요망")));	//통신 상태 기록.
			::AfxMessageBox("Gauge Port 연결 에러발생! 연결 확인 요망", MB_ICONINFORMATION);
		}
	}
}


void CVacuumGaugeDlg::OnBnClickedButton4()
{
	int ret = 1;
	char* str_ch1;

	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)(" MC Carlibrate 를 진행 하시겠습니까 ? (대기상태확인 760Torr에서 진행요망) ")));
	//if (pwdlg.m_strTxt != "srem")
	if (pwdlg.m_strTxt != "1234")
	{
		CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)(" Password 가 일치 하지 않습니다 ")));	//통신 상태 기록.
		::AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
		return;
	}
	else if (pwdlg.m_strTxt == "1234")
	{
		if (m_bOpenPort_State == TRUE)
		{
			CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC Carlibrate 를 진행 합니다.")));	//통신 상태 기록.
			::AfxMessageBox("Carlibrate를 진행 합니다.", MB_ICONINFORMATION);
			if (g_pIO->Is_MC_Atm_Check())
			{
				CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("ATM Sensor 확인 완료")));	//통신 상태 기록
				if (m_dPressure_MC > 755)
				{
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("진공값 755 이상 확인 완료")));	//통신 상태 기록
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC Carlibrate 시작")));	//통신 상태 기록
					str_ch1 = "#03TS\r";
					ret = CMKS390Gauge::SendData((LPSTR)(LPCTSTR)str_ch1, 500);
					Sleep(10);
					if (!ret)
					{
						CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC Carlibrate 완료")));	//통신 상태 기록
						::AfxMessageBox("MC Carlibrate 완료", MB_ICONINFORMATION);
					}
					else
					{
						CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC Carlibrate 실패")));	//통신 상태 기록
						::AfxMessageBox("MC Carlibrate 실패", MB_ICONINFORMATION);
					}
				}
				else
				{
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC 진공값 755 이하 이므로 Carlibrate 진행 불가")));	//통신 상태 기록
					::AfxMessageBox("MC 진공값 755 이하 이므로 Carlibrate 진행 불가.", MB_ICONINFORMATION);
				}
			}
			else
			{
				CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC ATM Sensor 감지 Error ( 진공상태 확인요망 ) Carlibrate 진행 불가")));	//통신 상태 기록.
				::AfxMessageBox("MC ATM Sensor 감지 Error ( 진공상태 확인요망 ) Carlibrate 진행 불가", MB_ICONINFORMATION);
			}
		}
		else
		{
			CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("Gauge Port 연결 에러발생! 연결 확인 요망")));	//통신 상태 기록.
			::AfxMessageBox("Gauge Port 연결 에러발생! 연결 확인 요망", MB_ICONINFORMATION);
		}
	}
}
