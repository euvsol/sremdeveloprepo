﻿/**
 * LLC Turbo Pump Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CTurboPumpDlg 대화 상자

class CTurboPumpDlg : public CDialogEx , public CAgilentTwisTorr304TMPCtrl
{
	DECLARE_DYNAMIC(CTurboPumpDlg)

public:
	CTurboPumpDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CTurboPumpDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TURBO_PUMP_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	
	bool						m_LLCTmpThreadExitFlag;
	CWinThread*					m_LLCTmpThread;
	static UINT					Updata_tmp_Thread(LPVOID pParam);

	DECLARE_MESSAGE_MAP()
public:

	HICON	m_LedIcon[3];

	clock_t		m_start_time, m_finish_time;

	int	OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);
	virtual	int	ReceiveData(char *lParam, DWORD dwRead); ///< Recive
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//afx_msg void OnBnClickedPumpTrgateCheck();
	//afx_msg void OnBnClickedPumpSrcgateCheck();
	//afx_msg void OnBnClickedPumpLlgateCheck();

	bool						m_LLCTmpConnectStateFlag;
	int							m_LLC_Tmp_State;


	//Vacuum Sequence에서 필요로하는 함수
	
	int			GetStatus();			//TMP 상태를 General_define.h에 정의하고, 현재 상태를 return: ex) TMP_NORMAL,TMP_OFFLINE,TMP_ERROR,....
	//int GetUpdate();
	int			LLCTmpReceiveDataUpdate();
	int			LLC_Tmp_Off();
	int			LLC_Tmp_On();
	void		LLCErrorModeOn();				// Error 발생 시 실행 함수.
	void GetStatus_Logging();


	afx_msg void OnBnClickedLlcTmpBtnOn();
	afx_msg void OnBnClickedLlcTmpBtnOff();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	int			Is_LLC_TmpWorking() { return m_LLC_Tmp_State; }
	BOOL		Is_LLC_TmpError()	 { return StrLLCTmpReceiveDataErrorState == _T("No Error") ? FALSE : TRUE; }
	BOOL		Is_LLC_Tmp_Connected() { return m_LLCTmpConnectStateFlag; }
	
	void		LLCTmpSendUpdate();


	CString		Get_LLC_Tmp_Hz() { return StrLLCTmpReceiveDataHz; }							//속도(hz)
	CString		Get_LLC_Tmp_Error_Code() { return StrLLCTmpReceiveDataErrorCode; }					// Error Code
	CString		Get_LLC_Tmp_Error_Comment() { return StrLLCTmpReceiveDataErrorState; }			// Error comment
	CString		Get_LLC_Tmp_State() { return StrLLCTmpReceiveDataState; }							// Tmp State

	CString		StrLLCTmpState;
	

	CString		StrLLCTmpReceiveDataHz;
	CString		StrLLCTmpReceiveDataRpm;
	CString		StrLLCTmpReceiveDataErrorCode;
	CString		StrLLCTmpReceiveDataErrorState;
	CString		StrLLCTmpReceiveDataTemperature;
	CString		StrLLCTmpReceiveDataTemperatureHeatsink;
	CString		StrLLCTmpReceiveDataTemperatureAir;
	CString		StrLLCTmpReceiveDataState;

	BYTE		m_BYTE[15];
 };
