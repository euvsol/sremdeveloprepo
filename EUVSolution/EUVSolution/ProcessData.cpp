#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

CProcessData::CProcessData()
{
	curFileIndex=0;
	pMeasureList = NULL;	
	TotalMeasureNum = 0;
	TotalMeasureSpecNum = 0;
	RecipeGenDateTime.Empty();
	EquipmentModelName.Empty();
	m_strSubstrateMap.Empty();
	Substrate.Empty();
	Lot.Empty();
	Device.Empty();
	SetupID.Empty();
	Step.Empty();
	SubstrateDirection.Empty();	
	SubstrateID.Empty();

	m_pstPTRMeasureList = NULL;

	ResetData();
}

CProcessData::~CProcessData()
{
	ResetData();
}

void CProcessData::ResetData()
{
	curFileIndex = 0;
	DeleteDefectRecord();
	TotalMeasureSpecNum = 0;
	RecipeGenDateTime.Empty();
	EquipmentModelName.Empty();
	m_strSubstrateMap.Empty();
	Substrate.Empty();
	Lot.Empty();
	SubstrateSize = 0;
	Device.Empty();
	SetupID.Empty();
	Step.Empty();
	SubstrateDirection.Empty();
	SubstrateID.Empty();
	Slot = 0;
	TotalMeasureNum = 0;
	classifiedNum = 0;
	int i = 0;
	m_stDiePitch.X = m_stDiePitch.Y = 0.0;
	m_stMaskCenterPos_um.X = m_stMaskCenterPos_um.Y = 0.0;
	m_stDieOriginPos_um.X = m_stDieOriginPos_um.Y = 0.0;
	m_stOriginCoordinate_um.X = m_stOriginCoordinate_um.Y = 0.0;
	for (i = 0; i < 4; i++)
	{
		m_stAlignmentPoint_um[i].X = 0.0;
		m_stAlignmentPoint_um[i].Y = 0.0;
	}
	m_stReferenceCoord_um.X = m_stReferenceCoord_um.Y = 0.0;
	m_stAlignmentLT_um.X = m_stAlignmentLT_um.Y = 0.0;
	m_stAlignmentRT_um.X = m_stAlignmentRT_um.Y = 0.0;
	m_stAlignmentRB_um.X = m_stAlignmentRB_um.Y = 0.0;
	m_stReferenceFrequencyPos_um.X = m_stReferenceFrequencyPos_um.Y = 0.0;
	m_stBeamDiagnosisPos_um.X = m_stBeamDiagnosisPos_um.Y = 0.0;
	m_nBeamDiagPosExposureTime_ms = 0;
	m_nPTRTotalMeasureNum = 0;
}

int CProcessData::LoadHeaderInfo(char *fpath)
{	
	int ret = 0, Index = 1, i=0;
	CString msg;
	char buf[512];

	ret=OpenFile(fpath, CFile::modeRead);
	if(ret!=TRUE)
	{		
		msg.Format("File Open 실패");
		return ret;
	}

	ret=FindItem("EquipmentModelName");
	if(ret>0)
	{
		GetBuffer(buf,';');
		EquipmentModelName = buf;
	}

	ret = FindItem("Substrate ");
	if (ret > 0)
	{
		GetBuffer(buf, ';');
		Substrate = buf;
	}

	ret = FindItem("SubstrateMap ");
	if (ret > 0)
	{
		GetBuffer(buf, ';');
		m_strSubstrateMap = buf;
	}

	ret=FindItem("SubstrateSize");
	if(ret>0)
	{
		GetItem(buf);		
		SubstrateSize = GetInt();
	}

	ret = FindItem("Lot");
	if (ret > 0)
	{
		GetItem(buf);
		Lot = buf;
	}

	ret = FindItem("Step");
	if (ret > 0)
	{
		GetItem(buf);
		Step = buf;
	}

	ret = FindItem("Device");
	if (ret > 0)
	{
		GetItem(buf);
		Device = buf;
	}
	ret = FindItem("SubstrateID");
	if (ret > 0)
	{
		GetItem(buf);
		PassCurrentLine();
		SubstrateID = buf;
	}

	ret = FindItem("Slot", Index);
	if (ret > 0)
	{
		Slot = GetInt();
		PassCurrentLine();
	}

	ret = FindItem("SubstrateDirection");
	if (ret > 0)
	{
		GetItem(buf);
		SubstrateDirection = buf;
	}

	ret = FindItem("SubstrateCenterPosition");
	if (ret > 0)
	{
		GetLine(buf);
		double xloc, yloc;
		sscanf(buf, "%lf %lf", &xloc, &yloc);
		m_stMaskCenterPos_um.X = xloc;
		m_stMaskCenterPos_um.Y = yloc;
	}

	ret = FindItem("AlignmentPointNumber");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%d", &nAlignmentPointTotal);
	}

	double posx = 0.0, posy = 0.0;
	int tmp;
	if (ret > 0)
	{
		for (i = 0; i < nAlignmentPointTotal; i++)
		{
			GetLine(buf);
			sscanf(buf, "%d %lf %lf", &tmp, &posx, &posy);
			m_stAlignmentPoint_um[i].X = posx;
			m_stAlignmentPoint_um[i].Y = posy;
		}

		if (nAlignmentPointTotal == 3)
		{
			m_stReferenceCoord_um.X = m_stAlignmentPoint_um[0].X;
			m_stReferenceCoord_um.Y = m_stAlignmentPoint_um[0].Y;
			m_stAlignmentLT_um.X = m_stAlignmentPoint_um[1].X;
			m_stAlignmentLT_um.Y = m_stAlignmentPoint_um[1].Y;
			m_stAlignmentRT_um.X = m_stAlignmentPoint_um[2].X;
			m_stAlignmentRT_um.Y = m_stAlignmentPoint_um[2].Y;
		}
		if (nAlignmentPointTotal == 4)
		{
			m_stReferenceCoord_um.X = m_stAlignmentPoint_um[0].X;
			m_stReferenceCoord_um.Y = m_stAlignmentPoint_um[0].Y;
			m_stAlignmentLT_um.X = m_stAlignmentPoint_um[1].X;
			m_stAlignmentLT_um.Y = m_stAlignmentPoint_um[1].Y;
			m_stAlignmentRT_um.X = m_stAlignmentPoint_um[2].X;
			m_stAlignmentRT_um.Y = m_stAlignmentPoint_um[2].Y;
			m_stAlignmentRB_um.X = m_stAlignmentPoint_um[3].X;
			m_stAlignmentRB_um.Y = m_stAlignmentPoint_um[3].Y;
		}
	}

	ret = FindItem("OriginCoordinate");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%lf %lf", &posx, &posy);
		m_stOriginCoordinate_um.X = posx;
		m_stOriginCoordinate_um.Y = posy;
	}

	ret = FindItem("Notch1Coordinate");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%lf %lf", &posx, &posy);
		m_stNotchAlignPos_um[0].X = posx;
		m_stNotchAlignPos_um[0].Y = posy;
	}

	ret = FindItem("Notch2Coordinate");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%lf %lf", &posx, &posy);
		m_stNotchAlignPos_um[1].X = posx;
		m_stNotchAlignPos_um[1].Y = posy;
	}

	ret = FindItem("ReferenceFrequencyPosition");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%lf %lf", &posx, &posy);
		m_stReferenceFrequencyPos_um.X = posx;
		m_stReferenceFrequencyPos_um.Y = posy;
	}

	ret = FindItem("BeamDiagnosisPosition");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%lf %lf", &posx, &posy);
		m_stBeamDiagnosisPos_um.X = posx;
		m_stBeamDiagnosisPos_um.Y = posy;
	}

	ret = FindItem("BeamDiagnosisPositionExposureTime");
	if (ret > 0)
	{
		m_nBeamDiagPosExposureTime_ms = GetInt();
	}

	ret = FindItem("DiePitch");
	if (ret > 0)
	{
		m_stDiePitch.X = GetDouble();
		m_stDiePitch.Y = GetDouble();
	}

	ret = FindItem("DieOriginPosition");
	if (ret > 0)
	{
		GetLine(buf);
		double xloc, yloc;
		sscanf(buf, "%lf %lf", &xloc, &yloc);
		m_stDieOriginPos_um.X = xloc;
		m_stDieOriginPos_um.Y = yloc;
	}

	ret = FindItem("DieInformation");
	PassCurrentLine();
	if (ret > 0)
	{
		GetLine(buf);
		double diesizex, diesizey, diepitchx, diepitchy;
		diesizex = diesizey = diepitchx = diepitchy = 0.0;
		int ndiex, ndiey;
		ndiex = ndiey = 0;
		sscanf(buf, "%lf %lf %d %d %lf %lf", &diesizex, &diesizey, &ndiex, &ndiey, &diepitchx, &diepitchy);
		DieInformation.DieSizeX = diesizex;
		DieInformation.DieSizeY = diesizey;
		DieInformation.nDieX = ndiex;
		DieInformation.nDieY = ndiey;
		DieInformation.DiePitchX = diepitchx;
		DieInformation.DiePitchY = diepitchy;
		//Diepitch 재개산
		if (diepitchx == 0)
		{
			m_stDiePitch.X = diesizex;
			m_stDiePitch.Y = diesizey;
		}
		else
		{
			m_stDiePitch.X = diepitchx;
			m_stDiePitch.Y = diepitchy;
		}
	}
	else
	{
		DieInformation.nDieX = 0;
		DieInformation.nDieY = 0;
	}

	ret = FindItem("TotalMeasureNumber");
	if (ret > 0)
	{
		TotalMeasureNum=GetInt();
	}

	return ret;
}


int CProcessData::LoadMACROCOmmandData(char *fpath)
{
	char buf[512];
	int i;
	int ret = 0;
	classifiedNum = 0;
	CString bufcompare;
	CString msg;

	ret = OpenFile(fpath, CFile::modeRead);
	if (ret != TRUE)
	{
		msg.Format("File Open 실패");
		AfxMessageBox(msg);
		Close();
		return ret;
	}

	if (TotalMeasureNum > 10000)
	{
		TotalMeasureNum = 10000;
	}
	if (TotalMeasureNum < 0)
	{
		TotalMeasureNum = 0;
	}

	if (TotalMeasureNum != 0)
	{
		if (CommandList == NULL)
		{
			CommandList = (_CommandList *)::GlobalAlloc(GPTR, sizeof(_CommandList)*TotalMeasureNum);
		}
		else
		{
			DeleteMacroRecord();
			CommandList = (_CommandList *)::GlobalAlloc(GPTR, sizeof(_CommandList)*TotalMeasureNum);

		}
		::GlobalUnlock(CommandList);
	}
	else
	{
		DeleteMacroRecord();

		Close();
		return 1;
	}

	ret = FindItem("MeasurementSpec");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%d", &TotalMeasureSpecNum);
	}

	GetDefectListName(buf);
	GetLine(buf);

	int cnt = 0;
	for (i = 0; i < TotalMeasureNum; i++)
	{
		if (GetLine(buf) == FALSE)
			break;
		
		bufcompare = buf;
		GetDefectListAllValue(buf);

		strcpy(CommandList[i].Command, GetCommandValue("COMMAND"));

		::GlobalUnlock(CommandList);
	}
	Close();

	return 1;

}

/* MACRO SYSTEM (kjh) */
int CProcessData::LoadMACROMeasureData(char *fpath)
{
	char buf[512];
	//char com[30] = { 0 };
	int i;
	int ret = 0;
	int xdie = 0, ydie = 0;
	double xdiepitch = 0.0f, ydiepitch = 0.0f;
	double centerlocx = 0.0f, centerlocy = 0.0f;
	int theta = 0, m_CurrentOrientation = 1;
	double xrel, yrel, xsize, ysize, defectarea, dsize;
	int MeasureNo, xindex, yindex, classnum, test, clusternum;
	classifiedNum = 0;
	int totalcnt = 0;
	CString bufcompare;
	CString msg;

	

	ret = OpenFile(fpath, CFile::modeRead);
	if (ret != TRUE)
	{
		msg.Format("File Open 실패");
		AfxMessageBox(msg);
		Close();
		return ret;
	}

	if (TotalMeasureNum > 10000)
	{
		TotalMeasureNum = 10000;
	}
	if (TotalMeasureNum < 0)
	{
		TotalMeasureNum = 0;
	}

	if (TotalMeasureNum != 0)
	{
		if (pMeasureList == NULL)
		{
			pMeasureList = (_MeasurementList *)::GlobalAlloc(GPTR, sizeof(_MeasurementList)*TotalMeasureNum);
		}
		else
		{
			DeleteDefectRecord();
			pMeasureList = (_MeasurementList *)::GlobalAlloc(GPTR, sizeof(_MeasurementList)*TotalMeasureNum);
		}

		::GlobalLock(pMeasureList);
	}
	else
	{
		DeleteDefectRecord();

		Close();
		return 1;
	}

	ret = FindItem("MeasurementSpec");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%d", &TotalMeasureSpecNum);
	}

	GetDefectListName(buf);
	GetLine(buf);

	int cnt = 0;
	for (i = 0; i < TotalMeasureNum; i++)
	{
		if (GetLine(buf) == FALSE)
			break;

		bufcompare = buf;

		GetDefectListAllValue(buf);

		pMeasureList[i].MeasurementNo = (int)GetDefectEachValue("NUMBER");
		//pMeasureList[i].Command = GetCommandValue("COMMAND");
		//strcpy(com,GetCommandValue("COMMAND"));
		strcpy(pMeasureList[i].Command,GetCommandValue("COMMAND"));

		pMeasureList[i].ReferenceCoodX_um = GetDefectEachValue("REFXPOS");
		pMeasureList[i].ReferenceCoodY_um = yrel = GetDefectEachValue("REFYPOS");
		pMeasureList[i].DieXNo = (int)GetDefectEachValue("DIEXNO");
		pMeasureList[i].DieYNo = (int)GetDefectEachValue("DIEYNO");
		pMeasureList[i].DieXPos = GetDefectEachValue("DIEXPOS");
		pMeasureList[i].DieYPos = GetDefectEachValue("DIEYPOS");
		pMeasureList[i].dFOVSize = GetDefectEachValue("FOVSIZE");
		pMeasureList[i].dStepSize = GetDefectEachValue("STEPSIZE");
		pMeasureList[i].nRepeatNo = (int)GetDefectEachValue("REPEATNO");
		pMeasureList[i].nThroughFocusNo = (int)GetDefectEachValue("THROUGHFOCUSNO");
		pMeasureList[i].dThroughFocusStep = GetDefectEachValue("THROUGHFOCUSSTEP");

		pMeasureList[i].index = i;
		pMeasureList[i].nSortID = i + 1;
		pMeasureList[i].bValidPoint = TRUE;

		::GlobalUnlock(pMeasureList);
	}
	Close();

	return 1;

}

int CProcessData::LoadMeasureData(char *fpath)
{
	char buf[512];
	int i;
	int ret=0;
	int xdie=0,ydie=0;
	double xdiepitch=0.0f,ydiepitch=0.0f;
	double centerlocx=0.0f,centerlocy=0.0f;
	int theta=0,m_CurrentOrientation=1;
	double xrel, yrel, xsize, ysize, defectarea, dsize;
	int MeasureNo, xindex, yindex, classnum, test, clusternum;
	classifiedNum = 0;
	int totalcnt = 0;
	CString bufcompare;
	CString msg;

	ret=OpenFile(fpath, CFile::modeRead);
	if(ret!=TRUE)
	{
		msg.Format("File Open 실패");
		AfxMessageBox(msg);
		Close();
		return ret;			
	}
	
	if(TotalMeasureNum>10000)
	{
		TotalMeasureNum=10000;	
	}
	if(TotalMeasureNum<0)
	{
		TotalMeasureNum=0;	
	}
		
	if(TotalMeasureNum!=0)
	{
		if(pMeasureList==NULL)
		{
			pMeasureList=(_MeasurementList *)::GlobalAlloc(GPTR,sizeof(_MeasurementList)*TotalMeasureNum);
		}
		else
		{
			DeleteDefectRecord();
			pMeasureList=(_MeasurementList *)::GlobalAlloc(GPTR,sizeof(_MeasurementList)*TotalMeasureNum);
		}

		::GlobalLock(pMeasureList);
	}	
	else
	{
		DeleteDefectRecord();

		Close();	
		return 1;
	}

	ret=FindItem("MeasurementSpec");
	if(ret>0)
	{
		GetLine(buf);
		sscanf(buf,"%d",&TotalMeasureSpecNum);
	}

	GetDefectListName(buf);
	GetLine(buf);

	int cnt = 0;
	for(i=0; i<TotalMeasureNum; i++)
	{			
		if(GetLine(buf)==FALSE)
			break;

		bufcompare=buf;
				
		GetDefectListAllValue(buf);

		pMeasureList[i].MeasurementNo = (int)GetDefectEachValue("NUMBER");
		pMeasureList[i].ReferenceCoodX_um = GetDefectEachValue("REFXPOS");
		pMeasureList[i].ReferenceCoodY_um = yrel =GetDefectEachValue("REFYPOS");
		pMeasureList[i].DieXNo = (int)GetDefectEachValue("DIEXNO");
		pMeasureList[i].DieYNo = (int)GetDefectEachValue("DIEYNO");
		pMeasureList[i].DieXPos = GetDefectEachValue("DIEXPOS");
		pMeasureList[i].DieYPos = GetDefectEachValue("DIEYPOS");
		pMeasureList[i].dFOVSize = GetDefectEachValue("FOVSIZE");
		pMeasureList[i].dStepSize =GetDefectEachValue("STEPSIZE");
		pMeasureList[i].nRepeatNo = (int)GetDefectEachValue("REPEATNO");
		pMeasureList[i].nThroughFocusNo = (int)GetDefectEachValue("THROUGHFOCUSNO");
		pMeasureList[i].dThroughFocusStep = GetDefectEachValue("THROUGHFOCUSSTEP");
 
		pMeasureList[i].index=i;
		pMeasureList[i].nSortID =i+1;
		pMeasureList[i].bValidPoint = TRUE;
			
		::GlobalUnlock(pMeasureList);
	}
	Close();	

	return 1;
}

int CProcessData::LoadPhaseMeasureData(char *fpath)
{
	char buf[512];
	int i;
	int ret = 0;
	CString bufcompare;
	CString msg;

	ret = OpenFile(fpath, CFile::modeRead);
	if (ret != TRUE)
	{
		msg.Format("File Open 실패");
		AfxMessageBox(msg);
		Close();
		return ret;
	}

	if (TotalMeasureNum > 10000)	//최대 측정 포인트 개수를 일단 10000개만
	{
		TotalMeasureNum = 10000;
	}
	if (TotalMeasureNum < 0)
	{
		TotalMeasureNum = 0;
	}

	if (TotalMeasureNum != 0)
	{
		if (pMeasureList == NULL)
		{
			pMeasureList = (_MeasurementList *)::GlobalAlloc(GPTR, sizeof(_MeasurementList)*TotalMeasureNum);
		}
		else
		{
			DeleteDefectRecord();
			pMeasureList = (_MeasurementList *)::GlobalAlloc(GPTR, sizeof(_MeasurementList)*TotalMeasureNum);
		}

		::GlobalLock(pMeasureList);
	}
	else
	{
		DeleteDefectRecord();

		Close();
		return 1;
	}

	ret = FindItem("MeasurementSpec");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%d", &TotalMeasureSpecNum);
	}

	GetDefectListName(buf);
	GetLine(buf);

	int cnt = 0;
	for (i = 0; i < TotalMeasureNum; i++)
	{
		if (GetLine(buf) == FALSE)
			break;

		bufcompare = buf;

		GetDefectListAllValue(buf);

		pMeasureList[i].MeasurementNo = (int)GetDefectEachValue("NUMBER");
		pMeasureList[i].ReferenceCoodX_um = GetDefectEachValue("LEFTXPOS");
		pMeasureList[i].ReferenceCoodY_um = GetDefectEachValue("LEFTYPOS");
		pMeasureList[i].dRightX_um = GetDefectEachValue("RIGHTXPOS");
		pMeasureList[i].dRightY_um = GetDefectEachValue("RIGHTYPOS");
		pMeasureList[i].nRepeatNo = (int)GetDefectEachValue("REPEATNO");

		pMeasureList[i].dAlignExposureTime = GetDefectEachValue("EXPOSURE_ALIGN");
		pMeasureList[i].dMeasureExposureTime = GetDefectEachValue("EXPOSURE_MEASURE");
		pMeasureList[i].bIsUseCoasreAlign = (int)GetDefectEachValue("USE_COARSE_ALIGN");
		pMeasureList[i].dLsHalfPitch = GetDefectEachValue("LS_HALF_PITCH");
		

		pMeasureList[i].index = i;
		pMeasureList[i].nSortID = i + 1;
		pMeasureList[i].bValidPoint = TRUE;

		::GlobalUnlock(pMeasureList);
	}
	Close();

	return 1;
}

int CProcessData::LoadPTRMeasureData(char *fpath)
{
	char buf[512];
	int i = 0, j = 0, k = 0;
	int ret = 0;
	CString bufcompare;
	CString msg;

	ret = OpenFile(fpath, CFile::modeRead);
	if (ret != TRUE)
	{
		msg.Format("File Open 실패");
		AfxMessageBox(msg);
		Close();
		return ret;
	}

	if (TotalMeasureNum > 10000)	//최대 측정 포인트 개수를 일단 10000개만
	{
		TotalMeasureNum = 10000;
	}
	if (TotalMeasureNum < 0)
	{
		TotalMeasureNum = 0;
	}

	if (TotalMeasureNum != 0)
	{
		if (pMeasureList == NULL)
		{
			pMeasureList = (_MeasurementList *)::GlobalAlloc(GPTR, sizeof(_MeasurementList)*TotalMeasureNum);
		}
		else
		{
			DeleteDefectRecord();
			pMeasureList = (_MeasurementList *)::GlobalAlloc(GPTR, sizeof(_MeasurementList)*TotalMeasureNum);
		}

		::GlobalLock(pMeasureList);
	}
	else
	{
		DeleteDefectRecord();

		Close();
		return 1;
	}

	ret = FindItem("MeasurementSpec");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%d", &TotalMeasureSpecNum);
	}

	GetDefectListName(buf);
	GetLine(buf);

	int cnt = 0;
	for (i = 0; i < TotalMeasureNum; i++)
	{
		if (GetLine(buf) == FALSE)
			break;

		bufcompare = buf;

		GetDefectListAllValue(buf);

		pMeasureList[i].MeasurementNo = (int)GetDefectEachValue("NUMBER");
		pMeasureList[i].m_dCouponXPos_um = GetDefectEachValue("COUPONXPOS");
		pMeasureList[i].m_dCouponYPos_um = GetDefectEachValue("COUPONYPOS");
		pMeasureList[i].dMeasurementXPos_um = GetDefectEachValue("MESUREXPOS");
		pMeasureList[i].dMeasurementYPos_um = GetDefectEachValue("MESUREYPOS");
		//pMeasureList[i].ReferenceCoodX_um = pMeasureList[i].m_dCouponXPos_um + pMeasureList[i].dMeasurementXPos_um;
		//pMeasureList[i].ReferenceCoodY_um = pMeasureList[i].m_dCouponYPos_um + pMeasureList[i].dMeasurementYPos_um;
		pMeasureList[i].dExposureTime_msec = (int)GetDefectEachValue("EXPOSURETIME");
		pMeasureList[i].nRepeatNo = (int)GetDefectEachValue("REPEATNO");

		pMeasureList[i].nMeasurementXNo = (int)GetDefectEachValue("MEASUREXNO");
		pMeasureList[i].nMeasurementYNo = (int)GetDefectEachValue("MEASUREYNO");
		m_nPTRTotalMeasureNum += pMeasureList[i].nMeasurementXNo*pMeasureList[i].nMeasurementYNo;
		pMeasureList[i].dMeasureDistance_um = GetDefectEachValue("DISTANCE");

		pMeasureList[i].index = i;
		pMeasureList[i].nSortID = i + 1;
		pMeasureList[i].bValidPoint = TRUE;

		::GlobalUnlock(pMeasureList);
	}

	if (m_nPTRTotalMeasureNum != 0)
	{
		if (m_pstPTRMeasureList == NULL)
		{
			m_pstPTRMeasureList = (_PTRMeasurementList *)::GlobalAlloc(GPTR, sizeof(_PTRMeasurementList)*m_nPTRTotalMeasureNum);
		}
		else
		{
			::GlobalFree(m_pstPTRMeasureList);
			m_pstPTRMeasureList = NULL;
			m_pstPTRMeasureList = (_PTRMeasurementList *)::GlobalAlloc(GPTR, sizeof(_PTRMeasurementList)*m_nPTRTotalMeasureNum);
		}
		::GlobalLock(m_pstPTRMeasureList);

		int previousNo = 0;
		for (i = 0; i < TotalMeasureNum; i++)
		{
			if (i > 0)
				previousNo += pMeasureList[i - 1].nMeasurementXNo * pMeasureList[i - 1].nMeasurementYNo;
			for (j = 0; j < pMeasureList[i].nMeasurementYNo; j++)
			{
				for (k = 0; k < pMeasureList[i].nMeasurementXNo; k++)
				{
					m_pstPTRMeasureList[(pMeasureList[i].nMeasurementXNo*j + k) + previousNo].dReferenceCoodX_um =
						pMeasureList[i].m_dCouponXPos_um + pMeasureList[i].dMeasurementXPos_um + pMeasureList[i].dMeasureDistance_um*k;
					m_pstPTRMeasureList[(pMeasureList[i].nMeasurementXNo*j + k) + previousNo].dReferenceCoodY_um =
						pMeasureList[i].m_dCouponYPos_um + pMeasureList[i].dMeasurementYPos_um + pMeasureList[i].dMeasureDistance_um*j;
				}
			}
		}
		::GlobalUnlock(m_pstPTRMeasureList);
	}

	Close();

	return 1;
}

int CProcessData::LoadLITHOMeasureData(char *fpath)
{
	char buf[512];
	int i;
	int ret = 0;
	CString bufcompare;
	CString msg;

	ret = OpenFile(fpath, CFile::modeRead);
	if (ret != TRUE)
	{
		msg.Format("File Open 실패");
		AfxMessageBox(msg);
		Close();
		return ret;
	}

	if (TotalMeasureNum > 10000)
	{
		TotalMeasureNum = 10000;
	}
	if (TotalMeasureNum < 0)
	{
		TotalMeasureNum = 0;
	}

	if (TotalMeasureNum != 0)
	{
		if (pMeasureList == NULL)
		{
			pMeasureList = (_MeasurementList *)::GlobalAlloc(GPTR, sizeof(_MeasurementList)*TotalMeasureNum);
		}
		else
		{
			DeleteDefectRecord();
			pMeasureList = (_MeasurementList *)::GlobalAlloc(GPTR, sizeof(_MeasurementList)*TotalMeasureNum);
		}

		::GlobalLock(pMeasureList);
	}
	else
	{
		DeleteDefectRecord();

		Close();
		return 1;
	}

	ret = FindItem("MeasurementSpec");
	if (ret > 0)
	{
		GetLine(buf);
		sscanf(buf, "%d", &TotalMeasureSpecNum);
	}

	GetDefectListName(buf);
	GetLine(buf);

	int cnt = 0;
	for (i = 0; i < TotalMeasureNum; i++)
	{
		if (GetLine(buf) == FALSE)
			break;

		bufcompare = buf;

		GetDefectListAllValue(buf);

		pMeasureList[i].MeasurementNo = (int)GetDefectEachValue("NUMBER");
		pMeasureList[i].ReferenceCoodX_um = GetDefectEachValue("REFXPOS");
		pMeasureList[i].ReferenceCoodY_um = GetDefectEachValue("REFYPOS");			
		pMeasureList[i].dFOVSizeX = GetDefectEachValue("FOVXSIZE");
		pMeasureList[i].dFOVSizeY = GetDefectEachValue("FOVYSIZE");
		pMeasureList[i].dStepSize = GetDefectEachValue("STEPSIZE");
		pMeasureList[i].nRepeatNo = (int)GetDefectEachValue("REPEATNO");
		pMeasureList[i].dExposureHeight_um = GetDefectEachValue("HEIGHT");
		pMeasureList[i].dStepSize = GetDefectEachValue("STEPSIZE");
		pMeasureList[i].nRepeatNo = (int)GetDefectEachValue("REPEATNO");
		pMeasureList[i].dExposureTime_msec = GetDefectEachValue("EXPOSURETIME");
		pMeasureList[i].dBeam_pitch = GetDefectEachValue("BEAM_PITCH");
		pMeasureList[i].dBeam_yaw = GetDefectEachValue("BEAM_YAW");

		pMeasureList[i].index = i;
		pMeasureList[i].nSortID = i + 1;
		pMeasureList[i].bValidPoint = TRUE;

		::GlobalUnlock(pMeasureList);
	}

	dExposureTime_total = 0.0;
	nExposureTime_total = 0;

	for (int count = 0; count < TotalMeasureNum; count++)
	{
		dExposureTime_total += pMeasureList[count].dExposureTime_msec;
	}

	// total Exposure time + 3 min (stage moving time)
	nExposureTime_total = (int)((dExposureTime_total / 1000) / 60) + 3;

	Close();

	return 1;
}

void CProcessData::DeleteDefectRecord()
{
	if(pMeasureList==NULL)
		return;
	::GlobalFree(pMeasureList);

	pMeasureList = NULL;
}

void CProcessData::DeleteMacroRecord()
{
	if (CommandList == NULL)
		return;
	::GlobalFree(CommandList);

	CommandList = NULL;
}

double CProcessData::GetXPos(int ndiex, double xrel)
{
	double posx, outposx;
	posx = outposx = 0.0;
	posx = (double)ndiex * m_stDiePitch.X + xrel - m_stMaskCenterPos_um.X;	
	outposx = posx/1000.0;
	return outposx;
}

double CProcessData::GetYPos(int ndiey, double yrel)
{
	double posy, outposy;
	posy = outposy = 0.0;
	posy = (double)ndiey * m_stDiePitch.Y + yrel - m_stMaskCenterPos_um.Y;
	outposy = posy/1000.0;
	return outposy;
}

int CProcessData::GetXIndex(double posx)
{
	double diex=(posx*1000.+m_stMaskCenterPos_um.X)/m_stDiePitch.X;
	if(diex<0.)
	{
		return (int)(diex-1);		
	}
	else
	{
		return (int)diex;
	}
}

int CProcessData::GetYIndex(double posy)
{
	double diey=(posy*1000.+m_stMaskCenterPos_um.Y)/m_stDiePitch.Y;
	if(diey<0.)
	{
		return (int)(diey-1);		
	}
	else
	{
		return (int)diey;
	}
}

void CProcessData::GetDefectListName(char *name)
{
	CString str=name;
	CString temp;
	int		count=0;

	sscanf(name,"%d",&count);
	sscanf(str,"%s",temp);
	str.Replace(temp," ");
	str.Replace(";"," ");
	
	for(int i=0;i<count;i++)
	{
		sscanf(str,"%s",temp);
		if(temp.IsEmpty()!=0)
		{
			strcpy(DefListName[i],(char*)(LPCSTR)temp);

			int len=strlen((char*)(LPCSTR)temp);
			int start=str.Find(temp);
			if(start>=0)
				str.Delete(start,len);
		}
	}
}

void CProcessData::GetDefectListAllValue(char *value)
{
	CString str=value;
	CString temp;
	int i = 0;

	str.Replace(";"," ");
	
	for(i=0;i<TotalMeasureSpecNum;i++)
	{
		sscanf(str,"%s",temp);
		if(temp.IsEmpty()!=0)
		{
			strcpy(DefListValue[i],(char*)(LPCSTR)temp);

			int len=strlen((char*)(LPCSTR)temp);
			int start=str.Find(temp);
			if(start>=0)
				str.Delete(start,len);
		}
	}
	//--------- Def Rec Spec 개수 이후에도 Def List상에 값이 존해하는 경우에 그 부분 저장시키는 부분---------//
	str.Replace(0x0d,0x00);
	str.Replace(0x0a,0x00);
	int ret=TRUE;
	while(ret)
	{
		ret=str.Replace("  "," ");
	}
	strcpy(DefListValue[i],(char*)(LPCSTR)str);
}

double CProcessData::GetDefectEachValue(char *name)
{
	CString str=name;
	CString temp;
	int index=-1;
	int len=strlen(name);

	for(int i=0;i<TotalMeasureSpecNum;i++)
	{
		temp=DefListName[i];
		int deflen=strlen(DefListName[i]);
		if(deflen==len)
		{
			if(temp.Find(name)!=-1)
			{
				index=i;
			}
		}
	}
	if(index<=-1 || index>20)
	{
		return 0;
	}
	return atof(DefListValue[index]);
}

CString CProcessData::GetCommandValue(char *name)
{
	CString str = name;
	CString temp;
	CString return_str;

	int index = -1;
	int len = strlen(name);

	for (int i = 0; i < TotalMeasureSpecNum; i++)
	{
		temp = DefListName[i];
		int deflen = strlen(DefListName[i]);
		if (deflen == len)
		{
			if (temp.Find(name) != -1)
			{
				index = i;
			}
		}
	}
	if (index <= -1 || index > 20)
	{
		return 0;
	}


	return_str = DefListValue[index];
	return return_str;
}


BOOL CProcessData::OpenFile(LPCTSTR lpszFileName, UINT nOpenFlags)
{
	if (nOpenFlags == CFile::modeRead)
	{
		if ((fp = fopen(lpszFileName, "rb")) == NULL)
			return FALSE;
	}
	else if (nOpenFlags == CFile::modeWrite)
	{
		if ((fp = fopen(lpszFileName, "wb")) == NULL)
			return FALSE;
	}
	else
	{
		if ((fp = fopen(lpszFileName, "rb+")) == NULL)
			return FALSE;
	}
	return TRUE;

}

void CProcessData::Close()
{
	if (fp != 0)
		fclose(fp);
}

long CProcessData::FindItem(char *Item, int Index)
{
	char line[255];
	long Cnt = 0;
	CString str;
	int dwPos, len;

	CString finditem = Item;
	finditem.MakeUpper();

	fseek(fp, 0L, SEEK_SET);
	do {
		str = fgets(line, sizeof(line), fp);
		if (str.IsEmpty())
			break;
		str = line;
		str.MakeUpper();
		int n = 0;
		if ((n = str.Find(finditem)) >= 0)
		{
			dwPos = ftell(fp);
			int lenth = 0;
			lenth = str.GetLength();
			len = str.GetLength() - (n + (strlen(Item)));
			fseek(fp, dwPos - len, SEEK_SET);
			Cnt++;
		}
	} while (Cnt < Index);
	return(Cnt);
}

void CProcessData::PassCurrentLine(void)
{
	char line[255];
	fgets(line, 255, fp);
}

BOOL CProcessData::GetLine(char *line)
{
	if (fgets((char *)(LPCSTR)line, 512, fp) == NULL)
		return FALSE;
	else
		return TRUE;
}

int CProcessData::GetInt()
{
	char buf[60];
	GetItem(buf);
	return atoi(buf);
}

double CProcessData::GetDouble()
{
	char buf[60];
	GetItem(buf);
	return (double)atof(buf);
}

int CProcessData::GetItem(char* Buffer)
{
	int Cnt = 0, TCnt = 0, pre_read = 0;
	char Byte = 0;
	char pBuffer[255];

	DWORD dwPos = ftell(fp);
	// 만약 처음에 divider가 읽혀질 경우 count한다.
	do {
		if ((Byte = fgetc(fp)) < 1)
			return -1;
		if ((pre_read++) >= 255)
			return -1;
	} while ((Byte == ';') || (Byte == ' ' || Byte == '\t'));//(Byte == 0x0d) || (Byte<=' ') ||

	if (Byte != 0x0d)
	{
		// 여기부터 실제 데이터를 읽는다.
		do {
			if ((Byte = fgetc(fp)) < 1)
				return -1;

			if ((Cnt++) >= 255)
				return (-1);

		} while ((Byte != 0x0d) && (Byte != ';') && (Byte != ' ') && (Byte != '\t'));// 0x0d는 CR임
		fseek(fp, dwPos, SEEK_SET);
		fread(pBuffer, pre_read + Cnt, 1, fp);
		fseek(fp, dwPos + pre_read + Cnt, SEEK_SET);
	}
	else
	{
		fseek(fp, dwPos, SEEK_SET);
		fread(pBuffer, pre_read + Cnt, 1, fp);
		fseek(fp, dwPos + pre_read + Cnt, SEEK_SET);
	}

	if (Byte == 0x0d) {
		pBuffer[(pre_read + Cnt)] = 0;
		pBuffer[(pre_read + Cnt) - 1] = 0;
		//this->Read(&Byte,1);
	}
	else {
		Cnt--;
		pBuffer[pre_read + Cnt] = 0;
	}
	strcpy(Buffer, pBuffer + (--pre_read));

	Buffer[strlen(Buffer)] = 0;

	return Cnt;
}

long CProcessData::CountItem(char *Buffer)
{
	int Cnt = 0;
	char line[255];
	CString str;

	fseek(fp, 0L, SEEK_SET);
	while (!feof(fp))
	{
		fgets(line, sizeof(line), fp);
		str = line;
		if (str.Find(Buffer) >= 0)
		{
			Cnt++;
		}
	}
	return Cnt;
}

int CProcessData::GetBuffer(char* Buffer, char Divider, int IncludeDivider)
{

	int Cnt = 0;
	char Byte = 0;
	long dwPos;

	dwPos = ftell(fp);

	do {
		if ((Byte = fgetc(fp)) < 1)
			break;

		if ((Cnt++) >= 255)
			return (-1);

	} while (Byte != Divider);

	Cnt--;

	fseek(fp, dwPos, SEEK_SET);

	fread(Buffer, sizeof(char), Cnt + IncludeDivider, fp);
	Buffer[Cnt] = 0;


	return Cnt;
}