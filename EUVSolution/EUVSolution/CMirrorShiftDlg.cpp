﻿// CMirrorShiftDlg.cpp: 구현 파일
//


#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

//#include "pch.h"


// CMirrorShiftDlg 대화 상자

IMPLEMENT_DYNAMIC(CMirrorShiftDlg, CDialogEx)

CMirrorShiftDlg::CMirrorShiftDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MIRROR_SHIFT_DIALOG, pParent)
{

}

CMirrorShiftDlg::~CMirrorShiftDlg()
{
}

void CMirrorShiftDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMirrorShiftDlg, CDialogEx)
END_MESSAGE_MAP()


// CMirrorShiftDlg 메시지 처리기
