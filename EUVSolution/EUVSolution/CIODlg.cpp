﻿// CCrevisIODlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

#define IO_INPUT_PART_DIALOG_WIDTH		750
#define IO_INPUT_PART_DIALOG_HEIGHT		1200
#define IO_OUTPUT_PART_DIALOG_WIDTH		800
#define IO_OUTPUT_PART_DIALOG_HEIGHT	1200

// CCrevisIODlg 대화 상자

IMPLEMENT_DYNAMIC(CIODlg, CDialogEx)

CIODlg::CIODlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_DIALOG, pParent)
{
	m_pInputData  = NULL;
	m_pOutputData = NULL;
	m_pDlgDi	  = NULL;
	m_pDlgDo	  = NULL;
	m_pDlgAi	  = NULL;
	m_pDlgAo	  = NULL;
	m_pThread	  = NULL;

	m_bThreadExitFlag = FALSE;

	m_FastVent_Inlet_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	//m_Source_Gate_Open_On = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_TRGate_Vauccum_Value_On = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_TRGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SlowVent_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCForelineGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCTmpGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCFastRough_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCSlowRough_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCForelineGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCTmpGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCFastRough_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCSlowRough_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_FastVent_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_FastVent_Inlet_WriteEvent_Close = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SlowVent_Inlet_WriteEvent_Close = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SlowVent_Inlet_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);

	m_LLCGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_TRGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SlowVent_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCForelineGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCTmpGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCFastRough_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCSlowRough_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCForelineGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCTmpGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCFastRough_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCSlowRough_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_FastVent_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
}

CIODlg::~CIODlg()
{
	return_int.Empty();
	m_bThreadExitFlag = TRUE;
	CloseHandle(m_FastVent_Inlet_WriteEvent_Open);
	CloseHandle(m_TRGate_Vauccum_Value_On);
	CloseHandle(m_LLCGate_WriteEvent);
	CloseHandle(m_TRGate_WriteEvent);
	CloseHandle(m_SlowVent_WriteEvent);
	CloseHandle(m_LLCForelineGate_WriteEvent);
	CloseHandle(m_LLCTmpGate_WriteEvent);
	CloseHandle(m_LLCFastRough_WriteEvent);
	CloseHandle(m_LLCSlowRough_WriteEvent);
	CloseHandle(m_MCForelineGate_WriteEvent);
	CloseHandle(m_MCTmpGate_WriteEvent);
	CloseHandle(m_MCFastRough_WriteEvent);
	CloseHandle(m_MCSlowRough_WriteEvent);
	CloseHandle(m_FastVent_WriteEvent);
	CloseHandle(m_FastVent_Inlet_WriteEvent_Close);
	CloseHandle(m_SlowVent_Inlet_WriteEvent_Close);
	CloseHandle(m_SlowVent_Inlet_WriteEvent_Open);
	//CloseHandle(m_Source_Gate_Open_On);
	CloseHandle(m_LLCGate_WriteEvent_Open);
	CloseHandle(m_TRGate_WriteEvent_Open);
	CloseHandle(m_SlowVent_WriteEvent_Open);
	CloseHandle(m_LLCForelineGate_WriteEvent_Open);
	CloseHandle(m_LLCTmpGate_WriteEvent_Open);
	CloseHandle(m_LLCFastRough_WriteEvent_Open);
	CloseHandle(m_LLCSlowRough_WriteEvent_Open);
	CloseHandle(m_MCForelineGate_WriteEvent_Open);
	CloseHandle(m_MCTmpGate_WriteEvent_Open);
	CloseHandle(m_MCFastRough_WriteEvent_Open);
	CloseHandle(m_MCSlowRough_WriteEvent_Open);
	CloseHandle(m_FastVent_WriteEvent_Open);

	if (m_pThread != NULL)
	{
		HANDLE threadHandle = m_pThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pThread = NULL;
	}


	if (m_pInputData)
		free(m_pInputData);
	if (m_pOutputData)
		free(m_pOutputData);


	if (m_pDlgDi != NULL)
	{
		m_pDlgDi->DestroyWindow();
		delete[] m_pDlgDi;
		m_pDlgDi = NULL;
	}

	if (m_pDlgDo != NULL)
	{
		m_pDlgDo->DestroyWindow();
		delete[] m_pDlgDo;
		m_pDlgDo = NULL;
	}

	if (m_pDlgAi != NULL)
	{
		m_pDlgAi->DestroyWindow();
		delete[] m_pDlgAi;
		m_pDlgAi = NULL;
	}

	if (m_pDlgAo != NULL)
	{
		m_pDlgAo->DestroyWindow();
		delete[] m_pDlgAo;
		m_pDlgAo = NULL;
	}

	if (m_pDlgDoPart != NULL)
	{
		m_pDlgDoPart->DestroyWindow();
		delete[] m_pDlgDoPart;
		m_pDlgDoPart = NULL;
	}

	if (m_pDlgDiPart != NULL)
	{
		m_pDlgDiPart->DestroyWindow();
		delete[] m_pDlgDiPart;
		m_pDlgDiPart = NULL;
	}

	if (m_pDlgDiLinePart != NULL)
	{
		m_pDlgDiLinePart->DestroyWindow();
		delete[] m_pDlgDiLinePart;
		m_pDlgDiLinePart = NULL;
	}

	if (m_pDlgDoLinePart != NULL)
	{
		m_pDlgDoLinePart->DestroyWindow();
		delete[] m_pDlgDoLinePart;
		m_pDlgDoLinePart = NULL;
	}

	StopIoUpdate();
	CloseDevice();
}

void CIODlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_DIGITAL_IN, m_TabCtrlDi);
	DDX_Control(pDX, IDC_TAB_DIGITAL_OUT, m_TabCtrlDo);
}


BEGIN_MESSAGE_MAP(CIODlg, CDialogEx)
	ON_WM_DESTROY()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_DIGITAL_IN, &CIODlg::OnSelchangeTabDigitalIn)
	ON_NOTIFY(TCN_SELCHANGING, IDC_TAB_DIGITAL_IN, &CIODlg::OnSelchangingTabDigitalIn)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_DIGITAL_OUT, &CIODlg::OnSelchangeTabDigitalOut)
	ON_NOTIFY(TCN_SELCHANGING, IDC_TAB_DIGITAL_OUT, &CIODlg::OnSelchangingTabDigitalOut)
	ON_BN_CLICKED(IDC_BTN_ANALOG, &CIODlg::OnBnClickedBtnAnalog)
	ON_BN_CLICKED(IDC_BTN_DIGITAL, &CIODlg::OnBnClickedBtnDigital)
	ON_BN_CLICKED(IDC_BTN_MAINT, &CIODlg::OnBnClickedBtnMaint)
	ON_BN_CLICKED(IDC_BTN_OPER, &CIODlg::OnBnClickedBtnOper)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_LINE, &CIODlg::OnBnClickedBtnLine)
END_MESSAGE_MAP()


// CCrevisIODlg 메시지 처리기


BOOL CIODlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CIODlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_bCamsensor_Trigger_On = false;

	for (int nIdx = 0; nIdx < DIGITAL_INPUT_NUMBER; nIdx++)
	{
		m_bDigitalIn[nIdx] = false;
	}

	for (int nIdx = 0; nIdx < DIGITAL_OUTPUT_NUMBER; nIdx++)
	{
		m_bDigitalOut[nIdx] = false;
	}

	m_TabCtrlDi.SetItemSize(CSize(0, 40));
	m_TabCtrlDo.SetItemSize(CSize(0, 40));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	this->SetWindowPos(NULL, 0, 0, 1550, 1900, SWP_NOZORDER);

	GetDlgItem(IDC_BTN_ANALOG)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_ANALOG)->EnableWindow(false);
	GetDlgItem(IDC_BTN_DIGITAL)->EnableWindow(true);

	GetDlgItem(IDC_BTN_OPER)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_OPER)->EnableWindow(false);
	GetDlgItem(IDC_BTN_MAINT)->EnableWindow(true);

	m_nIOMode = OPER_MODE_ON;
	m_nERROR_MODE = RUN_OFF;

	m_bCrevis_Open_Port = FALSE;

	//CREVIS TEST IP .. jhkim
	CString str = _T("192.168.10.10");
	//CString str = _T("192.168.100.4");
	/////////
	//OpenDevice(str);
	CreateInputControl(ANALOG_MAIN_TAB);
	CreateOutputControl(ANALOG_MAIN_TAB);

	((CStatic*)GetDlgItem(IDC_CREVIS_OFF))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_CREVIS_ON))->SetIcon(m_LedIcon[0]);

	return_int.Empty();

	m_bAPSequenceNormalCloseFlag = FALSE;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CIODlg::OpenDevice(CString strIpAddr)
{
	int nRet = -1;
		
	nRet = CCrevisIOCtrl::OpenDevice(LPSTR(LPCTSTR(strIpAddr)));
	
	if (nRet == 0)
	{
		m_pInputData  = (BYTE*)malloc(GetInputSize());
		m_pOutputData = (BYTE*)malloc(GetOutputSize());

		m_bCrevis_Open_Port = TRUE;

		((CStatic*)GetDlgItem(IDC_CREVIS_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_CREVIS_OFF))->SetIcon(m_LedIcon[0]);
	
		StartIoUpdate();
		
		CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM IO Successfully Connected")));	//통신 상태 기록.
		CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM OPER Mode On ")));				// IO Mode 기록.

		m_pThread = AfxBeginThread(IoCheckThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
		SetTimer(IO_UPDATE_TIMER, 10, NULL);
	}
	else
	{
		m_bCrevis_Open_Port = FALSE;
		
		CECommon::SaveLogFile("SREM__IO__", _T((LPSTR)(LPCTSTR)("IO Connection Failed")));							//통신 상태 기록.
		
		((CStatic*)GetDlgItem(IDC_CREVIS_OFF))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_CREVIS_ON))->SetIcon(m_LedIcon[0]);
		
		nRet = -87001;
		g_pAlarm->SetAlarm((nRet));
	}

	return nRet;
}

void CIODlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == IO_UPDATE_TIMER)
	{
		if (!m_bThreadExitFlag)
		{
			Open_SourceGate_Check();
			SetTimer(IO_UPDATE_TIMER, 10, NULL);
		}
	}


	__super::OnTimer(nIDEvent);
}



UINT CIODlg::IoCheckThread(LPVOID pParam)
{
	CIODlg*  pIoThread = (CIODlg*)pParam;

	while (!pIoThread->m_bThreadExitFlag)
	{
		pIoThread->GetInputIoData();
		pIoThread->GetOutputIoData();
		//pIoThread->Main_Error_Status();
		Sleep(50);
	}

	return 0;
}

void CIODlg::GetInputIoData()
{
	if (GetInputSize() < 0) return;


	int nDecimal = 0;
	int nPos_ch1 = 0;
	int nPos_ch2 = 32;
	int nPos_ch3 = 64;
	int nPos_ch4 = 96;


	ReadInputData(m_pInputData);

	for (int nIdx = 0; nIdx < GetInputSize(); nIdx++)
	{
		m_nDecimal_Addr_In[nIdx] = m_pInputData[nIdx];
	}

	// ANALOG INPUT

	//Module #1
	m_nDecimal_Analog_in_ch_1 = (m_nDecimal_Addr_In[1] << 8) | m_nDecimal_Addr_In[0];
	m_nDecimal_Analog_in_ch_2 = (m_nDecimal_Addr_In[3] << 8) | m_nDecimal_Addr_In[2];
	m_nDecimal_Analog_in_ch_3 = (m_nDecimal_Addr_In[5] << 8) | m_nDecimal_Addr_In[4];
	m_nDecimal_Analog_in_ch_4 = (m_nDecimal_Addr_In[7] << 8) | m_nDecimal_Addr_In[6];
	
	// Module #2
	m_nDecimal_Analog_in_ch_5 = (m_nDecimal_Addr_In[9] << 8) | m_nDecimal_Addr_In[8];
	m_nDecimal_Analog_in_ch_6 = (m_nDecimal_Addr_In[11] << 8) | m_nDecimal_Addr_In[10];
	m_nDecimal_Analog_in_ch_7 = (m_nDecimal_Addr_In[13] << 8) | m_nDecimal_Addr_In[12];
	m_nDecimal_Analog_in_ch_8 = (m_nDecimal_Addr_In[15] << 8) | m_nDecimal_Addr_In[14];
	
	// 온도센서모듈
	m_nDecimal_Analog_in_ch_9 = (m_nDecimal_Addr_In[17] << 8) | m_nDecimal_Addr_In[16];
	m_nDecimal_Analog_in_ch_10 = (m_nDecimal_Addr_In[19] << 8) | m_nDecimal_Addr_In[18];
	m_nDecimal_Analog_in_ch_11 = (m_nDecimal_Addr_In[21] << 8) | m_nDecimal_Addr_In[20];
	m_nDecimal_Analog_in_ch_12 = (m_nDecimal_Addr_In[23] << 8) | m_nDecimal_Addr_In[22];


	//	DIGITAL INPUT
	m_nDecimal_Digital_in_Module_1 = (m_nDecimal_Addr_In[27] << 24) | (m_nDecimal_Addr_In[26] << 16) | (m_nDecimal_Addr_In[25] << 8) | (m_nDecimal_Addr_In[24]);
	m_nDecimal_Digital_in_Module_2 = (m_nDecimal_Addr_In[31] << 24) | (m_nDecimal_Addr_In[30] << 16) | (m_nDecimal_Addr_In[29] << 8) | (m_nDecimal_Addr_In[28]);
	m_nDecimal_Digital_in_Module_3 = (m_nDecimal_Addr_In[35] << 24) | (m_nDecimal_Addr_In[34] << 16) | (m_nDecimal_Addr_In[33] << 8) | (m_nDecimal_Addr_In[32]);
	m_nDecimal_Digital_in_Module_4 = (m_nDecimal_Addr_In[39] << 24) | (m_nDecimal_Addr_In[38] << 16) | (m_nDecimal_Addr_In[37] << 8) | (m_nDecimal_Addr_In[36]);


	while (1)
	{
		m_bDigitalIn[nPos_ch1] = m_nDecimal_Digital_in_Module_1 % 2;
		m_bDigitalIn[nPos_ch2] = m_nDecimal_Digital_in_Module_2 % 2;
		m_bDigitalIn[nPos_ch3] = m_nDecimal_Digital_in_Module_3 % 2;
		m_bDigitalIn[nPos_ch4] = m_nDecimal_Digital_in_Module_4 % 2;

		m_nDecimal_Digital_in_Module_1 = m_nDecimal_Digital_in_Module_1 / 2;
		m_nDecimal_Digital_in_Module_2 = m_nDecimal_Digital_in_Module_2 / 2;
		m_nDecimal_Digital_in_Module_3 = m_nDecimal_Digital_in_Module_3 / 2;
		m_nDecimal_Digital_in_Module_4 = m_nDecimal_Digital_in_Module_4 / 2;

		nPos_ch1++;
		nPos_ch2++;
		nPos_ch3++;
		nPos_ch4++;

		if (m_nDecimal_Digital_in_Module_1 == 0 && m_nDecimal_Digital_in_Module_2 == 0 && m_nDecimal_Digital_in_Module_3 == 0 && m_nDecimal_Digital_in_Module_4 == 0) {
			break;
		}
	}
	//CString test_str;
	//CString test_str1;


	//LLC GATE OPEN STATE
	if (m_bDigitalIn[LL_GATE_CLOSE] == false && m_bDigitalIn[LL_GATE_OPEN] == true)	SetEvent(m_LLCGate_WriteEvent_Open);
	// TR GATE OPEN STATE
	if (m_bDigitalIn[TR_GATE_CLOSE] == false && m_bDigitalIn[TR_GATE_OPEN] == true)	SetEvent(m_TRGate_WriteEvent_Open);
	//// MC FAST ROUGH OPEN STATE
	if (m_bDigitalIn[MC_FAST_ROUGHING_OPEN] == true && m_bDigitalIn[MC_ROUGHING_CLOSE] == false) SetEvent(m_MCFastRough_WriteEvent_Open);
	//// MC TMP GATE OPEN STATE
	if (m_bDigitalIn[MC_TMP_GATE_OPEN] == true && m_bDigitalIn[MC_TMP_GATE_CLOSE] == false) SetEvent(m_MCTmpGate_WriteEvent_Open);
	//// MC TMP FORELINE OPEN STATE
	if (m_bDigitalIn[MC_FORELINE_OPEN] == true && m_bDigitalIn[MC_FORELINE_CLOSE] == false) SetEvent(m_MCForelineGate_WriteEvent_Open);
	//// LLC FAST ROUGH CLOSE STATE
	if (m_bDigitalIn[LL_FAST_ROUGHING_OPEN] == true && m_bDigitalIn[LL_ROUGHING_CLOSE] == false) SetEvent(m_LLCFastRough_WriteEvent_Open);
	////LLC TMP GATE CLOSE STATE
	if (m_bDigitalIn[LL_TMP_GATE_OPEN] == true && m_bDigitalIn[LL_TMP_GATE_CLOSE] == false) SetEvent(m_LLCTmpGate_WriteEvent_Open);
	////LLC FORELINE CLOSE STATE
	if (m_bDigitalIn[LL_FORELINE_OPEN] == true && m_bDigitalIn[LL_FORELINE_CLOSE] == false) SetEvent(m_LLCForelineGate_WriteEvent_Open);
	//LLC GATE CLOSE STATE
	if (m_bDigitalIn[LL_GATE_CLOSE] == true && m_bDigitalIn[LL_GATE_OPEN] == false) SetEvent(m_LLCGate_WriteEvent);
	// TR GATE CLOSE STATE
	if (m_bDigitalIn[(TR_GATE_CLOSE)] == true && m_bDigitalIn[(TR_GATE_OPEN)] == false) SetEvent(m_TRGate_WriteEvent);
	//LLC FORELINE CLOSE STATE
	if (m_bDigitalIn[LL_FORELINE_OPEN] == false && m_bDigitalIn[LL_FORELINE_CLOSE] == true) SetEvent(m_LLCForelineGate_WriteEvent);

//	Pre_open = m_bDigitalIn[Din = LL_FORELINE_OPEN];
//	Pre_close = m_bDigitalIn[Din = LL_FORELINE_CLOSE];
//
//	CString llc_forline_open_str;
//	llc_forline_open_str.Format("%d", m_bDigitalIn[Din = LL_FORELINE_OPEN]);
//	CString llc_forline_close_str;
//	llc_forline_close_str.Format("%d", m_bDigitalIn[Din = LL_FORELINE_CLOSE]);
//
//	if (Pre_open != Post_open)
//	{
//		Post_open = Pre_open;
//		CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_OPEN_STATE ::" + llc_forline_open_str)));
//	}
//	if (Pre_close != Post_close)
//	{
//		Post_close = Pre_close;
//		CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_CLOSE_STATE ::" + llc_forline_close_str)));
//	}


	//LLC TMP GATE CLOSE STATE
	if (m_bDigitalIn[LL_TMP_GATE_OPEN] == false && m_bDigitalIn[LL_TMP_GATE_CLOSE] == true) SetEvent(m_LLCTmpGate_WriteEvent);
	// LLC FAST ROUGH CLOSE STATE
	if (m_bDigitalIn[LL_FAST_ROUGHING_OPEN] == false && m_bDigitalIn[LL_ROUGHING_CLOSE] == true) SetEvent(m_LLCFastRough_WriteEvent);
	// MC TMP FORELINE CLOSE STATE
	if (m_bDigitalIn[MC_FORELINE_OPEN] == false && m_bDigitalIn[MC_FORELINE_CLOSE] == true) SetEvent(m_MCForelineGate_WriteEvent);
	// MC TMP GATE CLOSE STATE
	if (m_bDigitalIn[MC_TMP_GATE_OPEN] == false && m_bDigitalIn[MC_TMP_GATE_CLOSE] == true) SetEvent(m_MCTmpGate_WriteEvent);
	// MC FAST ROUGH CLOSE STATE
	if (m_bDigitalIn[MC_FAST_ROUGHING_OPEN] == false && m_bDigitalIn[MC_ROUGHING_CLOSE] == true) SetEvent(m_MCFastRough_WriteEvent);
	

	// MAIN VALVE CHECK
	//Main_Error_Status();
}

void CIODlg::GetOutputIoData()
{
	if (GetOutputSize() < 0) return;

	int nDecimal = 0;
	int nPos = 0;

	int nPos_ch1 = 0;
	int nPos_ch2 = 16;
	int nPos_ch3 = 32;




	ReadOutputData(m_pOutputData);

	for (int nIdx = 0; nIdx < GetOutputSize(); nIdx++)
	{
		m_nDecimal_Addr_Out[nIdx] = m_pOutputData[nIdx];
	}

	//ANALOG_OUTPUT
	m_nDecimal_Analog_out_ch_1 = (m_nDecimal_Addr_Out[1] << 8) | m_nDecimal_Addr_Out[0];
	m_nDecimal_Analog_out_ch_2 = (m_nDecimal_Addr_Out[3] << 8) | m_nDecimal_Addr_Out[2];
	m_nDecimal_Analog_out_ch_3 = (m_nDecimal_Addr_Out[5] << 8) | m_nDecimal_Addr_Out[4];
	m_nDecimal_Analog_out_ch_4 = (m_nDecimal_Addr_Out[7] << 8) | m_nDecimal_Addr_Out[6];

	//DIGITAL_OUTPUT
	m_nDecimal_Digital_out_Module_1 = (m_nDecimal_Addr_Out[9] << 8) | (m_nDecimal_Addr_Out[8]);
	m_nDecimal_Digital_out_Module_2 = (m_nDecimal_Addr_Out[11] << 8) | (m_nDecimal_Addr_Out[10]);
	m_nDecimal_Digital_out_Module_3 = (m_nDecimal_Addr_Out[13] << 8) | (m_nDecimal_Addr_Out[12]);
	//nDecimal_Digital_in_Module_4 = (nDecimal_Addr_In[36] << 24) | (nDecimal_Addr_In[37] << 16) | (nDecimal_Addr_In[38] << 8) | (nDecimal_Addr_In[39]);



	while (1)
	{
		
		m_bDigitalOut[nPos_ch1] = m_nDecimal_Digital_out_Module_1 % 2;
		m_bDigitalOut[nPos_ch2] = m_nDecimal_Digital_out_Module_2 % 2;
		m_bDigitalOut[nPos_ch3] = m_nDecimal_Digital_out_Module_3 % 2;

		m_nDecimal_Digital_out_Module_1 = m_nDecimal_Digital_out_Module_1 / 2;
		m_nDecimal_Digital_out_Module_2 = m_nDecimal_Digital_out_Module_2 / 2;
		m_nDecimal_Digital_out_Module_3 = m_nDecimal_Digital_out_Module_3 / 2;

		nPos_ch1++;
		nPos_ch2++;
		nPos_ch3++;
	
		//if (nDecimal_Digital_out_Module_1 == 0 && nDecimal_Digital_out_Module_2 == 0 && nDecimal_Digital_out_Module_3 == 0) {
		//	break;
		//}

		if (nPos_ch1 == 16 && nPos_ch2 == 32 && nPos_ch3 == 48) {
			break;
		}

	}

	//// FAST VENT OPEN STATE
	if (m_bDigitalOut[FAST_VENT_INLET] == true && m_bDigitalOut[FAST_VENT_OUTLET] == true) SetEvent(m_FastVent_WriteEvent_Open);
	//// MC SLOW ROUGH OPEN STATE
	if (m_bDigitalOut[MC_SLOW_ROUGH] == true) SetEvent(m_MCSlowRough_WriteEvent_Open);
	//// LLC SLOW ROUGH OPEN STATE
	if (m_bDigitalOut[LL_SLOW_ROUGH_VV] == true) SetEvent(m_LLCSlowRough_WriteEvent_Open);
	////SLOWVENT OPEN STATE
	if (m_bDigitalOut[SLOW_VENT_INLET] == true && m_bDigitalOut[SLOW_VENT_OUTLET] == true) SetEvent(m_SlowVent_WriteEvent_Open);
	// MC SLOW ROUGH CLOSE STATE
	if (m_bDigitalOut[MC_SLOW_ROUGH] == false) SetEvent(m_MCSlowRough_WriteEvent);
	// FAST VENT CLOSE STATE
	if (m_bDigitalOut[FAST_VENT_INLET] == false && m_bDigitalOut[FAST_VENT_OUTLET] == false) SetEvent(m_FastVent_WriteEvent);
	//SLOWVENT CLOSE STATE
	if (m_bDigitalOut[SLOW_VENT_INLET] == false && m_bDigitalOut[SLOW_VENT_OUTLET] == false)	SetEvent(m_SlowVent_WriteEvent);
	
	// Source Gate Open On
	//if (m_bDigitalOut[Dout = SHUTTER_OPEN] == true) SetEvent(m_Source_Gate_Open_On);

	// LLC SLOW ROUGH CLOSE STATE
	if (m_bDigitalOut[LL_SLOW_ROUGH_VV] == false) SetEvent(m_LLCSlowRough_WriteEvent);
	// FAST VENT INLET OPEN STATE
	if (m_bDigitalOut[FAST_VENT_INLET] == true) SetEvent(m_FastVent_Inlet_WriteEvent_Open);
	// FAST VENT INLET CLOSE STATE
	if (m_bDigitalOut[FAST_VENT_INLET] == false) SetEvent(m_FastVent_Inlet_WriteEvent_Close);
	// SLOW VENT INLET OPEN STATE
	if (m_bDigitalOut[SLOW_VENT_INLET] == true) SetEvent(m_SlowVent_Inlet_WriteEvent_Open);
	// SLOW VENT INLET CLOSE STATE
	if (m_bDigitalOut[SLOW_VENT_INLET] == false) SetEvent(m_SlowVent_Inlet_WriteEvent_Close);

	
	//CString llc_forline_out_str;
	//llc_forline_out_str.Format("%d", m_bDigitalOut[Dout = LL_FORELINE]);
	//CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_OUTPUT_STATE ::" + llc_forline_out_str)));

	///////////////////////////////////////////////////
	//CAMSENSOR Trigger On/Off Force input
	//////////////////////////////////////////////////
	if(m_bCamsensor_Trigger_On) 
		m_bDigitalOut[CAM_SENSOR_TRIGGER] = true;
	else 
		m_bDigitalOut[CAM_SENSOR_TRIGGER] = false;


}

void CIODlg::OnDestroy()
{
	CDialogEx::OnDestroy();

//	m_bThreadExitFlag = TRUE;
//
//	CloseHandle(m_LLCGate_WriteEvent);
//	CloseHandle(m_TRGate_WriteEvent);
//	CloseHandle(m_SlowVent_WriteEvent);
//	CloseHandle(m_LLCForelineGate_WriteEvent);
//	CloseHandle(m_LLCTmpGate_WriteEvent);
//	CloseHandle(m_LLCFastRough_WriteEvent);
//	CloseHandle(m_LLCSlowRough_WriteEvent);
//	CloseHandle(m_MCForelineGate_WriteEvent);
//	CloseHandle(m_MCTmpGate_WriteEvent);
//	CloseHandle(m_MCFastRough_WriteEvent);
//	CloseHandle(m_MCSlowRough_WriteEvent);
//	CloseHandle(m_FastVent_WriteEvent);
//
//	CloseHandle(m_LLCGate_WriteEvent_Open);
//	CloseHandle(m_TRGate_WriteEvent_Open);
//	CloseHandle(m_SlowVent_WriteEvent_Open);
//	CloseHandle(m_LLCForelineGate_WriteEvent_Open);
//	CloseHandle(m_LLCTmpGate_WriteEvent_Open);
//	CloseHandle(m_LLCFastRough_WriteEvent_Open);
//	CloseHandle(m_LLCSlowRough_WriteEvent_Open);
//	CloseHandle(m_MCForelineGate_WriteEvent_Open);
//	CloseHandle(m_MCTmpGate_WriteEvent_Open);
//	CloseHandle(m_MCFastRough_WriteEvent_Open);
//	CloseHandle(m_MCSlowRough_WriteEvent_Open);
//	CloseHandle(m_FastVent_WriteEvent_Open);
//
//
//	if (m_pInputData)
//		free(m_pInputData);
//	if (m_pOutputData)
//		free(m_pOutputData);
//	
//	if (m_pThread != NULL)
//	{
//		HANDLE threadHandle = m_pThread->m_hThread;
//		DWORD dwResult;
//		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
//		if (dwResult == WAIT_TIMEOUT)
//		{
//			DWORD dwExitCode = STILL_ACTIVE;
//			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
//			if (dwExitCode == STILL_ACTIVE)	//259
//			{
//				TerminateThread(threadHandle, 0/*dwExitCode*/);
//				CloseHandle(threadHandle);
//			}
//		}
//		m_pThread = NULL;
//	}
//
//	StopIoUpdate();
//
//	CloseDevice();
}

void CIODlg::CreateInputControl(int main_tab)
{
	CString sIndex;


	int nChCount = 0;// (GetInputSize() / 2) + (GetInputSize() % 2);
	m_TabCtrlDi.SetItemSize(CSize(400, 50));
	m_TabCtrlDi.SetWindowPos(NULL, 20, 110, 730, 1750, SWP_SHOWWINDOW);
	CRect TabRect;
	m_TabCtrlDi.GetClientRect(&TabRect);


	nChCount = ANALOG_SUB_TAB;
	m_pDlgAi = new CAnalogInput;
	
	m_pDlgAi->Create(IDD_IO_INPUT_AN_DIALOG, &m_TabCtrlDi);
	m_pDlgAi->SetWindowPos(NULL, 10, 1050, TabRect.Width() - 20, TabRect.Height() - 1090, SWP_SHOWWINDOW);
	m_pDlgAi->InitControls_AI();
	m_pDlgAi->ShowWindow(SW_SHOW);

	nChCount = DIGITAL_SUB_IN_TAB;
	m_pDlgDi = new CDigitalInput[nChCount];
	for (int nIdx = 0; nIdx < nChCount; nIdx++)
	{
		sIndex.Format("%d", nIdx);
		m_TabCtrlDi.InsertItem(nIdx + 1, _T("CH") + sIndex);
		m_pDlgDi[nIdx].Create(IDD_IO_INPUT_DIALOG, &m_TabCtrlDi);
		m_pDlgDi[nIdx].SetWindowPos(NULL, 10, 70, TabRect.Width() - 20, TabRect.Height() - 790, SWP_SHOWWINDOW);
		m_pDlgDi[nIdx].ShowWindow(SW_HIDE);
		m_pDlgDi[nIdx].InitControls_DI(nIdx);
	}

	m_pDlgDi[0].ShowWindow(SW_SHOW);

	m_pDlgDiPart = new CDigitalInputPart;
	m_pDlgDiPart->Create(IDD_IO_PART_INPUT_DIALOG, this);
	m_pDlgDiPart->SetWindowPos(NULL, 10, 150, IO_INPUT_PART_DIALOG_WIDTH, IO_INPUT_PART_DIALOG_HEIGHT, SWP_SHOWWINDOW);
	m_pDlgDiPart->InitControls_DI_PART();
	m_pDlgDiPart->ShowWindow(SW_HIDE);

	m_pDlgDiLinePart = new CDigitalInputLinePart;
	m_pDlgDiLinePart->Create(IDD_IO_LINE_PART_INPUT_DIALOG, this);
	m_pDlgDiLinePart->SetWindowPos(NULL, 10, 150, IO_INPUT_PART_DIALOG_WIDTH, IO_INPUT_PART_DIALOG_HEIGHT + 300, SWP_SHOWWINDOW);
	m_pDlgDiLinePart->InitControls_DI_LINE_PART();
	m_pDlgDiLinePart->ShowWindow(SW_HIDE);

}

void CIODlg::CreateOutputControl(int main_tab)
{
	CString sIndex;
	int nChCount = 0; // = (GetOutputSize() / 2) + (GetOutputSize() % 2);
	m_TabCtrlDo.SetItemSize(CSize(400, 50));
	m_TabCtrlDo.SetWindowPos(NULL, 770, 110, 750, 1750, SWP_SHOWWINDOW);
	CRect TabRect;
	m_TabCtrlDo.GetClientRect(&TabRect);

	m_pDlgAo = new CAnalogOutput;
	m_pDlgAo->Create(IDD_IO_OUTPUT_AN_DIALOG, &m_TabCtrlDo);
	m_pDlgAo->SetWindowPos(NULL, 10, 1050, TabRect.Width() - 20, TabRect.Height() - 1090, SWP_SHOWWINDOW);
	m_pDlgAo->InitControls_AO();
	m_pDlgAo->ShowWindow(SW_SHOW);

	nChCount = DIGITAL_SUB_OUT_TAB;
	m_pDlgDo = new CDigitalOutput[nChCount];
	for (int nIdx = 0; nIdx < nChCount; nIdx++)
	{
		sIndex.Format("%d", nIdx);
		m_TabCtrlDo.InsertItem(nIdx + 1, _T("CH") + sIndex);
		m_pDlgDo[nIdx].Create(IDD_IO_OUTPUT_DIALOG, &m_TabCtrlDo);
		m_pDlgDo[nIdx].SetWindowPos(NULL, 10, 70, TabRect.Width() - 20, TabRect.Height() - 790, SWP_SHOWWINDOW);
		m_pDlgDo[nIdx].ShowWindow(SW_HIDE);
		m_pDlgDo[nIdx].InitControls_DO(nIdx);
	}
	m_pDlgDo[0].ShowWindow(SW_SHOW);

	m_pDlgDoPart = new CDigitalOutputPart;
	m_pDlgDoPart->Create(IDD_IO_PART_OUTPUT_DIALOG, this);
	m_pDlgDoPart->SetWindowPos(NULL, 750, 150, IO_OUTPUT_PART_DIALOG_WIDTH, IO_OUTPUT_PART_DIALOG_HEIGHT, SWP_SHOWWINDOW);
	m_pDlgDoPart->InitControls_DO_PART();
	m_pDlgDoPart->ShowWindow(SW_HIDE);

	m_pDlgDoLinePart = new CDigitalOutputLinePart;
	m_pDlgDoLinePart->Create(IDD_IO_LINE_PART_OUTPUT_DIALOG, this);
	m_pDlgDoLinePart->SetWindowPos(NULL, 750, 150, IO_OUTPUT_PART_DIALOG_WIDTH, IO_OUTPUT_PART_DIALOG_HEIGHT + 100, SWP_SHOWWINDOW);
	m_pDlgDoLinePart->InitControls_DO_LINE_PART();
	m_pDlgDoLinePart->ShowWindow(SW_HIDE);
}

void CIODlg::OnBnClickedBtnAnalog()
{
	CString sIndex;


	GetDlgItem(IDC_BTN_ANALOG)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_ANALOG)->EnableWindow(false);
	GetDlgItem(IDC_BTN_DIGITAL)->EnableWindow(true);
	GetDlgItem(IDC_BTN_LINE)->EnableWindow(true);

	m_pDlgDoPart->ShowWindow(SW_HIDE);
	m_pDlgDiPart->ShowWindow(SW_HIDE);
	m_pDlgDiLinePart->ShowWindow(SW_HIDE);
	m_pDlgDoLinePart->ShowWindow(SW_HIDE);
	m_TabCtrlDi.ShowWindow(SW_SHOW);
	m_TabCtrlDo.ShowWindow(SW_SHOW);


}

void CIODlg::OnBnClickedBtnDigital()
{
	CString sIndex;

	GetDlgItem(IDC_BTN_DIGITAL)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_DIGITAL)->EnableWindow(false);
	GetDlgItem(IDC_BTN_ANALOG)->EnableWindow(true);
	GetDlgItem(IDC_BTN_LINE)->EnableWindow(true);

	m_TabCtrlDi.ShowWindow(SW_HIDE);
	m_TabCtrlDo.ShowWindow(SW_HIDE);
	m_pDlgDiLinePart->ShowWindow(SW_HIDE);
	m_pDlgDoLinePart->ShowWindow(SW_HIDE);
	m_pDlgDoPart->ShowWindow(SW_SHOW);
	m_pDlgDiPart->ShowWindow(SW_SHOW);

}

void CIODlg::OnBnClickedBtnLine()
{
	GetDlgItem(IDC_BTN_DIGITAL)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_DIGITAL)->EnableWindow(true);
	GetDlgItem(IDC_BTN_ANALOG)->EnableWindow(true);
	GetDlgItem(IDC_BTN_LINE)->EnableWindow(false);

	m_TabCtrlDi.ShowWindow(SW_HIDE);
	m_TabCtrlDo.ShowWindow(SW_HIDE);
	m_pDlgDoPart->ShowWindow(SW_HIDE);
	m_pDlgDiPart->ShowWindow(SW_HIDE);
	m_pDlgDiLinePart->ShowWindow(SW_SHOW);
	m_pDlgDoLinePart->ShowWindow(SW_SHOW);
}

void CIODlg::OnSelchangeTabDigitalIn(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nIdx = m_TabCtrlDi.GetCurSel();

	m_pDlgAi->ShowWindow(SW_SHOW);
	m_pDlgDi[nIdx].ShowWindow(SW_SHOW);

	*pResult = 0;
}

void CIODlg::OnSelchangingTabDigitalIn(NMHDR *pNMHDR, LRESULT *pResult)
{

	int nIdx = m_TabCtrlDi.GetCurSel();

	m_pDlgAi->ShowWindow(SW_SHOW);
	m_pDlgDi[nIdx].ShowWindow(SW_HIDE);
	*pResult = 0;
}

void CIODlg::OnSelchangeTabDigitalOut(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nIdx = m_TabCtrlDo.GetCurSel();

	m_pDlgAo->ShowWindow(SW_SHOW);
	m_pDlgDo[nIdx].ShowWindow(SW_SHOW);
	*pResult = 0;
}

void CIODlg::OnSelchangingTabDigitalOut(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nIdx = m_TabCtrlDo.GetCurSel();
	m_pDlgAo->ShowWindow(SW_SHOW);
	m_pDlgDo[nIdx].ShowWindow(SW_HIDE);

	*pResult = 0;
}

void CIODlg::SetIVNavigatorPgm(int Num)
{
	int iv_navi_data[4] = { 0 };
	int cnt = 0;
	int nRet = -1, ADDR = 11;
	
	
	if (0 <= Num && Num < 16)
	{
		while (1)
		{
			iv_navi_data[cnt] = Num % 2;
			Num = Num / 2;
			cnt++;

			if (Num == 0) 	break;
		}

		//IV NAVIGATOR #1 [0번비트]
		if (iv_navi_data[0])
		{
			nRet = g_pIO->WriteOutputDataBit(ADDR, 4, RUN);
			return_int.Format(_T("%d"), nRet);
		}
		else
		{
			nRet = g_pIO->WriteOutputDataBit(ADDR, 4, RUN_OFF);
			return_int.Format(_T("%d"), nRet);
		}

		if (nRet)
		{
			::AfxMessageBox("Write 실패! ", MB_ICONSTOP);
		}

		//IV NAVIGATOR #2 [1번비트]
		if (iv_navi_data[1])
		{
			nRet = g_pIO->WriteOutputDataBit(ADDR, 5, RUN);
			return_int.Format(_T("%d"), nRet);
		}
		else
		{
			nRet = g_pIO->WriteOutputDataBit(ADDR, 5, RUN_OFF);
			return_int.Format(_T("%d"), nRet);
		}

		if (nRet)
		{
			::AfxMessageBox("Write 실패! ", MB_ICONSTOP);
		}

		//IV NAVIGATOR #3 [2번비트]
		if (iv_navi_data[2])
		{
			nRet = g_pIO->WriteOutputDataBit(ADDR, 6, RUN);
			return_int.Format(_T("%d"), nRet);
		}
		else
		{
			nRet = g_pIO->WriteOutputDataBit(ADDR, 6, RUN_OFF);
			return_int.Format(_T("%d"), nRet);
		}

		if (nRet)
		{
			::AfxMessageBox("Write 실패! ", MB_ICONSTOP);
		}

		//IV NAVIGATOR #4 [3번비트]
		if (iv_navi_data[3])
		{
			nRet = g_pIO->WriteOutputDataBit(ADDR, 7, RUN);
			return_int.Format(_T("%d"), nRet);
		}
		else
		{
			nRet = g_pIO->WriteOutputDataBit(ADDR, 7, RUN_OFF);
			return_int.Format(_T("%d"), nRet);

		}

		if (nRet)
		{
			::AfxMessageBox("Write 실패! ", MB_ICONSTOP);
		}
	}
	else
	{
		AfxMessageBox(_T("숫자의 범위가 잘못 되었습니다 (0~15)"),MB_ICONERROR);
	}
}
/////////////////////////////////// Vacuum Sequence에서 필요로하는 함수 ////////////////////////////////////////
int CIODlg::Close_SourceGate_Check()
{
	int nAddr  = 13;
	int nIndex = 2;
	int nData  = 0;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN OFF TIME OUT ERROR 발생";
		return CLOSE_SOURCEGATE_CHECK_ERROR;
	}
	
	Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN OFF 완료";
	return OPERATION_COMPLETED;
}

int CIODlg::Open_SourceGate_Check()
{
	int nRet = -1;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;

//	double mc_gauge = g_pGauge_IO->m_dPressure_MC;

	if (Is_SourceGate_OpenOn_Check() != VALVE_CLOSE)
	{
		//	ResetEvent(m_Source_Gate_Open_On);

		nAddr  = 13;
		nIndex = 2;
		nData  = 1;

		nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		return_int.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN ON TIME OUT ERROR 발생";
			return OPEN_SOURCEGATE_CHECK_ERROR;
			
		}

		Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN ON 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		nAddr  = 13;
		nIndex = 2;
		nData  = 0;

		nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		return_int.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN OFF TIME OUT ERROR 발생";
			return CLOSE_SOURCEGATE_CHECK_ERROR;
		}

		Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN OFF 완료";
		return OPERATION_COMPLETED;
	}
}

int CIODlg::Open_LLCGateValve() // IO MAP : Y00.05  OPEN SET 1 m_bDigitalOut[5]
{
	////////////////////////////////////////
	//    LLC GATE OPEN INTERLOCK        ///
	////////////////////////////////////////
		// 1. LLC 대기상태 확인 (41B)
		// 2. MFC#1 OFF 확인 
		// 3. MFC#2 OFF 확인 
		// 4. MFC FAST VENT INLET CLOSE 확인 
		// 5. MFC SLOW VENT INLET CLOSE 확인  
		// 6. LLC SLOW ROUGHING V/V CLOSE 확인
		// 7. LLC FAST ROUGHING V/V CLOSE 확인
		// 8. LLC TMP GATE CLOSE 확인 
		// 9. TR GATE CLOSE 확인
		//10. LID CLOSE 확인. ( LID IO 없음 으로 사양삭제 )

	int nRet = -1;

	//if (Get_SLOW_MFC_Status() != VALVE_CLOSE) // MFC#1 OFF 확인
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_LLCGateValve() :: Get_SLOW_MFC_Status() = LLC GATE OPEN 시 SLOW MFC CLOSE INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}
	//if (Get_FAST_MFC_Status() != VALVE_CLOSE) // MFC#2 OFF 확인 
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_LLCGateValve() :: Get_FAST_MFC_Status() = LLC GATE OPEN 시 FAST MFC CLOSE INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}

	if (Get_LLC_Line_ATM_Status() != ATM_STATE)
	{
		Log_str = " Open_LLCGateValve(): Get_LLC_Line_ATM_Status() = LLC GATE OPEN 시 LLC ATM SENSOR STATUS 에러 발생!";
		return  LLCGATEOPEN_LLCLINEATMSTATUSERROR;
	}
	if (Is_FastVentValve_Open() != VALVE_CLOSE) // MFC FAST VENT CLOSE 확인 
	{
		Log_str = "Open_LLCGateValve() :: Is_FastVentValve_Open() = LLC GATE OPEN 시 FAST VENT CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_FASTVENTCLOSEERROR;
	}
	if (Is_SlowVentValve1_Open() != VALVE_CLOSE) //  MFC SLOW VENT CLOSE 확인 
	{
		Log_str = "Open_LLCGateValve() :: Is_SlowVentValve1_Open() = LLC GATE OPEN 시 SLOW VENT CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_SLOWVENTCLOSEERROR;
	}
	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE) // LLC FAST ROUGHING V/V CLOSE 확인
	{
		Log_str = "Open_LLCGateValve() :: Is_LLC_FastRoughValve_Open() = LLC GATE OPEN 시 LLC FAST ROUGH CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_LLCFASTROUGHCLOSEERROR;
	}
	if (Is_LLC_SlowRoughValve_Open() != VALVE_CLOSE) // LLC SLOW ROUGHING V/V CLOSE 확인
	{
		Log_str = "Open_LLCGateValve() :: Is_LLC_SlowRoughValve_Open() = LLC GATE OPEN 시 LLC SLOW ROUGH CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_LLCSLOWROUGHCLOSEERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE) // LLC TMP GATE CLOSE 확인 
	{
		Log_str = "Open_LLCGateValve() :: Is_LLC_TMP_GateValve_Open() = LLC GATE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_LLCTMPGATECLOSEERROR;
	}
	if (Is_TRGateValve_Open() != VALVE_CLOSE) // TR GATE CLOSE 확인
	{
		Log_str = "Open_LLCGateValve() :: Is_TRGateValve_Open() = LLC GATE OPEN 시 TR GATE CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_TRGATECLOSEERROR;
	}

	ResetEvent(m_LLCGate_WriteEvent_Open);

	int nAddr  = 8;
	int nIndex = 6;
	int nData  = 0;

	int nRet_1 = WriteOutputDataBit(nAddr, nIndex, nData);

	nIndex = 5;
	nData  = 1;

	int nRet_2 = WriteOutputDataBit(nAddr, nIndex, nData);
		
	if (!nRet_1 && !nRet_2) 
		nRet = 0;
	else 
		nRet = -1;
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_LLCGateValve() : LLC GATE OPEN WRITE 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLCGateValve() : LLC GATE OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLCGateValve() : LLC GATE OPEN 에러 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_TRGateValve()  // IO MAP Y01.09  OPEN SET 1 m_bDigitalOut[25]
{
// 진공상태 , 대기상태 TR GATE OPEN 인터락 조건 상이. (2가지 상태에 따른 인터락 별도 구성 필요)
//
//	진공상태 인터락 (LLC -> MC)
//
//	대기상태 인터락 (메인터넌스 모드일경우에만 해당)
//
//
//
	int nRet   = -1;
	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;


	double m_mcPressure = 0.0;
	double m_mcPressureRef = 0.0;


	//double m_mc_Pressure = 0.0;
	double m_llcPressure = 0.0;
	//double m_mc_llc_presure = 0.0;
	//double m_llc_mc_presure = 0.0;
	//m_mc_Pressure = (g_pGauge_IO->m_dPressure_MC);
	m_llcPressure = (g_pGauge_IO->m_dPressure_LLC);
	//m_mc_llc_presure = (m_mc_Pressure - m_llc_Presure);
	//
	//m_llc_mc_presure = (m_llc_Presure - m_mc_Pressure);
	// llc - mc = - 값 >>>>>>>>> llc > Mc  (Mc가 llc 보다 고진공)
	// llc - mc = + 값 >>>>>>>>> llc < mc  (llc가 mc 보다 고진공)

	/* TR Gate 조건

	MC 와 LLC 10^-2 차이 이상 -> FALSE
	MC 와 LLC 10^-2 차이 이하 -> TRUE

	m_mcPressure = (g_pGauge_IO->m_dPressure_MC);
	m_mcPressureRef = m_mcPressure * 100;

	if(m_llcPressure < m_mcPressureRef) TRUE

	*/
	
	m_mcPressure = (g_pGauge_IO->m_dPressure_MC);
	m_mcPressureRef = m_mcPressure * 100;




	//if (((0 < m_llc_mc_presure) && (m_llc_mc_presure < 10)) || ((0 > m_llc_mc_presure) && (m_llc_mc_presure > -10)))
	//if ((g_pGauge_IO->m_dPressure_MC < 0.0000009) && (g_pGauge_IO->m_dPressure_LLC < 0.000009))
	//if ((m_llcPressure < m_mcPressureRef) || (g_pGauge_IO->m_dPressure_MC < 0.000009) && (g_pGauge_IO->m_dPressure_LLC < 0.000009))
	if ((m_llcPressure < m_mcPressureRef) || (g_pGauge_IO->m_dPressure_MC < g_pConfig->m_dPressure_Rough_End) && (g_pGauge_IO->m_dPressure_LLC < g_pConfig->m_dPressure_Rough_End))
	{
	
		ResetEvent(m_TRGate_WriteEvent_Open);

		nAddr  = 11;
		nIndex = 1;
		nData  = 1;
		int nRet_ON = WriteOutputDataBit(nAddr, nIndex, nData);				// TR GATE OPEN ON

		nIndex = 2;
		nData  = 0;
		int nRet_OFF = WriteOutputDataBit(nAddr, nIndex, nData);			// TR GATE CLOSE OFF

		if (!nRet_ON && !nRet_OFF) 
			nRet = 0;
		else 
			nRet = -1;
		return_int.Format(_T("%d"), nRet);

		if (nRet != 0)
		{
			Log_str = "Open_TRGateValve() : TR GATE OPEN WRITE 에러 발생";
			return TRGATEOPEN_WRITEERROR;
		}
		
		if (WaitForSingleObject(m_TRGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Open_TRGateValve() : TR GATE OPEN 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Open_TRGateValve() : TR GATE OPEN 에러 발생 (TimeOut)";
			return TRGATEOPEN_TIMEOUTERROR;
		}
	}
	else
	{
		Log_str = "Open_TRGateValve() : TR GATE OPEN 에러 발생 ! 가능 진공 범위가 아님 !";
		return TRGATEOPEN_VACUUMRANGEERROR;
	}
}

int CIODlg::Open_SlowVent_Inlet_Valve1()
{
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_TRGateValve_Open() = SLOW VENT INLET VALVE OPEN 시 TR GATE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_TRGATECLOSEERROR;
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLCGateValve_Open() =  SLOW VENT INLET VALVE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_LLCGATECLOSEERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLC_TMP_GateValve_Open() =  SLOW VENT INLET VALVE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_LLCTMPGATECLOSEERROR;
	}

	if (!g_pConfig->m_dPressure_SlowMFC_Inlet_Valve_Open_Value)
	{
		if (Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
		{
			Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLC_TMP_ForelineValve_Open() = SLOW VENT INLET VALVE OPEN 시 LLC TMP FORELINE OPEN INTERLOCK 에러";
			return SLOWVENTINLETOPEN_LLCFORELINEOPENERROR;
		}
	}
	//if (Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLC_TMP_ForelineValve_Open() = SLOW VENT INLET VALVE OPEN 시 LLC TMP FORELINE CLOSE INTERLOCK 에러";
	//	return SLOWVENTINLETOPEN_LLCFORELINECLOSEERROR;
	//}

	if (!g_pConfig->m_dPressure_SlowMFC_Inlet_Valve_Open_Value)
	{
		if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLC_FastRoughValve_Open() = SLOW VENT INLET VALVE OPEN 시 LLC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
			return SLOWVENTINLETOPEN_LLCFASTROUGHCLOSEERROR;
		}
	}
	if (Is_LLC_SlowRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLC_SlowRoughValve_Open() = SLOW VENT INLET VALVE OPEN 시 LLC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_LLCSLOWROUGHCLOSEERROR;
	}
	if (Is_FastVent_Inlet_Valve1_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_FastVent_Inlet_Valve1_Open() = SLOW VENT INLET VALVE OPEN 시 FAST VENT INLET VALVE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_FASTVENTINLETCLOSEERROR;
	}
	if (Is_FastVent_Outlet_Valve1_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_FastVent_Outlet_Valve1_Open() = SLOW VENT INLET VALVE OPEN 시 FAST VENT OUTLET VALVE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_FASTVENTOUTLETCLOSEERROR;
	}



	ResetEvent(m_SlowVent_Inlet_WriteEvent_Open);

	int nAddr  = 9;
	int nIndex = 2;
	int nData  = 1;


	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() : SLOW VENT INLET OPEN WRITE 에러 발생";
		return SLOWVENTINLETOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_SlowVent_Inlet_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() : SLOW VENT INLET OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() : SLOW VENT INLET OPEN 에러 발생 (TimeOut)";
		return SLOWVENTINLETOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_SlowVent_Outlet_Valve1()
{
	//bool SLOW_VENT_INLET_OPEN = m_bDigitalOut[9];
	int nRet = -1;

//	if (Is_SlowVent_Inlet_Valve1_Open() != VALVE_OPEN)
//	{
//		INTERLOCK_STATE = FALSE;
//		Log_str = "Open_SlowVent_Outlet_Valve1() :: Is_SlowVent_Inlet_Valve1_Open() = SLOW VENT OUTLET VALVE OPEN 시 SLOW VENT INLET VALVE OPEN INTERLOCK 에러";
//		return SLOWVENTOUTLETOPEN_SLOWVENTINLETOPENERROR;
//	}

	int nAddr  = 10;
	int nIndex = 4;
	int nData  = 1;

	nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_SlowVent_Outlet_Valve1() : SLOW VENT OUTLET OPEN WRITE 에러 발생";
		return SLOWVENTOUTLETOPEN_WRITEERROR;
	}
	
	Log_str = "Open_SlowVent_Outlet_Valve1() : SLOW VENT OUTLET OPEN WRITE 완료";
	return OPERATION_COMPLETED;
}

int CIODlg::Open_SlowVentValve1()  // IO MAP : Y01.04  OPEN SET 1 m_bDigitalOut[20] --> OUTLET
									//        : Y00.10  OPEN SET 1 m_bDigitalOut[10] --> INLET
{
	////////////////////////////////////////
		//    SLOW VENT OPEN INTERLOCK       ///
		////////////////////////////////////////
		// 1. LLC GATE CLOSE 확인
		// 2. TR GATE CLOSE 확인
		// 3. LLC TMP GATE V/V CLOSE 확인
		// 4. LLC ROUGHING V/V CLOSE 확인
		// 5. LLC FORELINE V/V CLOSE 확인

	
	int nRet = -1;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;


	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> MAINT MODE
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> OPER MODE
	//                |
	//                |
	//                |
	//                |
	//                |
	//                V
	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> mc_venting_sequence_state = true
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> mc_venting_sequence_state = false

	//if (IO_MODE == OPER_MODE_ON)

	if (!g_pVP->m_bMc_venting_Sequence_State)
	{
		if (Is_TRGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_SlowVentValve1() :: Is_TRGateValve_Open() = SLOW VENT OPEN 시 TR GATE CLOSE INTERLOCK 에러";
			return SLOWVENTOPEN_TRGATEOPENERROR;
		}
	}

	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVentValve1() :: Is_LLCGateValve_Open() = SLOW VENT OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return SLOWVENTOPEN_LLCGATEOPENERROR;
	}
	
	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVentValve1() :: Is_LLC_FastRoughValve_Open() = SLOW VENT OPEN 시 LLC FAST ROUGH GATE CLOSE INTERLOCK 에러";
		return SLOWVENTOPEN_LLCFASTROUGHOPENERROR;
	}


	// VENTING 시, VACUUM 은 계속 진행 함. (FORELINE OPEN 되어 있어야하며, Pumping Rough 시 Close 됨)
	//
	//	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	//	{
	//		INTERLOCK_STATE = FALSE;
	//		Log_str = "Open_SlowVentValve1() :: Is_LLC_TMP_ForelineValve_Open() = SLOW VENT OPEN 시 LLC TMP FORELINE V/V CLOSE INTERLOCK 에러";
	//		return INTERLOCK_STATE;
	//	}
	//if (Get_LLC_DryPump_Status() != DRYPUMP_RUN)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_SlowVentValve1() :: Get_LLC_DryPump_Status() = SLOW VENT OPEN 시 LLC DRY PUMP OFF INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}


	ResetEvent(m_SlowVent_WriteEvent_Open);

	// INLET OPEN WRITE
	nAddr  = 9;
	nIndex = 2;
	nData  = 1;
	int nRet1 = WriteOutputDataBit(nAddr, nIndex, nData);

	// OUTLET OPEN WRITE
	nAddr  = 10;
	nIndex = 4;
	int nRet2 = WriteOutputDataBit(nAddr, nIndex, nData);

	if (!nRet1 && !nRet2) 
		nRet = 0;
	else 
		nRet = -1;
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_SlowVentValve() : SLOW VENT VALVE OPEN WRITE 에러 발생";
		return SLOWVENTOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_SlowVent_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_SlowVentValve() : SLOW VENT VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_SlowVentValve() : SLOW VENT VALVE OPEN 에러 발생 (TimeOut)";
		return SLOWVENTOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_SlowVentValve2()
{
	return TRUE;
}

int CIODlg::Open_FastVent_Inlet_Valve1()
{
	int nRet = 0;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;

	if (Is_TRGateValve_Open() != VALVE_CLOSE) // OPEN 상태
	{
		if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_FastVent_Inlet_Valve1() :: Is_MC_FastRoughValve_Open() = FAST VENT INLET VALVE OPEN 시 MC FAST ROUGH VALVE CLOSE INTERLOCK 에러 (TR OPEN 되어 있음)";
			return FASTVENTINLETOPEN_LLCFASTROUGHCLOSEERROR;
		}
		if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_FastVent_Inlet_Valve1() :: Is_MC_SlowRoughValve_Open() = FAST VENT INLET VALVE OPEN 시 MC SLOW ROUGH VALVE CLOSE INTERLOCK 에러 (TR OPEN 되어 있음)";
			return FASTVENTINLETOPEN_LLCSLOWROUGHCLOSEERROR;
		}
		if (Is_MC_TMP1_GateValve_Open() != VALVE_CLOSED)
		{
			Log_str = "Open_FastVent_Inlet_Valve1() :: Is_MC_TMP1_GateValve_Open() = FAST VENT INLET VALVE OPEN 시 MC TMP CLOSE INTERLOCK 에러 (TR OPEN 되어 있음)";
			return FASTVENTINLETOPEN_MCTMPGATECLOSEERROR;
		}
		if (Is_MC_TMP1_ForelineValve_Open() != VALVE_OPENED)
		{
			Log_str = "Open_FastVent_Inlet_Valve1() :: Is_MC_TMP1_ForelineValve_Open() = FAST VENT INLET VALVE OPEN 시 MC TMP FORELINE OPEN INTERLOCK 에러 (TR OPEN 되어 있음)";
			return FASTVENTINLETOPEN_MCFORELINEGATEOPENERROR;
		}
	}
	
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLCGateValve_Open() =  FAST VENT INLET VALVE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCGATECLOSEERROR;
	}
	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLC_TMP_ForelineValve_Open() = FAST VENT INLET VALVE OPEN 시 LLC TMP FORELINE OPEN INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCFORELINEGATEOPENERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLC_TMP_GateValve_Open() = FAST VENT INLET VALVE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCTMPGATECLOSEERROR;
	}

	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLC_FastRoughValve_Open() = FAST VENT INLET VALVE OPEN 시 LLC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCFASTROUGHCLOSEERROR;
	}
	if (Is_LLC_SlowRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLC_SlowRoughValve_Open() = FAST VENT INLET VALVE OPEN 시 LLC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCSLOWROUGHCLOSEERROR;
	}

	if (Is_SlowVent_Outlet_Valve1_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_SlowVent_Outlet_Valve1_Open() = FAST VENT INLET VALVE OPEN 시 SLOW VENT OUTLET OPEN INTERLOCK 에러";
		return FASTVENTINLETOPEN_SLOWVENTOUTLETCLOSEERROR;
	}
	if (Is_SlowVent_Inlet_Valve1_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_SlowVent_Outlet_Valve1_Open() = FAST VENT INLET VALVE OPEN 시 SLOW VENT INLET OPEN INTERLOCK 에러";
		return FASTVENTINLETOPEN_SLOWVENTINLETCLOSEERROR;
	}

	ResetEvent(m_FastVent_Inlet_WriteEvent_Open);

	nAddr  = 9;
	nIndex = 1;
	nData  = 1;

	nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);  //??????
	if (nRet != 0)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() : FAST VENT INLET OPEN WRITE 에러 발생";
		return FASTVENTINLETOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_FastVent_Inlet_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_FastVentValve() : FAST VENT INLET VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_FastVentValve() : FAST VENT VALVE INLET OPEN 에러 발생 (TimeOut)";
		return FASTVENTINLETOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_FastVent_Outlet_Valve1()
{
	int nRet = 0;

	int nAddr = 0;
	int nIndex = 0;
	int nData = 0;

	//if (Is_FastVent_Inlet_Valve1_Open() != VALVE_OPEN)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_FastVent_Outlet_Valve1() :: Is_FastVent_Inlet_Valve1_Open() = FAST VENT OUTLET VALVE OPEN 시 FAST VENT INLET OPEN INTERLOCK 에러";
	//	return FASTVENTOUTLETOPEN_FASTVENTINLETOPENERROR;
	//}
	
	//if (INTERLOCK_STATE)
	//{
		nAddr  = 10;
		nIndex = 3;
		nData  = 1;

		nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		return_int.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_FastVent_Outlet_Valve1() : FAST VENT OUTLET OPEN WRITE 에러 발생";
			return FASTVENTOUTLETOPEN_WRITEERROR;
		}

		Log_str = "Open_FastVent_Outlet_Valve1() : FAST VENT OUTLET OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	//}
	//else return FASTVENTOUTLETOPENU_NKNOWNERROR;
}

int CIODlg::Open_FastVentValve()// IO MAP : Y01.03  OPEN SET 1 m_bDigitalOut[19] -> OUTLET
								  //        : Y00.09  OPEN SET 1 m_bDigitalOut[9]  -> INLET
{
		////////////////////////////////////////
		//    SLOW VENT OPEN INTERLOCK       ///
		////////////////////////////////////////
		// 1. LLC GATE CLOSE 확인
		// 2. TR GATE CLOSE 확인
		// 3. LLC TMP GATE V/V CLOSE 확인 (OPER MODE)
		// 4. LLC ROUGHING V/V CLOSE 확인
		// 5. LLC FORELINE V/V OPEN 확인
	
	int nRet = 0;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;

	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> MAINT MODE
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> OPER MODE
	//                |
	//                |
	//                |
	//                |
	//                |
	//                V
	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> mc_venting_sequence_state = true
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> mc_venting_sequence_state = false

	//if (IO_MODE == OPER_MODE_ON)
	if (!g_pVP->m_bMc_venting_Sequence_State)
	{
		if (Is_TRGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_FastVentValve() :: Is_TRGateValve_Open() = FAST VENT OPEN 시 TR GATE CLOSE INTERLOCK 에러";
			return FASTVENTOPEN_TRGATECLOSEERROR;
		}
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVentValve() :: Is_LLCGateValve_Open() = FAST VENT OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return FASTVENTOPEN_LLCGATECLOSEERROR;
	}

	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVentValve() :: Is_LLC_FastRoughValve_Open() = FAST VENT OPEN 시 LLC FAST ROUGH GATE CLOSE INTERLOCK 에러";
		return FASTVENTOPEN_LLCFASTROUGHCLOSEERROR;
	}

	// VENTING 시, VACUUM 은 계속 진행 함. (FORELINE OPEN 되어 있어야하며, Pumping Rough 시 Close 됨)
	//
	//if (Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_FastVentValve() :: Is_LLC_TMP_ForelineValve_Open() = FAST VENT OPEN 시 LLC TMP FORELINE V/V CLOSE INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}
	//if (Get_LLC_DryPump_Status() != DRYPUMP_RUN)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_FastVentValve() :: Get_LLC_DryPump_Status() = FAST VENT OPEN 시 LLC DRY PUMP OFF INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}

	ResetEvent(m_FastVent_WriteEvent_Open);

	// INLET OPEN
	nAddr  = 9;
	nIndex = 1;
	nData  = 1;
	int nRet1 = WriteOutputDataBit(nAddr, nIndex, nData);

	// OUTLET OPEN
	nAddr  = 10;
	nIndex = 3;
	nData  = 1;
	int nRet2 = WriteOutputDataBit(nAddr, nIndex, nData);

	if (!nRet1 && !nRet2) 
		nRet = 0;
	else 
		nRet = -1;

	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_FastVentValve() : FAST VENT OPEN WRITE 에러 발생";
		return FASTVENTOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_FastVent_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_FastVentValve() : FAST VENT VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_FastVentValve() : FAST VENT VALVE OPEN 에러 발생 (Time Out)";
		return FASTVENTOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_MC_SlowRoughValve() // IO MAP : Y00.11 OPEN SET 1 m_bDigitalOut[11]
{
	int nRet = 0;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;
	//  게이지 모니터로 변경.

		///////////////////////////////////////////
		//   MC SLOW ROUGH V/V OPEN INTERLOCK    ///
		///////////////////////////////////////////

		// 1. MC DRY PUMP ON  확인					
		// 2. MC FORELINE CLSOE  확인					
		// 3. MC TMP GATE CLOSE 확인			
		// 4. SOURCE GATE CLOSE 확인				
		// 5. FAST ROUGHING VALVE CLOSE 확안.
		// 6. SLOW ROUGHING VALVE CLOSE 확인.
		// 7. TR GATE CLOSE 확인. 
		// 8. PUMPING LINE 압력 확인 확인.
		// 9. PUMPING LINE2 압력 확인 확인.


		/////TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
		// 1. FAST VENT CLOSE 확인
		// 2. SLOW VENT CLOSE 확인.
		// 3. LLC TMP GATE CLOSE  확인.
		// 4. LLC GATE CLOSE 확인.
		// 5. LLC FORELINE V/V CLOSE 확인.
		// 6. LLC ROUGHING V/V CLSOSE 확인.
		///////////////////////////////////////////


	if (Get_MC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Get_MC_DryPump_Status() = MC FAST ROUGH OPEN 시 MC DRY PUMP ON INTERLOCK 에러";
		return MCSLOWROUGHOPEN_DRYPUMPERROR;
	}
	if (Is_MC_TMP1_ForelineValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_MC_TMP1_ForelineValve_Open() = MC FAST ROUGH OPEN 시 MC TMP FORELINE VALVE CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_MCFORELINECLOSEERROR;
	}
	if (Is_MC_TMP1_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_MC_TMP1_GateValve_Open() = MC FAST ROUGH OPEN 시 MC TMP GATE CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_MCTMPGATECLOSEERROR;
	}
	if (Is_LaserSource_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_LaserSource_Open() = MC FAST ROUGH OPEN 시 SOURCE GATE CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_LASERSOURCECLOSEERROR;
	}
	if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_MC_FastRoughValve_Open() = MC FAST ROUGH OPEN 시 MC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_MCFASTROUGHCLOSEERROR;
	}
//	if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSE)
//	{
//		INTERLOCK_STATE = FALSE;
//		Log_str = "Open_MC_SlowRoughValve() :: Is_MC_SlowRoughValve_Open() = MC FAST ROUGH OPEN 시 MC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
//		return INTERLOCK_STATE;
//	}
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_TRGateValve_Open() = MC FAST ROUGH OPEN 시 TR GATE CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_TRGATECLOSEERROR;
	}
	////if() PUMPING_LINE_VALUE = TRUE;
	////if() PUMPING_LINE_VALUE2 = TRUE;

	ResetEvent(m_MCSlowRough_WriteEvent_Open);

	nAddr  = 9;
	nIndex = 3;
	nData  = 1;

	nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_MC_SlowRoughValve() :  MC SLOW ROUGH VALVE OPEN WRITE 에러 발생";
		return MCSLOWROUGHOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_MCSlowRough_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_MC_SlowRoughValve() : MC SLOW ROUGH VALVE OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_MC_SlowRoughValve() :  MC SLOW ROUGH VALVE OPEN 에러 발생 (TimeOut)";
		return MCSLOWROUGHOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_MC_FastRoughValve() // IO MAP : Y00.12 OPEN SET 1 m_bDigitalOut[12]
{

	///////////////////////////////////////////
	//   MC FAST ROUGH V/V OPEN INTERLOCK    ///
	///////////////////////////////////////////

	// 1. MC DRY PUMP ON  확인					
	// 2. MC FORELINE CLSOE  확인					
	// 3. MC TMP GATE CLOSE 확인			
	// 4. SOURCE GATE CLOSE 확인				
	// 5. FAST ROUGHING VALVE CLOSE 확안.
	// 6. SLOW ROUGHING VALVE CLOSE 확인.
	// 7. TR GATE CLOSE 확인. 
	// 8. PUMPING LINE 압력 확인 확인.
	// 9. PUMPING LINE2 압력 확인 확인.


	/////TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
	// 1. FAST VENT CLOSE 확인
	// 2. SLOW VENT CLOSE 확인.
	// 3. LLC TMP GATE CLOSE  확인.
	// 4. LLC GATE CLOSE 확인.
	// 5. LLC FORELINE V/V CLOSE 확인.
	// 6. LLC ROUGHING V/V CLSOSE 확인.
	///////////////////////////////////////////

	if (Get_MC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_MC_FastRoughValve() :: Get_MC_DryPump_Status() = MC FAST ROUGH OPEN 시 MC DRY PUMP ON INTERLOCK 에러";
		return MCFASTROUGHOPEN_DRYPUMPERROR;
	}
	if (Is_MC_TMP1_ForelineValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_MC_TMP1_ForelineValve_Open() = MC FAST ROUGH OPEN 시 MC TMP FORELINE VALVE CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_MCFORELINECLOSEERROR;
	}
	if (Is_MC_TMP1_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_MC_TMP1_GateValve_Open() = MC FAST ROUGH OPEN 시 MC TMP GATE CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_MCTMPGATECLOSEERROR;
	}
	if (Is_LaserSource_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_LaserSource_Open() = MC FAST ROUGH OPEN 시 SOURCE GATE CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_LASERSOURCECLOSEERROR;
	}
	//if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_MC_FastRoughValve() :: Is_MC_FastRoughValve_Open() = MC FAST ROUGH OPEN 시 MC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}
	if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_MC_SlowRoughValve_Open() = MC FAST ROUGH OPEN 시 MC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_MCSLOWROUGHCLOSEERROR;
	}
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_TRGateValve_Open() = MC FAST ROUGH OPEN 시 TR GATE CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_TRGATECLOSEERROR;
	}
	////if() PUMPING_LINE_VALUE = TRUE;
	////if() PUMPING_LINE_VALUE2 = TRUE;

	ResetEvent(m_MCFastRough_WriteEvent_Open);

	int nAddr  = 9;
	int nIndex = 4;
	int nData  = 1;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_MC_FastRoughValve() :  MC FAST ROUGH VALVE OPEN WRITE 에러 발생";
		return MCFASTROUGHOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCFastRough_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_MC_FastRoughValve() : MC FAST ROUGH VALVE OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_MC_FastRoughValve() :  MC FAST ROUGH VALVE OPEN 에러 발생 (TimeOut)";
		return MCFASTROUGHOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_MC_TMP1_GateValve() // IO MAP : Y01.07 OPEN SET 1 m_bDigitalOut[23]
{
	////////////////////////////////////////
	//    MC TMP GATE OPEN INTERLOCK    ///
	////////////////////////////////////////
	
	// 1. MC R.V CLOSE 확인  
	// 2. SOURCE GATE CLOSE 확인  
	// 3. MC 진공도 10^2 TORR 이하 확인 
	// 4. MC DRY PUMP RUN (ON) 확인 
	// 5. TR GATE CLOSE 확인   
	
	///// TR GATE OPEN 시 INTERLOCK 확인 필요 
	// 1. LLC R.V CLOSE 확인			  
	// 2. FAST VENT INLET V/V CLOSE 확인  
	// 3. SLOW VENT INLET V/V CLOSE 확인  
	////////////////////////////////////////


	if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Is_MC_FastRoughValve_Open() = MC TMP GATE OPEN 시 MC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
		return MCTMPGATEOPEN_MCFASTROUGHCLOSEERROR;
	}
	if (Is_LaserSource_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Is_LaserSource_Open() = MC TMP OPEN OPEN 시 SOURCE GATE CLOSE INTERLOCK 에러";
		return MCTMPGATEOPEN_LASERSOURCECLOSEERROR;
	}
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Is_TRGateValve_Open() = MC TMP OPEN OPEN 시 TR GATE CLOSE INTERLOCK 에러";
		return MCTMPGATEOPEN_TRGATECLOSEERROR;
	}
	if (Get_MC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Get_MC_DryPump_Status() = MC TMP OPEN OPEN 시 MC DRY PUMP INTERLOCK 에러";
		return MCTMPGATEOPEN_DRYPUMPERROR;
	}
	if (Is_MC_TMP1_ForelineValve_Open() != VALVE_OPEN)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Is_MC_TMP1_ForelineValve_Open() = MC TMP OPEN OPEN 시 MC FORELINE OPEN INTERLOCK 에러";
		return MCTMPGATEOPEN_MCFORELINEOPENERROR;
	}

	ResetEvent(m_MCTmpGate_WriteEvent_Open);

	int nAddr  = 10;
	int nIndex = 7;
	int nData  = 1;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_MC_TMP1_GateValve() :  MC TMP GATE VALVE OPEN WRIET 에러 발생";
		return MCTMPGATEOPEN_WRITEERROR;
		
	}
	
	if (WaitForSingleObject(m_MCTmpGate_WriteEvent_Open, 5000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_MC_TMP1_GateValve(): MC TMP GATE VALVE OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_MC_TMP1_GateValve() :  MC TMP GATE VALVE OPEN 에러 발생 (TimeOut)";
		return MCTMPGATEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_MC_TMP1_ForelineValve() // IO MAP : Y00.14 OPEN SET 1 m_bDigitalOut[14]
{

	///////////////////////////////////////////
	//    MC FORELINE V/V OPEN INTERLOCK    ///
	///////////////////////////////////////////

	// 1. MC DRY PUMP ON  확인	
	// 2. SOURCE GATE CLOSE 확인
	// 3. TR GATE CLOSE 확인.
	// 4. MC ROUGHING CLSOE  확인									
	// 5. MC FORELINE 진공상태 확인 (51B, 722B 확인)			

	//bool MC_LINE_901P_SET, MC_LINE_722B_SET, MC_ROUGHING_CLOSE, MC_DRY_ON = FALSE;


	/////추가 INTERLOCK 확인 필요 ///////////
	// 4. MC TMP GATE OPEN 확인.
	////////////////////////////////



	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> mc_venting_sequence_state = true
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> mc_venting_sequence_state = false
	//if (IO_MODE == OPER_MODE_ON)
	if (!g_pVP->m_bMc_venting_Sequence_State)
	{
		if (Is_TRGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_TMP1_ForelineValve() :: Is_TRGateValve_Open() = MC TMP FORELINE VALVE OPEN 시 TR GATE CLOSE INTERLOCK 에러";
			return MCFORELINEOPEN_TRGATECLOSEERROR;
		}
	}

	if (Get_MC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :: Get_MC_DryPump_Status() = MC TMP FORELINE VALVE OPEN 시 MC DRY PUMP ON INTERLOCK 에러";
		return MCFORELINEOPEN_DRYPUMPERROR;
	}
	if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :: Is_MC_FastRoughValve_Open() = MC TMP FORELINE VALVE OPEN 시 MC FAST ROUGHING VALVE CLOSE INTERLOCK 에러";
		return MCFORELINEOPEN_MCFASTROUGHCLOSEERROR;
	}
	if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Is_MC_SlowRoughValve_Open() :: Is_MC_SlowRoughValve_Open() = MC TMP FORELINE VALVE OPEN 시 MC SLOW ROUGHING VALVE CLOSE INTERLOCK 에러";
		return MCFORELINEOPEN_MCSLOWROUGHCLOSEERROR;
	}
	if (Is_LaserSource_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :: Is_LaserSource_Open() = MC TMP FORELINE VALVE OPEN 시 MC SOURCE GATE CLOSE INTERLOCK 에러";
		return MCFORELINEOPEN_LASERSOURCECLOSEERROR;
	}

	//if (Is_MC_TMP1_GateValve_Open() != VALVE_OPEN)
	//	{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_MC_TMP1_ForelineValve() :: Is_MC_TMP1_GateValve_Open() = MC TMP FORELINE VALVE OPEN  시 MC TMPE GATE OPEN INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}
	//if () MC_LINE_901P_SET = TRUE;
	//if () MC_LINE_722B_SET = TRUE;


	ResetEvent(m_MCForelineGate_WriteEvent_Open);

	int nAddr  = 9;
	int nIndex = 6;
	int nData  = 1;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE OPEN WRITE 에러 발생";
		return MCFORELINEOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCForelineGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_MC_TMP1_ForelineValve(): MC TMP FORELINE VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE OPEN 에러 발생 (TimeOut)";
		return MCFORELINEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_MC_TMP2_GateValve()
{
	return TRUE;
}

int CIODlg::Open_MC_TMP2_ForelineValve()
{
	return TRUE;
}

int CIODlg::Open_LLC_SlowRoughValve() // IO MAP : Y00.07 OPEN SET 1 m_bDigitalOut[7]
{


	/////////////////////////////////////////////
	//   LLC SLOW ROUGH V/V OPEN INTERLOCK    ///
	/////////////////////////////////////////////

	// 1. LLC DRY PUMP ON  확인					
	// 2. LLC GATE CLSOE  확인					
	// 3. FAST VENT INLET CLOSE 확인				
	// 4. SLOW VENT INLET CLOSE 확인				
	// 5. LLC TMP GATE CLOSE  확인
	// 6. LLC FORLINE V/V CLOSE 확인.
	// 7. TR GATE CLOSE 확인.
	// 8. PUMPING LINE 압력 확인. 901P
	// 9. PUMPING LINE 압력 확인. 722B


	///// TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
	// 1. LLC ROUGHING V/V CLOSE  확인.
	// 2. MC TMP GATE CLOSE 확인.
	// 3. SOURCE GATE CLOSE 확인.
	// 4. MC ROUGHING V/V CLSOSE 확인.
	/////////////////////////////////////////


	if (Get_LLC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Open_LLC_FastRoughValve() = LLC FAST ROUGH VALVE OPEN 시 LLC DRY PUMP ON INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_DRYPUMPERROR;
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLCGateValve_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_LLCGATECLOSEERROR;
	}
	if (Is_FastVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_FastVentValve_Open() = LLC FAST ROUGH VALVE OPEN 시 FAST VENT CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_FASTVENTCLOSEERROR;
	}
	if (Is_SlowVentValve1_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_SlowVentValve1_Open() = LLC FAST ROUGH VALVE OPEN 시 SLOW VENT CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_SLOWVENTCLOSEERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLC_TMP_GateValve_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_LLCTMPGATECLOSEERROR;
	}
	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLC_TMP_ForelineValve_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC TMP FORELINE VALVE CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_LLCFORELINECLOSEERROR;
	}
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_TRGateValve_Open() = LLC FAST ROUGH VALVE OPEN 시 TR GATE CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_TRGATECLOSEERROR;
	}
	
	ResetEvent(m_LLCSlowRough_WriteEvent_Open);
	int nAddr  = 8;
	int nIndex = 7;
	int nData  = 1;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE OPEN WRITE 에러 발생";
		return LLCSLOWROUGHOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCSlowRough_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLC_SlowRoughValve(): LLC SLOW ROUGH VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE OPEN 에러 발생 (TimeOut)";
		return LLCSLOWROUGHOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_LLC_FastRoughValve() // IO MAP : Y00.08 OPEN SET 1 mbDigitalOut[8]
{
	/////////////////////////////////////////////
	//   LLC FAST ROUGH V/V OPEN INTERLOCK    ///
	/////////////////////////////////////////////

	// 1. LLC DRY PUMP ON  확인					
	// 2. LLC GATE CLSOE  확인					
	// 3. FAST VENT INLET CLOSE 확인				
	// 4. SLOW VENT INLET CLOSE 확인				
	// 5. LLC TMP GATE CLOSE  확인
	// 6. LLC FORLINE V/V CLOSE 확인.
	// 7. TR GATE CLOSE 확인.
	// 8. PUMPING LINE 압력 확인. 901P
	// 9. PUMPING LINE 압력 확인. 722B


	////TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
	// 1. LLC ROUGHING V/V CLOSE  확인.
	// 2. MC TMP GATE CLOSE 확인.
	// 3. SOURCE GATE CLOSE 확인.
	// 4. MC ROUGHING V/V CLSOSE 확인.
	/////////////////////////////////////////

	
	if (Get_LLC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Open_LLC_FastRoughValve() = LLC FAST ROUGH VALVE OPEN 시 LLC DRY PUMP ON INTERLOCK 에러";
		return LLCFASTROUGHOPEN_DRYPUMPERROR;
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLCGateValve_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_LLCGATECLOSEERROR;
	}
	if (Is_FastVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_FastVentValve_Open() = LLC FAST ROUGH VALVE OPEN 시 FAST VENT CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_FASTVENTCLOSEERROR;
	}
	if (Is_SlowVentValve1_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_SlowVentValve1_Open() = LLC FAST ROUGH VALVE OPEN 시 SLOW VENT CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_SLOWVENTCLOSEERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLC_TMP_GateValve_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_LLCTMPGATECLOSEERROR;
	}
	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLC_TMP_ForelineValve_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC TMP FORELINE VALVE CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_LLCFORELINECLOSEERROR;
	}
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_TRGateValve_Open() = LLC FAST ROUGH VALVE OPEN 시 TR GATE CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_TRGATECLOSEERROR;
	}

	ResetEvent(m_LLCFastRough_WriteEvent_Open);

	int nAddr  = 9;
	int nIndex = 0;
	int nData  = 1;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE OPEN WRITE 에러 발생";
		return LLCFASTROUGHOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCFastRough_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLC_FastRoughValve(): LLC FAST ROUGH VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE OPEN 에러 발생";
		return LLCFASTROUGHOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_LLC_TMP_GateValve() // IO MAP Y01.06 OPEN SET 1 m_bDigitalOut[22]
{
	////////////////////////////////////////
	//    LLC TMP GATE OPEN INTERLOCK    ///
	////////////////////////////////////////

	// 1. LLC R.V CLOSE  확인 
	// 2. FAST VENT INLET V/V CLOSE 확인  
	// 3. SLOW VENT INLET V/V CLOSE 확인  
	// 4. LLC GATE CLOSE 확인  
	// 5. LLC 진공도 10^2 TORR 이하 확인  
	// 6. LLC DRY PUMP RUN (ON) 확인  
	// 7. TR GATE CLOSE 확인   
	// 8. LLC FORELINE V/V OPEN 확인.


	///////  TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
	// 2. MC R.V CLOSE  확인  
	// 6. SOURCE GATE CLOSE 확인 
	/////////////////////////////////////////////


	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_LLC_FastRoughValve_Open() = LLC TMP GATE OPEN 시 LLC ROUGH VALVE CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_LLCFASTROUGHCLOSEERROR;
	}
	if (Is_SlowVentValve1_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_SlowVentValve1_Open() = LLC TMP GATE OPEN 시 SLOW VENT VALVE CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_SLOWVENTCLOSEERROR;
	}
	if (Is_FastVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_FastVentValve_Open() = LLC TMP GATE OPEN 시 FAST VENT VALVE CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_FASTVENTCLOSEERROR;
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_LLCGateValve_Open() = LLC TMP GATE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_LLCGATECLOSEERROR;
	}
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_TRGateValve_Open() = LLC TMP GATE OPEN 시 TR GATE CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_TRGATECLOSEERROR;
	}
	if (Get_LLC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Get_LLC_DryPump_Status() = LLC TMP GATE OPEN 시 LLC DRY PUMP ON INTERLOCK 에러";
		return LLCTMPGATEOPEN_DRYPUMPERROR;
	}
	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_OPEN)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_LLC_TMP_ForelineValve_Open() = LLC TMP GATE OPEN 시 LLC TMP FORELINE OPEN INTERLOCK 에러";
		return LLCTMPGATEOPEN_LLCFORELINEOPENERROR;
	}

	ResetEvent(m_LLCTmpGate_WriteEvent_Open);

	int nAddr  = 10;
	int nIndex = 6;
	int nData  = 1;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_LLC_TMP_GateValve() :  LLC TMP GATE VALVE OPEN WRITE 에러 발생";
		return LLCTMPGATEOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCTmpGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLC_TMP_GateValve(): LLC TMP GATE VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLC_TMP_GateValve() :  LLC TMP GATE VALVE OPEN 에러 발생 (TimeOut)";
		return LLCTMPGATEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_LLC_TMP_ForelineValve() // IO MAP Y00.13 OPEN SET 1 m_bDigitalOut[13]
{
	///////////////////////////////////////////
	//   LLC FORELINE V/V OPEN INTERLOCK    ///
	///////////////////////////////////////////

	// 1. LLC DRY PUMP ON  확인										
	// 2. LLC FAST ROUGHING CLSOE 확인	
	// 3. LLC SLOW ROUGHING CLOSE 확인.
	// 4. LLC GATE CLOSE 확인.
	// 5. TR GATE CLOSE 확인.
	// 6. LLC TMP GATE CLOSE 확인.
	// 7. SLOW VENT CLOSE 확인.
	// 8. FAST VENT CLOSE 확인.


	// LLC FORELINE 진공상태 확인 (51B, 722B 확인)				>>>>>>  LLC_LINE_901P_SET , LLC_LINE_722B_SET

	//if (IO_MODE == OPER_MODE_ON)
	if (!g_pVP->m_bMc_venting_Sequence_State)
	{
		if (Is_TRGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_TMP_ForelineValve() :: Is_TRGateValve_Open() = LLC TMP FORELINE V/V OPEN 시 TR GATE CLOSE INTERLOCK 에러";
			return LLCFORELINEOPEN_TRGATECLOSEERROR;
		}
	}

	if (Get_LLC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Get_LLC_DryPump_Status() = LLC TMP FORELINE V/V OPEN 시 LLC DRY PUMP ON INTERLOCK 에러";
		return LLCFORELINEOPEN_DRYPUMPERROR;
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Is_LLCGateValve_Open() = LLC TMP FORELINE V/V OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return LLCFORELINEOPEN_LLCGATECLOSEERROR;
	}

	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Open_LLC_TMP_GateValve() = LLC TMP FORELINE V/V OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return LLCFORELINEOPEN_LLCTMPGATECLOSEERROR;
	}
	if (Is_SlowVentValve1_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Is_SlowVentValve1_Open() = LLC TMP FORELINE V/V OPEN 시 SLOW VENT GATE CLOSE INTERLOCK 에러";
		return LLCFORELINEOPEN_SLOWVENTCLOSEERROR;
	}
	if (Is_FastVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Is_FastVentValve_Open() = LLC TMP FORELINE V/V OPEN 시 FAST VENT GATE CLOSE INTERLOCK 에러";
		return LLCFORELINEOPEN_FASTVENTCLOSEERROR;
	}
	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Is_LLC_FastRoughValve_Open() = LLC TMP FORELINE V/V OPEN 시 LLC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
		return LLCFORELINEOPEN_LLCFASTROUGHCLOSEERROR;
	}
	if (Is_LLC_SlowRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Is_LLC_SlowRoughValve_Open() = LLC TMP FORELINE V/V OPEN 시 LLC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
		return LLCFORELINEOPEN_LLCSLOWROUGHCLOSEERROR;
	}
	

	ResetEvent(m_LLCForelineGate_WriteEvent_Open);

	int nAddr  = 9;
	int nIndex = 5;
	int nData  = 1;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :  LLC TMP FORELINE VALVE OPEN WRITE 에러 발생";
		return LLCFORELINEOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCForelineGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLC_TMP_ForelineValve(): LLC TMP FORELINE VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :  LLC TMP FORELINE VALVE OPEN 에러 발생 (TimeOut)";
		return LLCFORELINEOPEN_TIMEOUTERROR;
	}
}



int CIODlg::Close_LLCGateValve() // IO MAP Y00.06 CLOSE SET 1 m_bDigitalOut[6]
{
	int nRet = 0;
	ResetEvent(m_LLCGate_WriteEvent);

	int nAddr  = 8;
	int nIndex = 5;
	int nData  = 0;

	int nRet1 = WriteOutputDataBit(nAddr, nIndex, nData);

	nIndex = 6;
	nData  = 1;
	int nRet2 = WriteOutputDataBit(nAddr, nIndex, nData);
	
	if (!nRet1 && !nRet2) 
		nRet = IO_WRITE_COMPLETE;
	else 
		nRet = LLCGATECLOSE_WRITEERROR;

	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLCGateValve(): LLC GATE VALVE CLOSE WRITE 에러 발생";
		return  nRet;
	}
	
	if (WaitForSingleObject(m_LLCGate_WriteEvent, 5000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLCGateValve() : LLC GATE VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLCGateValve(): LLC GATE VALVE CLOSE 에러 발생 (TimeOut)";
		return  LLCGATECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_TRGateValve() // IO MAP Y01.10 CLOSE SET 1 m_bDIgitalOut[26]
{
	bool bVacRobotRetracted = m_bDigitalIn[88];

	if (bVacRobotRetracted == true)
	{
		ResetEvent(m_TRGate_WriteEvent);

		int nRet   = 0;
		int nAddr  = 11;
		int nIndex = 1;
		int nData  = 0;

		int nRet_OFF = WriteOutputDataBit(nAddr, nIndex, nData);			// TR GATE OPEN OFF

		nIndex = 2;
		nData  = 1;
		int nRet_ON = WriteOutputDataBit(nAddr, nIndex, nData);  // TR GATE CLOSE ON

		if (!nRet_ON && !nRet_OFF) 
			nRet = IO_WRITE_COMPLETE;
		else 
			nRet = TRGATECLOSE_WRITEERROR;
		return_int.Format(_T("%d"), nRet);

		if (nRet != 0)
		{
			Log_str = "Close_TRGateValve() : TR GATE CLOSE WRITE 에러 발생";
			return TRGATECLOSE_WRITEERROR;
		}

		if (WaitForSingleObject(m_TRGate_WriteEvent, 5000) == WAIT_OBJECT_0)
		{
			Log_str = "Close_TRGateValve() : TR GATE VALVE CLOSE 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Close_TRGateValve(): TR GATE VALVE CLOSE 에러 발생 (TimeOut)";
			return  TRGATECLOSE_TIMEOUTERROR;
		}
	}
	else
	{
		Log_str = "Close_TRGateValve() : VAC ROBOT ARM RETRACT 상태 확인 요망. Retract가 아니므로 Error발생";
		return TRGATECLOSE_VAC_ARM_RETRACT_ERROr;
	}
}

int CIODlg::Close_SlowVent_Inlet_Valve1()// IO MAP : Y00.10 CLOSE SET 0 m_bDigitalOut[10]  ---> INLET
{
	ResetEvent(m_SlowVent_Inlet_WriteEvent_Close);

	int nRet = 0;

	int nAddr  = 9;
	int nIndex = 2;
	int nData  = 0;

	//bool INTERLOCK_STATE = TRUE;

	//if (Is_SlowVent_Outlet_Valve1_Open() != VALVE_OPEN)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Close_SlowVent_Inlet_Valve1() :: Is_SlowVent_Outlet_Valve1_Open() = SLOW VENT INLET CLOSE 시 SLOW VENT OUTLET CLOSE INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}

	nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() :  SLOW VENT INLET CLOSE WRITE 에러 발생";
		return SLOWVENTINLETCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_SlowVent_Inlet_WriteEvent_Close, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() : SLOW VENT INLET CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() :  SLOW VENT INLET CLOSE 에러 발생(TimeOut)";
		return SLOWVENTINLETCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_SlowVent_Outlet_Valve1()// IO MAP : Y00.10 CLOSE SET 0 m_bDigitalOut[10]  ---> INLET
{
	if (Is_SlowVent_Inlet_Valve1_Open() != VALVE_CLOSE)
	{
		Log_str = "Close_SlowVent_Outlet_Valve1() :: Is_SlowVent_Inlet_Valve1_Open() = SLOW VENT OUTLET CLOSE 시 SLOW VENT INLET CLOSE INTERLOCK 에러";
		return SLOWVENTOUTLETCLOSE_SlOWVENTINLETCLOSEERROR;
	}

	int nRet = 0;

	int nAddr  = 10;
	int nIndex = 4;
	int nData  = 0;

	nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() :   SLOW VENT INLET CLOSE WRITE 에러 발생";
		return SLOWVENTOUTLETCLOSE_WRITEERROR;
	}
	
	Log_str = "Close_SlowVent_Inlet_Valve1() : SLOW VENT INLET CLOSE 완료";
	return OPERATION_COMPLETED;
}

int CIODlg::Close_SlowVentValve1()// IO MAP : Y00.10 CLOSE SET 0 m_bDigitalOut[10]  ---> INLET
								  //	      : Y01.04 CLOSE SET 0 m_bDigitalOut[20]  ---> OUTLET
{
	ResetEvent(m_SlowVent_WriteEvent);

	int ADDR1, bitdex1, data1 = -1;
	int nRet = -1;

	

	//OUTLET
	int nAddr  = 10;
	int nIndex = 4;
	int nData  = 0;

	int nRet2 = WriteOutputDataBit(nAddr, nIndex, nData);

	//INLET
	nAddr  = 9;
	nIndex = 2;
	nData  = 0;
	int nRet1 = WriteOutputDataBit(nAddr, nIndex, nData);

	if (!nRet1 && !nRet2) 
		nRet = IO_WRITE_COMPLETE;
	else 
		nRet = SLOWVENTCLOSE_WRITEERROR;

	return_int.Format(_T("%d"), nRet);

	if (nRet != 0)
	{
		Log_str = "Close_SlowVentValve1(): SLOW VENT CLOSE WRITE 에러 발생";
		return nRet;
	}
	
	//int ret = WaitEvent(3000);
	if (WaitForSingleObject(m_SlowVent_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_SlowVentValve1() : SLOW VENT CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_SlowVentValve1() : SLOW VENT CLOSE 에러 발생 (TimeOut)";
		return SLOWVENTCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_SlowVentValve2()
{
	return TRUE;
}

int CIODlg::Close_FastVent_Inlet_Valve()// IO MAP : Y00.10 CLOSE SET 0 m_bDigitalOut[10]  ---> INLET
{
	ResetEvent(m_FastVent_Inlet_WriteEvent_Close);

	int nAddr  = 9;
	int nIndex = 1;
	int nData  = 0;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_FastVent_Inlet_Valve() :   FAST VENT INLET CLOSE WRITE 에러 발생";
		return FASTVENTINLETCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_FastVent_Inlet_WriteEvent_Close, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_FastVent_Inlet_Valve() : FAST VENT Inlet CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_FastVent_Inlet_Valve() : FAST VENT Inlet CLOSE 에러 발생(TimeOut)";
		return FASTVENTINLETCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_FastVent_Outlet_Valve()// IO MAP : Y00.10 CLOSE SET 0 m_bDigitalOut[10]  ---> INLET
{
	int nAddr  = 10;
	int nIndex = 3;
	int nData  = 0;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() :   FAST VENT OUTLET CLOSE WRITE 에러 발생";
		return FASTVENTOUTLETCLOSE_WRITEERROR;
	}
	
	Log_str = "Close_SlowVent_Inlet_Valve1() : FAST VENT OUTLET CLOSE 완료";
	return OPERATION_COMPLETED;
}

int CIODlg::Close_FastVentValve()// IO MAP : Y00.09 CLOSE SET 0 m_bDigitalOut[9]  ----> INLET
								   //        : Y01.03 CLOSE SET 0 m_bdigitalOut[19] ----> OUTLET 
{
	ResetEvent(m_FastVent_WriteEvent);

	int nRet = 0;

	//OUTLET
	int nAddr  = 10;
	int nIndex = 3;
	int nData  = 0;

	int nRet2 = WriteOutputDataBit(nAddr, nIndex, nData);

	//INLET
	nAddr  = 9;
	nIndex = 1;
	nData  = 0;

	int nRet1 = WriteOutputDataBit(nAddr, nIndex, nData);

	if (!nRet1 && !nRet2) 
		nRet = 0;
	else 
		nRet = -1;
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_FastVentValve1()_OUTLET : FAST VENT WRITE CLOSE 에러 발생";
		return FASTVENTCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_FastVent_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_FastVentValve() : FAST VENT CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_FastVentValve() : FAST VENT CLOSE 에러 발생 (TimeOut)";
		return FASTVENTCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_MC_SlowRoughValve()
{
	int nAddr  = 9;
    int nIndex = 3;
	int nData  = 0;

	ResetEvent(m_MCSlowRough_WriteEvent);

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_MC_FastRoughValve() :  MC SLOW ROUGH VALVE WRITE CLOSE 에러 발생";
		return MCSLOWROUGHCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCSlowRough_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_MC_SlowRoughValve() :  MC SLOW ROUGH VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_MC_SlowRoughValve() : MC SLOW ROUGH VALVE CLOSE 에러 발생 (TimeOut)";
		return MCSLOWROUGHCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_MC_FastRoughValve() // IO MAP : Y00.12 CLOSE SET 0 m_bDigitalOut[12] 
{
	int nAddr  = 9;
	int nIndex = 4;
	int nData  = 0;

	ResetEvent(m_MCFastRough_WriteEvent);

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_MC_FastRoughValve() :  MC FAST ROUGH VALVE WRITE CLOSE 에러 발생";
		return MCFASTROUGHCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCFastRough_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_MC_FastRoughValve() :  MC FAST ROUGH VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_MC_FastRoughValve() :  MC FAST ROUGH VALVE CLOSE 에러 발생 (TimeOut)";
		return MCFASTROUGHCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_MC_TMP1_GateValve() // IO MAP : Y01.07 CLOSE SET 0 m_bDigitalOut[23]
{
	int nAddr  = 10;
	int nIndex = 7;
	int nData  = 0;

	ResetEvent(m_MCTmpGate_WriteEvent);

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_MC_TMP1_GateValve() :  MC TMP GATE VALVE WRITE CLOSE 에러 발생";
		return MCTMPGATECLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCTmpGate_WriteEvent, 5000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_MC_TMP1_GateValve() :  MC TMP GATE VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_MC_TMP1_GateValve() :  MC TMP GATE VALVE CLOSE 에러 발생 (TimeOut)";
		return MCTMPGATECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_MC_TMP1_ForelineValve()  // IO MAP : Y00.14 CLOSE SET 0 m_bDigitalOut[14] 
{
	int nAddr  = 9;
	int nIndex = 6;
	int nData  = 0;

	ResetEvent(m_MCForelineGate_WriteEvent);

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE WRITE CLOSE 에러 발생";
		return MCFORELINECLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCForelineGate_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE CLOSE 발생 (TimeOut)";
		return MCFORELINECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_MC_TMP2_GateValve()
{
	return TRUE;
}

int CIODlg::Close_MC_TMP2_ForelineValve()
{
	return TRUE;
}

int CIODlg::Close_LLC_SlowRoughValve()
{
	int nAddr  = 8;
	int nIndex = 7;
	int nData  = 0;

	ResetEvent(m_LLCSlowRough_WriteEvent);

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE WRITE CLOSE 에러 발생";
		return LLCSLOWROUGHCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCSlowRough_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE CLOSE  완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE CLOSE  발생 (TimeOut)";
		return LLCSLOWROUGHCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_LLC_FastRoughValve() // IO MAP : Y00.08 CLOSE SET 0 m_bDigitalOut[8]
{
	int nAddr  = 9;
	int nIndex = 0;
	int nData  = 0;
	
	ResetEvent(m_LLCFastRough_WriteEvent);

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE WRITE CLOSE 에러 발생";
		return LLCFASTROUGHCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCFastRough_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE CLOSE  완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE CLOSE  에러 발생 (TimeOut)";
		return LLCFASTROUGHCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_LLC_TMP_GateValve() // IO MAP : Y01.06 CLOSE SET 0 m_bDigitalOut[22]
{
	int nAddr  = 10;
	int nIndex = 6;
	int nData  = 0;

	ResetEvent(m_LLCTmpGate_WriteEvent);

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLC_TMP_GateValve() :  LLC TMP GATE VALVE WRITE CLOSE 에러 발생";
		return LLCTMPGATECLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCTmpGate_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLC_TMP_GateValve() :  LLC TMP GATE VALVE CLOSE  완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLC_TMP_GateValve() :  LLC TMP GATE VALVE CLOSE 에러 발생 (TimeOut)";
		return LLCTMPGATECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_LLC_TMP_ForelineValve() // IO MAP : Y00.13 CLOSE SET 0 m_bDigitalOut[13]
{
	int nAddr  = 9;
	int nIndex = 5;
	int nData  = 0;

	ResetEvent(m_LLCForelineGate_WriteEvent);

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	return_int.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLC_TMP_ForelineValve()  :  LLC TMP FORELINE VALVE WRITE CLOSE 에러 발생";
		return LLCFORELINECLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCForelineGate_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLC_TMP_ForelineValve() : LLC TMP FORELINE VALVE CLOSE  완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLC_TMP_ForelineValve() :  LLC TMP FORELINE VALVE CLOSE 에러 발생(TimeOut)";
		return LLCFORELINECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Is_LLCGateValve_Open()
{
	int ret = 0;
	
	bool bGateOpen  = m_bDigitalIn[LL_GATE_OPEN];
	bool bGateClose = m_bDigitalIn[LL_GATE_CLOSE];

	ret = Valve_Check(bGateOpen, bGateClose);

	return ret;
}

int CIODlg::Is_TRGateValve_Open()
{
	int ret = 0;

	bool bGateOpen  = m_bDigitalIn[TR_GATE_OPEN];
	bool bGateClose = m_bDigitalIn[TR_GATE_CLOSE];

	ret = Valve_Check(bGateOpen, bGateClose);

	return ret;
}

int CIODlg::Is_SlowVent_Inlet_Valve1_Open()
{
	int ret = 0;

	if (m_bDigitalOut[SLOW_VENT_INLET] == true) 
		ret = VALVE_OPEN;
	else  
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_SlowVent_Outlet_Valve1_Open()
{
	int ret = 0;

	if (m_bDigitalOut[SLOW_VENT_OUTLET] == true) 
		ret = VALVE_OPEN;
	else  
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_SlowVentValve1_Open()
{
	int ret = 0;

	bool bSlowVentInlet  = m_bDigitalOut[SLOW_VENT_INLET];
	bool bSlowVentOutlet = m_bDigitalOut[SLOW_VENT_OUTLET];

	if (bSlowVentInlet == false && bSlowVentOutlet == false)
		ret = VALVE_CLOSE;
	else if (bSlowVentInlet == true || bSlowVentOutlet == true)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_OPEN;

	return ret;
}

int CIODlg::Is_SlowVentValve2_Open()
{
	int ret = 0;



	return ret;
}

int CIODlg::Is_FastVent_Inlet_Valve1_Open()
{
	int ret = 0;

	bool bState = m_bDigitalOut[FAST_VENT_INLET];

	if (bState) 
		ret = VALVE_OPEN;
	else  
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_FastVent_Outlet_Valve1_Open()
{
	int ret = 0;

	bool bState = m_bDigitalOut[FAST_VENT_OUTLET];

	if (bState) 
		ret = VALVE_OPEN;
	else  
		ret = VALVE_CLOSE;

	return ret;
}



int CIODlg::Is_FastVentValve_Open()
{
	int ret = 0;

	bool bFastVentInlet = m_bDigitalOut[FAST_VENT_INLET];
	bool bFastVentOutlet = m_bDigitalOut[FAST_VENT_OUTLET];

	if ((bFastVentInlet == false) && (bFastVentOutlet == false))
		ret = VALVE_CLOSE;
	else if ((bFastVentInlet == true) || (bFastVentOutlet == true))
		ret = VALVE_OPEN;
	else 
		ret = VALVE_OPEN;

	return ret;
}

int CIODlg::Is_MC_SlowRoughValve_Open()
{
	int ret = 0;
	
	bool bMcSlowRoughState = m_bDigitalOut[MC_SLOW_ROUGH]; //1 open , 0 close
	//int MC_SLOW_ROUGH_STATE = m_bDigitalOut[11]; 

	if (bMcSlowRoughState != false)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_MC_FastRoughValve_Open()
{
	int ret = 0;

	bool bMcFastRoughValveOpen = m_bDigitalIn[MC_FAST_ROUGHING_OPEN];
	bool bMcRoughValveClose = m_bDigitalIn[MC_ROUGHING_CLOSE];

	ret = Valve_Check(bMcFastRoughValveOpen, bMcRoughValveClose);

	return ret;
}

int CIODlg::Is_MC_TMP1_GateValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[MC_TMP_GATE_OPEN];
	bool bClosed = m_bDigitalIn[MC_TMP_GATE_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}

int CIODlg::Is_MC_TMP1_ForelineValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[MC_FORELINE_OPEN];
	bool bClosed = m_bDigitalIn[MC_FORELINE_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}

int CIODlg::Is_MC_TMP2_GateValve_Open()
{
	int ret = 0;



	return ret;
}

int CIODlg::Is_MC_TMP2_ForelineValve_Open()
{
	int ret = 0;



	return ret;
}

int CIODlg::Is_LLC_SlowRoughValve_Open()
{
	int ret = 0;

	// 게이지 값으로 ON / OFF 로 대체.
	bool bState = m_bDigitalOut[LL_SLOW_ROUGH_VV]; //1 open , 0 close

	if (bState != false)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_LLC_FastRoughValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[LL_FAST_ROUGHING_OPEN];
	bool bClosed = m_bDigitalIn[LL_ROUGHING_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}

int CIODlg::Is_LLC_TMP_GateValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[LL_TMP_GATE_OPEN];
	bool bClosed = m_bDigitalIn[LL_TMP_GATE_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}

int CIODlg::Is_LLC_TMP_ForelineValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[LL_FORELINE_OPEN];
	bool bClosed = m_bDigitalIn[LL_FORELINE_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;

	//////////////////////
	// 값 테스트 ( kjh )
	//////////////////////

	//	CString llc_forline_open_str_;
	//	CString llc_forline_open_str_2;
	//	llc_forline_open_str_.Format("%d", m_bDigitalIn[Din = LL_FORELINE_OPEN]);
	//	llc_forline_open_str_2.Format("%d", LLC_TMP_FORELINE_VALVE_OPEN);
	//	CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_OPEN_STATE 1::" + llc_forline_open_str_)));
	//	CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_OPEN_STATE 2::" + llc_forline_open_str_2)));
	//
	//	CString llc_forline_close_str_;
	//	CString llc_forline_close_str_2;
	//	llc_forline_close_str_.Format("%d", m_bDigitalIn[Din = LL_FORELINE_CLOSE]);
	//	llc_forline_close_str_2.Format("%d", LLC_TMP_FORELINE_VALVE_CLOSE);
	//	CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_CLOSE_STATE 1::" + llc_forline_close_str_)));
	//	CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_CLOSE_STATE 2::" + llc_forline_close_str_2)));
	
	//	if (Pre_open != Post_open)
	//	{
	//		Post_open = Pre_open;
	//		CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_OPEN_STATE_변함 ::" + llc_forline_open_str_)));
	//	}
	//	if (Pre_close != Post_close)
	//	{
	//		Post_close = Pre_close;
	//		CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_CLOSE_STATE_변함::" + llc_forline_close_str_)));
	//	}
	
	//
	//	CString llc_forline_ret_str;
	//	llc_forline_ret_str.Format("%d", ret);
	//	CECommon::SaveLogFile("LLC_FORELINE_STATE_VAUE", _T((LPSTR)(LPCTSTR)("LLC_FORLIEN_RESULT_STATE::" + llc_forline_ret_str)));


}

int CIODlg::Is_LaserSource_Open()
{
	int ret = 0;
	
	bool bOpened = m_bDigitalIn[LASER_VALVE_OPEN_STATUS];
	bool bClosed = m_bDigitalIn[LASER_VALVE_CLOSE_STATUS];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}


int CIODlg::Is_Water_Valve_Open()
{
	int ret = 0;

	bool bSupplyValve = m_bDigitalOut[WATER_SUPPLY_VALVE];
	bool bReturnValve = m_bDigitalOut[WATER_RETURN_VALVE];

	if (bSupplyValve && bReturnValve)  
		ret = RUN;
	else 
		ret = RUN_OFF;

	return ret;
}

int CIODlg::Is_Mask_OnChuck()
{
	int ret = -1;

	bool bMaskOnChuck = m_bDigitalIn[MAIN_MASK_ON];
	bool bTiltSensor1 = m_bDigitalIn[MAIN_MASK_SLANT1];
	bool bTiltSensor2 = m_bDigitalIn[MAIN_MASK_SLANT2];

	if (bMaskOnChuck) 
		ret = MASK_ON;
	if (!bMaskOnChuck) 
		ret = MASK_NONE;
	if (bTiltSensor1) 
		ret = MASK_SLANT;
	if (bTiltSensor2) 
		ret = MASK_SLANT;

	return ret;
}

int CIODlg::Is_Mask_Check_OnChuck()
{
	int ret = -1;

	bool bMaskOnChuck = m_bDigitalIn[MAIN_MASK_ON];
	if (bMaskOnChuck)
		ret = MASK_ON;
	else 
		ret = MASK_NONE;

	return ret;
}

int CIODlg::Is_Mask_OnVMTR()
{
	int ret = -1;

	bool bCamSensor = m_bDigitalIn[MC_MASK_OK_CAM_SENSOR];

	if (bCamSensor) 
		ret = MASK_ON;
	else
		ret = MASK_NONE;
	
	return ret;
}

int CIODlg::Is_Mask_OnVMTR_LLC()
{
	int ret = -1;

	bool bCamSensor = m_bDigitalIn[LLC_MASK_OK_CAM_SENSOR];

	if (bCamSensor) 
		ret = MASK_ON;
	else
		ret = MASK_NONE;

	return ret;
}

int CIODlg::Send_TriggerToCamSensor()
{
	int nRet = 0;

	int nAddr = 13;
	int nIndex = 7;
	
	int nTriggerStart = WriteOutputDataBit(nAddr, nIndex, 1); // 정상 WRITE 면 retrun 0 임.
	Sleep(400); // 400ms  지연
	int nTriggerEnd = WriteOutputDataBit(nAddr, nIndex, 0);
	
	if (!nTriggerStart && !nTriggerEnd)
		nRet = TRIGGER_ON;
	else 
		nRet = TRIGGER_ERROR;

	return nRet;
}

int CIODlg::SetTriggerCamSensor(BOOL bTriggerOn)
{
	int ret = 0;

	int nAddr  = 13;
	int nIndex = 7;

	ret = WriteOutputDataBit(nAddr, nIndex, bTriggerOn); // 정상 WRITE 면 retrun 0 임.

	return ret;
}

int CIODlg::Is_Mask_OnLLC()
{
	int ret = -1;

	bool bMaskOnChuck = m_bDigitalIn[LLC_MASK_ON];
	bool bTilt1 = m_bDigitalIn[LLC_MASK_SLANT1];
	bool bTilt2 = m_bDigitalIn[LLC_MASK_SLANT2];

	if (bMaskOnChuck) 
		ret = MASK_ON;
	if (!bMaskOnChuck) 
		ret = MASK_NONE;
	if (bTilt1) 
		ret = MASK_SLANT;
	if (bTilt2) 
		ret = MASK_SLANT;
	
	return ret;
}

int CIODlg::Is_Mask_Check_OnLLC()
{
	int ret = -1;

	bool bMaskOnChuck = m_bDigitalIn[LLC_MASK_ON];
	if (bMaskOnChuck)
		ret = MASK_ON;
	else 
		ret = MASK_NONE;

	return ret;
}

int CIODlg::Is_Isolator_Up()
{
	int ret = 0;
	
	bool bState = m_bDigitalIn[ISOLATOR_INTERLOCK_SENSOR];

	if (bState) 
		ret = 1; //ISOLATOR UP STATE
	else 
		ret = 0;

	return ret;
}

int CIODlg::Get_LLC_DryPump_Status()
{
	int ret = 0;

	//bool DRY_ON = m_bDigitalIn[16];
	//bool DRY_ALARM = m_bDigitalIn[17];
	//bool DRY_WARNING = m_bDigitalIn[18];

	bool bDryOn		 = m_bDigitalIn[LL_DRY_PUMP1_ON_STATUS];
	bool bDryAlarm	 = m_bDigitalIn[LL_DRY_PUMP1_ALARM];
	bool bDryWarning = m_bDigitalIn[LL_DRY_PUMP1_WARNING];


	// DRY PUMP ON 일시 ON signal 점등.
	// ALARM, WARNING 도 ON 과 함께 점등. -> 발생시 소등 됨.

	if (bDryOn) 
		ret = DRYPUMP_RUN;
	if (!bDryOn) 
		ret = DRYPUMP_STOP;
	if (!bDryAlarm) 
		ret = DRYPUMP_ERROR;
	if (!bDryWarning) 
		ret = DRYPUMP_WARNING;

	//KJH
	//return DRYPUMP_RUN;

	return ret;

}

int CIODlg::Get_MC_DryPump_Status()
{
	int ret = 0;

	bool bDryOn = m_bDigitalIn[MC_DRY_PUMP2_ON_STATUS];
	bool bDryAlarm = m_bDigitalIn[MC_DRY_PUMP2_ALARM];
	bool bDryWarning = m_bDigitalIn[MC_DRY_PUMP2_WARNING];

	if (bDryOn) 
		ret = DRYPUMP_RUN;
	if (!bDryOn) 
		ret = DRYPUMP_STOP;
	if (!bDryAlarm) 
		ret = DRYPUMP_ERROR;
	if (!bDryWarning) 
		ret = DRYPUMP_WARNING;

	return ret;
}

int CIODlg::Get_LLC_Line_ATM_Status()
{
	int ret = 0;

	bool bState = m_bDigitalIn[LL_ATM_SENSOR];
	if (bState) 
		ret = ATM_STATE;
	else 
		ret = VACUUM_STATE;

	return ret;

}

bool CIODlg::Is_ATM_Robot_Arm_Retract()
{
	bool bState = m_bDigitalIn[ATM_ROBOT_ARM_RETRACT_STATUS];

	if (bState) 
		return TRUE;
	else 
		return FALSE;
}

bool CIODlg::Is_VAC_Robot_Arm_Extend()
{
	bool bExtended  = m_bDigitalIn[VAC_ROBOT_ARM_EXTEND_STATUS];
	bool bRetracted = m_bDigitalIn[VAC_ROBOT_ARM_RETRACT_STATUS];

	if ((!bExtended && !bRetracted) || (bExtended && bRetracted)) 
		return FALSE;

	if (bExtended) 
		return TRUE;
	else 
		return FALSE;
}

bool CIODlg::Is_VAC_Robot_Arm_Retract()
{
	bool bRetracted = m_bDigitalIn[VAC_ROBOT_ARM_RETRACT_STATUS];
	bool bExtended  = m_bDigitalIn[VAC_ROBOT_ARM_EXTEND_STATUS];

	if ((!bExtended && !bRetracted) || (bExtended && bRetracted)) 
		return FALSE;

	if (bRetracted) 
		return TRUE;
	else 
		return FALSE;
}

int CIODlg::Is_SourceGate_OpenOn_Check()
{
	if (g_pGauge_IO->m_dPressure_MC <= g_pConfig->m_dPressure_Rough_End) 
		return VALVE_OPEN;
	else 
		return VALVE_CLOSE;
}

int CIODlg::Get_Water_Supply_Status()
{
	int ret = 0;

	bool bState = m_bDigitalIn[MAIN_WATER];
	if (bState) 
		ret = RUN;
	else 
		ret = RUN_OFF;

	return ret;
}

int CIODlg::Get_Air_Supply_Status()
{
	int ret = 0;

	bool bState = m_bDigitalIn[MAIN_AIR];
	if (bState)
		ret = RUN;
	else 
		ret = RUN_OFF;

	return ret;
}

int CIODlg::Get_Leak_Status()
{
	int ret = 0;

	bool bLlcTmpLeakCheck = m_bDigitalIn[WATER_LEAK_SENSOR1_LL_TMP];
	bool bMcTmpLeakCheck = m_bDigitalIn[WATER_LEAK_SENSOR2_MC_TMP];

	// 1 : 정상 구동 상태.
	// 0 : leak  발생

	if (!bLlcTmpLeakCheck || !bMcTmpLeakCheck) 
		ret = RUN_OFF; // LEAK 발생
	else 
		ret = RUN; // 정상 구동 상태
	   
	return ret;
}

int CIODlg::Get_LLC_Leak_Status()
{
	int ret = 0;

	bool bLlcTmpLeakCheck = m_bDigitalIn[WATER_LEAK_SENSOR1_LL_TMP];


	// 1 : 정상 구동 상태.
	// 0 : leak  발생

	if (!bLlcTmpLeakCheck) 
		ret = RUN_OFF; // LEAK 발생
	else 
		ret = RUN; // 정상 구동 상태

	return ret;
}

int CIODlg::Get_MC_Leak_Status()
{
	int ret = 0;

	bool bMcTmpLeakCheck = m_bDigitalIn[WATER_LEAK_SENSOR2_MC_TMP];

	// 1 : 정상 구동 상태.
	// 0 : leak  발생

	if (!bMcTmpLeakCheck) 
		ret = RUN_OFF; // LEAK 발생
	else 
		ret = RUN; // 정상 구동 상태

	return ret;
}

int CIODlg::Get_detact_Status()
{
	int ret = 0;

	bool bSmokeCheckCB  = m_bDigitalIn[SMOKE_DETACT_SENSOR_CB];
	bool bSmokeCheckVAC = m_bDigitalIn[SMOKE_DETACT_SENSOR_VAC_ST]; // 전장 연결 안됨

	// 1 : 정상 구동 상태.
	// 0 : leak  발생

	if (!bSmokeCheckCB)
		ret = 0; // DETACT 발생
	else 
		ret = 1; // 정상 구동 상태

	return ret;
}

int CIODlg::Get_Loading_Position_Status()
{
	int ret = -1;

	bool bLoadingPos = m_bDigitalIn[STAGE_LOADING_POSITION];

	// 1 : Loading Position.
	// 0 : Loading Position 아님.

	if (!bLoadingPos) 
		ret = 0; // Loading Position X
	else 
		ret = 1; // Loading Position O

	return ret;
}

void CIODlg::Main_Error_Status()
{
	// MAIN AIR , MAIN WATER 상태 확인.
	// WATER_SUPPLY_VALVE , WATER_RETURN_VALVE 상태 확인.
	// LEAK , DETACT 상태 확인.
	char* str;




	//KJH TEST 
	// TRUE 정상, 

	str = "MAIN_ERROR_ON :: Main_Error_Status()";

	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));

	BOOL Air_supply_state = Get_Air_Supply_Status(); 
	BOOL Water_supply_state = Get_Water_Supply_Status(); 
	BOOL Water_Valve_state = Is_Water_Valve_Open();
	BOOL Leak_state = Get_Leak_Status();
	BOOL Detact_state = Get_detact_Status();
	
	CString air;
	CString water_supply;
	CString water_valve;
	CString leak;
	CString Detact;

	air.Format("%d", Air_supply_state);
	water_supply.Format("%d", Water_supply_state);
	water_valve.Format("%d", Water_Valve_state);
	leak.Format("%d", Leak_state);
	Detact.Format("%d", Detact_state);

	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Air_Supply_Status-- - 상태 값::" + air)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Water_Supply_Status --- 상태 값 ::" + water_supply)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Is_Water_Valve_Open --- 상태 값 ::" + water_valve)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Leak_Status --- 상태 값 ::"+ leak)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Leak_Status --- 상태 값 ::" + Detact)));

	//////////////////
	if (Get_Air_Supply_Status() == RUN_OFF || Get_Water_Supply_Status() == RUN_OFF || Is_Water_Valve_Open() == RUN_OFF || Get_Leak_Status() == RUN_OFF || Get_detact_Status() == RUN_OFF)
	{
		str = " MAIN_ERROR_ON() : Main Error 발생 점검 필요 !!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Air_Supply_Status ERROR --- 상태 값 ::" + air)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Water_Supply_Status ERROR --- 상태 값 ::" + water_supply)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Is_Water_Valve_Open ERROR --- 상태 값 ::" + water_valve)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Leak_Status ERROR --- 상태 값 ::" + leak)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Leak_Status ERROR --- 상태 값 ::" + Detact)));

		//::AfxMessageBox(str);

		//if (Error_On() != TRUE)
		//{
		//	str = " MAIN_ERROR_ON() : Main Error 발생 후 인터락 명령 실행 에러 발생 !";
		//	SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));
		//	g_pLog->Display(0, str);
		//}
	}
}

int CIODlg::Water_Valve_Open()
{
	int ret = 0;

	bool bSupplyValve = m_bDigitalOut[WATER_SUPPLY_VALVE];
	bool bReturnValve = m_bDigitalOut[WATER_RETURN_VALVE];

	if (bSupplyValve && bReturnValve)
		ret = VALVE_OPEN;
	else if (!bSupplyValve || !bReturnValve)
		ret = VALVE_CLOSE;
	else 
		ret = -1;

	return ret;
}

int CIODlg::Water_Supply_Valve_Open()
{
	int ret = 0;

	bool bSupplyValve = m_bDigitalOut[WATER_SUPPLY_VALVE];
	if (bSupplyValve)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Water_Return_Valve_Open()
{
	int ret = 0;

	bool bReturnValve = m_bDigitalOut[WATER_RETURN_VALVE];
	if (bReturnValve)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_CLOSE;

	return ret;
}

bool CIODlg::Is_LLC_Atm_Check()
{
	bool bLlcAtmSensorCheck = m_bDigitalIn[LL_ATM_SENSOR];

	if (bLlcAtmSensorCheck) 
		return TRUE;
	else 
		return FALSE;
}

bool CIODlg::Is_MC_Atm_Check()
{
	bool bMcAtmSensorCheck = m_bDigitalIn[MC_ATM_SW];

	if (bMcAtmSensorCheck) 
		return TRUE;
	else 
		return FALSE;
}

bool CIODlg::Is_MC_Vac_Check()
{
	if (g_pGauge_IO->m_dPressure_MC <= g_pConfig->m_dPressure_Rough_End_ini) 
		return TRUE;
	else 
		return FALSE;
}

bool CIODlg::Is_LLC_Mask_Slant_Check()
{
	bool bLlcTilt1 = g_pIO->m_bDigitalIn[LLC_MASK_SLANT1];
	bool bLlcTilt2 = g_pIO->m_bDigitalIn[LLC_MASK_SLANT2];

	if (bLlcTilt1 == false && bLlcTilt2 == false) 
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_MC_Mask_Slant_Check()
{
	bool bMcTilt1 = g_pIO->m_bDigitalIn[MAIN_MASK_SLANT1];
	bool bMcTilt2 = g_pIO->m_bDigitalIn[MAIN_MASK_SLANT2];

	if (bMcTilt1 == false && bMcTilt2 == false) 
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_LLC_Mask_Slant1_Check()
{
	bool bLlcTilt1 = g_pIO->m_bDigitalIn[LLC_MASK_SLANT1];

	if (bLlcTilt1 == false) 
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_LLC_Mask_Slant2_Check()
{
	bool bLlcTilt2 = g_pIO->m_bDigitalIn[LLC_MASK_SLANT2];

	if (bLlcTilt2 == false) 
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_MC_Mask_Slant1_Check()
{
	bool bMcTilt1 = g_pIO->m_bDigitalIn[MAIN_MASK_SLANT1];

	if (bMcTilt1 == false) 
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_MC_Mask_Slant2_Check()
{
	bool bMcTilt2 = g_pIO->m_bDigitalIn[MAIN_MASK_SLANT2];

	if (bMcTilt2 == false)
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Buzzer_on()
{
	int nAddr  = 8;
	int nIndex = 3;
	int nData  = 1;

	int nRet = g_pIO->WriteOutputDataBit(nAddr, nIndex, nData);
	if (nRet != 0)
		return false;
	else
		return true;
}

bool CIODlg::Buzzer_off()
{
	int nAddr  = 8;
	int nIndex = 3;
	int nData  = 0;

	int nRet = g_pIO->WriteOutputDataBit(nAddr, nIndex, nData);
	if (nRet != 0)
		return false;
	else
		return true;
}

bool CIODlg::LLC_Dry_Pump_off()
{
	int nAddr  = 12;
	int nIndex = 0;
	int nData  = 0;

	int nRet = g_pIO->WriteOutputDataBit(nAddr, nIndex, nData);
	if (nRet != 0)
		return false;
	else
		return true;
}

bool CIODlg::MC_Dry_Pump_off()
{
	int nAddr  = 12;
	int nIndex = 1;
	int nData  = 0;

	int nRet = g_pIO->WriteOutputDataBit(nAddr, nIndex, nData);
	if (nRet != 0)
		return false;
	else
		return true;
}

int CIODlg::Valve_Check(bool bOpen, bool bClose)
{
	int ret = 0;

	if (bOpen == bClose)
	{
		if ((bOpen == 0) && (bClose == 0))
			ret = VALVE_CLOSE; // CLOSE
		else 
			ret = VALVE_NOT_OPENED_CLOSED; //VALVE ERROR
	}
	else if (bOpen != bClose)
	{
		if (bOpen) 
			ret = VALVE_OPEN; // VALVE OPEN
		else 
			ret = VALVE_CLOSE; // VALVE CLOSE
	}
	else 
		ret = VALVE_NOT_OPENED_CLOSED;// VALVE ERROR

	return ret;
}

void CIODlg::OnBnClickedBtnMaint()
{
	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM MAINT Mode 로 진행 하시겠습니까? ")));	//통신 상태 기록.
	//if (pwdlg.m_strTxt != "srem")
	if (pwdlg.m_strTxt != "1234")
	{
		CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Password 가 일치 하지 않습니다 ")));	//통신 상태 기록.
		::AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
		return;
	}
	else if (pwdlg.m_strTxt == "1234")
	{
		GetDlgItem(IDC_BTN_MAINT)->EnableActiveAccessibility();
		GetDlgItem(IDC_BTN_MAINT)->EnableWindow(false);
		GetDlgItem(IDC_BTN_OPER)->EnableWindow(true);
		m_nIOMode = MAINT_MODE_ON;
		CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM MAINT Mode On ")));	//통신 상태 기록.
		::AfxMessageBox("MAINT MODE로 전환 되었습니다", MB_ICONINFORMATION);
	}

	
}


void CIODlg::OnBnClickedBtnOper()
{
	GetDlgItem(IDC_BTN_OPER)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_OPER)->EnableWindow(false);
	GetDlgItem(IDC_BTN_MAINT)->EnableWindow(true);
	m_nIOMode = OPER_MODE_ON;
	CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM OPER Mode On ")));
	::AfxMessageBox("OPER MODE로 전환 되었습니다", MB_ICONINFORMATION);
}


int CIODlg::Str_ok_Box(CString Log, CString str_nCheck)
{
	//char* str = Log + str_nCheck;

	if (AfxMessageBox(Log + str_nCheck + "\n" + "실행하시겠습니까?", MB_YESNO) == IDYES)
	{
		return RUN;
	}
	else return RUN_OFF;
}


int CIODlg::LLC_Dry_Pump_Error_Sequence()
{
	char* str;

	if ((Get_LLC_DryPump_Status() == DRYPUMP_ERROR) || (Get_LLC_DryPump_Status() == DRYPUMP_WARNING))
	{
		str = "LLC DRY PUMP ERROR";
		m_nERROR_MODE = RUN;
		//if (!Error_On(str))
		//{
		//	g_pWarning->UpdateData(FALSE);
		//	g_pWarning->ShowWindow(SW_HIDE);
		//}
	}
	
	return 0;

}


int CIODlg::MC_Dry_Pump_Error_Sequence()
{
	char* str;

	if ((Get_MC_DryPump_Status() == DRYPUMP_ERROR) || (Get_MC_DryPump_Status() == DRYPUMP_WARNING))
	{
		str = "MC DRY PUMP ERROR";
		m_nERROR_MODE = RUN;
		//if (!Error_On(str))
		//{
		//	//CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
		//	//g_pWarning->m_strWarningMessageVal = " Error Sequence 가 중지 되었습니다 ";
		//	g_pWarning->UpdateData(FALSE);
		//	g_pWarning->ShowWindow(SW_HIDE);
		//}

	}

	return 0;

}


int CIODlg::Error_Sequence(int ErrorCode, char* str)
{
	//char* str;

	m_nERROR_MODE = RUN;

	if (!Error_On(ErrorCode, str))
	{
		//g_pWarning->UpdateData(FALSE);
		//g_pWarning->ShowWindow(SW_HIDE);
	}

	return 0;
}



int CIODlg::Error_On(int ErrorCode, char* str)
{
	char* log_str;

	m_nERROR_MODE = RUN;
	
	//CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	//g_pWarning->m_strWarningMessageVal = " Error Sequence 가 작동 중입니다. ";
	//g_pWarning->UpdateData(FALSE);
	//g_pWarning->ShowWindow(SW_SHOW);
	
	g_pAlarm->SetAlarm(ErrorCode);
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));

	//////////////////////////////
	// Error Sequence
	//////////////////////////////
	// 1. TR GATE CLOSE
	// 2. LLC GATE CLOSE
	// 3. LLC TMP GATE CLOSE
	// 4. MC TMP GATE CLOSE
	// 5. LLC ROUGH GATE CLOSE
	// 6. MC ROUGH GATE CLOSE
	// 7. FAST MFC GATE CLOSE
	// 8. SLOW MFC GATE CLOSE
	// 9. MC TMP OFF
	// 10. LLC TMP OFF
	///////////////////////////////


	///////////////////////
	// SOURCE Shutter Close 
	///////////////////////
	if (g_pEUVSource != NULL)
	{
		g_pEUVSource->SRC_CloseShutter();
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Source Shutter Close!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		///////////////////////
		// EUV SOURCE OFF 
		///////////////////////
		g_pEUVSource->SetEUVSourceOn(FALSE);
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Euv OFF!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}
	/////////////////////
	// Source Gate Close Check and Write
	/////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Source Gate Close 전달!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	if (Is_SourceGate_OpenOn_Check() != VALVE_CLOSE)
	{
		if (Close_SourceGate_Check() != OPERATION_COMPLETED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Source Gate Close Check 명령 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, log_str);
			g_pLog->Display(0, g_pIO->Log_str);
			return FALSE;
		}
	}

	////////////////////////
	// LLC TMP GATE CLOSE
	////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Gate Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_LLC_TMP_GateValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Gate Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_LLC_TMP_GateValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	////////////////////////
	// MC TMP GATE CLOSE
	////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Gate Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_MC_TMP1_GateValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Gate Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_MC_TMP1_GateValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_MC_TMP1_GateValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	////////////////////////////////////////////////////////////
	// 로딩 & 언로딩 진행 상태 확인
	// ( 동작 중인 경우, 동작의 Sequence 를 정상 종료 확인 )
	////////////////////////////////////////////////////////////
	if (g_pAP != NULL)
	{
		while (g_pAP->m_bThreadStop != TRUE)
		{
			if (g_pAP->m_bThreadStop == FALSE)
				break;
		}

		if ((g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE) || (g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE))
		{
			/* Error Mode 실행 중 Loading , Unloading 정상 종료 된 경우 TR Gate Close 가능 */
			m_bAPSequenceNormalCloseFlag = TRUE;
		}
		else
		{
			/* Error Mode 실행 중 Loading , Unloading 비정상 종료 된 경우 TR Gate Close 불가능 */
			m_bAPSequenceNormalCloseFlag = FALSE;
		}

	}

	////////////////////////
	// TR GATE CLOSE
	//
	// Loading & Unloading 정상 종료에만 TR GATE 종료
	// 그렇치 않으면 TR OPEN
	////////////////////////
	if (m_bAPSequenceNormalCloseFlag)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 TR Gate Close !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
		if (Close_TRGateValve() != OPERATION_COMPLETED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 TR Gate Close 명령 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, log_str);
			g_pLog->Display(0, g_pIO->Log_str);
			return FALSE;
		}
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (Is_TRGateValve_Open() == VALVE_CLOSE) break;
		}

		if (Is_TRGateValve_Open() != VALVE_CLOSED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 TR Gate Close 확인 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			g_pLog->Display(0, log_str);

			return FALSE;
		}

		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 TR Gate Close 완료!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}
	else
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Loading & Unloading 상태 확인 불가로 TR Gate 현 상태 유지 진행!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}

	////////////////////////
	// LLC GATE CLOSE
	////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Gate Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_LLCGateValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Gate Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_LLCGateValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);





	///////////////////////////////
	// LLC SLOW ROUGH VALVE CLOSE
	///////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Slow Rough Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_LLC_SlowRoughValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Slow Rough Valve Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_LLC_SlowRoughValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Slow Rough Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Slow Rough Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);


	///////////////////////////////
	// LLC FAST ROUGH VALVE CLOSE
	///////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Fast Rough Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_LLC_FastRoughValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Fast Rough Valve Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T("return valve :: "));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->return_int)));
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_LLC_FastRoughValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Fast Rough Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Fast Rough Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	////////////////////////////
	// MC FAST ROUGH VALVE CLOSE
	////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Fast Rough Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_MC_FastRoughValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Fast Rough Valve Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;

	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_MC_FastRoughValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Fast Rough Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Fast Rough Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	/////////////////////////////////
	// MC SLOW ROUGH VALVE CLOSE
	/////////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Slow Rough Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_MC_SlowRoughValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Slow Rough Valve Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_MC_SlowRoughValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Slow Rough Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Slow Rough Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	
	////////////////////////////////
	// FAST VENT VALVE CLOSE
	////////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Fast Vent Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_FastVentValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Fast Vent Gate Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_FastVentValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Fast Vent Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Fast Vent Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	/////////////////////////////
	// SLOW VENT VALVE CLOSE
	/////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Slow Vent Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_SlowVentValve1() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Slow Vent Gate Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_SlowVentValve1_Open() == VALVE_CLOSE) break;
	}

	if (Is_SlowVentValve1_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Slow Vent Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Slow Vent Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	/////////////////////////////
	// MC TMP OFF
	/////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Tmp Off !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	if (g_pMCTmp_IO->Mc_Tmp_GetStatus() != TMP_OFFLINE)
	{
		if (g_pMCTmp_IO->Mc_Tmp_Off() != RUN_OFF)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Off 명령 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, log_str);
			return FALSE;
		}
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Tmp Off 명령 완료, off 진행 중 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}

	/////////////////////////////
	// LLC TMP OFF
	/////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Tmp Off !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	
	if (g_pLLKTmp_IO->GetStatus() != TMP_OFFLINE)
	{
		if (g_pLLKTmp_IO->LLC_Tmp_Off() != RUN_OFF)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Off 명령 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			g_pLog->Display(0, log_str);
			return FALSE;
		}
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Tmp Off 명령 완료, off 진행 중 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 인터락 발생 !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_HIDE);

	return TRUE;
}


int CIODlg::Set_DVR_Lamp(int dvr_ch, bool state)
{
	int nRet = -1;
	
	switch (dvr_ch)
		{
		case 1:
			nRet = g_pIO->WriteOutputDataBit(12, 5, state); // 정상 WRITE 면 retrun 0 임.
			break;
		case 2:
			nRet = g_pIO->WriteOutputDataBit(12, 6, state); // 정상 WRITE 면 retrun 0 임.
			break;
		case 3:
			nRet = g_pIO->WriteOutputDataBit(12, 7, state); // 정상 WRITE 면 retrun 0 임.
			break;
		case 4:
			nRet = g_pIO->WriteOutputDataBit(13, 0, state); // 정상 WRITE 면 retrun 0 임.
			break;
		default:
			break;
	}

	return nRet;
}


//int CIODlg::Tmp_Check()
//{
//
//	int nRet = 0;
//
//	if (g_pMCTmp_IO->mc_tmp_ReState == "Stop")
//	{
//		if (ERROR_MODE == RUN)
//		{
//			if (MC_Dry_Pump_off())
//			{
//				CString log_str = " MAIN_ERROR_ON() : Error 발생 후 MC TMP STOP , MC Dry Pump Off 실행 !";
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//				g_pLog->Display(0, log_str);
//
//
//				//////////////////////////////////////////////////////////////////
//				// TMP 완전 멈춤, 그후 DRY PUMP OFF 후 MC FORELINE GATE CLOSE
//				//////////////////////////////////////////////////////////////////
//
//				log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤, 그 후 MC Foreline Gate  Close !";
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//				g_pLog->Display(0, log_str);
//				if (Close_MC_TMP1_ForelineValve() != TRUE)
//				{
//					log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 명령 에러 발생 !";
//					CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//					CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
//					g_pLog->Display(0, log_str);
//					g_pLog->Display(0, g_pIO->Log_str);
//					return FALSE;
//				}
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
//				g_pLog->Display(0, g_pIO->Log_str);
//
//				m_start_time = clock();
//				m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
//				while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
//				{
//					if (Is_MC_TMP1_ForelineValve_Open() == VALVE_CLOSE) break;
//				}
//
//				if (Is_MC_TMP1_ForelineValve_Open() != VALVE_CLOSED)
//				{
//					log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 확인 에러 발생 !";
//					CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//					g_pLog->Display(0, log_str);
//
//					return FALSE;
//				}
//
//				log_str = "MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 확인 완료!";
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//				g_pLog->Display(0, log_str);
//
//			}
//			else
//			{
//				CString log_str = " MAIN_ERROR_ON() : Error 발생 후 MC TMP STOP , MC Dry Pump Off 실행 Error (확인필요) !";
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//				g_pLog->Display(0, log_str);
//			}
//		}
//	}
//
//}
//void CIODlg::Tmp_Check()
//{
//	mc_tmp_check();
//	
//}


void CIODlg::Closedevice_IO()
{
	StopIoUpdate();
	CloseDevice();
	CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM IO Disconnected Click")));	//통신 상태 기록.

	m_bThreadExitFlag = TRUE;
	m_bCrevis_Open_Port = FALSE;

	KillTimer(IO_UPDATE_TIMER);

	if (m_pThread != NULL)
	{
		HANDLE threadHandle = m_pThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pThread = NULL;
	}

}