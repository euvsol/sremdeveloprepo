
#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

/* Command Protocol */
/*

STAGEMOVE (double)XPOS, (doblue)YPOS 
	; 지정된 xpos, ypos로 이동
EUVON
	; EunOn, Shutter ON
EUVOFF
	; EuvOff, Shutter Off
SCANSTART (double)FovSize, (double)StepSize, (int)RepeatNo, (int)ThroughFocusNo, (double)ThroughFocusStep
	; 지정된 옵션으로 현 위치에서 Scan 시작
LOAD
	; Mask Load 진행
UNLOAD
	; Mask UnLoad 진행
OMALIGN
	;  OM algn 진행
EUVALGN
	; EUV Algn 진행

*/


CMacroSystem::CMacroSystem()
{

}

CMacroSystem::~CMacroSystem()
{

}

void CMacroSystem::DeleteCom()
{
	if (Stagemove != NULL)
	{
		delete[] Stagemove;
		Stagemove = NULL;
	}

	if (EuvCtl != NULL)
	{
		delete[] EuvCtl;
		EuvCtl = NULL;
	}

	if (ScanCtl != NULL)
	{
		delete[] ScanCtl;
		ScanCtl = NULL;
	}

	if (LoadCtl != NULL)
	{
		delete[] LoadCtl;
		LoadCtl = NULL;
	}

	//if ((StageMoveOnCom != NULL))
	//{
	//	delete[] StageMoveOnCom;
	//	StageMoveOnCom = NULL;
	//}
	//
	//if (EuvOn != NULL)
	//{
	//	delete[] EuvOn;
	//	EuvOn = NULL;
	//}
	//
	//if (EuvOff != NULL)
	//{
	//	delete[] EuvOff;
	//	EuvOff = NULL;
	//}
	//
	//if (ScanStart != NULL)
	//{
	//	delete[] ScanStart;
	//	ScanStart = NULL;
	//}
	//
	//if (ScanStop != NULL)
	//{
	//	delete[] ScanStop;
	//	ScanStop = NULL;
	//}
}



void CMacroSystem::DeleteMem()
{
	if (Stagemove != NULL)
	{
		delete[] Stagemove;
		Stagemove = NULL;
	}

	if (EuvCtl != NULL)
	{
		delete[] EuvCtl;
		EuvCtl = NULL;
	}

	if (ScanCtl != NULL)
	{
		delete[] ScanCtl;
		ScanCtl = NULL;
	}

	if (LoadCtl != NULL)
	{
		delete[] LoadCtl;
		LoadCtl = NULL;
	}

	if ((StageMoveOnCom != NULL))
	{
		delete[] StageMoveOnCom;
		StageMoveOnCom = NULL;
	}
	
	if (EuvOn != NULL)
	{
		delete[] EuvOn;
		EuvOn = NULL;
	}
	
	if (EuvOff != NULL)
	{
		delete[] EuvOff;
		EuvOff = NULL;
	}
	
	if (ScanStart != NULL)
	{
		delete[] ScanStart;
		ScanStart = NULL;
	}
	
	if (ScanStop != NULL)
	{
		delete[] ScanStop;
		ScanStop = NULL;
	}
}


void CMacroSystem::NewCom()
{
	if (Stagemove == NULL)
		STAGEMOVE *Stagemove = new STAGEMOVE;

	if (EuvCtl == NULL)
		EUVCTL *EuvCtl = new EUVCTL;

	if (ScanCtl == NULL)
		SCANCTL *ScanCtl = new SCANCTL;

	if (LoadCtl == NULL)
		LOADCTL *LoadCtl = new LOADCTL;

	if (StageMoveOnCom == NULL)
		Command *StageMoveOnCom;

	if (EuvOn == NULL)
		Command *EuvOn;

	if (EuvOff == NULL)
		Command *EuvOff;

	if (ScanStart == NULL)
		Command *ScanStart;

	if (ScanStop == NULL)
		Command *ScanStop;
}


void CMacroSystem::GetCommandData(char* com, double x_pos = 0.0, double y_pos = 0.0, double FovSize = 0.0, double StepSize = 0.0, int Repeatno = 0, int ThroughFocusNo = 0, double ThroughFocusStep = 0.0)
{
	CString Com;
	Com.Empty();
	Com.Format("%s", com);

	//STAGEMOVE *Stagemove = new STAGEMOVE;
	//EUVCTL *EuvCtl = new EUVCTL;
	//SCANCTL *ScanCtl = new SCANCTL;
	//LOADCTL *LoadCtl = new LOADCTL;

	//Command *StageMoveOnCom;
	//Command *EuvOn;
	//Command *EuvOff;
	//Command *ScanStart;
	//Command *ScanStop;
	
	NewCom();

	if(Com == "STAGEMOVE")
	{
		StageMoveOnCom = new StageMoveOnCommand(Stagemove, x_pos, y_pos);
		_ComList.addCommand(StageMoveOnCom);
	}
	else if (Com == "EUVON")
	{
		EuvOn = new EuvOnCommand(EuvCtl);
		_ComList.addCommand(EuvOn);
	}
	else if (Com == "EUVOFF")
	{
		EuvOff = new EuvOffCommand(EuvCtl);
		_ComList.addCommand(EuvOff);
	}
	else if (Com == "SCANSTART")
	{
		ScanStart = new ScanStartCommand(ScanCtl, FovSize, StepSize, Repeatno, ThroughFocusNo, ThroughFocusStep);
		_ComList.addCommand(ScanStart);
	}
	else if (Com == "SCANSTOP")
	{
		ScanStop = new ScanStopCommand(ScanCtl);
		_ComList.addCommand(ScanStop);
	}
	else if (Com == "STOP")
	{
		//_ComList.createRecipe();
		//DeleteCom();
	}
}

void RecipeStack::addCommand(Command *com)
{
	vector_commands.push_back(com);
};

BOOL RecipeStack::createRecipe()
{
	BOOL ReturnFlag = TRUE;


	for (vector<Command*>::size_type x = 0; x < vector_commands.size(); x++)
	{
		if ((vector_commands[x]->execute()) != TRUE)
		{

			::AfxMessageBox(" Macro 동작 Error 발생!");
			ReturnFlag = FALSE;
			break;
		}
	}

	return ReturnFlag;

}

BOOL STAGEMOVE::StageMoveStart(double x_pos, double y_pos)
{
	BOOL ReturnFlag = TRUE;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	MsgBoxAuto.DoModal(_T(" Mask Load 에 실패 하였습니다 ! "), 3);

	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Stage Move Start! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	if (g_pNavigationStage->MoveAbsolutePosition(x_pos, y_pos) != XY_NAVISTAGE_OK)
	{

		::AfxMessageBox(" Stage 동작 불가!");
		g_pWarning->ShowWindow(SW_HIDE);
		ReturnFlag = FALSE;
		return ReturnFlag;
	}

	g_pWarning->ShowWindow(SW_HIDE);
	return ReturnFlag;
}

BOOL EUVCTL::EuvOn()
{
	BOOL ReturnFlag = TRUE;

	g_pWarning->m_strWarningMessageVal = " MacroSystem :: EuvOn! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	
	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		ReturnFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);

		return ReturnFlag;
	}

	if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		g_pWarning->ShowWindow(SW_HIDE);
		ReturnFlag = FALSE;
		return ReturnFlag;
	}

	g_pEUVSource->SetMechShutterOpen(FALSE);
	if (g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
	{
		ReturnFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		return ReturnFlag;
	}
	Sleep(3000);
	if(g_pEUVSource->Is_EUV_On() != TRUE)
	{
		ReturnFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		return ReturnFlag;
	}

	g_pEUVSource->SetMechShutterOpen(TRUE);
	Sleep(3000);
	if (g_pEUVSource->Is_LaserShutter_Opened() != TRUE)
	{
		ReturnFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		return ReturnFlag;
	}
	
	g_pWarning->ShowWindow(SW_HIDE);
	return ReturnFlag;
}

BOOL EUVCTL::EuvOff()
{
	BOOL ReturnFlag = TRUE;

	g_pWarning->m_strWarningMessageVal = " MacroSystem :: EuvOff ! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		ReturnFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		return ReturnFlag;
	}

	if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		ReturnFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		return ReturnFlag;
	}

	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);

	Sleep(3000);

	if ((g_pEUVSource->Is_EUV_On() != FALSE) || (g_pEUVSource->Is_LaserShutter_Opened() != FALSE))
	{
		ReturnFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		return ReturnFlag;
	}

	g_pWarning->ShowWindow(SW_HIDE);
	return ReturnFlag;
}

BOOL SCANCTL::ScanStart(double Fovsize, double Stepsize, int Repeatno, int Throughfocusno, double Throughfocusstep)
{
	BOOL ReturnFlag = TRUE;

	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Scan 중 ! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	// Scan Stage Run	if (g_pScanStage != NULL)	
	if (!g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, (int)Fovsize, (int)Stepsize, Repeatno))// Scan Stage Run
	{
		::AfxMessageBox(_T(" SCAN 에 실패 하였습니다 ! "));
		ReturnFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		return ReturnFlag;

	}

	if (g_pEUVSource != NULL)
	{
		if (g_pEUVSource->Is_SRC_Connected() == TRUE)
		{
			g_pEUVSource->SetMechShutterOpen(FALSE);
		}
	}

	g_pWarning->ShowWindow(SW_HIDE);
	return ReturnFlag;
}

BOOL SCANCTL::ScanStop()
{
	BOOL ReturnFlag = TRUE;

	return ReturnFlag;
}

BOOL LOADCTL::Load()
{
	BOOL ReturnFlag = TRUE;

	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Mask Load 중! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		::AfxMessageBox(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "));
		ReturnFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		return ReturnFlag;
	}

	if (IDYES != AfxMessageBox("Mask를 Loading 하시겠습니까?", MB_YESNO))
	{
		ReturnFlag = FALSE;
		return ReturnFlag;
	}

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != TRUE)
	{
		if (IDYES != AfxMessageBox("캠센서 인터락이 해제되어 있습니다. 계속 진행하시겠습니까?", MB_YESNO))
		{
			ReturnFlag = FALSE;
			g_pWarning->ShowWindow(SW_HIDE);
			return ReturnFlag;
		}
	}

	if (!g_pMaskMap->MaskLoad())
	{
		::AfxMessageBox(_T(" Mask Load 에 실패 하였습니다 ! "));
		g_pWarning->ShowWindow(SW_HIDE);
		ReturnFlag = FALSE;
		return ReturnFlag;
	}

	g_pWarning->ShowWindow(SW_HIDE);
	return ReturnFlag;
}

BOOL LOADCTL::UnLoad()
{
	BOOL ReturnFlag = TRUE;

	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Mask UnLoad 중! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		AfxMessageBox(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "));
		g_pWarning->ShowWindow(SW_HIDE);
		ReturnFlag = FALSE;
		return ReturnFlag;
	}

	if (IDYES != AfxMessageBox("Mask를 Unloading 하시겠습니까?", MB_YESNO))
	{
		g_pWarning->ShowWindow(SW_HIDE);
		ReturnFlag = FALSE;
		return ReturnFlag;
	}

	if (g_pVacuumRobot->Is_VMTR_CamSensor_Interlock() != TRUE)
	{
		if (IDYES != AfxMessageBox("캠센서 인터락이 해제되어 있습니다. 계속 진행하시겠습니까?", MB_YESNO))
		{
			g_pWarning->ShowWindow(SW_HIDE);
			ReturnFlag = FALSE;
			return ReturnFlag;
		}
	}

	if (!g_pMaskMap->MaskUnload())
	{
		::AfxMessageBox(_T(" Mask UnLoad 에 실패 하였습니다 ! "));
		g_pWarning->ShowWindow(SW_HIDE);
		ReturnFlag = FALSE;
		return ReturnFlag;
	}

	g_pWarning->ShowWindow(SW_HIDE);
	return ReturnFlag;
}