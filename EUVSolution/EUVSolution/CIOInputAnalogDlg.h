﻿#pragma once

// CAnalogInput 대화 상자

class CAnalogInput : public CDialogEx
{
	DECLARE_DYNAMIC(CAnalogInput)

public:
	CAnalogInput(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CAnalogInput();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_INPUT_AN_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();
	HICON	m_LedIcon[3];


	DECLARE_MESSAGE_MAP()
public:

	int		m_nChannel;

	bool OnUpdateDigitalInput_AI();

	//void InitControls_AI(int nCh);
	void InitControls_AI();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();


	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	CBrush  m_brush;
	CFont	m_font;
};
