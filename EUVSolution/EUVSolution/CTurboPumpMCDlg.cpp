﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CTurboPumpMCDlg 대화 상자

IMPLEMENT_DYNAMIC(CTurboPumpMCDlg, CDialogEx)

CTurboPumpMCDlg::CTurboPumpMCDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TURBO_PUMP_MC_DIALOG, pParent)
{
	m_mc_tmp_Thread = NULL;
	str_CR = "\r";
	//_CrtSetBreakAlloc(17103);
}

CTurboPumpMCDlg::~CTurboPumpMCDlg()
{
	str_mc_tmp_ReceiveDataHz.Empty();				// HZ
	str_mc_tmp_ReceiveDataState.Empty();			// STATE
	str_mc_tmp_ReceiveDataErrorCode.Empty();		//	ERROR CODE
	str_mc_tmp_ReceiveDataErrorState.Empty();		//	ERROR STATE 
	str_mc_tmp_ReceiveDataOnOffState.Empty();		// ON/OFF STATE
	str_mc_tmp_ReceiveDataRpm.Empty();				// 현재 rpm
	str_mc_tmp_ReceiveDataTemperature_1.Empty();	//전자 장치 온도
	str_mc_tmp_ReceiveDataTemperature_2.Empty();	//펌프 하단부 온도
	str_mc_tmp_ReceiveDataTemperature_3.Empty();	//모터온도	
	str_mc_tmp_ReceiveDataTemperature_4.Empty();	//베어링 온도	
	str_mc_tmp_Error_War_State.Empty();				// Error , Warning 구분
	str_mc_tmp_ReveiveDataSetHz.Empty();			// 현재 설정 값 hz
	str_mc_tmp_ReveiveDataSetRpm.Empty();			// 현재 설정 값 rpm
}

void CTurboPumpMCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTurboPumpMCDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_MC_TMP_ON, &CTurboPumpMCDlg::OnBnClickedMcTmpOn)
	ON_BN_CLICKED(IDC_MC_TMP_OFF, &CTurboPumpMCDlg::OnBnClickedMcTmpOff)
	ON_BN_CLICKED(IDC_MC_TMP_ERROR_RESET, &CTurboPumpMCDlg::OnBnClickedMcTmpErrorReset)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_MC_TMP_LOCK, &CTurboPumpMCDlg::OnBnClickedMcTmpLock)
END_MESSAGE_MAP()


// CTurboPumpMCDlg 메시지 처리기


BOOL CTurboPumpMCDlg::OnInitDialog()
{
	__super::OnInitDialog();


	m_Mc_Tmp_OpenPort_State = FALSE;		// OPEN PORT 성공 여부 판단.
	m_mc_tmp_state = 0;				// getstatus() 함수로 인자 전달.
	m_mc_tmp_ThreadExitFlag = FALSE;	//ThreadExit 
	m_lock_state = 0;


	str_mc_tmp_ReceiveDataHz.Empty();				// HZ
	str_mc_tmp_ReceiveDataState.Empty();			// STATE
	str_mc_tmp_ReceiveDataErrorCode.Empty();		//	ERROR CODE
	str_mc_tmp_ReceiveDataErrorState.Empty();		//	ERROR STATE 
	str_mc_tmp_ReceiveDataOnOffState.Empty();		// ON/OFF STATE
	str_mc_tmp_ReceiveDataRpm.Empty();				// 현재 rpm
	str_mc_tmp_ReceiveDataTemperature_1.Empty();	//전자 장치 온도
	str_mc_tmp_ReceiveDataTemperature_2.Empty();	//펌프 하단부 온도
	str_mc_tmp_ReceiveDataTemperature_3.Empty();	//모터온도	
	str_mc_tmp_ReceiveDataTemperature_4.Empty();	//베어링 온도	
	str_mc_tmp_Error_War_State.Empty();				// Error , Warning 구분
	str_mc_tmp_ReveiveDataSetHz.Empty();			// 현재 설정 값 hz
	str_mc_tmp_ReveiveDataSetRpm.Empty();			// 현재 설정 값 rpm

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);


	((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_CON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_3))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_MC_ICON_SPEED_ON))->SetIcon(m_LedIcon[0]);

	str_mc_tmp_ReceiveDataErrorCode = "No Error";
	pre_data = 0;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CTurboPumpMCDlg::OnDestroy()
{
	__super::OnDestroy();

	m_mc_tmp_ThreadExitFlag = TRUE;

	if (m_mc_tmp_Thread != NULL)
	{
		HANDLE threadHandle = m_mc_tmp_Thread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/1000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_mc_tmp_Thread = NULL;
	}

}


BOOL CTurboPumpMCDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return __super::PreTranslateMessage(pMsg);
}


int CTurboPumpMCDlg::OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
{

	int nRet = -1;

	nRet = CPfeifferHiPace2300TMPCtrl::OpenPort(sPortName, dwBaud, wByte, wStop, wParity);

	if (nRet != 0) {

		m_Mc_Tmp_OpenPort_State = FALSE;
		m_mc_tmp_ThreadExitFlag = TRUE;

		SetDlgItemText(IDC_MC_TMP_HZ, "Connection Fail");
		SetDlgItemText(IDC_MC_TMP_STATE, "Connection Fail");
		SetDlgItemText(IDC_MC_TMP_ERROR_CODE, "Connection Fail");		
		SetDlgItemText(IDC_MC_TMP_ERROR_STATE, "Connection Fail");
		SetDlgItemText(IDC_MC_TMP_TEMP2, "Connection Fail");
		SetDlgItemText(IDC_MC_TMP_TEMP3, "Connection Fail");
		((CStatic*)GetDlgItem(IDC_MC_ICON_CON))->SetIcon(m_LedIcon[2]);
	}
	else {

		m_Mc_Tmp_OpenPort_State = TRUE;
		m_mc_tmp_ThreadExitFlag = FALSE;

		m_mc_tmp_Thread = AfxBeginThread(Mc_Tmp_UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);

		SetTimer(MC_TMP_UPDATE_TIMER, 100, NULL);
		((CStatic*)GetDlgItem(IDC_MC_ICON_CON))->SetIcon(m_LedIcon[1]);
	}

	return nRet;
}

//CString CTurboPumpMCDlg::GetCheckSumValue(char *command)
//{
//
//	CString strCommand;
//	strCommand.Format("%s", command);
//
//
//	byte	btCheckSum = 0x00;
//	CString strCS;
//
//	for (int nIdx = 0; nIdx < strCommand.GetLength(); nIdx++)
//	{
//		btCheckSum += (byte)strCommand[nIdx];
//	}
//
//	sprintf_s(LPSTR(LPCTSTR(strCS)), sizeof(strCS), "%03d", btCheckSum);
//	strCS.Format("%s", strCS);
//	strCommand.Append(strCS);
//	//sprintf_s(LPSTR(LPCTSTR(strCS)), sizeof(strCS), "%02X", btCheckSum);
//
//	return strCommand;
//}

int CTurboPumpMCDlg::ReceiveData(char *lParam, DWORD dwRead)
{
	int nRet = 0;
	CString	str_GetMCTMPRecevieData;
	str_GetMCTMPRecevieData.Empty();

	str_GetMCTMPRecevieData = (LPSTR)lParam;
	ParsingData(str_GetMCTMPRecevieData);

	return nRet;
}


void CTurboPumpMCDlg::ParsingData(CString strRecvMsg)
{

	int	cnt = 0;

	CString	StrTempLog;
	CString strRecvTmpMsg = strRecvMsg;
	CString StrRecvTmpMsgCommand;				// 명령값 추출
	CString StrRecvTmpMsgSubDataCommand;		// 리시브 값 추출
	CString StrRevvTmpMsgErrWarCommand;			// Error 코드 중 Error , Warning 구분

	StrRecvTmpMsgCommand = strRecvTmpMsg.Mid(5, 3);      //명령값
	StrRecvTmpMsgSubDataCommand = strRecvTmpMsg.Mid(10, 6);  //데이터값

	//ReData = lParam;
	//Received 데이터가 정상 데이터이면 Signal 해준다.
	SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]:  Command : " + StrRecvTmpMsgCommand + "    ReceiveData : " + StrRecvTmpMsgSubDataCommand))); //통신 상태 기록.

	// 펌핑스테이션
	if (StrRecvTmpMsgCommand == "010")
	{
		if (StrRecvTmpMsgSubDataCommand == "000000")
		{
			str_mc_tmp_ReceiveDataOnOffState = "TMP Turn Off";
			SetEvent(m_hPumpOffEvent);
		}
		else if (StrRecvTmpMsgSubDataCommand == "111111")
		{
			str_mc_tmp_ReceiveDataOnOffState = "TMP Turn On";
			SetEvent(m_hPumpOnEvent);
		}
		else
		{
			strRecvTmpMsg = _T("ERROR");
			SetEvent(m_hPumpOnEvent);
			SetEvent(m_hPumpOffEvent);
		}

	}
	// 실제 회전속도 (hz)
	else if (StrRecvTmpMsgCommand == "309")
	{
		//strRecvTmpMsg = strRecvTmpMsg.Mid(10, 6);
		int currentTmphz = atoi(StrRecvTmpMsgSubDataCommand);

		if (StrRecvTmpMsgSubDataCommand == "000000")
		{
			str_mc_tmp_ReceiveDataState = _T("Stop");
			str_mc_tmp_ReceiveDataOnOffState = _T("TMP Turn Off");
		}
		if (StrRecvTmpMsgSubDataCommand == "000525")
		{
			str_mc_tmp_ReceiveDataState = _T("Running");
			str_mc_tmp_ReceiveDataOnOffState = _T("TMP Turn On");
		}
		else if (currentTmphz < pre_data)
		{
			str_mc_tmp_ReceiveDataState = _T("Deceleration");
			SetEvent(m_hPumpOffEvent);
		}
		else if (currentTmphz > pre_data)
		{
			str_mc_tmp_ReceiveDataState = _T("Acceleration");
			SetEvent(m_hPumpOnEvent);
		}
		else if (currentTmphz == pre_data)
		{
			cnt++;
			if (cnt == 2000) {
				str_mc_tmp_ReceiveDataState = _T("Running");
				cnt = 0;
			}
		}

		//mc_tmp_ReState = "TMP RUNNING";
		pre_data = atoi(StrRecvTmpMsgSubDataCommand);
		str_mc_tmp_ReceiveDataHz = StrRecvTmpMsgSubDataCommand + " Hz";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: " + str_mc_tmp_ReceiveDataState + " ::  " + str_mc_tmp_ReceiveDataHz))); //통신 상태 기록.
	}
	////목표 속도 도달 
	else if (StrRecvTmpMsgCommand == "306")
	{
		if (StrRecvTmpMsgSubDataCommand == "000000") m_Mc_Tmp_Speed_SetOn = false;
		else if (StrRecvTmpMsgSubDataCommand == "111111") m_Mc_Tmp_Speed_SetOn = true;
		else m_Mc_Tmp_Speed_SetOn = false;
	}
	//설정 Hz
	else if (StrRecvTmpMsgCommand == "308")
	{
		str_mc_tmp_ReveiveDataSetHz = StrRecvTmpMsgSubDataCommand + " Hz";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 설정 Hz  :: " + str_mc_tmp_ReveiveDataSetHz))); //통신 상태 기록.
	}
	//설정 rpm
	else if (StrRecvTmpMsgCommand == "397")
	{
		str_mc_tmp_ReveiveDataSetRpm = StrRecvTmpMsgSubDataCommand + " rpm";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 설정 Rpm :: " + str_mc_tmp_ReveiveDataSetRpm))); //통신 상태 기록.
	}
	//현재 rpm
	else if (StrRecvTmpMsgCommand == "398")
	{
		str_mc_tmp_ReceiveDataRpm = StrRecvTmpMsgSubDataCommand + " rpm";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 현재 Rpm :: " + str_mc_tmp_ReceiveDataRpm))); //통신 상태 기록.
	}
	//전자 장치 온도
	else if (StrRecvTmpMsgCommand == "326")
	{
		str_mc_tmp_ReceiveDataTemperature_1 = StrRecvTmpMsgSubDataCommand + "℃";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 전자 장치 온도 :: " + str_mc_tmp_ReceiveDataTemperature_1))); //통신 상태 기록.
		// 000042
	}
	//펌프 하단부 온도
	else if (StrRecvTmpMsgCommand == "330")
	{
		str_mc_tmp_ReceiveDataTemperature_2 = StrRecvTmpMsgSubDataCommand + "℃";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 펌프 하단부 온도 :: " + str_mc_tmp_ReceiveDataTemperature_2))); //통신 상태 기록.
		// 000022
	}
	//모터온도	
	else if (StrRecvTmpMsgCommand == "346")
	{
		str_mc_tmp_ReceiveDataTemperature_3 = StrRecvTmpMsgSubDataCommand + "℃";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 모터 온도 :: " + str_mc_tmp_ReceiveDataTemperature_3))); //통신 상태 기록.
		// 000042
	}
	//베어링 온도	
	else if (StrRecvTmpMsgCommand == "342")
	{
		str_mc_tmp_ReceiveDataTemperature_4 = StrRecvTmpMsgSubDataCommand + "℃";
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: 베어링 온도 :: " + str_mc_tmp_ReceiveDataTemperature_4))); //통신 상태 기록.
	}
	//오류코드
	else if (StrRecvTmpMsgCommand == "303")
	{
		StrRevvTmpMsgErrWarCommand = StrRecvTmpMsgSubDataCommand.Mid(1, 3);
		if (StrRevvTmpMsgErrWarCommand == "Err")
		{
			str_mc_tmp_Error_War_State = "ERORR";
			str_mc_tmp_ReceiveDataState = "Error";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  :: " + str_mc_tmp_ReceiveDataState))); //통신 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE : " + str_mc_tmp_ReceiveDataState))); //ERROR 상태 기록
		}

		if (StrRecvTmpMsgSubDataCommand == "Err001")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err001 ";  // 과잉 회전 속도 문제
			str_mc_tmp_ReceiveDataErrorState = "과잉 회전 속도";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err002")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err002 "; // 과잉 전압 문제
			str_mc_tmp_ReceiveDataErrorState = "과잉 전압";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err006")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err006 "; //: 런업 오류
			str_mc_tmp_ReceiveDataErrorState = "런업 오류(확인필요)";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err007")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err007 "; // 작동유 낮음
			str_mc_tmp_ReceiveDataErrorState = "작동유 낮음";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err008")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err008 "; // 전자드라이브 유닛 <-> 터보 펌프 연결 결함
			str_mc_tmp_ReceiveDataErrorState = "전자드라이브 유닛 연결 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err010")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err010 "; // 내부 장치 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 장치 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err021")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err021 "; // 전자 드라이브 유닛이 터보 펌프를 감지 못함
			str_mc_tmp_ReceiveDataErrorState = "전자드라이브 유닛 터보 펌프 감지 못함 발생";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err041")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err041 "; // 전자 드라이브 유닛이 터보 펌프를 감지 못함
			str_mc_tmp_ReceiveDataErrorState = "드라이브 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err043")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err043 "; // 내부 구성 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 구성 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err044")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err044 "; // 과잉 온도, 전자 장치 ( 불충분한 냉각 )
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도, 전자 장치";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err045")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err045 "; // 과잉 온도, 모터 ( 불충분한 냉각 )
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도, 모터";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err046")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err046 "; // 내부 초기화 오류 ( 장치 결함 )
			str_mc_tmp_ReceiveDataErrorState = "내부 초기화 오류 (장치 결함)";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err073")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err073 "; // 축 방향의 자기 베어링 과부하 (압력 상승률이 너무 높음)
			str_mc_tmp_ReceiveDataErrorState = "축 방향의 자기 베어링 과부하 (압력 상승률이 너무 높음)";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err074")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err074 "; // 방시상의 자기 베어링 과부하 (압력 상승률이 너무 높음)
			str_mc_tmp_ReceiveDataErrorState = "방시상의 자기 베어링 과부하 (압력 상승률이 너무 높음)";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err089")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err089 "; // 로터 불안정
			str_mc_tmp_ReceiveDataErrorState = "로터 불안정";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err091")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err091 "; //내부 장치 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 장치 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err092")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err092 "; //알수 없는 연결 패널
			str_mc_tmp_ReceiveDataErrorState = "알수 없는 연결 패널";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err093")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err093 "; // 모터 온도 평가 결함
			str_mc_tmp_ReceiveDataErrorState = "모터 온도 평가 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err094")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err094 "; // 전자 장치 온도 평가 결함
			str_mc_tmp_ReceiveDataErrorState = "전자 장치 온도 평가 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err098")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err098 "; // 내부 통신 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 통신 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err107")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err107 "; // 최종 단계 그룹 오류
			str_mc_tmp_ReceiveDataErrorState = "최종 단계 그룹 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err108")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err108 "; // 회전 속도 측정 결함
			str_mc_tmp_ReceiveDataErrorState = "회전 속도 측정 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err109")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err109 "; // 소프트웨어 릴리스 되지 않음
			str_mc_tmp_ReceiveDataErrorState = "소프트웨어 릴리스 되지 않음";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err110")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err110 "; // 작동유 센서 결함
			str_mc_tmp_ReceiveDataErrorState = "작동유 센서 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err111")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err111 "; // 작동유 펌프 통신 오류
			str_mc_tmp_ReceiveDataErrorState = "작동유 펌프 통신 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err112")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err112 "; // 작동유 펌프 그룹 오류
			str_mc_tmp_ReceiveDataErrorState = "작동유 펌프 그룹 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err113")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err113 "; //: 과잉온도,펌프하단부
			str_mc_tmp_ReceiveDataErrorState = "로터온도부정확";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err114")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err114 "; //최종 단계 온도 평가 결함
			str_mc_tmp_ReceiveDataErrorState = "최종 단계 온도 평가 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err117")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err117 "; //: 과잉온도,펌프하단부
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도 펌프 하단부";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err118")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err118 "; // 과잉 온도 ,최동 단계
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도 최종 단계";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err119")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err119 "; //: 과잉온도,베어링
			str_mc_tmp_ReceiveDataErrorState = "과잉 온도 베어링";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err143")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err143 "; //: 과잉온도,작동유 펌프
			str_mc_tmp_ReceiveDataErrorState = "작동유 펌프 과잉 온도";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err777")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err777 "; // 공칭 회전 속도가 확인 되지 않음
			str_mc_tmp_ReceiveDataErrorState = "공칭 회전 속도가 확인 되지 않음";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err800")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err800 "; //자기 베어링 과류
			str_mc_tmp_ReceiveDataErrorState = "자기 베어링 과류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err802")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err802 "; //자기 베어링 센서 기술 결함
			str_mc_tmp_ReceiveDataErrorState = "자기 베어링 센서 기술 결함";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err810")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err810 "; //내부 구성 오류
			str_mc_tmp_ReceiveDataErrorState = "내부 구성 오류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err815")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err815 "; //자기 베어링 과류
			str_mc_tmp_ReceiveDataErrorState = "자기 베어링 과류";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err890")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err890 "; //안전 베어링 마모
			str_mc_tmp_ReceiveDataErrorState = "안전 베어링 마모";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "Err891")
		{
			str_mc_tmp_ReceiveDataErrorCode = "Err891 "; //로터 불균형이 너무 높음
			str_mc_tmp_ReceiveDataErrorState = "로터 불균형이 너무 높음";
			SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: Error  ::  TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
			SaveLogFile("MC_TMP_ERROR", _T((LPSTR)(LPCTSTR)("TMP ERROR CODE ::" + str_mc_tmp_ReceiveDataErrorCode + ",  ERROR : " + str_mc_tmp_ReceiveDataErrorState))); //ERROR 상태 기록.
		}
		else if (StrRecvTmpMsgSubDataCommand == "000000")
		{
			str_mc_tmp_ReceiveDataErrorCode = "No Error";
			str_mc_tmp_ReceiveDataErrorState = "No Error";
			str_mc_tmp_Error_War_State = "No Error";
		}

	}
	// UnKnown.
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC_TMP [RECEIVE]: UnKnown Command :: " + StrRecvTmpMsgCommand + ",  UnKnown SubData :: " + StrRecvTmpMsgSubDataCommand))); //통신 상태 기록.
	}

	StrTempLog.Format(_T("MC_TMP_HZ : %s , TMP STATE : %s , TMP Error Code :  %s "), str_mc_tmp_ReceiveDataHz, str_mc_tmp_ReceiveDataState, str_mc_tmp_ReceiveDataErrorCode);
	SaveLogFile("MC_TMP_LOG", (LPSTR)(LPCTSTR)StrTempLog); //통신 상태 기록.
}


int CTurboPumpMCDlg::Mc_Tmp_ReceiveData()
{


	int ret = 0;

	SetDlgItemText(IDC_MC_TMP_HZ, str_mc_tmp_ReceiveDataHz);					// 속도 (hz)
	SetDlgItemText(IDC_MC_TMP_RPM, str_mc_tmp_ReceiveDataRpm);					// 속도 (rpm)
	SetDlgItemText(IDC_MC_TMP_HZ_SET, str_mc_tmp_ReveiveDataSetHz);				// 설정 속도 (hz)
	SetDlgItemText(IDC_MC_TMP_RPM_SET, str_mc_tmp_ReveiveDataSetRpm);			// 설정 속도 (rpm)
	SetDlgItemText(IDC_MC_TMP_STATE, str_mc_tmp_ReceiveDataState);				// tmp 상태
	SetDlgItemText(IDC_MC_TMP_ERROR_CODE, str_mc_tmp_ReceiveDataErrorCode);		// Error code
	SetDlgItemText(IDC_MC_TMP_ERROR_STATE, str_mc_tmp_ReceiveDataErrorState);	// Error 설명
	SetDlgItemText(IDC_MC_TMP_ON_OFF, str_mc_tmp_ReceiveDataOnOffState);		// ON/OFF 상태
	SetDlgItemText(IDC_MC_TMP_TEMP1, str_mc_tmp_ReceiveDataTemperature_1);		// 전자장치온도
	SetDlgItemText(IDC_MC_TMP_TEMP2, str_mc_tmp_ReceiveDataTemperature_2);		//펌프하단부온도
	SetDlgItemText(IDC_MC_TMP_TEMP3, str_mc_tmp_ReceiveDataTemperature_3);		//모터온도
	SetDlgItemText(IDC_MC_TMP_ERR_WAR, str_mc_tmp_Error_War_State);
	

	//StrTempLog.Format(_T("MC_TMP_HZ : %s , TMP STATE : %s , TMP Error Code :  %s "), str_mc_tmp_ReceiveDataHz, str_mc_tmp_ReceiveDataState, str_mc_tmp_ReceiveDataErrorCode);
	//SaveLogFile("MC_TMP_LOG", (LPSTR)(LPCTSTR)StrTempLog); //통신 상태 기록.
	//SaveLogFile("MC_TMP_LOG", _T((LPSTR)(LPCTSTR)(" MC_TMP_HZ : " + str_mc_tmp_ReceiveDataHz + ",   TMP STATE : " + str_mc_tmp_ReceiveDataState + ", TMP Error Code : " + str_mc_tmp_ReceiveDataErrorCode))); //통신 상태 기록.
	//SaveLogFile("MC_TMP_LOG", _T(" MC_TMP_HZ : " + str_mc_tmp_ReceiveDataHz + ",   TMP STATE : " + str_mc_tmp_ReceiveDataState + ", TMP Error Code : " + str_mc_tmp_ReceiveDataErrorCode)); //통신 상태 기록.

	if (m_Mc_Tmp_Speed_SetOn == true)
	{
		((CStatic*)GetDlgItem(IDC_MC_ICON_SPEED_ON))->SetIcon(m_LedIcon[1]);
	}
	else if (m_Mc_Tmp_Speed_SetOn == false)
	{
		((CStatic*)GetDlgItem(IDC_MC_ICON_SPEED_ON))->SetIcon(m_LedIcon[0]);
	}

	if (str_mc_tmp_ReceiveDataState == "Stop")
	{
		m_mc_tmp_state = TMP_OFFLINE;
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[2]);

		if (g_pIO->m_nERROR_MODE == RUN)
		{
			SaveLogFile("MC_TMP_LOG", _T((LPSTR)(LPCTSTR)("ERROR MODE 발생!")));
			ErrorModeOn(); // TEST 및 수정 필요. ( Sequence 에 따른 Error mode 확인 필요 )
		}
	}
	else if (str_mc_tmp_ReceiveDataState == "Running")
	{
		m_mc_tmp_state = TMP_NORMAL;
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	}
	else if (str_mc_tmp_ReceiveDataState == "Deceleration")
	{
		m_mc_tmp_state = TMP_DECELERATION;
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[0]);
	}
	else if (str_mc_tmp_ReceiveDataState == "Acceleration")
	{
		m_mc_tmp_state = TMP_ACCELERATION;
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_1))->SetIcon(m_LedIcon[0]);
	}
	else if (str_mc_tmp_ReceiveDataState == "Error")
	{
		((CStatic*)GetDlgItem(IDC_MC_ICON_STATE_2))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	}

	return ret;
}

UINT CTurboPumpMCDlg::Mc_Tmp_UpdataThread(LPVOID pParam)
{
	int ret = 0;

	CTurboPumpMCDlg*  mc_tmp_runthread = (CTurboPumpMCDlg*)pParam;

	while (!mc_tmp_runthread->m_mc_tmp_ThreadExitFlag)
	{
		mc_tmp_runthread->TmpSendUpdate();
		Sleep(1000);
	}

	return 0;
}

void CTurboPumpMCDlg::OnTimer(UINT_PTR nIDEvent)
{

	KillTimer(nIDEvent);

	if (nIDEvent == MC_TMP_UPDATE_TIMER)
	{
		if (!m_mc_tmp_ThreadExitFlag)
		{
			Mc_Tmp_ReceiveData();
			SetTimer(MC_TMP_UPDATE_TIMER, 100, NULL);
		}
	}

	__super::OnTimer(nIDEvent);
}

int CTurboPumpMCDlg::Mc_Tmp_GetStatus()
{
	int ret = -1;

	switch (m_mc_tmp_state)
	{
	case TMP_ERROR:
		ret = TMP_ERROR;
		break;
	case TMP_OFFLINE:
		ret = TMP_OFFLINE;
		break;
	case TMP_NORMAL:
		ret = TMP_NORMAL;
		break;
	case TMP_ACCELERATION:
		ret = TMP_ACCELERATION;
		break;
	case TMP_DECELERATION:
		ret = TMP_DECELERATION;
		break;
	default:
		break;
	}

	return ret;
}

void CTurboPumpMCDlg::OnBnClickedMcTmpOn()
{
	int nRet = -1;

	if (m_Mc_Tmp_OpenPort_State == TRUE)
	{

		Mc_Tmp_On();

		//str_mc_tmp = "0021001006111111016\r";
		//nRet = SendData((LPSTR)(LPCTSTR)str_mc_tmp, 300);
		//((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[1]);
		//((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	}

	//GetDlgItem(IDC_MC_TMP_ON)->EnableActiveAccessibility();
	//GetDlgItem(IDC_MC_TMP_ON)->EnableWindow(false);
	//GetDlgItem(IDC_MC_TMP_OFF)->EnableWindow(true);
}

void CTurboPumpMCDlg::OnBnClickedMcTmpOff()
{
	int nRet = -1;

	if (m_Mc_Tmp_OpenPort_State == TRUE)
	{

		Mc_Tmp_Off();
		
		//002 10 010 06 000000
		//str_mc_tmp = "0021001006000000010\r";
		//nRet = SendData((LPSTR)(LPCTSTR)str_mc_tmp, 300);
		//GetDlgItem(IDC_MC_TMP_OFF)->EnableActiveAccessibility();
		//GetDlgItem(IDC_MC_TMP_OFF)->EnableWindow(false);
		//GetDlgItem(IDC_MC_TMP_ON)->EnableWindow(true);
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[2]);
	}
}

void CTurboPumpMCDlg::OnBnClickedMcTmpErrorReset()
{
	int nRet = -1;


	//002 10 009 06 111111  
	//str_mc_tmp = "0021000906111111024\r";
	//nRet = SendData((LPSTR)(LPCTSTR)str_mc_tmp, 300);
	nRet = g_pMCTmp_IO->ResetPump();
}

int CTurboPumpMCDlg::Mc_Tmp_Off()
{
	int nRet = -1;

	//str_mc_tmp = "0021001006000000010\r";
	//int nRet = SendData((LPSTR)(LPCTSTR)str_mc_tmp, 300);

	nRet = g_pMCTmp_IO->SetPUmpOff();

	if (nRet != 0)
	{
		return RUN;
		AfxMessageBox(_T("MC TMP OFF 에 실패 하였습니다"));
	}
	else
	{
		return RUN_OFF;
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[2]);
	}
}

int CTurboPumpMCDlg::Mc_Tmp_On()
{
	int nRet = -1;


	//str_mc_tmp = "0021001006111111016\r";
	//nRet = SendData((LPSTR)(LPCTSTR)str_mc_tmp, 300);
	
	nRet = g_pMCTmp_IO->SetPumpOn();
	if (nRet != 0)
	{
		return RUN_OFF;
		AfxMessageBox(_T("MC TMP ON 에 실패 하였습니다"));
	}
	else
	{
		return RUN;
		((CStatic*)GetDlgItem(IDC_MC_ICON_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_MC_ICON_OFF))->SetIcon(m_LedIcon[2]);
	}
}

void CTurboPumpMCDlg::TmpSendUpdate()
{
	int ret = -1;

	// TMP HZ READ
	ret = GetTmpHz();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpHz() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpHz()")));	//통신 상태 기록.
	}

	//ERROR VIEW	
	ret = GetErrorView();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetErrorView() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetErrorView()")));	//통신 상태 기록.
	}


	//셋팅 Hz
	ret = GetTmpSetHz();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetHz() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetHz()")));	//통신 상태 기록.
	}

	//셋팅 rpm
	ret = GetTmpSetRpm();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetRpm() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetRpm()")));	//통신 상태 기록.
	}

	//실제 rpm
	ret = GetTmpRpm();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpRpm() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpRpm()")));	//통신 상태 기록.
	}

	//목표 속도 도달 
	ret = GetTmpSetSpeedOn();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetSpeedOn() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpSetSpeedOn()")));	//통신 상태 기록.
	}

	//전자 장치 온도
	ret = GetTmpElectronicDeviceTemp();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpElectronicDeviceTemp() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpElectronicDeviceTemp()")));	//통신 상태 기록.
	}


	//펌프 하단부 온도
	ret = GetTmpLowerPartTemp();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpLowerPartTemp() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpLowerPartTemp()")));	//통신 상태 기록.
	}


	//모터온도	
	ret = GetTmpMotorTemp();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpMotorTemp() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpMotorTemp()")));	//통신 상태 기록.
	}


	//베어링 온도	
	ret = GetTmpBearingTemp();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpBearingTemp() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("MC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> MC TMP [SEND] : GetTmpBearingTemp()")));	//통신 상태 기록.
	}
}

void CTurboPumpMCDlg::OnBnClickedMcTmpLock()
{
	if (m_lock_state)
	{
		GetDlgItem(IDC_MC_TMP_ON)->EnableActiveAccessibility();
		GetDlgItem(IDC_MC_TMP_OFF)->EnableActiveAccessibility();
		GetDlgItem(IDC_MC_TMP_ON)->EnableWindow(false);
		GetDlgItem(IDC_MC_TMP_OFF)->EnableWindow(false);
		m_lock_state = 0;
	}
	else
	{
		GetDlgItem(IDC_MC_TMP_ON)->EnableWindow(true);
		GetDlgItem(IDC_MC_TMP_OFF)->EnableWindow(true);
		m_lock_state = 1;
	}
}


void CTurboPumpMCDlg::ErrorModeOn()
{
	if (g_pIO->MC_Dry_Pump_off())
	{
		CString log_str = " MAIN_ERROR_ON() : Error 발생 후 MC TMP STOP , MC Dry Pump Off 실행 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		//////////////////////////////////////////////////////////////////
		// TMP 완전 멈춤, 그후 DRY PUMP OFF 후 MC FORELINE GATE CLOSE
		//////////////////////////////////////////////////////////////////

		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤, 그 후 MC Foreline Gate  Close !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		if (g_pIO->Close_MC_TMP1_ForelineValve() != OPERATION_COMPLETED)
		{
			
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 명령 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, log_str);
			g_pLog->Display(0, g_pIO->Log_str);
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 명령 에러 발생으로 인해 재 시도 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, log_str);
			g_pLog->Display(0, g_pIO->Log_str);
		
			if (g_pIO->Close_MC_TMP1_ForelineValve() != OPERATION_COMPLETED)
			{
				log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 명령 에러 발생 !";
				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
				g_pLog->Display(0, log_str);
				g_pLog->Display(0, g_pIO->Log_str);
				return;
			}
		}

		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_MC_TMP1_ForelineValve_Open() == VALVE_CLOSE) break;
		}

		if (g_pIO->Is_MC_TMP1_ForelineValve_Open() != VALVE_CLOSED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 확인 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			g_pLog->Display(0, log_str);

			return ;
		}

		log_str = "MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 확인 완료!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

	}
	else
	{
		CString log_str = " MAIN_ERROR_ON() : Error 발생 후 MC TMP STOP , MC Dry Pump Off 실행 Error (확인필요) !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}
}