
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

import PythonAlgorithm
#plt.ioff() plt.draw()
#matplotlib.is_interactive()



x = np.linspace(0, 10, 101)
y = PythonAlgorithm.func(x, 1, 2)
yn = y + 0.9*np.random.normal(size=len(x))

PythonAlgorithm.curveFitTest(x, yn)





#def func(x, a, b):
#    return a*x + b


#x = np.linspace(0, 10, 101)
#print(x)

#y = func(x, 1, 2)
#print(y)

#np.random.seed(1)


#yn = y + 0.9*np.random.normal(size=len(x))


#popt, pcov = curve_fit(func, x, yn)
#print(popt) #popt는 주어진 func 모델에서 가장 최고의 fit values를 보여줍니다.
#print(pcov) #pcov의 대각성분들은 각 parameter들의 variances 입니다.


#plt.figure(1)
#plt.scatter(x, yn, marker='.')
#plt.plot(x, y, linewidth=2)
#plt.plot(x, func(x, *popt), color='red', linewidth=2)
#plt.legend(['Original', 'Best Fit'], loc=2)
#plt.show()


## Gaussian model을 리턴합니다.
#def func(x, a, b, c):
#    return a*np.exp(-(x-b)**2/(2*c**2))

## 마찬가지로 0~10까지 100개 구간으로 나눈 x를 가지고 
#x = np.linspace(0, 10, 100)
#y = func(x, 1, 5, 2) # 답인 y들과
#yn = y + 0.2*np.random.normal(size=len(x)) # noise가 낀 y값들을 만듭니다.

### 그런 후에 curve_fit을 하고 best fit model의 결과를 출력합니다.
#popt, pcov = curve_fit(func, x, yn)
#print(popt)
#print(pcov)

#plt.figure(3)
#plt.scatter(x, yn, marker='.')
#plt.plot(x, y, linewidth=2, color='blue')
#plt.plot(x, func(x, *popt), color='red', linewidth=2)
#plt.legend(['Original', 'Best Fit'], loc=2)
#plt.xlabel('x')
#plt.ylabel('y')
#plt.show()
#plt.show()