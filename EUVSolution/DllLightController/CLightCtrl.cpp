#include "pch.h"
#include "CLightCtrl.h"

CLightCtrl::CLightCtrl()
{
	m_bLightOnState = FALSE;
	m_nLightValue	= 0;
	m_bErrOccur		= FALSE;
	m_nErrCode		= -1;

	m_strReceiveEndOfStreamSymbol = _T("\r");

	m_hAckEvent		 = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hGetStateEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hGetValueEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
}

CLightCtrl::~CLightCtrl()
{
	CloseHandle(m_hAckEvent);
	CloseHandle(m_hGetStateEvent);
	CloseHandle(m_hGetValueEvent);
}

int	CLightCtrl::SendData(int nSize, BYTE *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	CString strSendData = TEXT("");
	CString Temp = TEXT("");

	for (int nIdx = 0; nIdx < nSize; nIdx++)
	{
		Temp.Format("%02X ", lParam[nIdx]);
		strSendData += Temp;
	}

	SaveLogFile("LightCtrl_Com", _T((LPSTR)(LPCTSTR)("PC -> LightController : " + strSendData)));	//통신 상태 기록.
	nRet = Send(nSize, lParam, nTimeOut);
	if (nRet != 0)
	{
		Temp.Format(_T("PC -> LightController : SEND FAIL (%d)"), nRet);
		SaveLogFile("LightCtrl_Com", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
	}

	return nRet;
}

int CLightCtrl::SendData(char * lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	CString strSendData = TEXT("");
	CString Temp = TEXT("");

	for (int nIdx = 0; nIdx < sizeof(lParam); nIdx++)
	{
		Temp.Format("%02X ", lParam[nIdx]);
		strSendData += Temp;
	}

	SaveLogFile("LightCtrl_Com", _T((LPSTR)(LPCTSTR)("PC -> LightController : " + strSendData)));	//통신 상태 기록.
	nRet = Send(lParam, nTimeOut);
	if (nRet != 0)
		SaveLogFile("LightCtrl_Com", _T("PC -> LightController : OFFLINE"));	//통신 상태 기록.

	return nRet;
}

int CLightCtrl::ReceiveData(char * lParam, DWORD dwRead)
{
	CString strRecvData;
	CString Temp = TEXT("");

	memset(m_RecvData, '\0', dwRead);
	memcpy(m_RecvData, lParam, sizeof(BYTE) * dwRead);

	m_nErrCode = 0;

	if (m_RecvData[1] == 0x00)
	{
		SetEvent(m_hAckEvent);
	}
	else if (m_RecvData[1] == 0x80)	//Error Local Mode
	{
		m_nErrCode = -2;
		SetEvent(m_hAckEvent);
	}
	else if (m_RecvData[1] == 0x81)	//Error Channel
	{
		m_nErrCode = -3;
		SetEvent(m_hAckEvent);
	}
	else if (m_RecvData[1] == 0x82)	//Error Command
	{
		m_nErrCode = -4;
		SetEvent(m_hAckEvent);
	}
	else if (m_RecvData[1] == 0x83)	//Error Parameter
	{
		m_nErrCode = -5;
		SetEvent(m_hAckEvent);
	}
	else if (m_RecvData[1] == 0x84)	//Error Checksum
	{
		m_nErrCode = -6;
		SetEvent(m_hAckEvent);
	}
	else if (m_RecvData[1] == 0x11)	//Light Value
	{
		SetEvent(m_hGetValueEvent);
		m_nLightValue = (int)m_RecvData[3];
	}
	else if (m_RecvData[1] == 0x12)	//Light OnOff State
	{
		SetEvent(m_hGetStateEvent);
		if (m_RecvData[2] != 0x00)
			m_bLightOnState = TRUE;
		else
			m_bLightOnState = FALSE;
	}

	for (int nIdx = 0; nIdx < dwRead; nIdx++)
	{
		Temp.Format("%02X ", m_RecvData[nIdx]);
		strRecvData += Temp;
	}

	SaveLogFile("LightCtrl_Com", _T((LPSTR)(LPCTSTR)("LightController -> PC : " + strRecvData))); //통신 상태 기록.

	return 0;
}

int CLightCtrl::SetLightValue(int nValue)
{
	int nRet = 0;

	BYTE _data[6];
	_data[0] = 0xFF;			//header
	_data[1] = 0x01;			//command
	_data[2] = 0x01;			//channel
	_data[3] = 0x00;			//data(H)
	_data[4] = nValue;	//data(L)
	_data[5] = GetCheckSum(_data, sizeof(_data) - 1);

	ResetEvent(m_hAckEvent);
	nRet = SendData(sizeof(_data), _data);
	if (nRet != 0)
		SetEvent(m_hAckEvent);
	else
		nRet = WaitExecAckEvent(1000);

	return nRet;
}

int CLightCtrl::GetLightValue()
{
	int nRet = 0;

	BYTE _data[3];
	_data[0] = 0xFF;	//header
	_data[1] = 0x11;	//command
	_data[2] = GetCheckSum(_data, sizeof(_data) - 1);

	ResetEvent(m_hGetValueEvent);
	nRet = SendData(sizeof(_data), _data);
	if (nRet != 0)
		SetEvent(m_hGetValueEvent);
	else
		nRet = WaitExecGetValueEvent(1000);

	return nRet;
}

int CLightCtrl::SetLightOnOff(BOOL bValue)
{
	int nRet = 0;

	BYTE _data[5];
	_data[0] = 0xFF;
	_data[1] = 0x02;
	_data[2] = 0x01;
	_data[3] = bValue == TRUE ? 0x01 : 0x00;
	_data[4] = GetCheckSum(_data, sizeof(_data) - 1);

	ResetEvent(m_hAckEvent);
	nRet = SendData(sizeof(_data), _data);
	if (nRet != 0)
		SetEvent(m_hAckEvent);
	else
		nRet = WaitExecAckEvent(1000);

	return nRet;
}

int CLightCtrl::GetLightOnOffState()
{
	int nRet = 0;

	BYTE _data[3];
	_data[0] = 0xFF;
	_data[1] = 0x12;
	_data[2] = GetCheckSum(_data, sizeof(_data) - 1);

	ResetEvent(m_hGetStateEvent);
	nRet = SendData(sizeof(_data), _data);
	if (nRet != 0)
		SetEvent(m_hGetStateEvent);
	else
		nRet = WaitExecGetStateEvent(1000);

	return nRet;
}

BYTE CLightCtrl::GetCheckSum(BYTE* Command, int leng)
{
	BYTE btChecksum = 0x00;

	for (int nIdx = 0; nIdx < leng; nIdx++)
	{
		btChecksum ^= Command[nIdx];
	}

	return btChecksum;
}

int CLightCtrl::GetErrorCode(int nCode)
{
	if (nCode == -1) return -16001;
	else if (nCode == -2) return -16002;
	else if (nCode == -3) return -16003;
	else if (nCode == -4) return -16004;
	else if (nCode == -5) return -16005;
	else if (nCode == -6) return -16006;
	else return -16007;
}

int CLightCtrl::WaitExecAckEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hAckEvent, nTimeout) != WAIT_OBJECT_0)
		return -16001;

	if (m_nErrCode != 0)
		return GetErrorCode(m_nErrCode);

	return 0;
}

int CLightCtrl::WaitExecGetValueEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hGetValueEvent, nTimeout) != WAIT_OBJECT_0)
		return -16001;

	if (m_nErrCode != 0)
		return GetErrorCode(m_nErrCode);

	return 0;
}

int CLightCtrl::WaitExecGetStateEvent(int nTimeout)
{
	if (WaitForSingleObject(m_hGetStateEvent, nTimeout) != WAIT_OBJECT_0)
		return -16001;

	if (m_nErrCode != 0)
		return GetErrorCode(m_nErrCode);

	return 0;
}
