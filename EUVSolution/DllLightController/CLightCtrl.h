#pragma once

class AFX_EXT_CLASS CLightCtrl : public CSerialCom
{
public:
	CLightCtrl();
	~CLightCtrl();

	virtual	int				SendData(int nSize, BYTE *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual int				ReceiveData(char *lParam, DWORD dwRead);

	

protected:
	/**
	*@var BYTE m_RecvData[10]
	수신 받은 데이터
	*/
	BYTE					m_RecvData[10];
	
	/**
	*@var BOOL m_nLightValue
	조명 밝기 값
	*/
	int						m_nLightValue;

	/**
	*@var BOOL m_nErrCode
	에러 코드
	*/
	int						m_nErrCode;

	/**
	*@var BOOL m_bLightOnState
	조명 On 상태 확인 변수
	*/
	BOOL					m_bLightOnState;

	/**
	*@var BOOL m_bErrOccur
	알람 발생 확인 변수
	*/
	BOOL					m_bErrOccur;

	/**
	*@var HANDLE m_hAckEvent
	ACK 메시지 수신 이벤트
	*/
	HANDLE					m_hAckEvent;

	/**
	*@var HANDLE m_hGetStateEvent
	상태 값 수신 이벤트
	*/
	HANDLE					m_hGetStateEvent;

	/**
	*@var HANDLE m_hGetValueEvent
	밝기 값 수신 이벤트
	*/
	HANDLE					m_hGetValueEvent;

	/**
	*@fn int SetLightValue(int nValue)
	*@brief 조명 밝기 셋팅
	*@date 2020/12/17
	*@param 밝기 값 (0~255)
	*/
	int						SetLightValue(int nValue);

	/**
	*@fn int GetLightValue()
	*@brief 조명 밝기 셋팅
	*@date 2020/12/17
	*@param 밝기 값 (0~255)
	*/
	int						GetLightValue();

	/**
	*@fn int SetLightOnOff(BOOL bValue)
	*@brief 조명 OnOff 컨트롤
	*@date 2020/12/17
	*@param FALSE : Off, TRUE : On	
	*/
	int						SetLightOnOff(BOOL bValue);

	/**
	*@fn int GetLightOnOffState()
	*@brief 조명 OnOff 상태
	*@date 2020/12/17
	*/
	int						GetLightOnOffState();

	/**
	*@fn int GetErrorCode(int nCode)
	*@brief 에러 코드 값 반환
	*@date 2020/12/17
	*/
	int						GetErrorCode(int nCode);

	/**
	*@fn int GetCheckSum(BYTE* Command, int leng)
	*@brief 체크섬 값 반환
	*@date 2020/12/17
	*@param Command : 명령메시지, leng : 명령어 길이
	*/
	BYTE					GetCheckSum(BYTE* Command, int leng);

	/**
	*@fn int WaitExecAckEvent(int nTimeout)
	*@brief Ack 메시지 수신 타임아웃 체크
	*@date 2020/12/17
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecAckEvent(int nTimeout);

	/**
	*@fn int WaitExecGetValueEvent(int nTimeout)
	*@brief 조명 밝기 값 수신 타임아웃 체크
	*@date 2020/12/17
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecGetValueEvent(int nTimeout);

	/**
	*@fn int WaitExecGetStateEvent(int nTimeout)
	*@brief 조명 상태 수신 타임아웃 체크
	*@date 2020/12/17
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecGetStateEvent(int nTimeout);
};

