/**
 * Agilent TwisTorr304 TMP Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

class AFX_EXT_CLASS CAgilentTwisTorr304TMPCtrl : public CSerialCom
{
public:
	CAgilentTwisTorr304TMPCtrl();
	~CAgilentTwisTorr304TMPCtrl();

	virtual int				SendData(int nSize, char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int				ReceiveData(char *lParam, DWORD dwRead); ///< Recive
	virtual int				OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);

	void initialize();
	

	HANDLE						TmpOnOffEvent;

//	CString						StrLLCTmpReceiveDataHz;
//	CString						StrLLCTmpReceiveDataRpm;
//	CString						StrLLCTmpReceiveDataErrorCode;
//	CString						StrLLCTmpReceiveDataErrorState;
//	CString						StrLLCTmpReceiveDataTemperature;
//	CString						StrLLCTmpReceiveDataTemperatureHeatsink;
//	CString						StrLLCTmpReceiveDataTemperatureAir;
//	CString						StrLLCTmpReceiveDataState;

//	BYTE						m_BYTE[15];


	int LLCTmp_Monitor_Com();
	int LLCTmp_HzRead_Com();
	int LLCTmp_RpmRead_Com();
	int LLCTmp_ErrorCode_Check_Com();
	int LLCTmp_Temprerature_Check_Com();
	int LLCTmp_Controller_Heatsink_Temprerature_Check_Com();
	int LLCTmp_Controller_Air_Temprerature_Check_Com();
	
	int LLCTmp_On_Com();
	int LLCTmp_Off_Com();

	int WaitTmpOnOffEvent(int nTimeout);

	BOOL TmpExecutionFlag;
	BOOL LLC_TMP_Error_On;
};

