#include "stdafx.h"
#include "CAgilentTwisTorr304TMPCtrl.h"


CAgilentTwisTorr304TMPCtrl::CAgilentTwisTorr304TMPCtrl()
{
	//m_strReceiveEndOfStreamSymbol = _T("\r");
	//memset(m_BYTE, '\0', 15);
	//StrLLCTmpReceiveDataErrorState = _T("No Error");
	//TmpOnEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	//TmpOffEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	TmpOnOffEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	TmpExecutionFlag = FALSE;
	LLC_TMP_Error_On = FALSE;
}

CAgilentTwisTorr304TMPCtrl::~CAgilentTwisTorr304TMPCtrl()
{
	//StrLLCTmpReceiveDataHz.Empty();
	//StrLLCTmpReceiveDataState.Empty();
	//StrLLCTmpReceiveDataErrorCode.Empty();
	//StrLLCTmpReceiveDataTemperature.Empty();
	m_strSendMsg.Empty();
	CloseHandle(TmpOnOffEvent);
}

int	CAgilentTwisTorr304TMPCtrl::SendData(int nSize, char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	CString strSendData = TEXT("");
	CString StrTemp = TEXT("");

	for (int nIdx = 0; nIdx < nSize; nIdx++)
	{
		StrTemp.Format("%02X ", lParam[nIdx]);
		strSendData += StrTemp;
	}

	nRet = Send(lParam, nTimeOut);

	if (nRet != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (SEND): " + strSendData + "Send Fail")));	//통신 상태 기록.
		nRet = -81002;
	}
	else
	{
		if (TmpExecutionFlag)
		{
			SetEvent(TmpOnOffEvent);
			TmpExecutionFlag = FALSE;
		}
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (SEND): " + strSendData)));	//통신 상태 기록.
	}

	return nRet;
}

int CAgilentTwisTorr304TMPCtrl::ReceiveData(char *lParam, DWORD dwRead)
{
	int nRet = 0;

	return nRet;
}

/*
int CAgilentTwisTorr304TMPCtrl::ReceiveData(char *lParam, DWORD dwRead)
{
	int nRet = 0;

	memset(m_BYTE, '\0', 15);
	memcpy(m_BYTE, lParam, sizeof(BYTE) * 15);

	CString StrTemp = TEXT("");
	CString StrRecevieData = TEXT("");
	CString StrRecevieDataMid = TEXT("");
	CString StrRevevieDataResult = TEXT("");

	for (int i = 0; i < 15; i++)            //String으로 변환
	{
		if (m_BYTE[i] == 0) StrTemp = "0 ";
		else StrTemp.Format("%02x", m_BYTE[i]);
		StrRecevieData += StrTemp;
	}

	SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrRecevieData)));	//통신 상태 기록.

	if (m_BYTE[2] == 0x06)
	{
		StrLLCTmpReceiveDataState = "실행 완료"; // 명령수행완료 리턴 값
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x35)
	{
		if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x30)
		{
			StrLLCTmpReceiveDataState = "Stop";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x31)
		{
			StrLLCTmpReceiveDataState = "Working Intlk";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x32)
		{
			StrLLCTmpReceiveDataState = "Starting";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x33)
		{
			StrLLCTmpReceiveDataState = "Auto Tuning";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x34)
		{
			StrLLCTmpReceiveDataState = "Breaking";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x35)
		{
			StrLLCTmpReceiveDataState = "Running";
		}
		else if (m_BYTE[5] == 0x30 && m_BYTE[6] == 0x30 && m_BYTE[7] == 0x30 && m_BYTE[8] == 0x30 && m_BYTE[9] == 0x30 && m_BYTE[10] == 0x30 && m_BYTE[11] == 0x36)
		{
			StrLLCTmpReceiveDataState = "Fail";
		}
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.

	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x33) // hz
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataHz = StrRevevieDataResult + " hz"; // 현재 Hz 값
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataHz)));	//통신 상태 기록.

	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x32 && m_BYTE[4] == 0x36) // rpm
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataRpm = StrRevevieDataResult + " Rpm"; // 현재 Hz 값

		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataRpm)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x36) // Error code view
	{
		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataErrorCode = StrRevevieDataResult; // 현재 Error Code 값

		if (StrLLCTmpReceiveDataErrorCode == "0000000")
		{
			StrLLCTmpReceiveDataErrorState = _T("No Error");
			LLC_TMP_Error_On = FALSE;
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000001")
		{
			StrLLCTmpReceiveDataErrorState = _T("No Connection");
			LLC_TMP_Error_On = FALSE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000002") // HEX 2 , DEC 2, BIN 0000 0010
		{
			StrLLCTmpReceiveDataErrorState = _T("Pump OverTemp");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000004")// HEX 4 , DEC 4, BIN 0000 0100
		{
			StrLLCTmpReceiveDataErrorState = _T("Controll OverTemp");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000008")// HEX 8 , DEC 8, BIN 0000 1000
		{
			StrLLCTmpReceiveDataErrorState = _T("Power Fail");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000016") // HEX 10 , DEC 16, BIN 0001 0000
		{
			StrLLCTmpReceiveDataErrorState = _T("Aux Fail");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000032")// HEX 20 , DEC 32, BIN 0010 0000
		{
			StrLLCTmpReceiveDataErrorState = _T("Over Voltage");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000064")// HEX 40 , DEC 64, BIN 0100 0000
		{
			StrLLCTmpReceiveDataErrorState = _T("Over Voltage");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else if (StrLLCTmpReceiveDataErrorCode == "0000128")// HEX 80 , DEC 128, BIN 1000 0000
		{
			StrLLCTmpReceiveDataErrorState = _T("Too High Load");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}
		else
		{
			StrLLCTmpReceiveDataErrorState = _T("Error 발생 ( Define 되지 않은 Error) ");
			LLC_TMP_Error_On = TRUE;
			SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("LLC ERROR CODE: " + StrLLCTmpReceiveDataErrorCode + ", ERROR STATE : " + StrLLCTmpReceiveDataErrorState))); //상태 기록.
		}

		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataErrorCode + " ::: " + StrLLCTmpReceiveDataErrorState)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x30 && m_BYTE[4] == 0x34) // TMP_temprerature 
	{

		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataTemperature = StrRevevieDataResult; // 현재 온도 값
		StrLLCTmpReceiveDataTemperature += _T(" ℃");
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataTemperature)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x31 && m_BYTE[4] == 0x31) //  Controller Heatsink Temperature 
	{

		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataTemperatureHeatsink = StrRevevieDataResult; // 현재 온도 값
		StrLLCTmpReceiveDataTemperatureHeatsink += " ℃";
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataTemperatureHeatsink)));	//통신 상태 기록.
	}
	else if (m_BYTE[2] == 0x32 && m_BYTE[3] == 0x31 && m_BYTE[4] == 0x36) // Controller Air Temperature (°C). [86]
	{

		StrRecevieDataMid = StrRecevieData.Mid(10, 14);
		StrRevevieDataResult += StrRecevieDataMid.Mid(1, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(3, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(5, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(7, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(9, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(11, 1);
		StrRevevieDataResult += StrRecevieDataMid.Mid(13, 1);

		StrLLCTmpReceiveDataTemperatureAir = StrRevevieDataResult; // 현재 온도 값
		StrLLCTmpReceiveDataTemperatureAir += " ℃";
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): " + StrLLCTmpReceiveDataTemperatureAir)));	//통신 상태 기록.
	}
	else 
	{
		StrLLCTmpReceiveDataHz = "Send Error";
		StrLLCTmpReceiveDataState = "Send Error";
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP (RECEIVE): UnKnown " + StrLLCTmpReceiveDataState)));	//통신 상태 기록.
	}

	return nRet;
}

*/

int CAgilentTwisTorr304TMPCtrl::OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity)
{
	int nRet = 0;

	nRet = OpenSerialPort(sPortName, dwBaud, wByte, wStop, wParity);

	if (nRet != 0)
	{
		m_strSendMsg = _T("SERIAL PORT OPEN FAIL");
		nRet = -81001;
	}
	else m_strSendMsg = _T("SERIAL PORT OPEN"); 
	
	SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : " + m_strSendMsg)));	//통신 상태 기록.

	return nRet;
}

void CAgilentTwisTorr304TMPCtrl::initialize()
{

}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Monitor_Com()
{
	int ret = -1;
	char Buff[9] = "";

	// TMP_MONITOR
	Buff[0] = 0x02;
	Buff[1] = 0x80;
	Buff[2] = 0x32;
	Buff[3] = 0x30;
	Buff[4] = 0x35;
	Buff[5] = 0x30;
	Buff[6] = 0x03;
	Buff[7] = 0x38;
	Buff[8] = 0x34;

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_HzRead_Com()
{
	int ret = -1;
	char Buff[9]= "";

	// TMP HZ READ
	Buff[0] = 0x02; //STX
	Buff[1] = 0x80; //ADDR
	Buff[2] = 0x32; //WINDOW
	Buff[3] = 0x30; //WINDOW
	Buff[4] = 0x33; //WINDOW
	Buff[5] = 0x30; //RD
	Buff[6] = 0x03; //ETX
	Buff[7] = 0x38; //CRC
	Buff[8] = 0x32; //CRC	

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_RpmRead_Com()
{
	int ret = -1;
	char Buff[9] = "";

	//TMP Rpm Read [85]
	Buff[0] = 0x02;
	Buff[1] = 0x80;
	Buff[2] = 0x32;
	Buff[3] = 0x32;
	Buff[4] = 0x36;
	Buff[5] = 0x30;
	Buff[6] = 0x03;
	Buff[7] = 0x38;
	Buff[8] = 0x35;

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_ErrorCode_Check_Com()
{
	int ret = -1;
	char Buff[9] = "";


	//TMP_ERROR CODE [87]
	Buff[0] = 0x02;
	Buff[1] = 0x80;
	Buff[2] = 0x32;
	Buff[3] = 0x30;
	Buff[4] = 0x36;
	Buff[5] = 0x30;
	Buff[6] = 0x03;
	Buff[7] = 0x38;
	Buff[8] = 0x37;

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Temprerature_Check_Com()
{
	int ret = -1;
	char Buff[9] = "";

	//TMP_temprerature [85]
	Buff[0] = 0x02;
	Buff[1] = 0x80;
	Buff[2] = 0x32;
	Buff[3] = 0x30;
	Buff[4] = 0x34;
	Buff[5] = 0x30;
	Buff[6] = 0x03;
	Buff[7] = 0x38;
	Buff[8] = 0x35;

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Controller_Heatsink_Temprerature_Check_Com()
{
	int ret = -1;
	char Buff[9] = "";

	// Controller Heatsink Temperature (°C) [81]
	Buff[0] = 0x02;
	Buff[1] = 0x80;
	Buff[2] = 0x32;
	Buff[3] = 0x31;
	Buff[4] = 0x31;
	Buff[5] = 0x30;
	Buff[6] = 0x03;
	Buff[7] = 0x38;
	Buff[8] = 0x31;

	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Controller_Air_Temprerature_Check_Com()
{
	int ret = -1;
	char Buff[9] = "";

	//Controller Air Temperature (°C). [86]
	Buff[0] = 0x02;
	Buff[1] = 0x80;
	Buff[2] = 0x32;
	Buff[3] = 0x31;
	Buff[4] = 0x36;
	Buff[5] = 0x30;
	Buff[6] = 0x03;
	Buff[7] = 0x38;
	Buff[8] = 0x36;
	
	ret = SendData(sizeof(Buff), Buff);
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_On_Com()
{
	int ret = -1;
	char Buff[10] = "";
	TmpExecutionFlag = TRUE;

	// TMP ON
	Buff[0] = 0x02;
	Buff[1] = 0x80;
	Buff[2] = 0x30;
	Buff[3] = 0x30;
	Buff[4] = 0x30;
	Buff[5] = 0x31;
	Buff[6] = 0x31;
	Buff[7] = 0x03;
	Buff[8] = 0x42;
	Buff[9] = 0x33;

	ResetEvent(TmpOnOffEvent);
	ret = SendData(sizeof(Buff), Buff);
	if (ret != 0)
	{
		SetEvent(TmpOnOffEvent);
	}
	else
	{
		ret = WaitTmpOnOffEvent(5000);
	}

	return ret;
}

int CAgilentTwisTorr304TMPCtrl::LLCTmp_Off_Com()
{
	int ret = -1;
	char Buff[10] = "";
	TmpExecutionFlag = TRUE;
	// TMP OFF
	Buff[0] = 0x02;
	Buff[1] = 0x80;
	Buff[2] = 0x30;
	Buff[3] = 0x30;
	Buff[4] = 0x30;
	Buff[5] = 0x31;
	Buff[6] = 0x30;
	Buff[7] = 0x03;
	Buff[8] = 0x42;
	Buff[9] = 0x32;

	ResetEvent(TmpOnOffEvent);
	ret = SendData(sizeof(Buff),Buff);
	
	if (ret != 0)
	{
		SetEvent(TmpOnOffEvent);
	}
	else
	{
		ret = WaitTmpOnOffEvent(5000);
	}
	
	return ret;
}

int CAgilentTwisTorr304TMPCtrl::WaitTmpOnOffEvent(int nTimeout)
{
	if (WaitForSingleObject(TmpOnOffEvent, nTimeout) == WAIT_OBJECT_0)
	{
		return 0;
	}
	else
	{
		return -1;
	}

	
}