#include "stdafx.h"
#include "CLinearRevolverCtrl.h"


CLinearRevolverCtrl::CLinearRevolverCtrl()
{
	Dev = new CLLC2_Changer();
}

CLinearRevolverCtrl::~CLinearRevolverCtrl()
{
	if (Dev != NULL)
	{
		delete Dev;
		Dev = NULL;
	}
}

int CLinearRevolverCtrl::OpenSerialPort(CString sPortName)
{
	long ret = 0;

	int pos = sPortName.Find(_T("COM"));
	CString port = sPortName.Mid(pos + 3, 2);

	long wPortID = atoi((LPSTR)(LPCTSTR)port); // COM1-> 1, COM2->2,,,,,

	ret = Dev->OpenComport(wPortID);

	return ret;
}

void CLinearRevolverCtrl::MoveHomeRevolver()
{
	Dev->Home();
}

void CLinearRevolverCtrl::MoveRight()
{
	Dev->Go_Right();
}

void CLinearRevolverCtrl::MoveLeft()
{
	Dev->Go_Left();
}

long CLinearRevolverCtrl::CheckDirection()
{
	long nRet = 0;

	nRet = Dev->Check_Direction();

	return nRet;
}

long CLinearRevolverCtrl::CheckStatus()
{
	long nRet = 0;

	nRet = Dev->Check_Status();

	return nRet;
}

int CLinearRevolverCtrl::CloseDevice()
{
	long nRet = 0;

	nRet = Dev->CloseComport();

	return nRet;
}