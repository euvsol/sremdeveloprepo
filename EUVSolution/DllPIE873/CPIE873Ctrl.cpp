#include "stdafx.h"
#include "CPIE873Ctrl.h"
#include <fstream>
#include "math.h"

using namespace std;

CPIE873Ctrl::CPIE873Ctrl()
{
	m_nPIStage_ID = 0;
	m_bConnect = FALSE;
	m_bServoOn = FALSE;

	strcpy(m_chPIStage_Axis, "1");

	m_dPIStage_MovePos[X_AXIS_E873]		  = X_INITIAL_POS_E873;
	m_dPIStage_MinusLimitPos[X_AXIS_E873] = X_MINUSLIMIT_E873;
	m_dPIStage_PlusLimitPos[X_AXIS_E873]  = X_PLUSLIMIT_E873;
}


CPIE873Ctrl::~CPIE873Ctrl()
{
}

int CPIE873Ctrl::ConnectComm(int CommunicationType, int nComPort, int nBaudRate)
{
	int nRet = 0;

	switch (CommunicationType)
	{
		case ETHERNET:
			break;
		case SERIAL:
			m_nPIStage_ID = PI_ConnectRS232(nComPort, nBaudRate);
			
			if (m_nPIStage_ID < 0)
			{
				nRet = PI_GetError(m_nPIStage_ID);
				return nRet;
			}
			
			m_bConnect = TRUE;
			break;
		case SIMULATOR:
		break;
	}

	return nRet;
}

void CPIE873Ctrl::Initialize_PIStage()
{
	if (m_bConnect == FALSE)
		return;

	ServoOn();
	//Move_Origin_Position();
}

void CPIE873Ctrl::DisconnectComm()
{
	PI_CloseConnection(m_nPIStage_ID);
	m_bConnect = FALSE;
}

int CPIE873Ctrl::ServoOn()
{
	int nRet = 0;
	const BOOL bFlags = true;

	if (!PI_SVO(m_nPIStage_ID, m_chPIStage_Axis, &bFlags))
		nRet = PI_GetError(m_nPIStage_ID);
	else
		m_bServoOn = TRUE;
	
	return nRet;
}

int CPIE873Ctrl::ServoOff()
{
	int nRet = 0;
	const BOOL bFlags = false;

	if (!PI_SVO(m_nPIStage_ID, m_chPIStage_Axis, &bFlags))
		nRet = PI_GetError(m_nPIStage_ID);
	else
		m_bServoOn = FALSE;

	return nRet;
}

int CPIE873Ctrl::IsMoving()
{
	int nRet = 0;
	BOOL bFlags = true;

	if (!PI_IsMoving(m_nPIStage_ID, m_chPIStage_Axis, &bFlags))
		nRet = PI_GetError(m_nPIStage_ID);

	return nRet;
}

int CPIE873Ctrl::GetPosAxesData()
{
	int nRet = 0;

	///////////////////////////////////////////
	// Get the current positions of szAxes   //
	///////////////////////////////////////////
	if (!PI_qPOS(m_nPIStage_ID, m_chPIStage_Axis, &m_dPIStage_GetPos))
	{
		nRet = PI_GetError(m_nPIStage_ID);
		DisconnectComm();
	}

	return nRet;
}

int CPIE873Ctrl::Get_Stage_Axes()
{
	int nRet = 0;

	/////////////////////////////////////////////////
	// Get the identifiers for all configured axes //
	/////////////////////////////////////////////////
	if (!PI_qSAI(m_nPIStage_ID, m_chPIStage_Axis, 2))
	{
		nRet = PI_GetError(m_nPIStage_ID);
		DisconnectComm();
	}

	return nRet;
}

int CPIE873Ctrl::PI_Move_Absolute()
{
	int nRet = 0;

	/////////////////////////////////////////////////////
	// Move all axes in szAxes to their home positions //
	/////////////////////////////////////////////////////	
	if (!PI_MOV(m_nPIStage_ID, m_chPIStage_Axis, m_dPIStage_MovePos))
	{
		nRet = PI_GetError(m_nPIStage_ID);
		DisconnectComm();
	}

	return nRet;
}

int CPIE873Ctrl::PI_Move_Relative()
{
	int nRet = 0;

	if (!PI_MVR(m_nPIStage_ID, m_chPIStage_Axis, m_dPIStage_MovePos))
	{
		nRet = PI_GetError(m_nPIStage_ID);
		//PI_TranslateError(m_nErrorCode, m_chPIStage_ErrorMessage, ERROR_STRING_SIZE);
		//TRACE(_T("PI_MVR: ERROR %d: %s\n"), m_nErrorCode, m_chPIStage_ErrorMessage);
		DisconnectComm();
	}

	return nRet;
}

int CPIE873Ctrl::Move_Origin_Position()
{
	int nRet = 0;
	m_dPIStage_MovePos[X_AXIS_E873] = X_INITIAL_POS_E873;
	nRet = PI_Move_Absolute();
	return nRet;
}
