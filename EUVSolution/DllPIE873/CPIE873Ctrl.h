/**
 * PI E873 Controller(Filter Stage) Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

#define ETHERNET	0
#define SERIAL		1
#define SIMULATOR	2

#define ERROR_STRING_SIZE			1024	

#define X_INITIAL_POS_E873			0

#define AXIS_NUMBER_E873				1			//축 수
#define X_AXIS_E873						0			//x축, Scan Axis

#define X_MINUSLIMIT_E873				-13
#define X_PLUSLIMIT_E873				13


class AFX_EXT_CLASS CPIE873Ctrl : public CECommon
{
public:
	CPIE873Ctrl();
	~CPIE873Ctrl();

	BOOL m_bConnect;
	BOOL m_bServoOn;
	int m_nErrorCode;
	
	char m_chPIStage_ErrorMessage[ERROR_STRING_SIZE];
	char m_chPIStage_Strings[ERROR_STRING_SIZE];
	char m_chPIStage_Axis[2];

	double m_dPIStage_MovePos[AXIS_NUMBER_E873];
	double m_dPIStage_GetPos;								
	double m_dPIStage_MinusLimitPos[AXIS_NUMBER_E873];
	double m_dPIStage_PlusLimitPos[AXIS_NUMBER_E873];

public:
	int ConnectComm(int CommunicationType, int nComPort, int nBaudRate); //Stage Controller와 통신 연결하기.(ETHERNET,SERIAL,SIMULATOR)
	void Initialize_PIStage();
	void DisconnectComm();	/** 통신 port를 닫는다.	 */

	int m_nPIStage_ID;											//ID of Controller

	int ServoOn();													//servo on
	int ServoOff();												//servo off
	int IsMoving();
	int GetPosAxesData();											//stage 좌표를 읽어온다.
	int Get_Stage_Axes();											//stage 축 정보를 읽어온다.

	int PI_Move_Absolute();										//MOVE 동작 수행
	int PI_Move_Relative();										//relative move
	int Move_Origin_Position();									//X_INITIAL_POS_UM,Y_INITIAL_POS_UM,TIP_COMPENSATION_ANGLE,TILT_COMPENSATION_ANGLE 으로 이동, 즉 z axis은 이동 없음
};

