#include "stdafx.h"
#include "CXRayCameraCtrl.h"

CXRayCameraCtrl* CXRayCameraCtrl::m_pInstance = NULL;
BOOL CXRayCameraCtrl::m_bStopped = FALSE;

CXRayCameraCtrl::CXRayCameraCtrl()
{
	m_hDevice = 0;
	m_nFramesPerReadout = 0;
	m_nFrameStride = 0;
	m_nFrameSize = 0;
	m_nReadoutStride = 0;
	m_nImageDataWidth = 0;
	m_nImageDataHeight = 0;
	m_CalculatedBufferSize = 0;
	m_nPixelBitDepth = 0;
	m_bHighResolution = false;
	m_bOnline = FALSE;
	m_bConnected = FALSE;
	m_pInstance = this;
}

CXRayCameraCtrl::~CXRayCameraCtrl()
{
	CloseXrayCamera();
}

BOOL CXRayCameraCtrl::OpenXrayCamera(BOOL bOnline)
{
	//20200622 jkseo, 생성자에서 임시로 위치 이동(LightField가 잡고 있으면 먹통됨)
	PicamError error = Picam_InitializeLibrary();
	//if (error != PicamError_None)
	//	DisplayError(L"Failed to initialize PICam.", error);
	//PicamError error;

	m_bOnline = bOnline;

	m_hAcquisitionInactive = CreateEvent(0, true /*manual*/, true /*signaled*/, 0);
	if (!m_hAcquisitionInactive)
	{
		DisplayError(L"Failed to create acquisition inactive event.");
		return false;
	}

	//PicamError error;
	piint id_count = 0;

	if (m_bOnline == TRUE)
	{
		const PicamCameraID* id_array;

		error = Picam_GetAvailableCameraIDs(&id_array, &id_count);
		if (error != PicamError_None)
		{
			DisplayError(L"Failed to get available camera ids.", error);
			return FALSE;
		}

		error = PicamAdvanced_OpenCameraDevice(id_array, &m_hDevice);
		if (error != PicamError_None)
		{
			DisplayError(L"Failed to open camera device.", error);
			return FALSE;
		}
	}
	else  //CONNECT DEMO CAMERA 
	{
		const PicamModel* model_array;

		error = Picam_GetAvailableDemoCameraModels(&model_array, &id_count);
		if (error != PicamError_None)
		{
			DisplayError(L"Failed to open Camera", error);
			return FALSE;
		}

		const pichar* serial_number = "aa";
		//error = Picam_ConnectDemoCamera(*model_array, serial_number, &m_IdArray);
		error = Picam_ConnectDemoCamera(PicamModel_SophiaXO2048B135, serial_number, &m_IdArray);
		if (error != PicamError_None)
		{
			DisplayError(L"Failed to open Camera", error);
			return FALSE;
		}

		error = PicamAdvanced_OpenCameraDevice(&m_IdArray, &m_hDevice);
		if (error != PicamError_None)
		{
			DisplayError(L"Failed to open Camera", error);
			return FALSE;
		}
	}

	if (m_hDevice)
	{
		if (!RegisterCameraCallbacks()) return FALSE;
		if (!InitializeCalculatedBufferSize()) return FALSE;
		if (!InitializeImage()) return FALSE;
	}
	else
	{
		return FALSE;
	}

	m_bConnected = TRUE;

	return TRUE;
}

BOOL CXRayCameraCtrl::CloseXrayCamera()
{
	if (m_hDevice)
	{
		// - handle an acquiring camera
		pibln running;
		PicamError error = Picam_IsAcquisitionRunning(m_hDevice, &running);
		if (error == PicamError_None && running)
		{
			error = Picam_StopAcquisition(m_hDevice);
			running = error != PicamError_None || WaitForSingleObject(m_hAcquisitionInactive, 10000) != WAIT_OBJECT_0;
			if (running)
			{
				DisplayError(L"Failed to stop camera.", error);
				return FALSE;
			}
		}

		// - handle an open camera
		if (!running)
		{
			error = Picam_CloseCamera(m_hDevice);
			if (error != PicamError_None)
			{
				DisplayError(L"Failed to close camera.", error);
				return FALSE;
			}
			else
			{
				m_hDevice = NULL;
			}

			if (m_bOnline != TRUE)
			{
				error = Picam_DisconnectDemoCamera(&m_IdArray);
				if (error != PicamError_None)
				{
					DisplayError(L"Failed to open Camera", error);
					return FALSE;
				}
			}
		}

		m_bConnected = FALSE;
	}

	return TRUE;
}

void CXRayCameraCtrl::RegisterFunction(callback p_func)
{
	CompleteAcquisition = p_func;
}

void CXRayCameraCtrl::SingleGrab()
{
	if (!SetReadoutCount(true))
		return;

	Start();
}

void CXRayCameraCtrl::ContinuousGrab()
{
	if (!SetReadoutCount(false))
		return;

	Start();
}

void CXRayCameraCtrl::GrabStop()
{
	if (m_hDevice)
	{
		m_bStopped = TRUE;
		PicamError error = Picam_StopAcquisition(m_hDevice);
		if (error == PicamError_None)
			DisplayError(L"Failed to stop acquisition.", error);
	}
}

void CXRayCameraCtrl::CommitParam()
{
	PicamError error;

	// - determine if parameters need to be committed
	pibln committed;
	error = Picam_AreParametersCommitted(m_hDevice, &committed);
	if (error != PicamError_None)
	{
		DisplayError(L"Cannot determine if parameters need to be committed.", error);
		return;
	}

	// - commit parameters from the model to the device
	if (!committed)
	{
		PicamHandle model;
		error = PicamAdvanced_GetCameraModel(m_hDevice, &model);
		if (error != PicamError_None)
		{
			DisplayError(L"Cannot get the camera model.", error);
			return;
		}

		error = PicamAdvanced_CommitParametersToCameraDevice(model);
		if (error != PicamError_None)
		{
			DisplayError(L"Failed to commit the camera model parameters.", error);
			return;
		}
	}
}

void CXRayCameraCtrl::Start()
{
	PicamError error;

	CommitParam();

	// - reallocate circular buffer if necessary
	if (m_CalculatedBufferSize == 0)
	{
		DisplayError(L"Cannot start with a circular buffer of no length.");
		return;
	}
	if (m_Buffer.size() != m_CalculatedBufferSize)
		m_Buffer.resize(m_CalculatedBufferSize);

	// - get current circular buffer
	PicamAcquisitionBuffer buffer;
	error = PicamAdvanced_GetAcquisitionBuffer(m_hDevice, &buffer);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get circular buffer.", error);
		return;
	}

	// - update circular buffer if neccessary
	if (&m_Buffer[0] != buffer.memory ||
		static_cast<pi64s>(m_Buffer.size()) != buffer.memory_size)
	{
		buffer.memory = &m_Buffer[0];
		buffer.memory_size = m_Buffer.size();
		error = PicamAdvanced_SetAcquisitionBuffer(m_hDevice, &buffer);
		if (error != PicamError_None)
		{
			DisplayError(L"Failed to set circular buffer.", error);
			return;
		}
	}

	// - cache information used to extract frames during acquisition
	if (!CacheFrameNavigation())
		return;

	// - initialize image data and display
	if (!InitializeImage())
		return;

	// - mark acquisition active just before acquisition begins
	ResetEvent(m_hAcquisitionInactive);

	// - start
	m_bStopped = FALSE;
	error = Picam_StartAcquisition(m_hDevice);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to start acquisition.", error);
		return;
	}
}

bool CXRayCameraCtrl::RegisterCameraCallbacks()
{
	// - register acquisition updated
	PicamError error = PicamAdvanced_RegisterForAcquisitionUpdated(m_hDevice, AcquisitionUpdated);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to register for acquisition updated.", error);
		return false;
	}

	return true;
}

bool CXRayCameraCtrl::InitializeCalculatedBufferSize()
{
	// - get the current readout rate
	// - note this accounts for rate increases in online scenarios
	piflt onlineReadoutRate;
	PicamError error = Picam_GetParameterFloatingPointValue(m_hDevice, PicamParameter_OnlineReadoutRateCalculation, &onlineReadoutRate);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get online readout rate.", error);
		return false;
	}

	// - get the current readout stride
	piint readoutStride;
	error = Picam_GetParameterIntegerValue(m_hDevice, PicamParameter_ReadoutStride, &readoutStride);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get readout stride.", error);
		return false;
	}

	// - calculate the buffer size
	CalculateBufferSize(readoutStride, onlineReadoutRate);

	return true;
}

void CXRayCameraCtrl::CalculateBufferSize(piint readoutStride, piflt onlineReadoutRate)
{
	// - calculate a circular buffer with the following simple rules:
	//   - contain at least 3 seconds worth of readout rate
	//   - contain at least 2 readouts
	// - note this takes into account changes that affect rate online (such as
	//   exposure) by assuming worst case (fastest rate)

#if FALSE
	pi64s readouts = static_cast<pi64s>(std::ceil(std::max(3.*onlineReadoutRate, 2.)));
#else
	double maxValue = (3.*onlineReadoutRate) > 2. ? (3.*onlineReadoutRate) : 2.;
	pi64s readouts = static_cast<pi64s>(std::ceil(maxValue));
#endif
	m_CalculatedBufferSize = static_cast<std::size_t>(readoutStride * readouts);
}

int CXRayCameraCtrl::SetTemperatureSetPoint(piflt fValue)
{
	pibln running;

	PicamError error = Picam_IsAcquisitionRunning(m_hDevice, &running);
	if (error != PicamError_None)
	{
		return -1;
	}

	PicamHandle model;
	error = PicamAdvanced_GetCameraModel(m_hDevice, &model);
	if (error != PicamError_None)
	{
		return -1;
	}

	if (running)
	{
		error = Picam_SetParameterFloatingPointValueOnline(model, PicamParameter_SensorTemperatureSetPoint, fValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}
	else
	{
		error = Picam_SetParameterFloatingPointValue(model, PicamParameter_SensorTemperatureSetPoint, fValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}

	CommitParam();

	return 0;
}

int CXRayCameraCtrl::SetExposureTime(piflt fValue)
{
	pibln running;

	PicamError error = Picam_IsAcquisitionRunning(m_hDevice, &running);
	if (error != PicamError_None)
	{
		return -1;
	}

	PicamHandle model;
	error = PicamAdvanced_GetCameraModel(m_hDevice, &model);
	if (error != PicamError_None)
	{
		return -1;
	}

	if (running)
	{
		error = Picam_SetParameterFloatingPointValueOnline(model, PicamParameter_ExposureTime, fValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}
	else
	{
		error = Picam_SetParameterFloatingPointValue(model, PicamParameter_ExposureTime, fValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}

	CommitParam();

	return 0;
}

int CXRayCameraCtrl::GetExposureTime(piflt *exposure)
{
	// - get the current set up exposure time

	PicamHandle model;
	PicamError error = PicamAdvanced_GetCameraModel(m_hDevice, &model);

	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get camera model.", error);
		return error;
	}

	//piflt exposure;
	error =
		Picam_GetParameterFloatingPointValue(
			model,
			PicamParameter_ExposureTime,
			exposure);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get exposure time.", error);
		return error;
	}

	return 0;
}


int CXRayCameraCtrl::SetAdcQuality(piint nValue)
{
	pibln running;

	PicamError error = Picam_IsAcquisitionRunning(m_hDevice, &running);
	if (error != PicamError_None)
	{
		return -1;
	}

	PicamHandle model;
	error = PicamAdvanced_GetCameraModel(m_hDevice, &model);
	if (error != PicamError_None)
	{
		return -1;
	}

	if (running)
	{
		error = Picam_SetParameterIntegerValueOnline(m_hDevice, PicamParameter_AdcQuality, nValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}
	else
	{
		error = Picam_SetParameterIntegerValue(m_hDevice, PicamParameter_AdcQuality, nValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}

	CommitParam();

	return 0;
}

int CXRayCameraCtrl::SetAdcSpeed(piflt fValue)
{
	pibln running;

	PicamError error = Picam_IsAcquisitionRunning(m_hDevice, &running);
	if (error != PicamError_None)
	{
		return -1;
	}

	PicamHandle model;
	error = PicamAdvanced_GetCameraModel(m_hDevice, &model);
	if (error != PicamError_None)
	{
		return -1;
	}

	if (running)
	{
		error = Picam_SetParameterFloatingPointValueOnline(m_hDevice, PicamParameter_AdcSpeed, fValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}
	else
	{
		error = Picam_SetParameterFloatingPointValue(m_hDevice, PicamParameter_AdcSpeed, fValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}

	CommitParam();

	return 0;
}

int CXRayCameraCtrl::SetAdcAnalogGain(piint nValue)
{
	pibln running;

	PicamError error = Picam_IsAcquisitionRunning(m_hDevice, &running);
	if (error != PicamError_None)
	{
		return -1;
	}

	PicamHandle model;
	error = PicamAdvanced_GetCameraModel(m_hDevice, &model);
	if (error != PicamError_None)
	{
		return -1;
	}

	if (running)
	{
		error = Picam_SetParameterIntegerValueOnline(m_hDevice, PicamParameter_AdcAnalogGain, nValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}
	else
	{
		error = Picam_SetParameterIntegerValue(m_hDevice, PicamParameter_AdcAnalogGain, nValue);
		if (error != PicamError_None)
		{
			return -1;
		}
	}

	CommitParam();

	return 0;
}

float CXRayCameraCtrl::GetTemperature()
{
	piflt fValue;
	PicamError error = Picam_GetParameterFloatingPointValue(m_hDevice, PicamParameter_SensorTemperatureReading, &fValue);
	if (error == PicamError_None)
		return fValue;
	else
		return 0;
}

// Sensor Temperature
float CXRayCameraCtrl::GetTemperatureSetValue()
{
	piflt fValue;
	
	PicamError error = Picam_GetParameterFloatingPointValue(m_hDevice, PicamParameter_SensorTemperatureSetPoint, &fValue);	
	if (error == PicamError_None)
		return fValue;
	else
		return 0;	
}

int CXRayCameraCtrl::GetTemperatureStatus()
{
	piint nValue;
	PicamError error = Picam_GetParameterIntegerValue(m_hDevice, PicamParameter_SensorTemperatureStatus, &nValue);
	if (error == PicamError_None)
		return nValue;
	else
		return -1;
}

int CXRayCameraCtrl::GetCoolingFanStatus()
{
	piint nValue;
	PicamError error = Picam_GetParameterIntegerValue(m_hDevice, PicamParameter_CoolingFanStatus, &nValue);
	if (error == PicamError_None)
		return nValue;
	else
		return -1;
}

int CXRayCameraCtrl::GetAcquisitionRunning()
{
	pibln bValue;
	PicamError error = Picam_IsAcquisitionRunning(m_hDevice, &bValue);
	if (error == PicamError_None)
		return bValue;
	else
		return -1;
}

bool CXRayCameraCtrl::SetReadoutCount(bool acquire)
{
	pi64s readouts = acquire ? 1 : 0;
	PicamError error = Picam_SetParameterLargeIntegerValue(m_hDevice, PicamParameter_ReadoutCount, readouts);
	if (error != PicamError_None)
	{
		DisplayError(L"Cannot set readout count.", error);
		return false;
	}

	return true;
}

bool CXRayCameraCtrl::CacheFrameNavigation()
{
	// - cache the readout stride
	PicamError error = Picam_GetParameterIntegerValue(m_hDevice, PicamParameter_ReadoutStride, &m_nReadoutStride);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get readout stride.", error);
		return false;
	}

	// - cache the frame stride
	error = Picam_GetParameterIntegerValue(m_hDevice, PicamParameter_FrameStride, &m_nFrameStride);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get frame stride.", error);
		return false;
	}

	// - cache the frames per readout
	error = Picam_GetParameterIntegerValue(m_hDevice, PicamParameter_FramesPerReadout, &m_nFramesPerReadout);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get frames per readout.", error);
		return false;
	}

	return true;
}

bool CXRayCameraCtrl::InitializeImage()
{
	// - cache frame size
	PicamError error = Picam_GetParameterIntegerValue(m_hDevice, PicamParameter_FrameSize, &m_nFrameSize);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get frame size.", error);
		return false;
	}

	// - determine pixel bit depth

	error = Picam_GetParameterIntegerValue(m_hDevice, PicamParameter_PixelBitDepth, &m_nPixelBitDepth);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get pixel bit depth.", error);
		return false;
	}

	// - cache resolution
	m_bHighResolution = m_nPixelBitDepth > 16;

	// - size image data to fit frame
	if (m_bHighResolution)
	{
		std::vector<pi16u>().swap(m_ImageData16);
		m_ImageData32.resize(m_nFrameSize / sizeof(m_ImageData32[0]));
	}
	else
	{
		std::vector<pi32u>().swap(m_ImageData32);
		m_ImageData16.resize(m_nFrameSize / sizeof(m_ImageData16[0]));
	}

	// - determine image dimensions
	const PicamRois* rois;
	error = Picam_GetParameterRoisValue(m_hDevice, PicamParameter_Rois, &rois);
	if (error != PicamError_None)
	{
		DisplayError(L"Failed to get rois.", error);
		return false;
	}
	m_nImageDataWidth = rois->roi_array[0].width / rois->roi_array[0].x_binning;
	m_nImageDataHeight = rois->roi_array[0].height / rois->roi_array[0].y_binning;

	Picam_DestroyRois(rois);

	return true;
}

std::wstring CXRayCameraCtrl::GetEnumString(PicamEnumeratedType type, piint value)
{
	const pichar* string;
	if (Picam_GetEnumerationString(type, value, &string) == PicamError_None)
	{
		std::string s(string);
		Picam_DestroyString(string);

		std::wstring w(s.length(), L'\0');
		std::copy(s.begin(), s.end(), w.begin());
		return w;
	}
	return std::wstring();
}

void CXRayCameraCtrl::DisplayError(const std::wstring & message, PicamError error)
{
	std::wstring details(message);
	if (error != PicamError_None)
		details += L" (" + GetEnumString(PicamEnumeratedType_Error, error) + L")";
}

PicamError PIL_CALL CXRayCameraCtrl::AcquisitionUpdated(PicamHandle device, const PicamAvailableData * available, const PicamAcquisitionStatus * status)
{
	if (available && available->readout_count)
	{
		// - copy the last available frame to the shared image buffer and notify

		pi64s lastReadoutOffset = m_pInstance->m_nReadoutStride * (available->readout_count - 1);
		pi64s lastFrameOffset = m_pInstance->m_nFrameStride * (m_pInstance->m_nFramesPerReadout - 1);
		const pibyte* frame = static_cast<const pibyte*>(available->initial_readout) + lastReadoutOffset + lastFrameOffset;
		if (m_pInstance->m_bHighResolution)
			std::memcpy(&m_pInstance->m_ImageData32[0], frame, m_pInstance->m_nFrameSize);
		else
			std::memcpy(&m_pInstance->m_ImageData16[0], frame, m_pInstance->m_nFrameSize);


		// - check for overrun after copying
		pibln overran;
		PicamError error = PicamAdvanced_HasAcquisitionBufferOverrun(device, &overran);
		if (error != PicamError_None)
		{
			std::wostringstream woss;
			woss << L"Failed check for buffer overrun. "
				<< L"("
				<< m_pInstance->GetEnumString(PicamEnumeratedType_Error, error)
				<< L")";
		}
		else if (overran)
		{
			//PostMessage(
			//	main_,
			//	WM_DISPLAY_ERROR,
			//	reinterpret_cast<WPARAM>(
			//		new std::wstring(L"Buffer overran.")),
			//	0);
		}
	}

	// - note when acquisition has completed
	if (!status->running)
	{
		if (!m_bStopped)
		{
			SetEvent(m_pInstance->m_hAcquisitionInactive);
			//m_pInstance->CompleteAcquisition();
		}		
	}

	return PicamError_None;
}

PicamError CXRayCameraCtrl::SetRoi(PicamRoi roi)
{
	PicamError					err;			 /* Error Code			*/

	PicamRois					region;
	const PicamParameter		*paramsFailed;	 /* Failed to commit    */
	piint						failCount;		 /* Count of failed	    */

	PicamRoi roi_temp;

	region.roi_count = 1;
	region.roi_array = &roi_temp;
	region.roi_array[0] = roi;

	err = Picam_SetParameterRoisValue(m_hDevice, PicamParameter_Rois, &region);

	if (err == PicamError_None)
	{
		err = Picam_CommitParameters(m_hDevice, &paramsFailed, &failCount);
		Picam_DestroyParameters(paramsFailed);
	}

	return err;
}

PicamError CXRayCameraCtrl::GetRoi(PicamRoi *roi)
{
	PicamError err;

	const PicamRois *region;

	err = Picam_GetParameterRoisValue(m_hDevice, PicamParameter_Rois, &region);

	if (err == PicamError_None)
	{
		*roi = region->roi_array[0];

	}

	return err;
}

CXRayCameraCtrl* CXRayCameraCtrl::GetXrayCameraInstance()
{
	return m_pInstance;
}


PicamError CXRayCameraCtrl::GetXrayCameraControlParameterInDevice()
{
	piint nValue;
	piflt fValue;
	PicamRoi roi;

	PicamParameter parameter;	
	PicamError error = PicamError_None;

	// ADC
	if (error == PicamError_None)
	{
		parameter = PicamParameter_AdcQuality;		
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.tAdcQuaulity = nValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_AdcSpeed;
		error = Picam_GetParameterFloatingPointValue(m_hDevice, parameter, &fValue);
		XrayCameraControlParameterInDevice.fAdcSpeed = fValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_AdcAnalogGain;		
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.tAdcAnalogGain = nValue;
	}
	// Sensor Temperature
	if (error == PicamError_None)
	{
		parameter = PicamParameter_SensorTemperatureSetPoint;
		error = Picam_GetParameterFloatingPointValue(m_hDevice, parameter, &fValue);
		XrayCameraControlParameterInDevice.fSensorTemperatureSetPoint = fValue;
	}
	// CCD ROI
	if (error == PicamError_None)
	{
		GetRoi(&XrayCameraControlParameterInDevice.Roi);
	}
	// READOUT CONTROL
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ReadoutControlMode;		
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.tReadoutControlMode = nValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ReadoutPortCount;
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.nReadoutPortCount = nValue;
	}
	// SHUTTER	
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ExposureTime;
		error = Picam_GetParameterFloatingPointValue(m_hDevice, parameter, &fValue);
		XrayCameraControlParameterInDevice.fExposureTime = fValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ShutterTimingMode;
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.tShutterTimingMode = nValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ShutterDelayResolution;
		error = Picam_GetParameterFloatingPointValue(m_hDevice, parameter, &fValue);
		XrayCameraControlParameterInDevice.fShutterDelayResolution = fValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ShutterOpeningDelay;
		error = Picam_GetParameterFloatingPointValue(m_hDevice, parameter, &fValue);
		XrayCameraControlParameterInDevice.fShutterOpeningDelay = fValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ShutterClosingDelay;
		error = Picam_GetParameterFloatingPointValue(m_hDevice, parameter, &fValue);
		XrayCameraControlParameterInDevice.fShutterClosingDelay = fValue;
	}
	// HARDWARE IO
	if (error == PicamError_None)
	{
		parameter = PicamParameter_TriggerResponse;		
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.tTriggerResponse = nValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_TriggerDetermination;		
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.tTriggerDetermination = nValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_OutputSignal;		
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.tOutputSignal = nValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_OutputSignal2;		
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.tOutputSignal2 = nValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_InvertOutputSignal;
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.nbInvertOutputsignal = nValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_InvertOutputSignal2;
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.nbInvertOutputsignal2 = nValue;
	}
	// SENSOR CLEANING
	if (error == PicamError_None)
	{
		parameter = PicamParameter_CleanCycleCount;
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.nCleanCycleCount = nValue;
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_CleanCycleHeight;
		error = Picam_GetParameterIntegerValue(m_hDevice, parameter, &nValue);
		XrayCameraControlParameterInDevice.nCleanCycleHeight = nValue;
	}

	return error;
}


PicamError CXRayCameraCtrl::SetXrayCameraControlParameterFromConfig()
{	
	PicamError error = PicamError_None;	
	error = SetXrayCameraControlParameter(XrayCameraControlParameterInConfig);
	return error;
}

PicamError CXRayCameraCtrl::SetXrayCameraControlParameter(XRAY_CAMERA_CONTROL_PARAMETER SetParameter)
{
	PicamParameter parameter;
	PicamError error = PicamError_None;

	// ADC
	if (error == PicamError_None)
	{
		parameter = PicamParameter_AdcQuality;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.tAdcQuaulity);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_AdcSpeed;
		error = Picam_SetParameterFloatingPointValue(m_hDevice, parameter, SetParameter.fAdcSpeed);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_AdcAnalogGain;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.tAdcAnalogGain);
	}
	// Sensor Temperature
	if (error == PicamError_None)
	{
		parameter = PicamParameter_SensorTemperatureSetPoint;
		error = Picam_SetParameterFloatingPointValue(m_hDevice, parameter, SetParameter.fSensorTemperatureSetPoint);
	}
	// CCD ROI
	if (error == PicamError_None)
	{
		error = SetRoi(SetParameter.Roi);
	}
	// READOUT CONTROL
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ReadoutControlMode;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.tReadoutControlMode);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ReadoutPortCount;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.nReadoutPortCount);
	}
	// SHUTTER	
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ExposureTime;
		error = Picam_SetParameterFloatingPointValue(m_hDevice, parameter, SetParameter.fExposureTime);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ShutterTimingMode;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.tShutterTimingMode);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ShutterDelayResolution;
		error = Picam_SetParameterFloatingPointValue(m_hDevice, parameter, SetParameter.fShutterDelayResolution);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ShutterOpeningDelay;
		error = Picam_SetParameterFloatingPointValue(m_hDevice, parameter, SetParameter.fShutterOpeningDelay);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_ShutterClosingDelay;
		error = Picam_SetParameterFloatingPointValue(m_hDevice, parameter, SetParameter.fShutterClosingDelay);
	}
	// HARDWARE IO
	if (error == PicamError_None)
	{
		parameter = PicamParameter_TriggerResponse;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.tTriggerResponse);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_TriggerDetermination;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.tTriggerDetermination);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_OutputSignal;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.tOutputSignal);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_OutputSignal2;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.tOutputSignal2);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_InvertOutputSignal;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.nbInvertOutputsignal);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_InvertOutputSignal2;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.nbInvertOutputsignal2);
	}
	// SENSOR CLEANING
	if (error == PicamError_None)
	{
		parameter = PicamParameter_CleanCycleCount;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.nCleanCycleCount);
	}
	if (error == PicamError_None)
	{
		parameter = PicamParameter_CleanCycleHeight;
		error = Picam_SetParameterIntegerValue(m_hDevice, parameter, SetParameter.nCleanCycleHeight);
	}

	return error;
}

void CXRayCameraCtrl::ReadConfigFile(CString fullfilename)
{
	//CString strValue;
	char chGetString[128];
	int nValue = 0;
	double fValue = 0.0;

	DWORD strSize = 128;

	// ADC	
	GetPrivateProfileString(_T("ADC"), _T("ADC_QUALITY"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.tAdcQuaulity = nValue;

	GetPrivateProfileString(_T("ADC"), _T("ADC_SPEED"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%lf", &fValue);
	XrayCameraControlParameterInConfig.fAdcSpeed = fValue;

	GetPrivateProfileString(_T("ADC"), _T("ADC_ANALOG_GAIN"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.tAdcAnalogGain = nValue;

	// Sensor Temperature
	GetPrivateProfileString(_T("SENSOR TEMPERATURE"), _T("SENSOR_TEMPERATURE_SET_POINT"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%lf", &fValue);
	XrayCameraControlParameterInConfig.fSensorTemperatureSetPoint = fValue;

	// CCD ROI
	GetPrivateProfileString(_T("CCD ROI"), _T("X"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.Roi.x = nValue;

	GetPrivateProfileString(_T("CCD ROI"), _T("Y"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.Roi.y = nValue;

	GetPrivateProfileString(_T("CCD ROI"), _T("WIDTH"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.Roi.width = nValue;

	GetPrivateProfileString(_T("CCD ROI"), _T("HEIGHT"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.Roi.height = nValue;

	GetPrivateProfileString(_T("CCD ROI"), _T("X_BINNING"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.Roi.x_binning = nValue;

	GetPrivateProfileString(_T("CCD ROI"), _T("Y_BINNING"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.Roi.y_binning = nValue;

	// READOUT CONTROL
	GetPrivateProfileString(_T("READOUT CONTROL"), _T("READOUT_CONTROL_MODE"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.tReadoutControlMode = nValue;

	GetPrivateProfileString(_T("READOUT CONTROL"), _T("READOUT_PORT_COUNT"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.nReadoutPortCount = nValue;

	// SHUTTER	
	GetPrivateProfileString(_T("SHUTTER"), _T("EXPOSURE_TIME"), _T("100"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%lf", &fValue);
	XrayCameraControlParameterInConfig.fExposureTime = fValue;

	GetPrivateProfileString(_T("SHUTTER"), _T("SHUTTER_TIMING_MODE"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.tShutterTimingMode = nValue;

	GetPrivateProfileString(_T("SHUTTER"), _T("SHUTTER_DELAY_RESOLUTION"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%lf", &fValue);
	XrayCameraControlParameterInConfig.fShutterDelayResolution = fValue;

	GetPrivateProfileString(_T("SHUTTER"), _T("SHUTTER_OPENING_DELAY"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%lf", &fValue);
	XrayCameraControlParameterInConfig.fShutterOpeningDelay = fValue;

	GetPrivateProfileString(_T("SHUTTER"), _T("SHUTTER_CLOSDING_DELAY"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%lf", &fValue);
	XrayCameraControlParameterInConfig.fShutterClosingDelay = fValue;

	// HARDWARE IO
	GetPrivateProfileString(_T("HARDWARE IO"), _T("TRIGGER_RESPONSE"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.tTriggerResponse = nValue;

	GetPrivateProfileString(_T("HARDWARE IO"), _T("TRIGGER_DETERMINATION"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.tTriggerDetermination = nValue;
	
	GetPrivateProfileString(_T("HARDWARE IO"), _T("OUTPUT_SIGNAL"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.tOutputSignal = nValue;

	GetPrivateProfileString(_T("HARDWARE IO"), _T("OUTPUT_SIGNAL2"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.tOutputSignal2 = nValue;

	GetPrivateProfileString(_T("HARDWARE IO"), _T("INVERT_OUTPUT_SIGNAL"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.nbInvertOutputsignal = nValue;

	GetPrivateProfileString(_T("HARDWARE IO"), _T("INVERT_OUTPUT_SIGNAL2"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.nbInvertOutputsignal2 = nValue;

	// SENSOR CLEANING
	GetPrivateProfileString(_T("SENSOR CLEANING"), _T("CLEAN_CYCLE_COUNT"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.nCleanCycleCount = nValue;

	GetPrivateProfileString(_T("SENSOR CLEANING"), _T("CLEAN_CYCLE_HEIGHT"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	XrayCameraControlParameterInConfig.nCleanCycleHeight = nValue;

	// SupplymentaryParameter
	GetPrivateProfileString(_T("SUPPLYMENTARY_PARAMETER"), _T("NORMAL_TEMPERATURE"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%lf", &fValue);
	SupplymentaryParameter.NormalTemperature = fValue;

	GetPrivateProfileString(_T("SUPPLYMENTARY_PARAMETER"), _T("OPERATION_TEMPERATURE"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%lf", &fValue);
	SupplymentaryParameter.OperationTemperature = fValue;

	GetPrivateProfileString(_T("SUPPLYMENTARY_PARAMETER"), _T("ROI_X"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	SupplymentaryParameter.ImageRoi.x = nValue;

	GetPrivateProfileString(_T("SUPPLYMENTARY_PARAMETER"), _T("ROI_Y"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	SupplymentaryParameter.ImageRoi.y = nValue;

	GetPrivateProfileString(_T("SUPPLYMENTARY_PARAMETER"), _T("ROI_WIDTH"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	SupplymentaryParameter.ImageRoi.width= nValue;

	GetPrivateProfileString(_T("SUPPLYMENTARY_PARAMETER"), _T("ROI_HEIGHT"), _T("-1"), chGetString, strSize, fullfilename);
	sscanf(chGetString, "%d", &nValue);
	SupplymentaryParameter.ImageRoi.height = nValue;
}