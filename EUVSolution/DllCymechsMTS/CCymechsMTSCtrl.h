/**
 * Cymechs MTS Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

class AFX_EXT_CLASS CCymechsMTSCtrl : public CEthernetCom
{
public:
	CCymechsMTSCtrl();
	~CCymechsMTSCtrl();

	virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int				ReceiveData(char *lParam); ///< Recive

protected:
	/**
	*@var HANDLE m_hAckEvent
	ACK 메시지 수신 이벤트
	*/
	HANDLE					m_hAckEvent;

	/**
	*@var HANDLE m_hFlipDoneEvent
	Flip 동작 완료 이벤트
	*/
	HANDLE					m_hFlipDoneEvent;

	/**
	*@var HANDLE m_hAlignDoneEvent
	Align 동작 완료 이벤트
	*/
	HANDLE					m_hAlignDoneEvent;

	/**
	*@var HANDLE m_hRobotDoneEvent
	Robot 동작 완료 이벤트
	*/
	HANDLE					m_hRobotDoneEvent;

	/**
	*@var HANDLE m_hLpmDoneEvent
	LPM 동작 완료 이벤트
	*/
	HANDLE					m_hLpmDoneEvent;

	/**
	*@var HANDLE m_hInitEvent
	Initialize 동작 완료 이벤트
	*/
	HANDLE					m_hInitEvent;

	/**
	*@var HANDLE m_hTagReadEvent
	Tag Reading 완료 이벤트
	*/
	HANDLE					m_hTagReadEvent;

	/**
	*@var HANDLE m_hSpeedReadEvent
	Robot Speed 수신 완료 이벤트
	*/
	HANDLE					m_hSpeedReadEvent;

	/**
	*@var BOOL m_bOccurAlarm
	알람 발생 확인 변수
	*/
	BOOL					m_bOccurAlarm;

	/**
	*@var BOOL m_bRobotInitComp
	로봇 이니셜 완료 확인 변수
	*/
	BOOL					m_bRobotInitComp;

	/**
	*@var BOOL m_bRobotWorking
	로봇 동작 확인 변수
	*/
	BOOL					m_bRobotWorking;

	/**
	*@var BOOL m_bRobotWithMask
	로봇 마스크 유무 확인 변수
	*/
	BOOL					m_bRobotWithMask;

	/**
	*@var BOOL m_bRobotRetracted
	로봇 Retract 확인 변수
	*/
	BOOL					m_bRobotRetracted;

	/**
	*@var BOOL m_bRobotError
	로봇 에러 확인 변수
	*/
	BOOL					m_bRobotError;

	/**
	*@var BOOL m_bRotatorInitComp
	얼라이너 이니셜 완료 확인 변수
	*/
	BOOL					m_bRotatorInitComp;

	/**
	*@var BOOL m_bRotatorWorking
	얼라이너 동작 확인 변수
	*/
	BOOL					m_bRotatorWorking;

	/**
	*@var BOOL m_bRotatorWithMask
	얼라이너 마스크 유무 확인 변수
	*/
	BOOL					m_bRotatorWithMask;

	/**
	*@var BOOL m_bRotatorError
	얼라이너 에러 확인 변수
	*/
	BOOL					m_bRotatorError;

	/**
	*@var BOOL m_bFlipperInitComp
	플립퍼 이니셜 완료 확인 변수
	*/
	BOOL					m_bFlipperInitComp;

	/**
	*@var BOOL m_bFlipperWorking
	플립퍼 동작 확인 변수
	*/
	BOOL					m_bFlipperWorking;

	/**
	*@var BOOL m_bFlipperWithMask
	플립퍼 마스크 유무 확인 변수
	*/
	BOOL					m_bFlipperWithMask;

	/**
	*@var BOOL m_bFlipperError
	플립퍼 에러 확인 변수
	*/
	BOOL					m_bFlipperError;

	/**
	*@var BOOL m_bLpmInitComp
	로드포트 이니셜 완료 확인 변수
	*/
	BOOL					m_bLpmInitComp;

	/**
	*@var BOOL m_bLpmWorking
	로드포트 동작 확인 변수
	*/
	BOOL					m_bLpmWorking;

	/**
	*@var BOOL m_bPodOnLPM
	POD 안착 여부 확인 변수
	*/
	BOOL					m_bPodOnLPM;

	/**
	*@var BOOL m_bPodPresenceOn
	POD Presence On 확인 변수
	*/
	BOOL					m_bPodPresenceOn;

	/**
	*@var BOOL m_bPodPlacementOn
	POD Placement On 확인 변수
	*/
	BOOL					m_bPodPlacementOn;

	/**
	*@var BOOL m_bLoadportClamped
	로드포트 클램프 확인 변수
	*/
	BOOL					m_bLoadportClamped;

	/**
	*@var BOOL m_bPodOpened
	로드포트 오픈 확인 변수
	*/
	BOOL					m_bPodOpened;

	/**
	*@var BOOL m_bMaskInPod
	Pod안의 Mask 유무 확인 변수
	*/
	BOOL					m_bMaskInPod;

	/**
	*@var BOOL m_bLpmError
	로드포트 에러 확인 변수
	*/
	BOOL					m_bLpmError;

	/**
	*@var BOOL m_bMtsEMOStatus
	MTS EMO 상태 확인 변수
	*/
	BOOL					m_bMtsEMOStatus;

	/**
	*@var BOOL m_bMtsError
	MTS 에러 확인 변수
	*/
	BOOL					m_bMtsError;

	/**
	*@var BOOL m_bPioManualMode
	PIO 모드 확인 변수
	*/
	BOOL					m_bPioManualMode;

	/**
	*@var CString m_strPioInBit
	PIO Input 신호
	*/
	CString					m_strPioInBit;

	/**
	*@var CString m_strPioOutBit
	PIO Output 신호
	*/
	CString					m_strPioOutBit;

	/**
	*@var BOOL m_strRfidTagData
	Rfid Tag 데이터
	*/
	CString					m_strRfidTagData;

	/**
	*@var BOOL m_strRobotSpeed
	Robot 속도 값
	*/
	CString					m_strRobotSpeed;

	/**
	*@var BOOL m_strCancelCode
	Cancel Code 값
	*/
	CString					m_strCancelCode;

	/**
	*@var CString m_strErrorCode
	MTS 에러 확인 변수
	*/
	CString					m_strErrorCode;

	/**
	*@var int m_nSignalLampRed
	
	*/
	int						m_nSignalLampRed;

	/**
	*@var int m_nSignalLampYellow
	
	*/
	int						m_nSignalLampYellow;

	/**
	*@var int m_nSignalLampGreen
	
	*/
	int						m_nSignalLampGreen;

	/**
	*@var int m_nSignalLampBlue
	
	*/
	int						m_nSignalLampBlue;

	

	/**
	*@fn void ParsingReceivedMsg(CString strRecvMsg)
	*@brief 수신 받은 데이터 파싱
	*@date 2019/07/31
	*@param strRecvMsg 수신받은 메시지 내용
	*/
	void					ParsingData(CString strRecvMsg);

	/**
	*@fn CString HexStringToBinary(char strHex)
	*@brief 16진수를 2진수로 변환
	*@date 2019/07/31
	*@param strHex 16진수값
	*/
	CString					HexStringToBinary(char strHex);

	/**
	*@fn int InitDevice()
	*@brief 모듈 초기화와 에러 클리어 한다
	*@date 2019/07/04
	*@remark \n
			 모듈이 동작 상태라면 명령은 무시된다 \n
			 모듈이 에러 상태라면 INIT~명령으로 에러를 클리어할 수 있다
	*/
	int						InitDevice();

	/**
	*@fn int InitModule(CString strModule)
	*@brief 해당 모듈 초기화와 에러 클리어 한다
	*@date 2019/11/08
	*@param strModule 유닛 번호
	*@remark \n
			 해당 모듈이 동작 상태라면 명령은 무시된다 \n
			 해당 모듈이 에러 상태라면 INIT~명령으로 에러를 클리어할 수 있다
	*/
	int						InitModule(CString strModule);
	
	/**
	*@fn void GetAlarmMessage()
	*@brief MTS의 발생된 에러를 알려준다 
	*@date 2019/07/04
	*@remark \n
			 에러가 발생했을 때 이벤트로 알람 메시지를 날려준다 \n
			 만약 "ALMSG"명령을 보내면 마지막 에러를 알려준다 \n
			 [응답형식] (6byte) \n
			 0: UnitNumber \n
			 1: "0" No Error "1" Error \n
			 2~5: 에러코드
	*/
	void					GetAlarmMessage();

	/**
	*@fn void GetMtsStatus()
	*@brief MTS 상태를 알려준다
	*@date 2019/07/04
	*@remark \n
			 [응답 형식] - MTS (7byte) \n
			 1 Status				0:Normal 1:Error \n
			 2 Signal Lamp	RED		0:OFF 1:ON 2:BLINK \n
			 3 Signal Lamp  YELLOW  0:OFF 1:ON 2:BLINK \n
			 4 Signal Lamp  GREEN	0:OFF 1:ON 2:BLINK \n
			 5 Signal Lamp  BLUE	0:OFF 1:ON 2:BLINK \n
			 6 EMO Status			0:OFF 1:ON \n
			 7 FFU Status(Hex)		Bit3:Not Use, Bit2:Door Close, Bit1:Pressure Fail, Bit0:RPM Fail \n
	*/
	void					GetMtsStatus();

	/**
	*@fn void GetRobotStatus()
	*@brief 로봇암 상태를 알려준다
	*@date 2019/07/04
	*@remark \n
			 [응답 형식] - Robot (4byte) \n
			 1 Robot Init Presence			0:Incompletion 1:Completion \n
			 2 Robot Status					0:Stop 1:Moving 2:Pause 3:Error \n
			 3 1st Arm Mask Presence		0:None 1:Existence \n
			 4 2nd Arm Mask Presence		0:None 1:Existence \n
	*/
	void					GetRobotStatus();
	
	/**
	*@fn void GetRotatorStatus()
	*@brief 얼라이너 상태를 알려준다
	*@date 2019/07/04
	*@remark \n
			 [응답 형식] - Aligner (3byte) \n
			 1 Aligner Initialize Presence	0:Incompletion 1:Completion \n
			 2 Aligner Status				0:Stop(inlude Initial Completed) 1:Moving 2:Align Completed 3:Error \n
			 3 Mask Presence				0:Not Presence 1:Presence \n
	*/
	void					GetRotatorStatus();
	
	/**
	*@fn void GetLoadportStatus()
	*@brief 로드포트 상태를 알려준다
	*@date 2019/07/04
	*@remark \n
			 [응답 형식] - Load Port (17byte) \n
			 1 Load Port Initialize Presence	0:Incompletion 1:Completion \n
			 2 Load Port Status					0:Stop 1:Moving 2:Pause 3:Error \n
			 3 FOUP Status						0:Not Presence 1:Existence 2:Presence on 3:Placement on \n
			 4 Door Status						0:Close 1:Open 2:Unknown \n
			 5 Clamp Status						0:Unclamp 1:Clamp \n
			 6 RFID Status						0:Ready 1:Reading 2:Read Complete 3:Writing 4:Write Complete 5:Error \n
			 7 AGV Status						0:Ready 1:Busy 3:Error(Fixed:0) \n
			   Dock Status(Specific equipment)	0:Undock 1:Dock \n
			 8 Load Port Enable					0:Not Use 1:Use \n
			 9 RFID Enable						0:Not Use 1:Use \n
			10 Close Mapping Enable				0:Not Use 1:Use \n
			11 Online Mode						0:Manual 1:AMHS \n
			12 Load Button Status				0:Off 1:On \n
			13 Unload Button Status				0:Off 1:On \n
			14 PIO Input(High Word)	 Bit7=CONT	1. 0:Off 1:On \n
									 Bit6=COMPT	2. Return Hex Reply.(00~FF) \n
									 Bit5=BUSY \n
									 Bit4=TR_REQ \n
			15 PIO Input(Low Word)	 Bit3=N.C \n
									 Bit2=CS_1 \n
									 Bit1=CS_0 \n
									 Bit0=VALID \n
			16 PIO Output(High Word) Bit7=ES	  1. 0:Off 1:On \n
									 Bit6=HO_AVBL 2. Return Hex Reply.(00~FF) \n
									 Bit5=N.C \n
									 Bit4=N.C \n
			17 PIO Output(Low Word)  Bit3=READY \n
									 Bit2=N.C \n
									 Bit1=U_REQ \n
									 Bit0=L_REQ \n

	*/
	void					GetLoadportStatus();

	/**
	*@fn void GetFlipperStatus()
	*@brief 플립퍼 상태를 알려준다
	*@date 2019/07/04
	*@remark \n
			 Status Format - Flipper (3byte) \n
			 1 Flipper Initialize Presence	0:Incompletion 1:Completion \n
			 2 Flipper Status				0:Stop(inlude Initial Completed) 1:Moving 2:Flip Completed 3:Error \n
			 3 Wafer Presence				0:Not Presence 1:Presence \n
	*/
	void					GetFlipperStatus();

	/**
	*@fn void RetryE84Communication()
	*@brief E84 통신중 에러발생 시 자동적으로 통신을 초기화 한다
	*@date 2019/07/04
	*/
	void					RetryE84Communication();

	/**
	*@fn void ConcludeE84Communication()
	*@brief E84 통신중 에러발생 시 자동적으로 통신을 끝낸다
	*@date 2019/07/04
	*/
	void					ConcludeE84Communication();

	/**
	*@fn void GetPioTimeoutValue()
	*@brief MTS에 세팅되어 있는 PIO 타임아웃 값을 알려준다
	*@date 2019/07/04
	*@remark \n
			 6블럭(TP1~TP6)로 알려주고 블럭과 블럭사이는 "/"로 구분된다 \n
			 ex) 000~999/000~999/000~999/000~999/000~999/000~999
	*/
	void					GetPioTimeoutValue();

	/**
	*@fn void ResetPioError()
	*@brief PIO 통신과 연관된 에러를 제거한다
	*@date 2019/07/04
	*/
	void					ResetPioError();

	/**
	*@fn void SetPioControl(int nLreq, int nUreq, int nNc1, int nReady, int nNc2, int nHoAvbl, int nEs)
	*@brief PIO 출력을 제어한다
	*@date 2019/07/04
	*@remark \n
			 Setting Status(8byte) \n
			 1.L_REQ	0:Off 1:On \n
			 2.U_REQ	0:Off 1:On \n
			 3.N.C		0:Off 1:On \n
			 4.READY	0:Off 1:On \n
			 5.N.C		0:Off 1:On \n
			 6.N.C		0:Off 1:On \n
			 7.HO_AVBL	0:Off 1:On \n
			 8.ES		0:Off 1:On \n
	*/
	void					SetPioControl(int nLreq, int nUreq, int nNc1, int nReady, int nNc2, int nNc3, int nHoAvbl, int nEs);

	/**
	*@fn int RobotMoveHome()
	*@brief 시작 위치로 로봇을 움직인다
	*@date 2019/07/04
	*@remark \n
			 에러상태를 리셋하지는 않는다
	*/
	int						RobotMoveHome();
	
	/**
	*@fn void RobotMovePause()
	*@brief 로봇을 잠정적으로 정지 시킨다
	*@date 2019/07/04
	*@remark \n
			 로봇이 동작중이지 않을 때 "PAUSE" 명령은 취소된다 \n
			 재동작 시키기위해 "RESUM" 명령을 사용할 수 있다 \n
			 로봇 상태가 일시정지 상태라면 "STAT~", "ALMSG", "RESUM", "ABORT" 명령은 \n
			 동작하지만 그 외의 명령은 취소된다
	*/
	void					RobotMovePause();

	/**
	*@fn void RobotMoveResume()
	*@brief 로봇이 일시정지라면 일시정지를 풀고 일시정지전 작업을 한다
	*@date 2019/07/04
	*@remark \n
			 로봇이 일시정지 상태가 아니라면 "RESUM"명령은 무시된다
	*/
	void					RobotMoveResume();

	/**
	*@fn void RobotMoveStop()
	*@brief 로봇 동작을 정지 시킨다
	*@date 2019/07/04
	*@remark \n
			 동작상태가 아니라면 "STOP"명령은 무시된다
	*/
	void					RobotMoveStop();

	/**
	*@fn void GetRobotPosition()
	*@brief 로봇의 현재 위치정보를 알려준다
	*@date 2019/07/04
	*@remark \n
			 [응답 형식] (44byte) \n
			 Dual Blade		Dual Arm	Single Arm		Status Format \n
			 TH-Axis		TH-Axis		TH-Axis			[XXXXXXXX] \n		
			 R-Axis			R1-Axis		R-Axis			[XXXXXXXX] \n		
			 H1-Axis		R2-Axis		Z-Axis			[XXXXXXXX] \n		
			 H2-Axis		Z-Axis						[XXXXXXXX] \n		
			 Z-Axis										[XXXXXXXX] \n		
			 (존재하지 않는 축은 "00000000"로 표시)
	*/
	void					GetRobotPosition();
	
	/**
	*@fn void GetRobotModelType()
	*@brief 로봇 모델 타입을 요청한다
	*@date 2019/07/04
	*/
	void					GetRobotModelType();

	/**
	*@fn void SetRobotSpeed(int nLowerArmWithoutMask, int nLowerArmWithMask, int nUpperArmWithoutMask, int nUpperArmWithMask)
	*@brief 로봇의 속도를 변경한다
	*@date 2019/07/04
	*@remark \n
			 Setting Status(8byte) \n
			 1, 2	Arm A(Lower)	Mask Unload Status		Speed	01~12 \n
			 3, 4	Arm A(Lower)	Mask Load Status		Speed	01~12 \n
			 5, 6	Arm B(Upper)	Mask Unload Status		Speed	01~12 \n
			 7, 8	Arm B(Upper)	Mask Load Status		Speed	01~12 \n
			 * Speed '01'은 10%를 의미한다
	*/
	void					SetRobotSpeed(int nLowerArmWithoutMask, int nLowerArmWithMask, int nUpperArmWithoutMask, int nUpperArmWithMask);

	/**
	*@fn int GetRobotSpeed()
	*@brief 로봇의 현재 속도값을 가져온다
	*@date 2019/07/04
	*@remark \n
			 Response Format Status(8byte) \n
			 1, 2	Arm A(Lower)	Mask Unload Status		Speed	01~12 \n
			 3, 4	Arm A(Lower)	Mask Load Status		Speed	01~12 \n
			 5, 6	Arm B(Upper)	Mask Unload Status		Speed	01~12 \n
			 7, 8	Arm B(Upper)	Mask Load Status		Speed	01~12 \n
			 * Speed '01'은 10%를 의미한다
	*/
	int						GetRobotSpeed();

	/**
	*@fn int LoadPod()
	*@brief Pod를 로드포트에 도킹 후 Pod를 열고 맵핑한다
	*@date 2019/07/04
	*@remark \n
			 매핑 데이터는 알려주지 않는다
	*/
	int						LoadPod();

	/**
	*@fn int UnloadPod()
	*@brief 로드포트에 열려있는 Pod를 닫고 언도킹한다
	*@date 2019/07/04
	*@remark \n
			 언로딩시에 클램프 릴리즈 동작은 사용되지 않는다
	*/
	int						UnloadPod();

	/**
	*@fn int MoveDockingPos()
	*@brief Pod를 로드포트 위의 도킹 위치로 움직인다
	*@date 2019/07/04
	*/
	int						MoveDockingPos();

	/**
	*@fn int MoveUndockingPos()
	*@brief Pod를 로드포트 위의 언도킹 위치로 움직인다
	*@date 2019/07/04
	*/
	int						MoveUndockingPos();

	/**
	*@fn void PlacePodOnLoadport()
	*@brief 로드포트에 Pod를 놓는다
	*@date 2019/07/04
	*@remark \n
			 시뮬레이션을 위한 함수이다
	*/
	void					PlacePodOnLoadport();

	/**
	*@fn void RemovePodOnLoadport()
	*@brief 로드포트에서 Pod를 제거한다
	*@date 2019/07/04
	*@remark \n
			 시뮬레이션을 위한 함수이다
	*/
	void					RemovePodOnLoadport();

	/**
	*@fn int OpenPod()
	*@brief 로드포트에 Pod를 도킹한 후 Pod를 연다
	*@date 2019/07/04
	*@remark \n
			 맵핑 동작은 하지 않는다 \n
			 맵핑 동작을 제외하고 "LOADL" 명령과 같다
	*/
	int						OpenPod();
	
	/**
	*@fn int ClosePod()
	*@brief 로드포트에 있는 Pod를 닫고 언도킹한다
	*@date 2019/07/04
	*@remark \n
			 클램프 릴리즈 동작은 사용되지 않는다 \n
			 닫을때 "MODE~" 명령을 사용해서 매핍플래그를 사용할 수 있다
	*/
	int						ClosePod();

	/**
	*@fn void GetMappingData()
	*@brief 로드포트의 마스크 맵핑 데이터를 알려준다
	*@date 2019/07/04
	*@remark \n
			 [응답 형식] (25byte) \n
			 0: Not Presence \n
			 1: Presence \n
			 2: Double \n
			 3: Cross \n
			 4: Position Error \n
	*/
	void					GetMappingData();

	/**
	*@fn void RetryMapping()
	*@brief 맵핑을 재시도한다
	*@date 2019/07/04
	*@remark \n
			 이 명령은 Door가 열려있는 상태에서 사용할 수 있다
	*/
	void					RetryMapping();

	/**
	*@fn void ClampPod()
	*@brief Pod를 클램프 한다
	*@date 2019/07/04
	*/
	void					ClampPod();
	
	/**
	*@fn void UnclampPod()
	*@brief Pod를 언클램프 한다
	*@date 2019/07/04
	*/
	void					UnclampPod();

	/**
	*@fn void AbortAllModules()
	*@brief 모듈이 일시정지 상태라면 일시정지상태를 풀고 일시정지 전 작업을 정지한다 \n
			만약 에러가 발생한 상태라면 "ABORT"명령으로 에러를 릴리즈 할 수 있다
	*@date 2019/07/04
	*/
	void					AbortAllModules();

	/**
	*@fn void SetSignalLamp()
	*@brief 
	*@date 2019/07/04
	*@param Red, Yellow, Green, Blue, Buzzer 동작 
	*@remark \n
			 [명령 형식] 5byte \n
			 Lamp(Red, Yellow, Green, Blue) 0:Off 1:On 2:Blink \n
			 Buzzer 0:Off 1:On
	*/
	void					SetSignalLamp(int nRed, int nYellow, int nGreen, int nBlue, int nBuzz);

	/**
	*@fn int ReadRfidData()
	*@brief RFID MID Scope 혹은 각 페이지의 값을 읽는다
	*@date 2019/07/04
	*@param 읽을 페이지 값
	*@remark \n
			 Setting Status(2byte) \n
			 "00" MID Area \n
			 "01" 1Page \n
			 "02" 2Page \n
			       :	\n
			 "17" 17Page
	*/
	int						ReadRfidData(int nPage = 0);

	/**
	*@fn void GetFfuStatus()
	*@brief FFU RPM 데이터와 압력 데이터를 알려준다
	*@date 2019/07/04
	*@remark \n
			 각 데이터는 "/"로 나누어져있다 \n
			 [응답 형식] 9byte \n
			 1~4 : RPM (0000~9999) \n
			   5 : / \n
			 6~9 : Pressure (0000~9999)
	*/
	void					GetFfuStatus();

	/**
	*@fn int PickMaskFromLoadPort()
	*@brief 로드포트로부터 마스크를 가져온다
	*@date 2019/07/04
	*/
	int						PickMaskFromLoadPort();
	
	/**
	*@fn int PlaceMaskToLoadPort()
	*@brief 로드포트에 마스크를 가져다 놓는다
	*@date 2019/07/04
	*/
	int						PlaceMaskToLoadPort();
	
	/**
	*@fn int PickMaskFromFlipper()
	*@brief 플립퍼로부터 마스크를 가져온다
	*@date 2019/07/04
	*/
	int						PickMaskFromFlipper();
	
	/**
	*@fn int PlaceMaskToFlipper()
	*@brief 플립퍼에 마스크를 가져다 놓는다
	*@date 2019/07/04
	*/
	int						PlaceMaskToFlipper();
	
	/**
	*@fn int PickMaskFromRotator()
	*@brief Rotator로부터 마스크를 가져온다
	*@date 2019/07/04
	*/
	int						PickMaskFromRotator();
	
	/**
	*@fn int PlaceMaskToRotator()
	*@brief Rotator에 마스크를 가져다 놓는다
	*@date 2019/07/04
	*/
	int						PlaceMaskToRotator();
	
	/**
	*@fn int PickMaskFromStage()
	*@brief 스테이지로부터 마스크를 가져온다
	*@date 2019/07/04
	*/
	int						PickMaskFromStage();
	
	/**
	*@fn int PlaceMaskToStage()
	*@brief 스테이지에 마스크를 가져다 놓는다
	*@date 2019/07/04
	*/
	int						PlaceMaskToStage();

	/**
	*@fn int SetRobotReadyPos(CString strUnitNum)
	*@brief 해당 유닛앞으로 로봇을 대기시킨다
	*@date 2020/01/22
	*/
	int						SetRobotReadyPos(CString strUnitNum);

	/**
	*@fn int RotateMask(int nAngle)
	*@brief 마스크를 회전 시킨다
	*@date 2019/07/04
	*@param 회전 시키려는 각도
	*@Remark \n
			 Deg : 5byte (scale : "00000" ~ "35999") (Unit:0.01deg) 
	*/
	int						RotateMask(int nAngle);

	/**
	*@fn int OriginRotator()
	*@brief 마스크 얼라이너의 오리진 동작
	*@date 2019/08/19
	*@Remark \n
			 ALNR~ 명령 후 오리진 필요
	*/
	int						OriginRotator();

	/**
	*@fn int GripMask(int nStatus)
	*@brief 마스크 그립 On/Off 동작
	*@date 2019/08/19
	*@param \n
			0 : Off, 1 : On
	*/
	int						GripMask(int nStatus);

	/**
	*@fn int FlipMask()
	*@brief 마스크 Flip 동작
	*@date 2019/08/19
	*/
	int						FlipMask();

	/**
	*@fn int OriginFlipper()
	*@brief Flipper Origin 동작
	*@date 2020/12/17
	*/
	int						OriginFlipper();

	/**
	*@fn void SetPioModeToAMHS(BOOL bCheck)
	*@brief PIO 모드 변경 (MANUAL - AMHS)
	*@date 2020/12/17
	*@param \n
			FALSE : MANUAL, TRUE : AMHS
	*/
	void					SetPioModeToAMHS(BOOL bCheck);

	/**
	*@fn void ClearAllAlarm()
	*@brief 발생한 모든 알람을 클리어 시킨다
	*@date 2019/07/05
	*/
	void					ClearAllAlarm();
	
	/**
	*@fn void GetCheckSumValue(CString strMsg)
	*@brief 전송 메시지에 대한 체크섬 값을 구한다
	*@date 2019/07/04
	*@param 전송 메시지
	*@return 체크섬 값 리턴
	*/
	CString					GetCheckSumValue(CString strMsg);

	/**
	*@fn int WaitExecAckEvnet(int nTimeout)
	*@brief Ack 메시지 수신 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecAckEvent(int nTimeout);
	
	/**
	*@fn int WaitExecAlignDoneEvent(int nTimeout)
	*@brief Align 동작완료 메시지 수신 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecAlignDoneEvent(int nTimeout);
	
	/**
	*@fn int WaitExecFlipDoneEvent(int nTimeout)
	*@brief Flip 동작완료 메시지 수신 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecFlipDoneEvent(int nTimeout);
	
	/**
	*@fn int WaitExecRobotDoneEvent(int nTimeout)
	*@brief Robot 동작완료 메시지 수신 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecRobotDoneEvent(int nTimeout);
	
	/**
	*@fn int WaitExecLpmDoneEvent(int nTimeout)
	*@brief Lpm 동작완료 메시지 수신 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecLpmDoneEvent(int nTimeout);

	/**
	*@fn int WaitExecInitEvent(int nTimeout)
	*@brief Initial 동작완료 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecInitEvent(int nTimeout);

	/**
	*@fn int WaitExecTagReadEvent(int nTimeout)
	*@brief Tag Reading 완료 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecTagReadEvent(int nTimeout);

	/**
	*@fn int WaitExecSpeedReadEvent(int nTimeout)
	*@brief Robot Speed Reading 완료 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int						WaitExecSpeedReadEvent(int nTimeout);
};

